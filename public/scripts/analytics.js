class Analytics {
  trackEvent(eventAction, eventValue1, eventValue2) {   
    if (!this.trackingId) {
      console.error('You need to set a trackingId, for example:');
      console.error('self.analytics.trackingId = \'UA-XXXXXXXX-X\';');

      // We want this to be a safe method, so avoid throwing Unless
      // It's absolutely necessary.
      return Promise.resolve();
    }

    if (!eventAction && !eventValue1) {
      console.warn('sendAnalyticsEvent() called with no eventAction or ' +
      'eventValue1.');
      return Promise.resolve();
    }

    return self.registration.pushManager.getSubscription()
    .then(subscription => {
      if (subscription === null) {
        // The user has not subscribed yet.
        throw new Error('No subscription currently available.');
      }

      const payloadData = {
        // Version Number
        v: 1,
        // Client ID
        //cid: subscription.endpoint,
        cid: '555',
        // Tracking ID
        tid: this.trackingId,
        // Hit Type
        t: 'event',
        // Data Source
        ds: 'serviceworker',
        // Event Category
        ec: 'Notification',
        // Event Action
        ea: eventAction,
        el: eventAction,
        // Custom dimension 8 : Page source
        cd8: eventValue1,
        // Custom dimension 14 : Extension Data 
        cd14: eventValue2
      };
      const payloadString = Object.keys(payloadData)
      .filter(analyticsKey => {
        return payloadData[analyticsKey];
      })
      .map(analyticsKey => {
        return `${analyticsKey}=` +
          encodeURIComponent(payloadData[analyticsKey]);
      })
      .join('&');

      return fetch('https://www.google-analytics.com/collect', {
        method: 'post',
        body: payloadString
      });
    })
    .then(response => {     
      if (!response.ok) {
        return response.text()
        .then(responseText => {
          throw new Error(
            `Bad response from Google Analytics ` +
            `[${response.status}] ${responseText}`);
        });
      }
    })
    .catch(err => {
      console.warn('Unable to send the analytics event', err);
    });
  }
}

if (typeof self !== 'undefined') {
  self.analytics = new Analytics();
}