"use strict";

if ('serviceWorker' in navigator && 'PushManager' in window) {
  try {
    navigator.serviceWorker.register('/service_worker.js');
  }
  catch (e) {
    console.log('Can not register Service Worker', e);
  }
}