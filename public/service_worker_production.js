'use strict';
importScripts('./scripts/analytics.js');
self.analytics.trackingId = 'UA-70494016-5';

self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  event.waitUntil(clients.matchAll({
    includeUncontrolled: true,
    type: "window"
  }).then(function(clientList) {
    if (event && event.notification && event.notification.data) {
      self.analytics.trackEvent('notification-click', event.notification.data.notification_type, event.notification.data.extension);
    }

    for (var i = 0; i < clientList.length; i++) {
      var client = clientList[i];
      if (client.url == event.notification.tag)
        return client.focus();
    }
    if (clients.openWindow)
      return clients.openWindow(event.notification.tag);
  }));
});


self.addEventListener('push', function(event) {
  const message = event.data.json();
  const clientUrl = 'https://m.ymeet.me';
  let needShowNotification = true;

  const options = {
    body: message.body,
    icon: message.icon,
    tag: message.ref_url,
    data: message.data
  };
  event.waitUntil(clients
    .matchAll({ includeUncontrolled: true, type: 'all'})
    .then(function(windowClients) {
      for (var i = 0; i < windowClients.length; i++) {
        var client = windowClients[i];
        if (options.data.notification_type !== 'chat_message' && client.url.indexOf(clientUrl) === 0 && client.focused) {
          needShowNotification = false;
//console.log("Do not need to show notification", message)
          break;
        }
      }
      if (needShowNotification) {
        self.analytics.trackEvent('notification-show', options.data.notification_type, options.data.extension)
        self.registration.showNotification(message.title, options)
      }
    })
  )
});


