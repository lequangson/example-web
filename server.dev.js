var path = require('path');
var express = require('express');
var webpack = require('webpack');
fs = require('fs');
var config = require('./webpack.config.dev');

/*
var cookieParser = require('cookie-parser');
var React = require('react/addons');
*/
var app = express();
var port = 8088;
var compiler = webpack(config);

app.use(express.static('public'));

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("X-Frame-Options", "SAMEORIGIN");
    res.header("X-XSS-Protection", "1");
    res.header("X-Content-Type-Options", "nosniff");
    next();
});

app.get('/robots.txt', function(req, res) {
  res.sendFile(path.join(__dirname, 'public', 'robots.txt'));
});

app.get('/sitemap.xml', function(req, res) {
  res.sendFile(path.join(__dirname, 'public', 'sitemap.xml'));
});

app.get('/landingpage.html', function(req, res) {
  res.sendFile(path.join(__dirname, 'landingpage.html'));
});

// the following for similar sharing page only
app.get('/similar-sharing/:current_user_id/:mode/:id/:similar_img', function(req, res) {
  fs.readFile('sharing.html', 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    res.send(data.replace('__sharing_img_url__', 'https://ymmtest.s3.amazonaws.com/combineimage/' + req.params.similar_img))
  });
  // res.sendFile(path.join(__dirname, 'sharing.html'));
});

// the following for minigae sharing page only
app.get('/quizvui/question/:question_id/:share_img', function(req, res) {
  fs.readFile('sharing.html', 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    if (req.params.share_img) {
      res.send(data.replace('__sharing_img_url__', 'https://cdn-static.ymeet.me/general/MiniGame/' + req.params.share_img));
    } else {
      res.send(data.replace('__sharing_img_url__', 'https://cdn-static.ymeet.me/general/LandingPage/shutterstock_360691904.jpg'))

    }
  });
  // res.sendFile(path.join(__dirname, 'sharing.html'));
});

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

var server = app.listen(port, '0.0.0.0', function(err) {
  if (err) {
    console.log(err);
    return;
  }
  console.log('Listening at http://0.0.0.0:' + port);
})
