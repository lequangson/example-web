#!/bin/bash
# git checkout -- .
git pull origin master
npm install
basetime=$(date +%s%N)
cp ./Winwheel.js ./node_modules/winwheel/lib/Winwheel.js
npm run build
sed -i -e "s/__version__/${basetime}/g" ./build/production/index.html
echo "Deploy DONE!!!!"
