var path = require('path');
// var compression = require('compression')
var express = require('express');
var webpack = require('webpack');
var config = require('./webpack.config.prod');

var app = express();
// app.use(compression())
var port = 8088;
var compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("X-Frame-Options", "SAMEORIGIN");
    res.header("X-XSS-Protection", "1");
    res.header("X-Content-Type-Options", "nosniff");
    next();
});

app.get('/robots.txt', function(req, res) {
  res.sendFile(path.join(__dirname, 'public', 'robots.txt'));
});

app.get('/sitemap.xml', function(req, res) {
  res.sendFile(path.join(__dirname, 'public', 'sitemap.xml'));
});

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});


app.post("/api/login", function(req, res) {
  var jwt = require('jsonwebtoken');
  var fs = require('fs');
  // var token = jwt.sign({ foo: 'bar' }, 'shhhhh');
  var cert = fs.readFileSync('private.rsa');  // get private key
  var public_api_key = 'change_me';
    var payload = {
    public_api_key: public_api_key,
    ip_address: '127.0.0.1',
    issue_time: '2016-07-04 00:00:00'
  }
  var token = jwt.sign(payload, cert, { algorithm: 'RS256'});
  res.json({token: token});
});

app.post("/api/get-api-key", function(req, res) {
  var jwt = require('jsonwebtoken');
  var fs = require('fs');
  // var token = jwt.sign({ foo: 'bar' }, 'shhhhh');
  var cert = fs.readFileSync('private.rsa');  // get private key
  var public_api_key = 'change_me';
    var payload = {
    public_api_key: public_api_key,
    ip_address: '127.0.0.1',
    issue_time: '2016-07-04 00:00:00'
  }
  var token = jwt.sign(payload, 'change_me', { algorithm: 'HS256'});
  res.json({token: token});
});
var server = app.listen(port, '0.0.0.0', function(err) {
  if (err) {
    console.log(err);
    return;
  }
  console.log('Listening at http://0.0.0.0:' + port);
})
//the following for socket
var socket = require('./socket.js');
var io = require('socket.io').listen(server);
io.sockets.on('connection', socket);
