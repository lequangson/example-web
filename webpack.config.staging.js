var path = require('path');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var src_dir = path.dirname(path.dirname(__filename));
module.exports = {
  entry: [
    'whatwg-fetch',
    'babel-polyfill',
    './client/index'
  ],
  output: {
    path: './build/staging',
    filename: 'js/bundle.js',
  },
  resolve: {
    root: [
      src_dir
    ]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('staging'),
        'FACEBOOK_APP_ID': "'1715923778659669'",
        'BASE_API_AUTHEN_URL': "'//auth.test.ymeet.me:8611/'",
        'BASE_API_URL': "'//api.test.ymeet.me:8630/'",
        'CHAT_SERVER_URL' : "'http://chat.staging.ymeet.me/'",
        'CHAT_API_URL': "'http://chat-api.staging.ymeet.me:8698/'",
        'SOCKET_URL': "'//socket.staging.ymeet.me:8687/'",
        'API_GENERATE_KEY_URL': "'//api-key.staging.ymeet.me:8687/api/get-api-key'",
        'ANONYMOUS_TOKEN': "'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjowLCJpcF9hZGRyZXNzIjpudWxsLCJpYXQiOjE0ODIzOTEyNzQsImV4cCI6ODk0NzM1MTI3NH0.TGLei5Dy3FYDjz8gjnR4Zc7ukjgmaFwIPLLbOcpCCvU'",
        'DOMAIN':"'staging.ymeet.me'",
        'CUSTOMER_SUPPORT_USER_ID': "3759",
        'FCM_SERVER_PUBLIC_KEY': "'BNVDDGV2omVj1ZQipqcoyAeouJA36pCkYYXXNjD5PsLGzxuZveBpBPlI5M0CJUfhx4F8aoVtqfxeOCqG1ClPKoE'",
        'GOOGLE_API_CLIENT_ID': "'888754797622-697vcqrk2ie8ela4280e5emd1b1sip0s.apps.googleusercontent.com'"
      }
    }),
    // new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new CopyWebpackPlugin([
        // {output}/file.txt
        { from: './public/favicon.ico' },
        { from: './public/index.html' },
        { from: './public/robots.txt' },
        { from: './public/sitemap.xml' },
        { from: './public/service_worker.js' },
        { from: './public/landingpage-assets', to: 'landingpage-assets' },
        { from: './public/scripts/main.js', to: 'scripts/main.js' },
        { from: './public/scripts/analytics.js', to: 'scripts/analytics.js' },
        { from: './public/manifest', to: 'manifest' }
    ]),
    new ExtractTextPlugin("/css/style.css"),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compressor: {
          warnings: false
      }
    })
  ],
  module: {
    loaders: [
    // js
    {
      test: /\.js$/,
      // exclude: /node_modules/,
      loaders: ['babel'],
      include: path.join(__dirname, 'client')
    },
    // CSS
    {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract("style-loader", "css-loader")
      // loader: 'style-loader!css-loader!postcss-loader'
    },
    { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/font-woff" },
    { test: /\.(ttf|eot|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=/fonts/[name].[ext]" },
    { test: /\.(jpg|jpg\?\w+|png\?\w+|png)$/, loader: "url-loader?name=/images/[name].[ext]&limit10000&mimetype=image/jpg" },
    ]
  }
};
