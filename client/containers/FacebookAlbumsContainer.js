import { connect } from 'react-redux'
import FacebookAlbums from '../components/FacebookAlbums'

function mapStateToProps(state) {
  return state
}

const FacebookAlbumsContainer = connect(
  mapStateToProps
)(FacebookAlbums)

export default FacebookAlbumsContainer
