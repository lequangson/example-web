import { connect } from 'react-redux'
import q from 'q'
import Contact from '../components/Contact'
import { sentContactRequest, sentContact } from '../actions/Enviroment'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    sendContact: (params, isAnonymous = false) => {
      const defer = q.defer()
      dispatch(sentContactRequest())
      dispatch(sentContact(params, isAnonymous)).then(() => {
        defer.resolve(true)
      }).catch(() => {
        defer.reject(false)
      })
      return defer.promise
    },
  }
}

const ContactContainer = connect(mapStateToProps, mapDispachToProps)(Contact)

export default ContactContainer
