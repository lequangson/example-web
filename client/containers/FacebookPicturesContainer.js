import { connect } from 'react-redux'
import FacebookPictures from '../components/FacebookPictures'

function mapStateToProps(state) {
  return state
}

// function mapDispachToProps(dispatch) {
//   return {
//   }
// }

const FacebookPicturesContainer = connect(mapStateToProps)(FacebookPictures)

export default FacebookPicturesContainer
