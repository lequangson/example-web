import { connect } from 'react-redux';
import MyAnswer from '../components/MyAnswer';
import { getUserQuestions } from '../actions/Match_Questions'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getUserQuestions: (identifier) => {
      dispatch(getUserQuestions(identifier))
    },
  }
}

const MyAnswerContainer = connect(mapStateToProps, mapDispachToProps)(MyAnswer)

export default MyAnswerContainer
