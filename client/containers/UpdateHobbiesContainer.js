import { connect } from 'react-redux'
import UpdateHobbies from '../components/UpdateHobbies'

function mapStateToProps(state) {
  return state
}

// function mapDispachToProps(dispatch) {
//   return {}
// }

const UpdateHobbiesContainer = connect(mapStateToProps)(UpdateHobbies)

export default UpdateHobbiesContainer
