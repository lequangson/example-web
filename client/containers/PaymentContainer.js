import { connect } from 'react-redux';
import q from 'q';
// import Upgrade from '../components/Upgrade'
import Payment from '../components/payments';
import { 
  getPaymentPackages, updatePaymentState, createShoppingCart, 
} from '../actions/Payment';
import { 
  getCurrentUser,
} from '../actions/userProfile'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getPaymentPackages: package_type => {
      dispatch(getPaymentPackages(package_type))
    },
    updatePaymentState: (key, value) => {
      dispatch(updatePaymentState(key, value))
    },
    createShoppingCart: payment_input => {
      const defer = q.defer()
      dispatch(createShoppingCart(payment_input)).then(response => {
        defer.resolve(response.response.data)
      }).catch((err) => {
        defer.reject(err)
      });
      return defer.promise
    }, 
  }
}

const PaymentContainer = connect(
  mapStateToProps,
  mapDispachToProps
)(Payment)

export default PaymentContainer
