import { connect } from 'react-redux';
import SearchInfinite from '../components/SearchInfinite'
import {
  updateViewMode,
  updatePagingInformation,
  getSearchConditionRequest,
  getSearchCondition,
  needScrollToPosition,
  resetData,
  updateSearchCondition
} from '../actions/search'
import { 
  getListBoostInRank, showNextBoostUsers 
} from 'pages/list-boost-in-rank/Action';
import { getRemind, updateRemind, removeBanner, updateScrollPossition } from '../actions/Enviroment'
import { SEARCH_PAGE } from '../constants/Enviroment'
import { updateAskedLocationStatus } from '../actions/userProfile'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    updateViewMode: (mode, enviroment) => {
      dispatch(updateViewMode(mode, enviroment))
    },
    getRemind: () => {
      dispatch(getRemind())
    },
    updateRemind: (identifier) => {
      dispatch(updateRemind(identifier))
    },
    updatePagingInformation: (total_elements, current_page, scroll_position, enviroment, next_enviroment) => {
      dispatch(updatePagingInformation(total_elements, current_page, scroll_position, enviroment, next_enviroment))
    },
    getSearchCondition: () => {
      dispatch(getSearchConditionRequest())
      dispatch(getSearchCondition())
    },
    updateAskedLocationStatus: (status) => {
      dispatch(updateAskedLocationStatus(status))
    },
    needScrollToPosition: (need_scroll_to_position) => {
      dispatch(needScrollToPosition(need_scroll_to_position))
    },
    removeBanner: (banner) => {
      dispatch(removeBanner(banner))
    },
    simpleFilterClick: (params, pageSource) => {    
      dispatch(updateSearchCondition(params)).then(() => {
        dispatch(resetData(pageSource))

        // clear keeping scroll position
        dispatch(updateScrollPossition(SEARCH_PAGE, 0));
        // We need reload top boost users after changing simple filter condition
        dispatch(getListBoostInRank());
      });
    },
    getListBoostInRank: () => dispatch(getListBoostInRank()),
    showNextBoostUsers: (pageSource) => dispatch(showNextBoostUsers(pageSource))
  }
}

const SearchInfiniteContainer = connect(mapStateToProps, mapDispachToProps)(SearchInfinite)

export default SearchInfiniteContainer
