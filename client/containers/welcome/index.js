import React from 'react';
import { connect } from 'react-redux';

import WelcomePageGpContainer from 'containers/welcome/welcome-page-google';
// import WelcomePageFbContainer from 'containers/welcome/welcome-page-face';
import { WELCOME_PAGE_4 } from 'containers/welcome/Constants';

class WelcomePageContainer extends React.Component{

  shouldComponentUpdate(nextProps, nextState){
    const current_welcome_page_completion = this.props.user_profile.current_user.welcome_page_completion;
    const next_welcome_page_completion = nextProps.user_profile.current_user.welcome_page_completion;

    return !(current_welcome_page_completion > 2) &&
                current_welcome_page_completion != next_welcome_page_completion;
  }

  render(){
    const { current_user } = this.props.user_profile;
    const { provider, welcome_page_completion } = current_user;
    return (
      <div>
        { welcome_page_completion < WELCOME_PAGE_4 &&
          <div>
          {/*}  { provider == 'facebook' &&
              <WelcomePageFbContainer />
            }
          */}
            { provider == 'google' &&
              <WelcomePageGpContainer {...this.props}/>
            }
          </div>
        }
      </div>
    )
  }
}

function mapStateToProps(state){
  return state;
}

export default connect(mapStateToProps)(WelcomePageContainer);