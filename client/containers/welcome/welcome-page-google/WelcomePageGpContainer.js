import React  from 'react';
import _ from 'lodash';

import WelcomePageGpModal from 'modals/welcome/WelcomePageGpModal';
import * as ymmStorage from 'utils/ymmStorage';
import { GA_ACTION_WELCOME_PAGE_SKIP, GA_ACTION_UPDATE_NICKNAME, WELCOME_PAGE, MODAL_PROMOTION } from 'constants/Enviroment'
import { API_EXECUTION_ERROR_NETWORK_MSG } from 'constants/TextDefinition';
import { WELCOME_PAGE_1, WELCOME_PAGE_2, WELCOME_PAGE_3, WELCOME_PAGE_4, WELCOME_COMPLETED } from 'containers/welcome/Constants';
import { AVATAR_PICTURE } from 'constants/userProfile';
import UploadImageModal from 'modals/UploadImageModal';
import Modal from 'modals/Modal';

class WelcomePageGpContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openUploadAvatar: false,
      step_id: props.user_profile.current_user.welcome_page_completion + 1
    };
  }

  updateAndNextStep = (params, skipUploadStep) => {
    if(this.state.step_id == WELCOME_PAGE_1){
      this.updateWelcomePage1(params, skipUploadStep);
    } else if(this.state.step_id == WELCOME_PAGE_2){
      this.updateWelcomePage2();
    } else {
      this.updateLastWelcomePage(params);
    }
  }

  openUploadAvatarModal = () => {
    this.setState({openUploadAvatar: true});
  }

  handleUpdateName = (nick_name) => {
    ymmStorage.setItem('name_welcome', nick_name);
  }

  updateGender = (genderParam) => {
    this.props.updateUserProfile(genderParam);
  }

  updateDob = (dob) => {
    this.props.updateDob(dob);
  }

  updateWelcomePage1(params, skipUploadStep) {
    this.props.updateUserProfile(params).then(() => {
      //get dictionaries to retrieve the family-position
      this.props.getDictionaries();
      this.props.gaSend(GA_ACTION_UPDATE_NICKNAME, {page_source: WELCOME_PAGE});
      ymmStorage.removeItem('name_welcome');

      if(!skipUploadStep){
        this.props.updateWelcomePageCompletion(WELCOME_PAGE_1);
        this.setState({step_id: WELCOME_PAGE_2});
      } else{
        this.props.updateWelcomePageCompletion(WELCOME_PAGE_2);
        this.props.gaSend(GA_ACTION_WELCOME_PAGE_SKIP, {page_source: 'Welcome page ' + WELCOME_PAGE_2});
        this.setState({step_id: WELCOME_PAGE_3});
      }
    }).catch(() => {
      that.props.showErrorMessage(API_EXECUTION_ERROR_NETWORK_MSG);
    });
  }

  updateWelcomePage2(){
    this.props.updateWelcomePageCompletion(WELCOME_PAGE_2);
    this.setState({step_id: WELCOME_PAGE_3});
  }

  updateLastWelcomePage(params){
    this.props.updateUserProfile(params).then(() => {
      this.props.updateWelcomePageCompletion(WELCOME_PAGE_4);
      this.props.modalShowNext(MODAL_PROMOTION);
    }).catch(() => {
      that.props.showErrorMessage(API_EXECUTION_ERROR_NETWORK_MSG);
    });
  }

  closeUploadAvatarModal = () => {
    this.setState({openUploadAvatar: false});
  }

  render() {
    const { user_profile, dictionaries } = this.props;
    if(user_profile.current_user.welcome_page_completion == WELCOME_PAGE_3){
      return <div></div>;
    }
    return (
      <div>
        <WelcomePageGpModal
          {...{user_profile}}
          {...{dictionaries}}
          step_id={this.state.step_id}
          openUploadAvatarModal={this.openUploadAvatarModal}
          handleUpdateName={this.handleUpdateName}
          updateAndNextStep={this.updateAndNextStep}
          updateDob={this.updateDob}
          updateGender={this.updateGender}
        />

      {/*Modal to upload avatar*/}
       <Modal
          transitionName="modal"
          isOpen={this.state.openUploadAvatar}
          onRequestClose={this.closeUploadAvatarModal}
          className="modal__content modal__content--3"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
          shouldCloseOnOverlayClick={false}
        >
          <UploadImageModal pictureType={AVATAR_PICTURE} closeModal={this.closeUploadAvatarModal} page_source="WELCOME_MODAL" {...this.props} />
          <button className="modal__btn-close" onClick={this.closeUploadAvatarModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    );
  }
}

export default WelcomePageGpContainer;