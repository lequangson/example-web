import {connect} from 'react-redux';
import q from 'q';

import WelcomePageGpContainer from './WelcomePageGpContainer';
import {
  updateUserProfileRequest, updateUserProfile, updateUserProfileFailure,
  getCurrentUser
} from 'actions/userProfile';
import {
  updateWelcomePageCompletion, updateBirthdayWelcomePage
 } from 'containers/welcome/Actions';
import { gaSend } from 'actions/Enviroment';
import { GA_ACTION_EDIT_PROFILE } from 'constants/Enviroment';
import { getDictionaries } from 'pages/dictionaries/Action';
import { modalShowNext } from 'actions/Enviroment';

function mapStateToProps(state){
  return state;
}

function mapDispatchToProps(dispatch){
  return {
    updateUserProfile: (params) => {
      const defer = q.defer();
      dispatch(gaSend(GA_ACTION_EDIT_PROFILE, {}));
      dispatch(updateUserProfileRequest());
      dispatch(updateUserProfile(params)).then((response) => {
        defer.resolve(true)
      }).catch(function(err) {
        //rollback data if any
        dispatch(updateUserProfileFailure(err));
        defer.reject(false);
      });
      return defer.promise;
    },
    getDictionaries: () => {
      dispatch(getDictionaries());
    },
    gaSend: (eventAction, ga_track_data) => {
      dispatch(gaSend(eventAction, ga_track_data))
    },
    showErrorMessage: (msg) => {
      dispatch(updateUserProfileFailure(msg))
    },
    updateWelcomePageCompletion: async (completionStep) => {
      await dispatch(updateWelcomePageCompletion(completionStep));
      await dispatch(getCurrentUser());
    },
    modalShowNext: (modal) => {
      dispatch(modalShowNext(modal));
    },
    updateDob: (birthday) => {
      dispatch(updateBirthdayWelcomePage(birthday));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomePageGpContainer);