import { CALL_API } from 'middleware/api';
import { API_POST, API_GET } from 'constants/Enviroment';
import {
  UPDATE_WELCOMEPAGE_COMPLETION_REQUEST, UPDATE_WELCOMEPAGE_COMPLETION_SUCCESS,
  UPDATE_WELCOMEPAGE_COMPLETION_FAILURE, UPDATE_BIRTHDAY_WELCOME_PAGE
} from './Constants';

export function updateWelcomePageCompletionRequest() {
  return {
    type: UPDATE_WELCOMEPAGE_COMPLETION_REQUEST,
  }
}

export function updateWelcomePageCompletion(completionStep) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'update_welcome_page_completion',
      params: {
        'welcome_page_completion': completionStep.toString(),
      },
      authenticated: true,
      types: [
        UPDATE_WELCOMEPAGE_COMPLETION_REQUEST,
        UPDATE_WELCOMEPAGE_COMPLETION_SUCCESS,
        UPDATE_WELCOMEPAGE_COMPLETION_FAILURE
      ],
      extentions: completionStep
    },
  }
}

export function updateWelcomePageCompletionFailure(msg = null) {
  return {
    type: UPDATE_WELCOMEPAGE_COMPLETION_FAILURE,
    msg,
  }
}

export function updateBirthdayWelcomePage(birthday) {
  return {
    type: UPDATE_BIRTHDAY_WELCOME_PAGE,
    birthday
  }
}