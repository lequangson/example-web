import { connect } from 'react-redux'
import Verify from '../components/Verify'

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
  }
}

const VerifyContainer = connect(mapStateToProps, mapDispatchToProps)(Verify)

export default VerifyContainer
