import { connect } from 'react-redux'
import BlockForm from '../components/BlockForm'
import {
    getBlockedUsersRequest,
    getBlockedUsers,
} from '../actions/userProfile'
import { showNextUser } from '../actions/WhatHot'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getBlockedUsers: (block_type) => {
      dispatch(getBlockedUsersRequest())
      dispatch(getBlockedUsers(block_type))
    },
    showNextWhatHotUser: () => {
      dispatch(showNextUser())
    },
  }
}
const BlockFormContainer = connect(mapStateToProps, mapDispachToProps)(BlockForm)

export default BlockFormContainer
