import { connect } from 'react-redux'
import Thankyou from '../components/Thankyou'
import { getUserProfile } from '../actions/userProfile'

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    getUserProfileByIdentifier: (identifier) => {
      dispatch(getUserProfile(identifier))
    },
  }
}

const ThankyouContainer = connect(mapStateToProps, mapDispatchToProps)(Thankyou)

export default ThankyouContainer
