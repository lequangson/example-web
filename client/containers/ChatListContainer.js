import { connect } from 'react-redux'
import ChatList from '../components/ChatList'
import q from 'q'
import { updateScrollPossition } from '../actions/Enviroment'
import { userSubscription } from '../actions/GCMNotification'
import { getChatSuggestedUsersRequest, getChatSuggestedUsers, startChat, } from '../actions/userProfile'
import { getLastMessages, ignoreMessage } from '../actions/ChatMessage'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    updateScrollPossition: (page, scroll_top) => {
      dispatch(updateScrollPossition(page, scroll_top))
    },
    getChatSuggestedUsers: () => {
      dispatch(getChatSuggestedUsers());
    },
    getLastMessages: () => {
      dispatch(getLastMessages());
    },
    startChat: () => {
      dispatch(startChat());
    },
    ignoreMessage: async (chat_room_ids) => await dispatch(ignoreMessage(chat_room_ids))
  }
}

const ChatListContainer = connect(
  mapStateToProps,
  mapDispachToProps
)(ChatList)

export default ChatListContainer
