import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import SearchCondition from 'components/SearchCondition';
import { updateSearchCondition,
  getSearchConditionRequest,
  getSearchCondition,
  resetData, resetSelectedConditions
} from 'actions/search';
import { updateScrollPossition } from 'actions/Enviroment';
import { SEARCH_PAGE } from 'constants/Enviroment';

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    searchConditionClick: (params) => {
      // dispatch(updateSearchConditionRequest());
      dispatch(updateSearchCondition(params)).then(() => {
        dispatch(resetData('default'));

        // clear keeping scroll position
        dispatch(updateScrollPossition(SEARCH_PAGE, 0));
        browserHistory.push('/search/default');
      });
    },
    getSearchCondition: () => {
      dispatch(getSearchConditionRequest());
      dispatch(getSearchCondition());
    },
    resetSelectedConditions: () => {
      dispatch(resetSelectedConditions());
    }
  }
}

const SearchConditionContainer = connect(mapStateToProps, mapDispachToProps)(SearchCondition);

export default SearchConditionContainer;
