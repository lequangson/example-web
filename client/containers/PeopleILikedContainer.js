import { connect } from 'react-redux'
import PeopleILiked from '../components/PeopleILiked'
import { getPeopleILiked, getPeopleILikedRequest } from '../actions/PeopleILiked'
import { updateScrollPossition } from '../actions/Enviroment'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getPeopleILiked: () => {
      dispatch(getPeopleILikedRequest())
      dispatch(getPeopleILiked())
    },
    updateScrollPossition: (page, scroll_top) => {
      dispatch(updateScrollPossition(page, scroll_top))
    },
  }
}

const PeopleILikedContainer = connect(
  mapStateToProps,
  mapDispachToProps
)(PeopleILiked)

export default PeopleILikedContainer
