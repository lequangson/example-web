import { connect } from 'react-redux';
import { browserHistory } from 'react-router'
import Main from '../components/Main';
import q from 'q'
import {
    getUserProfileRequest, getUserProfile,
    likeUserIdentifierRequest, likeUserIdentifier,
    updateUserProfileRequest, updateUserProfile, updateUserProfileFailure,
    validateImage, validateImageRequest, validateImageFailure,
    uploadImage, uploadImageRequest, uploadImageFailure,
    deleteUserPicture, deleteUserPictureRequest, deleteUserPictureFailure,
    getFavoriteRequest, getFavorite,
    getFacebookAlbumsRequest, getFacebookAlbums, getFacebookAlbumsFailure,
    getFacebookPicturesRequest, getFacebookPictures, getFacebookPicturesFailure,
    synchFacebookPicturesRequest, synchFacebookPictures, synchFacebookPicturesFailure,
    blockUser, blockUserFailure, unBlockUser, unBlockUserFailure, updateReducerUserProfileState,
    getVisitantRequest, getVisitantFailure, blockNotificationByIdentifier, unBlockNotificationByIdentifier,
    getPeopleMatchedMeRequest, getPeopleMatchedMe, getPeopleMatchedMeFailure,
    clearNotification,
    updateNotificationMessageForMatches, updateLocation,
    getCurrentUser
} from '../actions/userProfile'
import {
  getVisitant
} from 'pages/visitants/Action';
import { getNotificationMessages } from '../actions/ChatMessage';
import {
  updatePreviousPage, updateScrollPossition, sentNotification, gaSend, sendGoogleAnalytic, saveFilesUpload,
  addActivityAuditRequest, addActivityAudit , addActivityAuditFailure, updateElementHeight, setProfileUserNextIndex,
  modalShowNext, saveImage, updateUploadPicturePageSource, updateEnvironmentState
} from '../actions/Enviroment'
import {
  searchUserRequest, searchUserProcess, searchUserFailure, updateReducerSearchState,
  searchNewUserRequest, searchNewUserProcess, searchNewUserFailure,
  searchOnlineUserRequest, searchOnlineUserProcess, searchOnlineUserFailure,
  searchMatchingRatioUserRequest, searchMatchingRatioUserProcess, searchMatchingRatioUserFailure,
  searchNearbyUserRequest, searchNearbyUserProcess, searchNearbyUserFailure,
  searchHarmonyOfAgeProcess, searchHarmonyOfAgeRequest, searchHarmonyOfAgeFailure,
  searchHarmonyOfFateProcess, searchHarmonyOfFateRequest, searchHarmonyOfFateFailure,
  searchHarmonyOfHoroscopeProcess, searchHarmonyOfHoroscopeRequest, searchHarmonyOfHoroscopeFailure,
} from '../actions/search'
import {
  AVATAR_PICTURE, OTHER_PICTURE, UPLOAD_DEVIDE_MODE, VERIFY_PICTURE_TYPE, VERIFY_PICTURE_SECOND_WAY_TYPE
} from '../constants/userProfile'

import {
  GA_ACTION_EDIT_PROFILE, GA_ACTION_UPLOAD_AVATAR_DEVICE, GA_ACTION_UPLOAD_AVATAR_FACEBOOK, GA_ACTION_UPLOAD_OTHER_PICTURES_DEVICE,
  GA_ACTION_UPLOAD_OTHER_PICTURES_FACEBOOK, GA_ACTION_DELETE_OTHER_PICTURE, SEARCH_PAGE, MY_PROFILE_PAGE, NOTIFICATION_INFOR
} from '../constants/Enviroment'
import { compareIdentifiers, getUserId } from '../utils/common';
import notification from 'utils/errors/notification';
import * as ymmStorage from '../utils/ymmStorage';
import { userSubscription } from '../actions/GCMNotification'
// import { delay } from '../utils/common'

function mapStateToProps(state) {
  const { auth } = state
  const { isAuthenticated, errorMessage } = auth
  return {
    isAuthenticated,
    errorMessage
  }
}

//declare common dispatch
function mapDispachToProps(dispatch) {
  return {
    searchUser: (page, page_size=0) => {
      //write current page into session storage
      if(!page_size) {
        ymmStorage.setItem('current_page', page)
      }
      dispatch(searchUserRequest());
      dispatch(searchUserProcess(page, page_size)).then((response) => {});
    },
    searchNewUser: (page, page_size=0) => {
      //write current page into session storage
      if(!page_size) {
        ymmStorage.setItem('current_page', page)
      }
      dispatch(searchNewUserRequest());
      dispatch(searchNewUserProcess(page, page_size)).then((response) => {});
    },
    searchOnlineUser: (page, page_size=0) => {
      //write current page into session storage
      if(!page_size) {
        ymmStorage.setItem('current_page', page)
      }
      dispatch(searchOnlineUserRequest());
      dispatch(searchOnlineUserProcess(page, page_size)).then((response) => {});
    },
    searchNearbyUser: (page, page_size=0) => {
      //write current page into session storage
      if(!page_size) {
        ymmStorage.setItem('current_page', page)
      }
      dispatch(searchNearbyUserRequest());
      dispatch(searchNearbyUserProcess(page, page_size)).then((response) => {});
    },
    searchMatchingRatioUser: (page, page_size=0) => {
      //write current page into session storage
      if(!page_size) {
        ymmStorage.setItem('current_page', page)
      }
      dispatch(searchMatchingRatioUserRequest());
      dispatch(searchMatchingRatioUserProcess(page, page_size)).then((response) => {});
    },
    searchHarmonyOfAge: (page, page_size=0) => {
      //write current page into session storage
      if(!page_size) {
        ymmStorage.setItem('current_page', page)
      }
      dispatch(searchHarmonyOfAgeRequest());
      dispatch(searchHarmonyOfAgeProcess(page, page_size)).then((response) => {});
    },
    searchHarmonyOfFate: (page, page_size=0) => {
      //write current page into session storage
      if(!page_size) {
        ymmStorage.setItem('current_page', page)
      }
      dispatch(searchHarmonyOfFateRequest());
      dispatch(searchHarmonyOfFateProcess(page, page_size)).then((response) => {});
    },
    searchHarmonyOfHoroscope: (page, page_size=0) => {
      //write current page into session storage
      if(!page_size) {
        ymmStorage.setItem('current_page', page)
      }
      dispatch(searchHarmonyOfHoroscopeRequest());
      dispatch(searchHarmonyOfHoroscopeProcess(page, page_size)).then((response) => {});
    },
    getUserProfileByIdentifier: (identifier) => {
      dispatch(getUserProfileRequest())
      dispatch(getUserProfile(identifier)).then((response) => {});
    },
    updatePreviousPage: (page) => {
      dispatch(updatePreviousPage(page));
    },
    updateUploadPicturePageSource: (page) => {
      dispatch(updateUploadPicturePageSource(page));
    },
    updateScrollPossition: (page, scroll_top) => {
      dispatch(updateScrollPossition(page, scroll_top));
    },
    sendNotification: (msg, timeout=5000, type_error) => {
      dispatch(sentNotification(msg, timeout, type_error))
    },
    updateUserProfile: (params) => {
      const defer = q.defer()
      dispatch(gaSend(GA_ACTION_EDIT_PROFILE, {}));
      dispatch(updateUserProfileRequest())
      dispatch(updateUserProfile(params)).then((response) => {
        defer.resolve(true)
      }).catch(function(err) {
        //rollback data if any
        dispatch(updateUserProfileFailure(err))
        defer.reject(false)
      });
      return defer.promise
    },

    uploadImage: (pictureType, imageName, imageSize, imageData, croppingRect, pageSource, ignoreMsg=false) => {
      // Send GA code for uploading image
      let eventAction = GA_ACTION_UPLOAD_AVATAR_DEVICE;// pictureType == AVATAR_PICTURE? GA_ACTION_UPLOAD_AVATAR_DEVICE :
      let needGetCurrentUser = false;
      switch(pictureType){
        case AVATAR_PICTURE:
          eventAction = GA_ACTION_UPLOAD_AVATAR_DEVICE;
          needGetCurrentUser = true;
          break;
        case OTHER_PICTURE:
          eventAction = GA_ACTION_UPLOAD_OTHER_PICTURES_DEVICE;
          needGetCurrentUser = true;
          break;
        default:
          needGetCurrentUser = false;
          break;
      }
      const ga_track_data = {
        page_source: pageSource
      }
      if (pictureType !== VERIFY_PICTURE_TYPE && pictureType !== VERIFY_PICTURE_SECOND_WAY_TYPE) {
        dispatch(gaSend(eventAction, ga_track_data));
      }
      // End send GA code
      // Validate image
      var defer = q.defer()
      dispatch(validateImageRequest(pictureType));
      dispatch(validateImage(pictureType, imageName, imageSize, ignoreMsg)).then((response) => {
        // Image is valid. We would upload image
        if (response.response.data.session_id){
          dispatch(uploadImageRequest(pictureType)); //this is redundant, validateImageRequest did the same work
          dispatch(uploadImage(response.response.data.session_id, imageData, pictureType, croppingRect)).then((response) => {
            if (needGetCurrentUser) {
              // We need to refresh current user for getting new profile's completion.
              dispatch(getCurrentUser())
            }
            defer.resolve(response)
          }).catch(function(err) {
            dispatch(uploadImageFailure(pictureType));
            defer.reject(err)
          });
        }
      }).catch(function(err) {
        //dispatch(validateImageFailure(pictureType));
        defer.reject(err)
      });
      return defer.promise
    },
    validateImage: (pictureType, imageName, imageSize, ignoreMsg=false) => {
      dispatch(validateImageRequest(pictureType));
      var defer = q.defer()
      dispatch(validateImage(pictureType, imageName, imageSize, ignoreMsg)).then((response) => {
        defer.resolve(response)
      }).catch(function(err) {
        //dispatch(validateImageFailure(pictureType));
        defer.reject(err)
      });
      return defer.promise
    },
    deleteUserPicture: (pictureType, id) => {
      var defer = q.defer()
      dispatch(deleteUserPicture(pictureType, id))
        .then(response => {
          // Send GA code for deleting image
          dispatch(gaSend(GA_ACTION_DELETE_OTHER_PICTURE, {}));
          dispatch(getCurrentUser());
          // End send GA code
          defer.resolve(response)
        })
        .catch(err => {
          dispatch(deleteUserPictureFailure())
          defer.reject(err)
        })
      return defer.promise
    },
    getFacebookAlbums: () => {
      dispatch(getFacebookAlbumsRequest())
      dispatch(getFacebookAlbums()).then((response) => {/**do something after successfully**/});
    },
    getFacebookPictures: (album_id) => {
      dispatch(getFacebookPicturesRequest())
      dispatch(getFacebookPictures(album_id)).then((response) => {/**do something after successfully**/});
    },
    synchFacebookPictures: (picture_urls, pictureType, croppingRect, pageSource) => {
      // Send GA code for uploading image
      let eventAction = GA_ACTION_UPLOAD_AVATAR_FACEBOOK;// pictureType == AVATAR_PICTURE? GA_ACTION_UPLOAD_AVATAR_DEVICE :
      switch(Number(pictureType)){
        case AVATAR_PICTURE:
          eventAction = GA_ACTION_UPLOAD_AVATAR_FACEBOOK;
          break;
        case OTHER_PICTURE:
          eventAction = GA_ACTION_UPLOAD_OTHER_PICTURES_FACEBOOK;
          break;
        default:
          break;
      }
      const ga_track_data = {
        page_source: pageSource
      }
      dispatch(gaSend(eventAction, ga_track_data));
      // End send GA code
      var defer = q.defer()
      dispatch(synchFacebookPicturesRequest(pictureType))
      dispatch(synchFacebookPictures(picture_urls, pictureType, croppingRect)).then((response) => {
        dispatch(getCurrentUser())
        defer.resolve(response)
      })
      .catch(function(err) {
        dispatch(synchFacebookPicturesFailure(pictureType));
        defer.reject(err)
      });
      return defer.promise;
    },
    gaSend: (eventAction, ga_track_data) =>{
      dispatch(gaSend(eventAction, ga_track_data))
    },
    sendGoogleAnalytic: (eventAction, ga_track_data) =>{
      dispatch(sendGoogleAnalytic(eventAction, ga_track_data))
    },
    getFavorite: () => {
      dispatch(getFavoriteRequest())
      dispatch(getFavorite())
    },
    saveFilesUpload: (files, pictureType = OTHER_PICTURE, uploadType = UPLOAD_DEVIDE_MODE) => {
      dispatch(saveFilesUpload(files, uploadType))
      browserHistory.push(`/upload-image/${pictureType}/${uploadType}`)
    },
    saveImage: (file, atWelcome = false) => {
      dispatch(saveImage(file))
      if (atWelcome) {
        browserHistory.push('/change-avatar/welcome/preview')
      } else {
        browserHistory.push('/change-avatar/upload/preview')
      }
    },
    addActivityAudit: (activityCode, toUserIdentifier, originalValue, newValue) => {
      dispatch(addActivityAuditRequest())
      dispatch(addActivityAudit(activityCode, toUserIdentifier, originalValue, newValue)).then((response) => {})
    },
    blockUser: (identifier, block_type) => {
      var defer = q.defer()
      dispatch(blockUser(identifier, block_type)).then((response) => {
        defer.resolve(response)
      }).catch(function(err) {
        defer.reject(err)
        dispatch(blockUserFailure())
      })
      return defer.promise
    },
    unBlockUser: (identifier, block_type) => {
      var defer = q.defer()
      dispatch(unBlockUser(identifier, block_type)).then((response) => {
        defer.resolve(response)
      }).catch(function(err) {
        defer.reject(err)
        dispatch(unBlockUserFailure())
      })
      return defer.promise
    },
    updateReducerState: (page, identifier, key, value) => {
      if(page == SEARCH_PAGE) {
        dispatch(updateReducerSearchState(identifier, key, value))
      } else {
        dispatch(updateReducerUserProfileState(page, identifier, key, value))
      }
    },
    // LuanNM
    getVisitant: (pageIndex) => {
      var defer = q.defer()
      dispatch(getVisitant(pageIndex)).then((response) => {
        defer.resolve(response)
      }).catch(function(err) {
        defer.reject(err)
      })
      return defer.promise
    },
    blockNotificationByIdentifier: (identifier) => {
      dispatch(blockNotificationByIdentifier(identifier))
    },
    unBlockNotificationByIdentifier: (identifier) => {
      dispatch(unBlockNotificationByIdentifier(identifier))
    },
    getPeopleMatchedMe: () => {
      var defer = q.defer()
      dispatch(getPeopleMatchedMe()).then((matched_users) => {
        defer.resolve(matched_users)
        dispatch(getNotificationMessages()).then((res) => {
          // update notification message number for chat
          if (res.payload.data.data && res.payload.data.data.length) {
            if (!ymmStorage.getItem('read_chat_notification')) {
              let total_unread_messages = 0;
              const customer_support_user_id = res.payload.data.data.filter((item) => getUserId(item.user_identifier) == process.env.CUSTOMER_SUPPORT_USER_ID);
              if (customer_support_user_id.length) {
                total_unread_messages = 1;
              }
              matched_users.response.data.map((user) => {
                const new_message_number = res.payload.data.data.filter(
                    item => compareIdentifiers(item.user_identifier, user.identifier)
                )
                .map(item2 => item2.number_new_message).reduce((a, b) => a + b, 0);
                total_unread_messages += new_message_number;
                return user;
              })
              if (total_unread_messages) {
                notification(NOTIFICATION_INFOR, `Bạn có ${total_unread_messages} tin nhắn mới.`);
              }
              ymmStorage.setItem('read_chat_notification', true);
            }

            dispatch(updateNotificationMessageForMatches(res.payload.data.data))
          }
        })
      }).catch(function(err) {
        defer.reject(err)
        dispatch(getPeopleMatchedMeFailure())
      })
      return defer.promise
    },
    clearNotification: (type) => {
        var defer  = q.defer()
        dispatch(clearNotification(type)).then(function(response){
          defer.resolve(response)
        }).catch(function(err){
          defer.reject(err)
        })
        return defer.promise
    },
    setProfileUserNextIndex: (next_index) => {
      dispatch(setProfileUserNextIndex(next_index))
    },
    updateElementHeight: (view_mode, key, value) => {
      dispatch(updateElementHeight(view_mode, key, value))
    },
    modalShowNext: (modalName) => {
      dispatch(modalShowNext(modalName))
    },
    updateLocation: (long, lat) => {
      var defer = q.defer()
      dispatch(updateLocation(long, lat)).then(function(response){
        defer.resolve(response)
      }).catch(function(err){
        defer.reject(err)
      })
      return defer.promise
    },
    updateEnvironmentState: (key, value) => {
      dispatch(updateEnvironmentState(key, value))
    },
    userSubscription: (endpoint) => {
      dispatch(userSubscription(endpoint))
    }
  }
}

const App = connect(mapStateToProps, mapDispachToProps)(Main);

export default App;
