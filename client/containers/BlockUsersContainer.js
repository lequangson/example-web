import { connect } from 'react-redux';
import BlockUsers from '../components/BlockUsers';
import { getBlockedUsers } from '../actions/userProfile'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getBlockedUsers: (block_type) => {
      dispatch(getBlockedUsers(block_type))
    },
  }
}

const BlockUsersContainer = connect(mapStateToProps, mapDispachToProps)(BlockUsers)

export default BlockUsersContainer
