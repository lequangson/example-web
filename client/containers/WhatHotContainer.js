import { connect } from 'react-redux'
import WhatHot from '../components/WhatHot'
import q from 'q'
import {
  resetData,
  updateSearchCondition,
  getSearchCondition,
} from '../actions/search';
import { getListBoostInRank } from 'pages/list-boost-in-rank/Action';
import { getWhatHot, showNextUser, hideAdProgram, } from '../actions/WhatHot'
import { ignoreUser } from '../actions/userProfile'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getWhatHot: (pageIndex) => {
      const defer = q.defer()
      dispatch(getWhatHot(pageIndex)).then(res => {
        defer.resolve(res)
      }).catch(err => {
        defer.reject(err)
      })
      return defer.promise
    },
    showNextUser: () => {
      dispatch(showNextUser())
    },
    ignoreUser: (id, page_source) => {
      dispatch(ignoreUser(id, page_source))
    },
    hideAdProgram: () => {
      dispatch(hideAdProgram())
    },
    simpleFilterClick: (params) => {
      dispatch(updateSearchCondition(params)).then(() => {
        dispatch(resetData())
        // We need reload top boost users after changing simple filter condition
        dispatch(getListBoostInRank());
      });
    },
    getListBoostInRank: () => dispatch(getListBoostInRank()),
    getSearchCondition: () => {
      dispatch(getSearchCondition())
    },
  }
}

const WhatHotContainer = connect(
  mapStateToProps,
  mapDispachToProps
)(WhatHot)

export default WhatHotContainer
