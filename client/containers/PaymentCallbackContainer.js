import { connect } from 'react-redux'
import q from 'q';
import PaymentCallback from '../components/payments/PaymentCallback'
import { paymentCallback } from '../actions/Payment'
import { getCurrentUser, } from '../actions/userProfile'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    paymentCallback: (data, signature) => {
      const defer = q.defer()
      dispatch(paymentCallback(data, signature)).then(response => {
        dispatch(getCurrentUser());
        defer.resolve(response)
      }).catch((err) => {
        defer.reject(err)
      });
      return defer.promise
    },
  }
}

const PaymentCallbackContainer = connect(
  mapStateToProps,
  mapDispachToProps
)(PaymentCallback)

export default PaymentCallbackContainer
