import { connect } from 'react-redux'
import Promotion from '../components/Promotion'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
  }
}

const PromotionContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(Promotion)

export default PromotionContainer
