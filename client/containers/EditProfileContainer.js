import { connect } from 'react-redux'
import EditProfile from '../components/EditProfile'
import { getSelfIntroduction } from '../actions/userProfile'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getSelfIntroduction: () => {
      dispatch(getSelfIntroduction())
    },
  }
}
const EditProfileContainer = connect(mapStateToProps, mapDispachToProps)(EditProfile)

export default EditProfileContainer
