import { connect } from 'react-redux'
import EducationUpdate from '../components/EducationUpdate'
import { updateUserProfileFailure } from '../actions/userProfile'


function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    showErrorMessage: (msg) => {
      dispatch(updateUserProfileFailure(msg))
    },
  }
}

const EducationUpdateContainer = connect(mapStateToProps, mapDispachToProps)(EducationUpdate)

export default EducationUpdateContainer
