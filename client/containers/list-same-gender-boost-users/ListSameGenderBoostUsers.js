import React, { Component } from 'react';
import PropTypes from 'prop-types';
import shuffle from 'lodash/shuffle';
import isEmpty from 'lodash/isEmpty';
import showDefaultAvatar from 'src/scripts/showDefaultAvatar';
import PollingImg from 'components/commons/PollingImg';
import { isUserInBoostInRankPeriod } from 'utils/BoostInRankService';

class ListSameGenderBoostUsers extends Component {
  static propTypes = {
    list_same_gender_boost_users: PropTypes.object.isRequired,
    current_user: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    getSameGenderBoostUsers: PropTypes.func.isRequired
  }

  componentWillMount() {
    if (!this.props.list_same_gender_boost_users.sameGenderBoostUsers.length) {
      this.props.getSameGenderBoostUsers();
    }
  }

  shouldComponentUpdate(nextProps) {
    if (isEmpty(this.props.current_user)) {
      return true;
    } else if (!isEmpty(nextProps.current_user) &&
                    this.getAvatarUrl(this.props.current_user) != this.getAvatarUrl(nextProps.current_user)) {
      return true;
    }
    if (this.props.list_same_gender_boost_users.sameGenderBoostUsers.length != nextProps.list_same_gender_boost_users.sameGenderBoostUsers.length) {
      return true;
    }
     if (this.props.location.pathname != nextProps.location.pathname) {
      return true;
     }
    return false;
  }

  getAvatarUrl(current_user) {
    const gender = !isEmpty(current_user) ? current_user.gender : '';
    return (!isEmpty(current_user) &&
      current_user.user_pictures &&
      current_user.user_pictures.length > 0) ?
      current_user.user_pictures[0].thumb_image_url :
      showDefaultAvatar(gender, 'thumb');
  }

  renderMyAvatarWithIcon() {
    const { current_user } = this.props;
    const userPicture = this.getAvatarUrl(current_user);

    return (
      <div className="l-inline-block">
        <div className="category__item no-hover category__item--first txt-center">
          <div className="category__image category__image--icon small pos-relative">
            <div className="card__top-right--1 fill">
              <i className="fa fa-rocket icon-small"></i>
            </div>
          </div>
          <div className="category__text txt-blue">
            Đang<br/> nổi bật
          </div>
        </div>
        <div className="category__item no-hover">
          <div className="category__image pos-relative txt-center">
            <PollingImg
                src={userPicture}
                alt={'My Avatar'}
                className="rounded"
              />
          </div>
        </div>
      </div>
    )
  }

  renderSameGenderBoostUsers() {
    const { sameGenderBoostUsers } = this.props.list_same_gender_boost_users;
    const shuffledSameGenderBoostUsers = shuffle(sameGenderBoostUsers).slice(0, 3);

    return (
      <div className="l-inline-block">
         {
          shuffledSameGenderBoostUsers.map((user, index) => {
            return (
              <div key={index} className="category__item no-hover">
                <div className="category__image pos-relative  txt-center">
                  <img src={user.url} className="rounded" alt="Same Gender Boost User" />
                </div>
              </div>
              );
          })
        }
      </div>
    )
  }

  render() {
    const { current_user } = this.props;
    const isOnTop_24h = isUserInBoostInRankPeriod(current_user);

    return !isOnTop_24h ? null :
      <div className="category__same-boot category padding-t5">
        { this.renderMyAvatarWithIcon() }
        { this.renderSameGenderBoostUsers() }
      </div>
    }
}

export default ListSameGenderBoostUsers;
