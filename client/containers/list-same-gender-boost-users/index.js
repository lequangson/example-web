import { connect } from 'react-redux';
import ListSameGenderBoostUsers from './ListSameGenderBoostUsers';
import { getSameGenderBoostUsers } from './Actions';

function mapStateToProps(state) {
  return {
    current_user: state.user_profile.current_user,
    list_same_gender_boost_users: state.list_same_gender_boost_users
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getSameGenderBoostUsers: () => {
      dispatch(getSameGenderBoostUsers());
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListSameGenderBoostUsers);