import {
  GET_SAME_GENDER_BOOST_USERS_REQUEST,
  GET_SAME_GENDER_BOOST_USERS_SUCCESS,
  GET_SAME_GENDER_BOOST_USERS_FAILURE
} from './Constants';

const defaultState = {
  isFetching: false,
  sameGenderBoostUsers: []
};

export default function listSameGenderBoostUsers(state = defaultState, action) {
  switch (action.type) {
    case GET_SAME_GENDER_BOOST_USERS_REQUEST:
      return {
        ...state,
        isFetching: true
      }
    case GET_SAME_GENDER_BOOST_USERS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        sameGenderBoostUsers: action.response.data
      }
    case GET_SAME_GENDER_BOOST_USERS_FAILURE:
      return {
        ...state,
        isFetching: false
      }

    default:
      return state;
  }
}