import { CALL_API } from 'middleware/api';
import API_GET from 'constants/Enviroment';
import {
  GET_SAME_GENDER_BOOST_USERS_REQUEST,
  GET_SAME_GENDER_BOOST_USERS_SUCCESS,
  GET_SAME_GENDER_BOOST_USERS_FAILURE
} from './Constants';

export function getSameGenderBoostUsers() {
  return {
    [CALL_API]: {
      endpoint: 'current_boost_users',
      method: 'get',
      params: {},
      authenticated: true,
      types: [
        GET_SAME_GENDER_BOOST_USERS_REQUEST,
        GET_SAME_GENDER_BOOST_USERS_SUCCESS,
        GET_SAME_GENDER_BOOST_USERS_FAILURE
      ]
    },
  };
}
