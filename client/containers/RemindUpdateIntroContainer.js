import { connect } from 'react-redux'
import RemindUpdateIntro from '../components/RemindUpdateIntro'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
  }
}

const RemindUpdateIntroContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(RemindUpdateIntro)

export default RemindUpdateIntroContainer
