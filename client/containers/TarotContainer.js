import { connect } from 'react-redux';
import Tarot from '../components/Tarot';
import { updateTarotPage, updateContentPage3 } from '../actions/Tarot'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    updateTarotPage: (page) => {
      dispatch(updateTarotPage(page))
    },
    updateContentPage3: (url, text2) => {
      dispatch(updateContentPage3(url, text2))
    },
  }
}

const TarotContainer = connect(mapStateToProps, mapDispachToProps)(Tarot)

export default TarotContainer
