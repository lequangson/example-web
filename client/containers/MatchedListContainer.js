import { connect } from 'react-redux'
import MatchedList from '../components/MatchedList'
import { updateScrollPossition } from '../actions/Enviroment'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    updateScrollPossition: (page, scroll_top) => {
      dispatch(updateScrollPossition(page, scroll_top))
    },
  }
}

const MatchedListContainer = connect(
  mapStateToProps,
  mapDispachToProps
)(MatchedList)

export default MatchedListContainer
