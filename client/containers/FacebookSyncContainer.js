import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import q from 'q'
import FacebookSync from '../components/FacebookSync'
import { facebookSynchronizeRequest, facebookSynchronize,
  getLastFacebookSynchronize,
} from '../actions/Enviroment'
import { getCurrentUser } from '../actions/userProfile'
import { gaSend } from '../actions/Enviroment'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    facebookSynchronize: () => {
      const defer = q.defer()
      dispatch(facebookSynchronizeRequest())
      dispatch(facebookSynchronize()).then(() => {
        dispatch(getCurrentUser()).then(() => {
          defer.resolve(true)
        })
      }).catch(() => {
        defer.reject(false)
      })
      return defer.promise
    },

    getLastFacebookSynchronize: () => {
      const defer = q.defer()
      dispatch(facebookSynchronizeRequest())
      dispatch(getLastFacebookSynchronize()).then(() => {
        defer.resolve(true)
      }).catch(() => {
        defer.reject(false)
      })
      return defer.promise
    },

    gaSend: (eventAction, ga_track_data) => {
      dispatch(gaSend(eventAction, ga_track_data))
    },
  }
}


const FacebookSyncContainer = connect(mapStateToProps, mapDispachToProps)(FacebookSync)

export default FacebookSyncContainer
