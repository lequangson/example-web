import { connect } from 'react-redux';
import Profile from 'components/Profile';
import {
  addFavorite,
  deleteFavorite,
  getBlockedUsers,
} from 'actions/userProfile';
import { setViewProfileSource } from 'actions/Enviroment';
import { updateSearchUserProfile, needScrollToPosition } from 'actions/search';

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    addFavorite: id => (
      dispatch(addFavorite(id))
    ),

    deleteFavorite: id => (
      dispatch(deleteFavorite(id))
    ),

    updateSearchUserProfile: (id, key, val) => (
      dispatch(updateSearchUserProfile(id, key, val))
    ),

    getBlockedUsers: (block_type) => {
      dispatch(getBlockedUsers(block_type));
    },

    setViewProfileSource: (viewSource) => {
      dispatch(setViewProfileSource(viewSource));
    },

    needScrollToPosition: (need_scroll_to_position) => {
      dispatch(needScrollToPosition(need_scroll_to_position));
    },
  }
}
const ProfileContainer = connect(mapStateToProps, mapDispachToProps)(Profile);

export default ProfileContainer;
