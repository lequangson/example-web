import { connect } from 'react-redux'
import UpdateSelfIntro from '../components/UpdateSelfIntro'
import { getSelfIntroduction } from '../actions/userProfile'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getSelfIntroduction: () => {
      dispatch(getSelfIntroduction())
    },
  }
}
const UpdateSelfIntroContainer = connect(mapStateToProps, mapDispachToProps)(UpdateSelfIntro)

export default UpdateSelfIntroContainer
