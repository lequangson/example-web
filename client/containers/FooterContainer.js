import { connect } from 'react-redux'
import Footer from '../components/footers/Footer'

function mapStateToProps(state) {
  return state
}

// function mapDispatchToProps() {
//   return {}
// }

export default connect(mapStateToProps)(Footer)
