import { connect } from 'react-redux'
import WhoAddMeToFavourite from '../components/WhoAddMeToFavourite'
import { updateScrollPossition } from 'actions/Enviroment'
import { getWhoAddMeToFavourire } from 'actions/userProfile';

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    updateScrollPossition: (page, scroll_top) => {
      dispatch(updateScrollPossition(page, scroll_top))
    },
    getWhoAddMeToFavourire: () => {
      dispatch(getWhoAddMeToFavourire())
    },
  }
}

const WhoAddMeToFavouriteContainer = connect(
  mapStateToProps,
  mapDispachToProps
)(WhoAddMeToFavourite)

export default WhoAddMeToFavouriteContainer
