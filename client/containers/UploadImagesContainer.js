import { connect } from 'react-redux'
import UploadImages from '../components/UploadImages'
import { clearFilesUpload } from '../actions/Enviroment'
import { updateVerifyImageStatus, updagteVerificationStatus } from '../actions/userProfile';

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    clearFilesUpload: (uploadType) => {
      dispatch(clearFilesUpload(uploadType))
    },
    updateVerifyImageStatus: (status = false) => {
      dispatch(updateVerifyImageStatus(status));
    },
    updagteVerificationStatus: (field_name) => {
      dispatch(updagteVerificationStatus(field_name))
    }
  }
}
const UploadImagesContainer = connect(mapStateToProps, mapDispachToProps)(UploadImages)

export default UploadImagesContainer
