import { connect } from 'react-redux'
import ChangeAvatar from '../components/ChangeAvatar'
import q from 'q'
import {
  getCurrentUser, setAvatarRequest, setAvatar, setAvatarFailure,
} from '../actions/userProfile';
import { updateWelcomePageCompletion } from 'containers/welcome/Actions';

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getCurrentUser: () => {
      dispatch(getCurrentUser())
    },

    setAvatar: (id, croppingRect) => {
      var defer = q.defer()
      dispatch(setAvatarRequest())
      dispatch(setAvatar(id, croppingRect)).then((response) => {
        defer.resolve(response)
      }).catch(function(err) {
        defer.reject(err)
        dispatch(setAvatarFailure(err))
      })
      return defer.promise
    },

    updateWelcomePageCompletion: (pageNo) => {
      dispatch(updateWelcomePageCompletion(pageNo))
    },

  }
}

const ChangeAvatarContainer = connect(
  mapStateToProps,
  mapDispachToProps
)(ChangeAvatar)

export default ChangeAvatarContainer
