import { connect } from 'react-redux'
import FavoriteLists from '../components/FavoriteLists'
import { updateScrollPossition } from '../actions/Enviroment'

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    updateScrollPossition: (page, scroll_top) => {
      dispatch(updateScrollPossition(page, scroll_top))
    },
  }
}

const FavoriteListsContainer = connect(
  mapStateToProps,
  mapDispachToProps
)(FavoriteLists)

export default FavoriteListsContainer
