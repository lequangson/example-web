import React from 'react'
import { Route, IndexRoute, Redirect, browserHistory } from 'react-router'
import '../public/styles/main.css'
import '../public/styles/desktop.css'
import '../public/styles/tempStyle.css'
import '../public/scripts/mobile-detect.js'
import App from './pages/App'
import HomeIndex from './pages/HomeIndex'
import SearchInfinite from './pages/SearchInfinite'
import TarotPage from './pages/TarotPage'
import MyAnswerPage from './pages/MyAnswerPage'
import SearchCondition from './pages/SearchCondition'
import PeopleLikedMePage from './pages/PeopleLikedMePage'
import Profile from './pages/Profile'
import EditProfile from './pages/EditProfile'
import UpdateHobbies from './pages/UpdateHobbies'
import UpdateSelfIntro from './pages/UpdateSelfIntro'
import Logout from './components/Logout'
import EducationUpdate from './pages/EducationUpdate'
import UploadImages from './pages/UploadImages'
import FacebookPictures from './pages/FacebookPictures'
import FavoriteLists from './pages/FavoriteLists'
import FacebookAlbums from './pages/FacebookAlbums'
import BlockUsers from './pages/BlockUsers'
import BlockForm from './pages/BlockForm'
import ExpendList from './pages/ExpendList'
import Setting from './pages/Setting'
import DeleteAccount from './pages/delete-account/index'
import Thankyou from './pages/Thankyou'
import FileNotFound from './pages/FileNotFound'
import Report from './pages/report/index'
import PeopleILiked from './pages/PeopleILiked'
import FacebookSync from './pages/FacebookSync'
import TermsOfUse from './pages/static-pages/TermsOfUse'
import Help from './pages/static-pages/Help'
import Privacy from './pages/static-pages/Privacy'
import About from './pages/static-pages/About'
import Contact from './pages/Contact'
import SuspendedAccount from './pages/SuspendedAccount'
import MatchedList from './pages/MatchedList'
import BasicInfoUpdate from './pages/BasicInfoUpdate'
import ChangeAvatar from './pages/ChangeAvatar'
import Upgrade from './pages/Upgrade'
import Instruction from './pages/geolocations/Instruction';
import PaymentThankYou from './components/payments/ThankYou';
import PaymentFailure from './components/payments/Failure';
import PayCoinInstruction from './pages/PayCoinInstruction';
import PaymentInstruction from './components/payments/Instruction';
import Payment from './components/new-payments/index';
import PaymentCallback from './pages/PaymentCallback'
import WomenDayGuide from './pages/campaign/WomenDayGuide';
import WhoAddMeToFavourite from './pages/WhoAddMeToFavourite'
import WhatHot from './pages/WhatHot'
import ChatList from './pages/ChatList'
import Referral from './pages/Referral'
import NotificationInstruction from './pages/static-pages/NotificationInstruction'
import SimilarSharing from './pages/similar-face/sharing-page/index';
import ListSimilarFaceToMe from './pages/list-similar-to-me/index';
import ListSimilarFaceToMyFbFriends from './pages/list-similar-to-my-fb-friends/index';
import ListSimilarFaceToMyYmmFriends from './pages/list-similar-to-my-ymm-friends/index';
import ListSimilarFaceToMyPhotos from './pages/list-similar-to-my-photos/index';
import MyPresentList from './pages/campaign/MyPresentList'
import Verify from 'pages/verification';
import {
    IdCardVerification,
    SelfieVerification,
    PhoneVerification,
    JobVerification,
    EducationVerification
} from 'pages/verification';
import QuizVui, { QuizQuestion, QuizAnswer } from 'pages/quiz-vui';
import LuckySpinner from './pages/lucky-spinner/index';
import SuperLikeSpinner from './pages/super-like-spinner/index';

import ChatListX from 'chat/containers/ChatListContainer';
import Chat from 'chat/containers/ChatContainer';
import ListVisitants from 'pages/visitants';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomeIndex} />
    <Route path="/landingpage/:mode" component={HomeIndex} />
    <Route path="/search/:mode(/:id)" component={SearchInfinite} />
    <Route path="/search-condition" component={SearchCondition} />
    <Route path="/myprofile" component={EditProfile} />
    <Route path="/myprofile/update-hobbies" component={UpdateHobbies} />
    <Route path="/myprofile/update-self-intro" component={UpdateSelfIntro} />
    <Route path="/myprofile/update-basic-info" component={BasicInfoUpdate} />
    <Route path="/profile/:identifier" component={Profile} />
    <Route path="/logout" component={Logout} />
    <Route path="/people-like-me" component={PeopleLikedMePage} />
    <Route path="/update/:page" component={EducationUpdate} />
    <Route path="/upload-image(/:pictureType/:uploadType)" component={UploadImages} />
    <Route path="/favorite-lists" component={FavoriteLists} />
    <Route path="/fb-album/:pictureType" component={FacebookAlbums} />
    <Route path="/fb-pictures/:pictureType/:albumId" component={FacebookPictures} />
    <Route path="/visitants" component={ListVisitants} />
    <Route path="/blocked-users" component={BlockUsers} />
    <Route path="/muted-users" component={BlockUsers} />
    <Route path="/expend-list" component={ExpendList} />
    <Route path="/block-form/:identifier" component={BlockForm} />
    <Route path="/setting" component={Setting} />
    <Route path="/delete-account" component={DeleteAccount} />
    <Route path="/thankyou" component={Thankyou} />
    <Route path="/report/:identifier" component={Report} />
    <Route path="/people-i-liked" component={PeopleILiked} />
    <Route path="/facebook-sync" component={FacebookSync} />
    <Route path="/terms-of-use" component={TermsOfUse} />
    <Route path="/helps(/:tab/:question)" component={Help} />
    <Route path="/privacy" component={Privacy} />
    <Route path="/about" component={About} />
    <Route path="/contact" component={Contact} />
    <Route path="/suspend-account" component={SuspendedAccount} />
    <Route path="/matched-list" component={MatchedList} />
    <Route path="/change-avatar(/:mode/:id)" component={ChangeAvatar} />
    <Route path="/geo-instruction" component={Instruction} />
    <Route path="/payment/thankyou" component={PaymentThankYou} />
    <Route path="/my-answer" component={MyAnswerPage} />
    <Route path="/tarot-page(/:option(/:card_id))" component={TarotPage} />
    <Route path="/payment/callback" component={PaymentCallback} />
    <Route path="/payment/failure" component={PaymentFailure} />
    <Route path="/payment/upgrade" component={Payment} />
    <Route path="/payment(/:type(/:method))" component={Upgrade} />
    <Route path="/events" component={WomenDayGuide} />
    <Route path="/coin-instruction" component={PayCoinInstruction} />
    <Route path="/upgrade-instruction" component={PaymentInstruction} />
    <Route path="/who-add-favourite" component={WhoAddMeToFavourite} />
    <Route path='/whathot' component={WhatHot} />
    <Route path="/chat" component={ChatList} />
    <Route path="/referral-page" component={Referral} />
    <Route path="/noti-instruction" component={NotificationInstruction} />
    <Route path="/similar-sharing/:current_user_id/:mode/:id(/:similar_img)" component={SimilarSharing} />
    <Route path='/similar-face-to-me' component={ListSimilarFaceToMe} />
    <Route path='/similar-face-to-my-fb-friends(/:picture_id)' component={ListSimilarFaceToMyFbFriends} />
    <Route path='/similar-face-to-my-photos(/:picture_id)' component={ListSimilarFaceToMyPhotos} />
    <Route path='/similar-face-to-my-ymm-friends/:user_id' component={ListSimilarFaceToMyYmmFriends} />
    <Route path='/my-present-list' component={MyPresentList} />
    <Route path='/verification' component={Verify} />
    <Route path="/verification-id-card" component={IdCardVerification} />
    <Route path="/verification-selfie" component={SelfieVerification} />
    <Route path="/verification-phone" component={PhoneVerification} />
    <Route path="/verification-job" component={JobVerification} />
    <Route path="/verification-education" component={EducationVerification} />
    <Route path="/quizvui/question/:question_id(/:share_img)" component={QuizQuestion} />
    <Route path="/quizvui/answer/:question_id/:answer_id/:answer_id_for_male/:answer_id_for_female" component={QuizAnswer} />
    <Route path="/quizvui(/:page)" component={QuizVui} />
    <Route path="/lucky-spinner" component={LuckySpinner} />
    <Route path="/super-like-spinner" component={SuperLikeSpinner} />
    <Route path="/chat-list(/:type)" component={ChatListX} />
    <Route path="chat/:id" component={Chat} />
    {/* It should be the last route item */}
    <Route path="/404" component={FileNotFound} />
    <Redirect from="*" to="/404" />
  </Route>
)
