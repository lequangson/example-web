export const ANSWER_MY_EXPLORER_QUESTION = 'ANSWER_MY_EXPLORER_QUESTION';
export const ANSWER_OTHER_EXPLORER_QUESTION = 'ANSWER_OTHER_EXPLORER_QUESTION';

export const MATCH_QUESTION_UPDATE_STATE = 'MATCH_QUESTION_UPDATE_STATE';
export const GET_USER_QUESTIONS_REQUEST = 'GET_USER_QUESTIONS_REQUEST'
export const GET_USER_QUESTIONS_SUCCESS = 'GET_USER_QUESTIONS_SUCCESS'
export const GET_USER_QUESTIONS_FAILURE = 'GET_USER_QUESTIONS_FAILURE'

export const ANSWER_QUESTION_REQUEST = 'ANSWER_QUESTION_REQUEST'
export const ANSWER_QUESTION_SUCCESS = 'ANSWER_QUESTION_SUCCESS'
export const ANSWER_QUESTION_FAILURE = 'ANSWER_QUESTION_FAILURE'

export const CHOOSE_EXPLORER_QUESTION_REQUEST = 'CHOOSE_EXPLORER_QUESTION_REQUEST'
export const CHOOSE_EXPLORER_QUESTION_SUCCESS = 'CHOOSE_EXPLORER_QUESTION_SUCCESS'
export const CHOOSE_EXPLORER_QUESTION_FAILURE = 'CHOOSE_EXPLORER_QUESTION_FAILURE'

export const GET_MATCHED_ANSWERED_QUESTION_REQUEST = 'GET_MATCHED_ANSWERED_QUESTION_REQUEST'
export const GET_MATCHED_ANSWERED_QUESTION_SUCCESS = 'GET_MATCHED_ANSWERED_QUESTION_SUCCESS'
export const GET_MATCHED_ANSWERED_QUESTION_FAILURE = 'GET_MATCHED_ANSWERED_QUESTION_FAILURE'
