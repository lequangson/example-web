export const PAYMENT_GET_PACKAGE_REQUEST = 'PAYMENT_GET_PACKAGE_REQUEST';
export const PAYMENT_GET_PACKAGE_SUCCESS = 'PAYMENT_GET_PACKAGE_SUCCESS';
export const PAYMENT_GET_PACKAGE_FAILURE = 'PAYMENT_GET_PACKAGE_FAILURE';
export const PAYMENT_UPDATE_STATE = 'PAYMENT_UPDATE_STATE';

export const PAYMENT_CREATE_SHOPPING_CART_REQUEST = 'PAYMENT_CREATE_SHOPPING_CART_REQUEST';
export const PAYMENT_CREATE_SHOPPING_CART_SUCCESS = 'PAYMENT_CREATE_SHOPPING_CART_SUCCESS';
export const PAYMENT_CREATE_SHOPPING_CART_FAILURE = 'PAYMENT_CREATE_SHOPPING_CART_FAILURE';

export const PAYMENT_PROCESS_REQUEST = 'PAYMENT_PROCESS_REQUEST';
export const PAYMENT_PROCESS_SUCCESS = 'PAYMENT_PROCESS_SUCCESS';
export const PAYMENT_PROCESS_FAILURE = 'PAYMENT_PROCESS_FAILURE';

export const PAYMENT_CALLBACK_REQUEST = 'PAYMENT_CALLBACK_REQUEST';
export const PAYMENT_CALLBACK_SUCCESS = 'PAYMENT_CALLBACK_SUCCESS';
export const PAYMENT_CALLBACK_FAILURE = 'PAYMENT_CALLBACK_FAILURE';

export const UNLOCK_CHAT_BY_COIN_REQUEST = 'UNLOCK_CHAT_BY_COIN_REQUEST';
export const UNLOCK_CHAT_BY_COIN_SUCCESS = 'UNLOCK_CHAT_BY_COIN_SUCCESS';
export const UNLOCK_CHAT_BY_COIN_FAILURE = 'UNLOCK_CHAT_BY_COIN_FAILURE';

export const COIN_CONSUME_REQUEST = 'COIN_CONSUME_REQUEST'
export const COIN_CONSUME_SUCCESS = 'COIN_CONSUME_SUCCESS'
export const COIN_CONSUME_FAILURE = 'COIN_CONSUME_FAILURE'

export const PAYMENT_METHOD_BANK_TRANSFER = 1;
export const PAYMENT_METHOD_CREDIT_CARD = 2;
export const PAYMENT_METHOD_MOBILE_CARD = 3;

export const PAYMENT_PACKAGE_TYPE_COIN = 0;
export const PAYMENT_PACKAGE_TYPE_MONTHLY = 1;

export const PAYMENT_PACKAGE_ID_WEEKLY = 14;

export const VTC_GATEWAY_PAYMENT_STATUS_SUCCESS = '1';

export const PAYMENT_FAILURE_MESSAGE_1 = 'Giao dịch thất bại. Vui lòng kiểm tra lại với ngân hàng hoặc cổng thanh toán để biết thêm chi tiết.';
export const PAYMENT_FAILURE_MESSAGE_2 = 'Giao dịch thất bại do khách hàng huỷ thanh toán.';
export const PAYMENT_FAILURE_MESSAGE_3 = 'Giao dịch thất bại do thẻ / tài khoản không đủ điều kiện giao dịch (đang bị khoá, chưa đăng ký thanh toán online...). Vui lòng kiểm tra lại với ngân hàng phát hành thẻ / tài khoản để biết thêm chi tiết.';
export const PAYMENT_FAILURE_MESSAGE_4 = 'Giao dịch thất bại do số dư / tài khoản khách hàng không đủ để thực hiện giao dịch.';
export const PAYMENT_FAILURE_MESSAGE_5 = 'Giao dịch thất bại do khách hàng nhập sai thông tin thanh toán (sai số tài khoản hoặc mã OTP).';
export const PAYMENT_FAILURE_MESSAGE_6 = 'Giao dịch thất bại do cần chứng thực tại cổng thanh toán. Vui lòng kiểm tra email hoặc liên hệ với cổng thanh toán để biết thêm chi tiết.';

export const INSTRUCTION = 1;
export const CHOOSING_PACKAGE = 2;
export const FULFILL_PAYMENT = 3;
export const PAYMENT_RESULT = 4;