export const UPDATE_BEST_VOTE_PHOTO_STATE = 'UPDATE_BEST_VOTE_PHOTO_STATE'
export const BEST_VOTE_PHOTO_FEMALE = 'best_female'
export const BEST_VOTE_PHOTO_MALE = 'best_male'
export const BEST_VOTE_PHOTO_FEMALE_PHOTO = 'best_female_photo'
export const BEST_VOTE_PHOTO_MALE_PHOTO = 'best_male_photo'

export const UPDATE_COIN_REWARD_STATE = 'UPDATE_COIN_REWARD_STATE'
export const COIN_REWARD_3_LIKES = '3_likes';
export const COIN_REWARD_15_LIKES = '15_likes';
export const COIN_REWARD_20_LIKES = '20_likes';
export const COIN_REWARD_2_DAYS = '2_days';
export const COIN_REWARD_3_DAYS = '3_days';
export const COIN_REWARD_BIRTHDAY = 'birthday';