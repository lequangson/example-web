import { CALL_API } from '../middleware/api'
import {
  GET_WHAT_HOT_REQUEST, GET_WHAT_HOT_SUCCESS, GET_WHAT_HOT_FAILURE, 
  NEXT_WHAT_HOT_USER, HIDE_ADVERTISMENT_PROGRAM,
} from '../constants/WhatHot'
import {
  SEARCH_PAGE_SIZE,
} from '../constants/search'
import {
  API_GET,
} from '../constants/Enviroment'

export function getWhatHotRequest() {
  return {
    type: GET_WHAT_HOT_REQUEST,
  }
}

export function getWhatHot(pageIndex) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'whats_hot',
      params: {
        'paging_model[page_size]': SEARCH_PAGE_SIZE,
        'paging_model[page_index]': pageIndex,
      },
      authenticated: true,
      types: [GET_WHAT_HOT_REQUEST, GET_WHAT_HOT_SUCCESS, GET_WHAT_HOT_FAILURE],
      extentions: {
        pageIndex: pageIndex,
      }
    },
  }
}

export function showNextUser(isLikeAction = false, userGender = 'male', userType = 1) {
  return {
    type: NEXT_WHAT_HOT_USER,
    extentions: {
      isLikeAction,
      userGender,
      userType,
    }
  }
}

export function hideAdProgram() {
  return {
    type: HIDE_ADVERTISMENT_PROGRAM,
  }
}
