import { CALL_API } from '../middleware/api'
import {
  USER_SUBSCRIPTION_REQUEST,
  USER_SUBSCRIPTION_SUCCESS,
  USER_SUBSCRIPTION_FAILURE,

  USER_UNSUBSCRIPTION_REQUEST,
  USER_UNSUBSCRIPTION_SUCCESS,
  USER_UNSUBSCRIPTION_FAILURE,
} from '../constants/GCMNotification'
import {
  API_POST,
} from '../constants/Enviroment'

///////////////////////////////////////////
/// User subscription /////////////////////
export function userSubscriptionRequest() {
  return {
    type: USER_SUBSCRIPTION_REQUEST,
  }
}

export function userSubscription(endPoint) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'notify_subscription',
      params: {
        source_type: 1, //Mobile app
        end_point: endPoint,
      },
      authenticated: true,
      types: [USER_SUBSCRIPTION_REQUEST, USER_SUBSCRIPTION_SUCCESS, USER_SUBSCRIPTION_FAILURE],
    },
  }
}

export function userSubscriptionFailure() {
  return {
    type: USER_SUBSCRIPTION_FAILURE,
  }
}

///////////////////////////////////////////
/// User unsubscription ///////////////////
export function userUnsubscriptionRequest() {
  return {
    type: USER_UNSUBSCRIPTION_REQUEST,
  }
}

export function userUnsubscription(endPoint) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'notify_unsubscription',
      params: {
        end_point: endPoint,
      },
      authenticated: true,
      types: [USER_UNSUBSCRIPTION_REQUEST, USER_UNSUBSCRIPTION_SUCCESS, USER_UNSUBSCRIPTION_FAILURE],
    },
  }
}

export function userUnsubscriptionFailure() {
  return {
    type: USER_UNSUBSCRIPTION_FAILURE,
  }
}