import { CALL_API } from '../middleware/api'
import {
  UPDATE_TOROT_PAGE, UPDATE_CONTENT_PAGE_3
} from '../constants/Tarot'
import {
  API_GET,
} from '../constants/Enviroment'

export function updateTarotPage(page) {
  return {
    type: UPDATE_TOROT_PAGE,
    curentPage: page
  }
}

export function updateContentPage3(url, text2) {
  return {
    type: UPDATE_CONTENT_PAGE_3,
    url,
    text2
  }
}
