import { CALL_API } from '../middleware/api'
import { API_GET, API_POST } from '../constants/Enviroment'
import {
  GET_PHOTO_STREAM_REQUEST, GET_PHOTO_STREAM_SUCCESS, GET_PHOTO_STREAM_FAILURE,
  OPEN_PHOTO_STREAM_MODAL, CLOSE_PHOTO_STREAM_MODAL,
  VOTE_PHOTO_REQUEST, VOTE_PHOTO_SUCCESS, VOTE_PHOTO_FAILURE,
  GET_VOTED_USERS_REQUEST, GET_VOTED_USERS_SUCCESS, GET_VOTED_USERS_FAILURE,
  UPDATE_INFO_PHOTO_STREAM_MODAL,
  UPDATE_PHOTO_STREAM_STATE,
  GET_BEST_PHOTOS_REQUEST, GET_BEST_PHOTOS_SUCCESS, GET_BEST_PHOTOS_FAILURE,
  UPDATE_BEST_VOTE_PHOTO_STATE,
} from '../constants/photoStream';
import {
  SEARCH_PAGE_SIZE,
} from 'constants/search';

export function getPhotoStreamRequest() {
  return {
    type: GET_PHOTO_STREAM_REQUEST,
  }
}

export function getPhotoStream(pageIndex) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'pictures',
      params: {
        'paging_model[page_size]': SEARCH_PAGE_SIZE,
        'paging_model[page_index]': pageIndex,
      },
      authenticated: true,
      types: [GET_PHOTO_STREAM_REQUEST, GET_PHOTO_STREAM_SUCCESS, GET_PHOTO_STREAM_FAILURE],
      extentions: {
        pageIndex: pageIndex,
      },
    }
  }
}

export function getPhotoStreamFailure(msg = null) {
  return {
    type: GET_PHOTO_STREAM_FAILURE,
    msg,
  }
}

// Open and close photo stream modal
export function openPhotoStreamModal() {
  return {
    type: OPEN_PHOTO_STREAM_MODAL,
  }
}

export function closePhotoStreamModal() {
  return {
    type: CLOSE_PHOTO_STREAM_MODAL,
  }
}

// update info photo stream modal
export function updateInfoPhotoStreamModal(photo, photoIndex) {
  return {
    type: UPDATE_INFO_PHOTO_STREAM_MODAL,
    photo,
    photoIndex
  }
}

export function updatePhotoStreamState(identifier, key, value) {
  return {
    type: UPDATE_PHOTO_STREAM_STATE,
    identifier,
    key,
    value,
  }
}

export function votePhotoRequest() {
  return {
    type: VOTE_PHOTO_REQUEST,
  }
}

export function votePhoto(picture_id, picture_type, action_type, current_user_id, page_source) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'vote',
      params: {
        picture_id: picture_id,
        picture_type: picture_type,
        action_type: action_type,
      },
      authenticated: true,
      types: [
        VOTE_PHOTO_REQUEST,
        VOTE_PHOTO_SUCCESS,
        VOTE_PHOTO_FAILURE,
      ],
      extentions: {
        picture_id: picture_id,
        picture_type: picture_type,
        action_type: action_type,
        current_user_id: current_user_id,
        page_source: page_source,
      },
    },
  }
}

export function votePhotoFailure(msg = null) {
  return {
    type: VOTE_PHOTO_FAILURE,
    msg,
  }
}

export function getVotedUsersRequest() {
  return {
    type: GET_VOTED_USERS_REQUEST,
  }
}

export function getVotedUsers(picture_type, picture_id) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'vote_users',
      params: {
        picture_type: picture_type,
        picture_id: picture_id,
      },
      authenticated: true,
      types: [GET_VOTED_USERS_REQUEST, GET_VOTED_USERS_SUCCESS, GET_VOTED_USERS_FAILURE],
    }
  }
}

export function getVotedUsersFailure(msg = null) {
  return {
    type: GET_VOTED_USERS_FAILURE,
    msg,
  }
}

export function getBestPhotosRequest() {
  return {
    type: GET_BEST_PHOTOS_REQUEST,
  }
}

export function getBestPhotos(picture_type, picture_id) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'best_voted',
      params: '',
      authenticated: true,
      types: [GET_BEST_PHOTOS_REQUEST, GET_BEST_PHOTOS_SUCCESS, GET_BEST_PHOTOS_FAILURE],
    }
  }
}

export function getBestPhotosFailure(msg = null) {
  return {
    type: GET_BEST_PHOTOS_FAILURE,
    msg,
  }
}

export function updateBestVotePhotoState(modalInfo) {
  return {
    type: UPDATE_BEST_VOTE_PHOTO_STATE,
    modalInfo,
  }
}