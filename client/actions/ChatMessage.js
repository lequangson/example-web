import axios from 'axios'
import { CALL_API } from '../middleware/api';
import { API_POST, API_GET } from '../constants/Enviroment';
import {
  GET_NOTIFICATION_MESSAGES_REQUEST,
  GET_NOTIFICATION_MESSAGES_SUCCESS,
  GET_NOTIFICATION_MESSAGES_FAILURE,
  GET_LAST_MESSAGES_SUCCESS
} from '../constants/ChatMessage';
import * as ymmStorage from '../utils/ymmStorage';
// this action does not need to storre to reducer
export function getNotificationMessages() {

  const chatApiUrl = process.env.CHAT_API_URL
  // get api key
  const request = axios({
    method: 'GET',
    url: `${chatApiUrl}message_notifications`,
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` }
  })

  return {
    type: GET_NOTIFICATION_MESSAGES_SUCCESS,
    payload: request,
  }
}

export function getLastMessages() {

  const chatApiUrl = process.env.CHAT_API_URL
  // get api key
  const request = axios({
    method: 'GET',
    url: `${chatApiUrl}get-last-message-and-online-status`,
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` }
  })

  return {
    type: GET_LAST_MESSAGES_SUCCESS,
    payload: request,
  }
}

export function ignoreMessage(friend_user_ids) {

  const chatApiUrl = process.env.CHAT_API_URL
  // get api key
  const request = axios({
    method: 'POST',
    url: `${chatApiUrl}ignore-messages`,
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` },
    data: {
      friend_user_ids
    },
  })

  return {
    type: 'NONE',
    payload: request,
  }
}