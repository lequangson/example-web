import { CALL_API } from '../middleware/api';
import {
  PAYMENT_GET_PACKAGE_REQUEST, PAYMENT_GET_PACKAGE_SUCCESS,
  PAYMENT_GET_PACKAGE_FAILURE, PAYMENT_UPDATE_STATE,
  PAYMENT_CREATE_SHOPPING_CART_REQUEST, PAYMENT_CREATE_SHOPPING_CART_SUCCESS,
  PAYMENT_CREATE_SHOPPING_CART_FAILURE,
  PAYMENT_PROCESS_REQUEST, PAYMENT_PROCESS_SUCCESS, PAYMENT_PROCESS_FAILURE,
  PAYMENT_CALLBACK_REQUEST, PAYMENT_CALLBACK_SUCCESS, PAYMENT_CALLBACK_FAILURE,
  COIN_CONSUME_REQUEST, COIN_CONSUME_SUCCESS, COIN_CONSUME_FAILURE,
} from '../constants/Payment';
import { API_GET, API_POST } from '../constants/Enviroment';

export function getPaymentPackages(package_type = null) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'payment_packages',
      params: {package_type: package_type},
      authenticated: true,
      types: [
        PAYMENT_GET_PACKAGE_REQUEST,
        PAYMENT_GET_PACKAGE_SUCCESS,
        PAYMENT_GET_PACKAGE_FAILURE,
      ],
    },
  }
}

export function createShoppingCart(paymentInput) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'shopping_cart',
      params: {
        payment_method: paymentInput.payment_method,
        package_id: paymentInput.package_id,
        provider_code: paymentInput.provider_code,
        customer_name: paymentInput.customer_name,
        customer_phone_number: paymentInput.customer_phone_number,
        shopping_cart_id: paymentInput.shopping_cart_id,
        mobile_card_info: JSON.stringify(paymentInput.mobile_card_info)
      },
      authenticated: true,
      types: [
        PAYMENT_CREATE_SHOPPING_CART_REQUEST,
        PAYMENT_CREATE_SHOPPING_CART_SUCCESS,
        PAYMENT_CREATE_SHOPPING_CART_FAILURE,
      ],
    },
  }
}

export function processPayment(shopping_cart_id) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'payment_process',
      params: {
        shopping_cart_id: shopping_cart_id
      },
      authenticated: true,
      types: [
        PAYMENT_PROCESS_REQUEST,
        PAYMENT_PROCESS_SUCCESS,
        PAYMENT_PROCESS_FAILURE,
      ],
    },
  }
}

export function updatePaymentState(key, value) {
  return {
    type: PAYMENT_UPDATE_STATE,
    key,
    value
  }
}

export function paymentCallback(data, signature) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'web_payment_callback',
      params: {
        data: data,
        signature: signature
      },
      authenticated: true,
      types: [
        PAYMENT_CALLBACK_REQUEST,
        PAYMENT_CALLBACK_SUCCESS,
        PAYMENT_CALLBACK_FAILURE,
      ],
    },
  }
}

export function unlockChat(male_id, female_id) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'unlock_chat',
      params: {
        male_id: male_id,
        female_id: female_id
      },
      authenticated: true,
      types: [
        'NO_NEED_UPDATE_STATE',
        'NO_NEED_UPDATE_STATE',
        'NO_NEED_UPDATE_STATE'
      ],
    },
  }
}

export function coinConsume(coin_consume_type, male_id = null, female_id = null) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'coin_consume',
      params: {
        coin_consume_type,
        male_id,
        female_id
      },
      authenticated: true,
      types: [
        COIN_CONSUME_REQUEST,
        COIN_CONSUME_SUCCESS,
        COIN_CONSUME_FAILURE,
      ],
      extentions: coin_consume_type,
    },
  }
}