import { CALL_API } from '../middleware/api'
import {
  GET_DAILY_REMIND_REQUEST, GET_DAILY_REMIND_SUCCESS, GET_DAILY_REMIND_FAILURE,
  CLOSE_DAILY_REMIND_MODAL, SHOW_REMIND_MODAL, 
} from '../constants/Daily_Remind'
import {
  API_GET,
} from '../constants/Enviroment'

export function getDailyRemindRequest() {
  return {
    type: GET_DAILY_REMIND_REQUEST,
  }
}

export function getDailyRemind(gender) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'remind_first_access',
      params: {},
      authenticated: true,
      types: [GET_DAILY_REMIND_REQUEST, GET_DAILY_REMIND_SUCCESS, GET_DAILY_REMIND_FAILURE],
      extentions: {
        gender: gender,
      }
    },
  }
}

export function closeDailyRemind() {
  return {
    type: CLOSE_DAILY_REMIND_MODAL,
  }
}

export function showRemindModal(modalName) {
  return {
    type: SHOW_REMIND_MODAL,
    modalName: modalName
  }
}
