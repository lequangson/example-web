// The middleware to call the API for search user
import { CALL_API } from '../middleware/api'
import {
  UPDATE_PREVIOUS_PAGE, UPDATE_SCROLL_POSSITION,
  SEND_NOTIFICATION, SAVE_FILES_UPLOAD, CLEAR_FILES_UPLOAD,
  SET_VIEW_PROFILE_SOURCE, SET_PROFILE_NEXT_USER_INDEX,
  ADD_ACTIVITY_AUDIT_REQUEST, ADD_ACTIVITY_AUDIT_SUCCESS, ADD_ACTIVITY_AUDIT_FAILURE,
  GA_SEND, SEND_GOOGLE_ANALYTIC,
  API_POST, API_GET,
  MODAL_SHOW_NEXT, UPDATE_SEARCH_ELEMENT_HEIGHT,
  FACEBOOK_SYNC_REQUEST, FACEBOOK_SYNC_POST_SUCCESS, FACEBOOK_SYNC_GET_SUCCESS, FACEBOOK_SYNC_FAILURE,
  SEND_CONTACT_REQUEST, SEND_CONTACT_SUCCESS, SEND_CONTACT_FAILURE, SAVE_IMAGE,
  UPDATE_UPLOAD_PICTURE_PAGESOURCE, UPDATE_LAST_UPDATE,
  UPDATE_PUSH_NOTI_MODAL, UPDATE_ENVIRONMENT_STATE,
  GET_REMIND_REQUEST, GET_REMIND_SUCCESS, GET_REMIND_FAILURE,
  UPDATE_REMIND_REQUEST, UPDATE_REMIND_SUCCESS, UPDATE_REMIND_FAILURE,
  SHOW_NEXT_REMIND_USER, REMOVE_BANNER, DISABLE_THIRD_PARTY_COOKIES
} from '../constants/Enviroment'

export function gaSend(eventAction, ga_track_data) {
  return {
    type: GA_SEND,
    eventAction,
    ga_track_data,
  }
}

export function sendGoogleAnalytic(eventAction, ga_track_data) {
  return {
    type: SEND_GOOGLE_ANALYTIC,
    eventAction,
    ga_track_data,
  }
}

export function updatePreviousPage(page) {
  return {
    type: UPDATE_PREVIOUS_PAGE,
    previous_page: page,
  }
}

export function updateUploadPicturePageSource(page) {
  return {
    type: UPDATE_UPLOAD_PICTURE_PAGESOURCE,
    page: page,
  }
}

export function updateScrollPossition(page, scroll_top) {
  return {
    type: UPDATE_SCROLL_POSSITION,
    page,
    scroll_top,
  }
}

export function setViewProfileSource(viewSource) {
  return {
    type: SET_VIEW_PROFILE_SOURCE,
    viewSource,
  }
}

export function setProfileUserNextIndex(next_index) {
  return {
    type: SET_PROFILE_NEXT_USER_INDEX,
    next_index,
  }
}

export function sentNotification(msg = '', timeout = 0, type_error = '') {
  return {
    type: SEND_NOTIFICATION,
    msg,
    timeout,
    type_error,
  }
}

export function saveFilesUpload(files, uploadType) {
  return {
    type: SAVE_FILES_UPLOAD,
    files,
    uploadType,
  }
}

export function saveImage(file) {
  return {
    type: SAVE_IMAGE,
    file,
  }
}

export function clearFilesUpload(uploadType) {
  return {
    type: CLEAR_FILES_UPLOAD,
    uploadType,
  }
}

// ------------------- Add activity audit -------------------- //
export function addActivityAuditRequest() {
  return {
    type: ADD_ACTIVITY_AUDIT_REQUEST,
  }
}

export function addActivityAudit(activityCode, toUserIdentifier, originalValue, newValue) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'activity_audits',
      params: {
        'activity_audit[activity_code]': activityCode,
        'activity_audit[original_value]': originalValue,
        'activity_audit[new_value]': newValue,
        'activity_audit[to_user_identifier]': toUserIdentifier,
      },
      authenticated: true,
      types: [ADD_ACTIVITY_AUDIT_REQUEST, ADD_ACTIVITY_AUDIT_SUCCESS, ADD_ACTIVITY_AUDIT_FAILURE],
    },
  }
}
export function addActivityAuditFailure() {
  return {
    type: ADD_ACTIVITY_AUDIT_REQUEST,
  }
}
// ------------------------------------------------------------ //

// Modal
export function modalShowNext(modalName) {
  return {
    type: MODAL_SHOW_NEXT,
    modalName,
  }
}


export function updateElementHeight(view_mode, key, value) {
  return {
    type: UPDATE_SEARCH_ELEMENT_HEIGHT,
    key,
    value,
    view_mode,
  }
}

// ------------------Facebook Synchronize ------------------//
export function facebookSynchronizeRequest() {
  return {
    type: FACEBOOK_SYNC_REQUEST,
  }
}

export function facebookSynchronize() {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'facebook/synchronizes',
      params: {},
      authenticated: true,
      types: [FACEBOOK_SYNC_REQUEST, FACEBOOK_SYNC_POST_SUCCESS, FACEBOOK_SYNC_FAILURE],
    },
  }
}

export function getLastFacebookSynchronize() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'facebook/synchronizes',
      params: {},
      authenticated: true,
      types: [FACEBOOK_SYNC_REQUEST, FACEBOOK_SYNC_GET_SUCCESS, FACEBOOK_SYNC_FAILURE],
    },
  }
}


export function facebookSynchronizeFailure() {
  return {
    type: FACEBOOK_SYNC_FAILURE,
  }
}
// ================ Send contact ========================
export function sentContactRequest() {
  return {
    type: SEND_CONTACT_REQUEST,
  }
}

export function sentContact(params, isAnonymous = false) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'contact_us',
      params,
      authenticated: !isAnonymous,
      types: [SEND_CONTACT_REQUEST, SEND_CONTACT_SUCCESS, SEND_CONTACT_FAILURE],
      isAnonymous,
    },
  }
}

export function sentContactFailure() {
  return {
    type: SEND_CONTACT_FAILURE,
  }
}

export function updateLastUpdate(time) {
  return {
    type: UPDATE_LAST_UPDATE,
    time,
  }
}

export function updatePushNotiModal(isOpen, modalType, nick_name) {
  return {
    type: UPDATE_PUSH_NOTI_MODAL,
    isOpen,
    modalType,
    nick_name,
  }
}

export function updateEnvironmentState(key, value) {
  return {
    type: UPDATE_ENVIRONMENT_STATE,
    key,
    value,
  }
}

export function getRemind() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'remind_users',
      params: '',
      authenticated: true,
      types: [GET_REMIND_REQUEST, GET_REMIND_SUCCESS, GET_REMIND_FAILURE],
    },
  }
}

export function updateRemind(identifier) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'remind_users',
      params: {
        user_identifier: identifier,
      },
      authenticated: true,
      types: [UPDATE_REMIND_REQUEST, UPDATE_REMIND_SUCCESS, UPDATE_REMIND_FAILURE],
    },
  }
}

export function showNextRemindUser() {
  return {
    type: SHOW_NEXT_REMIND_USER,
  }
}

export function removeBanner(banner) {
  return {
    type: REMOVE_BANNER,
    banner,
  }
}

export function disable3rdPartyCookies(disabled) {
  return {
    type: DISABLE_THIRD_PARTY_COOKIES,
    disabled: disabled
  }
}