import { CALL_API } from '../middleware/api'
import {
  GET_USER_QUESTIONS_REQUEST, GET_USER_QUESTIONS_SUCCESS, GET_USER_QUESTIONS_FAILURE,
  ANSWER_QUESTION_REQUEST, ANSWER_QUESTION_SUCCESS, ANSWER_QUESTION_FAILURE,
  CHOOSE_EXPLORER_QUESTION_REQUEST, CHOOSE_EXPLORER_QUESTION_SUCCESS, CHOOSE_EXPLORER_QUESTION_FAILURE,
  GET_MATCHED_ANSWERED_QUESTION_REQUEST, GET_MATCHED_ANSWERED_QUESTION_SUCCESS, GET_MATCHED_ANSWERED_QUESTION_FAILURE,
  MATCH_QUESTION_UPDATE_STATE
} from '../constants/Match_Questions'
import {
  API_GET, API_POST
} from '../constants/Enviroment'

export function updateMatchQuestionState(key, value) {
  return {
    type: MATCH_QUESTION_UPDATE_STATE,
    key,
    value
  }
}


export function getUserQuestionsRequest() {
  return {
    type: GET_USER_QUESTIONS_REQUEST,
  }
}

export function getUserQuestions(identifier = null) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'match_questions',
      params: identifier !== null ? { user_identifier: identifier } : '',
      authenticated: true,
      types: [GET_USER_QUESTIONS_REQUEST, GET_USER_QUESTIONS_SUCCESS, GET_USER_QUESTIONS_FAILURE],
      extentions: {
        identifier: identifier
      }
    },
  }
}

export function getUserQuestionsFailure(msg = null) {
  return {
    type: GET_USER_QUESTIONS_FAILURE,
    msg,
  }
}

export function answerQuestionRequest() {
  return {
    type: ANSWER_QUESTION_REQUEST,
  }
}

export function answerQuestion(type, question_id, answer_code, explorer_user_identifier) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'answer_question',
      params:  type === 1 ? {
        answer_type: type,
        question_id: question_id,
        answer_code: answer_code,
        explorer_user: explorer_user_identifier,
      } :
      {
        answer_type: type,
        question_id: question_id,
        answer_code: answer_code,
      }
      ,
      authenticated: true,
      types: [ANSWER_QUESTION_REQUEST, ANSWER_QUESTION_SUCCESS, ANSWER_QUESTION_FAILURE],
      extentions: {
        type: type,
        question_id: question_id,
        answer_code: answer_code,
      }
    },
  }
}

export function answerQuestionFailure(msg = null) {
  return {
    type: ANSWER_QUESTION_FAILURE,
    msg,
  }
}

export function chooseExplorerQuestionRequest() {
  return {
    type: CHOOSE_EXPLORER_QUESTION_REQUEST,
  }
}

export function chooseExplorerQuestion(question_id, is_explore_question) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'explorer_question',
      params: {
        question_id: question_id,
        is_explore_question: is_explore_question,
      },
      authenticated: true,
      types: [CHOOSE_EXPLORER_QUESTION_REQUEST, CHOOSE_EXPLORER_QUESTION_SUCCESS, CHOOSE_EXPLORER_QUESTION_FAILURE],
      extentions: {
        question_id: question_id,
        is_explore_question: is_explore_question,
      }
    },
  }
}

export function chooseExplorerQuestionFailure(msg = null) {
  return {
    type: CHOOSE_EXPLORER_QUESTION_FAILURE,
    msg,
  }
}

export function getMatchedAnsweredQuestionRequest() {
  return {
    type: GET_MATCHED_ANSWERED_QUESTION_REQUEST,
  }
}

export function getMatchedAnsweredQuestion(identifier) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'matched_explore_answers',
      params: {
        explorer_user: identifier,
      },
      authenticated: true,
      types: [GET_MATCHED_ANSWERED_QUESTION_REQUEST, GET_MATCHED_ANSWERED_QUESTION_SUCCESS, GET_MATCHED_ANSWERED_QUESTION_FAILURE],
    },
  }
}

export function getMatchedAnsweredQuestionFailure(msg = null) {
  return {
    type: GET_MATCHED_ANSWERED_QUESTION_FAILURE,
    msg,
  }
}