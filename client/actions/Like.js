import { CALL_API } from '../middleware/api'
import {
  SET_LIKE_INFO_REQUEST, SET_LIKE_INFO, UPDATE_SIMILAR_USER_MODAL,
  UPDATE_MATCH_MODAL, UPDATE_FIRST_LIKE_MODAL,
  GET_FIRST_LIKE_INFO, GET_FIRST_LIKE_INFO_REQUEST, GET_FIRST_LIKE_INFO_FAILURE,
  SEND_SUPER_LIKE_REQUEST, SEND_SUPER_LIKE_SUCCESS, SEND_SUPER_LIKE_FAILURE
} from '../constants/Like'
import { API_GET, API_POST } from '../constants/Enviroment'

export function setLikeInfoRequest() {
  return {
    type: SET_LIKE_INFO_REQUEST,
  }
}

export function setLikeInfo(likeFrom, likeTo, isLikeModalOpen = false, isLikeMessageModalOpen = false, page_source) {
  return {
    type: SET_LIKE_INFO,
    likeFrom,
    likeTo,
    isLikeModalOpen,
    isLikeMessageModalOpen,
    page_source,
  }
}

export function updateMatchModal(isMatchModalOpen = false, matchInfo = {}) {
  return {
    type: UPDATE_MATCH_MODAL,
    isMatchModalOpen,
    matchInfo,
  }
}

export function updateFirstLikeModal(isOpen = false, firstLikeSent = false, firstLikeReceived = false, firstLikeInfo = {}) {
  return {
    type: UPDATE_FIRST_LIKE_MODAL,
    isOpen,
    firstLikeSent,
    firstLikeReceived,
    firstLikeInfo,
  }
}

export function getFirstLikeInfor() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'first_like',
      params: '',
      authenticated: true,
      types: [
        GET_FIRST_LIKE_INFO_REQUEST,
        GET_FIRST_LIKE_INFO,
        GET_FIRST_LIKE_INFO_FAILURE,
      ],
    },
  }
}

export function updateSimilarUserModal(isOpen = false, similarUserInfo = {}, likeFrom = {}, likeTo = {}) {
  return {
    type: UPDATE_SIMILAR_USER_MODAL,
    isOpen,
    similarUserInfo,
    likeFrom,
    likeTo
  }
}

export function sendSuperLike(male_id, female_id) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'super_match',
      params: {
        male_id,
        female_id
      },
      authenticated: true,
      types: [
        SEND_SUPER_LIKE_REQUEST,
        SEND_SUPER_LIKE_SUCCESS,
        SEND_SUPER_LIKE_FAILURE,
      ],
    },
  }
}
