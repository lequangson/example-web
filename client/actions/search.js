// The middleware to call the API for search user
import { CALL_API } from 'middleware/api';
import { API_GET, API_POST } from 'constants/Enviroment';
import {
  SEARCH_USER_REQUEST, SEARCH_USER_SUCCESS, SEARCH_USER_FAILURE,
  SEARCH_CONDITION_UPDATE_REQUEST, SEARCH_CONDITION_UPDATE_SUCCESS, SEARCH_CONDITION_UPDATE_FAILURE,
  SEARCH_UPDATE_VIEW_MODE, SEARCH_PAGE_SIZE, UPDATE_PAGING_INFORMATION, NEED_SCROLL_TO_POSITION,
  SEARCH_CONDITION_GET_REQUEST, SEARCH_CONDITION_GET_SUCCESS, SEARCH_CONDITION_GET_FAILURE,
  UPDATE_SEARCH_USER_PROFILE, UPDATE_SEARCH_STATE, SEARCH_RESET_DATA,
  SEARCH_NEW_USER_REQUEST, SEARCH_NEW_USER_SUCCESS, SEARCH_NEW_USER_FAILURE,
  SEARCH_MATCHING_RATIO_USER_REQUEST, SEARCH_MATCHING_RATIO_USER_SUCCESS, SEARCH_MATCHING_RATIO_USER_FAILURE,
  SEARCH_ONLINE_USER_REQUEST, SEARCH_ONLINE_USER_SUCCESS, SEARCH_ONLINE_USER_FAILURE,
  SEARCH_NEARBY_USER_REQUEST, SEARCH_NEARBY_USER_SUCCESS, SEARCH_NEARBY_USER_FAILURE,
  SEARCH_HARMONY_OF_AGE_REQUEST, SEARCH_HARMONY_OF_AGE_SUCCESS, SEARCH_HARMONY_OF_AGE_FAILURE,
  SEARCH_HARMONY_OF_HOROSCOPE_REQUEST, SEARCH_HARMONY_OF_HOROSCOPE_SUCCESS, SEARCH_HARMONY_OF_HOROSCOPE_FAILURE,
  SEARCH_HARMONY_OF_FATE_REQUEST, SEARCH_HARMONY_OF_FATE_SUCCESS, SEARCH_HARMONY_OF_FATE_FAILURE,
  SEARCH_RESET_SELECTED
} from 'constants/search';

export function searchUserRequest() {
  return {
    type: SEARCH_USER_REQUEST,
  }
}

export function searchUserProcess(page, page_size) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'search_users',
      params: {
        'paging_model[page_size]': page_size || SEARCH_PAGE_SIZE,
        'paging_model[page_index]': page,
      },
      authenticated: true,
      types: [SEARCH_USER_REQUEST, SEARCH_USER_SUCCESS, SEARCH_USER_FAILURE],
    },
  }
}

export function searchUserFailure() {
  return {
    type: SEARCH_USER_FAILURE,
  }
}

export function searchNewUserRequest() {
  return {
    type: SEARCH_NEW_USER_REQUEST,
  }
}

export function searchNewUserProcess(page, page_size) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'new_users',
      params: {
        'paging_model[page_size]': page_size || SEARCH_PAGE_SIZE,
        'paging_model[page_index]': page,
      },
      authenticated: true,
      types: [SEARCH_NEW_USER_REQUEST, SEARCH_NEW_USER_SUCCESS, SEARCH_NEW_USER_FAILURE],
    },
  }
}

export function searchNewUserFailure() {
  return {
    type: SEARCH_NEW_USER_FAILURE,
  }
}

export function searchOnlineUserRequest() {
  return {
    type: SEARCH_ONLINE_USER_REQUEST,
  }
}

export function searchOnlineUserProcess(page, page_size) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'online_users',
      params: {
        'paging_model[page_size]': page_size || SEARCH_PAGE_SIZE,
        'paging_model[page_index]': page,
      },
      authenticated: true,
      types: [SEARCH_ONLINE_USER_REQUEST, SEARCH_ONLINE_USER_SUCCESS, SEARCH_ONLINE_USER_FAILURE],
    },
  }
}

export function searchOnlineUserFailure() {
  return {
    type: SEARCH_ONLINE_USER_FAILURE,
  }
}

export function searchMatchingRatioUserRequest() {
  return {
    type: SEARCH_MATCHING_RATIO_USER_REQUEST,
  }
}

export function searchMatchingRatioUserProcess(page, page_size) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'matching_ratio_users',
      params: {
        'paging_model[page_size]': page_size || SEARCH_PAGE_SIZE,
        'paging_model[page_index]': page,
      },
      authenticated: true,
      types: [SEARCH_MATCHING_RATIO_USER_REQUEST, SEARCH_MATCHING_RATIO_USER_SUCCESS, SEARCH_MATCHING_RATIO_USER_FAILURE],
    },
  }
}

export function searchMatchingRatioUserFailure() {
  return {
    type: SEARCH_MATCHING_RATIO_USER_FAILURE,
  }
}

export function searchNearbyUserRequest() {
  return {
    type: SEARCH_NEARBY_USER_REQUEST,
  }
}

export function searchNearbyUserProcess(page, page_size) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'nearby_users',
      params: {
        'paging_model[page_size]': page_size || SEARCH_PAGE_SIZE,
        'paging_model[page_index]': page,
      },
      authenticated: true,
      types: [SEARCH_NEARBY_USER_REQUEST, SEARCH_NEARBY_USER_SUCCESS, SEARCH_NEARBY_USER_FAILURE],
    },
  }
}

export function searchNearbyUserFailure() {
  return {
    type: SEARCH_NEARBY_USER_FAILURE,
  }
}

export function searchHarmonyOfAgeRequest() {
  return {
    type: SEARCH_HARMONY_OF_AGE_REQUEST,
  }
}

export function searchHarmonyOfAgeProcess(page, page_size) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'harmony_of_age',
      params: {
        'paging_model[page_size]': page_size || SEARCH_PAGE_SIZE,
        'paging_model[page_index]': page,
      },
      authenticated: true,
      types: [SEARCH_HARMONY_OF_AGE_REQUEST, SEARCH_HARMONY_OF_AGE_SUCCESS, SEARCH_HARMONY_OF_AGE_FAILURE],
    },
  }
}

export function searchHarmonyOfAgeFailure() {
  return {
    type: SEARCH_HARMONY_OF_AGE_FAILURE,
  }
}

export function searchHarmonyOfFateRequest() {
  return {
    type: SEARCH_HARMONY_OF_FATE_REQUEST,
  }
}

export function searchHarmonyOfFateProcess(page, page_size) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'harmony_of_fate',
      params: {
        'paging_model[page_size]': page_size || SEARCH_PAGE_SIZE,
        'paging_model[page_index]': page,
      },
      authenticated: true,
      types: [ SEARCH_HARMONY_OF_FATE_REQUEST, SEARCH_HARMONY_OF_FATE_SUCCESS, SEARCH_HARMONY_OF_FATE_FAILURE],
    },
  }
}

export function searchHarmonyOfFateFailure() {
  return {
    type: SEARCH_HARMONY_OF_FATE_FAILURE,
  }
}

export function searchHarmonyOfHoroscopeRequest() {
  return {
    type: SEARCH_HARMONY_OF_HOROSCOPE_REQUEST,
  }
}

export function searchHarmonyOfHoroscopeProcess(page, page_size) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'harmony_of_horoscope',
      params: {
        'paging_model[page_size]': page_size || SEARCH_PAGE_SIZE,
        'paging_model[page_index]': page,
      },
      authenticated: true,
      types: [SEARCH_HARMONY_OF_HOROSCOPE_REQUEST, SEARCH_HARMONY_OF_HOROSCOPE_SUCCESS, SEARCH_HARMONY_OF_HOROSCOPE_FAILURE],
    },
  }
}

export function searchHarmonyOfHoroscopeFailure() {
  return {
    type: SEARCH_HARMONY_OF_HOROSCOPE_FAILURE,
  }
}

// The following for redux-infinite
export function updateSearchConditionRequest() {
  return {
    type: SEARCH_CONDITION_UPDATE_REQUEST,
  }
}

export function updateSearchCondition(params) {
  return {
    [CALL_API]: {
      method: 'POST',
      endpoint: 'search_conditions',
      params,
      authenticated: true,
      types: [
        SEARCH_CONDITION_UPDATE_REQUEST,
        SEARCH_CONDITION_UPDATE_SUCCESS,
        SEARCH_CONDITION_UPDATE_FAILURE,
      ],
    },
  }
}


export function getSearchConditionRequest() {
  return {
    type: SEARCH_CONDITION_GET_REQUEST,
  }
}

export function getSearchCondition() {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'search_conditions',
      params: {
        search_type: 0
      },
      authenticated: true,
      types: [
        SEARCH_CONDITION_GET_REQUEST,
        SEARCH_CONDITION_GET_SUCCESS,
        SEARCH_CONDITION_GET_FAILURE,
      ],
    },
  }
}

export function updateViewMode(mode, enviroment) {
  return {
    type: SEARCH_UPDATE_VIEW_MODE,
    view_mode: mode,
    enviroment: enviroment,
  }
}

export function updatePagingInformation(total_elements, current_page, scroll_position, enviroment, next_enviroment) {
  return {
    type: UPDATE_PAGING_INFORMATION,
    total_elements,
    current_page,
    scroll_position,
    enviroment,
    next_enviroment,
  }
}

export function resetData(pageSource) {
  return {
    type: SEARCH_RESET_DATA,
    pageSource
  }
}

export function needScrollToPosition(need_scroll_to_position) {
  return {
    type: NEED_SCROLL_TO_POSITION,
    need_scroll_to_position: need_scroll_to_position
  }
}

export function updateSearchUserProfile(identifier, key, value) {
  return {
    type: UPDATE_SEARCH_USER_PROFILE,
    identifier,
    key,
    value,
  }
}

export function updateReducerSearchState(identifier, key, value) {
  return {
    type: UPDATE_SEARCH_STATE,
    identifier,
    key,
    value,
  }
}

export function resetSelectedConditions() {
  return {
    type: SEARCH_RESET_SELECTED
  }
}

