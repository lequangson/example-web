import { CALL_API } from '../middleware/api'
import {
  GET_RANDOM_MATCH_REQUEST, GET_RANDOM_MATCH_SUCCESS, GET_RANDOM_MATCH_FAILURE,
  UPDATE_RANDOM_MATCH_REQUEST, UPDATE_RANDOM_MATCH_SUCCESS, UPDATE_RANDOM_MATCH_FAILURE,
  UPDATE_MODAL_STATE, CLEAR_MODAL_DATA,
  GET_DETAIL_RANDOM_MATCH_REQUEST, GET_DETAIL_RANDOM_MATCH_SUCCESS, GET_DETAIL_RANDOM_MATCH_FAILURE,
  UPDATE_DETAIL_RANDOM_MATCH, UPDATE_RANDOM_MATCH_STATE,
  GET_REMIND_RANDOM_MATCH_REQUEST, GET_REMIND_RANDOM_MATCH_SUCCESS, GET_REMIND_RANDOM_MATCH_FAILURE,
  DEACTIVE_RANDOM_MATCH_REQUEST,
  DEACTIVE_RANDOM_MATCH_SUCCESS,
  DEACTIVE_RANDOM_MATCH_FAILURE,
  SHOW_BUTTON_START_CHAT
} from '../constants/random_match'
import { API_GET, API_POST } from '../constants/Enviroment'

export function getRandomMatchRequest() {
  return {
    type: GET_RANDOM_MATCH_REQUEST,
  }
}
export function getRandomMatch() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'random_matches',
      params: '',
      authenticated: true,
      types: [
        GET_RANDOM_MATCH_REQUEST,
        GET_RANDOM_MATCH_SUCCESS,
        GET_RANDOM_MATCH_FAILURE,
      ],
    },
  }
}

export function getRandomMatchFailure(msg = null) {
  return {
    type: GET_RANDOM_MATCH_FAILURE,
    msg,
  }
}

export function updateRandomMatchRequest(nick_name) {
  return {
    type: UPDATE_RANDOM_MATCH_REQUEST,
    extentions: {
      nick_name: nick_name,
    }
  }
}
export function updateRandomMatch(identifier) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'random_matches',
      params: {
        user_identifier: identifier,
      },
      authenticated: true,
      types: [
        UPDATE_RANDOM_MATCH_REQUEST,
        UPDATE_RANDOM_MATCH_SUCCESS,
        UPDATE_RANDOM_MATCH_FAILURE,
      ],
      extentions: {
        identifier: identifier,
      }
    },
  }
}

export function updateRandomMatchFailure(msg = null) {
  return {
    type: UPDATE_RANDOM_MATCH_FAILURE,
    msg,
  }
}

export function updateModalState(state) {
  return {
    type: UPDATE_MODAL_STATE,
    state,
  }
}

export function clearModalData() {
  return {
    type: CLEAR_MODAL_DATA,
  }
}

export function getDetailRandomMatch() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'active_random_matched',
      authenticated: true,
      params: {},
      types: [
        GET_DETAIL_RANDOM_MATCH_REQUEST,
        GET_DETAIL_RANDOM_MATCH_SUCCESS,
        GET_DETAIL_RANDOM_MATCH_FAILURE,
      ]
    }
  }
}
export function getRemindRandomMatchRequest() {
  return {
    type: GET_REMIND_RANDOM_MATCH_REQUEST,
  }
}
export function getRemindRandomMatch() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'expired_random_matched',
      params: '',
      authenticated: true,
      types: [
        GET_REMIND_RANDOM_MATCH_REQUEST,
        GET_REMIND_RANDOM_MATCH_SUCCESS,
        GET_REMIND_RANDOM_MATCH_FAILURE,
      ],
    },
  }
}

export function updateDetailRandomMatch(random_match) {
  return {
    type: UPDATE_DETAIL_RANDOM_MATCH,
    random_match
  }
}

export function updateRandomMatchState(key, value) {
  return {
    type: UPDATE_RANDOM_MATCH_STATE,
    key,
    value
  }
}
export function getRemindRandomMatchFailure(msg = null) {
  return {
    type: GET_REMIND_RANDOM_MATCH_FAILURE,
    msg,
  }
}


export function deactiveRandomMatch(user_identifier) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'deactive_random_matched',
      authenticated: true,
      params: {
        user_identifier
      },
      types: [
        DEACTIVE_RANDOM_MATCH_REQUEST,
        DEACTIVE_RANDOM_MATCH_SUCCESS,
        DEACTIVE_RANDOM_MATCH_FAILURE,
      ]
    }
  }
}

export function showButton(identifier) {
  return {
    type: SHOW_BUTTON_START_CHAT,
    identifier,
  }
}