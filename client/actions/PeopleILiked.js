import { CALL_API } from '../middleware/api'
import {
  GET_PEOPLEILIKED_REQUEST,
  GET_PEOPLEILIKED_SUCCESS,
  GET_PEOPLEILIKED_FAILURE,
} from '../constants/PeopleILiked'
import {
  API_GET,
} from '../constants/Enviroment'

export function getPeopleILikedRequest() {
  return {
    type: GET_PEOPLEILIKED_REQUEST,
  }
}

export function getPeopleILiked() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'likes',
      params: '',
      authenticated: true,
      types: [GET_PEOPLEILIKED_REQUEST, GET_PEOPLEILIKED_SUCCESS, GET_PEOPLEILIKED_FAILURE],
    },
  }
}

export function getPeopleILikedFailure(msg = null) {
  return {
    type: GET_PEOPLEILIKED_FAILURE,
    msg,
  }
}