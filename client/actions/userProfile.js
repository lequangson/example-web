import axios from 'axios';
import debounce from "debounce";
// The middleware to call the API for quotes
import { CALL_API } from 'middleware/api';
import {
  GET_USER_PROFILE_REQUEST, GET_USER_PROFILE_SUCCESS, GET_USER_PROFILE_FAILURE,
  GET_CURRENT_USER_REQUEST, GET_CURRENT_USER_SUCCESS, GET_CURRENT_USER_FAILURE,
  SEND_LIKE_REQUEST, SEND_LIKE_SUCCESS, SEND_LIKE_FAILURE,
  UPDATE_USER_PROFILE_REQUEST, UPDATE_USER_PROFILE_SUCCESS,
  UPDATE_USER_PROFILE_FAILURE,
  GET_SELF_INTRODUCTION_REQUEST, GET_SELF_INTRODUCTION_SUCCESS,
  GET_SELF_INTRODUCTION_FAILURE,
  UPLOAD_IMAGE_VALIDATE_REQUEST, UPLOAD_IMAGE_VALIDATE_SUCCESS,
  UPLOAD_IMAGE_VALIDATE_FAILURE,
  UPLOAD_IMAGE_REQUEST, UPLOAD_IMAGE_SUCCESS, UPLOAD_IMAGE_FAILURE,
  DELETE_USER_PICTURE_REQUEST, DELETE_USER_PICTURE_SUCCESS,
  DELETE_USER_PICTURE_FAILURE,
  GET_FB_ALBUM_REQUEST, GET_FB_ALBUM_SUCCESS, GET_FB_ALBUM_FAILURE,
  GET_FB_PICTURE_REQUEST, GET_FB_PICTURE_SUCCESS, GET_FB_PICTURE_FAILURE,
  SYNCH_FB_PICTURE_REQUEST, SYNCH_FB_PICTURE_SUCCESS, SYNCH_FB_PICTURE_FAILURE,
  ADD_FAVORITE_REQUEST, ADD_FAVORITE_SUCCESS, ADD_FAVORITE_FAILURE,
  DELETE_FAVORITE_REQUEST, DELETE_FAVORITE_SUCCESS, DELETE_FAVORITE_FAILURE,
  GET_FAVORITE_REQUEST, GET_FAVORITE_SUCCESS, GET_FAVORITE_FAILURE,
  UNBLOCK_USER_REQUEST, UNBLOCK_USER_SUCCESS, UNBLOCK_USER_FAILURE,
  BLOCK_USER_REQUEST, BLOCK_USER_SUCCESS, BLOCK_USER_FAILURE, PEOPLE_LIKE_ME_REAL_TIME,
  GET_BLOCKED_USERS_REQUEST, GET_BLOCKED_USERS_SUCCESS, GET_BLOCKED_USERS_FAILURE,
  UPDATE_USER_PROFILE_STATE, BLOCK_NOTIFICATION, UNBLOCK_NOTIFICATION,
  GET_MATCHED_USERS_REQUEST, GET_MATCHED_USERS_SUCCESS, GET_MATCHED_USERS_FAILURE,
  INCREASE_VISITANT_NUMBER, UPDATE_MATCHED_NOTIFICATION,
  CLEAR_NOTIFICATION_SUCCESS, CLEAR_NOTIFICATION_REQUEST, CLEAR_NOTIFICATION_FAILURE,
  SHOW_CHAT_NOTIFICATION_SUCCESS, UPDATE_FIRST_LIKE_STATUS,
  INCREASE_NOTIFICATION_NUMBER,
  SET_AVATAR_REQUEST, SET_AVATAR_SUCCESS, SET_AVATAR_FAILURE, VISITANT_REAL_TIME,
  DECREASE_NOTIFICATION_NUMBER, UPLOADED_VERIFY_IMAGE,
  UPDATE_PAYMENT_PACKAGE_REQUEST, UPDATE_PAYMENT_PACKAGE_SUCCESS, UPDATE_PAYMENT_PACKAGE_FAILURE,
  UPDATE_LOCATION_REQUEST, UPDATE_LOCATION_SUCCESS, UPDATE_LOCATION_FAILURE, UPDATE_ASKED_LOCATION_STATUS,
  UPDATE_LIKE_STYLE, UPDATE_LIKED_MY_PHOTO_MODAL, UPDATE_VOTED_USERS,
  ACTIVE_BOOST_RANK_REQUEST, ACTIVE_BOOST_RANK_SUCCESS, ACTIVE_BOOST_RANK_FAILURE,
  GET_WHO_ADD_ME_TO_FAVOURITE_REQUEST, GET_WHO_ADD_ME_TO_FAVOURITE_SUCCESS, GET_WHO_ADD_ME_TO_FAVOURITE_FAILURE,
  GET_CHAT_SUGGESTED_USER_REQUEST, GET_CHAT_SUGGESTED_USER_SUCCESS, GET_CHAT_SUGGESTED_USER_FAILURE,
  IGNORE_USER_REQUEST, IGNORE_USER_SUCCESS, IGNORE_USER_FAILURE, SHOW_CHAT_SUGGESTED_USER,
  UPDATE_USER_PERMISSION_REQUEST, UPDATE_USER_PERMISSION_SUCCESS, UPDATE_USER_PERMISSION_FAILURE,
  START_CHAT, UPDATE_VERIFICATION_STATUS, INCREASE_CURRENT_COIN,
  GET_LUCKY_SPINNER_GIFT_REQUEST, GET_LUCKY_SPINNER_GIFT_SUCCESS, GET_LUCKY_SPINNER_GIFT_FAILURE,
  GET_USER_LICENCE_REQUEST, GET_USER_LICENCE_SUCCESS, GET_USER_LICENCE_FAILURE
} from 'constants/userProfile'
import { API_GET, API_POST, API_DELETE } from 'constants/Enviroment';

export function updateVerifyImageStatus(status) {
  return {
    type: UPLOADED_VERIFY_IMAGE,
    status
  }
}
// get current logged in user
export function getCurrentUserRequest() {
  return {
    type: GET_CURRENT_USER_REQUEST,
  }
}

export function getCurrentUserFailure() {
  return {
    type: GET_CURRENT_USER_FAILURE,
  }
}

function callApiGetCurrentUser() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'users',
      params: '',
      authenticated: true,
      types: [
        GET_CURRENT_USER_REQUEST,
        GET_CURRENT_USER_SUCCESS,
        GET_CURRENT_USER_FAILURE,
      ],
    },
  }
}

const getCurrentUser = debounce(callApiGetCurrentUser, 100, true);
export {getCurrentUser};

// get user profile by identifier
export function getUserProfileRequest() {
  return {
    type: GET_USER_PROFILE_REQUEST,
  }
}

export function getUserProfile(identifier) {
  return {
    [CALL_API]: {
      endpoint: 'users',
      method: API_GET,
      params: {
        user_identifier: identifier,
      },
      authenticated: true,
      types: [
        GET_USER_PROFILE_REQUEST,
        GET_USER_PROFILE_SUCCESS,
        GET_USER_PROFILE_FAILURE,
      ],
    },
  }
}

export function getUserProfileFailure() {
  return {
    type: GET_USER_PROFILE_FAILURE,
  }
}

export function sendLikeRequest() {
  return {
    type: SEND_LIKE_REQUEST,
  }
}

export function sendLike(identifier, likeMessage, pageSource, likeSuccessNotification) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'likes',
      params: {
        user_identifier: identifier,
        message: likeMessage,
        page_source: pageSource,
      },
      authenticated: true,
      types: [SEND_LIKE_REQUEST, SEND_LIKE_SUCCESS, SEND_LIKE_FAILURE],
      extentions: {
        likeSuccessNotification: likeSuccessNotification,
        page_source: pageSource,
      },
    },
  }
}

export function sendLikeFailure() {
  return {
    type: SEND_LIKE_FAILURE,
    show_error: false,
  }
}

// ------------------- Get ice breaking users -------------------- //
export function getChatSuggestedUsersRequest() {
  return {
    type: GET_CHAT_SUGGESTED_USER_REQUEST,
  }
}

export function getChatSuggestedUsers() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'chat_suggested_users',
      params: '',
      authenticated: true,
      types: [
        GET_CHAT_SUGGESTED_USER_REQUEST,
        GET_CHAT_SUGGESTED_USER_SUCCESS,
        GET_CHAT_SUGGESTED_USER_FAILURE,
      ],
    },
  }
}

export function showChatSuggestedUser() {
  return {
    type: SHOW_CHAT_SUGGESTED_USER,
  }
}

// ------------------------------------------------------------ //

export function updateUserProfileRequest() {
  return {
    type: UPDATE_USER_PROFILE_REQUEST,
  }
}

// need tobe refactor the following function to more flexible
export function updateUserProfile(params) {
  // update single field, this might refactor in the future to adapt multiple fields
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'users',
      params,
      authenticated: true,
      types: [
        UPDATE_USER_PROFILE_REQUEST,
        UPDATE_USER_PROFILE_SUCCESS,
        UPDATE_USER_PROFILE_FAILURE,
      ],
    },
  }
}

export function updateUserProfileFailure(msg = null) {
  return {
    type: UPDATE_USER_PROFILE_FAILURE,
    msg,
  }
}

// --------------------------- Delete user pictures --------------------------- //

export function deleteUserPictureRequest() {
  return {
    type: DELETE_USER_PICTURE_REQUEST,
  }
}

export function deleteUserPicture(pictureType, pictureId) {
  return {
    [CALL_API]: {
      method: API_DELETE,
      endpoint: 'pictures',
      params: {
        picture_type: pictureType.toString(),
        picture_identifier: pictureId.toString(),
      },
      authenticated: true,
      types: [
        DELETE_USER_PICTURE_REQUEST,
        DELETE_USER_PICTURE_SUCCESS,
        DELETE_USER_PICTURE_FAILURE,
      ],
      extentions: pictureId,
      mode: 1,
    },
  }
}

export function deleteUserPictureFailure(msg = null) {
  return {
    type: DELETE_USER_PICTURE_FAILURE,
    msg,
  }
}

// ------------------- Get recommendation self introduction -------------------- //
export function getSelfIntroductionRequest() {
  return {
    type: GET_SELF_INTRODUCTION_REQUEST,
  }
}

export function getSelfIntroduction() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'self_introductions',
      params: '',
      authenticated: true,
      types: [
        GET_SELF_INTRODUCTION_REQUEST,
        GET_SELF_INTRODUCTION_SUCCESS,
        GET_SELF_INTRODUCTION_FAILURE,
      ],
    },
  }
}

export function getSelfIntroductionFailure() {
  return {
    type: GET_SELF_INTRODUCTION_REQUEST,
  }
}

// ------------------- Validate image for uploading -------------------- //
export function validateImageRequest(pictureType = 0) {
  return {
    type: UPLOAD_IMAGE_VALIDATE_REQUEST,
    pictureType,
  }
}

export function validateImage(pictureType, imageName, imageSize, ignoreMsg = false) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'upload_image',
      params: {
        'photo[picture_type]': pictureType.toString(),
        'photo[image_name]': imageName,
        'photo[image_size]': imageSize.toString(),
      },
      authenticated: true,
      types: [
        UPLOAD_IMAGE_VALIDATE_REQUEST,
        UPLOAD_IMAGE_VALIDATE_SUCCESS,
        UPLOAD_IMAGE_VALIDATE_FAILURE,
      ],
      extentions: { ignoreMsg, pictureType },
    },
  }
}

export function validateImageFailure(pictureType = 0) {
  return {
    type: UPLOAD_IMAGE_VALIDATE_FAILURE,
    pictureType,
  }
}

// ------------------- Upload image --------------------------------------------- //
export function uploadImageRequest(pictureType = 0) {
  return {
    type: UPLOAD_IMAGE_REQUEST,
    pictureType,
  }
}

export function uploadImage(sessionId, imageData, pictureType, croppingRect) {
  const base64 = ';base64,'
  const startIndex = imageData.indexOf(base64) === -1
    ? 0
    : imageData.indexOf(base64) + base64.length
  const data = imageData.substring(startIndex)

  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'upload_image',
      params: {
        'photo[session_id]': sessionId,
        'photo[image_data]': data,
        'photo[crop][crop_x]': croppingRect.x,
        'photo[crop][crop_y]': croppingRect.y,
        'photo[crop][crop_w]': croppingRect.w,
        'photo[crop][crop_h]': croppingRect.h,
      },
      authenticated: true,
      types: [
        UPLOAD_IMAGE_REQUEST,
        UPLOAD_IMAGE_SUCCESS,
        UPLOAD_IMAGE_FAILURE,
      ],
      extentions: pictureType,
    },
  }
}

export function uploadImageFailure(pictureType = 0) {
  return {
    type: UPLOAD_IMAGE_FAILURE,
    pictureType,
  }
}

// ------------------- Get face book album -------------------------------------- //
export function getFacebookAlbumsRequest() {
  return {
    type: GET_FB_ALBUM_REQUEST,
  }
}

export function getFacebookAlbums() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'facebook_albums',
      params: {},
      authenticated: true,
      types: [
        GET_FB_ALBUM_REQUEST,
        GET_FB_ALBUM_SUCCESS,
        GET_FB_ALBUM_FAILURE,
      ],
    },
  }
}

export function getFacebookAlbumsFailure() {
  return {
    type: GET_FB_ALBUM_REQUEST,
  }
}

// ------------------- Get face book pictures -------------------------------------- //
export function getFacebookPicturesRequest() {
  return {
    type: GET_FB_PICTURE_REQUEST,
  }
}

export function getFacebookPictures(album_id) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'facebook_pictures',
      params: { album_id },
      authenticated: true,
      types: [
        GET_FB_PICTURE_REQUEST,
        GET_FB_PICTURE_SUCCESS,
        GET_FB_PICTURE_FAILURE,
      ],
    },
  }
}

export function getFacebookPicturesFailure() {
  return {
    type: GET_FB_PICTURE_REQUEST,
  }
}

// ------------------- Synchronize face book pictures --------------------------- //
export function synchFacebookPicturesRequest(pictureType = 0) {
  return {
    type: SYNCH_FB_PICTURE_REQUEST,
    pictureType,
  }
}

export function synchFacebookPictures(picture_urls, pictureType, croppingRect) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'facebook_pictures',
      params: {
        'picture[picture_urls][]': picture_urls,
        'picture[picture_type]': pictureType,
        'picture[crop][crop_x]': croppingRect.x,
        'picture[crop][crop_y]': croppingRect.y,
        'picture[crop][crop_w]': croppingRect.w,
        'picture[crop][crop_h]': croppingRect.h,

      },
      authenticated: true,
      types: [
        SYNCH_FB_PICTURE_REQUEST,
        SYNCH_FB_PICTURE_SUCCESS,
        SYNCH_FB_PICTURE_FAILURE,
      ],
      extentions: pictureType,
    },
  }
}

export function synchFacebookPicturesFailure(pictureType = 0) {
  return {
    type: SYNCH_FB_PICTURE_REQUEST,
    pictureType,
  }
}

// --------------------------- Add user to favorite lists --------------------------- //

export function addFavoriteRequest() {
  return {
    type: ADD_FAVORITE_REQUEST,
  }
}

export function addFavorite(identifier) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'user_favorites',
      params: {
        user_identifier: identifier,
      },
      authenticated: true,
      types: [
        ADD_FAVORITE_REQUEST,
        ADD_FAVORITE_SUCCESS,
        ADD_FAVORITE_FAILURE,
      ],
      extentions: identifier,
    },
  }
}

export function addFavoriteFailure(msg = null) {
  return {
    type: ADD_FAVORITE_FAILURE,
    msg,
  }
}

// --------------------------- Delete user from favorite lists --------------------------- //

export function deleteFavoriteRequest() {
  return {
    type: DELETE_FAVORITE_REQUEST,
  }
}

export function deleteFavorite(identifier) {
  return {
    [CALL_API]: {
      method: API_DELETE,
      endpoint: 'user_favorites',
      params: { user_identifier: identifier },
      authenticated: true,
      types: [
        DELETE_FAVORITE_REQUEST,
        DELETE_FAVORITE_SUCCESS,
        DELETE_FAVORITE_FAILURE,
      ],
      extentions: identifier,
    },
  }
}

export function deleteFavoriteFailure(msg = null) {
  return {
    type: DELETE_FAVORITE_FAILURE,
    msg,
  }
}

// --------------------------- Get user from favorite lists --------------------------- //

export function getFavoriteRequest() {
  return {
    type: GET_FAVORITE_REQUEST,
  }
}

export function getFavorite(identifier) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'user_favorites',
      params: '',
      authenticated: true,
      types: [
        GET_FAVORITE_REQUEST,
        GET_FAVORITE_SUCCESS,
        GET_FAVORITE_FAILURE,
      ],
      extentions: identifier,
    },
  }
}

export function getFavoriteFailure(msg = null) {
  return {
    type: GET_FAVORITE_FAILURE,
    msg,
  }
}

// --------------- Block or Mute User ------------------------------------------- //
export function blockUserRequest() {
  return {
    type: BLOCK_USER_REQUEST,
  }
}

/**
*block_type: 1 block user, 2: mute user
**/
export function blockUser(identifier, block_type) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'block_users',
      params: {
        block_type,
        user_identifier: identifier,
      },
      authenticated: true,
      types: [
        BLOCK_USER_REQUEST,
        BLOCK_USER_SUCCESS,
        BLOCK_USER_FAILURE,
      ],
    },
  }
}

export function blockUserFailure(msg = null) {
  return {
    type: BLOCK_USER_FAILURE,
    msg,
  }
}

export function unBlockUserRequest() {
  return {
    type: UNBLOCK_USER_REQUEST,
  }
}

/**
*block_type: 1 block user, 2: mute user
**/
export function unBlockUser(identifier, block_type) {
  return {
    [CALL_API]: {
      method: API_DELETE,
      endpoint: 'block_users',
      params: {
        block_type,
        user_identifier: identifier,
      },
      authenticated: true,
      types: [
        UNBLOCK_USER_REQUEST,
        UNBLOCK_USER_SUCCESS,
        UNBLOCK_USER_FAILURE,
      ],
    },
  }
}

export function unBlockUserFailure(msg = null) {
  return {
    type: UNBLOCK_USER_FAILURE,
    msg,
  }
}

export function getBlockedUsersRequest() {
  return {
    type: GET_BLOCKED_USERS_REQUEST,
  }
}

/**
*block_type: 1 block user, 2: mute user
**/
export function getBlockedUsers(block_type) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'block_users',
      params: { block_type },
      authenticated: true,
      types: [
        GET_BLOCKED_USERS_REQUEST,
        GET_BLOCKED_USERS_SUCCESS,
        GET_BLOCKED_USERS_FAILURE,
      ],
      extentions: block_type,
    },
  }
}

export function getBlockedUsersFailure(msg = null) {
  return {
    type: GET_BLOCKED_USERS_FAILURE,
    msg,
  }
}

// ===============Update user profile state (block_type) ==============
export function updateReducerUserProfileState(page, identifier, key, value) {
  return {
    type: UPDATE_USER_PROFILE_STATE,
    page,
    identifier,
    key,
    value,
  }
}

export function blockNotificationByIdentifier(identifier) {
  return {
    type: BLOCK_NOTIFICATION,
    identifier,
  }
}

export function unBlockNotificationByIdentifier(identifier) {
  return {
    type: UNBLOCK_NOTIFICATION,
    identifier,
  }
}

// ------------------- Get people matched me -------------------- //
export function getPeopleMatchedMeRequest() {
  return {
    type: GET_MATCHED_USERS_REQUEST,
  }
}

function callApiGetPeopleMatchedMe() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'matched_users',
      params: '',
      authenticated: true,
      types: [
        GET_MATCHED_USERS_REQUEST,
        GET_MATCHED_USERS_SUCCESS,
        GET_MATCHED_USERS_FAILURE,
      ],
    },
  }
}

const getPeopleMatchedMe = debounce(callApiGetPeopleMatchedMe, 500, true);
export {getPeopleMatchedMe};

export function getPeopleMatchedMeFailure() {
  return {
    type: GET_MATCHED_USERS_REQUEST,
  }
}

// update notification for matches list
export function updateNotificationMessageForMatches(notifications) {
  return {
    type: UPDATE_MATCHED_NOTIFICATION,
    notifications
  }
}

export function reduceNotificationNumber(identifier, number_message) {
  return {
    type: DECREASE_NOTIFICATION_NUMBER,
    identifier,
    number_message
  }
}

// ------------------------------------------------------------ //
export function increaseVisitantNumber() {
  return {
    type: INCREASE_VISITANT_NUMBER,
  }
}

export function visitantRealtime(identifier) {
  return {
    type: VISITANT_REAL_TIME,
    identifier,
  }
}

export function peopleLikedMeRealtime(identifier) {
  return {
    type: PEOPLE_LIKE_ME_REAL_TIME,
    identifier,
  }
}

export function clearNotification(type = 1) {
  return {
    [CALL_API]: {
      method: API_DELETE,
      endpoint: 'notifications',
      params: { notification_type: type },
      authenticated: true,
      types: [
        CLEAR_NOTIFICATION_REQUEST,
        CLEAR_NOTIFICATION_SUCCESS,
        CLEAR_NOTIFICATION_FAILURE,
      ],
      extentions: { type },
    },
  }
}

export function increaseNotificationNumber(notification_type, reset, notification_num = 0) {
  return {
    type: INCREASE_NOTIFICATION_NUMBER,
    notification_type,
    reset,
    notification_num, //this variable use for notification on chat app
  }
}

export function setAvatarRequest() {
  return {
    type: SET_AVATAR_REQUEST,
  }
}

export function setAvatar(pictureId, cropData) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'avatars',
      params: {
        'picture[other_picture_id]': pictureId.toString(),
        'picture[crop][crop_x]': cropData.x,
        'picture[crop][crop_y]': cropData.y,
        'picture[crop][crop_w]': cropData.w,
        'picture[crop][crop_h]': cropData.h,
      },
      authenticated: true,
      types: [
        SET_AVATAR_REQUEST,
        SET_AVATAR_SUCCESS,
        SET_AVATAR_FAILURE,
      ],
      extentions: pictureId,
    },
  }
}

export function setAvatarFailure(msg = null) {
  return {
    type: SET_AVATAR_FAILURE,
    msg,
  }
}

export function updatePaymentPackageRequest() {
  return {
    type: UPDATE_PAYMENT_PACKAGE_REQUEST,
  }
}
export function updatePaymentPackage(pack) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'payment',
      params: {
        payment_package_type: pack
      },
      authenticated: true,
      types: [
        UPDATE_PAYMENT_PACKAGE_REQUEST,
        UPDATE_PAYMENT_PACKAGE_SUCCESS,
        UPDATE_PAYMENT_PACKAGE_FAILURE,
      ],
      extentions: pack
    },
  }
}

export function updatePaymentPackageFailure(msg = null) {
  return {
    type: UPDATE_PAYMENT_PACKAGE_FAILURE,
    msg,
  }
}

export function updateLocationRequest() {
  return {
    type: UPDATE_LOCATION_REQUEST,
  }
}
export function updateLocation(long, lat) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'update_geo',
      params: {
        geo_long: long,
        geo_lat: lat,
      },
      authenticated: true,
      types: [
        UPDATE_LOCATION_REQUEST,
        UPDATE_LOCATION_SUCCESS,
        UPDATE_LOCATION_FAILURE,
      ],
    },
  }
}

export function updateLocationFailure(msg = null) {
  return {
    type: UPDATE_LOCATION_FAILURE,
    msg,
  }
}

export function updateAskedLocationStatus(status) {
  return {
    type: UPDATE_ASKED_LOCATION_STATUS,
    status,
  }
}

export function updateLikeStyle(style) {
  return {
    type: UPDATE_LIKE_STYLE,
    style,
  }
}

export function updateLikedMyPhotoModal(modalInfo) {
  return {
    type: UPDATE_LIKED_MY_PHOTO_MODAL,
    modalInfo,
  }
}

export function updateFirstLikeStatus(status) { //update in the state
  return {
    type: UPDATE_FIRST_LIKE_STATUS,
    status,
  }
}

export function updateFirstLikeStatusViaApi() { // update via api
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'first_like',
      authenticated: true,
      types: [
        'UPDATE_FIRST_LIKE_STATUS_API',
        'UPDATE_FIRST_LIKE_STATUS_API',
        'UPDATE_FIRST_LIKE_STATUS_API',
      ],
    },
  }
}

/**
* vote_type: vote: 1, unvote: 2
**/
export function updateVotedUser(picture_id, vote_user_id, vote_type) {
  return {
    type: UPDATE_VOTED_USERS,
    picture_id,
    vote_user_id,
    vote_type
  }
}

export function activeBoostRankRequest() {
  return {
    type: ACTIVE_BOOST_RANK_REQUEST,
  }
}

export function activeBoostRank() {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'boost_in_rank',
      params: '',
      authenticated: true,
      types: [
        ACTIVE_BOOST_RANK_REQUEST,
        ACTIVE_BOOST_RANK_SUCCESS,
        ACTIVE_BOOST_RANK_FAILURE,
      ],
    },
  }
}
export function activeBoostRankFailure() {
  return {
    type: ACTIVE_BOOST_RANK_FAILURE,
  }
}

export function getWhoAddMeToFavourireRequest() {
  return {
    type: GET_WHO_ADD_ME_TO_FAVOURITE_REQUEST,
  }
}

export function getWhoAddMeToFavourire() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'people_added_favorites',
      params: '',
      authenticated: true,
      types: [
        GET_WHO_ADD_ME_TO_FAVOURITE_REQUEST,
        GET_WHO_ADD_ME_TO_FAVOURITE_SUCCESS,
        GET_WHO_ADD_ME_TO_FAVOURITE_FAILURE,
      ],
    },
  }
}

export function getWhoAddMeToFavourireFailure() {
  return {
    type: GET_WHO_ADD_ME_TO_FAVOURITE_FAILURE,
  }
}

export function startChat() {
  return {
    type: START_CHAT,
  }
}

export function ignoreUser(id, page_source) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'ignore_user',
      params: {
        ignored_user_id: id,
        page_source: page_source,
      },
      authenticated: true,
      types: [
        IGNORE_USER_REQUEST,
        IGNORE_USER_SUCCESS,
        IGNORE_USER_FAILURE,
      ],
    },
  }
}

export function updateUserPermission(name, value) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'update_permission',
      params: {
        permission_name: name,
        permission_value: value,
      },
      authenticated: true,
      types: [
        UPDATE_USER_PERMISSION_REQUEST,
        UPDATE_USER_PERMISSION_SUCCESS,
        UPDATE_USER_PERMISSION_FAILURE,
      ],
      extentions: name,
    },
  }
}

export function updagteVerificationStatus(field_name) {
  return {
    type: UPDATE_VERIFICATION_STATUS,
    field_name
  }
}

export function getLuckySpinnerGift(){
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'user_spinner_gifts',
      params: '',
      authenticated: true,
      types: [
        GET_LUCKY_SPINNER_GIFT_REQUEST,
        GET_LUCKY_SPINNER_GIFT_SUCCESS,
        GET_LUCKY_SPINNER_GIFT_FAILURE
      ],
    },
  }
}

export function increaseCurrentCoin(coins) {
  return {
    type: INCREASE_CURRENT_COIN,
    coins
  }
}

export function getUserLicence() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'user_license',
      params: '',
      authenticated: true,
      types: [GET_USER_LICENCE_REQUEST, GET_USER_LICENCE_SUCCESS, GET_USER_LICENCE_FAILURE],
    },
  };
}