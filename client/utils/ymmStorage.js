import cookie from 'react-cookie';

// This function to concat name with env to prevent confused name for test, staging
function getNewName(name) {
  const new_name = process.env.NODE_ENV !== 'production' ? `${process.env.NODE_ENV}_${name}` : name;
  return new_name;
}

export function setItem(name, value, expire_time = null) {
  // concat env & name to prevent confusion between root domain and sub domain when test
  const new_name = getNewName(name);
  let expired_time = expire_time ? expire_time : null;
  if (!expired_time) {
    expired_time = new Date();
    expired_time.setDate(expired_time.getDate() + 7);
  }
  cookie.save(new_name, value, { expires: expired_time, domain: process.env.DOMAIN, path: '/' });
}

export function getItem(name) {
  const new_name = getNewName(name);
  return cookie.load(new_name);
}

export function removeItem(name) {
  const new_name = getNewName(name);
  cookie.remove(new_name, { domain: process.env.DOMAIN, path: '/' });
}

export function clear() {
  // should refactoring this function
  const new_name = getNewName('');
  cookie.remove(`${new_name}ymm_token`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}fb_token`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}google_token`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}ymm_last_active_time`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}viewMode`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}showed_remind_intro`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}previous_page`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}api_key`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}current_page`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}previous_url`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}read_chat_notification`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}number_uploaded_verify_images`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}allowed`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}askPushNotiViewAdminMessage`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}no_need_remind_random_match`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}Payment_Intention`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}require_payment_url`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}super_like_name`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}name_welcome`, { domain: process.env.DOMAIN, path: '/' });
  cookie.remove(`${new_name}skipShowLikeFanpage`, { domain: process.env.DOMAIN, path: '/' });
}
