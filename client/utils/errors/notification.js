import { NotificationManager } from 'components/commons/Notifications';
import { browserHistory } from 'react-router';
import $ from 'jquery';
import 'toastr/build/toastr.css';
import {
  NOTIFICATION_INFOR, NOTIFICATION_SUCCESS, NOTIFICATION_WARNING,
  NOTIFICATION_ERROR, NOTIFICATION_NOT_ENOUGH_LIKE, RECORD_NOT_FOUND, USER_NOT_FOUND, CDN_URL
} from 'constants/Enviroment';
import { API_EXECUTION_ERROR_NETWORK_MSG, DEFAULT_NOTI_LABEL, DEFAULT_ERROR_LABEL } from 'constants/TextDefinition';
import heartIcon from 'public/images/heartbreak-white.png';

export function sendNotification(type, label, msgParam, timeoutValue = 5000) {
  if (msgParam === RECORD_NOT_FOUND || (msgParam && (msgParam.substring(0, 18) === USER_NOT_FOUND))) {
    return
  }
  let msg = msgParam
  if (msgParam === '') {
    msg = API_EXECUTION_ERROR_NETWORK_MSG
  }

  switch (type) {
    case NOTIFICATION_INFOR:
      NotificationManager.info(msg, label, timeoutValue);
      break

    case NOTIFICATION_SUCCESS:
      NotificationManager.success(msg, label, timeoutValue);
      break

    case NOTIFICATION_WARNING:
      NotificationManager.warning(msg, label, timeoutValue)
      break

    case NOTIFICATION_ERROR:
      NotificationManager.error(msg, label, timeoutValue)
      break

    case NOTIFICATION_NOT_ENOUGH_LIKE:
      const notiNotEnoughLike = $(`<div class="noti noti--bg">
        <div class="noti__body">
          <div class="noti__icon"><img src="${heartIcon}" /></div>
          <div class="noti__content">${msg}</div>
        </div>
      </div>`)

      $('body').append(notiNotEnoughLike)

      setTimeout(function() {
        $(notiNotEnoughLike).remove()
      }, 1000)

      break

    case NOTIFICATION_BEGIN_HAPPY_HOURS:
      const clock_begin_url = `${CDN_URL}/general/clock-begin.png`
      const begin = $(`
        <div class="noti noti--bg-blue mbm">
          <div class="noti__body">
            <div class="noti__header mbm">
              <img src= ${clock_begin_url} alt="coin_reward_icon+coin.png" />
            </div>
            <div class="noti__content txt-bold">Giờ vui vẻ <br /> bắt đầu!</div>
          </div>
        </div>
      `)

      $('body').append(begin)

      setTimeout(function() {
        $(begin).remove()
      }, 1000)

      break

    case NOTIFICATION_END_HAPPY_HOURS:
      const clock_end_url = `${CDN_URL}/general/clock-end.png`
      const end = $(`
        <div class="noti noti--bg-red mbm">
          <div class="noti__body">
            <div class="noti__header mbm">
              <img src= ${clock_end_url} alt="coin_reward_icon+coin.png" />
            </div>
            <div class="noti__content txt-bold">Giờ vui vẻ <br /> kết thúc!</div>
          </div>
        </div>
      `)

      $('body').append(end)

      setTimeout(function() {
        $(end).remove()
      }, 1000)

      break

    default:
      break
  }
}

export default function notification(type, msg, timeout) {
  if (type === NOTIFICATION_WARNING || type === NOTIFICATION_ERROR) {
    sendNotification(type, DEFAULT_ERROR_LABEL, msg, timeout)
  } else {
    sendNotification(type, DEFAULT_NOTI_LABEL, msg, timeout)
  }
}

export function errorNotiWithLabel(label, msg, timeout) {
  sendNotification(NOTIFICATION_ERROR, label, msg, timeout)
}

export function sendClickableNotification(type, label, msg, url, timeout, is_blank = false, callback) {
  switch (type) {
    case NOTIFICATION_INFOR:
      NotificationManager.info(msg, label, timeout, () => {
        if(callback) {
          callback();
        }

        if (url) {
          browserHistory.push(url);
        }
      });
      break;

    case NOTIFICATION_SUCCESS:
      NotificationManager.success(msg, label, timeout, () => {
        if(callback) {
          callback();
        }

        if (url) {
          is_blank ? window.open(url) : browserHistory.push(url);
        }
      });
      break;

    case NOTIFICATION_WARNING:
      NotificationManager.warning(msg, label, timeout, () => {
        if(callback) {
          callback();
        }

        if (url) {
          browserHistory.push(url);
        }
      });
      break;

    case NOTIFICATION_ERROR:
      NotificationManager.error(msg, label, timeout, () => {
        if(callback) {
          callback();
        }

        if (url) {
          browserHistory.push(url);
        }
      });
      break;

    default:
      break;
  }
}