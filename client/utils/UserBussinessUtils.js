import {
  ACTIVE_USER, BLOCK_TYPE_BLOCKED, GHOST_USER,
} from '../constants/userProfile'

export function isNormalOtherUser(user){
  if(!user) {
    return false;
  }
  if (user.length === 0){
    return false;
  }
  // User is people who blocks me
  if (user.block_me_type !== null && user.block_me_type === BLOCK_TYPE_BLOCKED){
    return false;
  }
  // User is not an active user
  if (user.user_status !== null &&
    user.user_status !== ACTIVE_USER &&
    user.user_status !== GHOST_USER){
    return false;
  }

  return true;
}

export function getAgeGroup(age) {
  if (age < 23) {
    return 'group_18_22';
  } else if (age < 27) {
    return 'group_23_26';
  } else if (age < 36) {
    return 'group_27_35';
  }
  return 'group_older_than_35';
}
