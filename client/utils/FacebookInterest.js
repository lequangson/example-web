/**
* get same facebook interest between current user with other user
* @current_user_interest array of object: ex [{id: 1234, name: 'xxx'}]
* @ other_user_interest string: ex '1234, 343434, 434343'
**/
export function getSameInterest(current_user_interest = [], other_user_interest = '') {
  if (!current_user_interest.length || !other_user_interest.length) {
    return [];
  }
  let array_other_user_interest = '';
  if (Array.isArray(other_user_interest)) {
    array_other_user_interest = other_user_interest.reduce((total, curr) =>
      `${typeof total !== 'object' ? total : ''} ${curr.id.toString()}`
  ).trim();
} else { array_other_user_interest = other_user_interest.split(','); }
  return current_user_interest.filter(user_interest => array_other_user_interest.indexOf(user_interest.id.toString()) > -1)
}
//  If the name of interest/ group is more than 8 characters, it will be cut down to 8 first character +...
export function getShortName(name) {
  if (typeof name === 'undefined' || name.length <= 8) {
    return name || '';
  }
  return `${name.substr(0, 8)}...`
}