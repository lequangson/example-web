/**
 * How to use:
 * import GoogleOptimize from "./path/to/google_optimize"
 * @GoogleOptimize()
 * componentDidUpdate(){...}
 * 
 * Or if you want to call manually,
 * import { activateGoogleOptimize } from "./path/to/google_optimize"
 * activateGoogleOptimize();
 */

import debounce from "debounce";

function activateGoogleOptimize(){
    window.dataLayer.push({'event': 'optimize.activate'});
}
activateGoogleOptimize = debounce(activateGoogleOptimize, 50);


function appendGOptActivation( target, name, descriptor ){
    if( descriptor.value ) { // when method is given
        const original = descriptor.value;
        const new_func = function(...args) {
            const retval = original.bind(this)(...args);
            activateGoogleOptimize();
            return retval;
        };
        descriptor.value = new_func;
        return descriptor;
    }else{
        console.warn("warning: @GoogleOptimize can be applied only to a method");
        return; // do nothing
    }
}
appendGOptActivation.activateGoogleOptimize = activateGoogleOptimize;


export default appendGOptActivation;
