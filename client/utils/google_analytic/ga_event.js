/* global ga: true */

import {
  GA_ACTION_LOGIN, GA_ACTION_REGISTRATION, GA_ACTION_SAVE_SEARCH_CONDITION, GA_ACTION_SEARCH,
  GA_ACTION_LIKE, GA_ACTION_OPEN_LIKE, GA_ACTION_MATCH, GA_ACTION_SIMPLE_FILTER_CONDITION,
  GA_ACTION_ADD_FAVORITE, GA_ACTION_DELETE_FAVORITE,
  GA_ACTION_EDIT_PROFILE, GA_ACTION_UPLOAD_AVATAR_DEVICE, GA_ACTION_UPLOAD_AVATAR_FACEBOOK,
  GA_ACTION_UPLOAD_OTHER_PICTURES_DEVICE, GA_ACTION_UPLOAD_OTHER_PICTURES_FACEBOOK,
  GA_ACTION_EDIT_PROFILE_FIRST_TIMES, GA_ACTION_DELETE_OTHER_PICTURE,
  GA_ACTION_BLOCK_USER, GA_ACTION_UNBLOCK_USER, GA_ACTION_MUTE_USER, GA_ACTION_UNMUTE_USER, GA_OPEN_LINK_IN_EMAIL,
  GA_ACTION_LIKE_WITH_MESSAGE, GA_ACTION_OPEN_REMIND, GA_ACTION_OPEN_TIPS, GA_ACTION_OPEN_NEXTLINK_TIPS, GA_ACTION_UPDATE_NICKNAME,
  GA_ACTION_VIEW_OTHER_PROFILE, GA_ACTION_LOGIN_FAILURE,
  GA_ACTION_PHOTO_SERVICE, GA_ACTION_WELCOME_PAGE_SKIP, GA_ACTION_SAVE_ATTRACTIVE_USER,
  GA_ACTION_VIEW_ADMIN_MESSAGE, GA_ACTION_PAYMENT_LEARN_MORE, GA_ACTION_PAYMENT_PACKAGE_SUBMIT, GA_ACTION_PAYMENT_PROCESS_PAYMENT,
  GA_ACTION_VERIFY, GA_ACTION_VOTE_PHOTO, GA_ACTION_UNVOTE_PHOTO,
  GA_ACTION_BOOST_RANK_CLICK, GA_ACTION_BOOST_RANK_BUY, GA_ACTION_GET_MORE_COIN,
  GA_ACTION_PAYMENT_CALLBACK_PROCESS_FAILURE, GA_ACTION_PAYMENT_CALLBACK_PROCESS_SUCCESS,
  GA_ACTION_COIN_CLICK, GA_ACTION_COIN_BUY, GA_ACTION_RANDOM_MATCH_CLICK_BANNER, GA_ACTION_RANDOM_MATCH_OPEN_CARD, GA_ACTION_MATCH_QUESTIONS
} from '../../constants/Enviroment'
import { getUserIdFromIdentifier, getUserId } from '../common'

function getGaActionLabel(eventAction) {
  const gaAction = {
    hitType: 'event',
    eventCategory: '',
    eventAction: '',
    eventLabel: '',
  }

  switch (eventAction) {
    case GA_ACTION_LOGIN:
      gaAction.eventCategory = 'Login'
      gaAction.eventAction = 'Login'
      gaAction.eventLabel = 'Login'
      break

    case GA_ACTION_REGISTRATION:
      gaAction.eventCategory = 'Registration'
      gaAction.eventAction = 'Registration'
      gaAction.eventLabel = 'Registration'
      break

    case GA_ACTION_SAVE_SEARCH_CONDITION:
      gaAction.eventCategory = 'Search Condition'
      gaAction.eventAction = 'Save'
      gaAction.eventLabel = 'Save Search Condition'
      break

    case GA_ACTION_SIMPLE_FILTER_CONDITION:
      gaAction.eventCategory = 'Simple Filter'
      gaAction.eventAction = 'Save'
      gaAction.eventLabel = 'Save Simple Filter'
      break

    case GA_ACTION_SEARCH:
      gaAction.eventCategory = 'Search'
      gaAction.eventAction = 'Search'
      gaAction.eventLabel = 'Search'
      break

    case GA_ACTION_LIKE:
      gaAction.eventCategory = 'Like'
      gaAction.eventAction = 'Like'
      gaAction.eventLabel = 'Like'
      break

    case GA_ACTION_OPEN_LIKE:
      gaAction.eventCategory = 'Like'
      gaAction.eventAction = 'Open'
      gaAction.eventLabel = 'Open Like'
      break

    case GA_ACTION_MATCH:
      gaAction.eventCategory = 'Match'
      gaAction.eventAction = 'Send'
      gaAction.eventLabel = 'Match Made'
      break

    case GA_ACTION_ADD_FAVORITE:
      gaAction.eventCategory = 'Favorite'
      gaAction.eventAction = 'Add'
      gaAction.eventLabel = 'Add Favorite'
      break

    case GA_ACTION_DELETE_FAVORITE:
      gaAction.eventCategory = 'Favorite'
      gaAction.eventAction = 'Delete'
      gaAction.eventLabel = 'Delete Favorite'
      break

    case GA_ACTION_EDIT_PROFILE:
      gaAction.eventCategory = 'Profile'
      gaAction.eventAction = 'Edit'
      gaAction.eventLabel = 'Edit Profile'
      break

    case GA_ACTION_UPLOAD_AVATAR_DEVICE:
      gaAction.eventCategory = 'Profile'
      gaAction.eventAction = 'UploadAvatar'
      gaAction.eventLabel = 'Edit Profile'
      break

    case GA_ACTION_UPLOAD_AVATAR_FACEBOOK:
      gaAction.eventCategory = 'Profile'
      gaAction.eventAction = 'SynchFBAvatar'
      gaAction.eventLabel = 'Edit Profile'
      break

    case GA_ACTION_UPLOAD_OTHER_PICTURES_DEVICE:
      gaAction.eventCategory = 'Profile'
      gaAction.eventAction = 'UploadOtherPicture'
      gaAction.eventLabel = 'Edit Profile'
      break

    case GA_ACTION_UPLOAD_OTHER_PICTURES_FACEBOOK:
      gaAction.eventCategory = 'Profile'
      gaAction.eventAction = 'SynchFBOtherPicture'
      gaAction.eventLabel = 'Edit Profile'
      break

    case GA_ACTION_EDIT_PROFILE_FIRST_TIMES:
      gaAction.eventCategory = 'Profile'
      gaAction.eventAction = 'EditFirst'
      gaAction.eventLabel = 'Edit Profile'
      break

    case GA_ACTION_DELETE_OTHER_PICTURE:
      gaAction.eventCategory = 'Profile'
      gaAction.eventAction = 'DeletePicture'
      gaAction.eventLabel = 'Edit Profile'
      break

    case GA_ACTION_BLOCK_USER:
      gaAction.eventCategory = 'Block'
      gaAction.eventAction = 'Block'
      gaAction.eventLabel = 'Block User'
      break

    case GA_ACTION_UNBLOCK_USER:
      gaAction.eventCategory = 'Block'
      gaAction.eventAction = 'Unblock'
      gaAction.eventLabel = 'Unblock User'
      break

    case GA_ACTION_MUTE_USER:
      gaAction.eventCategory = 'Block'
      gaAction.eventAction = 'Mute'
      gaAction.eventLabel = 'Mute User'
      break

    case GA_ACTION_UNMUTE_USER:
      gaAction.eventCategory = 'Block'
      gaAction.eventAction = 'Unmute'
      gaAction.eventLabel = 'Unmute User'
      break

    case GA_OPEN_LINK_IN_EMAIL:
      gaAction.eventCategory = 'Open Link In Email'
      gaAction.eventAction = 'Open'
      gaAction.eventLabel = 'Open Link In Email'
      break

    case GA_ACTION_LIKE_WITH_MESSAGE:
      gaAction.eventCategory = 'Like'
      gaAction.eventAction = 'LikeWithMessage'
      gaAction.eventLabel = 'Like with message'
      break

    case GA_ACTION_OPEN_REMIND:
      gaAction.eventCategory = 'Remind'
      gaAction.eventAction = 'Open'
      gaAction.eventLabel = 'Open Remind Banner'
      break

    case GA_ACTION_OPEN_TIPS:
      gaAction.eventCategory = 'Tips'
      gaAction.eventAction = 'Open'
      gaAction.eventLabel = 'Open Tips Banner'
      break

    case GA_ACTION_OPEN_NEXTLINK_TIPS:
      gaAction.eventCategory = 'Tips'
      gaAction.eventAction = 'OpenNextLink'
      gaAction.eventLabel = 'Open Next Link Tips Banner'
      break

    case GA_ACTION_UPDATE_NICKNAME:
      gaAction.eventCategory = 'Profile'
      gaAction.eventAction = 'Update_NickName'
      gaAction.eventLabel = 'Update Nick Name'
      break

    case GA_ACTION_VIEW_OTHER_PROFILE:
      gaAction.eventCategory = 'Other Profile'
      gaAction.eventAction = 'View'
      gaAction.eventLabel = 'View Other Profile'
      break

    case GA_ACTION_LOGIN_FAILURE:
      gaAction.eventCategory = 'Failure'
      gaAction.eventAction = 'Login'
      gaAction.eventLabel = 'Login Failure'
      break

    case GA_ACTION_PAYMENT_CALLBACK_PROCESS_FAILURE:
      gaAction.eventCategory = 'Failure'
      gaAction.eventAction = 'Payment Callback'
      gaAction.eventLabel = 'Payment Callback Failure'
      break

    case GA_ACTION_PAYMENT_CALLBACK_PROCESS_SUCCESS:
      gaAction.eventCategory = 'Payment'
      gaAction.eventAction = 'Payment Callback'
      gaAction.eventLabel = 'Payment Callback Success'
      break

    case GA_ACTION_PHOTO_SERVICE:
        gaAction.eventCategory = 'Professional Photo Service'
        gaAction.eventAction = 'Click'
        gaAction.eventLabel = 'Professional Photo Service Click'
        break

    case GA_ACTION_WELCOME_PAGE_SKIP:
        gaAction.eventCategory = 'Welcome Page'
        gaAction.eventAction = 'Skip'
        gaAction.eventLabel = 'Welcome Page Skip'
        break

    case GA_ACTION_SAVE_ATTRACTIVE_USER:
        gaAction.eventCategory = 'Welcome Page'
        gaAction.eventAction = 'Save_Attractive'
        gaAction.eventLabel = 'Save attractive user'
        break

    case GA_ACTION_VIEW_ADMIN_MESSAGE:
      gaAction.eventCategory = 'Notification From Admin'
      gaAction.eventAction = 'Click'
      gaAction.eventLabel = 'Notification From Admin Click'
      break

    case GA_ACTION_PAYMENT_LEARN_MORE:
      gaAction.eventCategory = 'Payment Experience'
      gaAction.eventAction = 'LearnMore'
      gaAction.eventLabel = 'Payment Experience Learn More'
      break

    case GA_ACTION_PAYMENT_PACKAGE_SUBMIT:
      gaAction.eventCategory = 'Payment Experience'
      gaAction.eventAction = 'PackageSubmit'
      gaAction.eventLabel = 'Payment Experience Package Submit'
      break

    case GA_ACTION_PAYMENT_PROCESS_PAYMENT:
      gaAction.eventCategory = 'Payment Experience'
      gaAction.eventAction = 'ProcessPayment'
      gaAction.eventLabel = 'Payment Experience Process Payment'
      break

    case GA_ACTION_VERIFY:
      gaAction.eventCategory = 'User verification'
      gaAction.eventAction = 'Click'
      gaAction.eventLabel = 'User verification button click'
      break

    case GA_ACTION_VOTE_PHOTO:
      gaAction.eventCategory = 'Photo'
      gaAction.eventAction = 'Vote'
      gaAction.eventLabel = 'Vote Photo'
      break

    case GA_ACTION_UNVOTE_PHOTO:
      gaAction.eventCategory = 'Photo'
      gaAction.eventAction = 'Unvote'
      gaAction.eventLabel = 'Unvote Photo'
      break

    case GA_ACTION_BOOST_RANK_CLICK:
      gaAction.eventCategory = 'Boost Rank'
      gaAction.eventAction = 'Click'
      gaAction.eventLabel = 'Click To Boost Rank'
      break

    case GA_ACTION_BOOST_RANK_BUY:
      gaAction.eventCategory = 'Coin'
      gaAction.eventAction = 'Buy'
      gaAction.eventLabel = 'Buy Boost Rank'
      break

    case GA_ACTION_GET_MORE_COIN:
      gaAction.eventCategory = 'Coin'
      gaAction.eventAction = 'GetMoreCoin'
      gaAction.eventLabel = 'Get more coin'
      break

    case GA_ACTION_COIN_CLICK:
      gaAction.eventCategory = 'Coin'
      gaAction.eventAction = 'CLick'
      gaAction.eventLabel = 'Click to buy coin'
      break

    case GA_ACTION_COIN_BUY:
      gaAction.eventCategory = 'Coin'
      gaAction.eventAction = 'Buy'
      gaAction.eventLabel = 'Buy coin'
      break

    case GA_ACTION_RANDOM_MATCH_CLICK_BANNER:
      gaAction.eventCategory = 'Random Match'
      gaAction.eventAction = 'Click'
      gaAction.eventLabel = 'Click banner'
      break

    case GA_ACTION_RANDOM_MATCH_OPEN_CARD:
      gaAction.eventCategory = 'Random match'
      gaAction.eventAction = 'Open'
      gaAction.eventLabel = 'Open card'
      break

    case GA_ACTION_MATCH_QUESTIONS:
      gaAction.eventCategory = 'Match Questions'
      gaAction.eventAction = 'Click'
      gaAction.eventLabel = 'Click'
      break

    default:
      break
  }
  return gaAction
}

export function gaProcessAction(eventAction, current_user, ga_track_data) {
  const gaAction = getGaActionLabel(eventAction)
  const options = {
    hitType: gaAction.hitType,
    eventCategory: gaAction.eventCategory,
    eventAction: gaAction.eventAction,
    eventLabel: gaAction.eventLabel,
    dimension1: current_user ? current_user.age : '',
    dimension2: current_user ? current_user.gender || '' : '',
    dimension3: current_user ? (current_user.residence && current_user.residence.value) ? current_user.residence.value : '' : '',
    dimension4: current_user ? (current_user.birth_place && current_user.birth_place.value) ? current_user.birth_place.value : '' : '',
    // Custom data
    dimension5: ga_track_data.partner_residence || '',
    dimension6: ga_track_data.partner_birth_place || '',
    dimension7: ga_track_data.view_mode || '',
    dimension8: ga_track_data.page_source || '',
    dimension9: ga_track_data.search_condition
      ? JSON.stringify(ga_track_data.search_condition)
      : '',
    dimension10: ga_track_data.partner_age || null,
    dimension11: current_user ? getUserId(current_user.identifier) : '',
    dimension12: ga_track_data.partner_identifier
    ? getUserId(ga_track_data.partner_identifier)
    : null,
    dimension13: ga_track_data.message || '',
    dimension14: ga_track_data.extra_information || '',
    dimension15: ga_track_data.extra_information2 || '',
    metric3: ga_track_data.page_number || null,
  }

  // TODO: remove after test ok
  console.log('gaTrackEvent', options)
  //ga('send', options)
  ga('ymm.send', options );
}

/* This function should be handled for event
which should send another google analytic code before sending current GA code */
export function gaBeforeProcessAction(eventAction, current_user, ga_track_data) {
  switch (eventAction) {
    case GA_ACTION_EDIT_PROFILE:
    case GA_ACTION_UPLOAD_AVATAR_DEVICE:
    case GA_ACTION_UPLOAD_AVATAR_FACEBOOK:
    case GA_ACTION_UPLOAD_OTHER_PICTURES_DEVICE:
    case GA_ACTION_UPLOAD_OTHER_PICTURES_FACEBOOK:
      if (!current_user.have_updated_profile) {
        gaProcessAction(
          GA_ACTION_EDIT_PROFILE_FIRST_TIMES,
          current_user,
          ga_track_data
        )
      }

      break

    default:
      break
  }
}

/* This function should be handled for event
which should send another google analytic code after sending current GA code */
// export function gaAfterProcessAction(eventAction, current_user, ga_track_data) {
//
// }

export function gaTrackEvent(eventAction, current_user, ga_track_data) {
/*  if (current_user === null || current_user === undefined || current_user.length === 0) {
    console.log('null user')
    return;
  }*/
  gaBeforeProcessAction(eventAction, current_user, ga_track_data)
  gaProcessAction(eventAction, current_user, ga_track_data)
  // gaAfterProcessAction(eventAction, current_user, ga_track_data)
}

export function sendGoogleAnalytic(eventAction, current_user, ga_track_data) {
  const options = {
    hitType: 'event',
    eventCategory: eventAction.eventCategory,
    eventAction: eventAction.eventAction,
    eventLabel: eventAction.eventLabel,

    dimension1: current_user ? current_user.age : '',
    dimension2: current_user ? current_user.gender || '' : '',
    dimension3: current_user ? (current_user.residence && current_user.residence.value) ? current_user.residence.value : '' : '',
    dimension4: current_user ? (current_user.birth_place && current_user.birth_place.value) ? current_user.birth_place.value : '' : '',
    // Custom data
    dimension5: ga_track_data.partner_residence || '',
    dimension6: ga_track_data.partner_birth_place || '',
    dimension7: ga_track_data.view_mode || '',
    dimension8: ga_track_data.page_source || '',
    dimension9: ga_track_data.search_condition
      ? JSON.stringify(ga_track_data.search_condition)
      : '',
    dimension10: ga_track_data.partner_age || null,
    dimension11: current_user ? getUserId(current_user.identifier) : '',
    dimension12: ga_track_data.partner_identifier
    ? getUserId(ga_track_data.partner_identifier)
    : null,
    dimension13: ga_track_data.message || '',
    dimension14: ga_track_data.extra_information || '',
    dimension15: ga_track_data.extra_information2 || '',
    metric3: ga_track_data.page_number || null,
  }


  console.log('gaTrackEvent', options)

  //ga('send', options)
  ga('ymm.send', options );
}