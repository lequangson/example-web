import { convertUTCToLocalTime } from './common'

export function isUserInBoostInRankPeriod(currentUser) {
  let isInBoostInRank = false;
  if (currentUser.ontop_24h != null && 
    currentUser.ontop_24h.begin_at != null && 
    currentUser.ontop_24h.end_at != null) {
    const begin_at = convertUTCToLocalTime(currentUser.ontop_24h.begin_at)
    const end_at = convertUTCToLocalTime(currentUser.ontop_24h.end_at)
    const now = new Date();
    if (begin_at <= now && end_at >= now) {
      isInBoostInRank = true;
    }
  }
  return isInBoostInRank;
}