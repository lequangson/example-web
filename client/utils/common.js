import moment from 'moment';
import _ from 'lodash';

import {
  BLOCK_USER_BLOCKED_PAGE, BLOCK_USER_MUTED_PAGE,
  SEARCH_PAGE, PEOPLE_LIKED_ME_PAGE, PEOPLE_I_LIKED_PAGE,
  FAVORITE_PAGE, FOOTPRINT_PAGE, REMIND_BANNER, MATCHED_PAGE,
  VOTED_MODAL_PAGE, WHAT_HOT_PAGE, WHO_ADD_ME_TO_FAVOURITE_PAGE,
} from '../constants/Enviroment'
/**
* Because of security reason, user identifier is concated with more string
* @return true if identifier is match otherwise false
**/

export function getUserId(identifier) {
  const userId = getUserIdFromIdentifier(identifier);
  return parseInt(userId)
}

export function getUserIdFromIdentifier(identifier) {
  if (!identifier || identifier.length < 8) {
    return null
  }
  return identifier.substr(6, identifier.length - 8)
}

export  function getIdentifierFromId(id) {
  return moment().format('DDMMYY') + id + _.random(10, 99);
}

export function compareIdentifiers(identifier1, identifier2) {
  if (!identifier1 || !identifier2) {
    return false;
  }
  // remove first 6 character & last 2 characters before compare
  return !getUserIdFromIdentifier(identifier1.toString()).localeCompare(getUserIdFromIdentifier(identifier2.toString()))
}

export function convertUTCToLocalTime(dateTime) {
  if (!dateTime) {
    return null;
  }
  const localTime = new Date(dateTime.replace(/\s/, 'T').concat('Z'))
  return localTime;
}

export function dateTimeToString(dateTime) {
  if (!dateTime) {
    return ''
  }

  const localTime = new Date(dateTime.replace(/\s/, 'T').concat('Z'))
  let day = localTime.getDate().toString()
  let month = (localTime.getMonth() + 1).toString()
  const year = localTime.getFullYear()
  let hour = localTime.getHours().toString()
  let minute = localTime.getMinutes().toString()

  if (day.length < 2) {
    day = `0${day}`
  }

  if (month.length < 2) {
    month = `0${month}`
  }

  if (hour.length < 2) {
    hour = `0${hour}`
  }

  if (minute.length < 2) {
    minute = `0${minute}`
  }

  const date = [day, month, year].join('/')
  const time = [hour, minute].join(':')

  return `${date} ${time}`
}

export function isBlockedUser(
  identifier,
  blocked_users,
  muted_user,
  blocked_notification,
  unblocked_notification
) {
  for (let i = 0; i < unblocked_notification.length; i += 1) {
    if (compareIdentifiers(identifier, unblocked_notification[i])) {
      return false
    }
  }
  for (let j = 0; j < blocked_notification.length; j += 1) {
    if (compareIdentifiers(identifier, blocked_notification[j])) {
      return true
    }
  }
  for (let k = 0; k < muted_user.length; k += 1) {
    if (compareIdentifiers(identifier, muted_user[k].identifier)) {
      return true
    }
  }
  for (let m = 0; m < blocked_users.length; m += 1) {
    if (compareIdentifiers(identifier, blocked_users[m].identifier)) {
      return true
    }
  }

  return false
}
/**
* check if useridentifier is in liked List
  (this identifier might liked current user, or current user liked this identifier)
* @identifier - user identifier
* @people_liked_me_real_time - when user A like user B,
  A's identifier will be written into state of B
**/
export function inLikedList(identifier, people_liked_me, people_liked_me_real_time) {
  for (let i = 0; i < people_liked_me.length; i += 1) {
    if (compareIdentifiers(identifier, people_liked_me[i].identifier)) {
      return true
    }
  }

  for (let m = 0; m < people_liked_me_real_time.length; m += 1) {
    if (compareIdentifiers(identifier, people_liked_me_real_time[m])) {
      return true
    }
  }

  return false
}

export function inVisitList(identifier, visitants, visitant_real_time) {
  for (let i = 0; i < visitants.length; i += 1) {
    if (compareIdentifiers(identifier, visitants[i].identifier)) {
      return true
    }
  }

  for (let m = 0; m < visitant_real_time.length; m += 1) {
    if (compareIdentifiers(identifier, visitant_real_time[m])) {
      return true
    }
  }

  return false
}

export function getUserByIdentifierFromAppState(identifier, props, mode) {
  switch (props.Enviroment.previous_page) {
    case PEOPLE_LIKED_ME_PAGE:
      return props.people_liked_me.users
        ? props.people_liked_me.users.find(userProfile =>
          compareIdentifiers(userProfile.identifier, identifier))
        : [];
    case SEARCH_PAGE:
    case 'SEARCH_PAGE_DEFAULT':
    case 'SEARCH_PAGE_NEW':
    case 'SEARCH_PAGE_ONLINE':
    case 'SEARCH_PAGE_NEARBY':
    case 'SEARCH_PAGE_HARMONY_AGE':
    case 'SEARCH_PAGE_HARMONY_FATE':
    case 'SEARCH_PAGE_HARMONY_HOROSCOPE':
      return (props.search[mode].data)
        ? props.search[mode].data.find(userProfile =>
          compareIdentifiers(userProfile.identifier, identifier))
        : [];

    case FAVORITE_PAGE:
      return props.user_profile.favorite_lists
        ? props.user_profile.favorite_lists.find(userProfile =>
          compareIdentifiers(userProfile.identifier, identifier))
        : []
    case BLOCK_USER_BLOCKED_PAGE:
      return props.user_profile.blocked_users
        ? props.user_profile.blocked_users.find(userProfile =>
          compareIdentifiers(userProfile.identifier, identifier))
        : []

    case BLOCK_USER_MUTED_PAGE:
      return props.user_profile.muted_users
        ? props.user_profile.muted_users.find(userProfile =>
          compareIdentifiers(userProfile.identifier, identifier))
        : []
    case FOOTPRINT_PAGE:
      return props.user_profile.visitants
        ? props.user_profile.visitants.find(userProfile =>
          compareIdentifiers(userProfile.identifier, identifier))
        : []
    case PEOPLE_I_LIKED_PAGE:
      return props.PeopleILiked.data
        ? props.PeopleILiked.data.find(userProfile =>
          compareIdentifiers(userProfile.identifier, identifier))
        : []
    case REMIND_BANNER:
      if (props.Enviroment.remind) {
        return (props.Enviroment.remind.identifier === identifier ? props.Enviroment.remind : false)
      }

    case MATCHED_PAGE:
      return props.user_profile.matched_users
        ? props.user_profile.matched_users.find(userProfile =>
          compareIdentifiers(userProfile.identifier, identifier))
        : []
    case VOTED_MODAL_PAGE:
      return props.photoStream.voted_users
        ? props.photoStream.voted_users.find(userProfile =>
          compareIdentifiers(userProfile.identifier, identifier))
        : [];
    case WHAT_HOT_PAGE:
      return props.WhatHot.users?
        props.WhatHot.users.find(userProfile =>
        compareIdentifiers(userProfile.identifier, identifier))
        : []
    case WHO_ADD_ME_TO_FAVOURITE_PAGE:
      return props.user_profile.who_add_favourite ?
        props.user_profile.who_add_favourite.find(userProfile =>
        compareIdentifiers(userProfile.identifier, identifier))
        : []
    default:
      return [];
  }
}

export function removeLeftUser(list, props) {
  switch (list) {
    case 'people_liked_me':
      for (let i = 0; i < props.people_liked_me.users.length; i += 1) {
        if (props.people_liked_me.users[i].user_status != 1) {
          props.people_liked_me.users.splice(i, 1);
        }
      }
      return props.people_liked_me.users;
    case 'favorite_lists':
      for (let i = 0; i < props.user_profile.favorite_lists.length; i += 1) {
        if (props.user_profile.favorite_lists[i].user_status != 1 || props.user_profile.favorite_lists[i].block_me_type == 1) {
          props.user_profile.favorite_lists.splice(i, 1);
        }
      }
      return props.user_profile.favorite_lists;
    case 'blocked_users':
      for (let i = 0; i < props.user_profile.blocked_users.length; i += 1) {
        if (props.user_profile.blocked_users[i].user_status != 1 || props.user_profile.blocked_users[i].block_me_type == 1) {
          props.user_profile.blocked_users.splice(i, 1);
        }
      }
      return props.user_profile.blocked_users;
    case 'muted_users':
      for (let i = 0; i < props.user_profile.muted_users.length; i += 1) {
        if (props.user_profile.muted_users[i].user_status != 1 || props.user_profile.muted_users[i].block_me_type == 1) {
          props.user_profile.muted_users.splice(i, 1);
        }
      }
      return props.user_profile.muted_users;
    case 'visitants':
      for (let i = 0; i < props.user_profile.visitants.length; i += 1) {
        if (props.user_profile.visitants[i].user_status != 1 || props.user_profile.visitants[i].block_me_type == 1) {
          props.user_profile.visitants.splice(i, 1);
        }
      }
      return props.user_profile.visitants;
    case 'people_i_liked':
      for (let i = 0; i < props.PeopleILiked.data.length; i += 1) {
        if (props.PeopleILiked.data[i].user_status != 1 || props.PeopleILiked.data[i].block_me_type == 1) {
          props.PeopleILiked.data.splice(i, 1);
        }
      }
      return props.PeopleILiked.data;
    case 'matched_list':
      for (let i = 0; i < props.user_profile.matched_users.length; i += 1) {
        if (props.user_profile.matched_users[i].user_status != 1 || props.user_profile.matched_users[i].block_me_type == 1) {
          props.user_profile.matched_users.splice(i, 1);
        }
      }
      return props.user_profile.matched_users;
    case 'whoaddme_list':
      for (let i = 0; i < props.user_profile.who_add_favourite.length; i += 1) {
        if (props.user_profile.who_add_favourite[i].user_status != 1 || props.user_profile.who_add_favourite[i].block_me_type == 1) {
          props.user_profile.who_add_favourite.splice(i, 1);
        }
      }
      return props.user_profile.who_add_favourite;
    default:
      return [];
  }
}

export function isInLikeMeRealTimeList(identifier, likeMeRealTimeList) {
  if (!likeMeRealTimeList.length) {
    return false
  }
  return likeMeRealTimeList.find(userIdentifier => compareIdentifiers(userIdentifier, identifier))
}

export function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// in same case, API will response a duplicate users
// ie: in the first time API return 100 unique users, when  user viewing these 100 users, there are many new user registered
// this time the offset to get next 100 users will be wrong, the second list will included old users in the fisrt one
// @input array of list users
export function uniqUsers(listUsers) {
  const n = {},r=[];
  const userLength = listUsers.length;
  for(let i = 0; i < userLength; i++)
  {
    const identifier = getUserIdFromIdentifier(listUsers[i].identifier);
    if (!n[identifier])
    {
      n[identifier] = true;
      r.push(listUsers[i]);
    }
  }
  return r;
}

export function filterUniqueUsers(listUsers, newUsers) {
  if (!newUsers) {
    return listUsers;
  }
  
  if (!listUsers) {
    return newUsers;
  }

  const newUniqueUsers = newUsers.filter(
    newUser => !listUsers.find(user => compareIdentifiers(newUser.identifier, user.identifier))
  );
  return [...listUsers, ...newUniqueUsers];
}

export function uniqAndFilterUsers(listUsers, filterUsers) {
  const n = {},r=[];
  const userLength = listUsers.length;

  for(let i = 0; i < userLength; i++)
  {
    const identifier = getUserIdFromIdentifier(listUsers[i].identifier);
    if (!n[identifier])
    {
      n[identifier] = true;
      if (!filterUsers.find(x => compareIdentifiers(x.identifier, listUsers[i].identifier))) {
        r.push(listUsers[i]);
      }
    }
  }
  return r;
}

export function isTouchDevice() {
 return (('ontouchstart' in window)
      || (navigator.MaxTouchPoints > 0)
      || (navigator.msMaxTouchPoints > 0));
}

export function shuffleArray(arrayData) {
  if (!arrayData) {
    return arrayData;
  }
  const returnArray = [];
  let randomIndex;
  for (let i = 0; i < arrayData.length; i += 1) {
    randomIndex = Math.floor(Math.random() * arrayData.length);
    while (returnArray[randomIndex] !== undefined) {
      randomIndex = Math.floor(Math.random() * arrayData.length);
    }
    returnArray[randomIndex] = arrayData[i];
  }
  return returnArray;
}

export function getUserByIndex(index, props) {
  if (index < 0) {
    return null
  }
  if (props.enviroment.previous_page === WHAT_HOT_PAGE) {
    return null;
  }
  if (!props.enviroment) {
    return null
  }

  if (props.enviroment.previous_page === PEOPLE_LIKED_ME_PAGE) {
    props.people_liked_me.users = removeLeftUser('people_liked_me', props)
    return props.people_liked_me.users && index < props.people_liked_me.users.length ? props.people_liked_me.users[index] : null
  }
  if (props.enviroment.previous_page === SEARCH_PAGE ||
    props.enviroment.previous_page === 'SEARCH_PAGE_DEFAULT' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_NEW' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_ONLINE' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_NEARBY' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_HARMONY_AGE' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_HARMONY_FATE' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_HARMONY_HOROSCOPE'
  ) {
    return props.data && index < props.data.length && index < props.data.length ? props.data[index] : null
  }
  if (props.enviroment.previous_page === FAVORITE_PAGE) {
    props.user_profile.favorite_lists = removeLeftUser('favorite_lists', props)
    return props.user_profile.favorite_lists && index < props.user_profile.favorite_lists.length ? props.user_profile.favorite_lists[index] : null
  }
  if (props.enviroment.previous_page === BLOCK_USER_BLOCKED_PAGE) {
    props.user_profile.blocked_users = removeLeftUser('blocked_users', props)
    return props.user_profile.blocked_users && index < props.user_profile.blocked_users.length ? props.user_profile.blocked_users[index] : null
  }
  if (props.enviroment.previous_page === BLOCK_USER_MUTED_PAGE) {
    props.user_profile.muted_users = removeLeftUser('muted_users', props)
    return props.user_profile.muted_users && index < props.user_profile.muted_users.length ? props.user_profile.muted_users[index] : null
  }
  if (props.enviroment.previous_page === FOOTPRINT_PAGE) {
    props.user_profile.visitants = removeLeftUser('visitants', props)
    return props.user_profile.visitants && index < props.user_profile.visitants.length ? props.user_profile.visitants[index] : null
  }
  if (props.enviroment.previous_page === PEOPLE_I_LIKED_PAGE) {
    props.PeopleILiked.data = removeLeftUser('people_i_liked', props)
    return props.PeopleILiked.data && index < props.PeopleILiked.data.length ? props.PeopleILiked.data[index] : null
  }
  if (props.enviroment.previous_page === MATCHED_PAGE) {
    props.user_profile.matched_users = removeLeftUser('matched_list', props)
    return props.user_profile.matched_users && index < props.user_profile.matched_users.length ? props.user_profile.matched_users[index] : null
  }
  if (props.enviroment.previous_page === WHO_ADD_ME_TO_FAVOURITE_PAGE) {
    props.user_profile.who_add_favourite = removeLeftUser('whoaddme_list', props)
    return props.user_profile.who_add_favourite && index < props.user_profile.who_add_favourite.length ? props.user_profile.who_add_favourite[index] : null
  }
  return null
}

export function getUserIndex(props) {
  const user = props.user
  if (!props.enviroment) {
    return -1;
  }
  if (props.enviroment.previous_page === WHAT_HOT_PAGE) {
    return -1;
  }

  if (props.enviroment.previous_page === PEOPLE_LIKED_ME_PAGE) {
    return (props.people_liked_me && props.people_liked_me.users)
      ? props.people_liked_me.users.findIndex(userProfile => compareIdentifiers(userProfile.identifier, user.identifier))
      : -1;
  }
  if (props.enviroment.previous_page === SEARCH_PAGE ||
    props.enviroment.previous_page === 'SEARCH_PAGE_DEFAULT' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_NEW' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_ONLINE' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_NEARBY' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_HARMONY_AGE' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_HARMONY_FATE' ||
    props.enviroment.previous_page === 'SEARCH_PAGE_HARMONY_HOROSCOPE'
  ) {
    return (props.data)
      ? props.data.findIndex(userProfile => compareIdentifiers(userProfile.identifier, user.identifier))
      : -1;
  }
  if (props.enviroment.previous_page === FAVORITE_PAGE) {
    return props.user_profile.favorite_lists
      ? props.user_profile.favorite_lists.findIndex(userProfile => compareIdentifiers(userProfile.identifier, user.identifier))
      : -1
  }
  if (props.enviroment.previous_page === BLOCK_USER_BLOCKED_PAGE) {
    return props.user_profile.blocked_users
      ? props.user_profile.blocked_users.findIndex(userProfile => compareIdentifiers(userProfile.identifier, user.identifier))
      : -1
  }
  if (props.enviroment.previous_page === BLOCK_USER_MUTED_PAGE) {
    return props.user_profile.muted_users
      ? props.user_profile.muted_users.findIndex(userProfile => compareIdentifiers(userProfile.identifier, user.identifier))
      : -1
  }
  if (props.enviroment.previous_page === FOOTPRINT_PAGE) {
    return props.user_profile.visitants
      ? props.user_profile.visitants.findIndex(userProfile => compareIdentifiers(userProfile.identifier, user.identifier))
      : -1
  }
  if (props.enviroment.previous_page === PEOPLE_I_LIKED_PAGE) {
    return props.PeopleILiked.data
      ? props.PeopleILiked.data.findIndex(userProfile => compareIdentifiers(userProfile.identifier, user.identifier))
      : -1
  }
  if (props.enviroment.previous_page === MATCHED_PAGE) {
    return props.user_profile.matched_users
      ? props.user_profile.matched_users.findIndex(userProfile => compareIdentifiers(userProfile.identifier, user.identifier))
      : -1
  }
  if (props.enviroment.previous_page === WHO_ADD_ME_TO_FAVOURITE_PAGE) {
    return props.user_profile.who_add_favourite
      ? props.user_profile.who_add_favourite.findIndex(userProfile => compareIdentifiers(userProfile.identifier, user.identifier))
      : -1
  }
  return -1
}
