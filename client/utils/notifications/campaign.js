import {
  CDN_URL,
  SPINNER_COST,
  NOTIFICATION_BEGIN_HAPPY_HOURS, NOTIFICATION_END_HAPPY_HOURS
} from '../../constants/Enviroment';
import $ from 'jquery';

export function startHappyHours() {
  const clock_begin_url = `${CDN_URL}/general/clock-begin.png`
  const begin = $(`
    <div class="noti noti--bg-blue mbm">
      <div class="noti__body">
        <div class="noti__header mbm">
          <img src= ${clock_begin_url} alt="coin_reward_icon+coin.png" />
        </div>
        <div class="noti__content txt-bold">Giờ vui vẻ <br /> bắt đầu!</div>
      </div>
    </div>
  `)

  $('body').append(begin)

  setTimeout(function() {
    $(begin).remove()
  }, 10000)
}

export function stopHappyHours() {
  const clock_end_url = `${CDN_URL}/general/clock-end.png`
  const end = $(`
    <div class="noti noti--bg-red mbm">
      <div class="noti__body">
        <div class="noti__header mbm">
          <img src= ${clock_end_url} alt="coin_reward_icon+coin.png" />
        </div>
        <div class="noti__content txt-bold">Giờ vui vẻ <br /> kết thúc!</div>
      </div>
    </div>
  `)
  $('body').append(end)

  setTimeout(function() {
    $(end).remove()
  }, 10000)
}

export function deductionNofi() {
  const coin_url = `${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`
  const end = $(`
    <div class="noti noti--bg-yellow">
      <div class="noti__body">
        <div class="noti__content txt-lg l-flex-vertical-center txt-center txt-bold">
          - <img class="img-small" src= ${coin_url} alt="coin_reward_icon+coin.png" /> ${SPINNER_COST}
        </div>
      </div>
    </div>
  `)
  $('body').append(end)

  setTimeout(function() {
    $(end).remove()
  }, 2000)
}
