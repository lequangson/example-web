import $ from 'jquery';

export function showAlreadyUpgrade() {
  const begin = $(`
    <div class="noti noti--bg-green mbm">
      <div class="noti__body">
        <div class="noti__header mbm">
          <i class="fa fa-check-circle-o txt-xxxlg"></i>
        </div>
        <div class="noti__content txt-bold">Bạn đã nâng cấp tài khoản rồi!</div>
      </div>
    </div>
  `)

  $('body').append(begin)

  setTimeout(function() {
    $(begin).remove()
  }, 5000)
}