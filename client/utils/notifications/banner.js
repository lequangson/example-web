import { CDN_URL } from '../../constants/Enviroment';
import $ from 'jquery';

export function remindBannerWithCoinSuccess() {
  const clock_begin_url = `${CDN_URL}/general/speaker-noti-icon.png`
  const begin = $(`
    <div class="noti noti--bg-yellow mbm">
      <div class="noti__body">
        <div class="noti__header mbm">
          <img src= ${clock_begin_url} alt="coin_reward_icon+coin.png" />
        </div>
        <div class="noti__content txt-bold">Gợi nhớ <br /> thành công!</div>
      </div>
    </div>
  `)

  $('body').append(begin)

  setTimeout(function() {
    $(begin).remove()
  }, 10000)
}