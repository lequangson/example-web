const socket = require('socket.io-client')(process.env.SOCKET_URL);
export default socket;
