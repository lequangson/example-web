import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import moment from 'moment'
import { LOADING_DATA_TEXT } from '../constants/TextDefinition'
import { dateTimeToString } from '../utils/common'
import { GA_ACTION_OPEN_NEXTLINK_TIPS } from '../constants/Enviroment'

class FacebookSync extends Component {

  constructor(props) {
    super(props)
    this.state = {
      syncSuccess: false,
    }
  }

  componentWillMount() {
    this.props.getLastFacebookSynchronize()
  }

  synchronizeClick() {
    this.sendGaEvent()
    this.props.facebookSynchronize().then(() => {
      this.setState({ syncSuccess: true })
    })
  }

  sendGaEvent() {
    const { bannerIndex } = this.props.location.query
    if (bannerIndex) {
      const ga_message = this.props.Tips.banners[parseInt(bannerIndex, 10)].plainText
      const ga_track_data = {
        message: ga_message,
      }
      this.props.gaSend(GA_ACTION_OPEN_NEXTLINK_TIPS, ga_track_data)
    }
  }

  render() {
    const { facebookSynchronize } = this.props.Enviroment
    if (facebookSynchronize === '') {
      return <div>{LOADING_DATA_TEXT}</div>
    }
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3 className="txt-heading">Đồng bộ dữ liệu từ Facebook</h3>
            </div>
          </div>
          {!this.state.syncSuccess && <div className="row">
            <div className="col-xs-12">
              <p>
                Sẽ thế nào, khi bạn đang trong trang tìm kiếm, và bạn đột ngột nhận ra một đối tác,
                một người bạn, hoặc tệ hơn là đồng nghiệp của bạn.<br />
                Rất khó xử đúng không. Và thật không may, họ cũng có thể thấy bạn, giống như bạn đã thấy họ vậy.<br />
                Luôn luôn có những khả năng mà hồ sơ hẹn hò của bạn sẽ bị phát hiện bởi một người nào đó
                mà bạn không muốn cũng như không ngờ tới.<br />
                Hãy loại bỏ tình huống khó xử đó bằng cách đồng bộ hóa dữ liệu từ Facebook thường xuyên.
                Chúng tôi sẽ chặn toàn bộ bạn bè trên Facebook của bạn trên hệ thống Ymeet.me,
                đảm bảo họ không thể thấy hồ sơ của bạn và ngược lại.
              </p>
            </div>
          </div>
          }
          <div className="row">
            <div className="col-xs-12">
              {!this.state.syncSuccess && <button className="btn btn--p btn--b mbs" onClick={() => { this.synchronizeClick() }}>Đồng bộ hóa</button>}
              {this.state.syncSuccess && <p>Bạn đã đồng bộ dữ liệu thành công</p>}
              <div className="mbm txt-sm txt-light txt-center txt-red">
                Lần đồng bộ gần nhất: {!this.state.syncSuccess ? dateTimeToString(facebookSynchronize) : moment().format('DD/MM/YYYY H:mm')}
              </div>
              <button
                className="btn btn--b mbl"
                onClick={browserHistory.goBack}
              >Quay lại</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

FacebookSync.propTypes = {
  getLastFacebookSynchronize: PropTypes.func,
  facebookSynchronize: PropTypes.func,
  Enviroment: PropTypes.object,
  location: PropTypes.object,
  Tips: PropTypes.object,
  gaSend: PropTypes.func,
}

export default FacebookSync
