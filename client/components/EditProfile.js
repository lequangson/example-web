import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import $ from 'jquery'
import toggleBackdrop from '../src/scripts/toggleBackdrop'
import ProfileInformation from './Profile/ProfileInformation'
import { MY_PROFILE_PAGE } from '../constants/Enviroment'
import Headroom from './commons/Headroom'
import BigProgressBar from './Profile/BigProgressBar'
import { GA_ACTION_PHOTO_SERVICE, CDN_URL } from '../constants/Enviroment'
import UploadImageModal from './modals/UploadImageModal'

class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      pictureType: 0,
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  componentWillMount() {
    this.props.updateUploadPicturePageSource(MY_PROFILE_PAGE)
    this.props.updatePreviousPage(MY_PROFILE_PAGE)
  }

  componentDidMount() {
    window.scroll(0, this.props.Enviroment.my_profile_scroll_top);
    toggleBackdrop($)
  }

  componentWillUnmount() {
    this.props.updateScrollPossition(MY_PROFILE_PAGE, window.scrollY)
  }

  openModal(pictureType = 0) {
    this.setState(
      {
        modalIsOpen: true,
        pictureType,
      }
    )
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  render() {
    const { user_profile, dictionaries, user,campaign } = this.props
    const { current_user, isUploadingAvatar, isUploadingSubPhoto } = user_profile
    return (
      <div>
        <BigProgressBar completion={user_profile.current_user.profile_completion}/>
        <ProfileInformation
          updateUserProfile={this.props.updateUserProfile}
          user={current_user}
          current_user={current_user}
          dictionaries={dictionaries}
          gaSend={this.props.gaSend}
          mode="edit"
          isUploadingAvatar={isUploadingAvatar}
          isUploadingSubPhoto={isUploadingSubPhoto}
          deleteUserPicture={this.props.deleteUserPicture}
          setAvatar={this.props.setAvatar}
          getCurrentUser={this.props.getCurrentUser}
          openModal={this.openModal}
          campaign={campaign}
        />
        <div className="well well--sm well--b"></div>
        <Modal
          transitionName="modal"
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="modal__content modal__content--3"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
          shouldCloseOnOverlayClick={false}
        >
          <UploadImageModal pictureType={this.state.pictureType} closeModal={this.closeModal} page_source={MY_PROFILE_PAGE} {...this.props} />

          <button className="modal__btn-close" onClick={this.closeModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}
EditProfile.propTypes = {
  Enviroment: PropTypes.object,
  dictionaries: PropTypes.object,
  user_profile: PropTypes.object,
  updateScrollPossition: PropTypes.func,
  deleteUserPicture: PropTypes.func,
  updateUserProfile: PropTypes.func,
}
export default EditProfile
