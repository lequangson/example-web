import React, { PropTypes } from 'react';
import { NotificationContainer } from 'components/commons/Notifications';
import ReceiveGiftModal from 'pages/campaign/modals/ReceiveGiftModal';
import LuckyMoneyModal from 'pages/campaign/modals/LuckyMoneyModal';
import Socket from 'components/socket/Socket';
import LikeModal from './modals/LikeModal'
import MatchModal from './modals/MatchModal'
import SocialList from './SocialList'
import ScrollToTop from './ScrollToTop';
import FirstLikeModal from './modals/FirstLikeModal';
import SimilarModal from './modals/SimilarModal';
import NotificationRandomMatchModal from './modals/NotificationRandomMatchModal';
import PushNotificationModal from './modals/PushNotificationModal';
import DailyRemindModal from './modals/DailyRemindModal';
import SuperLikeModal from './modals/SuperLikeModal';
import QuestionMatchModal from './modals/match_question/MatchModal';
import HurdleNotificationModal from './modals/match_question/HurdleNotification';
// import '../pages/chat/_assets/styles/main.css';
const Main = props => (
  <div style={{ height: `${100} vh` }}>
    {React.cloneElement(props.children, { ...props })}
    <Socket {...props}/>
    <NotificationContainer />
    <LikeModal Like={props.Like} />
    <MatchModal />
    <SocialList />
    <ScrollToTop />
    <FirstLikeModal />
    <SimilarModal />
    <PushNotificationModal />
    <NotificationRandomMatchModal />
    <DailyRemindModal />
    <SuperLikeModal />
    <QuestionMatchModal />
    <ReceiveGiftModal {...props} />
    <LuckyMoneyModal />
    <HurdleNotificationModal />
  </div>
)

Main.propTypes = {
  children: PropTypes.element.isRequired,
}

export default Main;
