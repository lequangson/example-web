import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import q from 'q';
import { getDailyRemindRequest, getDailyRemind, } from 'actions/Daily_Remind';
import { getCampaign, getGiftDictionary, getMyGifts } from 'pages/campaign/Action';
import {
  SEND_3_LIKES, SEND_15_LIKES, SEND_20_LIKES,
  DAILY_ACCESS_2_DAYS, DAILY_ACCESS_3_DAYS, BIRTHDAY,
} from 'constants/Daily_Remind';
import { isEmpty } from 'lodash';
import { getCurrentUser } from 'actions/userProfile';

class FirstActionInDayJob {
  constructor(action, gender ) {
    this.actionDate = null;
    this.firstActionInDay = action;
    this.gender = gender;
    this.doAction = this.doAction.bind(this)
  }

  isFirstActionInday() {
    const currentdate = new Date();
    return this.actionDate === null || this.actionDate !== currentdate.getDate();
  }

  doAction() {
    if (this.isFirstActionInday() && this.firstActionInDay != null) {
      this.firstActionInDay(this.gender);
      const currentdate = new Date();
      this.actionDate = currentdate.getDate();
    }
  }
}

let firstActionInDayJobSingleton = null;
class FirstActionInDayJobSingleton {
  constructor(action, gender) {
    if (firstActionInDayJobSingleton === null) {
      firstActionInDayJobSingleton = new FirstActionInDayJob(action, gender);
    }
  }

  doAction() {
    firstActionInDayJobSingleton.doAction();
  }
}

class RegularCronJob extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.firstActionInDayJob = new FirstActionInDayJobSingleton(props.getDailyRemind, this.props.user_profile.current_user.gender);
    this.doCronJob = this.doCronJob.bind(this);
  }

  componentWillMount() {
    // We will check every minus
    if (this.props.user_profile.current_user && this.props.user_profile.current_user.welcome_page_completion === 4) {
      this.props.setInterval(this.doCronJob, 1000);
    }
  }

  componentWillReceiveProps({user_profile}) {
    //for case reload browser
    const { current_user } = user_profile;
    if (isEmpty(this.props.user_profile.current_user) &&
      !isEmpty(current_user) &&
      current_user.welcome_page_completion === 4) {
      this.props.setInterval(this.doCronJob, 1000);
    }
  }

  doCronJob() {
    this.firstActionInDayJob.doAction();
  }

  render() {
    return (
      <div></div>
    )
  }
}

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getDailyRemind: async (gender) => {
      await dispatch(getDailyRemindRequest());
      await dispatch(getDailyRemind(gender));
      await dispatch(getGiftDictionary());
      await dispatch(getCampaign());
      await dispatch(getMyGifts());
      await dispatch(getCurrentUser());
    },
  }
}

const RegularCronJobContainer = connect(mapStateToProps, mapDispachToProps)(RegularCronJob);

export default RegularCronJobContainer;
