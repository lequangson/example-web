/* global FileReader: true */
import React, { Component, PropTypes } from 'react'
import _ from 'lodash'
import { Link, browserHistory } from 'react-router'
import {
  UPLOAD_DEVIDE_MODE, UPLOAD_FACEBOOK_MODE,
  OTHER_PICTURE, VERIFY_PICTURE_TYPE, VERIFY_PICTURE_SECOND_WAY_TYPE,
  EDUCATION_VERIFICATION_CODE,
  JOB_VERIFICATION_CODE, UPLOAD_CAPTURE_MODE
} from '../constants/userProfile'
import { NOTIFICATION_ERROR } from '../constants/Enviroment';
import Dropzone from 'react-dropzone';
import * as ymmStorage from '../utils/ymmStorage';
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class UploadImages extends Component {
  constructor(props) {
    super(props)
    this.state = {
      over_maximun_picture: false,
      total_files: 0,
      upload_success: [], // contain index of file uploaded successfully
      upload_failure: {}, // contain index and detail error of failure upload files
      upload_more: {}, // for upload more verify image
      upload_more_failure: [],
      upload_more_success: [],
    }
    this.uploadVerifyImages = this.uploadVerifyImages.bind(this);
    this.onOpenClick = this.onOpenClick.bind(this);
  }

  @GoogleOptimize
  componentDidMount() {
    const { Enviroment } = this.props
    const { files_upload, facebook_pictures_synch } = Enviroment
    let { pictureType, uploadType } = this.props

    pictureType = parseInt(pictureType, 10)
    uploadType = parseInt(uploadType, 10)

    if (!pictureType) {
      pictureType = OTHER_PICTURE
    }

    if (!uploadType) {
      uploadType = UPLOAD_DEVIDE_MODE
    }

    const checkType = (uploadType === UPLOAD_DEVIDE_MODE ||
      uploadType === UPLOAD_CAPTURE_MODE) ?
      files_upload.length :
      facebook_pictures_synch.length;

    if (!this.validateNumberUploadPicture(checkType)) {
      return
    }

    if (uploadType === UPLOAD_DEVIDE_MODE || uploadType === UPLOAD_CAPTURE_MODE) {
      this.uploadPictureFiles(files_upload, pictureType)
    } else {
      this.synchronizeFacebookPictures(facebook_pictures_synch, pictureType)
    }
  }

  componentWillUnmount() {
    let { uploadType } = this.props
    if (!uploadType) {
      uploadType = UPLOAD_DEVIDE_MODE
    }
    this.props.clearFilesUpload(uploadType)
  }

  /**
  * upload more image for verify ID card / driver license
  **/
  uploadVerifyImages(files) {
    const checkType = files.length;
    if (!this.validateNumberUploadPicture(checkType)) {
      return
    }
    this.setState({ upload_more: files[0], upload_more_success: []})
    const uploadMore = true;
    this.uploadPictureFiles(files, VERIFY_PICTURE_TYPE, uploadMore)
  }

  onOpenClick() {
    this.dropzone.open()
  }

  validateNumberUploadPicture(numberFileUpload) {
    const { current_user } = this.props.user_profile
    if (numberFileUpload <= 0) {
      browserHistory.push('/myprofile')
      return false
    }

    this.setState({ total_files: numberFileUpload })
    return true
  }

  uploadPictureFiles(files_upload, pictureType = OTHER_PICTURE, uploadMore = false) {

    const props = this.props
    const that = this
    files_upload.forEach((file, index) => {
      const reader = new FileReader()
      reader.addEventListener('load', () => {
        props.uploadImage(pictureType, file.name, file.size, reader.result, {}, this.props.Enviroment.upload_picture_page_source, true)
          .then(() => {
            if (uploadMore) {
              that.setState({ upload_more: files_upload[0], upload_more_success: [0]})
            }
            if ((pictureType == VERIFY_PICTURE_TYPE && files_upload.length === 2) || uploadMore) {
              that.props.updateVerifyImageStatus(true);
            }
            const new_upload_success = that.state.upload_success
            new_upload_success.push(index)
            that.setState({ upload_success: new_upload_success });
            switch (pictureType) {
              case 4:
                that.props.updagteVerificationStatus('id_card_verify_status');
                break;
              case 5:
                that.props.updagteVerificationStatus('selfie_verify_status');
                break;
              case 9:
                that.props.updagteVerificationStatus('job_verify_status');
                break;
              case 8:
                that.props.updagteVerificationStatus('education_verify_status');
                break;
              default:
                break;
            }
          })
          .catch((err) => {
            // case upload failure
            if (pictureType == VERIFY_PICTURE_TYPE ) {
              that.props.updateVerifyImageStatus(false);
            }
            const new_upload_failure = that.state.upload_failure
            new_upload_failure[index] = err.error_message
            that.setState({ upload_failure: new_upload_failure })
          })
      }, false)

      if (file) {
        reader.readAsDataURL(file);
      }
    })
  }

  synchronizeFacebookPictures(facebook_pictures_synch, pictureType) {
    const that = this

    this.props.synchFacebookPictures(facebook_pictures_synch, pictureType, {}, this.props.Enviroment.upload_picture_page_source)
      .then(() => {
        const new_upload_success = that.state.upload_success
        for (let index = 0; index < facebook_pictures_synch.length; index += 1) {
          new_upload_success.push(index)
        }
        that.setState({ upload_success: new_upload_success })
      })
      .catch((err) => {
        // case upload failure
        const new_upload_failure = that.state.upload_failure
        for (let index = 0; index < facebook_pictures_synch.length; index += 1) {
          new_upload_failure[index] = err.error_message
        }
        that.setState({ upload_failure: new_upload_failure })
      })
  }


  renderDevicePictures(files_upload) {
    return (
      files_upload.map((file, i) => {
        const stateClass = {}.hasOwnProperty.call(this.state.upload_failure, i) ? 'is-error' : 'is-loading'
        return (
          <div className="l-flex-grid__item" key={i} >
            <span className={(this.state.upload_success.indexOf(i) === -1) && stateClass}>
              <img src={file.preview} alt="" />
              {{}.hasOwnProperty.call(this.state.upload_failure, i) &&
                <div className="is-error-msg">
                  <i className="fa fa-frown-o fa-2x"></i>
                  <p>{this.state.upload_failure[i]}</p>
                </div>
              }
            </span>
          </div>
        )
      }
      )
    )
  }

  renderFacebookPictures(facebook_pictures_synch) {
    return (
      facebook_pictures_synch.map((pictureUrl, i) =>
        <div className="l-flex-grid__item" key={i} >
          <span className={this.state.upload_success.indexOf(i) > -1 ? '' : 'is-loading'}>
            <img src={pictureUrl} alt="" />
          </span>
        </div>
      )
    )
  }

  render() {

    const { Enviroment } = this.props
    const { files_upload, facebook_pictures_synch } = Enviroment
    let { pictureType, uploadType } = this.props
    pictureType = parseInt(pictureType, 10)
    uploadType = parseInt(uploadType, 10)
    if (pictureType === VERIFY_PICTURE_TYPE) {
      if (!_.isEmpty(this.state.upload_more)) {
        // files_upload.push(this.state.upload_more);
      }
    }
    let requireUploadMore = false;
    if (files_upload.length === 1 && pictureType === VERIFY_PICTURE_TYPE) {
      requireUploadMore = true;
    }
    if (!pictureType) {
      pictureType = OTHER_PICTURE;
    }
    if (!uploadType) {
      uploadType = UPLOAD_DEVIDE_MODE;
    }

    if ((uploadType === UPLOAD_DEVIDE_MODE && files_upload.length === 0) ||
      (uploadType === UPLOAD_FACEBOOK_MODE && facebook_pictures_synch.length === 0)
    ) {
      return <div>Bạn chưa có ảnh nào được tải lên</div>
    }
    if (this.state.over_maximun_picture) {
      return (
        <div className="container">
          <p className="txt-light">{MAXIMUM_PICTURES_UPLOAD_ERROR_MSG}</p>
          <div className="row">
            <div className="col-xs-12">
              <Link to="/myprofile">
                <button className="btn btn--b mbm" disabled={false}>Thoát</button>
              </Link>
            </div>
          </div>
        </div>
      )
    }

    let uploadDone = false

    const uploadFail = this.state.upload_failure || {}
    const uploadSuccess = this.state.upload_success || []
    const totalFailFiles = Object.keys(uploadFail).length
    const totalFiles = this.state.total_files
    const validFiles = totalFiles - totalFailFiles

    if (validFiles > 0) {
      // upload files either have valid or valid + invalid files
      uploadDone = totalFiles === (uploadSuccess.length + totalFailFiles)
    } else {
      // upload files only have invalid files
      uploadDone = totalFailFiles !== 0
    }
    if (this.state.upload_more_success.length === 1) {
      uploadDone = true;
    }
    let successTitle = '';
    if (pictureType === VERIFY_PICTURE_TYPE) {
      successTitle = <p className="txt-light txt-center">
          Tải ảnh lên thành công! <br /> <br />
          Cảm ơn bạn đã xác thực tài khoản để cùng Ymeet.me hẹn hò an toàn và hiệu quả hơn. <br />
          Quá trình xác thực sẽ được tiến hành. Sau khi có kết quả,
          hệ thống sẽ gửi tin nhắn và e-mail xác nhận.
        </p>
    } else {
      successTitle = <p className="txt-light">Tiến trình tải ảnh đã hoàn tất.</p>
    }

    if (pictureType == VERIFY_PICTURE_SECOND_WAY_TYPE) {
      successTitle = <p className="txt-light txt-center">
          Tải ảnh lên thành công! <br /> <br />
          Cảm ơn bạn đã xác thực tài khoản để cùng Ymeet.me hẹn hò an toàn và hiệu quả hơn. <br />
          Quá trình xác thực sẽ được tiến hành. Sau khi có kết quả,
          hệ thống sẽ gửi tin nhắn và e-mail xác nhận.
        </p>
    }

    const isUploading = !uploadDone || (!_.isEmpty(this.state.upload_more) && !this.state.upload_more_success.length);
    return (
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3 className="txt-heading">
                {(pictureType === VERIFY_PICTURE_TYPE || pictureType === VERIFY_PICTURE_SECOND_WAY_TYPE
                || pictureType === EDUCATION_VERIFICATION_CODE || pictureType === JOB_VERIFICATION_CODE || pictureType === OTHER_PICTURE) ?
                  'Thêm ảnh từ thiết bị' :
                  (uploadType === UPLOAD_DEVIDE_MODE ? 'Thêm ảnh từ di động' : 'Thêm ảnh từ Facebook')
                 }
              </h3>
              {isUploading
                ? <p className="txt-light">
                  Tiến trình tải ảnh đang diễn ra.<br />Vui lòng đợi trong giây lát.
                </p>
                : (validFiles > 0 ? successTitle : '')
              }
            </div>
            <div className="col-xs-12 mbl">
              <div className="l-flex-grid">
                {uploadType === UPLOAD_DEVIDE_MODE
                  ? this.renderDevicePictures(files_upload)
                  : this.renderFacebookPictures(facebook_pictures_synch)
                }
              </div>
            </div>
            {uploadDone &&
              <div className="col-xs-12">
                { (!!uploadSuccess.length && pictureType == OTHER_PICTURE) && <Link to="/myprofile"><button
                    className="btn btn--b btn--5"
                  >
                    Thoát
                  </button>
                  </Link>
                }

                { (!!uploadSuccess.length && (
                    pictureType == VERIFY_PICTURE_TYPE ||
                    pictureType == VERIFY_PICTURE_SECOND_WAY_TYPE ||
                    pictureType == EDUCATION_VERIFICATION_CODE ||
                    pictureType == JOB_VERIFICATION_CODE
                  )) && <Link to="/verification"><button
                    className="btn btn--b btn--5"
                  >
                    Thoát
                  </button>
                  </Link>
                }

                { (!uploadSuccess.length) &&
                  <button
                    className="btn btn--b"
                    onClick={() => browserHistory.goBack()}
                  >
                    Quay lại
                  </button>
                }
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
}

UploadImages.propTypes = {
  Enviroment: PropTypes.object.isRequired,
  user_profile: PropTypes.object.isRequired,
  pictureType: PropTypes.string.isRequired,
  uploadType: PropTypes.string.isRequired,
  clearFilesUpload: PropTypes.func.isRequired,
  synchFacebookPictures: PropTypes.func.isRequired,
}

export default UploadImages
