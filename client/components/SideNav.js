import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router';
import { isEmpty } from 'lodash';
import showDefaultAvatar from '../src/scripts/showDefaultAvatar'
import SmallProgressBar from './Profile/SmallProgressBar'
import { NOTIFICATION_TYPE_VIEW_MY_PROFILE, NOTIFICATION_TYPE_VIEW_WHO_ADD_FAVOURITE } from '../constants/userProfile'
import { FACEBOOK_URL, TWITTER_URL, BLOG_URL, GA_ACTION_BOOST_RANK_CLICK, GA_ACTION_VERIFY, GA_ACTION_COIN_CLICK, GA_ACTION_MATCH_QUESTIONS } from '../constants/Enviroment';
import { VERIFICATION_NOT_APPROVE_STATUS, } from '../constants/userProfile';
import * as ymmStorage from '../utils/ymmStorage';
import * as UserDevice from 'utils/UserDevice';
import BoostRankModal from './modals/BoostRankModal'
import { convertUTCToLocalTime } from '../utils/common'
import { isUserInBoostInRankPeriod } from '../utils/BoostInRankService'
import BoostInRankButton from './commons/BoostInRankButton';
import PollingImg from './commons/PollingImg'
import { CDN_URL, ANDROID_URL, IOS_URL } from '../constants/Enviroment'
import BannerHasImgRight from './commons/BannerHasImgRight';
import IosDownloadButton from './commons/Buttons/IosDownloadButton'
import AndroidDownloadButton from './commons/Buttons/AndroidDownloadButton'
import {
  // NOT_VERIFIED,
  // PENDING_FOR_APPROVE,
  VERIFIED
} from 'pages/verification/Constant';
import FbLikeButton from 'components/commons/Buttons/FbLikeButton';

class SideNav extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isModalOpen: false,
      hour: 0,
      minute: 0,
      second: 0,
      expired: true,
    }
    this.visitantClick = this.visitantClick.bind(this)
    this.favouriteClick = this.favouriteClick.bind(this)
    this.handleSendGa = this.handleSendGa.bind(this)
    this.handleBoostRank = this.handleBoostRank.bind(this)
    this.showModal = this.showModal.bind(this)
    this.hideModal = this.hideModal.bind(this)
    this.getTimeRank = this.getTimeRank.bind(this)
  }

  componentWillMount() {
    const user = this.props.user_profile.current_user;
    this.props.setInterval(this.getTimeRank, 1000)
  }

  visitantClick() {
    // clear notification
    this.props.clearNotification(NOTIFICATION_TYPE_VIEW_MY_PROFILE)
  }

  favouriteClick() {
    // clear notification
    this.props.clearNotification(NOTIFICATION_TYPE_VIEW_WHO_ADD_FAVOURITE)
  }

  handleSendGa(type) {
    if (type === 1) {
      this.props.gaSend(GA_ACTION_VERIFY, { page_source: 'User_Verification_Button' });
    }
    else if (type === 2) {
      this.props.gaSend(GA_ACTION_BOOST_RANK_CLICK, { page_source: 'Boost Rank Button' });
    }
    else if (type === 3){
      this.props.gaSend(GA_ACTION_COIN_CLICK, { page_source: 'Coin payment Button' });
    }
    else if (type === 4) {
      this.props.gaSend(GA_ACTION_MATCH_QUESTIONS, { page_source: 'Side menu' });
    }
  }

  handleBoostRank(){
    this.props.hideSideNav()
    this.handleSendGa(2)
    this.showModal()
  }

  showModal() {
    this.setState({ isModalOpen: true })
  }

  hideModal() {
    this.setState({ isModalOpen: false })
  }

  getTimeRank() {
    const user = this.props.user_profile.current_user;
    if (isUserInBoostInRankPeriod(user)) {
      const begin_at = convertUTCToLocalTime(user.ontop_24h.begin_at)
      const end_at = convertUTCToLocalTime(user.ontop_24h.end_at)
      const now = new Date();
      const timediff = end_at - now;
      const a = timediff % 3600000;

      const hour = Math.floor(timediff / 3600000) >= 10 ? Math.floor(timediff / 3600000) : '0' + Math.floor(timediff / 3600000);
      const minute = Math.floor(a / 60000) >= 10 ? Math.floor(a / 60000) : '0' + Math.floor(a / 60000)
      const second = Math.floor((a % 60000) / 1000) >= 10 ? Math.floor((a % 60000) / 1000) : '0' + Math.floor((a % 60000) / 1000)
      this.setState({
        hour: hour,
        minute: minute,
        second: second,
        expired: false,
      })
      if (hour == '00' && minute == '00' && second == '00') {
        this.setState({
          expired: true
        })
      }
    }
  }

  renderBoostInRank() {
    const user = this.props.user_profile.current_user;
    const isPaidUser = user.user_type === 4;

    if (!isUserInBoostInRankPeriod(user) || this.state.expired === true) {
      if (isPaidUser) {
        return (
          <div className="col-xs-6 mbs">
            <BoostInRankButton
              current_user={user}
              boostRankCallBack={this.props.handleBoostRank}
              gaSend={this.props.gaSend}
              page={'SideMenu'}
              cssClassName={'btn noradius btn--b btn--change-color'}
            />
          </div>
        )
      }
      else {
        return (
          <div className="col-xs-6 mbs">
            <div onClick={this.handleBoostRank} className="btn noradius btn--b btn--change-color">
              <i className="fa fa-rocket padding-r5"></i>Nổi bật ngay!
            </div>
          </div>
        )
      }
    }
    else {
      return (
        <div className="col-xs-6 mbs">
          <div className="btn noradius btn--b btn--change-color txt-dark">
            <i className="fa fa-rocket padding-r5"></i>
            {this.state.hour}:{this.state.minute}:{this.state.second}
          </div>
        </div>
      )
    }
  }

  render() {
    const campaign = this.props.campaign;
    const user = this.props.user_profile.current_user;
    if (isEmpty(user)) {
      return null;
    }

    let userAvatar = ''
    if (user.user_pictures && user.user_pictures.length && user.user_pictures[0].is_main) {
      userAvatar = user.user_pictures[0].thumb_image_url
    } else {
      userAvatar = showDefaultAvatar(user.gender, 'thumb')
    }
    const nickName = user.nick_name
    const age = user.age
    const residence = user.residence ? user.residence.value : ''
    const identifier = user.identifier ? user.identifier.substring(6) : ''
    const verification_status = user.verification_status || {};
    const {
      id_card_verify_status,
      phone_verify_status,
      selfie_verify_status,
      job_verify_status,
      education_verify_status
    } = verification_status;
    let is_hide_verification_button = true;
    if (user.gender == 'male') {
      is_hide_verification_button = id_card_verify_status == VERIFIED &&
        phone_verify_status == VERIFIED &&
        selfie_verify_status == VERIFIED &&
        job_verify_status == VERIFIED &&
        education_verify_status == VERIFIED;
    } else { // female just verify id card or selfie
      is_hide_verification_button = id_card_verify_status == VERIFIED &&
        selfie_verify_status == VERIFIED;
    }
    const isPaidUser = user.user_type === 4;

    return (
      <div>
        <aside className={this.props.showSideNav ? 'sidenav is-slide-in' : 'sidenav'}>
          <Link to="/myprofile" className="sidenav__header">
            <div className="frame frame--sm frame--1">
              <PollingImg
                src={userAvatar}
                alt={nickName}
              />
            </div>
            <div className="sidenav__header__txt">
            <div className="txt-lg"><span className="txt-bold">{nickName} </span>- ID {identifier}</div>
              <div>
                <span>{age} tuổi</span>
                {residence ? <span> - {residence}</span> : ''}
              </div>
              <div className="sidenav__header__progress">
                <SmallProgressBar completion={user.profile_completion} /><span> hoàn thành hồ sơ</span>
              </div>
            </div>
            <i className="fa fa-pencil fa-lg"></i>
          </Link>

          <div className="padding-col-2 padding-b20 clearfix padding-rl5 ">
              <div className="col-xs-6 mbs">
                <Link to="/payment/coin" onClick={() => {this.handleSendGa(3)}} className="btn noradius btn--blue btn--b">
                  <img src={`${CDN_URL}/general/icon+coin.png`} alt="coin" className="img-small" /> Bạn có {user.coins? user.coins : 0} xu
                </Link>
              </div>
            {this.renderBoostInRank()}
            <BoostRankModal
              current_user={this.props.user_profile.current_user}
              isOpen={this.state.isModalOpen}
              gaSend={this.props.gaSend}
              onRequestClose={this.hideModal}
              page="SideMenu"
            />
            { !is_hide_verification_button && // will check condition later after done verification task
              <div className=" col-xs-12">
                <Link to="/verification" onClick={() => {this.handleSendGa(1)}} className="btn noradius btn--s btn--b">
                  <i className="fa fa-check-circle padding-r5"></i>Xác thực tài khoản
                </Link>
              </div>
            }
            </div>

          <div className="well well--sm well--b"></div>
          <ul className="list-border">
            { !isPaidUser && user.gender === 'male' &&
              <li className="list-border__item">
                <Link
                  to="/payment/upgrade"
                  className="list-border__link"
                  // onClick={this.visitantClick}
                >
                  <img
                    src={`${CDN_URL}/general/MobileApp/Payment/upgrade_account_unpaid_user.png`}
                    className="img-small mrm img-baseline"
                    alt="unpaid_user"
                  />
                  {isPaidUser ? 'Tài khoản đã nâng cấp' : 'Nâng cấp tài khoản'}
                </Link>
              </li>
            }
            <li className="list-border__item">
              <Link
                to="/matched-list"
                className="list-border__link"
                // onClick={this.visitantClick}
              >
                <img src={`${CDN_URL}/general/match-list-icon.png`} alt="" className="img-small mrm img-baseline"/>
                Danh sách kết đôi
              </Link>
            </li>
            <li className="list-border__item">
              <Link
                to="/visitants"
                className="list-border__link"
                onClick={this.visitantClick}
              >
                <i className="fa fa-eye"></i>Khách thăm
                { user.notification_number_visitants > 0 &&
                  <span className="sidenav__noti sidenav__noti--1">
                    {user.notification_number_visitants}
                  </span>
                }
              </Link>
            </li>
            <li className="list-border__item">
              <Link
                to="/who-add-favourite"
                className="list-border__link"
                onClick={this.favouriteClick}
              >
                <i className="fa fa-heartbeat"></i>Ai đã quan tâm tôi?
                {
                 user.notification_number_added_me_favorite > 0 &&
                  <span className="sidenav__noti sidenav__noti--1">
                    {user.notification_number_added_me_favorite}
                  </span>
                }
              </Link>
            </li>
            <li className="list-border__item">
              <Link
                to="/expend-list"
                className="list-border__link"
                onClick={this.props.hideSideNav}
              >
                <i className="fa fa-list-ol"></i>Danh sách mở rộng
              </Link>
            </li>
            <li className="list-border__item">
              <Link
                to="/tarot-page"
                className="list-border__link"
                onClick={this.props.hideSideNav}
              >
                <img src={`${CDN_URL}/general/Tarot/tarot-icon.png`} alt="" className="img-1 mrm img-baseline"/>
                Bói bài Tarot
              </Link>
            </li>
            <li className="list-border__item">
              <Link
                to="/my-answer"
                className="list-border__link"
                onClick={() => this.handleSendGa(4)}
              >
                <i className="fa fa-question-circle-o"></i>
                Khám phá bản thân
                {/*<span className="sidenav__noti sidenav__noti--2">
                    Mới!
                </span>*/}
              </Link>
            </li>
            <li className="list-border__item">
              <Link
                to="/setting"
                className="list-border__link"
                onClick={this.props.hideSideNav}
              >
                <i className="fa fa-cog"></i>Cài đặt hệ thống
              </Link>
            </li>
            <li className="list-border__item">
              <Link
                to="/logout"
                className="list-border__link"
                onClick={this.props.hideSideNav}
              >
                <i className="fa fa-sign-out"></i>Đăng xuất
              </Link>
            </li>
          </ul>
          {!isEmpty(campaign.data) &&
            <div>
              <Link
                className="banner banner__background--1"
                to="/events"
                onClick={this.props.hideSideNav}
              >
                <div className="banner__content col-xs-8 col-xs-offset-3">
                </div>
                <div className="banner__meta">
                </div>
              </Link>
            </div>
          }
          <div className="padding-rl5">
            <h4 className="txt-blue txt-left mbm">
              Tải ứng dụng hẹn hò miễn phí Ymeet.me
            </h4>
            <div className="l-flex-vertical-center">
              {(UserDevice.getName() === 'pc' || UserDevice.getName() === 'ios') &&
                <div className="col-xs-6 padding-0">
                  <IosDownloadButton redirect_url={IOS_URL} />
                </div>
              }
              {(UserDevice.getName() === 'pc' || UserDevice.getName() === 'android') &&
                <div className="col-xs-6 padding-0">
                  <AndroidDownloadButton redirect_url={ANDROID_URL}/>
                </div>
              }
            </div>
          </div>
          <div className="padding-rl10 padding-b10">
            <FbLikeButton
              appId={process.env.FACEBOOK_APP_ID}
              version="v2.10"
              href="https://www.facebook.com/ymeet.me/"
              showFaces={false}
              layout="button_count"
              share={false}
            />
          </div>
          <BannerHasImgRight
            pageSource="sideNav"
            to="/referral-page"
          // onClick={()=>console.log('that work')}
          />
          <ul className="list-brick mbl">
            <li className="list-brick__item">
              <Link to="/about" className="list-brick__link">Giới thiệu</Link>
            </li>
            <li className="list-brick__item">
              <Link to="/terms-of-use" className="list-brick__link">Điều khoản</Link>
            </li>
            <li className="list-brick__item">
              <Link to="/privacy" className="list-brick__link">Bảo mật</Link>
            </li>
            <li className="list-brick__item">
              <Link to="/helps" className="list-brick__link">Trợ giúp</Link>
            </li>
            <li className="list-brick__item">
              <Link to="/contact" className="list-brick__link">Liên hệ</Link>
            </li>
            <li className="list-brick__item">
              <a href={BLOG_URL} target="_blank" className="list-brick__link">Blog</a>
            </li>
          </ul>
          <br />
          <div className="sidenav__footer">
            <a
              href={FACEBOOK_URL}
              target="_blank"
              rel="noopener noreferrer"
              className="sidenav__btn sidenav__btn--facebook mrm"
            >
              <i className="fa fa-facebook-square"></i>
              &nbsp;/Ymeet.me
            </a>
            &nbsp;
            <a
              href={TWITTER_URL}
              target="_blank"
              rel="noopener noreferrer"
              className="sidenav__btn sidenav__btn--twitter"
            >
              <i className="fa fa-twitter-square"></i>
              &nbsp;/Ymeetme
            </a>
          </div>
        </aside>
        <button
          className={
            this.props.showSideNav
              ? 'sidenav__close is-show'
              : 'sidenav__close'
            }
          onClick={this.props.hideSideNav}
        >
          <i className="fa fa-times fa-2x"></i>
        </button>
        <div
          className={
            this.props.showSideNav
              ? 'is-show sidenav__overlay'
              : 'sidenav__overlay'
          }
          onClick={this.props.hideSideNav}
        ></div>
      </div>
    )
  }
}

SideNav.propTypes = {
  clearNotification: PropTypes.func.isRequired,
  user_profile: PropTypes.object.isRequired,
  showSideNav: PropTypes.bool,
  hideSideNav: PropTypes.func,
  gaSend: PropTypes.func,
}

export default SideNav