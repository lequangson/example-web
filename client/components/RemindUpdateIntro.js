import React, { Component, PropTypes } from 'react'
import Modal from 'react-modal'
import { Link } from 'react-router'
import { UPDATE_INTRO_IMAGE, GA_ACTION_EDIT_PROFILE, REMIND_UPDATE_INTRO } from '../constants/Enviroment'
import * as ymmStorage from '../utils/ymmStorage';

class RemindUpdateIntro extends Component {
  constructor() {
    super()
    this.state = {
      modalIsOpen: false,
    }

    this.modalOpen = this.modalOpen.bind(this)
    this.modalClose = this.modalClose.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    const current_user = this.props.user_profile.current_user
    const modalShowNext = nextProps.Enviroment.modalShowNext
    if (modalShowNext === REMIND_UPDATE_INTRO && typeof current_user.self_introduction !== 'undefined' && !current_user.self_introduction && !ymmStorage.getItem('showed_remind_intro')) {
      this.modalOpen()
    }
  }

  modalOpen() {
    this.setState({ modalIsOpen: true })
  }

  modalClose() {
    ymmStorage.setItem('showed_remind_intro', true)
    this.setState({ modalIsOpen: false })
  }

  handleClick() {
    this.modalClose()
    const ga_track_data = {
      page_source: REMIND_UPDATE_INTRO
    }
    this.props.gaSend(GA_ACTION_EDIT_PROFILE, ga_track_data)
  }

  render() {
    return (
      <Modal
        isOpen={this.state.modalIsOpen}
        onRequestClose={() => {
          this.modalClose('')
        }}
        className="modal__content modal__content--2"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        <Link
          to="/myprofile/update-self-intro"
          className="modal__content__frame"
          onClick={this.handleClick}
        >
          <img src={UPDATE_INTRO_IMAGE} alt="Promotion" />
        </Link>
        <button
          className="modal__btn-close"
          onClick={() => {
            this.modalClose('')
          }}
        >
          <i className="fa fa-times"></i>
        </button>
      </Modal>
    )
  }
}

RemindUpdateIntro.propTypes = {
  modalShowNext: PropTypes.func,
}

export default RemindUpdateIntro
