import React from 'react';
import SearchConditionSimple from './SearchConditionSimple';
import SearchConditionAdvance from './SearchConditionAdvance';

const SearchCondition = (props) => (
  <div>
      { (props.user_profile.current_user.user_type !== 4 && props.user_profile.current_user.gender !== 'female') && <SearchConditionSimple {...props} /> }
      { (props.user_profile.current_user.user_type === 4 || props.user_profile.current_user.gender === 'female') && <SearchConditionAdvance {...props} /> }
  </div>
)

export default SearchCondition;
