import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import $ from 'jquery';
import _ from 'lodash';
import {
  EXCEED_NAME_CHANGE_ERROR_MSG, SPECIAL_CHAR_NOT_ALLOWED,
  NAME_NOT_EXCEED_8_NUMBER, NAME_MUST_NOT_START_WITH_NUMBER, API_EXECUTION_ERROR_NETWORK_MSG,
  CHANGE_NAME_SUCCESS_DESCRIPTION, CHANGE_NAME_SUCCESS_LABEL, NAME_MUST_NOT_BE_EMPTY,
  NAME_MUST_BETWEEN_2_10_CHARACTER,
  DOB_MUST_NOT_BE_EMPTY, DOB_MUST_BE_A_DATE, GENDER_MUST_NOT_BE_EMPTY
} from 'constants/TextDefinition';
import { updateUserProfileFailure } from 'actions/userProfile';
import {
  REGEX_NAME, GA_ACTION_UPDATE_NICKNAME, NOTIFICATION_SUCCESS,
  REGEX_CONTAIN_SPECIAL_CHAR, REGEX_OVER_8_NUMBER_DETECTED, REGEX_FIRST_CHAR_IS_NUMBER, WELCOME_PAGE,
  REGEX_DOB, CDN_URL
} from 'constants/Enviroment';
import { sendNotification } from 'utils/errors/notification';

class BasicInfoUpdate extends React.Component {

  static validateName(input_value, dob, gender, type) {
    const errors = {
      name: [],
      dob: [],
      gender: []
    }
    if (!input_value) {
      errors.name.push(NAME_MUST_NOT_BE_EMPTY)
    }
    if(!input_value.match(REGEX_NAME) && input_value) {
      errors.name.push(NAME_MUST_BETWEEN_2_10_CHARACTER)
    }
    if (input_value.match(REGEX_CONTAIN_SPECIAL_CHAR)) {
      errors.name.push(SPECIAL_CHAR_NOT_ALLOWED)
    }
    if (input_value.match(REGEX_FIRST_CHAR_IS_NUMBER)) {
      errors.name.push(NAME_MUST_NOT_START_WITH_NUMBER)
    }
    if (input_value.match(REGEX_OVER_8_NUMBER_DETECTED)) {
      errors.name.push(NAME_NOT_EXCEED_8_NUMBER)
    }
    if (type === 1) {
      if (!dob.match(REGEX_DOB)) {
        errors.dob.push(DOB_MUST_BE_A_DATE)
      }
      if (!gender) {
        errors.gender.push(GENDER_MUST_NOT_BE_EMPTY)
      }
    }
    return errors
  }

  static renderErrorMsg(error) {
    return (
      <div key={error.toString()} className="txt-red">
        {error}
      </div>
    )
  }

  constructor(props) {
    super(props)
    this.state = {
      isAllowSubmit: true,
      errorMsgs: {},
      daySelected: false,
      monthSelected: false,
      yearSelected: false,
    }
    this.onChange = this.onChange.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
    this.handleSelectbox = this.handleSelectbox.bind(this)
  }

  onChange() {
    const input_value = $('#update-input-name').val().trim()
    const isNameChanged = this.props.user_profile.current_user &&
          input_value !== this.props.user_profile.current_user.nick_name
    const isAllowSubmit = isNameChanged && input_value.match(REGEX_NAME)
    this.setState({ isAllowSubmit })
  }

  handleUpdate() {
    const { fromWelcomePage } = this.props
    const input_value = $('#update-input-name').val().trim()

    if (!this.props.user_profile.current_user.allow_update_short_name) {
      this.props.showErrorMessage(EXCEED_NAME_CHANGE_ERROR_MSG)
      return
    }

    let errors = ''

    let params = {}
    if(this.props.user_profile.current_user.provider !== 'facebook' && fromWelcomePage) {
      const dd = this.refs.day.value < 10 ? '0' + this.refs.day.value : this.refs.day.value
      const mm = this.refs.month.value < 10 ? '0' + this.refs.month.value : this.refs.month.value
      const dob = dd+'-'+mm+'-'+this.refs.year.value
      const gender = $("input[name='gender']:checked").val()
      params = {
        'personal[birthday]': dob,
        'personal[gender]': gender,
        'personal[nick_name]': input_value,
      }
      errors = BasicInfoUpdate.validateName(input_value, dob, gender, 1)
    }
    else {
      params = {
        'personal[nick_name]': input_value,
      }
      errors = BasicInfoUpdate.validateName(input_value, 2)
    }
    if (errors && (errors.name.length !== 0 || errors.dob.length !== 0 || errors.gender.length !== 0)) {
      this.setState({ errorMsgs: errors })
      return
    }

    this.setState({ isAllowSubmit: false })
    const that = this
    this.props.updateUserProfile(params).then(() => {
      if (this.props.fromWelcomePage) {
        const ga_track_data = {
          page_source: WELCOME_PAGE
        }
        that.props.gaSend(GA_ACTION_UPDATE_NICKNAME, ga_track_data)
        that.setState({ isAllowSubmit: true })
        this.props.callBackSubmit()
      } else {
        const ga_track_data = {
          page_source: 'EDIT_PROFILE_PAGE'
        }
        that.props.gaSend(GA_ACTION_UPDATE_NICKNAME, ga_track_data)
        sendNotification(NOTIFICATION_SUCCESS, CHANGE_NAME_SUCCESS_LABEL, CHANGE_NAME_SUCCESS_DESCRIPTION)
        browserHistory.push('/myprofile')
      }
    }).catch(() => {
      that.setState({ isAllowSubmit: true })
      that.props.showErrorMessage(API_EXECUTION_ERROR_NETWORK_MSG)
    })
  }

  handleSelectbox() {
    if (this.refs.day.value > 0) {
      this.setState({ daySelected: true })
    }
    else if (this.refs.day.value == 0) {
      this.setState({ daySelected: false })
    }
    if (this.refs.month.value > 0) {
      this.setState({ monthSelected: true })
    }
    else if (this.refs.month.value == 0) {
      this.setState({ monthSelected: false })
    }
    if (this.refs.year.value > 0) {
      this.setState({ yearSelected: true })
    }
    else if (this.refs.year.value == 0) {
      this.setState({ yearSelected: false })
    }
  }

  renderDay(){
    return _.times(32).map(function(i) {
      if(i > 0) {
        return <option value={i} key={i}>{i}</option>
      }
    })
  }

  renderMonth(){
    return _.times(13).map(function(i) {
      if(i > 0) {
        return <option value={i} key={i}>{i}</option>
      }
    })
  }

  renderYear(){
    return _.times(1999).sort(function(a, b){return b-a}).map(function(i) {
      if(i > 1900) {
        return <option value={i} key={i}>{i}</option>
      }
    })
  }

  render() {
    const { user_profile, fromWelcomePage } = this.props
    const { current_user } = user_profile
    const name = this.state.errorMsgs.name || []
    const dob = this.state.errorMsgs.dob || []
    const gender = this.state.errorMsgs.gender || []

    if (_.isEmpty(current_user)) {
      return (
        <p>Loading...</p>
      )
    }
    return (
      <div>
        <div className="sticky-content">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <div id="basicInfoForm">
                  {
                    !fromWelcomePage &&
                    <div className="col-xs-12">
                      <h3 className="txt-heading">Tên</h3>
                      <p>Bạn có thế thay đổi tên sau</p>
                    </div>
                  }
                  <div className="form__group col-xs-12">
                  {
                    fromWelcomePage &&
                    <div>
                      <h3 className="welcome__heading"> <img src={`${CDN_URL}/general/WelComePage/Name.png`} alt="Name.png" className="is-img"/> Tên bạn là gì?</h3>
                    </div>
                  }
                    <div className="row mbm" key={2} >
                      <div className="col-xs-12">
                        <div className={name.length ? "form__input--rel-fail" : (dob.length || gender.length) ? "form__input--rel" : ""}>
                          <input
                            defaultValue={this.props.nick_name || ""}
                            id={'update-input-name'}
                            name={'personal[nick_name]'}
                            type="text"
                            className={name.length ? "border-r txt-blue" : "txt-blue"}
                            maxLength="10"
                            placeholder={'Từ 2 đến 10 ký tự'}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row mbm" key={1} >
                      <div className="col-xs-12">
                        {
                          !fromWelcomePage
                          ?
                          'Cập nhật tên của bạn'
                          :
                          'Bạn có thể thay đổi tên sau'
                        }
                        { this.state.errorMsgs.name
                          &&
                          this.state.errorMsgs.name.map(err => BasicInfoUpdate.renderErrorMsg(err))
                        }
                      </div>
                    </div>
                    {
                    !fromWelcomePage &&
                    <div>
                      <div className="txt-underline txt-bold">Chú ý:</div>
                      <div>1. Bạn chỉ có thể thay đổi tên <span className="txt-blue txt-bold">duy nhất một lần</span>.</div>
                      <div>2. Nên để <span className="txt-blue txt-bold">tên thật</span> (họ và tên hoặc tên đệm và tên)</div>
                      <div>
                        3. Tên
                        <span className="txt-blue txt-bold"> không chứa thông tin cá nhân</span>
                         (địa chỉ, liên lạc, ngày sinh, ...)
                      </div>
                      <div>4. Tên
                        <span className="txt-blue txt-bold"> không chứa từ có ý nghĩa xấu</span>
                         (tục tĩu, xúc phạm, khiêu dâm, phân biệt chủng tộc...)
                      </div>
                      <div>5. Cố ý vi phạm điều trên sẽ bị xử lý</div>
                    </div>
                  }
                  { (fromWelcomePage && this.props.user_profile.current_user.provider !== 'facebook') &&

                    <div>
                        <div className="row mbm">
                          <div className="col-xs-12">
                            <h3 className="welcome__heading"><img src={`${CDN_URL}/general/WelComePage/Birthday.png`} alt="Birthday.png" className="is-img"/>Ngày sinh</h3>
                          </div>
                          <div className="col-xs-4">
                            <select onChange={this.handleSelectbox} ref="day" id="day" name="day" className={this.state.daySelected ? "txt-light form__input--c" : "form__input--c"}>
                              <option value="0">Ngày</option>
                              {this.renderDay()}
                            </select>
                          </div>
                          <div className="col-xs-4">
                            <select onChange={this.handleSelectbox} ref="month" id="month" name="month" className={this.state.monthSelected ? "txt-light form__input--c" : "form__input--c"}>
                              <option value="0">Tháng</option>
                              {this.renderMonth()}
                            </select>
                          </div>
                          <div className="col-xs-4 mbm">
                            <select onChange={this.handleSelectbox} ref="year" id="year" name="year" className={this.state.yearSelected ? "txt-light form__input--c" : "form__input--c"}>
                              <option value="0">Năm</option>
                              {this.renderYear()}
                            </select>
                          </div>
                          <div className="col-xs-12">
                            { this.state.errorMsgs.dob
                              &&
                              this.state.errorMsgs.dob.map(err => BasicInfoUpdate.renderErrorMsg(err))
                            }
                          </div>
                        </div>
                        <div className="row mbm">
                          <div className="col-xs-12">
                            <h3 className="welcome__heading mbm"><img src={`${CDN_URL}/general/WelComePage/Gender.png`} alt="Gender.png" className="is-img"/>Giới tính <span className="txt-red txt-sm">(Bắt buộc)</span></h3>
                          </div>
                          <div className="col-xs-3 l-flex-vertical-center">
                            <input className="radio-size" id="male" type="radio" name="gender" value="1" />
                            <label className="padding-l10" htmlFor="nam">Nam</label>
                          </div>
                          <div className="col-xs-6 l-flex-vertical-center mbm">
                            <input className="radio-size" id="female" type="radio" name="gender" value="2" />
                            <label className="padding-l10" htmlFor="nu">Nữ</label>
                          </div>
                          <div className="col-xs-12">
                            { this.state.errorMsgs.gender
                              &&
                              this.state.errorMsgs.gender.map(err => BasicInfoUpdate.renderErrorMsg(err))
                            }
                          </div>
                        </div>
                    </div>
                  }
                  </div>
                  {
                    fromWelcomePage &&
                    <div>
                      <div className="col-xs-12 mbs">
                        <button
                          className="btn btn--aqua btn--b btn--lg txt-thin"
                          onClick={this.handleUpdate}
                          disabled={!this.state.isAllowSubmit}
                        >
                          Tiếp theo
                        </button>
                      </div>
                    </div>
                  }
                  {
                    !fromWelcomePage &&
                    <div className="row">
                      <div className="col-xs-6">
                        <Link to="/myprofile">
                          <button className="btn btn--b">Hủy</button>
                        </Link>
                      </div>
                      <div className="col-xs-6">
                        <button
                          className="btn btn--p btn--b"
                          onClick={this.handleUpdate}
                          disabled={!this.state.isAllowSubmit}
                        >
                          Cập nhật
                        </button>
                      </div>
                    </div>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

BasicInfoUpdate.propTypes = {
  user_profile: PropTypes.object,
  updateUserProfile: PropTypes.func,
  showErrorMessage: PropTypes.func,
  fromWelcomePage: PropTypes.bool,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    showErrorMessage: (msg) => {
      dispatch(updateUserProfileFailure(msg))
    },
  }
}
const BasicInfoUpdateContainer = connect(mapStateToProps, mapDispatchToProps)(BasicInfoUpdate);

export default BasicInfoUpdateContainer
