import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import q from 'q';
import _ from 'lodash';
import Modal from 'components/modals/Modal';
import { setLikeInfoRequest, setLikeInfo } from 'actions/Like';
import UserPicture from 'components/commons/UserPicture';
import LikeButton from 'components/commons/LikeButton';
import { shuffleArray } from 'utils/common';
import {
  ENVELOP_ICON, LIKE_2_ICON, NOTIFICATION_ERROR, NOTIFICATION_NOT_ENOUGH_LIKE, LIKE_STYLE_ICON
} from 'constants/Enviroment';
import {
  ERROR_LABEL_NOT_ENOUGH_LIKE, ERROR_MSG_NOT_ENOUGH_LIKE_FOR_MSG
} from 'constants/TextDefinition';
import GoogleOptimize from 'utils/google_analytic/google_optimize';
import { getAgeGroup } from 'utils/UserBussinessUtils';

class LikeModal extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      isLikeModalOpen: false,
      currentMsgNumber: 0,
      isSendingLike: false
    }
  }

  componentWillMount() {
    const { Like } = this.props;
    const { isLikeModalOpen, isLikeMessageModalOpen, likeFrom, likeTo } = Like;
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.Like.isLikeModalOpen && !nextProps.Like.isLikeModalOpen) {
      this.setState({currentMsgNumber: 0});
    }
  }

  @GoogleOptimize
  componentDidUpdate(){}

  closeLikeModal = () => {
    const isLikeModalOpen = false;
    const isLikeMessageModalOpen = false;
    const likeFrom = {};
    const likeTo = {};
    this.props.setLikeInfo(likeFrom, likeTo, isLikeModalOpen, isLikeMessageModalOpen);
  }

  getUserTemplateMap(have_sent_like_to_me = false) {
    if (typeof this.props.user_profile.current_user.gender === 'undefined' || this.props.user_profile.current_user.gender === null) {
      return [];
    }
    let like_type = 'like';
    if (have_sent_like_to_me) {
      like_type = 'like_back';
    }
    const user_age = this.props.user_profile.current_user.age || 0;
    const age_group = getAgeGroup(user_age);
    return this.props.dictionaries.like_msg_temp[this.props.user_profile.current_user.gender][like_type][age_group];
  }

  getCurrentTemplate(i, have_sent_like_to_me) {
    if (i < 0) return '';
    if (this.getUserTemplateMap(have_sent_like_to_me) === undefined ||
        typeof this.getUserTemplateMap(have_sent_like_to_me)[i] == 'undefined')
      return '';
    return this.getUserTemplateMap(have_sent_like_to_me)[i].value;
  }

  next = () => {
    const { Like } = this.props;
    const { likeTo } = Like;
    const have_sent_like_to_me = likeTo.have_sent_like_to_me || false;
    if (this.state.currentMsgNumber === this.getUserTemplateMap(have_sent_like_to_me).length - 1) {
      return;
    }
    this.setState({ currentMsgNumber: this.state.currentMsgNumber + 1 });
  }

  prev = () => {
    if (this.state.currentMsgNumber === 0) {
      return;
    }
    this.setState({ currentMsgNumber: this.state.currentMsgNumber - 1 });
  }

  markSendingLike(isSendingLike) {
    this.setState({ isSendingLike });
  }

  render() {
    const { Like } = this.props;
    const { isLikeModalOpen, isLikeMessageModalOpen, likeFrom, likeTo } = Like;

    const have_sent_like_to_me = likeTo.have_sent_like_to_me || false;
    const like_style = this.props.user_profile.like_style;

    return (
      <div>
        <Modal
          transitionName="modal"
          isOpen={isLikeModalOpen}
          onRequestClose={this.closeLikeModal}
          className="LikeModal modal__content"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="card mbm">
            <UserPicture user={likeTo} />
          </div>
          <div className="mbm l-flex-ce">
            <button className="btn btn--5" style={this.state.currentMsgNumber === 0 ? {visibility: 'hidden'} : {}} onClick={this.prev}>
              <i className="fa fa-chevron-left"></i> Trước
            </button>
            <span className="txt-bold txt-light txt-center">Gửi lời chào mẫu {this.state.currentMsgNumber + 1} / {this.getUserTemplateMap(have_sent_like_to_me).length}</span>
            <button className="btn btn--5" style={this.state.currentMsgNumber === this.getUserTemplateMap(have_sent_like_to_me).length - 1 ? {visibility: 'hidden'} : {}} onClick={this.next} >
              Tiếp <i className="fa fa-chevron-right"></i>
            </button>
          </div>
          <div className="mbs well well--g" style={{ height: '110px' }}>
            {this.getCurrentTemplate(this.state.currentMsgNumber, have_sent_like_to_me)}
          </div>
          <div className="mbs">
            <LikeButton
              user={likeTo}
              user_profile={likeFrom}
              forceLike
              title="Gửi thích kèm lời chào"
              msg={this.getCurrentTemplate(this.state.currentMsgNumber, have_sent_like_to_me)}
              isSendingLike={this.state.isSendingLike}
              markSendingLike={v => this.markSendingLike(v)}
              page_source={Like.page_source}
              inLikeModal={true}
            />
          </div>
          {likeTo.gender === 'male' && <div className="mbs">
              <LikeButton
                user={likeTo}
                user_profile={likeFrom}
                forceLike
                markSendingLike={v => this.markSendingLike(v)}
                isSendingLike={this.state.isSendingLike}
                title="Chỉ gửi thích"
                useDefaultStyle={true}
                page_source={Like.page_source}
                inLikeModal={true}
              />
            </div>
          }
          <button className="modal__btn-close" onClick={this.closeLikeModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}

LikeModal.propTypes = {
  Like: PropTypes.object,
  setLikeInfo: PropTypes.func,
  dictionaries: PropTypes.object,
  user_profile: PropTypes.object,
}


function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    setLikeInfo: (likeFrom, likeTo, isLikeModalOpen = false, isLikeMessageModalOpen = false) => {
      dispatch(setLikeInfoRequest())
      dispatch(setLikeInfo(likeFrom, likeTo, isLikeModalOpen, isLikeMessageModalOpen))
    },
  }
}
const LikeModalContainer = connect(mapStateToProps, mapDispachToProps)(LikeModal);

export default LikeModalContainer;
