import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { connect } from 'react-redux'
import { updateSimilarUserModal } from '../../actions/Like';
import { ignoreUser } from '../../actions/userProfile';
import { updateFirstLikeStatus, updateFirstLikeStatusViaApi } from '../../actions/userProfile';
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import LikeButton from '../../components/commons/LikeButton';
import UserPicture from '../../components/commons/UserPicture';
import { SIMILAR_MODAL_PAGE } from '../../constants/Enviroment'
import { getUserIdFromIdentifier } from '../../utils/common'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';
import {
  CDN_URL
} from '../../constants/Enviroment';

class SimilarModal extends React.Component {

  constructor(props) {
    super(props)
    this.closeModal = this.closeModal.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}

  closeModal() {
    const { similarUserInfo } = this.props.Like
    this.props.ignoreUser(getUserIdFromIdentifier(similarUserInfo.identifier), 'SIMILAR_MODAL')
    this.props.updateSimilarUserModal()
  }

  renderModalContent() {
    const { Like } = this.props
    const { similarUserInfo, likeTo } = Like
    const avatar = similarUserInfo.user_pictures.length ?
                similarUserInfo.user_pictures['0'].thumb_image_url : showDefaultAvatar(similarUserInfo.gender, 'small')
    const name = similarUserInfo.nick_name
    let headingTitle = `Bạn vừa gửi lượt thích đầu tiên đến ${name}`;
    let bottomTitle = `Bạn có thể trò chuyện ngay khi ${name} thích lại bạn`;
    if (similarUserInfo) {
      headingTitle = `Bạn vừa gửi lượt thích đầu tiên đến ${name}`;
      bottomTitle = `Bạn có thể trò chuyện ngay khi ${name} thích lại bạn`;
    } else { // first like receive
      headingTitle = `Bạn vừa nhận được lượt thích đầu tiên từ ${name}`;
      bottomTitle = `Bạn có thể trò chuyện ngay khi bạn thích lại ${name}`;
    }

    return (
      <div>
        <div className="mbs txt-center"><img className="mbs img-50" src={`${CDN_URL}/general/like.png`} />
          <div className="padding-b20"><div>Gửi thích thành công.</div><span className="txt-blue txt-medium">{similarUserInfo.nick_name}</span> cũng giống <span className="txt-blue txt-medium">{likeTo.nick_name}</span> đấy.</div>
          <div className="txt-blue txt-medium">Thử thích xem sao nhé!</div>
        </div>
        <div className="card mbm">
          <UserPicture user={similarUserInfo} />
        </div>
        <div className="mbs">
          <LikeButton
              user={similarUserInfo}
              page_source = {SIMILAR_MODAL_PAGE}
              forceLike={false}
            />
        </div>
        <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
      </div>
    )
  }

  render() {
    const { Like } = this.props
    const { isSimilarUserModalOpen } = Like

    return (
      <Modal
        transitionName="modal"
        isOpen={isSimilarUserModalOpen}
        onRequestClose={this.closeModal}
        className="SimilarModal modal__content modal__content--3"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
      { isSimilarUserModalOpen &&
        this.renderModalContent()
      }
      </Modal>
    )
  }
}

SimilarModal.propTypes = {
  Like: PropTypes.object,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateSimilarUserModal: () => {
      dispatch(updateSimilarUserModal());
    },
    ignoreUser: (id, page_source) => {
      dispatch(ignoreUser(id, page_source))
    }
  }
}

const SimilarModalContainer = connect(mapStateToProps, mapDispatchToProps)(SimilarModal);

export default SimilarModalContainer
