import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import $ from 'jquery'
import Modal from 'components/modals/Modal'
import { COIN_REWARD_URL, CDN_URL, PEOPLE_LIKED_ME_PAGE } from '../../constants/Enviroment'
import {
  getDailyRemind, closeDailyRemind
} from '../../actions/Daily_Remind';

import {
  UNLOCK_PEOPLE_LIKE_ME_REMIND,
} from '../../constants/Daily_Remind'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class DailyRemindModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    this.handleClick = this.handleClick.bind(this)
    this.closeModal = this.closeModal.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.Daily_Remind !== nextProps.Daily_Remind ||
      this.props.Enviroment.previous_page != nextProps.Enviroment.previous_page
  }

  @GoogleOptimize
  componentDidUpdate(){}

  handleClick() {
    const { Daily_Remind } = this.props;
    const { reward_coins } = Daily_Remind;
    this.props.closeDailyRemind();
    if (reward_coins != '0'){
      return this.showRewardNoti(reward_coins);
    }
  }

  closeModal() {
    this.props.closeDailyRemind();
  }

  showRewardNoti(reward_coins) {
    const reward_coins_url = `${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`
    const RewardNoti = $(`
      <div class="noti noti--bg-blue mbm">
        <div class="noti__body">
          <div class="noti__header mbm">
            <img src= ${reward_coins_url} alt="coin_reward_icon+coin.png" />
            <span class="txt-bold">+${reward_coins}</span>
          </div>
          <div class="noti__content txt-bold">Bạn vừa nhận ${reward_coins} xu!</div>
        </div>
      </div>
    `)

    setTimeout(function() {
      $('body').append(RewardNoti)
    }, 200)

    setTimeout(function() {
      $(RewardNoti).remove()
    }, 5000)
  }

  showRewardNotiOnly(reward_coins, notiText) {
    const reward_coins_url = `${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`
    const RewardNoti = $(`
      <div class="noti noti--bg-blue mbm">
        <div class="noti__body">
          <div class="noti__header mbm">
            <img src= ${reward_coins_url} alt="coin_reward_icon+coin.png" />
            <span class="txt-bold">+${reward_coins}</span>
          </div>
          <div class="noti__content txt-bold">${notiText}</div>
        </div>
      </div>
    `)

    setTimeout(function() {
      $('body').append(RewardNoti)
    }, 200)

    setTimeout(function() {
      $(RewardNoti).remove()
    }, 8000)
  }

  render() {
    const { Daily_Remind, Enviroment } = this.props;
    const { reminds, isOpen, reward_coins } = Daily_Remind;
    const selectedRemind = reminds.find(item => item.isActivated === true);
    if (typeof selectedRemind === 'undefined') {
      return <div></div>
    }

    if (selectedRemind.name === UNLOCK_PEOPLE_LIKE_ME_REMIND && Enviroment.previous_page !== PEOPLE_LIKED_ME_PAGE) {
      return <div></div>
    }
    if (selectedRemind.notiOnly) {
      return this.showRewardNotiOnly(reward_coins, selectedRemind.notiText);
    } else {
      return (
        <div className="container">
          <Modal
            transitionName="modal"
            isOpen={isOpen}
            onRequestClose={this.closeModal}
            className="DailyRemindModal modal__content modal__content--2"
            overlayClassName="modal__overlay"
            portalClassName="modal"
            contentLabel=""
          >
            <div className="modal__content__frame modal__content__frame--1">
              <img className="img-firstlike" src={selectedRemind.image} alt="" />
            </div>
            <div className="modal__txt-content">
              <div className="txt-center padding-t20 padding-b10" >
                <p className="banner__sub-heading txt-bold" dangerouslySetInnerHTML={{ __html: selectedRemind.text }}></p>
              </div>
              <button className="btn btn--p btn--b mbs" onClick={this.handleClick}>{selectedRemind.button_text}</button>
              <div className="txt-blue txt-md txt-center">{selectedRemind.next_target_text}</div>
            </div>
            <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
          </Modal>
        </div>
      )
    }
  }
}

DailyRemindModal.propTypes = {
  gaSend: PropTypes.func,
}



function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    closeDailyRemind: () => {
      dispatch(closeDailyRemind())
    }
  }
}

const DailyRemindModalContainer = connect(mapStateToProps, mapDispatchToProps)(DailyRemindModal);

export default DailyRemindModalContainer;