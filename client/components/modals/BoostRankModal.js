import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router';
import { connect } from 'react-redux'
import Modal from 'react-modal'
import {
  BOOST_RANK_MODAL, BOOST_RANK_COST
} from '../../constants/Enviroment'
import BoostInRankButton from '../commons/BoostInRankButton'
import * as ymmStorage from '../../utils/ymmStorage';
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class BoostRankModal extends Component {

  @GoogleOptimize
  componentDidUpdate(){}

  rememberPaymentIntention() {
    ymmStorage.setItem('Payment_Intention', 'PAYMENT_INTENTION_BOOST_RANK');
  }

  render() {
    const coins = this.props.current_user ? this.props.current_user.coins : ''
    const { user_profile } = this.props;
    const { current_user, lucky_spinner_gifts } = user_profile;
    const modalTxt = lucky_spinner_gifts.boost_in_rank_free_gift > 0 ?
                     `Bạn còn ${lucky_spinner_gifts.boost_in_rank_free_gift} lần nổi bật miễn phí!` :
                     `Số xu hiện có: ${coins} xu`
    if (!this.props.isOpen) {
      return <div></div>
    }
    return (
      <div>
        <Modal
          isOpen={this.props.isOpen}
          onRequestClose={this.props.onRequestClose}
          className="BoostRankModal modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
           <div className="txt-center">
              <img className="img-firstlike" src={BOOST_RANK_MODAL} alt="" />
            </div>
          <div className="modal__txt-content">
            <div className="txt-center padding-t20 padding-b20">{modalTxt}</div>
            <BoostInRankButton
              current_user={this.props.current_user}
              boostRankCallBack={this.props.onRequestClose}
              gaSend={this.props.gaSend}
              page={this.props.page}
            />
            {coins < BOOST_RANK_COST && current_user.gender == 'male' && current_user.user_type != 4 && lucky_spinner_gifts.boost_in_rank_free_gift == 0 &&
              <div className="txt-center padding-t5">
              hoặc <Link to="/payment/upgrade" onClick={() => {this.rememberPaymentIntention()}} className="txt-underline">Nâng cấp tài khoản</Link>
              </div>
            }
          </div>
          <button className="modal__btn-close" onClick={this.props.onRequestClose}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    )
  }
}

BoostRankModal.propTypes = {
  current_user: PropTypes.object,
  isOpen: PropTypes.bool,
  onRequestClose: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps() {
  return {
  }
}
const BoostRankModalContainer = connect(mapStateToProps, mapDispachToProps)(BoostRankModal);

export default BoostRankModalContainer
