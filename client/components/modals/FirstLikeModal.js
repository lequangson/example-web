import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { browserHistory } from 'react-router'
import { isEmpty } from 'lodash';
import { connect } from 'react-redux'
import { updateFirstLikeModal } from '../../actions/Like';
import { updateFirstLikeStatus, updateFirstLikeStatusViaApi } from '../../actions/userProfile';
import GoogleOptimize from '../../utils/google_analytic/google_optimize';
import { subscribeUser } from '../../src/scripts/userSubscription'
import { userSubscription } from '../../actions/GCMNotification'
import {
  CDN_URL
} from '../../constants/Enviroment'

class FirstLikeModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSendingLike: false,
    }
    this.showModal = this.showModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.showNotificationRequest = this.showNotificationRequest.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.Like.isFirstLikeModalOpen && nextProps.Like.isFirstLikeModalOpen) {
      this.props.updateFirstLikeStatus(true);
    }
  }

  @GoogleOptimize
  componentDidUpdate(){}

  showModal() {
    this.props.updateFirstLikeModal(true)
  }

  closeModal() {
    this.props.updateFirstLikeModal()
  }

  showNotificationRequest(props) {
    if (!("Notification" in window)) {
      return;
    }
    if (Notification.permission == 'denied') {
      browserHistory.push('/noti-instruction')
      this.props.updateFirstLikeModal()
      return;
    }
    subscribeUser(props)
    this.props.updateFirstLikeModal()
  }

  render() {
    const { Like, user_profile } = this.props
    const { isFirstLikeModalOpen, firstLikeSent, firstLikeReceived, firstLikeInfo } = Like


    return (
      <Modal
        transitionName="modal"
        isOpen={isFirstLikeModalOpen}
        onRequestClose={this.closeModal}
        className="BoostRankModal modal__content modal__content--2"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
         <div className="txt-center">
            <img className="img-firstlike" src={`${CDN_URL}/general/new_first_like.jpg`} alt="" />
          </div>
        <div className="modal__txt-content">
          <div className="txt-center txt-bold padding-t20 padding-b20"> Bật thông báo để nhận ngay tin nhắn khi người ấy thích lại bạn</div>
          <button
            className="btn btn--b btn--p"
            onClick={() => this.showNotificationRequest(this.props)}
          >
            Bật thông báo
          </button>
        </div>
        <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
      </Modal>
    )
  }
}

FirstLikeModal.propTypes = {
  Like: PropTypes.object,
  user_profile: PropTypes.object,
  getPeopleMatchedMe: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateFirstLikeModal: (isOpen = false, firstLikeInfo = {}) => {
      dispatch(updateFirstLikeModal(isOpen, firstLikeInfo));
    },
    userSubscription: (endpoint) => {
      dispatch(userSubscription(endpoint))
    },
    updateFirstLikeStatus: (status = false) => {
      dispatch(updateFirstLikeStatus(status));
      if (status) {
        dispatch(updateFirstLikeStatusViaApi());
      }
    }
  }
}

const FirstLikeModalContainer = connect(mapStateToProps, mapDispatchToProps)(FirstLikeModal);

export default FirstLikeModalContainer