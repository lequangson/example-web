import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { connect } from 'react-redux'
import { updateMatchModal } from '../../actions/Like'
import { MATCH_IMAGE } from '../../constants/Enviroment'
import { compareIdentifiers } from '../../utils/common'
import StartChatButton from '../commons/StartChatButton'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import {
  getPeopleMatchedMe,
  getPeopleMatchedMeFailure,
} from '../../actions/userProfile'

import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class MatchModal extends React.Component {

  static preloadMatchImage() {
    const img = new Image()
    img.src = MATCH_IMAGE
  }

  constructor(props) {
    super(props)
    this.showModal = this.showModal.bind(this)
    this.closeModal = this.closeModal.bind(this);
  }

  componentWillMount() {
    MatchModal.preloadMatchImage()
  }

  componentWillUpdate(nextProps) {
    if (nextProps.Like.isMatchModalOpen && !this.props.Like.isMatchModalOpen) {
      const { Like } = nextProps
      const { matchInfo } = Like
      const myUser = nextProps.user_profile.current_user
      const theirId = compareIdentifiers(myUser.identifier, matchInfo.like_from) ?
                      matchInfo.like_to : matchInfo.like_from;
      const theirUser = this.getUserFromMatchUsers(theirId);
      if (!theirUser) {
        this.props.getPeopleMatchedMe();
      }
    }
  }

  @GoogleOptimize
  componentDidUpdate(){}

  getUserFromMatchUsers(identifier) {
    const user = this.props.user_profile.matched_users.find(u =>
      compareIdentifiers(u.identifier, identifier)
    )
    return user
  }

  showModal() {
    this.props.updateMatchModal(true)
  }

  closeModal() {
    this.props.updateMatchModal()
  }

  renderModalContent() {
    const { Like } = this.props
    const { matchInfo } = Like
    const myUser = this.props.user_profile.current_user
    const myAvatar = myUser.user_pictures.length ?
                myUser.user_pictures['0'].thumb_image_url : showDefaultAvatar(myUser.gender, 'small')
    const myName = myUser.nick_name

    let theirAvatar = ''
    let theirName = ''
    let theirUser = {}

    if (matchInfo) {
      const theirId = compareIdentifiers(myUser.identifier, matchInfo.like_from) ?
                      matchInfo.like_to : matchInfo.like_from
      theirUser = this.getUserFromMatchUsers(theirId)
      if (!theirUser) {
        // this load is required for Start Chat Button to work. Or else we could have queried to get one user only
        this.props.getPeopleMatchedMe()
        theirUser = this.getUserFromMatchUsers(theirId)
        // if still can't get the user, then do not show match popup
        if (!theirUser) {
          return (<div></div>)
        }
      }
      theirAvatar = theirUser.user_pictures.length ?
                  theirUser.user_pictures['0'].thumb_image_url : showDefaultAvatar(theirUser.gender, 'small')
      theirName = theirUser.nick_name
    }

    return(
      <div>
        <div className="modal__content__frame">
          <img src={MATCH_IMAGE} alt="Promotion" />

          <div className="modal__content__absolute">
            <div className="modal__content__left">
              <div className="frame frame--xsm mbs">
                <img src={myAvatar} alt="avatar" />
              </div>
              <div className="txt-blue txt-bold txt-center">{myName}</div>
            </div>
            <div className="modal__content__right">
              <div className="frame frame--xsm mbs">
                <img src={theirAvatar} alt="avatar" />
              </div>
              <div className="txt-blue txt-bold txt-center">{theirName}</div>
            </div>
          </div>

          <div className="modal__content__absolute-2" >
            <StartChatButton user={theirUser} shortText={false} page_source='MatchModal'/>
          </div>
        </div>
        <button
          className="modal__btn-close"
          onClick={this.closeModal}
        >
          <i className="fa fa-times"></i>
        </button>
      </div>
    )
  }

  render() {
    const { Like } = this.props
    const { isMatchModalOpen } = Like

    return (
      <Modal
        transitionName="modal"
        isOpen={isMatchModalOpen}
        className="MatchModal modal__content modal__content--2 z-index-1400"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
        onRequestClose={this.closeModal}
      >
        { isMatchModalOpen && this.renderModalContent()}
      </Modal>
    )
  }
}

MatchModal.propTypes = {
  updateMatchModal: PropTypes.func,
  Like: PropTypes.object,
  user_profile: PropTypes.object,
  getPeopleMatchedMe: PropTypes.func,
}


function mapStateToProps(state) {
  return {
    Like: state.Like,
    user_profile: state.user_profile
  }
}

function mapDispatchToProps(dispatch) {
  return {
    updateMatchModal: (isMatchModalOpen = false, user = {}) => {
      dispatch(updateMatchModal(isMatchModalOpen, user))
    },
    getPeopleMatchedMe: () => {
      dispatch(getPeopleMatchedMe()).then(() => {
      }).catch(() => {
        dispatch(getPeopleMatchedMeFailure())
      })
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(MatchModal);
