import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { connect } from 'react-redux'
import q from 'q'
import _ from 'lodash'
import { updateLikedMyPhotoModal } from '../../actions/userProfile'
import UserPicture from '../commons/UserPicture'
import LikeButton from '../commons/LikeButton'
import { ENVELOP_ICON, LIKE_2_ICON, NOTIFICATION_ERROR, NOTIFICATION_NOT_ENOUGH_LIKE, LIKE_STYLE_ICON } from '../../constants/Enviroment'
import { ERROR_LABEL_NOT_ENOUGH_LIKE, ERROR_MSG_NOT_ENOUGH_LIKE_FOR_MSG } from '../../constants/TextDefinition'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class PeopleLikedMyPhotoModal extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      isLikeModalOpen: false,
      currentMsgNumber: -1,
      isSendingLike: false,
      user: {
        identifier: '',
      },
    }
    this.closeLikeModal = this.closeLikeModal.bind(this)
  }

  componentWillMount() {
    const user = this.props.data[0]
    this.setState({ user })
  }

  @GoogleOptimize
  componentDidUpdate(){}

  closeLikeModal() {
    const isLikeModalOpen = false
    const isLikeMessageModalOpen = false
    const likeFrom = {}
    const likeTo = {}
    this.props.setLikeInfo(likeFrom, likeTo, isLikeModalOpen, isLikeMessageModalOpen)
  }



  render() {
    const { Like } = this.props
    const { isOpen, isLikeMessageModalOpen, likeFrom, data } = this.props
    if (!isOpen) {
      return <div></div>
    }
    const current_user = this.props.user_profile.current_user
    const userIndex = this.state.user ? this.props.data.findIndex(user => user.identifier == this.state.user.identifier) : ''
    const user = data[0]

    return (
      <div>
        <Modal
          transitionName="modal"
          isOpen={isOpen}
          onRequestClose={this.props.onRequestClose}
          className="PeopleLikedMyPhotoModal modal__content"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="card mbm">
          {this.state.user &&
            <UserPicture user={this.state.user} />
          }
              <button
                className="pagi pagi--prev pagi--2"
                onClick={this.nextUser_OnClick.bind(this, userIndex, -1)}
              >
                <i className="fa fa-chevron-left"></i>
              </button>
              <button
                className="pagi pagi--next pagi--2"
                onClick={this.nextUser_OnClick.bind(this, userIndex, 1)}
              >
                <i className="fa fa-chevron-right"></i>
              </button>
          </div>
          <div className="mbs">
          { this.state.user &&
            <LikeButton
              user={this.state.user}
              user_profile={current_user}
              forceLike
              msg=''
            />
          }

          </div>
          <button className="modal__btn-close" onClick={this.props.onRequestClose}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}

PeopleLikedMyPhotoModal.propTypes = {
  Like: PropTypes.object,
  setLikeInfo: PropTypes.func,
  dictionaries: PropTypes.object,
  user_profile: PropTypes.object,
}


function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    updateLikedMyPhotoModal: (modalInfo) => {
      dispatch(updateLikedMyPhotoModal(modalInfo))
    },
  }
}
const PeopleLikedMyPhotoModalContainer = connect(mapStateToProps, mapDispachToProps)(PeopleLikedMyPhotoModal);

export default PeopleLikedMyPhotoModalContainer
