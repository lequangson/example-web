import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'
import Modal from 'components/modals/Modal'
import _ from 'lodash'
import q from 'q'
import $ from 'jquery'
import { modalShowNext } from 'actions/Enviroment';
import { SEARCH_MIN_HEIGHT, SEARCH_MAX_HEIGHT } from '../../constants/search'
import { updateUserProfile, getCurrentUser } from '../../actions/userProfile';
import { updateWelcomePageCompletion } from 'containers/welcome/Actions';
import { AVATAR_PICTURE, UPLOAD_DEVIDE_MODE } from '../../constants/userProfile';
import { WELCOME_PAGE_4 } from 'containers/welcome/Constants';
import {
  REGEX_NAME, REGEX_CONTAIN_SPECIAL_CHAR, REGEX_OVER_8_NUMBER_DETECTED,
  REGEX_FIRST_CHAR_IS_NUMBER, MODAL_PROMOTION
} from '../../constants/Enviroment'
import { SPECIAL_CHAR_NOT_ALLOWED,
  NAME_NOT_EXCEED_8_NUMBER, NAME_MUST_NOT_START_WITH_NUMBER,
  NAME_MUST_NOT_BE_EMPTY, NAME_MUST_BETWEEN_2_10_CHARACTER,
} from '../../constants/TextDefinition'
import SelectEditable from '../commons/SelectEditable'
import InputEditable from '../commons/InputEditable'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import UploadImageModal from './UploadImageModal'
import { errorNotiWithLabel } from '../../utils/errors/notification'
import * as ymmStorage from '../../utils/ymmStorage';

class WelcomePageModal extends React.Component {
   constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
      imageLoadCount: 0,
      waiting: false,
    }
    this.completeWelcome = this.completeWelcome.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handleUpdateName = this.handleUpdateName.bind(this)
  }

  validateInput() {
    return ($('#height').val() && $('#residence').val() && $('#job_category').val() &&
      $('#job_position').val() && $('#intention_marriage').val() )
  }

  async completeWelcome() {
    if (!this.validateInput()) {
      errorNotiWithLabel('Bạn chưa điền đầy đủ thông tin', 'Vui lòng điền đầy đủ tất cả các mục', 5000)
      return
    }
    const nick_name = this.refs.nick_name.value
    if (!nick_name || !nick_name.replace(/\s/g, '').length) {
      errorNotiWithLabel('Vui lòng nhập lại tên', NAME_MUST_NOT_BE_EMPTY, 5000);
      return;
    }
    if(!nick_name.match(REGEX_NAME) && nick_name) {
      errorNotiWithLabel('Vui lòng nhập lại tên', NAME_MUST_BETWEEN_2_10_CHARACTER, 5000);
      return;
    }
    if (nick_name.match(REGEX_CONTAIN_SPECIAL_CHAR)) {
      errorNotiWithLabel('Vui lòng nhập lại tên', SPECIAL_CHAR_NOT_ALLOWED, 5000);
      return;
    }
    if (nick_name.match(REGEX_FIRST_CHAR_IS_NUMBER)) {
      errorNotiWithLabel('Vui lòng nhập lại tên', NAME_MUST_NOT_START_WITH_NUMBER, 5000);
      return;
    }
    if (nick_name.match(REGEX_OVER_8_NUMBER_DETECTED)) {
      errorNotiWithLabel('Vui lòng nhập lại tên', NAME_NOT_EXCEED_8_NUMBER, 5000);
      return;
    }
    const params = {
      'personal[nick_name]': nick_name,
    }
    await this.props.updateUserProfile(params);
    ymmStorage.removeItem('name_welcome')
    await this.props.updateWelcomePageCompletion(WELCOME_PAGE_4);
    await this.props.modalShowNext(MODAL_PROMOTION);

  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  onImageLoadError(event) {
    const self = this;
    self.setState({waiting: true})
    setTimeout(() => {
      const newImageLoadCount = self.state.imageLoadCount + 1;
      self.setState({imageLoadCount: newImageLoadCount, waiting: false})
    }, 2000);
  }

  handleUpdateName() {
    const nick_name = this.refs.nick_name.value
    ymmStorage.setItem('name_welcome', nick_name)
  }

  render() {
    const { user_profile, dictionaries } = this.props;
    const { current_user } = user_profile
    const isOpen = current_user.welcome_page_completion < WELCOME_PAGE_4 && current_user.provider == 'facebook'

    const { age, height, job_category, residence, hope_meet, intention_marriage } = current_user
    let userAvatar = ''
    const nickName = current_user.nick_name
    if (current_user.user_pictures && current_user.user_pictures.length && current_user.user_pictures[0].is_main) {
      userAvatar = current_user.user_pictures[0].thumb_image_url
    } else {
      userAvatar = showDefaultAvatar(current_user.gender, 'thumb')
    }

    return (
      <div>
        <Modal
          transitionName="modal"
          isOpen={isOpen}
          onRequestClose={this.props.onRequestClose}
          className="modal__content modal__content--5"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          {isOpen &&
            <div>
              <div className="txt-white txt-uppercase mbm txt-center txt-xlg">
                Chào mừng đến với Ymeet.me
              </div>
              <div className="row">
                <div className="col-xs-6 col-xs-offset-3">
                  <div className="card__link mbl">
                    <img
                      src={`${userAvatar}?cnt=${this.state.imageLoadCount}`}
                      alt={nickName}
                      onError={this.onImageLoadError.bind(this)}
                      className="mbs rounded"
                    />
                    <div className="card__bottom-center card__bottom-center--1">
                      <div onClick={this.openModal} className="btn btn--p btn--round l-flex-center txt-xlg">
                        <i className="fa fa-camera"></i>
                      </div>
                    </div>
                  </div>
                  <div className="">
                    <input
                      defaultValue={ymmStorage.getItem('name_welcome') || nickName}
                      id={'update-input-name-facebook'}
                      ref="nick_name"
                      type="text"
                      className="form__input--f txt-xlg txt-center mbm"
                      maxLength="10"
                      placeholder=""
                      onBlur={this.handleUpdateName}
                    />
                  </div>
                  <div className="txt-lg txt-white txt-center mbm">{`${age} tuổi`}</div>
                </div>
              </div>

              <div className="row mbm">
                <div className="col-xs-6">
                  <div className="txt-uppercase txt-medium mbs">Chiều cao</div>
                  <SelectEditable
                    id="height"
                    className="form__input--f"
                    updateUserProfile={this.props.updateUserProfile}
                    name="personal[height]"
                    value={height || 135}
                    data={_.drop(_.times(SEARCH_MAX_HEIGHT), SEARCH_MIN_HEIGHT)}
                    fromWelcomePage={true}
                    pageSource="WelcomePageModal"
                  />
                </div>
                <div className="col-xs-6">
                  <div className="txt-uppercase txt-medium mbs">Nơi ở</div>
                  <SelectEditable
                    id="residence"
                    className="form__input--f"
                    updateUserProfile={this.props.updateUserProfile}
                    name="personal[residence][key]"
                    value={residence.key || 24}
                    data={this.props.dictionaries.cities}
                    fromWelcomePage={true}
                    pageSource="WelcomePageModal"
                  />
                </div>
              </div>

              <div className="row mbm">
                <div className="col-xs-6">
                  <div className="txt-uppercase txt-medium mbs">Nghề nghiệp</div>
                  <SelectEditable
                    className="form__input--f"
                    id="job_category"
                    updateUserProfile={this.props.updateUserProfile}
                    name="personal[job_category][key]"
                    value={job_category.key || 10}
                    data={dictionaries.occupations}
                    fromWelcomePage={true}
                    pageSource="WelcomePageModal"
                  />
                </div>
                <div className="col-xs-6">
                  <div className="txt-uppercase txt-medium mbs">Vị trí công việc</div>
                  <InputEditable
                    className="form__input--f"
                    updateUserProfile={this.props.updateUserProfile}
                    id="job_position"
                    name="personal[job_position]"
                    value={(current_user.job_position || 'Nhân viên')}
                    placeholder={'VD:Trưởng phòng, Nhân viên, …'}
                    maxLength="50"
                    fromWelcomePage={true}
                    pageSource="WelcomePageModal"
                  />
                </div>
              </div>

              <div className="row mbl">
                <div className="col-xs-12">
                  <div className="txt-uppercase txt-medium mbs">Mong muốn kết hôn</div>
                  <SelectEditable
                    className="form__input--f"
                    id="intention_marriage"
                    updateUserProfile={this.props.updateUserProfile}
                    name="personal[intention_marriage][key]"
                    value={intention_marriage.key || 3}
                    data={dictionaries.intention_marriages}
                    fromWelcomePage={true}
                    pageSource="WelcomePageModal"
                  />
                </div>
              </div>

              <div className="row">
                <div className="col-xs-12">
                  <button
                    className="btn btn--b txt-uppercase btn--noradius"
                    onClick={this.completeWelcome}
                  >
                    BẮT ĐẦU NGAY!
                  </button>
                </div>
              </div>
              <Modal
                transitionName="modal"
                isOpen={this.state.modalIsOpen}
                onRequestClose={this.closeModal}
                className="modal__content modal__content--3"
                overlayClassName="modal__overlay"
                portalClassName="modal"
                contentLabel=""
                shouldCloseOnOverlayClick={false}
              >
                <UploadImageModal pictureType={AVATAR_PICTURE} closeModal={this.closeModal} page_source="WELCOME_MODAL" {...this.props} />
                <button className="modal__btn-close" onClick={this.closeModal}>
                  <i className="fa fa-times"></i>
                </button>
              </Modal>
            </div>
          }
        </Modal>
      </div>
    )
  }
}

WelcomePageModal.propTypes = {
  updateUserProfile: PropTypes.func,
  dictionaries: PropTypes.object,
  user_profile: PropTypes.object,
  modalShowNext: PropTypes.func,
  validateImage: PropTypes.func,
  saveImage: PropTypes.func,
}



function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateUserProfile: async (params) => {
      await dispatch(updateUserProfile(params));
    },
    updateWelcomePageCompletion: async (page) => {
      await dispatch(updateWelcomePageCompletion(page));
      await dispatch(getCurrentUser());
    },
    modalShowNext: async (modal) => {
      await dispatch(modalShowNext(modal));
    }
  }
}

const WelcomePageModalContainer = connect(mapStateToProps, mapDispatchToProps)(WelcomePageModal);

export default WelcomePageModalContainer;