import React, { PropTypes } from 'react';
import Modal from 'components/modals/Modal';
import { browserHistory, Link } from 'react-router';
import { connect } from 'react-redux';
import _ from 'lodash';
import RandomMatchItem from '../searches/RandomMatchItem';
import { GA_ACTION_GET_MORE_COIN, RANDOM_MATCH_COST, RANDOM_MATCH_MODAL, RANDOM_SIGN, PAYMENT_INTENTION_RANDOM_MATCH } from '../../constants/Enviroment';
import * as ymmStorage from '../../utils/ymmStorage';
import * as showNotification from '../../utils/notifications/alreadyUpgrade';
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class RandomMatchModal extends React.Component {

  constructor(props) {
    super(props)
    this.handleBuyCoin = this.handleBuyCoin.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}

  handleBuyCoin() {
    this.props.gaSend(GA_ACTION_GET_MORE_COIN, { page_source: 'RANDOM_MATCH_MODAL' })
    this.rememberPaymentIntention()
    browserHistory.push('/payment/coin')
  }

  rememberPaymentIntention() {
    ymmStorage.setItem('Payment_Intention', PAYMENT_INTENTION_RANDOM_MATCH);
  }

  renderUpgradeAccount() {
    const { user, current_user } = this.props;
    if ((!_.isEmpty(current_user) && current_user.permission.number_random_match_free > 0) || current_user.coins >= RANDOM_MATCH_COST || current_user.gender == 'female') {
      return '';
    }
    if (!_.isEmpty(current_user) && current_user.user_type === 4 && current_user.permission.number_random_match_free === 0 && current_user.coins < RANDOM_MATCH_COST) {
      showNotification.showAlreadyUpgrade()
    }
    return (
      <div className="txt-center padding-t5">
        hoặc <Link to="/payment/upgrade" onClick={() => {this.rememberPaymentIntention()}} className="txt-underline">Nâng cấp tài khoản</Link>
      </div>
    )
  }

  render() {
    const data = this.props.data ? this.props.data : []
    const coins = this.props.current_user.coins
    const { hiddenCard, isUpdateRandomMatch, random_match_user, isModalCloseable } = this.props.random_match;
    const { random_match_free_gift } = this.props.user_profile.lucky_spinner_gifts;
    const user_random_match_free = !_.isEmpty(this.props.current_user) ?
    this.props.current_user.permission.number_random_match_free : 0
    const number_random_match_free = random_match_free_gift + user_random_match_free;

    return (
      <Modal
        transitionName="modal"
        isOpen={this.props.isOpen}
        onRequestClose={isModalCloseable ? this.props.onRequestClose : () => {}}
        className="RandomMatchModal modal__content modal__content--4"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
      { number_random_match_free > 0 || coins >= RANDOM_MATCH_COST || (coins < RANDOM_MATCH_COST && data.length > 0) ?
        <div>
          <div className="mbs txt-center"><img className="img-70 padding-t10" src={RANDOM_SIGN} />
            {
              hiddenCard ?
              number_random_match_free > 0 ?
              <div>
                <p>Bạn còn {number_random_match_free} lần ghép đôi miễn phí</p>
                <div className="txt-blue txt-medium">Bấm vào thẻ bạn chọn để mở</div>
              </div>
              :
              <div>
                {
                  !this.props.inCampaign &&
                  <p>25 xu = 1 lần mở thẻ</p>
                }
                <div className="txt-blue txt-medium">Bấm vào thẻ bạn chọn để mở</div>
              </div>
              :
              <div className="txt-medium">Xin chúc mừng.<br/> Bạn đã được ghép đôi ngẫu nhiên với <span className="txt-blue txt-medium">{random_match_user}</span>. <br /> Hai bạn có 24h để trò chuyện. Đừng bỏ lỡ.</div>
            }

          </div>
          <div className="clearfix">
              {data.map((User, i) => <div key={i} className="padding-col-half">
                                   <RandomMatchItem
                                    current_user={this.props.current_user}
                                    data={User}
                                    hiddenCard={hiddenCard}
                                    activeHeart={true}
                                    updateModalState={this.props.updateModalState}
                                    isUpdateRandomMatch={isUpdateRandomMatch}
                                    gaSend={this.props.gaSend}
                                    page_source='RANDOM_MATCH_MODAL'
                                    />
                                 </div>
                        )
              }
          </div>
          <button className="modal__btn-close" onClick={this.props.onRequestClose}><i className="fa fa-times"></i></button>
        </div>
        :
        <div>
          <img className="img-firstlike" src={RANDOM_MATCH_MODAL} alt="" />
          <div className="modal__txt-content">
            <div className="txt-center padding-t20 padding-b20 txt-bold"> Số xu hiện có: <span className="txt-blue">{coins}</span></div>
            <button onClick={() => this.handleBuyCoin()} className="btn btn--p btn--b">Nạp thêm xu</button>
            {this.renderUpgradeAccount()}
          </div>
          <button className="modal__btn-close" onClick={this.props.onRequestClose}><i className="fa fa-times"></i></button>
        </div>
      }
      </Modal>
    )
  }
}

RandomMatchModal.propTypes = {
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {};
}

const RandomMatchModalContainer = connect(mapStateToProps, mapDispatchToProps)(RandomMatchModal);

export default RandomMatchModalContainer
