import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'
import Modal from 'components/modals/Modal'
import Dropzone from 'react-dropzone'
import { COIN_REWARD_URL } from '../../constants/Enviroment'
import { updateBestVotePhotoState } from '../../actions/photoStream';
import { saveFilesUpload } from '../../actions/Enviroment'
import { OTHER_PICTURE, UPLOAD_DEVIDE_MODE } from '../../constants/userProfile'

import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class BestPhotoModal extends React.Component {
   constructor(props) {
    super(props)
    this.onOpenClick = this.onOpenClick.bind(this)
    this.uploadOtherImages = this.uploadOtherImages.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}

  classButton() {
    const { BestVotePhoto } = this.props;
    const { modalInfo } = BestVotePhoto;
    let customClass = "btn btn--4 btn--b txt-center";
    if (modalInfo.gender == 'male'){
      customClass = "btn--p btn--b btn"
    }
    return (
      customClass
    )
  }

  onOpenClick() {
    this.dropzone.open()
  }

  uploadOtherImages(files) {
    // save list uploaded files into state and bring user to upload-images page
    this.props.saveFilesUpload(files, OTHER_PICTURE, UPLOAD_DEVIDE_MODE)
  }

  render() {
    const { BestVotePhoto } = this.props;
    const { modalInfo, banners_image } = BestVotePhoto;
    const picture = modalInfo.user_pictures ? modalInfo.user_pictures.filter(pic => pic.is_main === true).length > 0 ? modalInfo.user_pictures.filter(pic => pic.is_main === true)[0] : modalInfo.user_pictures[0] : {large_image_url: modalInfo.picture_url}
    const icon_url = modalInfo.gender === 'male' ? banners_image[0] : banners_image[1]
    return (
      <div>
        <Modal
          transitionName="modal"
          isOpen={this.props.isOpen}
          onRequestClose={this.props.onRequestClose}
          className="BestPhotoModal modal__content modal__content--3"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="mbm txt-center">
            <img className="img-50" src={icon_url} alt="" />
          </div>
          <div className="card mbm">
            <div className="card__upper">
              <div className="card__link">
                <img src={picture ? picture.large_image_url : ""} alt=""/>
              </div>
              <div className="card__meta card__meta--1">
              {modalInfo.number_photos ?
                <span className="txt-bold">{modalInfo.nick_name} - {modalInfo.number_photos} ảnh</span>
                :
                ''
              }
              </div>
              <div className="card__addon l-flex-vertical-center">
                <span className="txt-bold">{modalInfo.received_votes} vote</span>
              </div>
            </div>
          </div>
          <div className="txt-center">
            <p className="mbm txt-light txt-medium">Tuần này giải nhất sẽ là bạn chứ?</p>
            <Dropzone
              onDrop={this.uploadOtherImages}
              ref={(drz) => { this.dropzone = drz; }}
              multiple style={{ display: 'none' }}
            />
            <button className={this.classButton()} onClick={this.onOpenClick} >
              Tải ảnh ngay!
            </button>
          </div>
          <button className="modal__btn-close" onClick={this.props.onRequestClose}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    )
  }
}

BestPhotoModal.propTypes = {
  updatePreviousPage: PropTypes.func,
  gaSend: PropTypes.func,
  onRequestClose: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateBestVotePhotoState: (modalInfo) => {
      dispatch(updateBestVotePhotoState(modalInfo))
    },
    saveFilesUpload: (files, pictureType = OTHER_PICTURE, uploadType = UPLOAD_DEVIDE_MODE) => {
      dispatch(saveFilesUpload(files, uploadType))
      browserHistory.push(`/upload-image/${pictureType}/${uploadType}`)
    },
  }
}

const BestPhotoModalContainer = connect(mapStateToProps, mapDispatchToProps)(BestPhotoModal);

export default BestPhotoModalContainer;