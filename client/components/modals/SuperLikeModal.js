import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { connect } from 'react-redux'
import UserPicture from '../../components/commons/UserPicture';
import { SIMILAR_MODAL_PAGE, CDN_URL } from '../../constants/Enviroment'
import { compareIdentifiers } from '../../utils/common'
import { getPeopleMatchedMe, getCurrentUser } from '../../actions/userProfile'
import socket from "../../utils/socket";

class SuperLikeModal extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      user_info: {}
    }
    this.closeModal = this.closeModal.bind(this);
    this.chatWithUser = this.chatWithUser.bind(this);
  }

  componentWillMount() {
    socket.on('receive_event', this.receive_event.bind(this));

  }

  receive_event(data) {
    if(data.type == 'show_super_like') {
      this.showSuperLike(data);
    }
  }

  showSuperLike(super_like) {
    const { user_profile } = this.props;
    const { current_user } = user_profile;
    if (compareIdentifiers(current_user.identifier, super_like.to_user_identifier)) {
      this.props.getPeopleMatchedMe();
      this.props.getCurrentUser();
      this.setState({isOpen: true, user_info: super_like.super_like_info});
    }
  }

  chatWithUser(identifier) {
    window.open(`${process.env.CHAT_SERVER_URL}chat/${identifier}`);
    this.setState({isOpen: false, user_info: {}})
  }

  closeModal() {
    this.setState({isOpen: false})
  }

  render() {
    return (
      <Modal
        transitionName="modal"
        isOpen={this.state.isOpen}
        onRequestClose={this.closeModal}
        className="SuperLikeModal modal__content modal__content--3"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        <div className="txt-center" key="1">
          <img className="mbm img-70" src={`${CDN_URL}/super_like/Heart.png`} />
          <div className="mbm txt-bold txt-pink">{this.state.user_info.nick_name} muốn chat luôn với bạn.</div>
        </div>
        <div className="card mbm" key="2">
          <UserPicture user={this.state.user_info} />
        </div>
        <div className="txt-center mbs" key="3">
          <button className="btn btn--4 btn--b" onClick={() => {this.chatWithUser(this.state.user_info.identifier)}} >Chat với {this.state.user_info.nick_name} ngay!</button>
        </div>
        <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
      </Modal>
    )
  }
}

SuperLikeModal.propTypes = {
  user_profile: PropTypes.object,
  getPeopleMatchedMe: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    getPeopleMatchedMe: () => {
      dispatch(getPeopleMatchedMe())
    },
    getCurrentUser: () => {
      dispatch(getCurrentUser())
    }
  }
}

const SuperLikeModalContainer = connect(mapStateToProps, mapDispatchToProps)(SuperLikeModal);

export default SuperLikeModalContainer
