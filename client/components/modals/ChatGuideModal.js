import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'
import $ from 'jquery'
import Modal from 'react-modal'
import { updateUserPermission } from '../../actions/userProfile'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class ChatGuideModal extends React.Component {
   constructor() {
    super()
    this.state = {
      modalIsOpen: false,
    }

    this.modalClose = this.modalClose.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}
  
  modalClose() {
    this.props.updateUserPermission('have_shown_chat_guide', 1)
  }

  getParent() {
    return document.querySelector('#tooltip');
  }

  render() {
    const isOpen = this.props.user_profile.current_user.permission ? !this.props.user_profile.current_user.permission.have_shown_chat_guide : false
    if (!isOpen) {
      return <div></div>
    }

    return (
        <Modal
          isOpen={isOpen}
          parentSelector={this.getParent}
          onRequestClose={this.modalClose}
          className="ChatGuideModal tooltip__container"
          overlayClassName="overlay_tooltip"
          portalClassName="modal"
          contentLabel=""
        >
        <div className="tooltip__content">
          <div className="tooltip__header">
            <i className="fa fa-comments mrs"></i>
            <span>Trò chuyện dễ dàng</span>
            <span className="tooltip__close" onClick={this.modalClose}><i className="fa fa-times"></i></span>
          </div>
          <div>Nếu bạn và người ấy cùng thích nhau thì hai bạn đã ghép đôi thành công và có thể trò chuyện!</div>
        </div>

        <div className="modal__overlay modal__overlay--tooltip"></div>
        </Modal>
    )
  }
}

ChatGuideModal.propTypes = {
}



function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateUserPermission: (name, value) => {
      dispatch(updateUserPermission(name, value))
    },
  }
}

const ChatGuideModalContainer = connect(mapStateToProps, mapDispatchToProps)(ChatGuideModal);

export default ChatGuideModalContainer;