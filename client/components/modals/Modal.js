import React, { Component, PropTypes } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class Modal extends Component {

  constructor(props) {
    super(props);

    this.shouldClose = null;
    this.moveFromContentToOverlay = null;
  }

  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    transitionName: PropTypes.string,
    overlayClassName: PropTypes.string,
    className: PropTypes.string,
    portalClassName: PropTypes.string,
    onRequestClose: PropTypes.func
  }

  static defaultProps = {
    shouldCloseOnOverlayClick: true
  }

  handleOverlayOnClick = () => {
    if (this.shouldClose === null) {
      this.shouldClose = true;
    }
    if (this.shouldClose && this.props.shouldCloseOnOverlayClick) {
      if (this.ownerHandlesClose()) {
        this.props.onRequestClose();
      }
    }
    this.shouldClose = null;
    this.moveFromContentToOverlay = null;
  };

  ownerHandlesClose = () => this.props.onRequestClose;

  handleContentOnClick = () => {
    this.shouldClose = false;
  };

  handleContentOnMouseUp = () => {
    this.shouldClose = false;
  };

  handleContentOnMouseDown = () => {
    this.shouldClose = false;
    this.moveFromContentToOverlay = false;
  };

  render() {
    if(this.props.isOpen) {
      return (
        <ReactCSSTransitionGroup
          transitionName={this.props.transitionName}
          transitionAppear={true}
          transitionEnter={true}
          transitionLeave={true}
          transitionAppearTimeout={250}
          transitionEnterTimeout={250}
          transitionLeaveTimeout={400}
        >
          <div className={this.props.portalClassName}>
            <div className={this.props.overlayClassName} onClick={this.handleOverlayOnClick}>
              <div
                className={this.props.className}
                onMouseDown={this.handleContentOnMouseDown}
                onMouseUp={this.handleContentOnMouseUp}>
                {this.props.children}
              </div>
            </div>
          </div>
        </ReactCSSTransitionGroup>
      );
    } else {
      return <ReactCSSTransitionGroup
                transitionAppear={true}
                transitionEnter={true}
                transitionLeave={true}
                transitionAppearTimeout={250}
                transitionEnterTimeout={250}
                transitionLeaveTimeout={400}
                transitionName={this.props.transitionName} />;
    }
  }
}

export default Modal;