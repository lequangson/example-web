import React, { PropTypes } from 'react'
import { browserHistory, Link } from 'react-router';
import { connect } from 'react-redux'
import { REMIND_BANNER_WITH_COIN, BANNER_REMIND_WITH_COIN_IMAGE, BANNER_ICON_URL } from '../../constants/Enviroment'
import { coinConsume } from '../../actions/Payment';
import { isEmpty } from 'lodash';
import Modal from 'react-modal';
import q from 'q';
import * as sendBannerNotification from '../../utils/notifications/banner';
import { getCurrentUser } from '../../actions/userProfile';
import * as ymmStorage from '../../utils/ymmStorage';
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class RemindBannerWithCoinModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpenConfirmModal: false,
      accept_remind_with_coin: false
    }
    this.handleRemindClick = this.handleRemindClick.bind(this)
    this.closeModal = this.closeModal.bind(this);
  }

  componentWillMount() {
    // this.props.changeBannerStatus(REMIND_BANNER_WITH_COIN)
    ymmStorage.removeItem('BannerRemindWithCoin');
  }

  @GoogleOptimize
  componentDidUpdate(){}
  
  closeModal() {
    this.setState({isOpenConfirmModal: false})
    ymmStorage.removeItem('upgrade_success');
  }

  handleRemindClick() {
    if (this.state.accept_remind_with_coin) {
      return;
    }
    this.setState({accept_remind_with_coin: true})
    this.props.acceptRemindWithCoin().then(res => {
      ymmStorage.removeItem('upgrade_success');
      sendBannerNotification.remindBannerWithCoinSuccess()
      const gaTrackData = {
        page_source: 'REMIND_BANNER_WITH_COIN'
      };
      const eventAction = {
        eventCategory: 'Coin',
        eventAction: 'Buy',
        eventLabel: 'Buy remind banner with coin'
      }
      this.props.sendGoogleAnalytic(eventAction, gaTrackData)
      this.setState({accept_remind_with_coin: false, isOpenConfirmModal: false})
      this.props.getCurrentUser()
    }).catch(() => {this.setState({accept_remind_with_coin: false})})
  }

  render() {
    if (!ymmStorage.getItem('upgrade_success')) {
      return <div></div>
    }
    const { user_profile } = this.props;
    const { current_user } = user_profile;
    const { coins, permission } = current_user;
    const forFree = permission != null && permission.send_boost_in_rank_free;
    return (
      <Modal
        isOpen={true}
        onRequestClose={this.closeModal}
        className="RemindBannerWithCoinModal modal__content modal__content--2"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        <div className="txt-center">
            <img className="img-firstlike" src={BANNER_REMIND_WITH_COIN_IMAGE} alt="" />
          </div>
        <div className="modal__txt-content">
          <div className="txt-center padding-t20 padding-b20"> Bạn có 1 tháng sử dụng miễn phí</div>
          <div className="mbs txt-center">
            <button className="btn btn--p btn--b" onClick={this.handleRemindClick} >Gây ấn tượng ngay!</button>
          </div>
        </div>
        <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
      </Modal>
    )
  }

}

RemindBannerWithCoinModal.propTypes = {
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    acceptRemindWithCoin: () => {
      const defer = q.defer();
      dispatch(coinConsume('coin_consume_remind_liked_partners')).then(res => {
        defer.resolve(res);
      }).catch(() => {
        defer.reject(false);
      })
      return defer.promise;
    },
    getCurrentUser: () => {
      dispatch(getCurrentUser())
    },
  }
}

const RemindBannerWithCoinModalContainer = connect(mapStateToProps, mapDispatchToProps)(RemindBannerWithCoinModal);

export default RemindBannerWithCoinModalContainer
