import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import { Link, browserHistory } from 'react-router'
import { PAYMENT_MODAL_IMAGE, GA_ACTION_PAYMENT_LEARN_MORE, WHO_ADD_ME_TO_FAVOURITE_PAGE } from '../../constants/Enviroment'
import { gaSend } from '../../actions/Enviroment'
import * as ymmStorage from '../../utils/ymmStorage';
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class PaymentModal extends Component {
  constructor() {
    super()
    this.state = {
      modalIsOpen: false,
    }
    this.modalOpen = this.modalOpen.bind(this)
    this.modalClose = this.modalClose.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}
  
  handleClick() {
    this.props.gaSend(GA_ACTION_PAYMENT_LEARN_MORE, {page_source: this.props.Enviroment.previous_page});
    this.modalClose()
    if (this.props.page === WHO_ADD_ME_TO_FAVOURITE_PAGE) {
      ymmStorage.setItem('Payment_Intention', WHO_ADD_ME_TO_FAVOURITE_PAGE)
    }
    browserHistory.push('/upgrade-instruction')
  }

  modalOpen() {
    this.setState({ modalIsOpen: true })
  }

  modalClose() {
    this.setState({ modalIsOpen: false })
  }

  render() {
    if (!this.props.isOpen) {
      return <div></div>
    }
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        className="PaymentModal modal__content modal__content--2"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
         <div className="txt-center">
            <img className="img-firstlike" src={PAYMENT_MODAL_IMAGE} alt="" />
          </div>
        <div className="modal__txt-content">
          <div className="txt-center padding-t20 padding-b20">Để xem được danh sách này và mở nhiều tính năng hẹn hò không giới hạn!</div>
          <button
            className="btn txt-center btn--p btn--b"
            onClick={this.handleClick}
          >
            <span className="txt-bold">Nâng cấp tài khoản ngay!</span>
          </button>
        </div>
        <button className="modal__btn-close" onClick={this.props.onRequestClose}><i className="fa fa-times"></i></button>
      </Modal>
    )
  }
}

PaymentModal.propTypes = {
  modalShowNext: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    gaSend: (eventAction, ga_track_data) => {
      dispatch(gaSend(eventAction, ga_track_data))
    },
  }
}

const PaymentModalContainer = connect(mapStateToProps, mapDispachToProps)(PaymentModal);

export default PaymentModalContainer