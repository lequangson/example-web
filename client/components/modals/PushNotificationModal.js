import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'
import $ from 'jquery'
import Modal from 'components/modals/Modal'
import { COIN_REWARD_URL, PUSH_NOTI_MODAL_LIKE, PUSH_NOTI_MODAL_FOOTPRINT, BROKEN_HEART, HAPPY_HEART, PUSH_NOTI_MODAL_HEADER } from '../../constants/Enviroment'
import { updatePushNotiModal, sendGoogleAnalytic } from '../../actions/Enviroment'
import { userSubscription } from '../../actions/GCMNotification'
import { subscribeUser } from '../../src/scripts/userSubscription'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class PushNotificationModal extends React.Component {
   constructor() {
    super()
    this.state = {
      modalIsOpen: false,
    }

    this.modalOpen = this.modalOpen.bind(this)
    this.modalClose = this.modalClose.bind(this)
    this.showNotificationRequest = this.showNotificationRequest.bind(this)
    this.sendGA = this.sendGA.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}

  modalOpen() {
    this.props.updatePushNotiModal(true)
  }

  showNotificationRequest(props) {
    this.sendGA(1)
    if (!("Notification" in window)) {
      return;
    }
    if (Notification.permission == 'denied') {
      browserHistory.push('/noti-instruction')
      this.props.updatePushNotiModal(false);
      return;
    }
    subscribeUser(props)
    this.props.updatePushNotiModal(false)
  }

  sendGA(type) {
    const modal = this.props.Enviroment.pushNotiModalType == PUSH_NOTI_MODAL_LIKE ? 'PUSH_NOTI_MODAL_LIKE' : 'PUSH_NOTI_MODAL_FOOTPRINT'
    const gaTrackData = {
      page_source: this.props.Enviroment.previous_page + ', ' + modal,
    };
    const eventAction = {
      eventCategory: 'Push Notification',
      eventAction: 'Click',
      eventLabel: type === 1 ? 'Click Yes' : 'Click No'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
  }

  modalClose() {
    this.props.updatePushNotiModal(false)
  }

  render() {
    const isOpen = this.props.Enviroment.isPushNotiModalOpen
    const type = this.props.Enviroment.pushNotiModalType
    const nick_name = this.props.Enviroment.pushNotiModalNickname

    return (
      <div className="container">
        <Modal
          transitionName="modal"
          isOpen={isOpen}
          onRequestClose={this.modalClose}
          className="PushNotificationModal modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <img className="img-firstlike" src={PUSH_NOTI_MODAL_HEADER} alt="" />
          <div className="modal__txt-content">
            <div className="txt-center" >
            {type === PUSH_NOTI_MODAL_LIKE ?
              <div>
                <p className="txt-blue txt-bold">Thật đáng tiếc nếu bạn bỏ lỡ kết đôi với {nick_name}.</p>
                <img src={BROKEN_HEART} alt="broken-heart" className="img-50"/>
                <p>Nhấn "Cho phép" khi Ymeet.me muốn gửi thông báo cho bạn nhé!</p>
              </div>
              :
              <div>
                <p className="txt-blue txt-bold">Nhận thông báo về những người ghé thăm bạn.</p>
                <img src={HAPPY_HEART} alt="happy-heart" className="img-50"/>
                <p >Nhấn "Cho phép" để biết ai quan tâm đến mình nhé!</p>
              </div>
            }

            </div>
            <div className="row">
            <div className="col-xs-6">
              <button
                className="btn btn--b btn--5 txt-thin"
                onClick={() => {
                  this.sendGA(2)
                  this.modalClose()
                }
                }
              >Bỏ qua</button>
            </div>
            <div className="col-xs-6">
              <button
                className="btn btn--b btn--p txt-thin"
                onClick={() => this.showNotificationRequest(this.props)}
              >Đồng ý</button>
            </div>
            </div>
          </div>
          <button className="modal__btn-close" onClick={this.modalClose}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    )
  }
}

PushNotificationModal.propTypes = {
  updatePreviousPage: PropTypes.func,
  gaSend: PropTypes.func,
}



function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updatePushNotiModal: (isOpen, modalType, nick_name) => {
      dispatch(updatePushNotiModal(isOpen, modalType, nick_name))
    },
    userSubscription: (endpoint) => {
      dispatch(userSubscription(endpoint))
    },
    sendGoogleAnalytic: (eventAction, ga_track_data) =>{
      dispatch(sendGoogleAnalytic(eventAction, ga_track_data))
    },
  }
}

const PushNotificationModalContainer = connect(mapStateToProps, mapDispatchToProps)(PushNotificationModal);

export default PushNotificationModalContainer;