import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { browserHistory } from 'react-router'
import { connect } from 'react-redux'
import _ from 'lodash'
import q from 'q'
import UserPicture from '../commons/UserPicture'
import LikeButton from '../commons/LikeButton'
import { getRemindRandomMatch } from '../../actions/random_match'
import { modalShowNext } from '../../actions/Enviroment'
import { GA_ACTION_GET_MORE_COIN, RANDOM_MATCH_COST, RANDOM_MATCH_MODAL, RANDOM_SIGN, MODAL_REMIND_RANDOM_MATCH } from '../../constants/Enviroment'
import * as ymmStorage from '../../utils/ymmStorage';
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class RemindRandomMatchModal extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
    }
    this.hideModal = this.hideModal.bind(this)
  }

  componentWillMount() {
    // this.props.getRemindRandomMatch()
  }

  componentWillReceiveProps(nextProps) {
    const data = this.props.random_match.remind
    const modalShowNext = nextProps.Enviroment.modalShowNext
    if (modalShowNext === MODAL_REMIND_RANDOM_MATCH && !ymmStorage.getItem('no_need_remind_random_match')) {
      this.props.modalShowNext('MODAL')
      this.props.getRemindRandomMatch().then(() => {
        if(!_.isEmpty(this.props.random_match.remind)) {
          this.setState({ isOpen: true})
        } else {
          ymmStorage.setItem('no_need_remind_random_match', true)
        }
      })
    }
  }

  @GoogleOptimize
  componentDidUpdate(){}

  hideModal() {
    ymmStorage.setItem('no_need_remind_random_match', true)
    this.setState({ isOpen: false })
  }

  render() {
    const data = this.props.random_match.remind ? this.props.random_match.remind : {}
    const current_user = this.props.user_profile.current_user
    const nick_name = data ? data.nick_name : ''

    return (
      <Modal
        transitionName="modal"
        isOpen={this.state.isOpen}
        onRequestClose={this.hideModal}
        className="RemindRandomMatchModal modal__content"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        <div>
          <div className="mbs txt-center"><img className="img-50" src={RANDOM_SIGN} />
            <div className="txt-medium">Ghép đôi ngẫu nhiên thế nào? <br/> <span className="txt-blue txt-medium">Bạn thích {nick_name} chứ?</span></div>
          </div>
          <div className="card mbm">
            <UserPicture go_to_profile={true} user={data} />
          </div>
          <LikeButton
            user={data}
            user_profile={current_user}
            forceLike
            msg=''
          />
          <button className="modal__btn-close" onClick={this.hideModal}><i className="fa fa-times"></i></button>
        </div>
      </Modal>
    )
  }
}

RemindRandomMatchModal.propTypes = {
  user_profile: PropTypes.object,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    getRemindRandomMatch: () => {
      const defer = q.defer()
      dispatch(getRemindRandomMatch()).then((response) => {
        defer.resolve(response)
      }).catch((err) => {
        defer.reject(err)
      })
      return defer.promise
    },
    modalShowNext: (modalName) => {
      dispatch(modalShowNext(modalName))
    },
  }
}

const RemindRandomMatchModalContainer = connect(mapStateToProps, mapDispatchToProps)(RemindRandomMatchModal);

export default RemindRandomMatchModalContainer
