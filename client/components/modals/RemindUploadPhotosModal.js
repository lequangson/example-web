import React from 'react'
import { Link } from 'react-router'
import Modal from 'components/modals/Modal'
import FileInput from 'react-file-reader-input'
import { AVATAR_PICTURE, OTHER_PICTURE, UPLOAD_DEVIDE_MODE } from '../../constants/userProfile'
import { OTHER_PROFILE_PAGE, GA_ACTION_UPLOAD_AVATAR_DEVICE, GA_ACTION_UPLOAD_AVATAR_FACEBOOK, MY_PROFILE_PAGE, CDN_URL } from '../../constants/Enviroment'
import Dropzone from 'react-dropzone'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class RemindUploadPhotosModal extends React.Component {
  constructor(props) {
    super(props)
    this.uploadOtherImages = this.uploadOtherImages.bind(this)
    this.onOpenClick = this.onOpenClick.bind(this)
  }

  componentWillMount() {
    this.props.updateUploadPicturePageSource(OTHER_PROFILE_PAGE)
  }

  @GoogleOptimize
  componentDidUpdate(){}

  uploadOtherImages(files) {
    // save list uploaded files into state and bring user to upload-images page
    this.props.saveFilesUpload(files, OTHER_PICTURE, UPLOAD_DEVIDE_MODE)
  }

  onOpenClick() {
    this.dropzone.open()
  }

  render() {
    return(
      <Modal
        transitionName="modal"
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        className="RemindUploadPhotosModal modal__content modal__content--3"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        <div className="well well--a well--sm mbm">
          <div className="txt-center txt-bold">Thêm ảnh phụ</div>
        </div>
        <p className="txt-center">Bạn cần có hơn 3 ảnh phụ để xem thêm ảnh</p>
        <div>
          <Dropzone
            onDrop={this.uploadOtherImages}
            ref={(drz) => { this.dropzone = drz; }}
            multiple style={{ display: 'none' }}
          />
          <button className="btn btn--b btn--p mbm" onClick={this.onOpenClick} >
            Tải ảnh từ thiết bị
          </button>
        </div>
        {
          this.props.user_profile.current_user.provider === 'facebook' &&
          <Link to={`/fb-album/${OTHER_PICTURE}`} className="btn btn--b btn--p mbm">Tải ảnh từ Facebook</Link>
        }

        <div className="modal__extra txt-center">
          <div className="txt-bold txt-lg txt-uppercase mbm">Gợi ý từ Ymeet.me</div>

          {(this.props.gender === 'female') ? (
            <div className="l-flex-cspa mbm">
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nu-4.png`} />
                </div>
                <div>Hãy là<br />chính bạn</div>
              </div>
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nu-5.png`} />
                </div>
                <div>Ảnh<br />cuộc sống</div>
              </div>
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nu-6.png`} />
                </div>
                <div>Ảnh thật,<br />rõ nét</div>
              </div>
            </div>
          ) : (
            <div className="l-flex-cspa mbm">
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nam-4.png`} />
                </div>
                <div>Hãy là<br />chính bạn</div>
              </div>
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nam-5.png`} />
                </div>
                <div>Ảnh<br />cuộc sống</div>
              </div>
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nam-6.png`} />
                </div>
                <div>Ảnh thật,<br />rõ nét</div>
              </div>
            </div>
          )}
          <div className="well well--sm">
            <Link to="/helps/6/3" className="txt-underline">Tham khảo bí kíp chọn ảnh hợp lệ</Link>
          </div>
        </div>
        <button className="modal__btn-close" onClick={this.props.onRequestClose}>
          <i className="fa fa-times"></i>
        </button>
      </Modal>
    )
  }
}

export default RemindUploadPhotosModal
