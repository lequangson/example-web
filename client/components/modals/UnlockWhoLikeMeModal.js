import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'components/modals/Modal'
import $ from 'jquery'
import { UNLOCK_WHO_LIKE_ME_MODAL, CDN_URL } from '../../constants/Enviroment'
import CoinConsumeButton from '../commons/CoinConsumeButton'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class UnlockWhoLikeMeModal extends Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}

  handleClick(){
    return this.showRewardNoti();
  }

  render() {
    const coins = this.props.current_user ? this.props.current_user.coins : ''
    return (
      <div>
        <Modal
          transitionName="modal"
          isOpen={this.props.isOpen}
          onRequestClose={this.props.onRequestClose}
          className="UnlockWhoLikeMeModal modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="modal__content__frame modal__content__frame--1">
            <img className="img-firstlike" src={UNLOCK_WHO_LIKE_ME_MODAL} alt="" />
          </div>
          <div className="modal__txt-content">
            <div className="txt-center padding-t20 padding-b20 txt-medium txt-light"> Số xu hiện có: <span className="txt-blue">{coins} xu</span></div>
            <CoinConsumeButton
              current_user={this.props.current_user}
              coinConsumeCallBack={this.props.onRequestClose}
              gaSend={this.props.gaSend}
              page={this.props.page}
            />
          </div>
          <button className="modal__btn-close" onClick={this.props.onRequestClose}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    )
  }
}

UnlockWhoLikeMeModal.propTypes = {
  current_user: PropTypes.object,
  isOpen: PropTypes.bool,
  onRequestClose: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps() {
  return {
  }
}
const UnlockWhoLikeMeModalContainer = connect(mapStateToProps, mapDispachToProps)(UnlockWhoLikeMeModal);

export default UnlockWhoLikeMeModalContainer
