import React, { PropTypes } from 'react'
import FileInput from 'react-file-reader-input'
import Dropzone from 'react-dropzone'
import { Link, browserHistory } from 'react-router'
import { AVATAR_PICTURE, OTHER_PICTURE, UPLOAD_DEVIDE_MODE } from '../../constants/userProfile'
import { CDN_URL } from '../../constants/Enviroment'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class UploadImageModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    this.uploadAvatar = this.uploadAvatar.bind(this)
    this.uploadOtherImages = this.uploadOtherImages.bind(this)
    this.onOpenClick = this.onOpenClick.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}

  onOpenClick() {
    this.dropzone.open()
  }

  uploadAvatar(e, results) {
    results.forEach((result) => {
      const [, file] = result;
      this.props.validateImage(AVATAR_PICTURE, file.name, file.size, false).then(() => {
        this.props.saveImage(file)
      })
    });
    this.props.closeModal();
  }

  uploadOtherImages(files) {
    this.props.updateScrollPossition(this.props.page_source, window.scrollY)
    // save list uploaded files into state and bring user to upload-images page
    this.props.saveFilesUpload(files, OTHER_PICTURE, UPLOAD_DEVIDE_MODE)
  }

  renderTitle() {
    if (this.props.pictureType === AVATAR_PICTURE) {
      return (
        <div className="well well--a well--sm mbm">
          <div className="txt-center txt-bold">Thay đổi ảnh đại diện</div>
        </div>
      )
    }
    else {
      return (
        <div className="well well--a well--sm mbm">
          <div className="txt-center txt-bold">Thêm ảnh phụ</div>
        </div>
      )
    }
  }

  renderUploadImageComponent() {
    if (this.props.pictureType === AVATAR_PICTURE) {
      return (
        <FileInput id="my-file-input" onChange={this.uploadAvatar} accept="image/*">
          <button className="btn btn--b btn--p mbm">Tải ảnh từ thiết bị</button>
        </FileInput>
      )
    }
    else {
      return (
        <div>
          <Dropzone
            onDrop={this.uploadOtherImages}
            ref={(drz) => { this.dropzone = drz; }}
            multiple style={{ display: 'none' }}
          />
          <button className="btn btn--b btn--p mbm" onClick={this.onOpenClick} >
            Tải ảnh từ thiết bị
          </button>
        </div>
      )
    }
  }

  renderAvatarFemaleSusgestion() {
    return (
      <div className="l-flex-cspa mbm">
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nu-1.png`} />
          </div>
          <div>Cười lên<br />nào!</div>
        </div>
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nu-2.png`} />
          </div>
          <div>Rõ ràng,<br />đủ sáng</div>
        </div>
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nu-3.png`} />
          </div>
          <div>Ảnh<br />toàn thân</div>
        </div>
      </div>
    )
  }

  renderAvatarMaleSusgestion() {
    return (
      <div className="l-flex-cspa mbm">
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nam-1.png`} />
          </div>
          <div>Cười lên<br />nào!</div>
        </div>
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nam-2.png`} />
          </div>
          <div>Rõ ràng,<br />đủ sáng</div>
        </div>
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nam-3.png`} />
          </div>
          <div>Ảnh<br />toàn thân</div>
        </div>
      </div>
    )
  }

  renderSubPhotoFemaleSusgestion() {
    return (
      <div className="l-flex-cspa mbm">
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nu-4.png`} />
          </div>
          <div>Hãy là<br />chính bạn</div>
        </div>
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nu-5.png`} />
          </div>
          <div>Ảnh<br />cuộc sống</div>
        </div>
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nu-6.png`} />
          </div>
          <div>Ảnh thật,<br />rõ nét</div>
        </div>
      </div>
    )
  }

  renderSubPhotoMaleSusgestion() {
    return (
      <div className="l-flex-cspa mbm">
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nam-4.png`} />
          </div>
          <div>Hãy là<br />chính bạn</div>
        </div>
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nam-5.png`} />
          </div>
          <div>Ảnh<br />cuộc sống</div>
        </div>
        <div>
          <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
          <div className="frame frame--sm mbs">
            <img src={`${CDN_URL}/nam-6.png`} />
          </div>
          <div>Ảnh thật,<br />rõ nét</div>
        </div>
      </div>
    )
  }

  renderSusgestion() {
    if (this.props.pictureType === AVATAR_PICTURE) {
      if (this.props.user_profile.current_user.gender === 'female') {
        return this.renderAvatarFemaleSusgestion();
      }
      else {
        return this.renderAvatarMaleSusgestion();
      }
    }
    else {
      if (this.props.user_profile.current_user.gender === 'female') {
        return this.renderSubPhotoFemaleSusgestion();
      }
      else {
        return this.renderSubPhotoMaleSusgestion();
      }
    }
  }

  render() {
    return (
      <div>
        {this.renderTitle()}
        {this.renderUploadImageComponent()}

        {
          this.props.user_profile.current_user.provider === 'facebook' &&
          <Link to={`/fb-album/${this.props.pictureType}`} className="btn btn--b btn--p mbm">Tải ảnh từ Facebook</Link>
        }
        <div className="modal__extra txt-center">
          <div className="txt-bold txt-lg txt-uppercase mbm">Gợi ý từ Ymeet.me</div>
          {this.renderSusgestion()}
          <div className="well well--sm">

            {(this.props.pictureType === AVATAR_PICTURE) ? (
              <Link to="/helps/6/2">Tham khảo bí kíp chọn ảnh hợp lệ</Link>
            ) : (
              <Link to="/helps/6/3">Tham khảo bí kíp chọn ảnh hợp lệ</Link>
            )}

          </div>
        </div>

      </div>
    )
  }
}

UploadImageModal.propTypes = {
  pictureType: PropTypes.number.isRequired,
  closeModal: PropTypes.func.isRequired,
  page_source: PropTypes.string.isRequired
}

export default UploadImageModal
