import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { isEmpty } from 'lodash';
import { browserHistory } from 'react-router'
import { connect } from 'react-redux'
import RandomMatchItem from '../searches/RandomMatchItem'
import { GA_ACTION_GET_MORE_COIN, RANDOM_MATCH_COST, RANDOM_MATCH_MODAL, RANDOM_SIGN } from 'constants/Enviroment';
import UserPicture from 'components/commons/UserPicture'
import { updateRandomMatchState } from 'actions/random_match';
import { increaseCurrentCoin } from 'actions/userProfile';
import GoogleOptimize from 'utils/google_analytic/google_optimize';

class NotificationRandomMatchModal extends React.Component {

  constructor(props) {
    super(props);
    this.closeModal = this.closeModal.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    //this.props.addActivityAudit(ACTIVITY_VIEW_PROFILE, this.props.identifier, null, null);
    if (!this.props.random_match.isOpenNotificationRandomMatch && nextProps.random_match.isOpenNotificationRandomMatch) {
      this.props.increaseCurrentCoin(5); // tang them 5 coins
    }
  }

  @GoogleOptimize
  componentDidUpdate(){}

  closeModal() {
    this.props.updateRandomMatchState('isOpenNotificationRandomMatch', false);
    this.props.updateRandomMatchState('randomMatchInfo', {});
  }

  render() {
    const { random_match } = this.props;
    const { isOpenNotificationRandomMatch, randomMatchInfo } = random_match;

    return (
      <Modal
        transitionName="modal"
        isOpen={isOpenNotificationRandomMatch}
        onRequestClose={this.closeModal}
        className="NotificationRandomMatchModal modal__content modal__content--3"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        {!isEmpty(randomMatchInfo) &&
          <div>
            <div className="mbs txt-center"><img className="img-70" src={RANDOM_SIGN} />
              <br /> <br />
              <div className="txt-medium">Wow!  <span className="txt-blue txt-bold">{randomMatchInfo.nick_name}</span> vừa mới ghép đôi ngẫu nhiên với bạn. Ymeet.me tặng bạn 5 xu.</div>
              <br />
              <div className="txt-medium"><span className="txt-blue txt-bold">Ghép đôi chỉ tồn tại trong 24h.</span> <br /> Đừng bỏ lỡ</div>

            </div>
            <UserPicture user={randomMatchInfo} haveChatButton={true} />
            <div className="row">
            </div>
            <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
          </div>
        }
      </Modal>
    )
  }
}

NotificationRandomMatchModal.propTypes = {

}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateRandomMatchState: (key = '', value = '') => {
      dispatch(updateRandomMatchState(key, value));
    },
    increaseCurrentCoin: (coins = 5) => {
      dispatch(increaseCurrentCoin(coins));
    }
  }
}

const NotificationRandomMatchModalContainer = connect(mapStateToProps, mapDispatchToProps)(NotificationRandomMatchModal);

export default NotificationRandomMatchModalContainer
