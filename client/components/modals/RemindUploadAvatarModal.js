import React from 'react'
import { Link } from 'react-router'
import FileInput from 'react-file-reader-input'
import Modal from 'react-modal'
import { AVATAR_PICTURE, OTHER_PICTURE, UPLOAD_DEVIDE_MODE } from '../../constants/userProfile'
import { OTHER_PROFILE_PAGE, GA_ACTION_UPLOAD_AVATAR_DEVICE, GA_ACTION_UPLOAD_AVATAR_FACEBOOK, CDN_URL } from '../../constants/Enviroment'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class RemindUploadAvatarModal extends React.Component {
  constructor(props) {
    super(props)
    this.uploadAvatar = this.uploadAvatar.bind(this)
  }

  componentWillMount() {
    this.props.updateUploadPicturePageSource(OTHER_PROFILE_PAGE)
  }

  @GoogleOptimize
  componentDidUpdate(){}
  
  uploadAvatar(e, results) {
    results.forEach((result) => {
      const [, file] = result;
      this.props.validateImage(AVATAR_PICTURE, file.name, file.size, false).then(() => {
        this.props.saveImage(file)
      })
    });
  }

  render() {
    if (!this.props.isOpen) {
      return <div></div>
    }
    return(
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        className="RemindUploadAvatarModal modal__content modal__content--3"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        <div className="well well--a well--sm mbm">
          <div className="txt-center txt-bold">Cập nhật ảnh đại diện</div>
        </div>
        <p className="txt-center">Bạn chưa có ảnh đại diện.<br />Vui lòng tải ảnh lên để có trải nghiệm tốt hơn.</p>
        <FileInput
          id="my-file-input"
          onChange={this.uploadAvatar}
          accept="image/*">
          <button className="btn btn--b btn--p mbm" >Tải ảnh từ thiết bị</button>
        </FileInput>
        {
          this.props.user_profile.current_user.provider === 'facebook' &&
          <Link to="/fb-album/2" className="btn btn--b btn--p mbm">Tải ảnh từ Facebook</Link>
        }
        <div className="modal__extra txt-center">
          <div className="txt-bold txt-lg txt-uppercase mbm">Gợi ý từ Ymeet.me</div>

          {(this.props.gender === 'female') ? (
            <div className="l-flex-cspa mbm">
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nu-1.png`} />
                </div>
                <div>Cười lên<br />nào!</div>
              </div>
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nu-2.png`} />
                </div>
                <div>Rõ ràng,<br />đủ sáng</div>
              </div>
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nu-3.png`} />
                </div>
                <div>Ảnh<br />toàn thân</div>
              </div>
            </div>
          ) : (
            <div className="l-flex-cspa mbm">
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nam-1.png`} />
                </div>
                <div>Cười lên<br />nào!</div>
              </div>
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nam-2.png`} />
                </div>
                <div>Rõ ràng,<br />đủ sáng</div>
              </div>
              <div>
                <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                <div className="frame frame--sm mbs">
                  <img src={`${CDN_URL}/nam-3.png`} />
                </div>
                <div>Ảnh<br />toàn thân</div>
              </div>
            </div>
          )}

          <div className="well well--sm">
            <Link to="/helps/6/2" className="txt-underline">Tham khảo bí kíp chọn ảnh hợp lệ</Link>
          </div>
        </div>
        <button className="modal__btn-close" onClick={this.props.onRequestClose}>
          <i className="fa fa-times"></i>
        </button>
      </Modal>
    )
  }
}

export default RemindUploadAvatarModal
