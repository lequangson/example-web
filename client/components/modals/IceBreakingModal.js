import React, { Component, PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { connect } from 'react-redux'
import _ from 'lodash'
import { REMIND_UPDATE_INTRO,CDN_URL } from '../../constants/Enviroment'
import StartChatButton from '../commons/StartChatButton'
import { defaultAvatarObject } from '../../src/scripts/showDefaultAvatar'
import { showChatSuggestedUser } from '../../actions/userProfile'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';

class IceBreakingModal extends Component {
  constructor() {
    super()
    this.state = {
      modalIsOpen: false,
    }
    this.closeModal = this.closeModal.bind(this)
    this.startChartCallback = this.startChartCallback.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.modalIsOpen !== nextState.modalIsOpen) {
      return true;
    }
    if (nextProps.modalIsOpen !== this.props.modalIsOpen) {
      return true;
    }
    if ((this.props.user != null || nextProps.user != null) && this.props.user != nextProps.user) {
      return true;
    }
    return false;
  }

  componentWillUpdate(nextProps) {
    if (nextProps.modalIsOpen && nextProps.user) {
      this.props.showChatSuggestedUser()
    }
  }

  componentWillReceiveProps() {
    this.setState({ modalIsOpen: this.props.modalIsOpen })
  }

  @GoogleOptimize
  componentDidUpdate(){}

  closeModal() {
    this.setState({ modalIsOpen: false })
    if (this.props.closeModalCallback) {
      this.props.closeModalCallback()
    }
  }

  renderMessage(suggestUser) {
    if (suggestUser.gender === 'female') {
      if (this.props.chat_online_status) {
        return (
          <div className="txt-light mbm">
            <span className="txt-bold txt-light-2">{suggestUser.nick_name}</span> chờ bạn mở lời <br />
            khá lâu rồi đó, <br />
            hãy nhắn tin cho cô ấy nhé!
          </div>
        )
      }
      else {
        return (
          <div className="txt-light mbm">
            Hãy mở lời với <span className="txt-bold txt-light-2">{suggestUser.nick_name}</span> trước <br />
            Đừng để cô ấy phải <br />
            chờ đợi bạn quá lâu nhé!
          </div>
        )
      }
    }
    else {
      if (this.props.chat_online_status) {
        return (
          <div className="txt-light mbm">
            <span className="txt-bold txt-light-2">{suggestUser.nick_name}</span> đã đợi bạn khá lâu rồi <br />
            hãy nói chuyện với anh ấy nhé!
          </div>
        )
      }
      else {
        return (
          <div className="txt-light mbm">
            <span className="txt-bold txt-light-2">{suggestUser.nick_name}</span> đã đợi bạn khá lâu rồi <br />
            hãy nói chuyện với anh ấy nhé!
          </div>
        )
      }
    }
  }

  getPopupTitle(suggestUser) {
    if (this.props.chat_online_status) {
      return suggestUser.nick_name + ' đang online kìa!'
    }
    else {
      if (suggestUser.gender === 'male') {
        return 'Chat với ' + suggestUser.nick_name + ' nhé?'
      }
      return `${suggestUser.nick_name} muốn chat với bạn`
    }
  }

  startChartCallback() {
    this.setState({modalIsOpen: false})
    if (this.props.startChartCallback) {
      this.props.startChartCallback()
    }
  }

  renderModalContent() {
    let suggestUser = this.props.user;
    const userPicture = suggestUser.user_pictures.length?
    suggestUser.user_pictures[0] :
    defaultAvatarObject(suggestUser.gender, 'small')

    return (
      <div>
        <div className="modal__container">
          <div className="banner banner--blue-medium no-cursor mbm radius-top">
            <div className="frame frame--xsm frame--2">
              <img src={`${CDN_URL}/general/Heart-smile.png`} alt="smile"/>
            </div>
            <div className="banner__content">
              <div className="txt-medium txt-lg txt-white">{this.getPopupTitle(suggestUser)}</div>
            </div>
          </div>

          <div className="modal__txt-content txt-center">
            <div className="mbs">
              <img
                className="img-70 avatar-border"
                src={userPicture.small_image_url}/>
            </div>
            <div className="txt-bold txt-light-2 mbm">{suggestUser.nick_name}</div>
            {this.renderMessage(suggestUser)}
            <StartChatButton user={suggestUser} shortText={true} startChartCallback={this.startChartCallback} page_source='IceBreakingModal'/>
          </div>
        </div>
        <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
      </div>
    )
  }

  render() {
    let suggestUser = this.props.user;
    let requiredOpen = typeof suggestUser !== 'undefined' && suggestUser != null
    return (
      <div>
        <Modal
          transitionName="modal"
          isOpen={this.state.modalIsOpen && requiredOpen}
          onRequestClose={this.closeModal}
          shouldCloseOnOverlayClick={false}
          className="IceBreakingModal modal__content modal__content--3 padding-0"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
         {requiredOpen
          && this.renderModalContent()}
        </Modal>
      </div>
    )
  }
}

IceBreakingModal.propTypes = {
  modalIsOpen: PropTypes.bool.isRequired,
  user: PropTypes.object,
  chat_online_status: PropTypes.bool.isRequired,
  startChartCallback: PropTypes.func.isRequired,
  closeModalCallback: PropTypes.func.isRequired,
}


function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    showChatSuggestedUser: () => {
      dispatch(showChatSuggestedUser())
    },
  }
}
const IceBreakingModalContainer = connect(mapStateToProps, mapDispachToProps)(IceBreakingModal);

export default IceBreakingModalContainer
