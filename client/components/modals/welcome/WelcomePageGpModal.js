import React, { PropTypes } from 'react';

import Modal from 'components/modals/Modal';
import WelcomePageGpStep1 from 'modals/welcome/welcome-page-google/WelcomePageGpStep1';
import WelcomePageGpStep2 from 'modals/welcome/welcome-page-google/WelcomePageGpStep2';
import WelcomePageGpLastStep from 'modals/welcome/welcome-page-google/WelcomePageGpLastStep';

import {
  WELCOME_GP_STEP_1, WELCOME_GP_STEP_2, WELCOME_GP_STEP_3, WELCOME_GP_LAST_STEP
} from 'modals/welcome/Constants';

const WelcomePageGpModal = (props) => {

  const currentStep = (step_id) => {
    const { user_profile, dictionaries } = props;
    switch(step_id) {
      case WELCOME_GP_STEP_1:
        return <WelcomePageGpStep1
                      {...{user_profile}}
                      openUploadAvatarModal={props.openUploadAvatarModal}
                      handleUpdateName={props.handleUpdateName}
                      updateAndNextStep={props.updateAndNextStep}
                      updateGender={props.updateGender}
                      updateDob={props.updateDob}
                    />;

      case WELCOME_GP_STEP_2:
        return <WelcomePageGpStep2
                      {...{user_profile}}
                      updateAndNextStep={props.updateAndNextStep}
                      openUploadAvatarModal={props.openUploadAvatarModal}
                    />

      case WELCOME_GP_STEP_3:
        return <WelcomePageGpLastStep
                      {...{user_profile}}
                      {...{dictionaries}}
                      updateAndNextStep={props.updateAndNextStep}
                    />
      default:
        return;
    }
  }

  return (
    <Modal
      transitionName="modal"
      isOpen={true}
      onRequestClose={props.onRequestClose}
      className="modal__content modal__content--5"
      overlayClassName="modal__overlay"
      portalClassName="modal"
      contentLabel=""
    >
      {currentStep(props.step_id)}
    </Modal>
  );
}

WelcomePageGpModal.propTypes = {
  openUploadAvatarModal: PropTypes.func.isRequired,
  handleUpdateName: PropTypes.func.isRequired,
  updateAndNextStep: PropTypes.func.isRequired,
  updateGender: PropTypes.func.isRequired,
  updateDob: PropTypes.func.isRequired,
  user_profile: PropTypes.object.isRequired,
  dictionaries: PropTypes.object.isRequired
}

export default WelcomePageGpModal;