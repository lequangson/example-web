import React, { PropTypes } from 'react';

import PollingImg from 'components/commons/PollingImg';
import showDefaultAvatar from 'src/scripts/showDefaultAvatar';


export default class WelcomePageGpStep2 extends React.Component {

  static propTypes = {
    openUploadAvatarModal: PropTypes.func.isRequired,
    updateAndNextStep: PropTypes.func.isRequired,
    user_profile: PropTypes.object.isRequired
  }

  openUploadAvatarModal = () => {
    this.props.openUploadAvatarModal();
  }

  updateAndNextStep = () => {
    this.props.updateAndNextStep();
  }

  render(){
    const { current_user } = this.props.user_profile;
    const { user_pictures, nick_name, gender } = current_user;
    const isExistedAvatar = user_pictures && user_pictures.length && user_pictures[0].is_main;
    const userAvatar =  isExistedAvatar ? user_pictures[0].thumb_image_url : showDefaultAvatar(gender, 'thumb');
    const genderText = gender == 'male' ? 'cô gái' : 'chàng trai';

    return (
      <div>
        {/*Avatar*/}
        <div className="row">
          <div className="col-xs-6 col-xs-offset-3">
            <div className="card__link mbl">
              <PollingImg
                src={userAvatar}
                alt={nick_name}
                className="mbs rounded"
              />
            </div>
          </div>
        </div>

        <div className="col-xs-12 mbl">
          <div className="txt-medium mbs l-flex-center">
            Bạn sẽ thu hút các {genderText}<br /> hơn nếu có ảnh đại diện
          </div>
        </div>

        <div className="row mbl">
          <div className="col-xs-12">
            <button
              className="btn btn--b txt-uppercase btn--noradius"
              onClick={this.openUploadAvatarModal}
            >
              Tải ảnh đại diện
            </button>
          </div>
        </div>

        <div className="row">
          <div
            className="col-xs-12 txt-center cursor"
            onClick={this.updateAndNextStep}
          >
            <span className="txt-medium txt-underline txt-uppercase">
              {isExistedAvatar ? 'Tiếp tục' : 'Để sau'}
            </span>
          </div>
        </div>
      </div>
      );
  }
}