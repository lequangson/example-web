import React, { PropTypes } from 'react';
import _ from 'lodash';

import { errorNotiWithLabel } from 'utils/errors/notification';
import SelectEditable from 'components/commons/SelectEditable';
import { SEARCH_MIN_HEIGHT, SEARCH_MAX_HEIGHT } from 'constants/search';
import InputEditable from 'components/commons/InputEditable';

const WelcomePageGpLastStep = (props) => {
  const { current_user } = props.user_profile;
  const { cities, occupations, intention_marriages } = props.dictionaries;
  const { age, height, job_category, job_position, residence, hope_meet, intention_marriage } = current_user;

  const getParams = () => {
    const height = document.querySelector('#gp_height');
    const residence = document.querySelector('#gp_residence');
    const jobCategory = document.querySelector('#gp_job-category');
    const jobPosition = document.querySelector('#gp_job-position');
    const intentionMarriage = document.querySelector('#gp_intention-marriage');
    const params = {
      'personal[height]': height.value,
      'personal[residence][key]': residence.value,
      'personal[job_category][key]': jobCategory.value,
      'personal[job_position]': jobPosition.value,
      'personal[intention_marriage][key]': intentionMarriage.value
    }
    return params;
  };

  const validateInput = () => {
    const height = document.querySelector('#gp_height');
    const residence = document.querySelector('#gp_residence');
    const jobCategory = document.querySelector('#gp_job-category');
    const jobPosition = document.querySelector('#gp_job-position');
    const intentionMarriage = document.querySelector('#gp_intention-marriage');

    return height.value && residence.value && jobCategory.value && jobPosition.value && intentionMarriage.value;
  }

  const updateAndNextStep = () => {
    if (!validateInput()) {
      errorNotiWithLabel('Bạn chưa điền đầy đủ thông tin', 'Vui lòng điền đầy đủ tất cả các mục', 5000)
      return;
    }
    const params = getParams();
    props.updateAndNextStep(params);
  }

  return (
    <div>
      {/*Greeting*/}
      <div className="txt-white txt-uppercase mbm txt-center txt-xlg">
        Chào mừng đến với Ymeetme
      </div>

      <div className="row mbm">
        <div className="col-xs-6">
          <div className="txt-uppercase txt-medium txt-black mbs">Chiều cao</div>
          <SelectEditable
            id="gp_height"
            className="form__input--f"
            updateUserProfile={() => {}}
            name="personal[gp_height]"
            value={height || 135}
            data={_.drop(_.times(SEARCH_MAX_HEIGHT), SEARCH_MIN_HEIGHT)}
            fromWelcomePage={true}
            pageSource="WelcomePageModal"
          />
        </div>
        <div className="col-xs-6">
          <div className="txt-uppercase txt-medium txt-black mbs">Nơi ở</div>
          <SelectEditable
            id="gp_residence"
            className="form__input--f"
            updateUserProfile={() => {}}
            name="personal[gp_residence]"
            value={residence.key || 24}
            data={cities}
            fromWelcomePage={true}
            pageSource="WelcomePageModal"
          />
        </div>
      </div>

      <div className="row mbm">
        <div className="col-xs-12">
          <div className="txt-uppercase txt-medium txt-black mbs">Nghề nghiệp</div>
          <SelectEditable
            id="gp_job-category"
            className="form__input--f"
            updateUserProfile={() => {}}
            name="personal[gp_job-category]"
            value={job_category.key || 10}
            data={occupations}
            fromWelcomePage={true}
            pageSource="WelcomePageModal"
          />
        </div>
      </div>

      <div className="row mbm">
        <div className="col-xs-12">
          <div className="txt-uppercase txt-medium txt-black mbs">Vị trí công tác</div>
          <InputEditable
            className="form__input--f"
            updateUserProfile={() => {}}
            id="gp_job-position"
            name="personal[gp_job-position]"
            value={job_position || 'Nhân viên'}
            placeholder={'VD:Trưởng phòng, Nhân viên, …'}
            maxLength="50"
            fromWelcomePage={true}
            pageSource="WelcomePageModal"
          />
        </div>
      </div>

      <div className="row mbl">
        <div className="col-xs-12">
          <div className="txt-uppercase txt-medium txt-black mbs">Mong muốn kết hôn</div>
          <SelectEditable
            id="gp_intention-marriage"
            className="form__input--f"
            updateUserProfile={() => {}}
            name="personal[gp_intention-marriage]"
            value={intention_marriage.key || 3}
            data={intention_marriages}
            fromWelcomePage={true}
            pageSource="WelcomePageModal"
          />
        </div>
      </div>

      <div className="row">
        <div className="col-xs-12">
          <button
            className="btn btn--b txt-uppercase btn--noradius"
            onClick={updateAndNextStep}
          >
            Bắt đầu ngay!
          </button>
        </div>
      </div>

    </div>
    );
}

WelcomePageGpLastStep.propTypes = {
  updateAndNextStep: PropTypes.func.isRequired,
  user_profile: PropTypes.object.isRequired,
  dictionaries: PropTypes.object.isRequired
}

export default WelcomePageGpLastStep;