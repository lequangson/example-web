import React, { PropTypes } from 'react';
import _ from 'lodash';

import * as ymmStorage from 'utils/ymmStorage';
import {
  REGEX_NAME, REGEX_CONTAIN_SPECIAL_CHAR, REGEX_OVER_8_NUMBER_DETECTED,
  REGEX_FIRST_CHAR_IS_NUMBER, MODAL_PROMOTION
} from 'constants/Enviroment';
import {
  SPECIAL_CHAR_NOT_ALLOWED,
  NAME_NOT_EXCEED_8_NUMBER, NAME_MUST_NOT_START_WITH_NUMBER,
  NAME_MUST_NOT_BE_EMPTY, NAME_MUST_BETWEEN_2_10_CHARACTER,
} from 'constants/TextDefinition';
import showDefaultAvatar from 'src/scripts/showDefaultAvatar';
import SelectEditable from 'components/commons/SelectEditable';
import { errorNotiWithLabel } from 'utils/errors/notification';
import PollingImg from 'components/commons/PollingImg';


const WelcomePageGpStep1 = (props) => {
  const { current_user } = props.user_profile;
  const { user_pictures, nick_name, gender, birthday } = current_user;
  const isExistedAvatar = user_pictures && user_pictures.length && user_pictures[0].is_main;
  const userAvatar =  isExistedAvatar ? user_pictures[0].thumb_image_url : showDefaultAvatar(gender, 'thumb');

  const validateDob = () => {
    const date = document.querySelector('#gp_date');
    const month = document.querySelector('#gp_month');
    const year = document.querySelector('#gp_year');

    return date.value && month.value && year.value;
  }

  const validateName = ()  => {
    const name = document.querySelector('#gp_name');

    return name.value;
  };

  const getDobParam = () => {
    const date = document.querySelector('#gp_date');
    const month = document.querySelector('#gp_month');
    const year = document.querySelector('#gp_year');
    const dd = date.value < 10 ? '0' + date.value : date.value;
    const mm = month.value < 10 ? '0' + month.value : month.value;
    const dob = dd + '-' + mm + '-' + year.value;
    const dobParam = {
      'personal[birthday]': dob
    }

    return dobParam;
  }

  const getGenderParam = () => {
    const gender = document.querySelector('input[name="gender"]:checked');
    const genderParam = {
      'personal[gender]': gender.value
    }
    return genderParam;
  }

  const getNameParam = () => {
    const name = document.querySelector('#gp_name');
    const nameParam = {
      'personal[nick_name]': name.value,
    }
    return nameParam;
  }

  const getAllParams = () => {
    const params = {
      ...getDobParam(),
      // ...getGenderParam(),
      ...getNameParam()
    }
    return params;
  };

  const handleUpdateName = (e) => {
    props.handleUpdateName(e.target.value);
  }

  const openUploadAvatarModal = () => {
    props.openUploadAvatarModal();
  }

  const onChangeGender = () => {
    const genderParam = getGenderParam();
    props.updateGender(genderParam);
  }

  const onChangeDob = () => {
    if(validateDob()) {
      const date = document.querySelector('#gp_date').value;
      const month = document.querySelector('#gp_month').value;
      const year = document.querySelector('#gp_year').value;

      props.updateDob({date, month, year});
    }
  }

  const updateAndNextStep = () => {
    const name = document.querySelector('#gp_name').value;
    const gender = document.querySelector('input[name="gender"]:checked');

    if(!validateDob()){
      errorNotiWithLabel('Bạn chưa điền đầy đủ thông tin', 'Vui lòng điền đầy đủ tất cả các mục', 5000);
      return;
    }
    if (!name || !name.replace(/\s/g, '').length) {
      errorNotiWithLabel('Vui lòng nhập lại tên', NAME_MUST_NOT_BE_EMPTY, 5000);
      return;
    }
    if(!name.match(REGEX_NAME) && name) {
      errorNotiWithLabel('Vui lòng nhập lại tên', NAME_MUST_BETWEEN_2_10_CHARACTER, 5000);
      return;
    }
    if (name.match(REGEX_CONTAIN_SPECIAL_CHAR)) {
      errorNotiWithLabel('Vui lòng nhập lại tên', SPECIAL_CHAR_NOT_ALLOWED, 5000);
      return;
    }
    if (name.match(REGEX_FIRST_CHAR_IS_NUMBER)) {
      errorNotiWithLabel('Vui lòng nhập lại tên', NAME_MUST_NOT_START_WITH_NUMBER, 5000);
      return;
    }
    if (name.match(REGEX_OVER_8_NUMBER_DETECTED)) {
      errorNotiWithLabel('Vui lòng nhập lại tên', NAME_NOT_EXCEED_8_NUMBER, 5000);
      return;
    }
    if(!gender){
      errorNotiWithLabel('Bạn chưa điền đầy đủ thông tin', 'Vui lòng điền đầy đủ tất cả các mục', 5000);
      return;
    }

    const params = getAllParams();
    props.updateAndNextStep(params, isExistedAvatar);
  }

  return(
    <div>
      {/*Greeting*/}
      <div className="txt-white txt-uppercase mbm txt-center txt-xlg">
        Chào mừng đến với Ymeetme
      </div>

      {/* Avartar */}
      <div className="row">
        <div className="col-xs-6 col-xs-offset-3">
          <div className="card__link mbl">
            <PollingImg
              src={userAvatar}
              alt={nick_name}
              className="mbs rounded"
            />
            {!isExistedAvatar && <div className="perfect-center txt-medium">
              Tải ảnh <br />đại diện
            </div>}
            <div className="card__bottom-center card__bottom-center--1">
              <div onClick={openUploadAvatarModal} className="btn btn--p btn--round l-flex-center txt-xlg">
                <i className="fa fa-camera"></i>
              </div>
            </div>
          </div>
          {/*Nickname*/}
          <div>
            <input
              defaultValue={ymmStorage.getItem('name_welcome') || nick_name}
              id='gp_name'
              type="text"
              className="form__input--f txt-xlg txt-center mbm txt-black"
              maxLength="10"
              placeholder=""
              onBlur={handleUpdateName}
            />
          </div>
        </div>
      </div>

      {/* Gender */}
      <div className='row mbm'>
        <div className='col-xs-12'>
          <div className='txt-uppercase txt-lg txt-medium txt-black mbs'>Giới tính</div>
        </div>
        <div className='col-xs-5 l-flex-vertical-center'>
          <input className='radio-size' id='male' type='radio' name='gender' value='1' onChange={onChangeGender} checked={gender=='male'} />
          <label className='padding-rl10' htmlFor='male'>Nam</label>
        </div>
        <div className='col-xs-5 l-flex-vertical-center'>
          <input className='radio-size' id='female' type='radio' name='gender' value='2' onChange={onChangeGender} checked={gender=='female'}/>
          <label className='padding-rl10' htmlFor='female'>Nữ</label>
        </div>
      </div>

      {/*Birthday*/}
       <div className="row mbl">
          <div className="col-xs-12">
            <div className="txt-uppercase txt-lg txt-medium txt-black mbs">Ngày sinh</div>
          </div>
          <div className="col-xs-4">
            <SelectEditable
              id="gp_date"
              className="form__input--f"
              updateUserProfile={() => {}}
              name="personal[gp_date]"
              value={birthday ? birthday.date : 'Ngày'}
              select_option_text='Ngày'
              data={_.range(1, 32)}
              fromWelcomePage={true}
              pageSource="WelcomePageModal"
              onChange={onChangeDob}
            />
          </div>
          <div className="col-xs-4">
            <SelectEditable
              id="gp_month"
              className="form__input--f"
              updateUserProfile={() => {}}
              name="personal[gp_month]"
              value={birthday ? birthday.month : 'Tháng'}
              select_option_text='Tháng'
              data={_.range(1, 13)}
              fromWelcomePage={true}
              pageSource="WelcomePageModal"
              onChange={onChangeDob}
            />
          </div>
          <div className="col-xs-4">
            <SelectEditable
              id="gp_year"
              className="form__input--f"
              updateUserProfile={() => {}}
              name="personal[gp_year]"
              value={birthday ? birthday.year : 'Năm'}
              select_option_text='Năm'
              data={_.range(1901, 1999).sort(function(a, b){return b-a})}
              fromWelcomePage={true}
              pageSource="WelcomePageModal"
              onChange={onChangeDob}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <button
              className="btn btn--b txt-uppercase btn--noradius"
              onClick={updateAndNextStep}
            >
              Hoàn thành 1 bước nữa để bắt đầu!
            </button>
          </div>
        </div>
    </div>
    )
}

WelcomePageGpStep1.propTypes = {
  updateAndNextStep: PropTypes.func.isRequired,
  handleUpdateName: PropTypes.func.isRequired,
  updateGender: PropTypes.func.isRequired,
  updateDob: PropTypes.func.isRequired,
  user_profile: PropTypes.object.isRequired
}

export default WelcomePageGpStep1;
