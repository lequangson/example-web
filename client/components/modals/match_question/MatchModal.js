import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { connect } from 'react-redux'
import { MATCH_QUESTION_MATCH_IMAGE } from '../../../constants/Enviroment'
import { compareIdentifiers } from '../../../utils/common'
import StartChatButton from '../../commons/StartChatButton'
import showDefaultAvatar from '../../../src/scripts/showDefaultAvatar'
import { updateMatchQuestionState } from '../../../actions/Match_Questions';

class MatchModal extends React.Component {

  static preloadMatchImage() {
    const img = new Image()
    img.src = MATCH_QUESTION_MATCH_IMAGE
  }

  constructor(props) {
    super(props)
    this.closeModal = this.closeModal.bind(this)
  }

  componentWillMount() {
    MatchModal.preloadMatchImage()
  }

  getUserFromMatchUsers(identifier) {
    const user = this.props.user_profile.matched_users.find(u =>
      compareIdentifiers(u.identifier, identifier)
    )
    return user
  }

  closeModal() {
    this.props.updateMatchQuestionState('isOpenMatchModal', false);
  }

  renderModalContent() {
    const { user_profile, Match_Questions } = this.props
    const { match_info } = Match_Questions;
    const { current_user } = user_profile;

    const myUser = current_user
    const myAvatar = typeof myUser.user_pictures != 'undefined' && myUser.user_pictures.length ?
                myUser.user_pictures['0'].thumb_image_url : showDefaultAvatar(myUser.gender, 'small')
    const myName = myUser.nick_name

    let theirAvatar = ''
    let theirName = ''
    let theirUser = match_info

    theirAvatar = typeof match_info.user_pictures != 'undefined' && match_info.user_pictures.length ?
                  match_info.user_pictures['0'].thumb_image_url : showDefaultAvatar(match_info.gender, 'small')
      theirName = match_info.nick_name
    theirUser.match_type = 6; //Users have 5 correct explorer answers.
    return (
      <div>
        <div className="modal__content__frame">
          <img src={MATCH_QUESTION_MATCH_IMAGE} alt="Promotion" />

          <div className="modal__content__absolute">
            <div className="modal__content__left">
              <div className="frame frame--xsm mbs">
                <img src={myAvatar} alt="avatar" />
              </div>
              <div className="txt-blue txt-bold txt-center">{myName}</div>
            </div>
            <div className="modal__content__right">
              <div className="frame frame--xsm mbs">
                <img src={theirAvatar} alt="avatar" />
              </div>
              <div className="txt-blue txt-bold txt-center">{theirName}</div>
            </div>
          </div>

          <div className="modal__content__absolute-2" >
            <StartChatButton user={theirUser} shortText={false} page_source='MatchModal'/>
          </div>
        </div>
        <button
          className="modal__btn-close"
          onClick={this.closeModal}
        >
          <i className="fa fa-times"></i>
        </button>
      </div>
    )
  }

  render() {
    const { Match_Questions } = this.props
    const { isOpenMatchModal, match_info } = Match_Questions;

    return (
      <Modal
        transitionName="modal"
        isOpen={isOpenMatchModal}
        className="MatchModal modal__content modal__content--2"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        {match_info && this.renderModalContent()}
      </Modal>
    )
  }
}

MatchModal.propTypes = {
  user_profile: PropTypes.object,
}


function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateMatchQuestionState: (key, value) => {
      dispatch(updateMatchQuestionState(key, value))
    },
  }
}
const MatchModalContainer = connect(mapStateToProps, mapDispatchToProps)(MatchModal);

export default MatchModalContainer
