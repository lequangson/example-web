import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash';
import { updateSimilarUserModal } from '../../../actions/Like';
import { updateFirstLikeStatus, updateFirstLikeStatusViaApi } from '../../../actions/userProfile';
import showDefaultAvatar from '../../../src/scripts/showDefaultAvatar'
import LikeButton from '../../../components/commons/LikeButton';
import UserPicture from '../../../components/commons/UserPicture';
import { SIMILAR_MODAL_PAGE } from '../../../constants/Enviroment'
import { updateMatchQuestionState } from '../../../actions/Match_Questions';
import { compareIdentifiers } from '../../../utils/common'
import StartChatButton from '../../commons/StartChatButton'
import {
  CDN_URL
} from 'constants/Enviroment'

class HurdleNotification extends React.Component {

  constructor(props) {
    super(props)
    this.closeModal = this.closeModal.bind(this)
  }

  closeModal() {
    this.props.updateMatchQuestionState('isOpenHurdleNotification', false)
  }

  render() {
    const { user_profile, Match_Questions } = this.props
    const { isOpenHurdleNotification, hurdle_matched_answers } = Match_Questions;
    const hurdleInfo = this.props.user_profile.selected_user
    if (isEmpty(hurdleInfo)) {
      return (<div></div>)
    }
    const avatar = typeof hurdleInfo.user_pictures != 'undefined' && hurdleInfo.user_pictures.length ?
                hurdleInfo.user_pictures['0'].thumb_image_url : showDefaultAvatar(hurdleInfo.gender, 'small')
    const name = hurdleInfo.nick_name
    let end_desc = '';
    if (hurdle_matched_answers == 2) {
      end_desc = `${hurdleInfo.gender == 'male' ? 'Anh' : 'Cô'} ấy đã rất cố gắng!`;
    }
    if (hurdle_matched_answers == 3) {
      end_desc = `Hãy xem ${hurdleInfo.gender == 'male' ? 'anh' : 'cô'} ấy là ai!`;
    }
    if (hurdle_matched_answers == 4) {
      end_desc = `Hãy xem ${hurdleInfo.gender == 'male' ? 'anh' : 'cô'} ấy là ai!`;
    }
    if (hurdle_matched_answers == 5) {
      end_desc = `Chúc mừng hai bạn ghép đôi thành công`;
    }
    return (
      <Modal
        transitionName="modal"
        isOpen={isOpenHurdleNotification}
        onRequestClose={this.closeModal}
        className="modal__content modal__content--3"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        <div className="mbs txt-center">
          <div className="txt-blue txt-medium mbm">Wow! Vượt rào {hurdle_matched_answers}/5</div>
          <img className="mbs img-50" src={`${CDN_URL}/general/hurdle+.png`} />
          <div className="padding-b10">
            <span className="txt-blue txt-medium"> {hurdleInfo.nick_name}</span> đoán đúng {hurdle_matched_answers}/5 câu hỏi của bạn. {end_desc}
          </div>
        </div>
        <div className="card mbm">
          <UserPicture goToProfileCallback={this.closeModal} go_to_profile={true} user={hurdleInfo} />
        </div>
        <div className="mbs">
          {hurdle_matched_answers != 5 && <LikeButton
              user={hurdleInfo}
              page_source = {SIMILAR_MODAL_PAGE}
              forceLike={false}
            />
          }
          {hurdle_matched_answers == 5 && <StartChatButton user={hurdleInfo} shortText={false} page_source='MatchModal'/>}
        </div>
        <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
      </Modal>
    )
  }
}

HurdleNotification.propTypes = {
  user_profile: PropTypes.object,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateMatchQuestionState: (key, value) => {
      dispatch(updateMatchQuestionState(key, value))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HurdleNotification);
