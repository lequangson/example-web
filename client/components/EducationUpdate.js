import React, { PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import $ from 'jquery'
import _ from 'lodash'
import InputEditable from './commons/InputEditable'
import SelectEditable from './commons/SelectEditable'
import { MAXIMUM_TEXT_SIZE } from '../constants/userProfile'
import { MAXIMUM_TEXT_ERROR_MSG } from '../constants/TextDefinition'
import {
  // NOT_VERIFIED,
  // PENDING_FOR_APPROVE,
  VERIFIED
} from 'pages/verification/Constant';

class EducationUpdate extends React.Component {
  constructor() {
    super()
    this.state = {
      page: '',
    }
    this.handleUpdate = this.handleUpdate.bind(this)
    this.onChangeSelector = this.onChangeSelector.bind(this)
  }

  componentWillMount() {
    const isInJob = this.props.page === 'job'
    const { current_user } = this.props.user_profile;
    if (_.isEmpty(current_user)) {
      return;
    }
    const hideInput = (this.props.page === 'education' && !current_user.education.key)
     || (this.props.page === 'job' && !current_user.job_category.key)
     || (this.props.page === 'income' && !current_user.income_type)
    this.setState(
      {
        page: this.props.page,
        hideInput,
      }
    )
  }

  handleUpdate() {
    const select_value = $(`#update-select-${this.state.page}`).val()
    const input_value = (this.state.hideInput || this.state.page === 'income') ? '' : $(`#update-input-${this.state.page}`).val()
    let input_job_position = ''
    if (this.props.page === 'job' && !this.state.hideInput) {
      input_job_position = $(`#update-job-position`).val().trim()
      if (input_job_position.length > MAXIMUM_TEXT_SIZE) {
        this.props.showErrorMessage(MAXIMUM_TEXT_ERROR_MSG)
        return
      }
    }
    if (input_value.length > MAXIMUM_TEXT_SIZE) {
      this.props.showErrorMessage(MAXIMUM_TEXT_ERROR_MSG)
      return
    }
    let params = {}
    if (this.state.page === 'education') {
      params = {
        'personal[education][key]': select_value,
        'personal[school_name]': input_value,
      }
    }
    if (this.state.page === 'job') {
      params = {
        'personal[job_category][key]': select_value,
        'personal[job_description]': input_value,
        'personal[job_position]': input_job_position,
      }
    }
    if (this.state.page === 'income') {
      params = {
        'personal[income_type]': select_value
      }
    }
    this.props.updateUserProfile(params).then(() => {
      browserHistory.push('/myprofile')
    })
  }

  onChangeSelector() {
    this.setState({ hideInput: !this.edJobSelector.getComponentValue() })
  }

  render() {
    const { page, user_profile, dictionaries } = this.props
    const { current_user } = user_profile;

    if (_.isEmpty(current_user) || _.isEmpty(dictionaries)) {
      return (
        <p>Loading...</p>
      )
    }
    const { verification_status } = current_user;
    if (
      (page == 'job' && verification_status.job_verify_status == VERIFIED) ||
      (page == 'education' && verification_status.education_verify_status == VERIFIED)
    ) {
      return <div>Bạn không thể sửa thông tin đã được xác minh.</div>;
    }
    return (
      <div>
        <div className="sticky-content">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                { (page === 'education' || page === 'job') &&
                  <div id="hobbiesForm">
                    <h3 className="txt-heading">{page === 'education' ? 'Học vấn' : 'Nghề nghiệp' }</h3>
                    <div id="hobbiesField" className="form__group">
                      <div className="row mbm" key={1} >
                        <div className="col-xs-12">
                          <div className="mbs">{page === 'education' ? 'Trình độ học vấn' : 'Chọn lĩnh vực'}</div>
                          <SelectEditable
                            ref={(c) => { this.edJobSelector = c }}
                            id={`update-select-${page}`}
                            className="form__input--b"
                            name={page === 'education' ? 'personal[education][key]' : 'personal[job_category][key]'}
                            value={
                              page === 'education' ?
                              (current_user.education.key || 0) :
                              (current_user.job_category.key || 0)
                            }
                            data={
                              page === 'education' ?
                              dictionaries.educations :
                              dictionaries.occupations
                            }
                            disabled_update_data={true}
                            onChange={this.onChangeSelector}
                          />
                        </div>
                      </div>
                      {
                        page === 'job' && !this.state.hideInput
                            &&
                        <div className="row mbm" key={3} >
                          <div className="col-xs-12">
                            <div className="mbs">Vị trí công việc {/*<span className="txt-red txt-sm">(bắt buộc)</span>*/}</div>
                            <InputEditable
                              id={`update-job-position`}
                              name={'personal[job_position]'}
                              value={ (current_user.job_position || '') }
                              placeholder={'Ví dụ: Designer, Y tá, Trưởng phòng, Nhân viên, …'}
                              disabled_event
                            />
                          </div>
                        </div>
                      }
                      {
                        !this.state.hideInput &&
                        <div className="row mbm" key={2} >
                          <div className="col-xs-12">
                            <div className="mbs">{page === 'education' ? 'Tên trường' : 'Nơi làm việc'}</div>
                            <InputEditable
                              id={`update-input-${page}`}
                              name={page === 'education' ? 'personal[school_name]' : 'personal[job_description]'}
                              value={
                                page === 'education' ?
                                (current_user.school_name || '') :
                                (current_user.job_description || '')
                              }
                              placeholder={page === 'education' ? 'Ví dụ: ĐH Bách Khoa' : 'Ví dụ: Vietcombank Cầu Giấy, Nhà hàng Sen, ...'}
                              disabled_event
                            />
                          </div>
                        </div>
                      }
                    </div>
                    <div className="row">
                      <div className="col-xs-6">
                        <Link to="/myprofile">
                          <button className="btn btn--b">Hủy</button>
                        </Link>
                      </div>
                      <div className="col-xs-6">
                        <button className="btn btn--p btn--b" onClick={this.handleUpdate} >
                          Cập nhật
                        </button>
                      </div>
                    </div>
                  </div>
                }

                { page === 'income' &&
                  <div id="incomeForm">
                    <h3 className="txt-heading">Thu nhập hàng tháng</h3>
                    <div id="incomeField" className="form__group">
                      <div className="row mbm" key={1} >
                        <div className="col-xs-12">
                          <div className="mbs">Chọn mức thu nhập</div>
                          <SelectEditable
                            id={`update-select-${page}`}
                            className="form__input--b"
                            name='personal[income_type]'
                            value={
                              current_user.income_type
                            }
                            data={
                              dictionaries.income_types
                            }
                            disabled_update_data={true}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-xs-6">
                        <Link to="/myprofile">
                          <button className="btn btn--b">Hủy</button>
                        </Link>
                      </div>
                      <div className="col-xs-6">
                        <button className="btn btn--p btn--b" onClick={this.handleUpdate} >
                          Cập nhật
                        </button>
                      </div>
                    </div>
                  </div>
                }

              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

EducationUpdate.propTypes = {
  page: PropTypes.string,
  dictionaries: PropTypes.object,
  user_profile: PropTypes.object,
  updateUserProfile: PropTypes.func,
  showErrorMessage: PropTypes.func,
}

export default EducationUpdate
