import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { CDN_URL } from 'constants/Enviroment'
import _ from 'lodash'

export default class UpgradeAccountStatus extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired
  }
  render() {
    const { user } = this.props;
    const isPaidUser = user && user.user_type === 4;
    return (
      <div className="panel">
        <div className="panel__heading">
          <h3 className="panel__title">Nâng cấp tài khoản</h3>
        </div>
        <div className="panel__body padding-t20 padding-b10">
          <div className="container">
            <div className="row">
              {!isPaidUser ? <div className="col-xs-6 col-xs-offset-3">
                <Link to="/payment/upgrade" className="btn btn--p btn--b mbm">
              <img 
                src={`${CDN_URL}/general/Payment/Upgrade-account-white-+stroke.png`} 
                alt="unpaid_user" className="img-like"
              />    
              Nâng cấp ngay
                </Link>
              </div>
              : <div className="col-xs-12 l-flex-center mbm">
                <img
                src={`${CDN_URL}/general/MobileApp/Payment/upgrade+account.png`}
                alt="upgrade+account"
                className="img-50"/>
                <span className="txt-blue txt-medium">Tài khoản đã nâng cấp</span>
              </div>
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}
