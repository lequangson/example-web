import React, { PropTypes } from 'react'
import { browserHistory } from 'react-router'
import $ from 'jquery'
import _ from 'lodash'
import picturefill from 'picturefill/dist/picturefill'
import { ProgressiveImage } from 'react-progressive-image-loading'
import slickCSS from '../../../public/styles/slick.css'
import slick from '../../../public/scripts/slick.min.js'
import showOnlineStatus from '../../src/scripts/showStatus'
import { SEARCH_PAGE, PEOPLE_LIKED_ME_PAGE,
 FAVORITE_PAGE, FOOTPRINT_PAGE, BLOCK_USER_BLOCKED_PAGE,
 BLOCK_USER_MUTED_PAGE, PEOPLE_I_LIKED_PAGE, MATCHED_PAGE,
 BANNER_ICON_URL, MY_PROFILE_PAGE, WHAT_HOT_PAGE, WHO_ADD_ME_TO_FAVOURITE_PAGE,
 CDN_URL }
from '../../constants/Enviroment'
 import { VOTE_BUTTON_VOTE, VOTE_BUTTON_VIEW, AVATAR_PICTURE, OTHER_PICTURE, VOTE, UNVOTE } from '../../constants/userProfile'
import { compareIdentifiers, removeLeftUser, getUserIdFromIdentifier, getUserByIndex, getUserIndex } from '../../utils/common'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import RemindUploadPhotosModal from '../modals/RemindUploadPhotosModal'
import VoteButton from '../commons/VoteButton'
import * as ymmStorage from '../../utils/ymmStorage';
import * as UserDevice from '../../utils/UserDevice';

class ProfilePictures extends React.Component {
  constructor() {
    super()
    this.state = {
      isPhotosModalOpen: false,
    }

    this.shouldShowRemindUploadPhoto = this.shouldShowRemindUploadPhoto.bind(this)
    this.showPhotoModal = this.showPhotoModal.bind(this)
    this.hidePhotosModal = this.hidePhotosModal.bind(this)
  }

  _carousel() {
    // active slick carousel
    // main carousel
    if ($(`#carousel-for-${this.props.user.identifier}`)) {
      $(`#carousel-for-${this.props.user.identifier}`).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        infinite: false,
        asNavFor: `#carousel-nav-${this.props.user.identifier}`,
      });
    }

    // nav carousel
    if ($(`#carousel-nav-${this.props.user.identifier}`)) {
      $(`#carousel-nav-${this.props.user.identifier}`).slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        asNavFor: `#carousel-for-${this.props.user.identifier}`,
        arrows: false,
        infinite: false,
        initialSlide: 0,
        focusOnSelect: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 5,
            },
          },
          {
            breakpoint: 425,
            settings: {
              slidesToShow: 5,
            },
          },
        ],
      })
    }
  }

  _unCarousel() {
    if ($(`#carousel-for-${this.props.user.identifier}`)) {
      $(`#carousel-for-${this.props.user.identifier}`).slick('unslick');
    }
    if ($(`#carousel-nav-${this.props.user.identifier}`)) {
      $(`#carousel-nav-${this.props.user.identifier}`).slick('unslick')
    }
  }

  componentDidMount() {
    if (!_.isEmpty(this.props.user)) { // in case user did not reload browser
      this._carousel()
    }

    const offsetBottom = $(this.carouselNav).offset().top - $(this.carouselNav).height()

    this.props.setCarouselOffsetBottom(offsetBottom)
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return !_.isEqual(nextProps.user, this.props.user);
  // }

  componentWillUpdate(nextProps) {
    if (!_.isEmpty(this.props.user) && getUserIdFromIdentifier(this.props.user.identifier) !== getUserIdFromIdentifier(nextProps.user.identifier)) {
      this._unCarousel()
    }
  }

  componentDidUpdate(prevProps) { // in case user reload browser
    if (!_.isEmpty(this.props.user) && getUserIdFromIdentifier(prevProps.user.identifier) !== getUserIdFromIdentifier(this.props.user.identifier)) {
      this._carousel()
      if ($(`#carousel-nav-${this.props.user.identifier}`)) {
        $(`#carousel-nav-${this.props.user.identifier}`).slick('slickGoTo', 0);
      }
    }

    const index = getUserIndex(this.props)
    const previousUser = getUserByIndex(index - 1, this.props)
    const nextUser = getUserByIndex(index + 1, this.props)

    this.preloadImage(previousUser)
    this.preloadImage(nextUser)
  }

  renderNewUserIcon() {
    const user = this.props.user
    const isNewUser = user ? user.is_new_user : false
    if (!isNewUser) {
      return ''
    }
    return (
      <div>
        <div className="card__addon-2">
          <div className="card__addon-2__title">Mới!</div>
        </div>
        <div className="card__pad-2"></div>
      </div>
    )
  }

  preloadImage(user) {
    if (!user || user.user_pictures.length === 0) {
      return;
    }

    const img = new Image()
    const isExtraLargeRequired = UserDevice.isHeighResolution();
    img.src = isExtraLargeRequired
              ? user.user_pictures[0].extra_large_image_url
              : user.user_pictures[0].large_image_url;

    let thumbImgArray = []

    const lastIndex = user.user_pictures.length > 6 ? 5 : user.user_pictures.length - 1
    user.user_pictures.slice(0, lastIndex).map((picture, i) => {
      thumbImgArray[i] = new Image()
      thumbImgArray[i].src = picture.thumb_image_url
    })
  }

  nextUser_OnClick(userIdentifier, stepIndex) {

    const profile_content = document.querySelector('.profile-slide');
    $('div.profile-slide').removeClass('slide-appear');
    if(stepIndex > 0) {
      profile_content.classList.add('slide-enter-active');
    } else {
      profile_content.classList.add('slide-leave-active');
    }
    setTimeout(() => {
      this.props.setProfileUserNextIndex(this.props.enviroment.profile_next_user_index + stepIndex)
      ymmStorage.setItem('isNeedSendGA', true)
      browserHistory.push(`/profile/${userIdentifier}`)
      profile_content.classList.remove('slide-enter-active');
      profile_content.classList.remove('slide-leave-active');
      $('div.profile-slide').addClass('slide-appear');
    }, 180);
  }

  shouldShowRemindUploadPhoto(me) {
    if (!me) {
      return false
    }

    const mySubPhotos = me.user_pictures.filter(photo => photo.is_main === false)

    if (mySubPhotos.length > 3) {
      return false
    }

    return true
  }

  showPhotoModal() {
    this.setState({ isPhotosModalOpen: true })
  }

  hidePhotosModal() {
    this.setState({ isPhotosModalOpen: false })
  }

  renderVerifiedUserIcon() {
    const { user } = this.props;
    const score = user.verification_status ? user.verification_status.score : 0;
    let url, verifyAlt = ''
    if(score == 0) {
      return ''
    }

    if(score <= 30) {
      url = `${CDN_URL}/general/Verify/Bronze.png`
      verifyAlt = 'verify-bronze'
    }else if(score <= 70 && score > 31) {
      url = `${CDN_URL}/general/Verify/Silver.png`
      verifyAlt = 'verify-silver'
    }else {
      url = `${CDN_URL}/general/Verify/Gold.png`
      verifyAlt = 'verify-gold'
    }

    return (
      <div className="card__verify card__verify--2">
        <img className="img-round img-verify--list" src={url} alt={verifyAlt} />
      </div>
    )
  }

  renderNextPreviousButtons(previousUserIdentifier, nextUserIdentifier) {
    if (this.props.enviroment.previous_page === WHAT_HOT_PAGE ||
      this.props.enviroment.previous_page === PEOPLE_LIKED_ME_PAGE) {
      return ''
    }
    return (
      <div>
        <button
          className={`pagi pagi--prev${(previousUserIdentifier) ? '' : ' is-hide'}`}
          onClick={this.nextUser_OnClick.bind(this, previousUserIdentifier, -1)}
        >
          <i className="fa fa-chevron-left"></i>
        </button>
        <button
          className={`pagi pagi--next${(nextUserIdentifier) ? '' : ' is-hide'}`}
          onClick={this.nextUser_OnClick.bind(this, nextUserIdentifier, 1)}
        >
          <i className="fa fa-chevron-right"></i>
        </button>
      </div>
    )
  }

  render() {
    const me = this.props.user_profile.current_user
    const user = this.props.user
    const isVerifiedUser = user ? user.is_verified : false
    const index = getUserIndex(this.props)
    const previousUser = getUserByIndex(index - 1, this.props)
    const previousUserIdentifier = previousUser ? previousUser.identifier : ''
    const nextUser = getUserByIndex(index + 1, this.props)
    const nextUserIdentifier = nextUser ? nextUser.identifier : ''
    const isHeighResolution = UserDevice.isHeighResolution();

    return (
      <div className="container padding-t10">
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 user-main-carousel">
            <article className="card mbm">
              <div className="card__upper">
                <div className="card__link">
                  <div className="carousel-wrap">
                    <div
                      id={`carousel-for-${user.identifier}`}
                      className="carousel-for"
                      ref={(ref) => {
                        this.carouselFor = ref
                      }}
                    >
                      {
                        (typeof user.user_pictures !== 'undefined' && user.user_pictures.length > 0) ? user.user_pictures.map((picture, i) => (
                          <div key={i}>
                            <picture>
                              <source
                                srcSet={picture.extra_large_image_url}
                                media="(min-resolution: 2dppx)"
                                alt={user.nick_name}
                              />
                              <source
                                srcSet={picture.extra_large_image_url}
                                media="(min-width: 1200px) and (min-resolution: 1dppx)"
                                alt={user.nick_name}
                              />
                              <img src={picture.large_image_url} alt={user.nick_name} />
                            </picture>
                            {/*<ProgressiveImage
                              preview={picture.small_image_url}
                              src={ isHeighResolution? picture.extra_large_image_url : picture.large_image_url}
                              render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                            />*/}
                            <VoteButton
                              type={VOTE_BUTTON_VOTE}
                              style={ isVerifiedUser ? 2 : 1}
                              picture={picture}
                              target_user_id={user.identifier}
                              vote_from_name={me.nick_name}
                            />
                          </div>
                        )) : (
                          <div>
                            <img src={showDefaultAvatar(user.gender, 'large')} alt={user.nick_name} />
                          </div>
                        )
                      }
                    </div>
                    {this.shouldShowRemindUploadPhoto(me) && (
                      <div
                        className="carousel-mask"
                        onClick={this.showPhotoModal}
                      />
                    )}

                  </div>

                  <div className="card__meta card__meta--1">
                    <span className={`dot dot--${showOnlineStatus(user.online_status)} mrs`}></span>
                    <span className="txt-bold txt-lg">{`${user.nick_name} - `}</span>
                    <span className="txt-bold txt-lg">
                      {user.age
                        ? user.age.toString()
                        : '?'
                      }
                    </span>
                    <span className="txt-lg">
                      {user.residence.value
                        ? ` - ${user.residence.value}`
                        : ''
                      }
                    </span>
                    <div>
                      <i className="fa fa-image"></i> {user.user_pictures.length} ảnh
                    </div>
                  </div>

                  <div className="card__addon">
                    <i className="fa fa-heart"></i>
                    <div className="txt-bold">{user.compatibility}%</div>
                  </div>
                  <div className="card__pad"></div>
                  {this.renderNewUserIcon()}
                </div>
              </div>
              {this.renderNextPreviousButtons(previousUserIdentifier, nextUserIdentifier)}
              {this.renderVerifiedUserIcon()}
            </article>
          </div>
        </div>

        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 user-sub-carousel">
            <div id={`carousel-nav-${user.identifier}`} className="carousel-nav mbm" ref={(ref) => {
              this.carouselNav = ref
            }}>
              {
                (typeof user.user_pictures !== 'undefined' && user.user_pictures.length > 0)
                ? user.user_pictures.map((picture, i) => (
                    <div key={i}>
                      <img src={picture.thumb_image_url} alt={user.nick_name} />
                    </div>
                  ))
                : <div>
                    <img src={showDefaultAvatar(user.gender)} alt={user.nick_name} />
                  </div>
              }
            </div>
            {this.shouldShowRemindUploadPhoto(me) && (
              <div
                className="carousel-mask"
                onClick={this.showPhotoModal}
              />
            )}
          </div>
        </div>
        <RemindUploadPhotosModal
          updateUploadPicturePageSource={this.props.updateUploadPicturePageSource}
          identifier={user.identifier}
          validateImage={this.props.validateImage}
          saveImage={this.props.saveImage}
          isOpen={this.state.isPhotosModalOpen}
          onRequestClose={this.hidePhotosModal}
          gender={me.gender}
          gaSend={this.props.gaSend}
          saveFilesUpload={this.props.saveFilesUpload}
          user_profile={this.props.user_profile}
        />
      </div>
    )
  }
}

ProfilePictures.propTypes = {
  enviroment: PropTypes.object,
  user_profile: PropTypes.object,
  PeopleILiked: PropTypes.object,
  user: PropTypes.object,
  data: PropTypes.array,
  setProfileUserNextIndex: PropTypes.func,
  setCarouselOffsetBottom: PropTypes.func,
}

export default ProfilePictures
