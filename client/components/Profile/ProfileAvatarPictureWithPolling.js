import React from 'react';

class ProfileAvatarPictureWithPolling extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      imageLoadCount: 0,
      waiting: false
    }
  }

  onImageLoadError(event) {
    const self = this;
    self.setState({waiting: true})
    setTimeout(() => {
      const newImageLoadCount = self.state.imageLoadCount + 1;
      //console.log(`reload image in ProfileAvatarPictureWithPolling.js. src: ${this.props.src}, load count: ${newImageLoadCount}`)
      self.setState({imageLoadCount: newImageLoadCount, waiting: false})
    }, 2000);
  }

  render() {
    const {user, userPicture} = this.props;

    if (this.state.waiting) {
      return <i className="fa fa-spinner fa-5x"></i>
    }

    return (
      <picture>
        <source
          srcSet={`${userPicture.extra_large_image_url}?cnt=${this.state.imageLoadCount}`}
          media="(min-width: 1200px) and (min-resolution: 1dppx)"
          alt={user.nick_name}
        />
        <source
          srcSet={`${userPicture.extra_large_image_url}?cnt=${this.state.imageLoadCount}`}
          media="(min-width: 320px) and (max-width: 768px) and (min-resolution: 2dppx)"
          alt={user.nick_name}
        />
        <img
          srcSet={`${userPicture.large_image_url}?cnt=${this.state.imageLoadCount}`}
          className="card__img"
          alt={user.nick_name}
          onError={this.onImageLoadError.bind(this)}
        />
      </picture>
    );
  }
}

export default ProfileAvatarPictureWithPolling;
