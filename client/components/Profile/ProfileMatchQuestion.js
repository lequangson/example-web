import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import _ from 'lodash'
import StartAnswerButton from '../commons/Buttons/StartAnswerButton'
import { ANSWER_OTHER_EXPLORER_QUESTION } from '../../constants/Match_Questions'
import { getUserQuestions } from '../../actions/Match_Questions'
import { compareIdentifiers } from '../../utils/common'

class ProfileMatchQuestion extends React.Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.props.getUserQuestions(this.props.user.identifier)
  }

  componentWillReceiveProps(nextProps) {
    if (!compareIdentifiers(this.props.user.identifier, nextProps.user.identifier)) {
      this.props.getUserQuestions(nextProps.user.identifier)
    }
  }

  render() {
    if (this.props.mode == 'edit') {
      const explore_yourself = this.props.user.explore_yourself
      return (
        <div className="panel">
          <div className="panel__heading">
            <h3 className="panel__title">Tôi là mẫu người thế nào?</h3>
          </div>
          <div className="panel__body">
            <div className="container">
              { explore_yourself ||
                <div className="row">
                  <div className="col-xs-12">
                    <p>Cùng Ymeet khám phá tính cách thú vị của bạn qua 20 câu hỏi!</p>
                    <div className="row">
                      <div className="col-xs-8 col-xs-offset-2">
                        <Link to="/my-answer" className="btn btn--b btn--p mbm">Tìm hiểu thêm</Link>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
      )
    }
    else {
      const explore_yourself = this.props.user.explore_yourself
      const isInMatchedList = this.props.user_profile.matched_users.findIndex(user => compareIdentifiers(user.identifier, this.props.user.identifier)) > -1
      const isHaveExplorerQuestions = this.props.Match_Questions.other_questions.length == 5
      const needShowExplorerButton = !isInMatchedList && isHaveExplorerQuestions && this.props.Match_Questions.other_questions.filter(q => q.explorer_answer !== null).length < 5
      return (
        <div className="panel">
        {
          explore_yourself &&
          <div>
            <div className="panel__heading">
              <h3 className="panel__title">{this.props.user.gender == 'male' ? 'Anh ấy' : 'Cô ấy'} là mẫu người thế nào?</h3>
            </div>
            <div className="panel__body">
              <div className="container">
                { explore_yourself }
                {
                  needShowExplorerButton &&
                  <div className="row">
                    <div className="col-xs-12">
                      <div className="row">
                        <div className="col-xs-8 col-xs-offset-2">
                          <StartAnswerButton Mode={ANSWER_OTHER_EXPLORER_QUESTION} user={this.props.user}/>
                        </div>
                      </div>
                    </div>
                  </div>
                }
              </div>
            </div>
          </div>
        }
        </div>
      )
    }
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    getUserQuestions: (id) => {
      dispatch(getUserQuestions(id))
    },
  }
}
const ProfileMatchQuestionContainer = connect(mapStateToProps, mapDispachToProps)(ProfileMatchQuestion);

export default ProfileMatchQuestionContainer;
