import React, { PropTypes } from 'react'
import _ from 'lodash'
import { Link } from 'react-router'
import SelectEditable from '../commons/SelectEditable'
import IconEditable from '../commons/IconEditable'
import {
  SEARCH_MIN_HEIGHT, SEARCH_MAX_HEIGHT,
} from '../../constants/search'
import {
  VERIFICATION_APPROVE_STATUS,
} from '../../constants/userProfile'

class BasicInformation extends React.Component {
  static calculateNumberFacebookFriend(number_friend) {
    let displayText = '';

    if (number_friend < 10) {
      displayText = 'Ít hơn 10';
    } else if (number_friend < 50) {
      const rounfTimes = Math.floor(number_friend / 10);
      displayText = `Nhiều hơn ${(rounfTimes * 10).toString()}`;
    } else {
      const rounfTimes = Math.floor(number_friend / 50);
      displayText = `Nhiều hơn ${(rounfTimes * 50).toString()}`;
    }
    return displayText;
  }

  // shouldComponentUpdate(nextProps) {
  //   console.log(nextProps)
  //   return false
  // }

  render() {
    const { user, mode } = this.props
    const { verification_status } = user;
    if (_.isEmpty(user)) {
      return (
        <div className="panel">
          <div className="panel__heading">
            <h3 className="panel__title">Thông tin cơ bản</h3>
          </div>
        </div>
      )
    }
    const {
      id_card_verify_status,
    } = verification_status

    return (
      <div className="panel">
        <div className="panel__heading">
          <h3 className="panel__title">Thông tin cơ bản</h3>
        </div>
        <div className="panel__body">
          <table className="table table--a">
            <tbody>
              <tr>
                <td className="txt-light">Tên</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return (<span>{user.nick_name}</span>)
                    }
                    return (
                      user.allow_update_short_name ?
                      <Link
                        to="/myprofile/update-basic-info"
                        className={'txt-blue l-flex-grow'}
                      >
                      <span>{user.nick_name}</span>
                      <IconEditable />
                      </Link>
                      :
                      <span>{user.nick_name}</span>
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Tuổi</td>
                <td className="txt-blue">
                  {user.age ? user.age.toString() : ''}
                  {id_card_verify_status === VERIFICATION_APPROVE_STATUS
                    && <i className="fa fa-check pull-right txt-green"></i>}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Chòm sao</td>
                <td className="txt-blue">
                  {user.constellation ? user.constellation : ''}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Chiều cao</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.height ? `${user.height.toString()} cm` : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[height]"
                        value={user.height || 0}
                        data={_.drop(_.times(SEARCH_MAX_HEIGHT), SEARCH_MIN_HEIGHT)}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Dáng người</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.body_type.value ? user.body_type.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[body_type][key]"
                        value={user.body_type.key || 0}
                        data={this.props.dictionaries.body_types}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Nhóm máu</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.blood_type.value ? user.blood_type.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[blood_type][key]"
                        value={user.blood_type.key || 0}
                        data={this.props.dictionaries.blood_types}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Nơi ở hiện tại</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.residence.value ? user.residence.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[residence][key]"
                        value={user.residence.key || 0}
                        data={this.props.dictionaries.cities}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Quê quán</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return (
                        user.birth_place.value ? user.birth_place.value : 'Không rõ'
                      )
                    }
                    return (
                      (id_card_verify_status !== VERIFICATION_APPROVE_STATUS) ?
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[birth_place][key]"
                        value={user.birth_place.key || 0}
                        read_only={id_card_verify_status === VERIFICATION_APPROVE_STATUS}
                        data={this.props.dictionaries.cities}
                      />: <span id="birth_place">{user.birth_place.value}</span>
                    )
                  })()}
                  {id_card_verify_status === VERIFICATION_APPROVE_STATUS
                    && <i className="fa fa-check pull-right txt-green"></i>}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Anh chị em </td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.family_position.value ? user.family_position.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[family_position][key]"
                        value={user.family_position.key || 0}
                        data={this.props.dictionaries.family_relation_types || []}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Số bạn Facebook</td>
                <td className="txt-blue" id="fb-friend">
                  {
                    BasicInformation.calculateNumberFacebookFriend(
                      (typeof user.number_facebook_friend !== 'undefined' && user.number_facebook_friend)
                      ?
                      user.number_facebook_friend
                      :
                      10
                    )
                  }
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

BasicInformation.propTypes = {
  updateUserProfile: PropTypes.func,
  dictionaries: PropTypes.object,
  mode: PropTypes.string,
  user: PropTypes.object,

}

export default BasicInformation
