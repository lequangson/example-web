import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import IconEditable from '../commons/IconEditable'
import SelectMultipleEditable from '../commons/SelectMultipleEditable'
import {
  //NOT_VERIFIED,
  // PENDING_FOR_APPROVE,
  VERIFIED
} from 'pages/verification/Constant';

class EducationInformation extends React.Component {

  render() {
    const { user, mode, dictionaries, current_user } = this.props;
    const { verification_status } = user;


    if (_.isEmpty(user)) {
      return (
        <div className="panel">
          <div className="panel__heading">
            <h3 className="panel__title">Học vấn - Nghề nghiệp</h3>
          </div>
        </div>
      )
    }

    const {
      job_verify_status,
      education_verify_status,
    } = verification_status

    return (
      <div className="panel">
        <div className="panel__heading">
          <h3 className="panel__title">Học vấn - Nghề nghiệp</h3>
        </div>
        <div className="panel__body">
          <table className="table table--a">
            <tbody>
              <tr id="education">
                <td className="txt-light">Học vấn</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read' || education_verify_status == VERIFIED ) {
                     return <span> {user.education.value ? user.education.value : 'Không rõ'} <br />
                       {user.school_name ? user.school_name : ''}
                       {education_verify_status === VERIFIED
                              && <i className="fa fa-check pull-right txt-green"></i>}
                     </span>
                    }
                    return (
                      <Link
                        to="/update/education"
                        className={user.education.value ? 'txt-blue l-flex-grow' : 'txt-red l-flex-grow'}
                      >
                        {
                          user.education.value
                          ?
                            <span>{user.education.value} <br /> {user.school_name || ''}</span>
                          :
                            <span>{'Chưa cập nhật'}</span>
                        }
                        {education_verify_status === VERIFIED
                              ? <i className="fa fa-check pull-right txt-green"></i> : <IconEditable />}
                      </Link>
                    )
                  })()}
                </td>
              </tr>
              <tr id="job">
                <td className="txt-light">Nghề nghiệp</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read' || job_verify_status == VERIFIED) {
                      return (
                        user.job_category.value
                          ?
                            <span>
                              {user.job_category.value}
                              {user.job_position ? <br /> : ''} {user.job_position}
                              {user.job_description ? <br /> : ''} {user.job_description}
                              {job_verify_status === VERIFIED
                              && <i className="fa fa-check pull-right txt-green"></i>}
                            </span>
                          :
                            <span>{'Không rõ'}</span>
                      )
                    }
                    return (
                      <Link
                        to="/update/job"
                        className={user.job_category.value ? 'txt-blue l-flex-grow' : 'txt-red l-flex-grow'}
                      >
                        {
                          user.job_category.value
                          ?
                            <span>
                              {user.job_category.value}
                              {user.job_position ? <br /> : ''} {user.job_position}
                              {user.job_description ? <br /> : ''} {user.job_description}
                            </span>
                          :
                            <span>{'Chưa cập nhật'}</span>
                        }
                        {job_verify_status === VERIFIED
                              ? <i className="fa fa-check pull-right txt-green"></i> : <IconEditable />}
                      </Link>
                    )
                  })()}
                </td>
              </tr>
              <tr id="income">
                <td className="txt-light">Thu nhập hàng tháng</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return (
                        current_user.income_type !== null
                          ?
                          user.income_type === null ? <span>{'Chưa cập nhật'}</span> : <span>{dictionaries.income_types[user.income_type].value}</span>
                          :
                          <span className="txt-red l-flex-grow">
                            Bạn cần phải nhập thông tin về thu nhập của mình để nhìn thấy nội dung này
                          </span>
                      )
                    }
                    return (
                      <Link
                        to="/update/income"
                        className="txt-blue l-flex-grow"
                      >
                        {
                          user.income_type === null
                           ?
                           <span className="txt-red l-flex-grow">{'Chưa cập nhật'}</span>
                           :
                           <span>{dictionaries.income_types[user.income_type].value}</span>
                        }
                        <IconEditable />
                      </Link>
                    )
                  })()}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

EducationInformation.propTypes = {
  mode: PropTypes.string,
  user: PropTypes.object,
}

export default EducationInformation
