import React, { PropTypes } from 'react';

class SmallProgressBar extends React.Component {
  getClassName(className) {
    const level1Value = 30;
    const level2Value = 70;
    const level3Value = 99;
    if (!this.props.completion || this.props.completion === 0) {
      return className + 0;
    }
    else if (this.props.completion <= level1Value) {
      return className + 1;
    }
    else if (this.props.completion <= level2Value) {
      return className + 2;
    }
    else if (this.props.completion <= level3Value) {
      return className + 3;
    }
    return className + 4;
  }

  render() {
    return (
      <div>
        <i className={this.getClassName('fa progress__pin progress__pin--')}></i>
        <span className= "padding-r5">{this.props.completion? this.props.completion : 0}%</span>
      </div>
    )
  }
}

SmallProgressBar.propTypes = {
  completion: PropTypes.number,
}

export default SmallProgressBar
