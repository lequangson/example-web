import React, { PropTypes } from 'react'
import LikeButton from '../commons/LikeButton'
import SuperLikeButton from '../commons/SuperLikeButton'
import StartChatButton from '../commons/StartChatButton'
import { OTHER_PROFILE_PAGE } from '../../constants/Enviroment'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import {
  SEARCH_PAGE_NEW,
  SEARCH_PAGE_ONLINE,
  SEARCH_PAGE_HARMONY_AGE,
  SEARCH_PAGE_HARMONY_FATE,
  SEARCH_PAGE_HARMONY_HOROSCOPE,
  SEARCH_PAGE_NEARBY,
  SEARCH_PAGE_DEFAULT,
  PEOPLE_LIKED_ME_PAGE,
  FAVORITE_PAGE,
  FOOTPRINT_PAGE,
  BLOCK_USER_BLOCKED_PAGE, BLOCK_USER_MUTED_PAGE,
  PEOPLE_I_LIKED_PAGE,
  MATCHED_PAGE,
  MY_PROFILE_PAGE, VOTED_MODAL_PAGE,
} from '../../constants/Enviroment'
import { Link } from 'react-router'
import * as ymmStorage from '../../utils/ymmStorage';
import { MATCHED_TYPE_SUPER } from '../../constants/Like';
import { GHOST_USER } from '../../constants/userProfile';

class ProfileLikeBanner extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      avatarVisbility: false,
      super_like_success: false
    }
    this.track = this.track.bind(this)
    this.superLikeClick = this.superLikeClick.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.track)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.track)
    if (this.isNeedScrollToPosition()) {
      this.props.needScrollToPosition(true) // only auto scroll when back to search from other profile
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.user !== nextProps.user) {
      this.setState({
        super_like_success: false
      })
    }
  }

  isNeedScrollToPosition() {
    const needScrollPages = [
      SEARCH_PAGE_NEW,
      SEARCH_PAGE_ONLINE,
      SEARCH_PAGE_HARMONY_AGE,
      SEARCH_PAGE_HARMONY_FATE,
      SEARCH_PAGE_HARMONY_HOROSCOPE,
      SEARCH_PAGE_NEARBY,
      SEARCH_PAGE_DEFAULT
    ];
    return needScrollPages.indexOf(this.props.enviroment.previous_page) >= 0;
  }

  track() {
    if (window.scrollY > this.props.target) {
      this.setState({
        avatarVisbility: true,
      })
    } else {
      this.setState({
        avatarVisbility: false,
      })
    }
  }


  renderButton() {
    const { user, current_user } = this.props;
    let isMatchedUser = false;
    if ( (user.have_sent_like && user.have_sent_like_to_me) || user.is_random_matched || user.match_type == MATCHED_TYPE_SUPER) {
      isMatchedUser = true;
    }
    if (user.gender == 'male') {
      return (
        <div className='col-xs-12'>
          {
            isMatchedUser ?
            <StartChatButton user={user} page_source={OTHER_PROFILE_PAGE} shortText super_like={user.match_type == MATCHED_TYPE_SUPER ? true : false} />
            :
            current_user.user_status !== GHOST_USER && current_user.permission.show_super_like ?
            <SuperLikeButton user={user} superLikeClick={this.superLikeClick} />
            :
            <LikeButton user={user} />
          }
        </div>
      )
    }
    else {
      return (
        <div className='col-xs-12'>
          <div className={this.state.avatarVisbility ? `col-xs-${this.state.super_like_success ? '8' : '5'}` : `col-xs-${this.state.super_like_success? '12' : '6'}`}>
            {!isMatchedUser && <SuperLikeButton user={user} superLikeClick={this.superLikeClick} />}
          </div>
          <div className={this.state.avatarVisbility ? `col-xs-${isMatchedUser || this.state.super_like_success ? '8' : '5'}` : `col-xs-${isMatchedUser? '12' : '6'}`}>
            <LikeButton user={user} />
            <StartChatButton user={user} page_source={OTHER_PROFILE_PAGE} shortText super_like={user.match_type == MATCHED_TYPE_SUPER ? true : false} />
          </div>
        </div>
      )
    }
  }

  superLikeClick() {
    this.setState({super_like_success: true});
  }

  render() {
    const user = this.props.user
    let userAvatar = ''
    if (user.user_pictures.length) {
      userAvatar = user.user_pictures[0].small_image_url
    } else {
      userAvatar = showDefaultAvatar(user.gender)
    }
    let isMatchedUser = false;
    if ( (user.have_sent_like && user.have_sent_like_to_me) || user.is_random_matched || user.match_type == MATCHED_TYPE_SUPER) {
      isMatchedUser = true;
    }
    return (
      <div className="fixed-bottom z-index-1400">
        <div className="container">
          <div className="row l-flex-vertical-center">
            {
              this.state.avatarVisbility &&
                <div className="col-xs-3">
                  <div className="frame frame--xsm frame--1" >
                    <img className="img-like" src={userAvatar} alt="" />
                  </div>
                </div>
            }
            {this.renderButton()}
          </div>
        </div>
      </div>
    )
  }
}

ProfileLikeBanner.propTypes = {
  user: PropTypes.object,
  target: PropTypes.number,
  needScrollToPosition: PropTypes.func,
}

export default ProfileLikeBanner
