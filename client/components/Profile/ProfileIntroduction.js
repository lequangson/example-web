import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import _ from 'lodash'

class ProfileIntroduction extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { user } = this.props
    if (_.isEmpty(user)) {
      return <div>Loading...</div>
    }
    return (
      <div className="panel">
        <div className="panel__heading">
          <h3 className="panel__title">Giới thiệu bản thân</h3>
        </div>
        <div className="panel__body">
          <div className="container" id="introduction">
            {
              !this.props.user.self_introduction &&
                <p className="txt-red txt-center mbm">Giới thiệu bản thân thật thú vị và thu hút nhé</p>
            }
{/*            <p dangerouslySetInnerHTML={{ __html: this.props.user.self_introduction ? this.props.user.self_introduction.replace(/(?:\r\n|\r|\n)/g, '<br />') : '' }}></p>  */}
               { this.props.user.self_introduction || ''}
            <div className="row">
              <div className="col-xs-8 col-xs-offset-2">
                <Link to="/myprofile/update-self-intro" className="btn btn--p btn--b mbm">Sửa giới thiệu bản thân</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProfileIntroduction.propTypes = {
  user: PropTypes.object,
}

export default ProfileIntroduction
