import React, { PropTypes } from 'react'
import _ from 'lodash'
import picturefill from 'picturefill/dist/picturefill'
import { defaultAvatarObject } from '../../src/scripts/showDefaultAvatar'
import { VERIFICATION_APPROVE_STATUS, VOTE_BUTTON_VOTE, VOTE_BUTTON_VIEW } from '../../constants/userProfile'
import { MY_PROFILE_PAGE, CDN_URL } from '../../constants/Enviroment'
import VoteButton from '../commons/VoteButton'
import ProfileAvatarPictureWithPolling from './ProfileAvatarPictureWithPolling'

class ProfileAvatar extends React.Component {

  renderVerifiedUserIcon() {
    const { user } = this.props;
    const score = user.verification_status ? user.verification_status.score : 0;
    let url, verifyAlt = ''
    if(score === undefined || score == 0) {
      return ''
    }

    if(score <= 30) {
      url = `${CDN_URL}/general/Verify/Bronze.png`
      verifyAlt = 'verify-bronze'
    }else if(score <= 70 && score > 31) {
      url = `${CDN_URL}/general/Verify/Silver.png`
      verifyAlt = 'verify-silver'
    }else {
      url = `${CDN_URL}/general/Verify/Gold.png`
      verifyAlt = 'verify-gold'
    }

    return (
      <div className="card__verify card__verify--2">
        <img className="img-round img-verify" src={url} alt={verifyAlt} />
      </div>
    )
  }

  render() {
    const { user, isUploadingAvatar } = this.props
    const isVerifiedUser = user? user.verification_status === VERIFICATION_APPROVE_STATUS : false;
    if (_.isEmpty(user)) {
      return <div>Loading...</div>
    }
    let userPicture = ''
    const is_have_avatar = user.user_pictures.filter(pic => pic.is_main == true).length > 0
    if (user.user_pictures.length && user.user_pictures[0].is_main) {
      userPicture = user.user_pictures[0]
    } else {
      userPicture = defaultAvatarObject(user.gender, 'large')
    }
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 card-user-avatar">
            <article
              className={isUploadingAvatar ? 'card mbl is-loading' : 'card mbl'}
              /** 2: pciture type for avatar **/
            >
              <div className="card__upper" onClick={this.props.openModal.bind(null, 2)}>
                <a href="#" className="card__link">
                  <ProfileAvatarPictureWithPolling
                    user={user}
                    userPicture={userPicture}
                  />

                  <div className="card__meta card__meta--1 card__meta--2">
                    Thay ảnh đại diện
                  </div>
                </a>
              </div>
              {this.renderVerifiedUserIcon()}
              {is_have_avatar &&
                <VoteButton
                  type={VOTE_BUTTON_VIEW}
                  style={ isVerifiedUser ? 2 : 1}
                  picture={userPicture}
                  page_source={MY_PROFILE_PAGE}
                />
              }
            </article>
          </div>
        </div>
      </div>
    )
  }
}

ProfileAvatar.propTypes = {
  openModal: PropTypes.func,
  user: PropTypes.object,
  isUploadingAvatar: PropTypes.bool,
}

export default ProfileAvatar
