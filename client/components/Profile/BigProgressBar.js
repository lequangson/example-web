import React, { PropTypes } from 'react';
import SmallProgressBar from './SmallProgressBar'

class BigProgressBar extends React.Component {
	getClassName(className) {
		const level1Value = 30;
		const level2Value = 70;
		const level3Value = 99;
		if (!this.props.completion || this.props.completion === 0 ) {
			return className + 0;
		}
		else if (this.props.completion <= level1Value) {
			return className + 1;
		}
		else if (this.props.completion <= level2Value) {
			return className + 2;
		}
		else if (this.props.completion <= level3Value) {
			return className + 3;
		}
		else {
			return className + 4;
		}
	}

	getStyle() {
		const completion = this.props.completion? this.props.completion : 0;
		return {
  		width: completion + '%'
  	}
	}

	getStyle2() {
		const completion = this.props.completion? this.props.completion - 3 : 3;
		return {
  		left: completion + '%'
  	}
	}

  render() {
    return (
  		<div className="container">
	    	<div className="progress-fix-top">
				  <div className="progress clearfix mbl">
				    <div className={this.getClassName('progress__header progress__header--')} style={this.getStyle2()}>
				    	<SmallProgressBar completion={this.props.completion} />
				    </div>
				    <div className="progress__bar">
				      <div className={this.getClassName('progress__fill progress__fill--')} style={this.getStyle()}></div>
				    </div>
				    <div className="progress__txt l-flex-sb padding-t10">
				    	<span>Kém tự tin</span>
				    	<span>Tự tin hơn</span>
				    	<span>Hấp dẫn</span>
				    </div>
				  </div>
				</div>
			</div>	
  	)
  }
}

BigProgressBar.propTypes = {
  completion: PropTypes.number,
}

export default BigProgressBar
