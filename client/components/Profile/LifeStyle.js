import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import SelectEditable from '../commons/SelectEditable'
import SelectMultipleEditable from '../commons/SelectMultipleEditable'
import IconEditable from '../commons/IconEditable'
import { MAXIMUM_PERSONAL_STYLE } from '../../constants/userProfile'
import { MAXIMUM_PERSONAL_STYLE_ERROR_MSG } from '../../constants/TextDefinition'

class LifeStyle extends React.Component {

  render() {
    const { user, mode, dictionaries } = this.props
    if (_.isEmpty(user)) {
      return (
        <div className="panel mbl">
          <div className="panel__heading">
            <h3 className="panel__title">Quan điểm - Phong cách sống</h3>
          </div>
        </div>
      )
    }
    return (
      <div className="panel mbl">
        <div className="panel__heading">
          <h3 className="panel__title">Quan điểm - Phong cách sống</h3>
        </div>
        <div className="panel__body">
          <table className="table table--a">
            <tbody>
              <tr id="hobby">
                <td className="txt-light">Sở thích</td>
                <td className="txt-blue">
                  { mode === 'edit' &&
                    <Link to="/myprofile/update-hobbies" className="txt-blue l-flex-grow">
                      <ul className="list-unstyled">
                        {
                          user.user_hobbies.length
                          ? user.user_hobbies.map((hobby, i) => (
                            <li key={hobby.key}>{hobby.value} { mode === 'edit' && i === 0 }</li>)
                          )
                          : <div className="txt-red">Chưa cập nhật</div>
                        }
                      </ul>
                      <IconEditable />
                    </Link>
                  }
                  { mode !== 'edit' &&
                    <ul className="list-unstyled">
                      {
                        user.user_hobbies.length
                        ?
                          user.user_hobbies.map(hobby =>
                            (<li key={hobby.key}>{hobby.value}</li>))
                        : <li>Không rõ</li>
                      }
                    </ul>
                  }
                </td>
              </tr>
              <tr>
                <td className="txt-light">Tính cách nổi bật</td>
                {
                  mode === 'read' &&
                    <td className="txt-blue">
                      <ul className="list-unstyled">
                        {
                          user.personality_types.length
                          ? user.personality_types.map((personality, i) =>
                              (<li key={i}>{personality.value}</li>))
                          : <li>Không rõ</li>
                        }
                      </ul>
                    </td>
                }
                {
                  mode !== 'read' &&
                    <td>
                      <SelectMultipleEditable
                        updateUserProfile={this.props.updateUserProfile}
                        id="personality_types"
                        name="personal[personality_types][]"
                        value={user.personality_types}
                        data={dictionaries.personal_types}
                        max_size={MAXIMUM_PERSONAL_STYLE}
                        max_size_msg={MAXIMUM_PERSONAL_STYLE_ERROR_MSG}
                      />
                    </td>
                }
              </tr>
              <tr>
                <td className="txt-light">Quan hệ xã hội</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.sociability.value ? user.sociability.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[sociability][key]"
                        value={user.sociability.key || 0}
                        data={dictionaries.sociabilities}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Mong muốn gặp mặt</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.hope_meet.value ? user.hope_meet.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[hope_meet][key]"
                        value={user.hope_meet.key || 0}
                        data={dictionaries.hope_meets}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Mong muốn kết hôn</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.intention_marriage.value ? user.intention_marriage.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[intention_marriage][key]"
                        value={user.intention_marriage.key || 0}
                        data={dictionaries.intention_marriages}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Sẵn sàng làm việc nhà</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.house_work.value ? user.house_work.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[house_work][key]"
                        value={user.house_work.key || 0}
                        data={dictionaries.house_works}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Ngày nghỉ</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.holiday.value ? user.holiday.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[holiday][key]"
                        value={user.holiday.key || 0}
                        data={dictionaries.holidays}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Uống rượu</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.drinking.value ? user.drinking.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[drinking][key]"
                        value={user.drinking.key || 0}
                        data={dictionaries.drinkings}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Hút thuốc</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.smoking_type.value ? user.smoking_type.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[smoking_type][key]"
                        value={user.smoking_type.key || 0}
                        data={dictionaries.smoking_types}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Chi phí cho lần hẹn đầu</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.first_dating_cost.value ? user.first_dating_cost.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[first_dating_cost][key]"
                        value={user.first_dating_cost.key || 0}
                        data={dictionaries.first_dating_costs}
                      />
                    )
                  })()}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

LifeStyle.propTypes = {
  updateUserProfile: PropTypes.func,
  dictionaries: PropTypes.object,
  mode: PropTypes.string,
  user: PropTypes.object,

}

export default LifeStyle
