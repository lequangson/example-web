import React, { Component, PropTypes } from 'react'

import { CDN_URL } from 'constants/Enviroment'

class UserVerification extends Component {

  static propTypes = {
    score: PropTypes.number.isRequired,
  }

  constructor(props) {
    super(props)
  }

  renderStatusImage() {
    const { score } = this.props
    if(!score && score === 0){
      return ''
    }
    if(score <= 30) {
      return (
        <div>
          <div className="verify-status__image">
            <img className="img-round img-70 mbs" src={`${CDN_URL}/general/Verify/Bronze.png`} alt="verify-bronze" />
          </div>
          <span className="txt-warning txt-bold">Xác thực</span>
        </div>
      )
    }else if(score <= 70 && score > 31){
      return (
        <div>
          <div className="verify-status__image">
            <img className="img-round img-70 mbs" src={`${CDN_URL}/general/Verify/Silver.png`} alt="verify-silver" />
          </div>
          <span className="txt-warning txt-bold">Đáng tin</span>
        </div>
      )
    }else {
      return (
        <div>
          <div className="verify-status__image">
            <img className="img-round img-70 mbs" src={`${CDN_URL}/general/Verify/Gold.png`} alt="verify-gold" />
          </div>
          <span className="txt-warning txt-bold">Uy tín</span>
        </div>
      )
    }
    return null
  }

  render() {
    const { score } = this.props;
  
    if(!score || score === 0){ 
      return <div />
    }  

    return (
      <div className="panel">
        <div className="panel__heading">
          <h3 className="panel__title">{`Hồ sơ đã được xác thực`}</h3>
        </div>
        <div className="panel__body txt-center padding-t10 padding-b10" id="verification">
          {this.renderStatusImage()}
        </div>
      </div>
    );
  }
}

export default UserVerification;