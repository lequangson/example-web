import React, { PropTypes } from 'react'

const Description = props => (
  <div className="panel">
    <div className="panel__heading">
      <h3 className="panel__title">Giới thiệu bản thân</h3>
    </div>
    <div className="panel__body">
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <p>{props.user.self_introduction || 'Chưa có mô tả'}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
)

Description.propTypes = {
  user: PropTypes.object,
}

export default Description
