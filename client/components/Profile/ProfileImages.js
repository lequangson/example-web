import React, { PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { Link } from 'react-router'
import { BANNER_ICON_URL, MY_PROFILE_PAGE } from '../../constants/Enviroment'
import { OTHER_PICTURE, VOTE_BUTTON_VIEW } from '../../constants/userProfile'
import VoteButton from '../commons/VoteButton'
import $ from 'jquery'
import slick from '../../../public/scripts/slick.min.js'
import PollingImg from '../commons/PollingImg'

class ProfileImages extends React.Component {
  constructor() {
    super()
    this.state = {
      modalIsOpen: false,
      pictureInfo: {},
      deleting: [],
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.deletePicture = this.deletePicture.bind(this)
    this.preloadImage = this.preloadImage.bind(this)
  }


  _carousel() {

    const carousel = $('.carousel-subPhoto');
    if (!carousel) {
      return;
    }
    // fb carousel
    carousel.slick({
      slidesToShow: 6,
      slidesToScroll: 6,
      arrows: false,
      infinite: false,
      initialSlide: 0,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 8,
          },
        },
        {
          breakpoint: 425,
          settings: {
            slidesToShow: 5,
          },
        },
        {
          breakpoint: 321,
          settings: {
            slidesToShow: 4,
          },
        },
      ],
    })
  }

  _unCarousel() {
    //if ($('.carousel-form .carousel-subPhoto')) {
    if ($('.carousel-subPhoto')) {
      $('.carousel-subPhoto').slick('unslick')
    }
  }

  componentDidMount() {
    this._carousel()
    this.preloadImage()
  }

  shouldComponentUpdate(nextProps, nextState) {
    // console.log('shouldComponentUpdate', nextState !== this.state, nextProps.user !== this.props.user)
    return nextState !== this.state ||
      nextProps.user !== this.props.user
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.user !== this.props.user) {
      this._unCarousel()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.user !== this.props.user) {
      this._carousel()
      this.preloadImage()
    }
  }

  openModal(pictureType, id, index, pictureUrl, is_main, voted_user_ids) {
    this.setState({
      modalIsOpen: true,
      pictureInfo: {
        id,
        index,
        pictureType,
        pictureUrl,
        is_main,
        voted_user_ids
      },
    })
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  nextPicture_OnClick(pictureIndex, stepIndex) {
    const userPictures = (this.props.user && this.props.user.user_pictures)
      ?
        this.props.user.user_pictures.filter(picture => !picture.is_main)
      :
        this.props.user.user_pictures
    const picture = userPictures[pictureIndex + stepIndex]
    this.setState({
      modalIsOpen: true,
      pictureInfo: {
        id: picture.id,
        index: pictureIndex + stepIndex,
        pictureUrl: picture.large_image_url,
        pictureType: OTHER_PICTURE,
        is_main: picture.is_main,
        voted_user_ids: picture.voted_user_ids,
      },
    })
  }

  deletePicture() {
    const pictureType = this.state.pictureInfo.pictureType
    const id = this.state.pictureInfo.id

    let current_deleting = this.state.deleting
    current_deleting.push(id)
    this.setState({ deleting: current_deleting })
    this.props.deleteUserPicture(pictureType, id).then(() => {
    }, () => {
      // remove loading icon
      current_deleting = this.state.deleting
      const failure_index = current_deleting.indexOf(id)
      if (failure_index > -1) {
        current_deleting.splice(failure_index, 1)
      }
      this.setState({ deleting: current_deleting })
    })
    this.closeModal()
  }

  preloadImage() {
    const userPictures = (this.props.user && this.props.user.user_pictures)
      ?
        this.props.user.user_pictures.filter(picture => !picture.is_main)
      :
        this.props.user.user_pictures
    if (userPictures && userPictures.length > 0) {
      const thumbImgArray = []
      const lastIndex = userPictures.length - 1
      userPictures.slice(0, lastIndex).map((picture, i) => {
        thumbImgArray[i] = new Image()
        thumbImgArray[i].src = picture.large_image_url
      })
    }
  }

  render() {
    const { isUploadingAvatar, isUploadingSubPhoto } = this.props
    // Do not show avatar pictute here
    const userPictures = (this.props.user && this.props.user.user_pictures)
      ?
        this.props.user.user_pictures.filter(picture => !picture.is_main)
      :
        this.props.user.user_pictures;
    const pictures = (userPictures)
      ? userPictures.map((p, index) => (
        <div className="item" key={index}>
          <div
            className={(this.state.deleting.indexOf(p.id) > -1 || isUploadingAvatar || isUploadingSubPhoto) ? 'brick is-loading' : 'brick'}
            onClick={() => {
              this.openModal(OTHER_PICTURE, p.id, index, p.large_image_url, p.is_main, p.voted_user_ids)
            }}
          >
            <PollingImg src={p.thumb_image_url} alt={this.props.user.nick_name} />
          </div>
        </div>
      ))
      : []
    const pictureUrl = this.state.pictureInfo.pictureUrl
    const pictureId = this.state.pictureInfo.id
    const pictureIndex = this.state.pictureInfo.index
    const nextPicture = typeof pictureIndex !== 'undefined' ? userPictures[pictureIndex + 1] : ''
    const prevPicture = typeof pictureIndex !== 'undefined' ? userPictures[pictureIndex - 1] : ''
    return (
      <div className="panel">
        <div className="panel__heading">
          <h3 className="panel__title">Ảnh
          </h3>
        </div>
        <div className="panel__body">
          { (!pictures || pictures.length === 0) ?
            <div className="container">
              <p className="txt-red txt-center mbm">
                Chia sẻ những hình ảnh mới nhất của bạn
              </p>
              <div className="row">
                <div className="col-xs-8 col-xs-offset-2">
                  <button
                    className="btn btn--p btn--b mbm"
                    onClick={() => { this.props.openModal(3) }}
                  >
                    Tải ảnh lên
                  </button>
                </div>
              </div>
            </div>
            :
              <div className="container">
                {/* show section below after user upload images successful */}
                <div className="list-group carousel-subPhoto">
                  <div className="item">
                    <button
                      className="btn btn--p btn--c btn--2"
                      onClick={() => { this.props.openModal(3) }}
                      disabled={isUploadingSubPhoto}
                    >
                      <i className="fa fa-plus-circle fa-2x" /><br />Thêm ảnh
                    </button>
                  </div>
                  {pictures}
                </div>
              </div>
          }
        </div>
        <Modal
          transitionName="modal"
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="modal__content"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="row">
            <div className="col-xs-12 mbm">
              <div className="modal__frame">
                <img src={pictureUrl} alt={this.props.user.nick_name} />
                <button
                  className={`pagi pagi--prev pagi--2${prevPicture ? '' : ' is-hide'}`}
                  onClick={this.nextPicture_OnClick.bind(this, pictureIndex, -1)}
                >
                  <i className="fa fa-chevron-left"></i>
                </button>
                <button
                  className={`pagi pagi--next pagi--2${nextPicture ? '' : ' is-hide'}`}
                  onClick={this.nextPicture_OnClick.bind(this, pictureIndex, 1)}
                >
                  <i className="fa fa-chevron-right"></i>
                </button>
                <VoteButton
                  type={VOTE_BUTTON_VIEW}
                  style={1}
                  picture={this.state.pictureInfo}
                  page_source={MY_PROFILE_PAGE}
                />
              </div>
            </div>
            <div className="col-xs-12 mbm">
              {/* <button className="btn btn--b" onClick={this.closeModal}>
                {CONFIRMATION_BUTTON_NO}
              </button> */}
              <Link to={`/change-avatar/other-picture/${pictureId}`}
                className="btn btn--p btn--b"
              >
                Sử dụng làm ảnh đại diện
              </Link>
            </div>
            <div className="col-xs-12">
              <button
                className="btn btn--b"
                onClick={this.deletePicture}
              >
                {/* {CONFIRMATION_BUTTON_YES} */}
                Xóa ảnh này
              </button>
            </div>

            <button
              className="modal__btn-close"
              onClick={this.closeModal}
            >
              <i className="fa fa-times" />
            </button>

          </div>
        </Modal>
      </div>
    )
  }
}

ProfileImages.propTypes = {
  openModal: PropTypes.func,
  user: PropTypes.object,
  deleteUserPicture: PropTypes.func,
  isUploadingAvatar: PropTypes.bool,
  isUploadingSubPhoto: PropTypes.bool,
}

export default ProfileImages
