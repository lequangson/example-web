import React, { PropTypes } from 'react'
import _ from 'lodash'
import SelectEditable from '../commons/SelectEditable'

class Family extends React.Component {

  render() {
    const { user, mode, dictionaries } = this.props
    if (_.isEmpty(user)) {
      return (
        <div className="panel">
          <div className="panel__heading">
            <h3 className="panel__title">Gia đình</h3>
          </div>
        </div>
      )
    }
    return (
      <div className="panel">
        <div className="panel__heading">
          <h3 className="panel__title">Gia đình</h3>
        </div>
        <div className="panel__body">
          <table className="table table--a">
            <tbody>
              <tr>
                <td className="txt-light">Tình trạng hôn nhân</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.marital_status.value ? user.marital_status.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[marital_status][key]"
                        value={user.marital_status.key || 0}
                        data={dictionaries.relationship_statuses}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Đã có con</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.presence_child_status.value ? user.presence_child_status.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[presence_child_status][key]"
                        value={user.presence_child_status.key || 0}
                        data={dictionaries.presence_child_statuses}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Mong muốn có con</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.willing_have_child.value ? user.willing_have_child.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[willing_have_child][key]"
                        value={user.willing_have_child.key || 0}
                        data={dictionaries.willing_have_children}
                      />
                    )
                  })()}
                </td>
              </tr>
              <tr>
                <td className="txt-light">Tình trạng sinh sống</td>
                <td className="txt-blue">
                  {(() => {
                    if (mode === 'read') {
                      return user.house_mate.value ? user.house_mate.value : 'Không rõ'
                    }
                    return (
                      <SelectEditable
                        updateUserProfile={this.props.updateUserProfile}
                        name="personal[house_mate][key]"
                        value={user.house_mate.key || 0}
                        data={dictionaries.house_mates}
                      />
                    )
                  })()}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

Family.propTypes = {
  mode: PropTypes.string,
  dictionaries: PropTypes.object,
  updateUserProfile: PropTypes.func,
  user: PropTypes.object,
}

export default Family
