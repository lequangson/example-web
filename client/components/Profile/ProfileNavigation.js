import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import reactTimeout from 'react-timeout';
import $ from 'jquery';
import {
  SEARCH_PAGE,
  PEOPLE_LIKED_ME_PAGE,
  FAVORITE_PAGE,
  FOOTPRINT_PAGE,
  GA_ACTION_ADD_FAVORITE,
  GA_ACTION_DELETE_FAVORITE,
  BLOCK_USER_BLOCKED_PAGE, BLOCK_USER_MUTED_PAGE,
  PEOPLE_I_LIKED_PAGE,
  MATCHED_PAGE,
  MY_PROFILE_PAGE,
} from 'constants/Enviroment';
import { compareIdentifiers, isBlockedUser, inLikedList, inVisitList } from 'utils/common';
import toggleBackdrop from 'src/scripts/toggleBackdrop';
import BlockButton from 'components/commons/BlockButton';
import {
  NOTIFICATION_TYPE_LIKE, NOTIFICATION_TYPE_CHAT, NOTIFICATION_TYPE_CHAT_APP
} from 'constants/userProfile';
import * as ymmStorage from 'utils/ymmStorage';
import socket from "utils/socket";
import { ACTIVE_USER } from 'constants/userProfile';

class ProfileNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clickedFavorite: false
    };

    this.onClickAddFavorite = this.onClickAddFavorite.bind(this);
    this.onClickDeleteFavorite = this.onClickDeleteFavorite.bind(this);
    this.collectGaData = this.collectGaData.bind(this);
    this.onClickDeleteFavorite = this.onClickDeleteFavorite.bind();
  }

  componentDidMount() {
    toggleBackdrop($);
  }

  onClickAddFavorite(user) {
    user.is_favorite_friend = true;
    this.props.updateSearchUserProfile(user.identifier, 'is_favorite_friend', true);
    this.collectGaData(user);
    ymmStorage.removeItem('inprogress');
  }

  onClickDeleteFavorite(user) {
    user.is_favorite_friend = false;
    this.props.updateSearchUserProfile(user.identifier, 'is_favorite_friend', false);
    this.collectGaData(user);
  }

  getPreviousNavigation() {
    if (!this.props.enviroment || !this.props.enviroment.previous_page) {
      return '/search';
    }

    switch (this.props.enviroment.previous_page) {
      case SEARCH_PAGE:
        return '/search';
      case PEOPLE_LIKED_ME_PAGE:
        return '/people-like-me';
      case FAVORITE_PAGE:
        return '/favorite-lists';
      case BLOCK_USER_BLOCKED_PAGE:
        return '/blocked-users';
      case BLOCK_USER_MUTED_PAGE:
        return '/muted-users';
      case FOOTPRINT_PAGE:
        return '/visitants';
      case PEOPLE_I_LIKED_PAGE:
        return '/people-i-liked';
      case MATCHED_PAGE:
        return '/matched-list';
      case MY_PROFILE_PAGE:
        return '/myprofile';
      default:
        return '/search';
    }
  }

  handleAddFavorite() {
    const { user_profile } = this.props;
    const { current_user } = user_profile;
    if (this.state.clickedFavorite) {
      return;
    }
    const user = this.props.user;
    this.setState({ clickedFavorite: true });
    this.props.addFavorite(user.identifier).then(() => {
      this.setState({ clickedFavorite: false });
      this.onClickAddFavorite(user);
      if (current_user.user_status == ACTIVE_USER) {
        const userPicture = current_user && current_user.user_pictures && current_user.user_pictures.length > 0 ?
          current_user.user_pictures[0].small_image_url : '';
        socket.emit('send_event', {
          room: user.identifier,
          type: 'add_favorite',
          to_identifier: user.identifier,
          from_identifier: current_user.identifier,
          from_nick_name: current_user.nick_name,
          avatar: userPicture
        })
      }
    }).catch(() => {
      this.setState({ clickedFavorite: false });
      user.is_favorite_friend = false;
    })
  }

  handleRemoveFavorite() {
     if (this.state.clickedFavorite) {
      return;
    }
    const user = this.props.user;
    this.setState({ clickedFavorite: true });
    this.props.deleteFavorite(user.identifier).then(() => {
      this.setState({ clickedFavorite: false });
      this.onClickDeleteFavorite(user);
    }).catch(() => {
      this.setState({ clickedFavorite: false });
      user.is_favorite_friend = true;
    })
  }
  collectGaData(target) {
    const gaAction = target.is_favorite_friend ?
                                    GA_ACTION_ADD_FAVORITE :
                                    GA_ACTION_DELETE_FAVORITE;
    const ga_track_data = {
      partner_residence: target.residence.value || '',
      partner_birth_place: target.birth_place.value || '',
      partner_age: target.age,
    };
    this.props.gaSend(gaAction, ga_track_data);
  }

  render() {
    const user = this.props.user;
    const userResidence = user.residence.value || '';
    const userInfo = `${user.nick_name} - ${user.age || '?'} tuổi ${userResidence ? `- ${userResidence}` : ''}`;
    return (
      <div className="container">
        <div className="row txt-center">
          <div className="col-xs-12 l-flex txt-center mbm">
            {(user.is_favorite_friend)
              ? <button
                type="button"
                className="btn btn--5 mrs"
                onClick={() => { this.handleRemoveFavorite() }}
              >
                Bỏ quan tâm
              </button>
              : <button
                type="button"
                className="btn btn--5 mrs"
                onClick={() => { this.handleAddFavorite() }}
              >
                <i className="fa fa-star txt-green padding-r5"></i>
                Quan tâm
              </button>
            }
            <Link to={`/report/${user.identifier || ''}`} className="btn btn--5 txt-center mrs">
              <i className="fa fa-warning txt-warning padding-r5"></i>
              Báo cáo
            </Link>
            <BlockButton
              user={user}
              blockUser={this.props.blockUser}
              updateReducerState={this.props.updateReducerState}
              Enviroment={this.props.Enviroment}
              unBlockUser={this.props.unBlockUser}
              gaSend={this.props.gaSend}
              unBlockNotificationByIdentifier={this.props.unBlockNotificationByIdentifier}
              blockNotificationByIdentifier={this.props.blockNotificationByIdentifier}
            />
          </div>
        </div>
      </div>
    )
  }
}

ProfileNavigation.propTypes = {
  deleteFavorite: PropTypes.func,
  blockNotificationByIdentifier: PropTypes.func,
  unBlockNotificationByIdentifier: PropTypes.func,
  gaSend: PropTypes.func,
  blockUser: PropTypes.func,
  unBlockUser: PropTypes.func,
  updateReducerState: PropTypes.func,
  enviroment: PropTypes.object,
  user_profile: PropTypes.object,
  socket: PropTypes.object,
  user: PropTypes.object,
  updateSearchUserProfile: PropTypes.func,
  addFavorite: PropTypes.func,
}

export default reactTimeout(ProfileNavigation);
