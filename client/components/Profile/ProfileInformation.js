import React, { PropTypes } from 'react'
import BasicInformation from './BasicInformation'
import EducationInformation from './EducationInformation'
import Family from './Family'
import LifeStyle from './LifeStyle'
import ProfileAvatar from './ProfileAvatar'
import ProfileImages from './ProfileImages'
import UpgradeAccountStatus from './UpgradeAccountStatus'
import ProfileIntroduction from './ProfileIntroduction'
import ProfileMatchQuestion from './ProfileMatchQuestion'
import ProfileListPresent from 'pages/campaign/ProfileListPresent'
import UserVerification from '../Profile/UserVerification'

class ProfileInformation extends React.Component {

  render() {
    const { mode, user, campaign } = this.props
    const score = user.verification_status ? user.verification_status.score : 0;
    return (
      <div>
        { mode === 'edit' && <ProfileAvatar {...this.props} updateUserProfile={this.props.updateUserProfile} /> }
        { mode === 'edit' && <ProfileImages {...this.props} updateUserProfile={this.props.updateUserProfile} /> }
        { mode === 'edit' && <UserVerification {...this.props} score={score} /> }
        { mode === 'edit' && user.gender === 'male' && <UpgradeAccountStatus score={score} user={user} /> }
        { mode === 'edit' && <ProfileIntroduction {...this.props} updateUserProfile={this.props.updateUserProfile} /> }
        { (mode === 'edit' && !!campaign.my_gifts.length) && <ProfileListPresent />}
        <ProfileMatchQuestion {...this.props} />
        <BasicInformation {...this.props} />
        <EducationInformation {...this.props} />
        <Family {...this.props} />
        <LifeStyle {...this.props} />
        <div className="well"></div>
      </div>
    )
  }
}

ProfileInformation.propTypes = {
  updateUserProfile: PropTypes.func,
  mode: PropTypes.string,

}

export default ProfileInformation
