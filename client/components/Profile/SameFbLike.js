import React, { PropTypes } from 'react'
import $ from 'jquery'
import slick from '../../../public/scripts/slick.min.js'
import { getSameInterest, getShortName } from '../../utils/FacebookInterest';

class SameFbLike extends React.Component {
  constructor(props) {
    super(props)
  }

  _carousel() {

    if (!$('.carousel-fb')) {
      return;
    }
    // fb carousel
    $('.carousel-fb').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      infinite: true,
      initialSlide: 0,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
          },
        },
        {
          breakpoint: 425,
          settings: {
            slidesToShow: 4,
          },
        },
      ],
    })
  }

  _unCarousel() {
    if ($('.carousel-fb')) {
      $('.carousel-fb').slick('unslick')
    }
  }

  componentDidMount() {
      this._carousel()
  }


  componentWillUpdate() {
    this._unCarousel()
  }

  componentDidUpdate() {
    this._carousel()
    if ($('.carousel-fb')) {
      $('.carousel-fb').slick('slickGoTo', 0)
    }
  }


  // shouldComponentUpdate(nextProps, nextState) {
  //   return !_.isEqual(nextProps.user, this.props.user);
  // }

  render() {
    const { user, user_profile } = this.props;
    const { current_user } = user_profile;
    // console.log('user', user, 'current_user', current_user);
    if (
      typeof user.facebook_interests === 'undefined' || typeof current_user.facebook_interests === 'undefined' ||
      !user.facebook_interests.length || !current_user.facebook_interests.length) {
      return <div></div>
    }
    const sameFacebookInterestPage = getSameInterest(current_user.facebook_interests, user.facebook_interests);
    // console.log('sameFacebookInterestPage', sameFacebookInterestPage);
    if (!sameFacebookInterestPage.length) {
      return <div></div>
    }
    return (
      <div className="panel">
        <div className="panel__heading mbm">
          <h3 className="panel__title">{`Sở thích chung (${sameFacebookInterestPage.length})`}</h3>
        </div>
        <div className="clearfix">
          <div className="col-xs-12 user-sub-carousel">
            <div className="carousel-fb mbm" >
              {
                sameFacebookInterestPage.map((samePage, i) => (
                    <div className="txt-center" key={i}>
                      <img className="img-round" src={samePage.picture_url} alt={samePage.name} />
                      <span>{getShortName(samePage.name)}</span>
                    </div>
                  ))
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

SameFbLike.propTypes = {
}

export default SameFbLike