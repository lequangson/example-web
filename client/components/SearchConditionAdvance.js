import React, { PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import $ from 'jquery';
import _ from 'lodash';
import ValidateRange from 'src/scripts/validateRange';
import {SEARCH_MAX_AGE, SEARCH_MAX_HEIGHT, SEARCH_DONT_CARE} from 'constants/search';
import SubMenu from 'components/headers/SubMenu';
import {
  GA_ACTION_SAVE_SEARCH_CONDITION,
} from 'constants/Enviroment';
import * as ymmStorage from 'utils/ymmStorage';
import DropdownPanel from './commons/DropdownPanel';
import SelectMultiple from './commons/SelectMultiple';

class SearchConditionAdvance extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      resetSelected: false,
      resetClickStatus: true,
    };
  }

  componentWillMount() {
    ymmStorage.setItem('searchmode', 2);
    this.props.getSearchCondition();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state !== nextState) {
      return true;
    }

    if (this.props.search.condition !== nextProps.search.condition) {
      return true;
    }

    return false;
  }

  componentDidMount() {
    // List all locations user selected
    //this._displayLocations()
    window.scroll(0, 0);
    // check all uncheck input as initial state
    $('.js-uncheck').prop('checked', true)
    this._checkUncheckInput();

    // remember checkbox
    this._bindConditionData();

    // check 'khong quan tam' checkbox again when form reset
    this._handleFormReset();

    const form = this.refs.form;
    const submitBtn = this.refs.searchBtn;
    const age = new ValidateRange({element: '.js-validate-age'});
    const height = new ValidateRange({element: '.js-validate-height'});

    form.addEventListener('change', function() {
      if (age.validate() && height.validate()) {
        submitBtn.removeAttribute('disabled');
      } else {
        submitBtn.setAttribute('disabled', 'disabled');
      }
    })
    //the following is hacking for hide keyboard on ios when unfocus textbox
    $( '*' ).click( function ( ) { } );
    $( 'html' ).css( "-webkit-tap-highlight-color", "rgba(0, 0, 0, 0)" );
  }

  componentDidUpdate() {
    const condition = this.props.search.condition.find((el) => el.search_type == 0);
    let selectNode = this.refs.age_min;
    selectNode.value = condition.age_min ? condition.age_min : 0

    let selectNode2 = this.refs.age_max
    selectNode2.value = condition.age_max ? condition.age_max : 0

    let height_min = this.refs.height_min
    height_min.value = condition.height_min ? condition.height_min : 0

    let height_max = this.refs.height_max
    height_max.value = condition.height_max ? condition.height_max : 0

/*    let last_visit = this.refs.last_visit
    last_visit.value = condition.last_visit ? condition.last_visit : 0

    let smoking_types = this.refs.smoking_types
    smoking_types.value = condition.smoking_types ? condition.smoking_types : 0

    let search_sort = this.refs.search_sort
    search_sort.value = condition.search_sort ? condition.search_sort : 0*/

    this._bindConditionData()
  }

  _bindConditionData(){
    const condition = this.props.search.condition.find((el) => el.search_type == 0);
    this._rememberCheckbox('#body_type_ids', condition.body_type_ids)
    this._rememberCheckbox('#blood_types', condition.blood_types)
    this._rememberCheckbox('#marital_status_ids', condition.marital_status_ids)

    this._rememberCheckbox('#smoking_types', condition.smoking_types)
    this._rememberCheckbox('#education_ids', condition.education_ids)
    this._rememberCheckbox('#occupation_ids', condition.occupation_ids)
    this._rememberCheckbox('#income_types', condition.income_types)
    this._rememberCheckbox('#house_mate_types', condition.house_mate_types)
    this._rememberCheckbox('#holiday_types', condition.holiday_types)
    this._rememberCheckbox('#drink_types', condition.drink_types)
    this._rememberCheckbox('#intention_marriages', condition.intention_marriages)

    if (condition.has_self_introduction != 'undefined' && condition.has_self_introduction){
      $("#has_self_introduction").prop('checked', true)
    }

    if (condition.has_sub_images != 'undefined' && condition.has_sub_images){
      $("#has_sub_images").prop('checked', true)
    }

    if (condition.new_user_only != 'undefined' && condition.new_user_only){
      $("#new_user_only").prop('checked', true)
    }

    if (condition.registered_in_3_days != 'undefined' && condition.registered_in_3_days){
      $("#registered_in_3_days").prop('checked', true)
    }
  }

  _handleFormReset() {
    // check 'khong quan tam' checkbox again when form reset
    const form = $('.form')
    const uncheckInput = $('.js-uncheck')

    form.on('reset', () => {
      uncheckInput.attr('checked', true)
    })
  }

  _isCheckboxChecked(checkboxs) {
    // return array hold checkboxs that checked
    // empty if not
    return checkboxs.filter(i => $(checkboxs[i]).prop('checked'))
  }
  _uncheckSelectBox(evt) {
    // uncheck all if user click checkbox 'khong quan tam'
    const uncheckInput = $(evt.target)
    const checkboxs = uncheckInput.closest('.js-checkbox-wrap').find('option')
    checkboxs.prop('selected', false)

  }
  _uncheckAll = (evt) => {
    // uncheck all if user click checkbox 'khong quan tam'
    const uncheckInput = $(evt.target)
    const checkboxs = uncheckInput.closest('.js-checkbox-wrap').find('.checkbox')
    checkboxs.prop('checked', false)

    // if all checkboxs uncheck -> uncheckInput checked
    if (!this._isCheckboxChecked(checkboxs).length) {
      uncheckInput.prop('checked', true)
    }
  }
  _checkUncheckInput() {
    // Listen change event on all checkboxs
    $('.js-checkbox-wrap').find('.checkbox').on('change', (evt) => {
      const elm = $(evt.target)
      const uncheckInput = elm.closest('.js-checkbox-wrap').find('.js-uncheck')
      const checkboxs = elm.closest('.js-checkbox-wrap').find('.checkbox')

      if (this._isCheckboxChecked(checkboxs).length) {
        // if one of checkboxs checked
        // set uncheckInput uncheck
        uncheckInput.prop('checked', false)
      } else {
        // vice versa
        uncheckInput.prop('checked', true)
      }
    })
  }
  _rememberCheckbox(section, data) {
    var data = data ? data : ''
    const elm = $(section)
    const checkboxs = elm.find('.checkbox')

    const dataArr = data.split(',')

    Array.prototype.forEach.call(checkboxs, (checkbox)=> {
      if (dataArr.indexOf(checkbox.value) !== -1) {
        $(checkbox).prop('checked', true)
        $(checkbox).trigger('change')
      }
    })
  }

  _renderAge(){
    return _.times(SEARCH_MAX_AGE).map(function(i) {
      if(i > 17){
        return <option className="checkbox" value={i} key={i}>{i}</option>
      }
    })
  }

  _renderHeight(){
    let list_height = new Array();

    for (var height = 135; height <= 200; height += 5){
      list_height.push(height);
    }

    return list_height.map((height) => {
      return(
        <option value={height} key={height}>{height}</option>
      )
    })
  }

  _renderBodyType(){
    return this.props.dictionaries.body_types.map((bodyType, i) => {
      return (
        <div key={bodyType.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" ref="body_type_ids" name="body_type_ids" className="checkbox" value={bodyType.key} /> {bodyType.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderBloodType(){
    return this.props.dictionaries.blood_types.map((bloodType, i) => {
      return (
        <div key={bloodType.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" ref="blood_types" name="blood_types" className="checkbox" value={bloodType.key} /> {bloodType.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderOccupations(){
    return this.props.dictionaries.occupations.map((occupation) => {
      return(
        <div key={occupation.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" name="occupation_ids" className="checkbox" value={occupation.key} /> {occupation.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderIncomes(disabled){
    return this.props.dictionaries.income_types.map((income) => {
      return(
        <div key={income.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input disabled={disabled} type="checkbox" ref="income_types" name="income_types" className="checkbox" value={income.key} /> {income.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderEducations(){
    return this.props.dictionaries.educations.map((education) => {
      return (
        <div key={education.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" name="education_ids" className="checkbox" value={education.key} /> {education.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderLastVisits() {

    return this.props.dictionaries.last_visits.map((lastVisit) => {
      return(
        <option value={lastVisit.key} key={lastVisit.key} >{lastVisit.value}</option>
      )
    })
  }

  _renderHouseMates() {
    return this.props.dictionaries.house_mates.map((houseMate) => {
      return (
        <div key={houseMate.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" name="house_mate_types" className="checkbox HouseMates" value={houseMate.key} /> {houseMate.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderMaritalStatus() {
    return this.props.dictionaries.relationship_statuses.map((maritalStatus) => {
      return (
        <div key={maritalStatus.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" name="marital_status_ids" className="checkbox HouseMates" value={maritalStatus.key} /> {maritalStatus.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }
  _renderHolidays() {
    return this.props.dictionaries.holidays.map((holiday) => {
      return (
        <div key={holiday.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" name="holiday_types" className="checkbox" value={holiday.key} /> {holiday.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderDrinkings() {
    return this.props.dictionaries.drinkings.map((drinking) => {
      return (
        <div key={drinking.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" name="drink_types" className="checkbox" value={drinking.key} /> {drinking.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderSmokingTypes() {
    return this.props.dictionaries.smoking_types.map((smokingType) => {
      return(
        <div key={smokingType.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" name="smoking_types" className="checkbox" value={smokingType.key} /> {smokingType.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderIntentionMarriages() {
    return this.props.dictionaries.intention_marriages.map((intention) => {
      return (
        <div key={intention.key} className="l-flex-grid__item">
          <label className="form__input--c">
            <input type="checkbox" name="intention_marriages" className="checkbox" value={intention.key} /> {intention.value}
            <div className="fake-checkbox"></div>
          </label>
        </div>
      )
    })
  }

  _renderSearchSort(){
    //search_sorts
    return this.props.dictionaries.search_sorts.map((sort) => {
      return(
        <option value={sort.key} key={sort.key} >{sort.value}</option>
      )
    })
  }

  handleClick(event) {

    event.preventDefault()
    var body_type_ids = [];
    $.each($("input[name='body_type_ids']:checked"), function(){
        body_type_ids.push($(this).val());
    });

    var blood_types = [];
    $.each($("input[name='blood_types']:checked"), function(){
        blood_types.push($(this).val());
    });
    var smoking_types = [];
    $.each($("input[name='smoking_types']:checked"), function(){
        smoking_types.push($(this).val());
    });
    var marital_status_ids = [];
    $.each($("input[name='marital_status_ids']:checked"), function(){
        marital_status_ids.push($(this).val());
    });

    var education_ids = [];
    $.each($("input[name='education_ids']:checked"), function(){
        education_ids.push($(this).val());
    });

    let occupation_ids = [];
    $.each($("input[name='occupation_ids']:checked"), function(){
        occupation_ids.push($(this).val());
    });

    let income_types = [];
    $.each($("input[name='income_types']:checked"), function(){
        income_types.push($(this).val());
    });

    var house_mate_types = [];
    $.each($("input[name='house_mate_types']:checked"), function(){
        house_mate_types.push($(this).val());
    });

    var holiday_types = [];
    $.each($("input[name='holiday_types']:checked"), function(){
        holiday_types.push($(this).val());
    });

    var drink_types = [];
    $.each($("input[name='drink_types']:checked"), function(){
        drink_types.push($(this).val());
    });

    var intention_marriages = [];
    $.each($("input[name='intention_marriages']:checked"), function(){
        intention_marriages.push($(this).val());
    });

    var constellation_types = [];
    $.each($("input[name='constellation_types']"), function(){
        constellation_types.push($(this).val());
    });

    var location_ids = [];
    $.each($("input[name='location_ids']"), function(){
        location_ids.push($(this).val());
    });

    var birth_place_ids = [];
    $.each($("input[name='birth_place_ids']"), function(){
        birth_place_ids.push($(this).val());
    });

    const params = {
      "search_condition[age_min]": this.refs.age_min.value != "0" ? this.refs.age_min.value : '',
      "search_condition[age_max]": this.refs.age_max.value != "0"  ? this.refs.age_max.value : '',
      "search_condition[location_ids]": location_ids.join(','),
      "search_condition[birth_place_ids]": birth_place_ids.join(','),
      "search_condition[marital_status_ids]": marital_status_ids.join(','),
      "search_condition[constellation_types]": constellation_types.join(','),
      "search_condition[height_min]": this.refs.height_min.value != "0" ? this.refs.height_min.value : '',
      "search_condition[height_max]": this.refs.height_max.value != "0" ? this.refs.height_max.value : '',
      "search_condition[body_type_ids]": body_type_ids.join(","),
      "search_condition[blood_types]": blood_types.join(","),
      "search_condition[occupation_ids]": occupation_ids.join(','),
      "search_condition[income_types]": income_types.join(','),
      "search_condition[education_ids]": education_ids.join(","),
      // "search_condition[last_visit]": this.refs.last_visit.value != "0" ? this.refs.last_visit.value : '',
      "search_condition[house_mate_types]": house_mate_types.join(","),
      "search_condition[holiday_types]": holiday_types.join(","),
      "search_condition[drink_types]": drink_types.join(","),
      "search_condition[smoking_types]": smoking_types.join(','),
      "search_condition[has_self_introduction]": $('#has_self_introduction').is(':checked') ? true : false,
      "search_condition[has_sub_images]": $('#has_sub_images').is(':checked') ? true : false,
      // "search_condition[registered_in_3_days]": $('#registered_in_3_days').is(':checked') ? true : false,
      // "search_condition[search_sort]": this.refs.search_sort.value != "0" ? this.refs.search_sort.value : '',
      "search_condition[intention_marriages]": intention_marriages.join(", "),
      "search_type": 0
    }

    const ga_params = {
      "age_min": this.refs.age_min.value != "0" ? this.refs.age_min.value : '',
      "age_max": this.refs.age_max.value != "0"  ? this.refs.age_max.value : '',
      "location_ids": location_ids.join(','),
      "birth_place_ids": birth_place_ids.join(','),
      "marital_status_ids": marital_status_ids.join(','),
      "constellation_types": constellation_types.join(','),
      "height_min": this.refs.height_min.value != "0" ? this.refs.height_min.value : '',
      "height_max": this.refs.height_max.value != "0" ? this.refs.height_max.value : '',
      "body_type_ids": body_type_ids.join(","),
      "blood_types": blood_types.join(","),
      "occupation_ids": occupation_ids.join(','),
      "income_types": income_types.join(','),
      "education_ids": education_ids.join(","),
      // "last_visit": this.refs.last_visit.value != "0" ? this.refs.last_visit.value : '',
      "house_mate_types": house_mate_types.join(","),
      "holiday_types": holiday_types.join(","),
      "drink_types": drink_types.join(","),
      "smoking_types": smoking_types.join(','),
      "has_self_introduction": $('#has_self_introduction').is(':checked') ? true : false,
      "has_sub_images": $('#has_sub_images').is(':checked') ? true : false,
      // "registered_in_3_days": $('#registered_in_3_days').is(':checked') ? true : false,
      // "search_sort": this.refs.search_sort.value != "0" ? this.refs.search_sort.value : '',
      "intention_marriages": intention_marriages.join(", "),
      "search_type": 0
    }

    const ga_new_params = _.pickBy(ga_params);

      const ga_track_data = {
          search_condition: ga_new_params
      };
      this.props.gaSend(GA_ACTION_SAVE_SEARCH_CONDITION, ga_track_data);
      this.props.searchConditionClick(params);
  }

  resetSelected = () => {
    this.props.resetSelectedConditions();
    const newResetClickStatus = !this.state.resetClickStatus;
    this.setState({
      resetSelected: true,
      resetClickStatus: newResetClickStatus
    });
  }

  render() {
    const condition = this.props.search.condition.find((el) => el.search_type == 0);
    const { current_user } = this.props.user_profile;
    return (
      <div className="site-content">
        <SubMenu mode='default' scrollToRight/>
        <form action="/" ref="form" className="form mbl" onSubmit={(event) => this.handleClick(event)}>
          {/*<div className="container mbm">
            <div className="row">
              <div className="col-xs-12 l-flex-ce">
                <button type="reset" onClick={this.resetSelectMultiple} className="btn btn--b">Thiết lập lại</button>
              </div>
            </div>
          </div>*/}
          <DropdownPanel
            panelTitle="Thông tin cơ bản"
            isOpen
          >
            <div  className="container">

              <div id="secondStep">
                <h3 className="form__sec-title">Tuổi</h3>
                <div className="row js-validate-age">
                  <div className="col-xs-6">
                    <select ref="age_min" id="age_min" name="age_min" className="js-validate-to form__input--b">
                      <option value="0">Bất kỳ</option>
                        {this._renderAge()}
                    </select>
                  </div>
                  <div className="col-xs-2 txt-center"></div>
                  <div className="col-xs-6">
                    <select ref="age_max" id="age_max" name="age_max" className="js-validate-from form__input--b">
                      <option value="0">Bất kỳ</option>
                        {this._renderAge(condition.age_max)}
                    </select>
                  </div>
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Chòm sao</h3>
              <div className="row">
                <div className="col-xs-12">
                  <SelectMultiple name="constellation_types" data={this.props.dictionaries.constellation_types} search={this.props.search}  type="0" reset={this.state.resetSelected}/>
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Chiều cao (cm)</h3>

              <div className="row js-validate-height">
                <div className="col-xs-6">
                  <select ref="height_min" className="js-validate-to form__input--b">
                    <option value="0">Bất kỳ</option>
                    {this._renderHeight(condition.height_min)}
                  </select>
                </div>
                <div className="col-xs-2 txt-center"></div>
                <div className="col-xs-6">
                  <select ref="height_max" className="js-validate-from form__input--b">
                    <option value="0">Bất kỳ</option>
                    {this._renderHeight(condition.height_max)}
                  </select>
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Nơi ở hiện tại</h3>
              <div className="row">
                <div className="col-xs-12">
                  <SelectMultiple name="location_ids" data={this.props.dictionaries.cities} search={this.props.search}  type="0" reset={this.state.resetSelected}/>
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Quê quán</h3>
              <div className="row">
                <div className="col-xs-12">
                  <SelectMultiple name="birth_place_ids" data={this.props.dictionaries.cities} search={this.props.search}  type="0" reset={this.state.resetSelected}/>
                </div>
              </div>

              <hr />

              <div id="thirdStep">
                <h3 className="form__sec-title">Thân hình</h3>
                <div className="row">
                  <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="body_type_ids">
                    <div className="l-flex-grid__item">
                      <label className="form__input--c">
                        <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                        <div className="fake-checkbox"></div>
                      </label>
                    </div>
                    {this._renderBodyType()}
                  </div>
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Nhóm máu</h3>
              <div className="row">
                <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="blood_types">
                  <div className="l-flex-grid__item">
                    <label className="form__input--c">
                      <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  {this._renderBloodType()}
                </div>
              </div>

              <hr />
            </div>
          </DropdownPanel>

          <DropdownPanel
            panelTitle="Học vấn - Nghề nghiệp"
          >
            <div className="container">
              <h3 className="form__sec-title">Trình độ học vấn</h3>
              <div className="row">
                <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="education_ids">
                  <div className="l-flex-grid__item">
                    <label className="form__input--c">
                      <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  {this._renderEducations()}
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Nghề nghiệp</h3>
              <div className="row">
                <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="occupation_ids">
                  <div className="l-flex-grid__item">
                    <label className="form__input--c">
                      <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  {this._renderOccupations(condition.occupation_ids)}
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Thu nhập hàng tháng</h3>
              {
                current_user.income_type === null &&
                <span className="txt-red l-flex-grow">
                  Bạn cần cập nhật thông tin về thu nhập của mình trước khi sử dụng tính năng này
                </span>
              }
              {
                current_user.income_type !== null &&
                <div className="row">
                  <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="income_types">
                    <div className="l-flex-grid__item">
                      <label className="form__input--c">
                        <input disabled={current_user.income_type === null} type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                        <div className="fake-checkbox"></div>
                      </label>
                    </div>
                    {this._renderIncomes(current_user.income_type === null)}
                  </div>
                </div>
              }
              <hr />

            </div>
          </DropdownPanel>

          <DropdownPanel
            panelTitle="Gia đình"
          >
          <div className="container">

          <h3 className="form__sec-title">Tình trạng hôn nhân</h3>
          <div className="row">
            <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="marital_status_ids">
              <div className="l-flex-grid__item">
                <label className="form__input--c">
                  <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                  <div className="fake-checkbox"></div>
                </label>
              </div>
              {this._renderMaritalStatus()}
            </div>
          </div>

          <hr />

            <h3 className="form__sec-title">Tình trạng sinh sống</h3>
            <div className="row">
              <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="house_mate_types">
                <div className="l-flex-grid__item">
                  <label className="form__input--c">
                    <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                    <div className="fake-checkbox"></div>
                  </label>
                </div>
                {this._renderHouseMates()}
              </div>
            </div>

            <hr />

          </div>
          </DropdownPanel>

          <DropdownPanel
            panelTitle="Quan điểm - Phong cách sống"
          >
            <div className="container">

              <h3 className="form__sec-title">Mong muốn kết hôn</h3>
              <div className="row">
                <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="intention_marriages">
                  <div className="l-flex-grid__item">
                    <label className="form__input--c">
                      <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  {this._renderIntentionMarriages()}
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Ngày nghỉ</h3>
              <div className="row">
                <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="holiday_types">
                  <div className="l-flex-grid__item">
                    <label className="form__input--c">
                      <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  {this._renderHolidays()}
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Uống rượu</h3>
              <div className="row">
                <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="drink_types">
                  <div className="l-flex-grid__item">
                    <label className="form__input--c">
                      <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  {this._renderDrinkings()}
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Hút thuốc</h3>
              <div className="row">
                <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="smoking_types">
                  <div className="l-flex-grid__item" ref="smoking_types">
                    <label className="form__input--c">
                      <input type="checkbox" onChange={this._uncheckAll} className="js-uncheck" /> Bất kỳ
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  {this._renderSmokingTypes(condition.smoking_types)}
                </div>
              </div>

              <hr />

            </div>
          </DropdownPanel>

          <DropdownPanel
            panelTitle="Khác"
          >
            <div className="container">
{/*              <h3 className="form__sec-title">Ngày đăng ký</h3>
              <div className="row">
                <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap" id="intention_marriages">
                  <div className="l-flex-grid__item">
                    <label className="form__input--c">
                      <input type="checkbox" id="registered_in_3_days"/> Trong 3 ngày
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                </div>
              </div>

              <hr />

              <h3 className="form__sec-title">Lần truy cập gần nhất</h3>
              <div className="row">
                <div className="col-xs-12">
                  <select ref="last_visit" className="form__input--b txt-center">
                    <option value="0">Bất kỳ</option>
                    {this._renderLastVisits(condition.last_visit)}
                  </select>
                </div>
              </div>

              <hr />*/}

              <h3 className="form__sec-title">Mức độ hoàn thành hồ sơ trên Ymeet.me</h3>
              <div className="row">
                <div className="l-flex-grid l-flex-grid--1 js-checkbox-wrap">
                  <div className="l-flex-grid__item">
                    <label className="form__input--c">
                      <input type="checkbox" id="has_self_introduction"/> Có giới thiệu
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  <div className="l-flex-grid__item">
                    <label className="form__input--c">
                      <input type="checkbox" id="has_sub_images" /> Có ảnh phụ
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                </div>
              </div>

              <hr />

{/*              <h3 className="form__sec-title">Thứ tự hiển thị kết quả tìm kiếm</h3>
              <div className="row">
                <div className="col-xs-12">
                  <select ref="search_sort" className="form__input--b txt-center">
                    <option value="0">Bất kỳ</option>
                    {this._renderSearchSort(condition.search_sort)}
                  </select>
                </div>
              </div>*/}

            </div>
          </DropdownPanel>
          <div className="fixed-bottom">
            <div className="container">
              <div className="row">
                <div className="col-xs-6">
                  {/*<Link onClick={this.resetSelectMultiple} className="btn btn--b">Thiết lập lại</Link>*/}
                  <button type="reset" onClick={this.resetSelected} className="btn btn--b">Thiết lập lại</button>
                </div>
                <div className="col-xs-6">
                  <button
                    type="submit"
                    ref="searchBtn"
                    className="btn btn--p btn--b"
                    id="fourthStep"
                  >Tìm kiếm</button>
                </div>
              </div>
            </div>
          </div>

        </form>

        <div className="well"></div>
      </div>
    )
  }
}

export default SearchConditionAdvance;
