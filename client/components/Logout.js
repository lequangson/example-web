import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { logout } from 'actions/auth'
import cookie from 'react-cookie'
import * as ymmStorage from 'utils/ymmStorage';

class Logout extends React.Component {
  componentWillMount() {
    if (ymmStorage.getItem('ymm_token')) {
      ymmStorage.removeItem('ymm_token');
      ymmStorage.clear()
      this.props.logout()
      browserHistory.push('/')
    } else {
      browserHistory.push('/')
    }
    location.reload();
  }
  render() {
    return (<div></div>)
  }
}

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    logout: () => {
      dispatch(logout())
    },
  }
}

const LogoutContainer = connect(mapStateToProps, mapDispachToProps)(Logout)

export default LogoutContainer
