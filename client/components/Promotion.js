import React, { Component, PropTypes } from 'react'
import Modal from 'react-modal'
import { Link } from 'react-router'
import { PROMOTION_IMAGE, MODAL_PROMOTION } from '../constants/Enviroment'
import * as ymmStorage from '../utils/ymmStorage';
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class Promotion extends Component {
  constructor() {
    super()
    this.state = {
      modalIsOpen: false,
    }

    this.modalOpen = this.modalOpen.bind(this)
    this.modalClose = this.modalClose.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    const modalShowNext = nextProps.Enviroment.modalShowNext
    if (modalShowNext === MODAL_PROMOTION) {
        this.modalOpen()
    }
  }

  @GoogleOptimize
  componentDidMount() {}

  modalOpen() {
    this.setState({ modalIsOpen: true })
  }

  modalClose(modalName) {
    ymmStorage.setItem('showed_remind_intro', true)
    this.props.modalShowNext(modalName)
    this.setState({ modalIsOpen: false })
  }

  render() {
    return (
      <Modal
        isOpen={this.state.modalIsOpen}
        onRequestClose={() => {
          this.modalClose('')
        }}
        className="modal__content modal__content--2"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        <Link
          to="/myprofile"
          className="modal__content__frame"
          onClick={() => {
            this.modalClose('')
          }}
        >
          <img src={PROMOTION_IMAGE} alt="Promotion" />
        </Link>
        <button
          className="modal__btn-close"
          onClick={() => {
            this.modalClose('')
          }}
        >
          <i className="fa fa-times"></i>
        </button>
      </Modal>
    )
  }
}

Promotion.propTypes = {
  modalShowNext: PropTypes.func,
}

export default Promotion
