import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

class InputEditable extends React.Component {

  static handleNothing() {
  }

  constructor(props) {
    super(props);
    this.state = {
      name: '',
    }
    this.handleTextChange = this.handleTextChange.bind(this)
  }

  componentWillMount() {
    this.setState({ name: this.props.name })
    const params = {}
    params[this.props.name] = this.props.value
    if (this.props.fromWelcomePage) {
      this.props.updateUserProfile(params)
    }
  }

  handleTextChange(e) {
    const params = {}
    params[this.state.name] = e.target.value
    this.props.updateUserProfile(params)
  }

  render() {
    const { id, value, placeholder, disabled_event, maxLength, className } = this.props
    let placeholderText = ''
    if (placeholder) {
      placeholderText = placeholder
    }
    let class_name = "txt-blue"
    if (typeof className !== 'undefined') {
      class_name = className
    }

    return (
      <input
        id={id}
        type="text"
        defaultValue={value}
        className={class_name}
        maxLength={maxLength}
        onBlur={
          !disabled_event
          ?
          this.handleTextChange
          :
          InputEditable.handleNothing
        }
        placeholder={placeholderText}
      />
    )
  }
}

InputEditable.propTypes = {
  disabled_event: PropTypes.bool,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  // id: PropTypes.number,
  name: PropTypes.string,
  updateUserProfile: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps() {
  return {}
}
const InputEditableContainer = connect(mapStateToProps, mapDispachToProps)(InputEditable);

export default InputEditableContainer
