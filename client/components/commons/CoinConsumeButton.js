import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import $ from 'jquery'
import q from 'q'
import {
  GA_ACTION_BOOST_RANK_BUY, GA_ACTION_GET_MORE_COIN, UNLOCK_WHO_LIKE_ME_PARAMS, UNLOCK_WHO_LIKE_ME_COST,
  PAYMENT_INTENTION_UNLOCK_WHO_LIKE_ME, CDN_URL
} from '../../constants/Enviroment'
import * as ymmStorage from '../../utils/ymmStorage';
import { coinConsume } from '../../actions/Payment'
import { sendGoogleAnalytic } from '../../actions/Enviroment'

class CoinConsumeButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.handleCoinConsume = this.handleCoinConsume.bind(this)
    this.handleBuyCoin = this.handleBuyCoin.bind(this),
    this.state = {
      clickedButton: false
    }
  }

  handleCoinConsume(coinConsumeCallBack) {
    if (this.state.clickedButton) {
      return;
    }
    const gaTrackData = {
      page_source: 'UNLOCK_WHO_LIKE_ME_MODAL'
    };
    const eventAction = {
      eventCategory: 'Coin',
      eventAction: 'Buy',
      eventLabel: 'Buy Unlock Who Like Me'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
    this.setState({clickedButton: true});
    this.props.coinConsume(UNLOCK_WHO_LIKE_ME_PARAMS).then(() => {
      if (coinConsumeCallBack && this.props.page) {
        this.showRewardNoti()
        coinConsumeCallBack()
      }
      this.showRewardNoti()
      // this.props.changeBannerStatus('1') //update status for change banner again
    }).catch(() => {
      this.setState({clickedButton: false});
    })
  }

  handleBuyCoin() {
    const gaTrackData = {
      page_source: 'UNLOCK_WHO_LIKE_ME_MODAL'
    };
    const eventAction = {
      eventCategory: 'Coin',
      eventAction: 'GetMoreCoin',
      eventLabel: 'Get more coin'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
    ymmStorage.setItem('Payment_Intention', PAYMENT_INTENTION_UNLOCK_WHO_LIKE_ME)
    browserHistory.push('/payment/coin')
  }

  showRewardNoti() {
    const reward_coins_url = `${CDN_URL}/general/who-like-me-unlock-success.png`
    const RewardNoti = $(`
      <div class="noti noti--bg-green mbm">
        <div class="noti__body">
          <div class="noti__header mbm">
            <img src= ${reward_coins_url} alt="coin_reward_icon+coin.png" />
          </div>
          <div class="noti__content txt-bold">Mở danh sách thành công</div>
        </div>
      </div>
    `)

    setTimeout(function() {
      $('body').append(RewardNoti)
    }, 200)

    setTimeout(function() {
      $(RewardNoti).remove()
    }, 5000)
  }

  render() {
    const coins = this.props.current_user ? this.props.current_user.coins : 0
    return (
      <div>
        {coins >= UNLOCK_WHO_LIKE_ME_COST ?
          <button onClick={() => this.handleCoinConsume(this.props.coinConsumeCallBack)} className="btn btn--p btn--b">{this.props.page === 'THANK_YOU' ? "Mở ngay!" : "Mở danh sách Ai thích tôi ngay!"}</button>
          :
          <button onClick={() => this.handleBuyCoin()} className="btn btn--p btn--b">Nạp thêm xu</button>
        }
      </div>
    )
  }
}

CoinConsumeButton.propTypes = {
  current_user: PropTypes.object,
  boostRankCallBack: PropTypes.func,
  gaSend: PropTypes.func,
  activeBoostRank: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    coinConsume: (coin_consume_type) => {
      const defer = q.defer()
      dispatch(coinConsume(coin_consume_type)).then(response => {
        defer.resolve(response)
      })
      .catch(err => {
        defer.reject(err)
      })
      return defer.promise
    },
    sendGoogleAnalytic: (eventAction, ga_track_data) =>{
      dispatch(sendGoogleAnalytic(eventAction, ga_track_data))
    },
  }
}

const CoinConsumeButtonContainer = connect(mapStateToProps, mapDispachToProps)(CoinConsumeButton);

export default CoinConsumeButtonContainer;
