import React, { PropTypes } from 'react'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'

class MediaProfile extends React.Component {

  render() {
    const { user } = this.props
    let userAvatar = ''
    if (user.user_pictures && user.user_pictures.length && user.user_pictures[0].is_main) {
      userAvatar = user.user_pictures[0].thumb_image_url
    } else {
      userAvatar = showDefaultAvatar(user.gender, 'thumb')
    }

    const nickName = user.nick_name
    const age = user.age
    const residence = user.residence ? user.residence.value : ''
    return (
      <div className="l-flex-vertical-center">
        <div className="frame frame--sm frame--1">
          <img
            src={userAvatar}
            alt={nickName}
          />
        </div>
        <div className="txt-blue">
          <div className="txt-lg txt-bold">{nickName}</div>
          <span>{age} tuổi {residence ? `- ${residence}` : ''}</span>
        </div>
      </div>
    )
  }
}

export default MediaProfile
