import React, { PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import _ from 'lodash'
import ReactTimeout from 'react-timeout'
import BannerRemindWithCoin from './BannerRemindWithCoin';
import BoostRankBanner from './BoostRankBanner'
import RandomMatchBanner from './RandomMatchBanner'
import { REMIND_FEMALE, REMIND_MALE } from '../../constants/TextDefinition'
import * as ymmStorage from '../../utils/ymmStorage';
import { MAP_NEARBY_ACTIVE, SMILE_FACE, RANDOM_MATCH, BOOST_RANK, REMIND_BANNER, REMIND_BANNER_WITH_COIN } from '../../constants/Enviroment'
import { convertUTCToLocalTime } from '../../utils/common'
import { isUserInBoostInRankPeriod } from '../../utils/BoostInRankService'

class CoinBanner extends React.Component {
  static elementInfiniteLoad() {
    return (
      <div className="col-xs-12 txt-center loader is-load">
        <i className="fa fa-spinner fa-3x" />
      </div>
    )
  }

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  renderBoostRankBanner() {
    const current_user = this.props.user_profile.current_user;
    let needShow = current_user.user_type !== 4 && !isUserInBoostInRankPeriod(current_user);
    if (!needShow) {
      return ''
    }
    return (
      <div>
      <BoostRankBanner
        current_user={this.props.user_profile.current_user}
        gaSend={this.props.gaSend}
        changeBannerStatus={this.props.changeBannerStatus}
      />
      </div>
    )
  }

  renderRandomMatchBanner() {
    return(
      <div className="col-xs-12">
        <RandomMatchBanner
          current_user={this.props.user_profile.current_user}
          gaSend={this.props.gaSend}
        />
      </div>

    )
  }

  renderBannerRemindWithCoin() {
    return(
      <div className="container">
        <BannerRemindWithCoin
          sendGoogleAnalytic={this.props.sendGoogleAnalytic}
          changeBannerStatus={this.props.changeBannerStatus}
        />
      </div>
    )
  }

  render() {
    const banner = this.props.banner
      if (banner === 'boot_rank') {
        return (
          <div>
            {this.renderBoostRankBanner()}
          </div>
        )
      }
      else if (banner === 'random_match') {
        return (
          <div>
            {this.renderRandomMatchBanner()}
          </div>
        )
      }
      else if (banner === 'remind_banner_with_coin') {
        return (
          <div>
            {this.renderBannerRemindWithCoin()}
          </div>
        )
      }
  }
}

CoinBanner.propTypes = {
}

export default ReactTimeout(CoinBanner)