import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { gaSend } from '../../actions/Enviroment'
import { BANNER_ICON_URL, GA_ACTION_OPEN_TIPS, GA_ACTION_OPEN_NEXTLINK_TIPS } from '../../constants/Enviroment'

function showIcon(icon) {
  return `${BANNER_ICON_URL}icon_${icon}.png`
}

class Banner extends React.Component {
  constructor(props) {
    super(props)
    this.sendGaEvent = this.sendGaEvent.bind(this)
  }

  sendGaEvent() {
    const { heading, description } = this.props
    const message = `${heading} ${description}`
    const div = document.createElement('div');
    div.innerHTML = message;
    const ga_message = div.textContent || div.innerText || ''
    const ga_track_data = {
      message: ga_message,
    }
    this.props.gaSend(GA_ACTION_OPEN_TIPS, ga_track_data)
  }

  render() {
    const props = this.props
    return (
      <Link to={props.url} onClick={this.sendGaEvent} className="banner mbm">
        <img src={showIcon(props.iconNumber)} className="banner__icon" role="presentation" />
        <div className="banner__content">
          <h3 className="banner__heading">{props.heading}</h3>
          <p className="banner__sub-heading" dangerouslySetInnerHTML={{ __html: props.description }}></p>
        </div>
        <div className="banner__meta">
          <i className="fa fa-chevron-right"></i>
        </div>
      </Link>
    )
  }
}

Banner.propTypes = {
  url: PropTypes.string.isRequired,
  iconNumber: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  gaSend: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    gaSend: (eventAction, ga_track_data) => {
      dispatch(gaSend(eventAction, ga_track_data))
    },
  }
}

const BannerContainer = connect(mapStateToProps, mapDispachToProps)(Banner);

export default BannerContainer
