import React, { PropTypes, Component } from 'react';
import {
  CDN_URL, ANDROID_URL, IOS_URL
} from 'constants/Enviroment';
import IosDownloadButton from 'components/commons/Buttons/IosDownloadButton'
import AndroidDownloadButton from 'components/commons/Buttons/AndroidDownloadButton'
import * as UserDevice from 'utils/UserDevice';

class BannerSwapSite extends Component {
  state = {  }

  renderBr() {
    const br = (<br />)
    const width = window.innerWidth
    if (width > 1200){
      return br
    }
    return
  }

  render() {

    const defaultStyleBanner = {
      backgroundImage: `url(${CDN_URL}/general/Banner/Phones.png)`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: '90% bottom',
      overflow: 'hidden',
      justifyContent: 'flex-start'}

    return (
      <div className="row">
        <div className="col-xs-12">
          <div
            style={UserDevice.getName() !== 'pc'? {backgroundSize: '33%', ...defaultStyleBanner} : defaultStyleBanner}
            className="banner pos-relative mbm"
            to="/coin-instruction#refund"
          >
            <div>
              <div className="card__addon-2">
                <div className="card__addon-2__title rotate-0 margin-l5 mbt txt-xlg">!</div>
              </div>
              <div className="card__pad-2 small card__pad-2--red"></div>
            </div>
            <div className="banner__content banner__content--2 txt-blue margin-l20 txt-left">
              <h3 className="banner__heading mbm">
                Từ ngày <span className="txt-red">1/7/2018</span>, phiên bản web m.ymeet.me sẽ ngưng{this.renderBr()}
                hoạt động. Hãy tải ứng dụng để đồng{this.renderBr()}
                hành cùng YmeetMe bạn nhé!
              </h3>
              <div className="l-flex--jc l-flex--ac l-flex row col-xs-6 col-sm-7 padding-0">
                {(UserDevice.getName() === 'pc' || UserDevice.getName() === 'ios') &&
                  <IosDownloadButton redirect_url={IOS_URL} />
                }
                {(UserDevice.getName() === 'pc' || UserDevice.getName() === 'android') &&
                  <AndroidDownloadButton redirect_url={ANDROID_URL}/>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default BannerSwapSite;