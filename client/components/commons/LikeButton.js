import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import q from 'q';
import _ from 'lodash';
import $ from 'jquery';
import { Experiment, Variant } from "react-ab";
import { updateReducerSearchState } from 'actions/search';
import { updatePhotoStreamState } from 'actions/photoStream';
import { BLOCK_TYPE_BLOCKED, ACTIVE_USER } from 'constants/userProfile';
import { gaSend, updatePushNotiModal } from 'actions/Enviroment';
import {
  GA_ACTION_LIKE, GA_ACTION_MATCH, GA_ACTION_LIKE_WITH_MESSAGE, SEARCH_PAGE, RECOMMENDATION_PAGE,
  NOTIFICATION_NOT_ENOUGH_LIKE, NOTIFICATION_ERROR, LIKE_WITH_MESSAGE_ICON , LIKE_STYLE_ICON, LIKE_STYLE_ICON_PINK, LIKE_STYLE_ICON_GREY,
  NOTIFICATION_SUCCESS, WHAT_HOT_PAGE, PEOPLE_LIKED_ME_PAGE, OTHER_PROFILE_PAGE, PEOPLE_I_LIKED_PAGE,
  PHOTO_STREAM, PUSH_NOTI_MODAL_LIKE, CLICK_NOFI_LIKE, CLICK_NOTI_CATEGORY
} from 'constants/Enviroment';
import {
  getPeopleMatchedMe, getPeopleMatchedMeFailure,
  sendLikeRequest, sendLike,
  updateLikeStyle
} from 'actions/userProfile';
import { showNextPeopleLikedMe } from 'pages/people-liked-me/Actions';
import { getPeopleILiked } from 'actions/PeopleILiked';
import {
  LIKE_BUTTON_A, ALREADY_LIKE_BUTTON_A,
  LIKE_BUTTON_B, ALREADY_LIKE_BUTTON_B,
  LIKE_BACK_BUTTON, WELCOME_MATCH_MADE_MESSAGE,
  ERROR_LABEL_NOT_ENOUGH_LIKE,
  ERROR_MSG_NOT_ENOUGH_LIKE,
  DEFAULT_NOTI_LABEL
} from 'constants/TextDefinition';
import { setLikeInfoRequest, setLikeInfo, updateMatchModal, updateFirstLikeModal, updateSimilarUserModal } from 'actions/Like';
import { isInLikeMeRealTimeList, compareIdentifiers } from 'utils/common';
import { sendClickableNotification } from 'utils/errors/notification';
import { showNextUser } from 'actions/WhatHot';
import { shuffleMessage } from 'pages/dictionaries/Action'
import { MATCHED_TYPE_SUPER } from 'constants/Like';
import socket from "utils/socket";

class LikeButton extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      identifier: '',
      isSendingLike: false,
      variant: '',
    }
    this.choice = this.choice.bind(this)
  }

  componentWillMount() {
    this.setState({ identifier: this.props.user.identifier })
  }

  // handle like real time to change button name and like status
  // normally, when other user press like current user, we will storage user like from (identifier)
  // into people_liked_me_real_time when socket call
  componentWillReceiveProps() {
    const { user, user_profile } = this.props
    const { have_sent_like_to_me, have_sent_like } = user
    const { people_liked_me_real_time } = user_profile
    if (
      !_.isEmpty(people_liked_me_real_time)
      && !have_sent_like_to_me
      && isInLikeMeRealTimeList(user.identifier, people_liked_me_real_time)
    ) {
      user.have_sent_like_to_me = true
      if (have_sent_like) {
        // reload match list to handler chat button
        this.props.getPeopleMatchedMe();
      }
    }
  }

  getPageSource() {
    if (this.props.page_source && this.props.page_source !== '') {
      return this.props.page_source;
    }
    if (this.props.Enviroment.previous_page === WHAT_HOT_PAGE &&
      this.props.WhatHot.users.length
    ) {
      const user = this.props.WhatHot.users[this.props.WhatHot.selectedUserIndex];
      if (user && user.is_recommendation) {
        return RECOMMENDATION_PAGE;
      }
      else {
        return WHAT_HOT_PAGE;
      }
    }
    if (this.props.Enviroment.view_profile_source !== '') {
      return this.props.Enviroment.view_profile_source;
    }
    return this.props.Enviroment.previous_page;
  }

  getButtonName() {
    if (this.props.title) {
      return this.props.title
    }
    let buttonName = (this.props.user.have_sent_like)? ALREADY_LIKE_BUTTON_A : LIKE_BUTTON_A

    return buttonName;
  }

  choice(experiment, variant) {
    // console.log(variant)
    this.props.updateLikeStyle(variant)
    this.setState({ variant })
    }

  getSendLikeMessage() {
    if (this.isMatchMade()) {
      return WELCOME_MATCH_MADE_MESSAGE;
    }
    return '';
  }

  isMatchMade() {
    if ((this.props.user.have_sent_like && this.props.user.have_sent_like_to_me) || this.props.user.is_random_matched) {
      return true;
    }

    return false;
  }

  isDisable() {
//debugger;
    if (this.props.isSendingLike || this.state.isSendingLike) {
      return true
    }
    if (this.props.user.have_sent_like && this.props.user.have_sent_like_to_me) {
      return false
    }
    if (parseInt(this.props.user.block_type, 10) === BLOCK_TYPE_BLOCKED) {
      return true
    }
    return this.props.user.have_sent_like;
  }

  sendingLike(isSendingLike) {
    this.setState({ isSendingLike })
  }

  markSendingLike(isSending) {
    if ({}.hasOwnProperty.call(this.props, 'markSendingLike')) {
     this.props.markSendingLike(isSending)
    } else if (this.props.user && this.props.user.have_sent_like_to_me && this.props.Like.isLikeModalOpen) {
     this.sendingLike(isSending)
    }
  }

  handleLikeClick(like_info, isOpenModal = false, likeFrom = {}, likeTo = {}, msg = '') {
    const { user_profile } = this.props;
    const {current_user } = user_profile;
    const { matched_users} = user_profile;

    if (!_.isEmpty(matched_users)) {
      if (matched_users.some(item => compareIdentifiers(item.identifier, like_info.like_to) && item.is_random_matched == false)) {
        const msgLikeSuccess = 'Hai bạn đã ghép đôi thành công, vào Trò chuyện thôi!';
        const url = `${process.env.CHAT_SERVER_URL}chat/${like_info.like_to}`;
        const gaTrackDataClickNoti = {
          page_source:CLICK_NOFI_LIKE
        }
        const eventActionClickNoti = {
          eventCategory: CLICK_NOTI_CATEGORY,
          eventAction: 'Click',
          eventLabel: CLICK_NOTI_CATEGORY
        }
        const callbackClickNoti = () => this.props.sendGoogleAnalytic(eventActionClickNoti, gaTrackDataClickNoti);

        sendClickableNotification(NOTIFICATION_SUCCESS, DEFAULT_NOTI_LABEL, msgLikeSuccess, url, 3000, true, callbackClickNoti);
        likeTo.match_type = MATCHED_TYPE_SUPER;
        return;
      }
    }
    const page_source = this.getPageSource();

    if (isOpenModal) {
      const isLikeModalOpen = true
      const isLikeMessageModalOpen = false
      this.props.setLikeInfo(likeFrom, likeTo, isLikeModalOpen, isLikeMessageModalOpen, page_source)
      this.props.shuffleMessage(current_user.gender, current_user.age);
      return
    }
    this.markSendingLike(true)
    const likeMessage = msg ? msg : ''
    // Send google analytics
    let gaAction = ''
    if (this.props.user.have_sent_like_to_me) {
      gaAction = GA_ACTION_MATCH
    } else {
      gaAction = msg ? GA_ACTION_LIKE_WITH_MESSAGE : GA_ACTION_LIKE;
    }
    const ga_track_data = {
      partner_residence: this.props.user.residence ? this.props.user.residence.value : '',
      partner_birth_place: this.props.user.birth_place ? this.props.user.birth_place.value : '',
      page_source: page_source,
      partner_age: this.props.user.age,
      message: likeMessage,
      extra_information: this.state.variant,
    };

    this.props.gaSend(gaAction, ga_track_data);
    // Send like and update user status
    const that = this
    const likeSuccessNotification = this.getSendLikeMessage()
    this.props.sendLike(like_info, likeMessage, page_source, likeSuccessNotification, current_user.user_status).then((likeResponse) => {
      this.props.user.have_sent_like = true
      this.props.getPeopleMatchedMe().then(() => {
        const people_i_liked = this.props.PeopleILiked.data
        const people_match_me = this.props.user_profile.matched_users
        // if (people_i_liked.length === 0 && people_match_me.length === 0 && !this.props.user.have_sent_like_to_me) {
        //   if (typeof Notification != 'undefined' && Notification.permission !== 'granted') {
        //     this.props.updatePushNotiModal(true, PUSH_NOTI_MODAL_LIKE, this.props.user.nick_name)
        //   }
        // }
      })
      // this.props.getPeopleILiked()
      // If user like other user in other list which is not search page,
      // we need to update like status in search state if any
        if (this.props.Enviroment.previous_page === PHOTO_STREAM) {
          this.props.updatePhotoStreamState(like_info.like_to, 'have_sent_like', true)
        }
        else {
          this.props.updateReducerState(like_info.like_to, 'have_sent_like', true)
        }
      // in some case when user like, or like back, the button does not re-render,
      // so, we set a state with current time to force button re-rende to display correctly button name
      const date = new Date();
      that.setState({ time: date.getTime() })
      this.markSendingLike(false)
      this.closeLikeModal()
      // Show Match popup
      if (this.props.user.have_sent_like_to_me) {
        this.props.updateMatchModal(true, like_info)
      }
      // Show First Like modal
      else if (!likeFrom.current_user.has_first_like && likeFrom.current_user.has_first_like != 'undefined'
        && (page_source.substring(0,6) === "SEARCH" || page_source === WHAT_HOT_PAGE)
      ) {
        const isOpenFirstLikeModal = true;
        const firstLikeSent = true;
        const firstLikeReceived = false;
        const firstLikeInfo = likeTo;
        this.props.updateFirstLikeModal(isOpenFirstLikeModal, firstLikeSent, firstLikeReceived, firstLikeInfo);
      }
      // Try to show similar user.
      else {
        const isOpenFirstLikeModal = true;
        if ((page_source !== WHAT_HOT_PAGE && page_source !== PHOTO_STREAM &&
          this.props.Enviroment.previous_page !== WHAT_HOT_PAGE) &&
          this.props.user.have_sent_like_to_me === false &&
          typeof likeResponse.response.data.identifier !== 'undefined') {
          this.props.updateSimilarUserModal(isOpenFirstLikeModal, likeResponse.response.data, likeFrom, likeTo);
        } else {
          this.showNotiSendLikeSuccess()
        }
      }
      if (page_source === WHAT_HOT_PAGE ||
        page_source === RECOMMENDATION_PAGE ||
        (page_source === OTHER_PROFILE_PAGE && this.props.Enviroment.previous_page === WHAT_HOT_PAGE)) {
        const isLikeFromWhatHot = page_source !== RECOMMENDATION_PAGE;
        this.props.showNextWhatHotUser(isLikeFromWhatHot, current_user.gender, current_user.user_type);
      }
      if (page_source === PEOPLE_LIKED_ME_PAGE ||
        this.props.Enviroment.previous_page === PEOPLE_LIKED_ME_PAGE) {
        this.props.showNextPeopleLikedMe();
      }
    }).catch((e) => {
      console.log('e', e);
      // send like failure
      that.props.user.have_sent_like = false
      const date = new Date();
      that.setState({ time: date.getTime() })// force rendering component after apdate state
      that.markSendingLike(false)
    })
  }

  showNotiSendLikeSuccess() {
    const sendLikeSuccess = $(`<div class="noti noti--bg">
      <div class="noti__body">
        <div class="noti__icon"><i class="fa fa-thumbs-up"></i></div>
        <div class="noti__content">Bạn đã gửi thích thành công</div>
      </div>
    </div>`)

    setTimeout(function() {
      $('body').append(sendLikeSuccess)
    }, 200)

    setTimeout(function() {
      $(sendLikeSuccess).remove()
    }, 1500)
  }

  closeLikeModal() {
    // close the like Modal if user click Like button on the popup modal
    const isLikeModalOpen = false
    const isLikeMessageModalOpen = false
    const likeFrom = {}
    const likeTo = {}
    const page_source = this.getPageSource()
    this.props.setLikeInfo(likeFrom, likeTo, isLikeModalOpen, isLikeMessageModalOpen, page_source)
  }

  cardLike() {
    var $card = $(".site-content").find('.card.front');

    let swipe = new TimelineMax({repeat:0, yoyo:false, repeatDelay:0, onComplete:this.remove(), onCompleteParams:[$card]});
    swipe.staggerTo($card, 0.8, {bezier:[{left:"+=400", top:"+=300", rotation:"60"}], ease:Power1.easeInOut});
  }

  remove(card) {
    $(card).remove();
  }

  renderLikeButtonVariant(likeIcon, className) {
    const { user, user_profile, forceLike, msg, isUserActive, useDefaultStyle } = this.props
    const { current_user } = user_profile
    const { nick_name, gender, user_pictures } = current_user
    // default like button is missing forceLike atribute which will show  a popup for like with msg
    // otherwise send like immediately
    // const isOpenLikeModal = !Boolean(forceLike) && !user.have_sent_like_to_me
    const isOpenLikeModal = !Boolean(forceLike)
    let likeClass = useDefaultStyle === true ? "btn txt-center btn--b btn--5" : className;
    // if (this.props.page_source == WHAT_HOT_PAGE && !this.props.inLikeModal) {
      // likeClass = 'btn--round btn btn--p'
    // }
    const likeIconUrl = useDefaultStyle === true ? LIKE_STYLE_ICON_GREY : likeIcon;
    const page_source = this.getPageSource();
    const likeIconClassName = 'img-like'

    return (
      <button
        className={likeClass}
        id='LikeButton'
        onClick={
          () => {
            if(page_source === PEOPLE_I_LIKED_PAGE ||
              page_source === WHAT_HOT_PAGE ||
              page_source === RECOMMENDATION_PAGE ) {
              this.cardLike()
              setTimeout(() => {
                this.handleLikeClick(
                  {
                    room: user.identifier,
                    like_from: current_user.identifier,
                    like_to: user.identifier,
                    nick_name,
                    have_sent_like_to_me: user.have_sent_like_to_me,
                    message: msg,
                    detail_user_from: current_user
                  }, isOpenLikeModal, user_profile, user, msg
                )
              }, 800)
            }else {
              this.handleLikeClick(
                {
                  room: user.identifier,
                  like_from: current_user.identifier,
                  like_to: user.identifier,
                  nick_name,
                  have_sent_like_to_me: user.have_sent_like_to_me,
                  message: msg,
                  detail_user_from: current_user
                }, isOpenLikeModal, user_profile, user, msg
              )
            }
          }
        }
        disabled={typeof isUserActive != 'undefined' ? !isUserActive : this.isDisable()}
      >
        <img className={likeIconClassName} src={!_.isEmpty(msg) ? LIKE_WITH_MESSAGE_ICON : likeIconUrl } alt="tym-icon" />
        {this.getButtonName()}
      </button>
    )
  }

  renderButton() {
    return (
      <div>
        {this.renderLikeButtonVariant(LIKE_STYLE_ICON, 'btn txt-center btn--p btn--b')}
      </div>
    )
    /*return (
      <div>
        <Experiment onChoice={this.choice} name="LikeButton">
          <Variant name="Blue">
            {this.renderLikeButtonVariant(LIKE_STYLE_ICON, 'btn txt-center btn--p btn--b')}
          </Variant>
          <Variant name="Pink">
            {this.renderLikeButtonVariant(LIKE_STYLE_ICON_PINK, 'btn txt-center btn--4 btn--b')}
          </Variant>
        </Experiment>
      </div>
    )*/
  }

  render() {
    const { user, user_profile } = this.props;
    const { matched_users } = user_profile;
    const is_matched_user = matched_users.some(item => compareIdentifiers(item.identifier, user.identifier));
    if (user.match_type == MATCHED_TYPE_SUPER || is_matched_user) {
      return <div></div>
    }
    return (
      !this.isMatchMade() ? this.renderButton() : <div></div>
    )
  }
}

LikeButton.propTypes = {
  user: PropTypes.object,
  page_source: PropTypes.string,
  user_profile: PropTypes.object,
  Enviroment: PropTypes.object,
  gaSend: PropTypes.func,
  sendLike: PropTypes.func,
  msg: PropTypes.string,
  title: PropTypes.string,
  updateReducerState: PropTypes.func,
  getPeopleMatchedMe: PropTypes.func,
  setLikeInfo: PropTypes.func,
  markSendingLike: PropTypes.func,
  isSendingLike: PropTypes.bool,
  updateMatchModal: PropTypes.func,
  useDefaultStyle: PropTypes.bool,
  showNextWhatHotUser: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    sendLike: (like_info, likeMessage, pageSource, likeSuccessNotification, user_status) => {
      const defer = q.defer()
      dispatch(sendLikeRequest())
      dispatch(sendLike(like_info.like_to, likeMessage, pageSource, likeSuccessNotification)).then((response) => {
        if (typeof response !== 'undefined') {
          defer.resolve(response)
          if (user_status == ACTIVE_USER) {
            socket.emit('like_user', like_info);
          }
        }
      }).catch((err) => {
        defer.reject(err)
      });
      return defer.promise
    },
    gaSend: (eventAction, ga_track_data) => {
      dispatch(gaSend(eventAction, ga_track_data))
    },
    setLikeInfo: (likeFrom, likeTo, isLikeModalOpen = false, isLikeMessageModalOpen = false, page_source) => {
      dispatch(setLikeInfoRequest())
      dispatch(setLikeInfo(likeFrom, likeTo, isLikeModalOpen, isLikeMessageModalOpen, page_source))
    },
    getPeopleMatchedMe: () => {
      const defer = q.defer()
      dispatch(getPeopleMatchedMe()).then(() => {
        dispatch(getPeopleILiked()).then((response) => {
          defer.resolve(response)
        })
      }).catch(() => {
        dispatch(getPeopleMatchedMeFailure())
      })
      return defer.promise
    },
    updateReducerState: (identifier, key, value) => {
      dispatch(updateReducerSearchState(identifier, key, value))
    },
    updatePhotoStreamState: (identifier, key, value) => {
      dispatch(updatePhotoStreamState(identifier, key, value))
    },
    updateMatchModal: (isMatchModalOpen = false, matchInfo = {}) => {
      dispatch(updateMatchModal(isMatchModalOpen, matchInfo))
    },
    updateLikeStyle: (style) => {
      dispatch(updateLikeStyle(style))
    },
    updateFirstLikeModal: (isOpen = false, firstLikeSent = false, firstLikeReceived = false, firstLikeInfo = {}) => {
      dispatch(updateFirstLikeModal(isOpen, firstLikeSent, firstLikeReceived, firstLikeInfo));
    },
    updateSimilarUserModal: (isOpen = false, similarUserInfo = {}, likeFrom = {}, likeTo = {}) => {
      dispatch(updateSimilarUserModal(isOpen, similarUserInfo, likeFrom, likeTo));
    },
    updatePushNotiModal: (isOpen, modalType, nick_name) => {
      dispatch(updatePushNotiModal(isOpen, modalType, nick_name))
    },
    showNextWhatHotUser: (isLikeAction, userGender, userType) => {
      dispatch(showNextUser(isLikeAction, userGender, userType))
    },
    shuffleMessage: (gender, age) => {
      dispatch(shuffleMessage(gender, age))
    },
    showNextPeopleLikedMe: () => {
      dispatch(showNextPeopleLikedMe())
    },
  }
}
const LikeContainer = connect(mapStateToProps, mapDispachToProps)(LikeButton);

export default LikeContainer;
