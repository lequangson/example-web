import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import ReactTimeout from 'react-timeout'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import { GA_ACTION_OPEN_REMIND, REMIND_BANNER } from '../../constants/Enviroment'
import { changeBannerStatus, showNextRemindUser } from '../../actions/Enviroment'
import { isEmpty } from 'lodash';


class BannerRemind extends React.Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
    this.props.setInterval(this.props.showNextRemindUser, 5000)
  }

  componentWillMount() {
  }

  handleClick(identifier) {
    this.props.gaSend(GA_ACTION_OPEN_REMIND, {})
    this.props.updatePreviousPage(REMIND_BANNER)
    this.props.showNextRemindUser()
    this.props.updateRemind(identifier)
  }

  render() {
    const { data } = this.props
    const remindIndex = this.props.Enviroment.remind.remindIndex
    const remind = data[remindIndex]
    if (isEmpty(remind)) {
      return <div></div>
    }
    let userAvatar = ''
    if (remind.user_pictures && remind.user_pictures.length) {
      userAvatar = remind.user_pictures[0].thumb_image_url
    } else {
      userAvatar = showDefaultAvatar(remind.gender, 'thumb')
    }
    const props = this.props
    return (
      <Link to={`/profile/${remind.identifier}`} onClick={() => {this.handleClick(remind.identifier)}} className="banner mbm">
        <div className="frame frame--xsm frame--1">
          <img src={userAvatar} alt="" />
        </div>
        <div className="banner__content">
          <h3 className="banner__heading">{remind.nick_name} thích bạn!</h3>
          <p className="banner__sub-heading">{props.remindText}</p>
        </div>
        <div className="banner__meta">
          <i className="fa fa-chevron-right"></i>
        </div>
      </Link>
    )
  }

}

BannerRemind.propTypes = {
  updatePreviousPage: PropTypes.func.isRequired,
  gaSend: PropTypes.func,
  updateRemind: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    showNextRemindUser: () => {
      dispatch(showNextRemindUser())
    },
    changeBannerStatus: (banner) => {
      dispatch(changeBannerStatus(banner))
    },
  }
}

const BannerRemindContainer = connect(mapStateToProps, mapDispatchToProps)(BannerRemind);

export default ReactTimeout(BannerRemindContainer)
