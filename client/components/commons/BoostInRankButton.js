import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import $ from 'jquery'
import q from 'q'
import {
  GA_ACTION_BOOST_RANK_BUY, GA_ACTION_GET_MORE_COIN, BOOST_RANK_COST, BOOST_RANK_SUCCESS,
  PAYMENT_INTENTION_BOOST_RANK,
} from '../../constants/Enviroment'
import * as ymmStorage from '../../utils/ymmStorage';
import { activeBoostRank } from '../../actions/userProfile'

class BoostInRankButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.handleBoostRank = this.handleBoostRank.bind(this)
    this.handleBuyCoin = this.handleBuyCoin.bind(this),
    this.state = {
      clickedBoostRank: false
    }
  }

  handleBoostRank(boostRankCallBack) {
    if (this.state.clickedBoostRank) {
      return;
    }
    this.setState({clickedBoostRank: true});
    this.props.gaSend(GA_ACTION_BOOST_RANK_BUY, {})
    this.props.activeBoostRank().then(() => {
      if (boostRankCallBack && this.props.page) {
        this.showNotiBoostSuccess()
        boostRankCallBack()
      }
      this.showNotiBoostSuccess()
    }).catch(() => {
      this.setState({clickedBoostRank: false});
    })
  }

  handleBuyCoin() {
    this.props.gaSend(GA_ACTION_GET_MORE_COIN, { page_source: 'BOOST_RANK_MODAL' })
    ymmStorage.setItem('Payment_Intention', PAYMENT_INTENTION_BOOST_RANK)
    browserHistory.push('/payment/coin')
  }

  showNotiBoostSuccess() {
    const boostSuccess = $(`<div class="noti">
      <img className="img-firstlike" src=${BOOST_RANK_SUCCESS} alt="" />
    </div>`)

    setTimeout(function() {
      $('body').append(boostSuccess)
    }, 200)

    setTimeout(function() {
      $(boostSuccess).remove()
    }, 4500)
  }

  render() {
    const coins = this.props.current_user ? this.props.current_user.coins : 0
    const forFree = this.props.current_user.permission != null && this.props.current_user.permission.send_boost_in_rank_free;
    const isPaidUser = this.props.current_user.user_type === 4;
    const { boost_in_rank_free_gift } = this.props.user_profile.lucky_spinner_gifts;
    return (
      <div>
        {(forFree || coins >= BOOST_RANK_COST || isPaidUser || boost_in_rank_free_gift > 0) ?
          <button onClick={() => this.handleBoostRank(this.props.boostRankCallBack)} className={this.props.cssClassName != null? this.props.cssClassName : 'btn btn--p btn--b'}>Nổi bật ngay</button>
          :
          <button onClick={() => this.handleBuyCoin()} className={this.props.cssClassName != null? this.props.cssClassName : 'btn btn--p btn--b'}>Nạp thêm xu</button>
        }
      </div>
    )
  }
}

BoostInRankButton.propTypes = {
  current_user: PropTypes.object,
  boostRankCallBack: PropTypes.func,
  gaSend: PropTypes.func,
  activeBoostRank: PropTypes.func,
  cssClassName: PropTypes.string,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    activeBoostRank: () => {
      const defer = q.defer()
      dispatch(activeBoostRank()).then(response => {
        defer.resolve(response)
      })
      .catch(err => {
        defer.reject(err)
      })
      return defer.promise
    }
  }
}

const BoostInRankContainer = connect(mapStateToProps, mapDispachToProps)(BoostInRankButton);

export default BoostInRankContainer;
