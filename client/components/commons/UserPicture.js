import React, { PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import showOnlineStatus from '../../src/scripts/showStatus'
import { BLOCK_TYPE_BLOCKED } from '../../constants/userProfile'
import StartChatButton from './StartChatButton'

class UserPicture extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      showChatButton: false,
    }
    this.handleClick = this.handleClick.bind(this)
  }

  componentWillMount() {
    if (this.props.haveChatButton) {
      setTimeout(() => {this.setState({showChatButton: true})}, 1500)
    }
  }

  renderNewUserIcon(){
    const { user } = this.props
    const isNewUser = user ? user.is_new_user : false;
    if (!isNewUser){
      return ''
    }
    return (
      <div>
        <div className="card__addon-2">
          <div className="card__addon-2__title">Mới!</div>
        </div>
        <div className="card__pad-2"></div>
      </div>
    )
  }

  handleClick(user) {
    if (this.props.goToProfileCallback) {
      this.props.goToProfileCallback()
    }
    browserHistory.push(`/profile/${user.identifier}`)
  }

  addClassButton() {
    const elt = document.querySelectorAll('article .card__center');
    // console.log('elt', elt)
    for (let value of elt) {
      value.classList.add('card__center--1')
    }
  }

  render() {
    const { user, go_to_profile, show_similar_picture } = this.props
    let userPicture = ''
    if (show_similar_picture) {
      userPicture = user.user_pictures.find((picture) => picture.is_most_similar).large_image_url;
    } else if (typeof user.user_pictures !== 'undefined') {
      userPicture = user.user_pictures.length ?
                user.user_pictures['0'].large_image_url : showDefaultAvatar(user.gender, 'large')
    } else {
      userPicture = typeof user.picture ? user.picture : showDefaultAvatar(user.gender, 'large')
    }
    const userStatus = user.user_status
    const isBlockedUser = user.block_me_type
      ? user.block_me_type === BLOCK_TYPE_BLOCKED
      : false;
    const isUserActive = ((userStatus === 1) || (userStatus === null)) && !isBlockedUser

    let jobDescription = user.job_description ? user.job_description.trim() : ''

    if (jobDescription.length > 17) {
      jobDescription = `${jobDescription.substring(0, 17)}...`
    }
    return (
      <div className="card__upper">
      {go_to_profile ?
        <div onClick={() => {this.handleClick(user)}} className="card__link">
          <img
            src={userPicture}
            className="card__img"
            alt={`${user.nick_name}`}
          />
          <div className="card__addon">
            <i className="fa fa-heart"></i>
            <div className="txt-bold">{user.compatibility}%</div>
          </div>
          <div className="card__pad"></div>
          {this.renderNewUserIcon()}
          <div className="card__meta card__meta--1">
            <div>
              <i className={`dot dot--${showOnlineStatus(user.online_status)} mrs`}></i>
              <span className="txt-bold txt-lg">{user.nick_name} - </span>
              <span className="txt-bold txt-lg">{user.age ? user.age.toString() : '?'}</span>
            </div>
            <div className="txt-color-primary">
              {
                (
                  typeof user.residence !== 'undefined' &&
                  typeof user.residence.value !== 'undefined' &&
                  user.residence.value
                ) ? user.residence.value : 'Không rõ'
              }
            </div>
          </div>
        </div>
       :
       <div href="#" className="card__link">
         <img
           src={userPicture}
           className="card__img"
           alt={`${user.nick_name}`}
         />
         <div className="card__addon">
           <i className="fa fa-heart"></i>
           <div className="txt-bold">{user.compatibility}%</div>
         </div>
         <div className="card__pad"></div>
         {this.renderNewUserIcon()}
         <div className="card__meta card__meta--1">
           <div>
             <i className={`dot dot--${showOnlineStatus(user.online_status)} mrs`}></i>
             <span className="txt-bold txt-lg">{user.nick_name} - </span>
             <span className="txt-bold txt-lg">{user.age ? user.age.toString() : '?'}</span>
           </div>
           <div className="txt-color-primary">
             {
               (
                 typeof user.residence !== 'undefined' &&
                 typeof user.residence.value !== 'undefined' &&
                 user.residence.value
               ) ? user.residence.value : 'Không rõ'
             }
           </div>

          {typeof user.like_message !== 'undefined' && user.like_message && <div className="card__text-2">
            <i className="fa fa-envelope mrs"></i>
            {user.like_message}
          </div>}

         </div>
         {
          (this.props.haveChatButton && this.state.showChatButton) &&
          <div className="card__center card__center--1">
            <StartChatButton user={user} noBlock={true} page_source='' />
          </div>
         }
       </div>
      }
      </div>
    )
  }
}

UserPicture.propTypes = {
}

export default UserPicture
