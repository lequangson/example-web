import React, { PropTypes } from 'react'
import { browserHistory, Link } from 'react-router';
import { connect } from 'react-redux'
import { REMIND_BANNER_WITH_COIN, BANNER_REMIND_WITH_COIN_IMAGE, BANNER_ICON_URL } from '../../constants/Enviroment'
import { coinConsume } from '../../actions/Payment';
import { isEmpty } from 'lodash';
import Modal from 'components/modals/Modal'
import q from 'q';
import * as sendBannerNotification from '../../utils/notifications/banner';
import { getCurrentUser } from '../../actions/userProfile';
import * as ymmStorage from '../../utils/ymmStorage';

import socket from "../../utils/socket";
import { ACTIVE_USER } from 'constants/userProfile';

class BannerRemindWithCoin extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpenConfirmModal: false,
      accept_remind_with_coin: false
    }
    this.openConfirmModal = this.openConfirmModal.bind(this)
    this.handleRemindClick = this.handleRemindClick.bind(this)
    this.closeModal = this.closeModal.bind(this);
    this.addMoreCoinClick = this.addMoreCoinClick.bind(this);
    this.rememberPaymentIntention = this.rememberPaymentIntention.bind(this);
  }

  openConfirmModal() {
    const gaTrackData = {
      page_source: 'REMIND_BANNER_WITH_COIN'
    };
    const eventAction = {
      eventCategory: 'REMIND BANNER WITH COIN',
      eventAction: 'Click',
      eventLabel: 'Click banner'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
    this.setState({isOpenConfirmModal: true})
  }

  closeModal() {
    this.setState({isOpenConfirmModal: false})
  }

  handleRemindClick() {
    if (this.state.accept_remind_with_coin) {
      return;
    }
    const people_i_liked = this.props.PeopleILiked.data
    this.setState({accept_remind_with_coin: true})
    this.props.acceptRemindWithCoin().then(res => {
      sendBannerNotification.remindBannerWithCoinSuccess()
      const gaTrackData = {
        page_source: 'REMIND_BANNER_WITH_COIN'
      };
      const eventAction = {
        eventCategory: 'Coin',
        eventAction: 'Buy',
        eventLabel: 'Buy remind banner with coin'
      }
      this.props.sendGoogleAnalytic(eventAction, gaTrackData)
      this.setState({accept_remind_with_coin: false, isOpenConfirmModal: false})
      if (this.props.user_profile.current_user.user_status == ACTIVE_USER) {
        socket.emit('buy_remind', {remind_data: people_i_liked});
      }
      this.props.getCurrentUser()
    }).catch(() => {this.setState({accept_remind_with_coin: false})})
  }

  addMoreCoinClick() {
    this.rememberPaymentIntention()
    const gaTrackData = {
      page_source: 'REMIND_BANNER_WITH_COIN'
    };
    const eventAction = {
      eventCategory: 'Coin',
      eventAction: 'GetMoreCoin',
      eventLabel: 'Get more coin'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
    browserHistory.push('/payment/coin');
  }

  rememberPaymentIntention() {
    ymmStorage.setItem('Payment_Intention', 'REMIND_BANNER_WITH_COIN');
  }

  render() {
    const { user_profile } = this.props;
    const { current_user } = user_profile;
    const { coins, permission } = current_user;
    const forFree = permission != null && permission.send_boost_in_rank_free;
    return (
      <div>
        <div className="banner mbm" onClick={this.openConfirmModal}>
          <div className="banner__content">
            <span className="banner__heading banner__heading--1 txt-blue">Nhắc người ấy thích lại bạn</span>
            <p className="banner__sub-heading banner__sub-heading--1 txt-bold">
              Gây ấn tượng liên tục trong <strong className="zoom">30</strong> ngày!
            </p>
          </div>
          <img src={`${BANNER_ICON_URL}speaker-icon2.png`} alt="remind-with-coin-img" className="banner__icon"/>
          <div className="banner__meta txt-blue">
            <i className="fa fa-chevron-right"></i>
          </div>
        </div>
        <div>
          <Modal
            transitionName="modal"
            isOpen={this.state.isOpenConfirmModal}
            onRequestClose={this.closeModal}
            className="modal__content modal__content--2"
            overlayClassName="modal__overlay"
            portalClassName="modal"
            contentLabel=""
          >
            <div className="txt-center">
                <img className="img-firstlike" src={BANNER_REMIND_WITH_COIN_IMAGE} alt="" />
              </div>
            <div className="modal__txt-content">
              {current_user.user_type != 4 && <div className="txt-center padding-t20 padding-b20"> Số xu hiện có: <span className="txt-blue">{coins} xu</span></div>}
              {current_user.user_type == 4 && <div className="txt-center padding-t20 padding-b20"> Bạn có 1 tháng sử dụng miễn phí</div>}
              <div className="mbs txt-center">
                {(forFree || coins >= 100 || current_user.user_type == 4) && <button className="btn btn--p btn--b" onClick={this.handleRemindClick} >Gây ấn tượng ngay!</button>}
                {coins < 100 && !forFree && current_user.user_type != 4 && <button className="btn btn--b btn--p " onClick={this.addMoreCoinClick} >Nạp thêm xu</button>}
              </div>
              {coins < 100 && current_user.gender == 'male' && current_user.user_type != 4 &&
                <div>
                  <div className="txt-center"> hoặc
                    <Link
                      to="/payment/upgrade"
                      onClick={() => {
                        const gaTrackData = {
                          page_source: 'REMIND_BANNER_WITH_COIN'
                        };
                        const eventAction = {
                          eventCategory: 'Upgrade',
                          eventAction: 'Click',
                          eventLabel: 'Click link on popup'
                        }
                        this.props.sendGoogleAnalytic(eventAction, gaTrackData)

                        this.rememberPaymentIntention()
                      }}
                      className="txt-underline"> Nâng cấp tài khoản
                    </Link>
                  </div>
                </div>
              }
            </div>
            <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
          </Modal>
        </div>
      </div>
    )
  }

}

BannerRemindWithCoin.propTypes = {
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    acceptRemindWithCoin: () => {
      const defer = q.defer();
      dispatch(coinConsume('coin_consume_remind_liked_partners')).then(res => {
        defer.resolve(res);
      }).catch(() => {
        defer.reject(false);
      })
      return defer.promise;
    },
    getCurrentUser: () => {
      dispatch(getCurrentUser())
    },
  }
}

const BannerRemindWithCoinContainer = connect(mapStateToProps, mapDispatchToProps)(BannerRemindWithCoin);

export default BannerRemindWithCoinContainer
