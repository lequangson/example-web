import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'components/modals/Modal'
import q from 'q'
import {
  ANSWER_MY_EXPLORER_QUESTION, ANSWER_OTHER_EXPLORER_QUESTION
} from '../../../constants/Match_Questions'
import AnswerQuestions from '../AnswerQuestions'
import { getUserQuestions, answerQuestion, getMatchedAnsweredQuestion } from '../../../actions/Match_Questions'
import { getCurrentUser } from '../../../actions/userProfile'
import GoogleOptimize from '../../../utils/google_analytic/google_optimize';

class StartAnswerButton extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  componentWillMount() {
    const id = this.props.user.identifier
    if (this.props.Mode === ANSWER_MY_EXPLORER_QUESTION &&
      this.props.Match_Questions.my_questions.length === 0) {
      // this.props.getUserQuestions();
    }
    if (this.props.Mode === ANSWER_OTHER_EXPLORER_QUESTION &&
      this.props.Match_Questions.other_questions.length === 0) {
      this.props.getUserQuestions(id);
    }
  }

  @GoogleOptimize
  componentDidUpdate(){}

  openModal() {
    this.setState({ modalIsOpen: true })
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  getButtonName() {
    const questions = this.props.Mode === ANSWER_MY_EXPLORER_QUESTION?
      this.props.Match_Questions.my_questions :
      this.props.Match_Questions.other_questions;
    if (this.props.Mode === ANSWER_OTHER_EXPLORER_QUESTION) {
      return 'Thử thách 5 câu hỏi vượt rào'
    }
    if (questions.length === 0) {
      return 'Bắt đầu ngay';
    }
    const answeredQuestions = questions.filter(question => question.answer_code != null);
    if (answeredQuestions.length === 0) {
      return 'Bắt đầu ngay';
    }
    return 'Đã trả lời ' + answeredQuestions.length + '/' + questions.length;
  }

  render() {
    return (
      <div>
        <button
          className='btn btn--b btn--p'
          onClick={this.openModal}
        >
          {this.getButtonName()}
        </button>
        <Modal
          transitionName="modal"
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="StartAnswerButton modal__content modal__content--2"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
          shouldCloseOnOverlayClick={false}
        >
          <AnswerQuestions Mode={this.Mode} closeModal={this.closeModal} {...this.props}/>
          <button className="modal__btn-close" onClick={this.closeModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}

StartAnswerButton.propTypes = {
  Mode: PropTypes.string.isRequired,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    answerQuestion: (type, question_id, answer_code, explorer_user_identifier) => {
      dispatch(answerQuestion(type, question_id, answer_code, explorer_user_identifier))
    },
    getCurrentUser: () => {
      var defer = q.defer()
      dispatch(getCurrentUser()).then((response) => {
        defer.resolve(response)
      }).catch(function(err) {
        defer.reject(err)
      })
      return defer.promise
    },
    getUserQuestions: (id) => {
      dispatch(getUserQuestions(id))
    },
    getMatchedAnsweredQuestion: (id) => {
      var defer = q.defer()
      dispatch(getMatchedAnsweredQuestion(id)).then((response) => {
        defer.resolve(response)
      }).catch(function(err) {
        defer.reject(err)
      })
      return defer.promise
    }
  }
}
const StartAnswerButtonContainer = connect(mapStateToProps, mapDispachToProps)(StartAnswerButton);

export default StartAnswerButtonContainer;
