import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'components/modals/Modal'
import UploadImageModal from '../../modals/UploadImageModal'
import GoogleOptimize from '../../../utils/google_analytic/google_optimize';

class UploadImageButton extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}

  openModal() {
    this.setState({ modalIsOpen: true })
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  render() {
    return (
      <div>
        <button
          className={this.props.className}
          onClick={this.openModal}
        >
          {this.props.buttonName}
        </button>
        <Modal
          transitionName="modal"
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="UploadImageButton modal__content modal__content--3"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <UploadImageModal pictureType={this.props.pictureType} closeModal={this.closeModal} page_source={this.props.page_source} {...this.props} />
          <button className="modal__btn-close" onClick={this.closeModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}

UploadImageButton.propTypes = {
  className: PropTypes.string.isRequired,
  buttonName: PropTypes.string.isRequired,
  page_source: PropTypes.string.isRequired,
  pictureType: PropTypes.number.isRequired,
}

export default UploadImageButton;
