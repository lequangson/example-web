import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import { getUserQuestions, answerQuestion, getMatchedAnsweredQuestion } from '../../../actions/Match_Questions'
import { getCurrentUser } from '../../../actions/userProfile'
import {GIFT_IMAGES} from 'constants/Enviroment';
import GoogleOptimize from '../../../utils/google_analytic/google_optimize';
import {
  CDN_URL
} from 'constants/Enviroment'

class SendGiftButton extends React.Component {

  static propTypes = {
    Mode: PropTypes.string,
  }

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  componentWillMount() {

  }

  @GoogleOptimize
  componentDidUpdate(){}

  openModal() {
    this.setState({ modalIsOpen: true })
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  render() {
    const listGift = [
      {
        id: '1',
        name: "Thiệp",
        price: "5"
      },
      {
        id: '2',
        name: "Gấu bông",
        price: "10"
      },
      {
        id: '3',
        name: "Hoa hồng",
        price: "15"
      }
    ]
    return (
      <div className="padding-t10 padding-b10">
        <button
          className='btn btn--b btn--p'
          onClick={this.openModal}
        >
          Chọn quà
        </button>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="GiftListModal modal__content modal__content--2"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="well well--e txt-white radius-top txt-medium txt-lg txt-center mbm">
            Chọn quà
          </div>
          <div className="padding-rl10 mbm">
            {listGift.map((item, i) => {
              return (
                <div className="row modal__content--item l-flex-vertical-center padding-t10 padding-b10" key={i}>
                  <div className="col-xs-2">
                    <label>
                      <input type="checkbox" name="gift_ids" value={i} className="checkbox" />
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  <div className="col-xs-3"><img src={GIFT_IMAGES} className="img-gift" alt="Gift Image" /></div>
                  <div className="col-xs-4"><span className="txt-medium">{item.name}</span></div>
                  <div className="col-xs-3 l-flex-vertical-center">
                    <img className="margin-0 banner__icon--1" src={`${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`} alt="Khuyến mãi 20/10" />
                    <span className="padding-l5 txt-bold"> {item.price}</span>
                  </div>
                </div>
              )
            })}
          </div>

          <div className="padding-rl10 padding-b10">
            <button
                className="btn btn--b btn--p"
                onClick={() => {console.log("sent gift")}}
              >
              Tặng quà
            </button>
          </div>

          <button className="modal__btn-close" onClick={this.closeModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
    return {

    }
}
const SendGiftButtonContainer = connect(mapStateToProps, mapDispachToProps)(SendGiftButton);

export default SendGiftButtonContainer;
