import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  ShareButtons,
  ShareCounts,
  generateShareIcon
} from 'react-share';
import CopyToClipboard from 'react-copy-to-clipboard';
import $ from 'jquery'

const {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  PinterestShareButton,
  VKShareButton,
  OKShareButton,
  RedditShareButton,
  EmailShareButton,
} = ShareButtons;

const {
  FacebookShareCount,
  GooglePlusShareCount,
  LinkedinShareCount,
  PinterestShareCount,
  VKShareCount,
  OKShareCount,
  RedditShareCount,
} = ShareCounts;

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');
const LinkedinIcon = generateShareIcon('linkedin');
const PinterestIcon = generateShareIcon('pinterest');
const VKIcon = generateShareIcon('vk');
const OKIcon = generateShareIcon('ok');
const TelegramIcon = generateShareIcon('telegram');
const WhatsappIcon = generateShareIcon('whatsapp');
const RedditIcon = generateShareIcon('reddit');
const EmailIcon = generateShareIcon('email');

class ShareButton extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
    }
    this.copyLinkToClipboard = this.copyLinkToClipboard.bind(this);
  }

  renderFacebookShareCount() {
    if (this.props.showShareCount) {
      const {shareUrl, title} = this.props;
      return (
        <FacebookShareCount
          url={shareUrl}
          className="Demo__some-network__share-count">
          {count => count}
        </FacebookShareCount>
      )
    }
    return null;
  }

  renderFacebookShare() {
    if (this.props.facebookShare) {
      const {shareUrl, title, round, buttonTitle, size, buttonClass, titleClass, wrapperClass} = this.props;
      return (
        <div className={`btn-share ${wrapperClass}`}>
          <FacebookShareButton
            url={shareUrl}
            quote={title}
            className={`Demo__some-network__share-button ${buttonClass}`}>
            <FacebookIcon
              size={size}
              round={round} />
            <span className={titleClass}>{buttonTitle}</span>
          </FacebookShareButton>
          {this.renderFacebookShareCount()}
        </div>
      )
    }
    return null;
  }

  renderTwitterShareButton() {
    if (this.props.twitterShare) {
      const {shareUrl, title} = this.props;
      return (
        <div className="btn-share">
          <TwitterShareButton
            url={shareUrl}
            title={title}
            className="Demo__some-network__share-button">
            <TwitterIcon
              size={40}
              round />
          </TwitterShareButton>
        </div>
      )
    }
    return null;
  }

  renderTelegramShare() {
    if (this.props.telegramShare) {
      const {shareUrl, title} = this.props;
      return (
        <div className="btn-share">
          <TelegramShareButton
            url={shareUrl}
            title={title}
            className="Demo__some-network__share-button">
            <TelegramIcon size={40} round />
          </TelegramShareButton>
        </div>
      )
    }
    return null;
  }

  renderWhatsappShare() {
    if (this.props.whatsappShare) {
      const {shareUrl, title} = this.props;
      return (
        <div className="btn-share">
          <WhatsappShareButton
            url={shareUrl}
            title={title}
            separator=":: "
            className="Demo__some-network__share-button">
            <WhatsappIcon size={40} round />
          </WhatsappShareButton>
        </div>
      )
    }
    return null;
  }

  renderGooglePlusShareCount() {
    if (this.props.showShareCount) {
      const {shareUrl, title} = this.props;
      return (
        <GooglePlusShareCount
          url={shareUrl}
          className="Demo__some-network__share-count">
          {count => count}
        </GooglePlusShareCount>
      )
    }
    return null;
  }

  renderGooglePlusShare() {
    if (this.props.googlePlusShare) {
      const {shareUrl, title} = this.props;
      return (
        <div className="btn-share">
          <GooglePlusShareButton
            url={shareUrl}
            className="Demo__some-network__share-button">
            <GooglePlusIcon
              size={40}
              round />
          </GooglePlusShareButton>

          {this.renderGooglePlusShareCount()}
        </div>
      )
    }
    return null;
  }

  renderLinkedinShareCount() {
    if (this.props.showShareCount) {
      const {shareUrl, title} = this.props;
      return (
        <LinkedinShareCount
          url={shareUrl}
          className="Demo__some-network__share-count">
          {count => count}
        </LinkedinShareCount>
      )
    }
    return null;
  }

  renderLinkedinShare() {
    if (this.props.linkedinShare) {
      const {shareUrl, title} = this.props;
      return (
        <div className="btn-share">
          <LinkedinShareButton
            url={shareUrl}
            title={title}
            windowWidth={750}
            windowHeight={600}
            className="Demo__some-network__share-button">
            <LinkedinIcon
              size={40}
              round />
          </LinkedinShareButton>
          {this.renderLinkedinShareCount()}
        </div>
      )
    }
    return null;
  }

  renderPinterestShareCount() {
    if (this.props.showShareCount) {
      const {shareUrl} = this.props;
      return (
        <PinterestShareCount
          url={shareUrl}
          className="Demo__some-network__share-count" />
      )
    }
    return null;
  }

  renderPinterestShare() {
    if (this.props.pinterestShare) {
      const {shareUrl, shareImageUrl} = this.props;
      return (
        <div className="btn-share">
          <PinterestShareButton
            url={shareUrl}
            media={shareImageUrl}
            windowWidth={1000}
            windowHeight={730}
            className="Demo__some-network__share-button">
            <PinterestIcon size={40} round />
          </PinterestShareButton>

          {this.renderPinterestShareCount()}
        </div>
      )
    }
    return null;
  }

  renderVKShareCount() {
    if (this.props.showShareCount) {
      const {shareUrl} = this.props;
      return (
        <VKShareCount
          url={shareUrl}
          className="Demo__some-network__share-count" />
      )
    }
    return null;
  }

  renderVKShare() {
    if (this.props.vKShare) {
      const {shareUrl, shareImageUrl} = this.props;
      return (
        <div className="btn-share">
          <VKShareButton
            url={shareUrl}
            image={shareImageUrl}
            windowWidth={660}
            windowHeight={460}
            className="Demo__some-network__share-button">
            <VKIcon
              size={40}
              round />
          </VKShareButton>

          {this.renderVKShareCount()}
        </div>
      )
    }
    return null;
  }

  renderOKShareCount() {
    if (this.props.showShareCount) {
      const {shareUrl} = this.props;
      return (
        <OKShareCount
          url={shareUrl}
          className="Demo__some-network__share-count" />
      )
    }
    return null;
  }

  renderOKShare() {
    if (this.props.oKShare) {
      const {shareUrl, shareImageUrl} = this.props;
      return (
        <div className="btn-share">
          <OKShareButton
            url={shareUrl}
            image={shareImageUrl}
            windowWidth={660}
            windowHeight={460}
            className="Demo__some-network__share-button">
            <OKIcon
              size={40}
              round />
          </OKShareButton>

          {this.renderOKShareCount()}
        </div>
      )
    }
    return null;
  }

  renderRedditShareCount() {
    if (this.props.showShareCount) {
      const {shareUrl} = this.props;
      return (
        <RedditShareCount
          url={shareUrl}
          className="Demo__some-network__share-count" />
      )
    }
    return null;
  }

  renderRedditShare() {
    if (this.props.redditShare) {
      const {shareUrl, title} = this.props;
      return (
        <div className="Demo__some-network">
          <RedditShareButton
            url={shareUrl}
            title={title}
            windowWidth={660}
            windowHeight={460}
            className="Demo__some-network__share-button">
            <RedditIcon
              size={40}
              round />
          </RedditShareButton>

          {this.renderRedditShareCount()}
        </div>
      )
    }
    return null;
  }

  renderEmailShare() {
    if (this.props.emailShare) {
      const {shareUrl, title} = this.props;
      return (
        <div className="Demo__some-network">
          <EmailShareButton
            url={shareUrl}
            subject={title}
            body="body"
            className="Demo__some-network__share-button">
            <EmailIcon
              size={40}
              round />
          </EmailShareButton>
        </div>
      )
    }
    return null;
  }

  renderCopyLink() {
    if (this.props.copyLink) {
      const {shareUrl} = this.props;
      return (
        <div className="btn-share">
          <CopyToClipboard
            text={shareUrl}
            onCopy={this.copyLinkToClipboard}
          >
            <button className="btn btn--s btn-share__btn"
              style={{"width": "40px","height": "40px"}}>Link</button>
          </CopyToClipboard>
        </div>
      )
    }
  }

  copyLinkToClipboard() {
    const resultNoti = $(`
      <div class="noti noti--bg-green mbm txt-bold">
        <div class="noti__body txt-md">
          <div class="noti__header mbs">
            <i class="fa fa-check-circle txt-xxxxlg"></i>
          </div>
          <div class="noti__content">Link đã được \n sao chép</div>
        </div>
      </div>
    `)
    setTimeout(function() {
      $('body').append(resultNoti)
    }, 200)

    setTimeout(function() {
      $(resultNoti).remove()
    }, 7000)
  }

  render() {
    return (
      <div className="Demo__container l-flex-vertical-center">
        {this.renderFacebookShare()}
        {this.renderTwitterShareButton()}
        {this.renderGooglePlusShare()}
        {this.renderEmailShare()}
        {this.renderTelegramShare()}
        {this.renderWhatsappShare()}
        {this.renderLinkedinShare()}
        {this.renderPinterestShare()}
        {this.renderVKShare()}
        {this.renderOKShare()}
        {this.renderRedditShare()}
        {this.renderCopyLink()}
      </div>
    )
  }
}

ShareButton.propTypes = {
  shareUrl: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  buttonTitle: PropTypes.string,
  round: PropTypes.bool,
  size: PropTypes.number,
  showShareCount: PropTypes.bool,
  facebookShare: PropTypes.bool,
  twitterShare: PropTypes.bool,
  telegramShare: PropTypes.bool,
  whatsappShare: PropTypes.bool,
  googlePlusShare: PropTypes.bool,
  linkedinShare: PropTypes.bool,
  pinterestShare: PropTypes.bool,
  vKShare: PropTypes.bool,
  oKShare: PropTypes.bool,
  redditShare: PropTypes.bool,
  emailShare: PropTypes.bool,
  copyLink: PropTypes.bool,
  shareImageUrl: PropTypes.string,
  wrapperClass: PropTypes.string,
  buttonClass: PropTypes.string,
  titleClass: PropTypes.string
}

export default ShareButton;