import React, { Component, PropTypes } from 'react';
import { LANDINGPAGE_NEW_BASE_URL } from 'constants/Enviroment'

class AndroidDownloadButton extends Component {
  static propTypes = {
    redirect_url: PropTypes.string.isRequired
  }

  render() {
    return (
      <a href={this.props.redirect_url} target="_blank">
        <div className="lp-installation__item txt-center mbm">
          <img className="padding-rl10" src={LANDINGPAGE_NEW_BASE_URL + 'google_play.png'} alt="App-hen-ho-online-Ymeetme-tren-Android" />
        </div>
      </a>
    );
  }
}

export default AndroidDownloadButton;