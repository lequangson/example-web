import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import ViewMyAnswer from '../ViewMyAnswer'
import { chooseExplorerQuestion } from '../../../actions/Match_Questions'
import GoogleOptimize from '../../../utils/google_analytic/google_optimize';

class SelectMyExploreQuestionButton extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  componentWillMount() {
    if (this.props.Match_Questions.my_questions.length === 0) {
      // this.props.getMyQuestion();
    }
  }

  @GoogleOptimize
  componentDidUpdate(){}

  openModal() {
    this.setState({ modalIsOpen: true })
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  getButtonName() {
    if (this.props.Match_Questions.my_questions.length === 0) {
      return 'Chọn câu hỏi ngay!';
    }
    const exploreQuestions = this.props.Match_Questions.my_questions.filter(question => question.is_explore_question === 1);
    if (exploreQuestions.length === 0) {
      return 'Chọn câu hỏi ngay!';
    }
    return 'Đã chọn ' + exploreQuestions.length + '/5';
  }

  render() {

    return (
      <div>
        <button
          className='btn btn--b btn--p'
          onClick={this.openModal}
        >
          {this.getButtonName()}
        </button>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="SelectMyExploreQuestionButton modal__content"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          <ViewMyAnswer {...this.props}/>
          <button className="modal__btn-close" onClick={this.closeModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}

SelectMyExploreQuestionButton.propTypes = {
  // className: PropTypes.string.isRequired,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    chooseExplorerQuestion: (question_id, is_explore_question) => {
      dispatch(chooseExplorerQuestion(question_id, is_explore_question))
    },
  }
}
const SelectMyExploreQuestionButtonContainer = connect(mapStateToProps, mapDispachToProps)(SelectMyExploreQuestionButton);

export default SelectMyExploreQuestionButtonContainer;
