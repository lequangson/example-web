import React, { Component,PropTypes } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router'
import { browserHistory } from 'react-router'
import {
  CDN_URL
} from '../../constants/Enviroment'
import { 
  updatePaymentState, getPaymentPackages,
} from '../../actions/Payment';

class TinderAdCard extends Component {
  constructor(props) {
		super(props);
    this.state = {
      adProgramData: [
        {
          title: 'GIỚI THIỆU BẠN BÈ',
          description: 'Nhận ngay 25 xu với mỗi người bạn đăng ký thành công.',
          imageUrl: `${CDN_URL}/general/Referral/shokai.jpg`,
          titleColor: '#005c99',
          skipButtonId: 'SkipReferralProgram',
          showMoreButtonId: 'showMoreReferralProgram',
        },
        {
          title: 'Thoát kiếp FA',
          description: 'Chỉ cần sử dụng gói nâng cấp<br />3 tháng và online 69 ngày liên tục.',
          imageUrl: `${CDN_URL}/general/Referral/Guarantee+policy_1_image.png`,
          titleColor: '#ff7e81',
          skipButtonId: 'SkipGuaranteePolicy1',
          showMoreButtonId: 'showMoreGuaranteePolicy1',
        },
        {
          title: '"Tìm người tâm sự"<br />nhanh nhất!',
          description: 'Gửi ít nhất 30 lượt "thích"<br />và nâng cấp tài khoản để lên đầu<br />trang tìm kiếm',
          imageUrl: `${CDN_URL}/general/Referral/Guarantee+policy_2_image.png`,
          titleColor: '#6fa6d3',
          skipButtonId: 'SkipGuaranteePolicy2',
          showMoreButtonId: 'showMoreGuaranteePolicy2',
        },
        {
          title: '100% đã dùng<br />và hẹn hò thành công!',
          description: 'Nâng cấp tài khoản để<br />nói chuyện nhiều hơn.',
          imageUrl: `${CDN_URL}/general/Referral/Guarantee+policy_3_image.png`,
          titleColor: '#f1b666',
          skipButtonId: 'SkipGuaranteePolicy3',
          showMoreButtonId: 'showMoreGuaranteePolicy3',
        },
        {
          title: 'Chữa khỏi bệnh "Ế"!',
          description: 'Dùng liều "nâng cấp tài khoản"<br />3 tháng và chủ động trò chuyện<br />bất kì lúc nào!',
          imageUrl: `${CDN_URL}/general/Referral/Guarantee+policy_4_image.png`,
          titleColor: '#6fa6d3',
          skipButtonId: 'SkipGuaranteePolicy4',
          showMoreButtonId: 'showMoreGuaranteePolicy4',
        },
        {
          title: 'Để chuyện "có gấu"<br />không còn xa vời',
          description: 'Đừng quên sử dụng gói nâng cấp tài<br />khoản 3 tháng và hẹn gặp<br />người ấy sau 3 ngày nói chuyện!',
          imageUrl: `${CDN_URL}/general/Referral/Guarantee+policy_5_image.png`,
          titleColor: '#c57476',
          skipButtonId: 'SkipGuaranteePolicy5',
          showMoreButtonId: 'showMoreGuaranteePolicy5',
        },
      ],
    }
    this.showMore = this.showMore.bind(this);
    this.onSkip = this.onSkip.bind(this);
	}

  componentWillMount() {
    const needGotoPaymentIndex = [1,2,3,4,5];
    if (needGotoPaymentIndex.indexOf(this.props.adProgramIndex) >= 0 &&
      this.props.Payment.packages.length === 0) {
      this.props.getPaymentPackages(1);
    }
  }

  showMore() {
    this.props.showMoreCallback();

    switch (this.props.adProgramIndex) {
      case 0:
        browserHistory.push('/referral-page');
        break;
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      {
        const popularPackage = this.props.Payment.packages.find(pk1 => (pk1.license_type == 'license_type_monthly' && pk1.is_popular));    
        if (popularPackage) {
          this.props.updatePaymentState('default_package_id', parseInt(popularPackage.id))
        }
        browserHistory.push('/payment/upgrade');
        break;
      }
      default:
        break;
    }
  }

  onSkip() {
    this.props.skipCallBack();
  }

	render(){
    const adData = this.state.adProgramData[this.props.adProgramIndex];
    return(
			<div className="col-sm-12 col-md-8 col-md-offset-2 padding-t30">
        <article className="card mbm">
          <div className="card__upper card__upper--bg-white">
            <div className="padding-b20">
            	<Link onClick={this.showMore} className="cursor">
                <img src={adData.imageUrl} alt={adData.title} className="img-full"/>
              </Link>
            </div>
            <div className="txt-center">
              <div className="txt-bold txt-xlg txt-blue txt-uppercase" style={{color: adData.titleColor}}>
                <div dangerouslySetInnerHTML={{__html: adData.title}} />
              </div>
            </div>
          </div>
          <div className="card__under card__under--bg-white">
            <div className="txt-center padding-b10">
              <div dangerouslySetInnerHTML={{__html: adData.description}} />
            </div>
            <div>
              <div className="row padding-b30">
                <div className="col-xs-6">
                  <button className="btn btn--b txt-dark" id={adData.skipButtonId} onClick={this.onSkip}>
                    Không, cảm ơn
                  </button>
                </div>
                <div className="col-xs-6 txt-center">
                  <button className="btn txt-center btn--b btn--6" id={adData.showMoreButtonId} onClick={this.showMore}>
                    Xem thêm
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="card__top"></div>
        </article>
      </div>
		);
	}
}

TinderAdCard.propsType = {
  adProgramIndex: PropTypes.number.isRequired,
  showMoreCallback: PropTypes.func.isRequired,
  skipCallBack: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    updatePaymentState: (key, value) => {
      dispatch(updatePaymentState(key, value));
    },

    getPaymentPackages: package_type => {
      dispatch(getPaymentPackages(package_type))
    },
  }
}
const TinderAdCardContainer = connect(mapStateToProps, mapDispachToProps)(TinderAdCard);

export default TinderAdCardContainer;