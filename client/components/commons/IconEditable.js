import React from 'react'

function IconEditable() {
  return <i className="fa fa-pencil" />
}

export default IconEditable;
