import React, { Component, PropTypes } from 'react'

class DropdownPanel extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isOpen: this.props.isOpen,
    }

    this.togglePanelBody = this.togglePanelBody.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen != this.state.isOpen && +nextProps.activeDropdowm === 6) {
    console.log('nextProps.DropdownPanel', nextProps.activeDropdowm)
      this.state = {
        isOpen: this.props.isOpen,
      }      
    }
  }

  togglePanelBody() {
    this.setState({ isOpen: !this.state.isOpen })
  }

  render() {
    return (
      <div className="panel mbs">
        <a
          href=""
          className="panel__heading panel__heading--2"
          onClick={(evt) => {
            evt.preventDefault()
            this.togglePanelBody()
          }}
        >
          <h3 className="panel__title">{this.props.panelTitle}</h3>
          <i
            className={this.state.isOpen
              ? 'fa fa-chevron-down is-up'
              : 'fa fa-chevron-down'
            }
          ></i>
        </a>
        <div
          className={this.state.isOpen
            ? 'panel__body panel__body--2'
            : 'panel__body panel__body--2 is-hide'
          }
        >
          {this.props.children}
        </div>
      </div>
    )
  }
}

DropdownPanel.propTypes = {
  isOpen: PropTypes.bool,
  panelTitle: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
}

export default DropdownPanel
