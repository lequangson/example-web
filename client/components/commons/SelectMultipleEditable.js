import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import $ from 'jquery'
import { updateUserProfileFailure } from '../../actions/userProfile'

class SelectMultipleEditable extends React.Component {

  static renderSelect(value, data) {
    return data.map((val, i) => {
      if (isNaN(val)) {
        return <option className="checkbox" value={val.key} key={i}>{val.value}</option>
      }
      return <option className="checkbox" value={val} key={i}>{val}</option>
    })
  }

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      id: '',
      max_size: 0,
      max_size_msg: '',
    }
  }

  componentWillMount() {
    const { id, name, max_size, max_size_msg } = this.props
    this.setState({
      name,
      id,
      max_size,
      max_size_msg,
    })
  }

  handleOnchangeEvent() {
    const selected_values = $(`#${this.state.id}`).val()
    const { max_size, max_size_msg } = this.state
    if (max_size && selected_values.length > max_size) {
      this.props.showErrorMessage(max_size_msg)
      return
    }
    const params = {};
    params[this.state.name] = []
    for (let i = 0; i < selected_values.length; i += 1) {
      params[this.state.name].push(selected_values[i])
    }
    if (params[this.state.name][0] == null) {
      params[this.state.name].push('');
    }
    this.props.updateUserProfile(params)
  }

  render() {
    const { id, name, value, data } = this.props
    const defaultValue = []
    if (value && value.length) {
      for (let i = 0; i < value.length; i += 1) {
        defaultValue.push(value[i].key)
      }
    }
    return (
      <select
        id={id}
        name={name}
        multiple
        className="form__input--a txt-blue"
        defaultValue={defaultValue}
        onChange={() => { this.handleOnchangeEvent() }}
      >
        <optgroup disabled></optgroup>
        { SelectMultipleEditable.renderSelect(value, data) }
      </select>
    )
  }
}

SelectMultipleEditable.propTypes = {
  // id: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.array,
  max_size: PropTypes.number,
  max_size_msg: PropTypes.string,
  data: PropTypes.array,
  updateUserProfile: PropTypes.func,
  showErrorMessage: PropTypes.func,

}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    showErrorMessage: (msg) => {
      dispatch(updateUserProfileFailure(msg))
    },
  }
}

const SelectMultipleEditableContainer = connect(mapStateToProps, mapDispachToProps)(SelectMultipleEditable);

export default SelectMultipleEditableContainer;
