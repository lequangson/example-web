import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'components/modals/Modal'
import ReactSlider from 'react-slider'
import SelectMultiple from 'components/commons/SelectMultiple'
import Infinite from 'react-infinite'
import { Link, browserHistory } from 'react-router'
import $ from 'jquery'
import _ from 'lodash'
import DetailItem from '../searches/DetailItem'
import ListTwoItems from '../searches/ListTwoItems'
import Banner from '../commons/Banner'
import CoinBanner from '../commons/CoinBanner'
import { shuffleArray, uniqUsers, compareIdentifiers } from '../../utils/common'
import { isUserInBoostInRankPeriod } from '../../utils/BoostInRankService'
import { InfiniteLoad_ICON_URL } from '../../constants/Enviroment'
import { SEARCH_PAGE, GA_ACTION_SEARCH, GA_ACTION_COMPLETE_TUTORIAL, GA_ACTION_SIMPLE_FILTER_CONDITION } from '../../constants/Enviroment'
import { SEARCH_PAGE_SIZE, PRE_LOAD_ITEM_NUMBER } from '../../constants/search'
import { NUMBER_OF_TIPS, DISPLAY_TIPS_AFTER_LIST_ITEM_NO, DISPLAY_TIPS_AFTER_DETAIL_ITEM_NO } from '../../constants/Tips'
import { gaSend } from '../../actions/Enviroment'
import * as ymmStorage from '../../utils/ymmStorage';
import { LOADING_DATA_TEXT } from '../../constants/TextDefinition'
import ListSameGenderBoostUsers from 'containers/list-same-gender-boost-users';

class InfiniteLoad extends React.Component {
  static elementInfiniteLoad() {
    return (
      <div className="col-xs-12 txt-center loader is-load">
        <i className="fa fa-spinner fa-3x" />
      </div>
    )
  }

  constructor(props) {
    super(props)
    this.state = {
      item_number: 0,
      elements: {
        default: [],
        new: [],
        nearby: [],
        online: [],
        matching_ratio: [],
        harmony_age: [],
        harmony_fate: [],
        harmony_horoscope: [],
      },
      listView_ElementHeights: [],
      detailView_elementHeights: [],
      componentHeight: {
        subMenu: 59,
        changeViewMode: 41,
        tipsBanner: 65,
        gridCard: 457,
        detailCard:845
      },

      infinite_loading: 200,
      isInfiniteLoading: false,
      current_page: {
        default: 1,
        new: 1,
        nearby: 1,
        online: 1,
        matching_ratio: 1,
        harmony_age: 1,
        harmony_fate: 1,
        harmony_horoscope: 1,
      },
      fixedDivOffsetBottom: 0,
      shuffleTips: [],
      updateTips: false,
      needSendGA: false,
      banner_list: shuffleArray(['boot_rank', 'random_match', 'remind_banner_with_coin']),
      isOpen: false,
      locationValues: 1,
      startAge: 18,
      endAge: 70,
    }
    this.closeFilterModal = this.closeFilterModal.bind(this)
    this.changeViewMode=this.changeViewMode.bind(this)
    this.handleInfiniteLoad = this.handleInfiniteLoad.bind(this)
    this.handleChangeHarmony = this.handleChangeHarmony.bind(this)
    this.updateElement = this.updateElement.bind(this)
  }

  componentWillMount() {
    window.scroll(0, 0)
    const { search, enviroment } = this.props
    if (!this.props.list_boost_in_rank.isFetching && this.props.list_boost_in_rank.boostInRankUser.length === 0) {
      this.props.getListBoostInRank();
    }
    if (!this.props.search.loaded_condition) {
      this.props.getSearchCondition();
    }
    this.initialSimpleFilterParameter(this.props.search, this.props.user_profile.current_user);

    switch (enviroment) {
      case 'default':
        if (!this.props.search[enviroment].data.length || this.props.search.condition.updated) {
          this.props.searchUser(this.state.current_page[enviroment])
        }
        break;
      case 'new':
        this.props.searchNewUser(this.state.current_page[enviroment])
        break;
      case 'nearby':
        this.props.searchNearbyUser(this.state.current_page[enviroment])
        break;
      case 'online':
        this.props.searchOnlineUser(this.state.current_page[enviroment])
        break;
      case 'matching_ratio':
        if (!this.props.search[enviroment].data.length) {
          this.props.searchMatchingRatioUser(this.state.current_page[enviroment])
        }
        break;
      case 'harmony_age':
        if (!this.props.search[enviroment].data.length) {
          this.props.searchHarmonyOfAge(this.state.current_page[enviroment])
        }
        break;
      case 'harmony_fate':
        if (!this.props.search[enviroment].data.length) {
          this.props.searchHarmonyOfFate(this.state.current_page[enviroment])
        }
        break;
      case 'harmony_horoscope':
        if (!this.props.search[enviroment].data.length) {
          this.props.searchHarmonyOfHoroscope(this.state.current_page[enviroment])
        }
        break;
      default:
        break;
    }
    this.setState({
      shuffleTips: shuffleArray(this.props.Tips.banners),
    })
    // handle infinite loading
    const { total_elements, total_result } = search[enviroment]
    if (total_result && !this.state.elements[enviroment].length) {
      if (this.state.elements[enviroment].length >= this.props.search[enviroment].total_result) {
        this.setState({ infinite_loading: undefined }) // set undefined to disabled loading
      }
      this.setState({
        infinite_loading: 123,
        current_page: {...this.state.current_page, [enviroment]: search[enviroment].current_page},
      })
    }

    this.updateElement(this.buildElements(0, search[enviroment].total_elements ? search[enviroment].total_elements : PRE_LOAD_ITEM_NUMBER))
    const page_source = 'SEARCH_PAGE_' + enviroment.toUpperCase()
    this.props.updatePreviousPage(page_source);
  }

  componentWillReceiveProps(nextProps) {
    if ((nextProps.search.loaded_condition && !this.props.search.loaded_condition) ||
      (!_.isEmpty(nextProps.user_profile.current_user) && _.isEmpty(this.props.user_profile.current_user))) {
      this.initialSimpleFilterParameter(nextProps.search, nextProps.user_profile.current_user);
    }
    if(this.props.enviroment !== nextProps.enviroment ||
      (nextProps.search[nextProps.enviroment].data.length === 0 &&
      this.props.search[this.props.enviroment].data.length > 0)) {
      const page_source = 'SEARCH_PAGE_' + nextProps.enviroment.toUpperCase()
      this.props.updatePreviousPage(page_source);
      this.showNextBoostUsers();
      window.scroll(0, 0)
      this.setState({
        elements: {...this.state.elements, [nextProps.enviroment]: []},
        listView_ElementHeights: this.calculateElementHeights([], 'LIST_VIEW'),
        detailView_elementHeights: this.calculateElementHeights([], 'DETAIL_VIEW'),
      })
      this.props.updatePagingInformation(
        this.state.elements[this.props.enviroment].length,
        this.state.current_page[this.props.enviroment],
        Math.floor(window.scrollY),
        this.props.enviroment,
        nextProps.enviroment
      )
      const enviroment = nextProps.enviroment
      switch (enviroment) {
        case 'default':
          if (!this.props.search[nextProps.enviroment].data.length) {
            this.props.searchUser(this.state.current_page[enviroment])
          }
          break;
        case 'new':
          this.setState({ current_page: { ...this.state.current_page, new: 1 }},
            () => { this.props.searchNewUser(this.state.current_page[enviroment]) }
          )
          break;
        case 'nearby':
          this.setState({ current_page: { ...this.state.current_page, nearby: 1 }},
            () => { this.props.searchNearbyUser(this.state.current_page[enviroment]) }
          )
          break;
        case 'online':
          this.setState({ current_page: { ...this.state.current_page, online: 1 }},
            () => { this.props.searchOnlineUser(this.state.current_page[enviroment]) }
          )
          break;
        case 'matching_ratio':
          if (!this.props.search[nextProps.enviroment].data.length) {
            this.props.searchMatchingRatioUser(this.state.current_page[enviroment])
          }
          break;
        case 'harmony_age':
          if (!this.props.search[nextProps.enviroment].data.length) {
            this.props.searchHarmonyOfAge(this.state.current_page[enviroment])
          }
          break;
        case 'harmony_fate':
          if (!this.props.search[nextProps.enviroment].data.length) {
            this.props.searchHarmonyOfFate(this.state.current_page[enviroment])
          }
          break;
        case 'harmony_horoscope':
          if (!this.props.search[nextProps.enviroment].data.length) {
            this.props.searchHarmonyOfHoroscope(this.state.current_page[enviroment])
          }
          break;
        default:
          break;
      }
    }
    if (!_.isEmpty(nextProps.user_profile.current_user) && _.isEmpty(this.props.user_profile.current_user)) {
      this.setState({ needSendGA: true })
    }
    const current_user = this.props.user_profile.current_user;
    const banners = current_user.gender == 'male' ? this.props.Tips.banners : this.props.Tips.banners.filter(banner => banner.icon_number !== '4')
    if (!_.isEmpty(current_user) && !this.state.updateTips) {
      this.setState({
        shuffleTips: shuffleArray(banners),
        updateTips: true,
      })
    }
    let needShow = current_user.user_type !== 4 && !isUserInBoostInRankPeriod(current_user);
    if (!needShow && this.state.banner_list.findIndex(banner => banner == 'boot_rank') > -1) {
      const new_banner_list = this.state.banner_list.filter(banner => banner !== 'boot_rank')
      this.setState({banner_list: new_banner_list});
    }
    if (this.props.user_profile.current_user.permission !== nextProps.user_profile.current_user.permission) {
      this.forceUpdate() // for case have 3 free random match
    }
  }

  componentWillUnmount() {
    const { enviroment } = this.props
    this.props.updatePagingInformation(
      this.state.elements.length,
      this.state.current_page[enviroment],
      Math.floor(window.scrollY),
      enviroment,
      '' // get source for view other profile
    )

    this.props.setProfileUserNextIndex(0)
    this.showNextBoostUsers();
  }

  showNextBoostUsers = () => {
    const { enviroment } = this.props;
    this.props.showNextBoostUsers(enviroment);
  }

  defaultSimpleFilterParameter = (current_user) => {
    let startAge = this.state.startAge;
    let endAge = this.state.endAge;

    if (!_.isEmpty(current_user)){
      if (current_user.gender === 'male') {
        startAge = current_user.age - 10;
        endAge = current_user.age + 5;
      }
      else {
        startAge = current_user.age - 5;
        endAge = current_user.age + 10;
      }

      if (startAge < 18) {
        startAge = 18;
      }
      if (endAge > 70) {
        endAge = 70;
      }
    }
    if (startAge != this.state.startAge ||
      endAge != this.state.endAge) {
      this.setState({startAge, endAge});
    }
  }

  initialSimpleFilterParameter = (searchData, current_user) => {
    let startAge = this.state.startAge;
    let endAge = this.state.endAge;

    if (searchData.loaded_condition &&
      searchData.condition.length > 1) {
      if (searchData.condition[1].age_min && searchData.condition[1].age_min > 0 &&
        searchData.condition[1].age_max && searchData.condition[1].age_max > 0) {
        startAge = searchData.condition[1].age_min;
        endAge = searchData.condition[1].age_max;
        this.setState({startAge, endAge});
        return;
      }
    }
    this.defaultSimpleFilterParameter(current_user);
  }

  scrollToPreviousPosition() {
    const { enviroment } = this.props
    const need_scroll_to_position = this.props.search.need_scroll_to_position
    const view_mode = this.props.search[enviroment].view_mode
    const scroll_position = this.props.search[enviroment].scroll_position
    if (scroll_position > 0 && need_scroll_to_position) {
      const userCardHeight = $('.element_height').height()
      let segmentIndex = 0
      if (view_mode == 'DETAIL_VIEW') {
        segmentIndex = this.props.Enviroment.profile_next_user_index * userCardHeight;
      }
      else {
        segmentIndex = Math.floor(this.props.Enviroment.profile_next_user_index / 2) * userCardHeight;
      }
      const position = Math.floor(scroll_position + segmentIndex);
      //window.scroll(0, position)
      window.scrollTo(0, position)
      if (Math.round(window.scrollY) >= position -10) {
        this.props.needScrollToPosition(false)
      }
    }
  }

  componentDidMount() {
    this.getComponentHeight();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.force_reload_infinite != nextProps.force_reload_infinite) {
       return true;
    }

    if (nextProps.search[nextProps.enviroment].data.length === 0 &&
      this.props.search[this.props.enviroment].data.length > 0) {
      return true;
    }

    return nextState !== this.state ||
      nextProps.search[nextProps.enviroment] !== this.props.search[this.props.enviroment] ||
      (nextProps.search.need_scroll_to_position &&
        nextProps.search[nextProps.enviroment].scroll_position > 0)
  }

  componentWillUpdate(nextProps) {
    const { search, enviroment } = this.props
    if (this.state.needSendGA) {
      const ga_track_data = {
        view_mode: this.props.search[enviroment].view_mode,
        page_number: this.state.current_page,
      };
      this.props.gaSend(GA_ACTION_SEARCH, ga_track_data);
      this.setState({ needSendGA: false })
    }
  }

  componentDidUpdate() {
    const { search, enviroment } = this.props
    const { elements } = this.state
    this.scrollToPreviousPosition();
    this.getComponentHeight();

    const boostUsers = this.getBoostUser();
    if (search[enviroment].data.length && elements[enviroment].length <= boostUsers.length) {
      const newElements = this.buildElements(0, PRE_LOAD_ITEM_NUMBER)
      this.updateElement(newElements)
    }
  }

  getComponentHeight() {
    const { enviroment } = this.props
    let subMenuHeight = Math.round($('.submenu-component').height());
    let changeViewMode = Math.round($('.list-view').height());
    let tipsBanner = Math.round($('.banner').height());
    let cardHeight = Math.round($('.element_height').height());
    const view_mode = this.props.search[enviroment].view_mode;
    const stateCardHeight = view_mode == 'LIST_VIEW' ? this.state.componentHeight.gridCard : this.state.componentHeight.detailCard;
    let needCalculateElementHeight = false;
    if (!isNaN(subMenuHeight) && subMenuHeight != 0 && subMenuHeight != this.state.componentHeight.subMenu) {
      needCalculateElementHeight = true
    }
    else {
      subMenuHeight = this.state.componentHeight.subMenu
    }

    if (!isNaN(changeViewMode) && changeViewMode != 0 && changeViewMode != this.state.componentHeight.changeViewMode) {
      needCalculateElementHeight = true
    }
    else {
      changeViewMode != this.state.componentHeight.changeViewMode
    }

    if (!isNaN(tipsBanner) && tipsBanner != 0 && tipsBanner != this.state.componentHeight.tipsBanner) {
      needCalculateElementHeight = true
    }
    else {
      tipsBanner = this.state.componentHeight.tipsBanner
    }

    if (!isNaN(cardHeight) && cardHeight != 0 && cardHeight != stateCardHeight) {
      needCalculateElementHeight = true
    }
    else {
      cardHeight = stateCardHeight
    }
    if (needCalculateElementHeight){
      const gridCard = (view_mode == 'LIST_VIEW'? cardHeight : this.state.componentHeight.gridCard);
      const detailCard = (view_mode != 'LIST_VIEW'? cardHeight : this.state.componentHeight.detailCard);
      this.setState(
        {
          componentHeight: {
            ...this.state.componentHeight,
            subMenu: subMenuHeight,
            changeViewMode: changeViewMode,
            tipsBanner: tipsBanner,
            gridCard: gridCard,
            detailCard: detailCard
          },
          listView_ElementHeights: this.buildElementHeights(this.state.elements[enviroment], 'LIST_VIEW', gridCard, changeViewMode, subMenuHeight, tipsBanner),
          detailView_elementHeights: this.buildElementHeights(this.state.elements[enviroment], 'DETAIL_VIEW', detailCard, changeViewMode, subMenuHeight, tipsBanner),
        }
      )
    }
  }

  getBoostUser() {
    const { enviroment, list_boost_in_rank } = this.props;
    switch (enviroment) {
      case 'new':
        return list_boost_in_rank.boostInRankUserNew[list_boost_in_rank.currentIndexNew] || [];
      case 'online':
        return list_boost_in_rank.boostInRankUserOnline[list_boost_in_rank.currentIndexOnline] || [];
      default:
        return [];
    }
  }

  buildElements(start, end) {
    const { enviroment } = this.props;
    const search = this.props.search[enviroment].data;
    const boostUsers = this.getBoostUser();

    let elements;
    if (start === 0) {
      elements = Array.from(boostUsers);
    } else {
      elements = [];
      start = start >= boostUsers.length ? start - boostUsers.length : 0;
      end = end > boostUsers.length ? end - boostUsers.length : 0;
    }

    if (search.length > start) {
      this.setState({ infinite_loading: 200 }) // set undefined to disabled loading
      for (let i = start; i < end; i += 1) {

        if (typeof search[i] !== 'undefined'
          && parseInt(search[i].block_type, 10) !== 1) {
          elements.push(search[i])
        }
      }
    }
    if (this.state.elements[enviroment].length >= this.props.search[enviroment].total_result) {
      this.setState({ infinite_loading: undefined }) // set undefined to disabled loading
      // there is no more data, need to do something
    }

    return elements;
  }

  changeViewMode() {
    const enviroment = this.props.enviroment
    const newViewMode = this.props.search[enviroment].view_mode === 'DETAIL_VIEW' ? 'LIST_VIEW' : 'DETAIL_VIEW'
    ymmStorage.setItem('viewMode', newViewMode)
    this.props.updateViewMode(newViewMode, enviroment);
  }

  handleChangeHarmony() {
    browserHistory.push(`/search/${this.refs.harmony.value}`)
  }

  handleInfiniteLoad() {
    const { enviroment } = this.props
    const that = this;
    this.setState({
      isInfiniteLoading: true,
    });
    let elemLength = 0
    let newElements = []
    setTimeout(() => {
      that.setState({ total_elements: that.state.elements[enviroment].length + PRE_LOAD_ITEM_NUMBER }) // save current total element into global state
      elemLength = that.state.elements[enviroment].length
      newElements = that.buildElements(elemLength, elemLength + PRE_LOAD_ITEM_NUMBER);
      if (
        (elemLength + PRE_LOAD_ITEM_NUMBER) % SEARCH_PAGE_SIZE === 0
        || (elemLength % SEARCH_PAGE_SIZE !== 0 && elemLength < that.props.search[enviroment].total_result && elemLength > PRE_LOAD_ITEM_NUMBER)
      ) {
        switch (enviroment) {
          case 'default':
            that.props.searchUser(that.state.current_page[enviroment] + 1, '')
            break;
          case 'new':
            that.props.searchNewUser(that.state.current_page[enviroment] + 1, '')
            break;
          case 'nearby':
            that.props.searchNearbyUser(that.state.current_page[enviroment] + 1, '')
            break;
          case 'online':
            that.props.searchOnlineUser(that.state.current_page[enviroment] + 1, '')
            break;
          case 'matching_ratio':
            that.props.searchMatchingRatioUser(that.state.current_page[enviroment] + 1, '')
            break;
          case 'harmony_age':
            that.props.searchHarmonyOfAge(that.state.current_page[enviroment] + 1, '')
            break;
          case 'harmony_fate':
            that.props.searchHarmonyOfFate(that.state.current_page[enviroment] + 1, '')
            break;
          case 'harmony_horoscope':
            that.props.searchHarmonyOfHoroscope(that.state.current_page[enviroment] + 1, '')
            break;
          default:
            break;
        }
        that.setState({ current_page: {...that.state.current_page, [enviroment]: that.state.current_page[enviroment] + 1} })
      }
      that.setState({
        isInfiniteLoading: false,
      });
      that.updateElement(uniqUsers(that.state.elements[enviroment].concat(newElements)))
    }, 500);
    // Send google analytics
    const view_mode = this.props.search[enviroment].view_mode
    if (!_.isEmpty(this.props.user_profile.current_user)) {
      const ga_track_data = {
        view_mode: view_mode,
        page_number: that.state.current_page,
      };
      that.props.gaSend(GA_ACTION_SEARCH, ga_track_data);
    }
  }

  updateElement(newElements) {
    const { enviroment } = this.props
    this.setState({
      elements: {...this.state.elements, [enviroment]: newElements,},
      listView_ElementHeights: this.calculateElementHeights(newElements, 'LIST_VIEW'),
      detailView_elementHeights: this.calculateElementHeights(newElements, 'DETAIL_VIEW'),
    })
  }

  shouldRenderBanner(viewMode, index) {
    const displayTipAfterItemNumber = viewMode == 'DETAIL_VIEW' ? DISPLAY_TIPS_AFTER_DETAIL_ITEM_NO - 1: (DISPLAY_TIPS_AFTER_LIST_ITEM_NO / 2) - 1; // one row display 2 items
    const banners = this.state.shuffleTips;
    if (index <= 0 || index % displayTipAfterItemNumber !== 0 || banners == null || banners.length === 0) {
      return false;
    }
    return true
  }

  calculateElementHeights(newElements, view_mode) {
    const cardHeight = view_mode == 'LIST_VIEW'? this.state.componentHeight.gridCard : this.state.componentHeight.detailCard;
    return this.buildElementHeights(newElements, view_mode, cardHeight, this.state.componentHeight.changeViewMode, this.state.componentHeight.subMenu, this.state.componentHeight.tipsBanner)
  }

  buildElementHeights(newElements, view_mode, cardHeight, changeViewMode, subMenu, tipsBanner) {
    const { enviroment } = this.props
    let newElementHeights = [];
    let numberViewItems = newElements? newElements.length : 0;
    const haveRemindBannerData = this.props.Enviroment.remind.data !== 'unload' &&
      this.props.Enviroment.remind.data.length > 0;
    const needShowRemindBanner = enviroment !== 'photo-stream' &&
      enviroment !== 'nearby' &&
      haveRemindBannerData;
    // Element 0 : Change View Mode + Sub Menu + Tip Banner + Card
    const firstElementHeight = needShowRemindBanner?
      (changeViewMode + subMenu + tipsBanner + cardHeight) :
      (changeViewMode + subMenu + cardHeight)

    if (view_mode == 'LIST_VIEW') {
      numberViewItems = Math.round(numberViewItems / 2);
    }

    newElementHeights.push(firstElementHeight)
    for(let i = 1; i < numberViewItems; i++) {
      if (this.shouldRenderBanner(view_mode, i)) {
        // Tip banner + Card
        newElementHeights.push(tipsBanner + cardHeight);
      }
      else {
        newElementHeights.push(cardHeight);
      }
    }
    return newElementHeights;
  }

  closeFilterModal() {
    this.setState({ isOpen: false })
  }

  handleClick(event) {
    event.preventDefault()

    var location_ids = [];
    $.each($("input[name='location_ids']"), function(){
      location_ids.push($(this).val());
    });

    this.setState({value: location_ids.join(',')})

    const params = {
      'search_condition[age_min]': this.state.startAge + "",
      'search_condition[age_max]': this.state.endAge + "",
      'search_condition[location_ids]': location_ids.join(','),
      'search_condition[search_type]': 1
    }

    const ga_params = {
      'age_min': this.state.startAge + "",
      'age_max': this.state.endAge + "",
      'location_ids': location_ids.join(','),
      'search_type': 1
    }

    const ga_new_params = _.pickBy(ga_params);

    const ga_track_data = {
        search_condition: ga_new_params
    };
    this.props.gaSend(GA_ACTION_SIMPLE_FILTER_CONDITION, ga_track_data);
    this.props.simpleFilterClick(params, this.props.enviroment);
    this.closeFilterModal()
  }

  render() {
    const { search, enviroment, user_profile, Tips, location } = this.props
    const page_source = 'SEARCH_PAGE_' + enviroment.toUpperCase()
    const banners = this.state.shuffleTips;
    const { isFetching } = search
    const { total_elements, total_result, view_mode } = search[enviroment]
    const listViewActiveClass = (view_mode === 'LIST_VIEW') ? 'active' : ''
    const detailActiveClass = (view_mode === 'DETAIL_VIEW') ? 'active' : ''
    const harmonySearch = enviroment == 'harmony_age' || enviroment == 'harmony_fate' || enviroment == 'harmony_horoscope'
    const elementHeights = (view_mode === 'LIST_VIEW'? this.state.listView_ElementHeights : this.state.detailView_elementHeights)
    const containerBannerClass = enviroment ==='online' ? 'container' : 'le';
    const banner_list = this.state.banner_list
    let display_total_result = 0;
    if (location.pathname == "/search/default" || location.pathname == "/search/nearby") {
      display_total_result = total_result;
    } else {
      display_total_result = total_result * 3;
    }
    const NEW_NUMBER_OF_TIPS = this.state.shuffleTips.length
    return (
      <div>
        <div className="container mbm">
          <div className="row">
            <div className="col-xs-12 l-flex-ce">
              <select className={harmonySearch ? "form__input--e" : "hide form__input--e"} ref="harmony" name="harmony" onChange={this.handleChangeHarmony}>
                {enviroment === 'harmony_age' ? <option value="harmony_age" selected>Hợp tuổi</option> : <option value="harmony_age" >Hợp tuổi</option>}
                {enviroment === 'harmony_fate' ? <option value="harmony_fate" selected>Hợp mệnh</option> : <option value="harmony_fate" >Hợp mệnh</option>}
                {enviroment === 'harmony_horoscope' ? <option value="harmony_horoscope" selected>Chòm sao</option> : <option value="harmony_horoscope" >Chòm sao</option>}
              </select>

              {enviroment === 'default' &&
                <Link
                  to="/search-condition"
                  className="btn btn--p"
                  id="firstStep"
                >
                  Thiết lập
                </Link>
              }
              {isFetching ? (
                <div>
                  <span>{LOADING_DATA_TEXT}</span>
                </div>
              ) : (
                <div>
                  {display_total_result >= 0 && <span className="txt-blue txt-bold"> Kết quả {display_total_result}</span>}
                </div>
              )}
              {enviroment !== 'default' && <div className="simple-filter l-flex-item-grow txt-right-mc mrm" onClick={() => {this.setState({isOpen: true})}}>
                <i className="fa fa-sliders txt-blue fa-2x"></i>
              </div>}

              <div className="list-view">
                <button className={`btn btn--p ${listViewActiveClass}`} onClick={this.changeViewMode}>
                  <i className="fa fa-th-large" />
                </button>
                <button className={`btn btn--p ${detailActiveClass}`} onClick={this.changeViewMode}>
                  <i className="fa fa-th-list" />
                </button>
              </div>
            </div>
            <div className="col-xs-12">
              {(enviroment === 'new' || enviroment === 'online') &&
                <ListSameGenderBoostUsers location={this.props.location} />
              }
            </div>
          </div>
          <Modal
            transitionName="modal"
            isOpen={this.state.isOpen}
            onRequestClose={this.closeFilterModal}
            className="FilterModal modal__content"
            overlayClassName="modal__overlay modal__overlay--2"
            portalClassName="modal"
            contentLabel=""
          >
            <div>
              <div className="well well--e txt-center txt-blue txt-medium txt-lg mbm">Tiêu chí</div>
              <form action="/" ref="form" className="form" onSubmit={(event) => this.handleClick(event)}>
                <h3 className="form__sec-title l-flex-ce">
                  Tuổi
                  <span className="txt-right">{this.state.startAge} - {this.state.endAge}</span>
                </h3>
                <div className="row mbm">
                  <div className="col-xs-12">
                  <ReactSlider
                    ref="slider"
                    min={18}
                    max={70}
                    defaultValue={[this.state.startAge, this.state.endAge]}
                    className="horizontal-slider"
                    withBars
                    onAfterChange={() => {
                      let rangeAge = this.refs.slider.state.value;
                      this.setState({startAge: rangeAge[0]})
                      this.setState({endAge: rangeAge[1]})
                  }}/>
                  </div>
                </div>
                <h3 className="form__sec-title">Nơi ở hiện tại(Có thể chọn nhiều)</h3>
                <div className="row mbm">
                  <div className="col-xs-12">
                    <SelectMultiple
                      value={this.state.locationValues}
                      name="location_ids"
                      data={this.props.dictionaries.cities}
                      search={this.props.search}
                      type={1}
                      current_user={user_profile.current_user}
                      defaultSimpleFilter />
                  </div>
                </div>
                <div className="mbm">
                  <button
                    className="btn btn--p btn--b"
                    type="submit"
                  >
                    Đồng Ý
                  </button>
                </div>
              </form>
              <button className="modal__btn-close" onClick={this.closeFilterModal}>
                <i className="fa fa-times"></i>
              </button>
            </div>
          </Modal>
        </div>
        <div className="container">
          <div className="row">
          <Infinite
            elementHeight={elementHeights}
            infiniteLoadBeginEdgeOffset={
              (!isFetching && total_elements && total_elements >= total_result && this.state.elements[enviroment].length >= this.props.search[enviroment].total_result) ?
               undefined :
               this.state.infinite_loading
            }
            onInfiniteLoad={this.handleInfiniteLoad}
            loadingSpinnerDelegate={InfiniteLoad.elementInfiniteLoad()}
            isInfiniteLoading={this.state.isInfiniteLoading}
            useWindowAsScrollContainer
            ref={(rf) => { this.displayIndexStart = rf; }}
          >
            {(() => {
              switch (view_mode) {
                case 'LIST_VIEW':
                  if (!this.state.elements[enviroment].length) {
                    return ''
                  }
                  return _.chunk(this.state.elements[enviroment], 2).map((users, i) => {
                    const displayTipAfterItemNumber = DISPLAY_TIPS_AFTER_LIST_ITEM_NO / 2; // one row display 2 items
                    const bannerNo = (i / displayTipAfterItemNumber - 1) % NEW_NUMBER_OF_TIPS;
                    const coinBannerNo = (i / displayTipAfterItemNumber - 1) % banner_list.length;
                    return <div key={`ListItem-${i}`}>
                      {i > 0 && i % displayTipAfterItemNumber === 0 && <div className={containerBannerClass} key={`banner-${i}`}>
                          {
                            enviroment == 'new' &&
                            <CoinBanner
                              {...this.props}
                              current_user={this.props.user_profile.current_user}
                              banner={banner_list[coinBannerNo]}
                            />
                          }
                          {
                            enviroment !== 'new' &&
                            <Banner
                              url={banners[bannerNo].url}
                              iconNumber={banners[bannerNo].icon_number}
                              heading={banners[bannerNo].heading}
                              description={banners[bannerNo].html}
                            >
                              {banners[bannerNo].html}
                            </Banner>
                          }
                        </div>
                      }
                      <ListTwoItems
                        pageSource={page_source}
                        key={i}
                        index={i}
                        users={users}
                        current_user={user_profile.current_user}
                      />
                    </div>
                    }
                  )
                case 'DETAIL_VIEW':
                  if (!this.state.elements[enviroment].length) {
                    return ''
                  }
                  return this.state.elements[enviroment].map((user, i) => {
                    const displayTipAfterItemNumber = DISPLAY_TIPS_AFTER_DETAIL_ITEM_NO
                    const bannerNo = (i / displayTipAfterItemNumber - 1) % NEW_NUMBER_OF_TIPS;
                    const coinBannerNo = (i / displayTipAfterItemNumber - 1) % banner_list.length;
                    return <div key={`ListItem-${i}`}>
                      {i > 0 && i % displayTipAfterItemNumber === 0 && <div className={containerBannerClass} key={`banner-${i}`}>
                          {
                            enviroment == 'new' &&
                            <CoinBanner
                              {...this.props}
                              banner={banner_list[coinBannerNo]}
                            />
                          }
                          {
                            enviroment !== 'new' &&
                            <Banner
                              url={banners[bannerNo].url}
                              iconNumber={banners[bannerNo].icon_number}
                              heading={banners[bannerNo].heading}
                              description={banners[bannerNo].html}
                            >
                              {banners[bannerNo].html}
                            </Banner>
                          }
                        </div>
                      }
                      <DetailItem
                        pageSource={page_source}
                        key={i}
                        index={i}
                        data={user}
                        current_user={user_profile.current_user}
                      />
                    </div>
                    }
                  )
                default: return ''
              }
            })()}
          </Infinite>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <div className="well txt-center">
                <p>
                  {(() => {
                    if (
                      !isFetching &&
                      !this.state.isInfiniteLoading && total_result >= 0 &&
                      (
                        total_elements >= total_result ||
                        this.state.elements[enviroment].length >= total_result
                      )
                    ) {
                      return 'Bạn đã đến cuối danh sách'
                    }
                    if(enviroment == 'default' && search[enviroment].total_result === 0) {
                      return 'Không có kết quả!'
                    }
                    return ''
                  })()}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

InfiniteLoad.propTypes = {
  search: PropTypes.object,
  user_profile: PropTypes.object,
  Enviroment: PropTypes.object,
  updateScrollPossition: PropTypes.func,
  updateViewMode: PropTypes.func,
  updatePagingInformation: PropTypes.func,
  updatePreviousPage: PropTypes.func,
  searchUser: PropTypes.func,
  updateElementHeight: PropTypes.func,
  setProfileUserNextIndex: PropTypes.func,
  simpleFilterClick: PropTypes.func,
  gaSend: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    gaSend: (eventAction, ga_track_data) => {
      dispatch(gaSend(eventAction, ga_track_data))
    },
  }
}

const InfiniteLoadContainer = connect(mapStateToProps, mapDispachToProps)(InfiniteLoad);

export default InfiniteLoadContainer
