import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import _ from 'lodash'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import RandomMatchModal from '../modals/RandomMatchModal'
import { getRandomMatch, updateModalState, clearModalData } from '../../actions/random_match'
import { CDN_URL, GA_ACTION_RANDOM_MATCH_CLICK, RANDOM_MATCH_BANNER, RANDOM_MATCH_COST, GA_ACTION_RANDOM_MATCH_CLICK_BANNER, RANDOM_MATCH } from '../../constants/Enviroment'
import { changeBannerStatus } from '../../actions/Enviroment'
import { GHOST_USER } from '../../constants/userProfile';

class RandomMatchBanner extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isModalOpen: false,
    }
    this.handleClick = this.handleClick.bind(this)
    this.showModal = this.showModal.bind(this)
    this.hideModal = this.hideModal.bind(this)
  }

  componentWillMount() {
    if(this.props.random_match.data.length == 0) {
      this.props.getRandomMatch()
    }
  }

  showModal() {
    this.setState({ isModalOpen: true })
  }

  componentDidUpdate() {
    const coins = this.props.user_profile.current_user.coins
    const data = this.props.random_match.data
    const hiddenCard = this.props.random_match.hiddenCard
    const { random_match_free_gift } = this.props.user_profile.lucky_spinner_gifts;
    const user_free_random_match = !_.isEmpty(this.props.current_user) ? this.props.current_user.permission.number_random_match_free : 0
    const number_random_match_free = random_match_free_gift + user_free_random_match;
    if (number_random_match_free <= 0 && coins < RANDOM_MATCH_COST && data.length > 0 && hiddenCard && !this.props.random_match.isUpdateRandomMatch) {
      this.props.clearModalData()
    }
  }

  hideModal() {
    const state = !this.props.random_match.hiddenCard
    const coins = this.props.user_profile.current_user.coins
    const { random_match_free_gift } = this.props.user_profile.lucky_spinner_gifts;
    const user_free_random_match = !_.isEmpty(this.props.current_user) ? this.props.current_user.permission.number_random_match_free : 0
    const number_random_match_free = random_match_free_gift + user_free_random_match;
    this.setState({ isModalOpen: false })
    if (number_random_match_free > 0 || coins >= RANDOM_MATCH_COST) {
      this.props.getRandomMatch()
    }
    else {
      this.props.clearModalData()
    }
    this.props.updateModalState(true) //hidden card when close popup
  }

  componentWillUnmount() {
    this.props.updateModalState(true)
    if (this.props.random_match.isUpdateRandomMatch) {
      this.props.clearModalData() //clear data when user view other profile from random match modal
    }
  }

  handleClick() {
    this.showModal()
    this.props.gaSend(GA_ACTION_RANDOM_MATCH_CLICK_BANNER, { page_source: 'Random Match Banner' })
  }

  render() {
    const data = this.props.random_match.data;
    const { current_user, updateModalState, inCampaign, gaSend } = this.props;
    if (current_user.user_status === GHOST_USER) {
      return <div />
    }
    return (
      <div>
        <div onClick={this.handleClick} className="banner mbm">
          <div className="banner__content">
            <span className="banner__heading banner__heading--1 txt-blue">
              {
                'Chọn ghép đôi ngẫu nhiên'
              }
            </span>
            <div className="banner__sub-heading banner__sub-heading--1 txt-bold">
              {(() => {
                if (this.props.inCampaign) {
                  return (
                    <div>Mỗi ngày nhận ngay 1 Ghép đôi ngẫu nhiên miễn phí!</div>
                  )
                }
                return (
                  <div>Tâm sự với người lạ trong <strong className="zoom">24</strong> giờ!</div>
                )
              })()}
            </div>
          </div>
          <img src={`${CDN_URL}/general/icon+random+match+.png`} alt="icon+random+match+.png" className="banner__icon"/>
          <div className="banner__meta txt-blue">
            <i className="fa fa-chevron-right"></i>
          </div>
        </div>
        <RandomMatchModal
          current_user={current_user}
          isOpen={this.state.isModalOpen}
          gaSend={gaSend}
          onRequestClose={this.hideModal}
          data={data}
          updateModalState={updateModalState}
          inCampaign={inCampaign}
        />
      </div>
    )
  }
}

RandomMatchBanner.propTypes = {
  updatePreviousPage: PropTypes.func,
  gaSend: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    getRandomMatch: () => {
      dispatch(getRandomMatch())
    },
    updateModalState: (state) => {
      dispatch(updateModalState(state))
    },
    clearModalData: () => {
      dispatch(clearModalData())
    },
    changeBannerStatus: (banner) => {
      dispatch(changeBannerStatus(banner))
    }
  }
}

const RandomMatchBannerContainer = connect(mapStateToProps, mapDispatchToProps)(RandomMatchBanner);

export default RandomMatchBannerContainer
