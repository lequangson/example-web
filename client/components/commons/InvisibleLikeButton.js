import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import q from 'q';
import $ from 'jquery';
import { hiddenLikeRequest, hiddenLike } from 'pages/people-liked-me/Actions';
import { HIDDEN_LIKE_BUTTON } from 'constants/TextDefinition';

class InvisibleLikeButton extends React.Component {

  constructor(props) {
    super(props)
  }

  getPageSource() {
    if (this.props.page_source && this.props.page_source !== '') {
      return this.props.page_source;
    }
    if (this.props.Enviroment.view_profile_source !== '') {
      return this.props.Enviroment.view_profile_source;
    }
    return this.props.Enviroment.previous_page;
  }

  isMatchMade() {
    if (
      this.props.user.have_sent_like &&
      this.props.user.have_sent_like_to_me
    ) {
      return true;
    }
    return false;
  }

  cardIngore() {
    var $card = $(".site-content").find('.card.front');

    var swipe = new TimelineMax({repeat:0, yoyo:false, repeatDelay:0, onComplete: this.remove(), onCompleteParams:[$card]});
    swipe.staggerTo($card, 0.8, {bezier:[{left:"+=-350", top:"+=300", rotation:"-60"}], ease:Power1.easeInOut});
  }

  remove(card) {
    $(card).remove();
  }

  renderButton() {
    const { user } = this.props
    return (
      <button
        className="btn btn--b txt-dark"
        onClick={
          () => {
            this.cardIngore();
            setTimeout(() => {
              this.props.hiddenLike(user.identifier)
            }, 800)
          }
        }
      >
        { HIDDEN_LIKE_BUTTON }
      </button>
    )
  }

  render() {
    return (
      !this.isMatchMade() ? this.renderButton() : <div></div>
    )
  }
}

InvisibleLikeButton.propTypes = {
  user: PropTypes.object,
  page_source: PropTypes.string,
  Enviroment: PropTypes.object,
  hiddenLike: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    hiddenLike: (identifier) => {
      const defer = q.defer()
      dispatch(hiddenLikeRequest())
      dispatch(hiddenLike(identifier)).then((response) => {
        if (typeof response !== 'undefined') {
          defer.resolve(response)
        }
      }).catch((err) => {
        defer.reject(err)
      });
      return defer.promise
    },
  }
}
const HiddenLikeContainer = connect(mapStateToProps, mapDispachToProps)(InvisibleLikeButton);

export default HiddenLikeContainer;
