import * as React from "react";

class ImageQueue {
    static get QUEUE_SIZE(){ return 8 }; // maximum loading images at the same time
    constructor(){
        // list of waiting images to be loaded
        this.fifos = {
            small: new FIFO(), large: new FIFO()
        }
        this.loadings = {
            small: new Map(), large: new Map()
        }
        this.entries = {};
        this.onLoaded = this.onLoaded.bind(this);
        this.schedule = this.schedule.bind(this);
    }

    uuid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
    
    push(size, img, src){
        img.uuid = this.uuid();
        const promise = new Promise(resolve => {
            const entry = new QueueEntry(size, img, src, resolve); // it will be resolved in QueueEntry, when image is loaded
            const fifo = this.fifos[size];
            fifo.push(entry);
            this.entries[img.uuid] = entry;
            this.schedule();
            
        }).then(this.onLoaded).catch(this.onLoaded);
        return promise;
    }

    cancel(img){
        const entries = this.entries;
        const entry = entries[img.uuid];
        if(entry){
            this.fifos[entry.size].deleteByUUID(img.uuid);
            this.loadings[entry.size].delete(img.uuid);
            entry.cancel(); // cancel has to be loaded after deleteByUUID (because img.uuid is cleaned)
        }
        delete entries[img.uuid];
        img.uuid = null;
        setTimeout(()=>this.schedule(), 0);
    }

    schedule(){
        const entry = this.shiftAppropriateEntry();
        if(entry && entry.img ){
            entry.start();
            this.loadings[entry.size].set(entry.img.uuid, entry);
        }else if(entry){ // if entry exists but is already canceled, we schedule next
            setTimeout(()=>this.schedule(), 0);
        }
    }

    shiftAppropriateEntry(){
        if( this.loadings.small.size + this.loadings.large.size > ImageQueue.QUEUE_SIZE ) {
            return null;
        }
        
        let size = this.chooseSize();
        let entry = this.fifos[size].shift();
        if(!entry){
            size = this.theOtherSize(size);
            entry = this.fifos[size].shift();
        }
        if(!entry){
            return; // no waiting anymore
        }
        return entry
    }

    chooseSize(){
        if(this.loadings.small.size < this.loadings.large.size){ // try to balance
            return "small";
        }else{
            return "large";
        }
    }

    theOtherSize(size){
        if( size === "small"){
            return "large";
        }else if( size === "large"){
            return "small";
        }else {
            console.error("unknown size", size);
            return null;
        }
    }

    onLoaded(entry){
        const loading = this.loadings[entry.size];
        if( loading ){
            loading.delete(entry.img.uuid);
            delete entries[entry.img.uuid];
        }
        this.schedule();
    }

}

class QueueEntry {
    constructor(size, img, src, resolve){
        this.size = size;
        this.img = img;
        this.src = src;
        this.resolve = resolve;
        this.status = "initialized";
    }

    start(){
        if( this.status === "canceled" ){
            return;
        }
        this.status = "loading";
        this.onLoaded = this.onLoaded.bind(this)
        this.img.addEventListener("load", this.onLoaded, false);
        this.img.src = this.src; // by this browser starts loading
    }

    
    cancel(){
        this.status = "canceled";
        this.img.src = ""; // I read it will cancel loading, but it did not :( anyway I will keep this code for future improvement of browser.
        this.img.removeEventListener("load", this.onLoaded, false);
        this.img = null;
    }

    // private
    onLoaded(){
        // no matter what happens it call resolve
        this.resolve(this);
        this.status = "loaded";
    }

}

class FIFONode {
    constructor( next, value ){
        this.next = next;
        this.prev = null;
        this.value = value;
    }
}

class FIFO {
    constructor(){
        this.head = null;
        this.tail = null;
        this.length = 0;
        this.mapByUUID = new Map();
    }

    
    push(e){
        const node = new FIFONode( this.tail, e );
        if( this.mapByUUID.get(e.img.uuid) ){ // avoid duplicate
            this.deleteByUUID(e.img.uuid);
        } 
        this.mapByUUID.set( e.img.uuid, node );
        if( !this.head ){
            this.head = node;
        }
        if( this.tail ){
            this.tail.prev = node;
        }
        this.tail = node;
        this.length = this.length + 1;
    }

    shift(){
        const node = this.head;
        if( node ){
            const new_head = node.prev;
            if(new_head){
                new_head.next = null;
            }
            this.head = new_head;
            this.length = this.length - 1;
            const entry = node.value;
            if(entry && entry.img ){
                this.mapByUUID.delete( entry.img.uuid );
            }

            return entry;
        }else{
            return null;
        }
    }

    deleteByUUID(uuid){
        const node = this.mapByUUID.get(uuid);
        if( node ){
            if( node.next ){
                node.next.prev = node.prev;
            }
            if( node.prev ){
                node.prev.next = node.next;
            }
            if( node.next == null ) { // head
                this.head = node.prev;
            }
            if( node.prev == null ){ // tail
                this.tail = node.next;
            }
            node.next = node.prev = null;
            this.mapByUUID.delete(uuid);
            return node.value;
        }else{
            return null;
        }
    }
    
}

const queue = new ImageQueue();

export class ProgressiveImage extends React.Component {
    static defaultProps = {
        transitionTime: 500,
        timingFunction: "ease",
        initialBlur: 10,
        emptyGif: "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==",
    };

    loadImage(){
        const {src, preview} = this.props;

        queue.push("small", this.small, preview).then(() => this.setState({smallLoaded: true}));
        queue.push("large", this.large, src).then(() => this.setState({largeLoaded: true}));
    }

    cancelLoading(){
        queue.cancel(this.small);
        queue.cancel(this.large);
    }

    componentWillMount() {
        this.setState( {smallLoaded: false, largeLoaded: false } );

        // load large and small images at the same time but large is the first.
        // By this way, we can prioritize large images higher than small images in the queue.
        // And if both are already loaded, then largeLoaded is flagged earlier than smallLoaded so the large image is displayed first.
        if(!this.large){ this.large = new Image(); }
        if(!this.small){ this.small = new Image(); }
        this.loadImage(); 
    }
    
    componentWillUnmount(){
        this.cancelLoading();
    }

    // we don't want to be affected by render function's change
    shouldComponentUpdate(nextProps, nextState){
        return this.props.src !== nextProps.src ||
            this.props.preview !== nextProps.preview ||
            this.state.largeLoaded !== nextState.largeLoaded ||
            this.state.smallLoaded !== nextState.smallLoaded;
    }
    
    render() {
        const src = this.state.largeLoaded ? this.props.src : ( this.state.smallLoaded ? this.props.preview : this.props.emptyGif);
        const {render} = this.props;
        return render(src, this.getStyle());
    }

    getStyle() {
        const {transitionTime, timingFunction} = this.props;
        let blur = (this.state.largeLoaded) ? 0 : 10;
        return {
            filter: `blur(${blur}px)`,
            transition: `filter ${transitionTime}ms ${timingFunction}`
        };
    }
}

export default { ProgressiveImage }
