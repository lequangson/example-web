import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import Modal from 'react-modal'
import BoostRankModal from '../modals/BoostRankModal'
import { GA_ACTION_BOOST_RANK_CLICK, BOOST_RANK_BANNER, BOOST_RANK, CDN_URL } from '../../constants/Enviroment'


class BoostRankBanner extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isModalOpen: false,
    }
    this.handleClick = this.handleClick.bind(this)
    this.showModal = this.showModal.bind(this)
    this.hideModal = this.hideModal.bind(this)
  }

  showModal() {
    this.setState({ isModalOpen: true })
  }

  hideModal() {
    this.setState({ isModalOpen: false })
  }

  handleClick() {
    this.showModal()
    this.props.gaSend(GA_ACTION_BOOST_RANK_CLICK, { page_source: 'Boost Rank Banner' })
  }

  render() {
    return (
      <div className="container">
        <div onClick={this.handleClick} className="banner mbm">
          <div className="banner__content">
            <span className="banner__heading banner__heading--1 txt-blue">Nổi bật giữa hàng ngàn người</span>
            <p className="banner__sub-heading banner__sub-heading--1 txt-bold">
              Sáng nhất đêm nay chỉ với <strong className="zoom">30</strong> xu!
            </p>
          </div>
          <img src={`${CDN_URL}/general/icon+boost+in+rank+.png`} alt="icon+boost+in+rank+.png" className="banner__icon"/>
          <div className="banner__meta txt-blue">
            <i className="fa fa-chevron-right"></i>
          </div>
        </div>
        <BoostRankModal
          current_user={this.props.current_user}
          isOpen={this.state.isModalOpen}
          gaSend={this.props.gaSend}
          onRequestClose={this.hideModal}
        />
      </div>
    )
  }
}

BoostRankBanner.propTypes = {
  updatePreviousPage: PropTypes.func,
  gaSend: PropTypes.func,
}

export default BoostRankBanner
