import React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';
import 'public/styles/react-select.css';

class SelectMultiple extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      options: [],
      value: [],
      name: '',
    }
    this.handleSelectChange = this.handleSelectChange.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    const { name, search, type, reset } = nextProps;
    const conditionType0 = this.props.search.condition[0] || {} ;
    const nextConditionType0 = search.condition[0] || {};

    if (conditionType0[name] != nextConditionType0[name]) {
      this.setState({
        value:  nextConditionType0[name]
      })
    }
    if (reset) {
      this.setState({
        value: []
      })
    }
  }

  componentWillMount() {
    const { data, name, search, type, defaultSimpleFilter } = this.props;
    const obj = data.map(d => ({ "label": d.value, "value": d.key.toString() }));
    const condition = search.condition.find((el) => el.search_type == type);
    let selectedLocation = condition[name];

    if (selectedLocation == '' && defaultSimpleFilter) {
      const simpleFilter = search.condition[1];
      selectedLocation = !simpleFilter.age_min ? this.props.current_user.residence.key : '';
    }
    this.setState({
      options: obj,
      name,
      value: selectedLocation ? selectedLocation.toString() : ''
    })
  }

  handleSelectChange(value) {
    this.setState({ value });
  }

  render() {
    return (
      <Select
        name={this.state.name}
        autoBlur={false}
        multi
        searchable={false}
        simpleValue
        value={this.state.value}
        placeholder={this.props.placeholder || "Bất kỳ"}
        options={this.state.options}
        onChange={this.handleSelectChange}
      />
    );
  }
}

SelectMultiple.propTypes = {
  data: React.PropTypes.array,
  name: React.PropTypes.string,
  search: React.PropTypes.object,
  placeholder: React.PropTypes.string,
  current_user: React.PropTypes.object
}

export default SelectMultiple;