import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import BestPhotoModal from '../modals/BestPhotoModal';
import {
  updateBestVotePhotoState
} from 'actions/photoStream';

class BestPhotoBanner extends React.Component {
	constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
    }
    this.handleClick = this.handleClick.bind(this);
    this.hideModal = this.hideModal.bind(this)
  }

	handleClick(modalInfo){
		this.props.updateBestVotePhotoState(modalInfo);
    this.setState({ isOpen: true })
	}

  hideModal() {
    this.setState({ isOpen: false })
  }

  renderBannerName() {
    const banner = this.props.banner || []
    if (banner.gender === 'male' && !banner.picture_url) {
      return 'Bạn nam nhiều vote nhất tuần'
    }
    if (banner.gender === 'female' && !banner.picture_url) {
      return 'Bạn nữ nhiều vote nhất tuần'
    }
    if (banner.gender === 'male' && banner.picture_url) {
      return '[Nam] Ảnh nhiều vote nhất tuần'
    }
    if (banner.gender === 'female' && banner.picture_url) {
      return '[Nữ] Ảnh nhiều vote nhất tuần'
    }
  }

  render() {
    const { BestVotePhoto } = this.props;
    const { banners, banners_image } = BestVotePhoto;
    const banner = this.props.banner || []
    const picture = banner.user_pictures ? banner.user_pictures.filter(pic => pic.is_main === true).length > 0 ? banner.user_pictures.filter(pic => pic.is_main === true)[0] : banner.user_pictures[0] : {large_image_url: banner.picture_url}

    return (
	    <div>
	      <div onClick={ () => this.handleClick(banner)} className="banner mbm">
	        <div className="frame frame--xsm frame--1">
            <img src={picture ? picture.large_image_url : ""} />
	        </div>
	        <div className="banner__content">
	          <h3 className="banner__heading txt-blue">{this.renderBannerName()}</h3>
            {
              banner.picture_url ?
              <p className="banner__sub-heading">{banner.received_votes} vote</p>
              :
              <p className="banner__sub-heading">{banner.nick_name} / {banner.number_photos} ảnh / {banner.received_votes} vote</p>
            }
	        </div>
	        <img src={banner.gender === 'male' ? banners_image[0] : banners_image[1]} alt="" className="banner__icon" />
	      </div>
        <BestPhotoModal
          isOpen={this.state.isOpen}
          onRequestClose={this.hideModal}
        />
	    </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    BestVotePhoto: state.BestVotePhoto, 
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateBestVotePhotoState: (modalInfo) => {
      dispatch(updateBestVotePhotoState(modalInfo))
    },
  }
}

const BestPhotoBannerContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BestPhotoBanner);

export default BestPhotoBannerContainer;
