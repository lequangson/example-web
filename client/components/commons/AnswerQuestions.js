import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import {
  ANSWER_MY_EXPLORER_QUESTION, ANSWER_OTHER_EXPLORER_QUESTION
} from '../../constants/Match_Questions'
import { CDN_URL } from '../../constants/Enviroment.js'
import $ from 'jquery'
import { compareIdentifiers } from '../../utils/common'
import { updateMatchQuestionState } from '../../actions/Match_Questions';
import socket from "../../utils/socket";
import { ACTIVE_USER } from 'constants/userProfile';

class AnswerQuestions extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentQuestionIndex: 0,
    }
    this.next = this.next.bind(this)
    this.prev = this.prev.bind(this)
    this.answerQuestion = this.answerQuestion.bind(this)
    this.completeAnswer = this.completeAnswer.bind(this)
  }

  next() {
    const totalQuestion = this.props.Mode === ANSWER_MY_EXPLORER_QUESTION?
      this.props.Match_Questions.my_questions.length :
      this.props.Match_Questions.other_questions.length;
    if (this.state.currentQuestionIndex >= totalQuestion - 1) {
      return
    }
    this.setState({ currentQuestionIndex: this.state.currentQuestionIndex + 1 })
  }

  showResultNoti(matched_answers) {
    const text_female = ['Hừm! Tìm hiểu thêm', 'Hừm! Tìm hiểu thêm', 'Không sao! Hãy thích cô ấy', 'Quá tốt! Thích cô ấy đi', 'Wow! Quá đỉnh Làm quen bạn êi']
    const text_male = ['Hừm! Tìm hiểu thêm', 'Hừm! Tìm hiểu thêm', 'Không sao! Hãy thích anh ấy', 'Quá tốt! Thích anh ấy đi', 'Wow! Quá đỉnh Làm quen bạn êi']
    const text = this.props.user.gender == 'male' ? text_male : text_female
    const result_url = `${CDN_URL}/general/result-question-noti.png`
    const resultNoti = $(`
      <div class="noti noti--bg-red mbm txt-bold">
        <div class="noti__body txt-sm">
          <div class="mbm"> Đúng ${matched_answers}/5 </div>
          <div class="noti__header mbm">
            <img src= ${result_url} alt="result-question-noti" />
          </div>
          <div class="noti__content">${text[matched_answers]}</div>
        </div>
      </div>
    `)

    setTimeout(function() {
      $('body').append(resultNoti)
    }, 200)

    setTimeout(function() {
      $(resultNoti).remove()
    }, 7000)
  }

  prev() {
    if (this.state.currentQuestionIndex <= 0) {
      return
    }
    this.setState({ currentQuestionIndex: this.state.currentQuestionIndex - 1 })
  }

  renderDescription() {
    const { current_user } = this.props.user_profile
    const gender = current_user.gender ? current_user.gender : 'male'
    if (this.props.Mode === ANSWER_MY_EXPLORER_QUESTION) {
      return ''
    }
    return (
        <div className="mbm txt-center">Bạn cần trả lời 5 câu hỏi mà {gender === 'male' ? 'cô' : 'anh' } ấy đã chọn. Chính xác cả 5 câu là bạn đã vượt rào thành công. Hai bạn sẽ được ghép đôi ngay lập tức!</div>
    )
  }

  componentWillUnmount() {
    const totalQuestion = this.props.Match_Questions.other_questions.filter(q => q.answer_code !== null).length;
    if (totalQuestion == 5 && this.props.Mode === ANSWER_OTHER_EXPLORER_QUESTION && this.props.Match_Questions.matched_answers == 0) {
      this.completeAnswer(this.props.user.identifier)
    }
  }

  renderLastQuestion() {
    const { current_user } = this.props.user_profile
    const gender = current_user.gender ? current_user.gender : 'male'
    const contentText = (gender === 'male') ? 'cô' : 'anh'
    return (
      `Hoàn toàn không phải ${contentText} ấy`
    )
  }

  completeAnswer(identifier) {
    if (this.props.Mode === ANSWER_MY_EXPLORER_QUESTION) {
      this.props.getCurrentUser()
    }
    else {
      const { user_profile } = this.props
      const { current_user} = user_profile;

      this.props.getMatchedAnsweredQuestion(identifier).then((response) => {
        const matched_answers = response.response.data.matched_answers
        if (matched_answers < 5) {
          if (matched_answers > 1 && current_user.user_status == ACTIVE_USER) {
            socket.emit('send_event',
              {
                room: identifier,
                type: 'match_question_hurdle',
                to_identifier: identifier,
                matched_answers: matched_answers,
              }
            )
          }
          this.showResultNoti(matched_answers)
        }
        else {
          // socket.emit('send_event', {type: 'match_question_match', user_info: this.props.user_profile.current_user, to_identifier: identifier});
          if (current_user.user_status == ACTIVE_USER) {
            socket.emit('send_event',
              {
                room: identifier,
                type: 'match_question_hurdle',
                to_identifier: identifier,
                matched_answers: 5,
              }
            )
          }

          this.props.updateMatchQuestionState('isOpenMatchModal', true);
          this.props.updateMatchQuestionState('match_info', this.props.user);
          //this.props.getPeopleMatchedMe();
          this.props.user.have_sent_like = true;
          this.props.user.have_sent_like_to_me = true;
        }
        this.props.getUserQuestions(identifier)
        // this.props.closeModal()
      }).catch(e => {
        console.log('e', e);
      })
    }
    // handle send socket to match, hurdle here
  }

  answerQuestion(e, question_id, identifier) {
    if (this.props.Mode === ANSWER_MY_EXPLORER_QUESTION) {
      this.props.answerQuestion(0, question_id, e.target.value)
    }
    else {
      this.props.answerQuestion(1, question_id, e.target.value, identifier)
    }
  }

  renderTextButton() {
    if (this.props.Mode === ANSWER_MY_EXPLORER_QUESTION) {
      return 'Hoàn thành'
    }
    else {
      return 'Gửi câu trả lời'
    }
  }

  render() {
    const title = this.props.Mode === ANSWER_MY_EXPLORER_QUESTION?
      'Khám phá bản thân' :
      'Thử thách vượt rào';
    const AnswerQuestionsCallback = this.props.Mode === ANSWER_MY_EXPLORER_QUESTION?
      this.props.answerMyQuestionCallback : this.props.answerOtherQuestionCallback

    const currentQuestion = this.props.Mode === ANSWER_MY_EXPLORER_QUESTION?
      this.props.Match_Questions.my_questions[this.state.currentQuestionIndex] :
      this.props.Match_Questions.other_questions[this.state.currentQuestionIndex];

    const totalQuestion = this.props.Mode === ANSWER_MY_EXPLORER_QUESTION?
      this.props.Match_Questions.my_questions.length :
      this.props.Match_Questions.other_questions.length;
    const total_answered = this.props.Mode === ANSWER_MY_EXPLORER_QUESTION?
      this.props.Match_Questions.my_questions.filter(q => q.answer_code !== null).length:
      this.props.Match_Questions.other_questions.filter(q => q.answer_code !== null).length

    const customClass =  this.props.Mode === ANSWER_MY_EXPLORER_QUESTION?
      'modal__txt-content' :
      'modal__txt-content modal__txt-content--bg-blueLight padding-t10';

    return (
      <div className="padding-t10">
        <div className="padding-rl10">
          <div className="well well--e txt-blue txt-bold txt-center mbm">{title}</div>
          {this.renderDescription()}
        </div>
        <div className={customClass}>
          <div className="mbm l-flex-ce">
            <button className="btn btn--5" style={this.state.currentQuestionIndex <= 0 ? {visibility: 'hidden'} : {}} onClick={this.prev}>
              <i className="fa fa-chevron-left"></i> Trước
            </button>
            <span className="txt-bold txt-blue txt-center">Câu {this.state.currentQuestionIndex + 1}/{totalQuestion}</span>
            <button className="btn btn--5" style={this.state.currentQuestionIndex === totalQuestion - 1 ? {visibility: 'hidden'} : {}} onClick={this.next} >
              Tiếp <i className="fa fa-chevron-right"></i>
            </button>
          </div>
          <div className="mbm txt-center txt-blue txt-bold txt-boafd" style={{ minHfaeight: '110px' }}>
            {currentQuestion.title}
          </div>
          {this.props.Match_Questions.answers.map((answer, i) => (
            <label className="form__input--c mbs" key={`${currentQuestion.answer_code}_${i}`}>
              <input
                type="radio"
                onChange={(e) => {this.answerQuestion(e, currentQuestion.id, this.props.user.identifier)}}
                ref="answer" name={`question ${this.state.currentQuestionIndex}`}
                value={answer.code}
                checked={currentQuestion.answer_code == answer.code }
              />
              {(this.props.Mode == ANSWER_OTHER_EXPLORER_QUESTION && answer.code == 5 ) ?
              this.renderLastQuestion()
              :
              answer.title
              }
              <div className="fake-radio"></div>
            </label>
            )
          )}
          <div className="mbs">
            <button
              className="btn btn--b btn--p"
              disabled={this.props.Mode == ANSWER_MY_EXPLORER_QUESTION ? !(total_answered == 20) : !(total_answered == 5)}
              onClick={() => this.completeAnswer(this.props.user.identifier)}
            >
              {this.renderTextButton()}
            </button>
          </div>
        </div>
      </div>
    )
  }
}

AnswerQuestions.propTypes = {
  Mode: PropTypes.string.isRequired,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
    return {
      updateMatchQuestionState: (key, value) => dispatch(updateMatchQuestionState(key, value)),
      /*getPeopleMatchedMe: () => {
        dispatch(getPeopleMatchedMe())
      },*/
    }
}
export default connect(mapStateToProps, mapDispachToProps)(AnswerQuestions);