import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import q from 'q'
import UserPicture from './UserPicture'
import LikeButton from './LikeButton'
import StartChatButton from './StartChatButton'
import { gaSend, updatePreviousPage } from '../../actions/Enviroment'
import {
  VOTE_ICON, MY_PROFILE_PAGE, VOTED_MODAL_PAGE,
  GA_ACTION_VOTE_PHOTO, GA_ACTION_UNVOTE_PHOTO,
} from '../../constants/Enviroment'
import {
  votePhoto, getVotedUsers,
} from '../../actions/photoStream'
import { VOTE_BUTTON_VOTE, VOTE_BUTTON_VIEW, AVATAR_PICTURE, OTHER_PICTURE, VOTE, UNVOTE } from '../../constants/userProfile'
import { compareIdentifiers, getUserIdFromIdentifier } from '../../utils/common'
import GoogleOptimize from '../../utils/google_analytic/google_optimize';
import socket from "../../utils/socket";
import { ACTIVE_USER } from 'constants/userProfile';

class VoteButton extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      identifier: '',
      isModalOpen: false,
      user: {},
      type: '',
      isDisabled: false,
    }
    this.showModal = this.showModal.bind(this)
    this.hideModal = this.hideModal.bind(this)
  }

  componentWillMount() {
    const type = this.props.type
    this.setState({ type })
  }

  @GoogleOptimize
  componentDidUpdate(){}

  getPageSource() {
    if (this.props.page_source && this.props.page_source !== '') {
      return this.props.page_source;
    }
    if (this.props.Enviroment.view_profile_source !== '') {
      return this.props.Enviroment.view_profile_source;
    }
    return this.props.Enviroment.previous_page;
  }

  handleVote(type) {
    const current_user_id = getUserIdFromIdentifier(this.props.user_profile.current_user.identifier)
    const current_user = this.props.user_profile.current_user;
    const page_source = this.getPageSource()
    const picture_type = this.props.picture.is_main ? AVATAR_PICTURE : OTHER_PICTURE
    const picture_id = this.props.picture.id
    this.setState({ isDisabled: true })
    this.props.votePhoto(picture_id, picture_type, type, current_user_id, page_source, this.props.target_user_id, current_user).then(() => {
      this.sendGoogleAnalytic(type === VOTE ? GA_ACTION_VOTE_PHOTO : GA_ACTION_UNVOTE_PHOTO, page_source)
      if (type === VOTE) {
        this.props.picture.voted_user_ids = this.props.picture.voted_user_ids.length > 0 ?
          this.props.picture.voted_user_ids.concat(',', current_user_id) :
          this.props.picture.voted_user_ids.concat(current_user_id);
      } else {
        this.props.picture.voted_user_ids = this.props.picture.voted_user_ids.split(',').length > 1 ?
          this.props.picture.voted_user_ids.split(',').filter(id => id != current_user_id).join(',') :
          this.props.picture.voted_user_ids.replace(current_user_id, '');
      }
      this.setState({
        isDisabled: false,
      })
    }).catch(() => {
      this.setState({
        isDisabled: false,
      })
    })
  }

  sendGoogleAnalytic(gaAction, page_source) {
    const ga_track_data = {
      page_source: page_source,
    };
    this.props.gaSend(gaAction, ga_track_data);
  }

  nextUser_OnClick(userIndex, stepIndex) {
    const user = this.props.photoStream.voted_users[userIndex + stepIndex]
    this.setState({ user })
  }

  showModal() {
    const picture_type = this.props.picture.is_main ? AVATAR_PICTURE : OTHER_PICTURE
    const picture_id = this.props.picture.id
    this.props.getVotedUsers(picture_type, picture_id).then(() => {
      this.props.updatePreviousPage(VOTED_MODAL_PAGE)
      this.setState({ isModalOpen: true, user: this.props.photoStream.voted_users[0] })
    })
  }

  hideModal() {
    this.setState({ isModalOpen: false })
  }

  renderVoteButton() {
    const current_user = this.props.user_profile.current_user
    const { picture } = this.props
    const number_people_liked = picture.voted_user_ids ? picture.voted_user_ids.split(',').filter((v, i, a) => a.indexOf(v) === i).length : 0
    const voted = (picture.voted_user_ids && picture.voted_user_ids.length > 0) ? picture.voted_user_ids.split(',').findIndex(user => user == getUserIdFromIdentifier(current_user.identifier)) > -1 : false
    if (this.state.type === VOTE_BUTTON_VIEW) {
      if (number_people_liked > 0) {
        return (
          <button
            onClick={() => this.showModal()}
            className="btn btn--didvote"
            disabled={this.state.isDisabled}
          >
            <img className="img-like" src={VOTE_ICON} alt="vote-icon" />
            {number_people_liked} người vote
          </button>
        )
      }
      else {
        return (
          ''
        )
      }
    }
    else {
      if (this.state.type === VOTE_BUTTON_VOTE && !voted) {
        return (
          <button
            onClick={() => this.handleVote(VOTE)}
            className="btn btn--vote"
            disabled={this.state.isDisabled}
          >
            <img className="img-like" src={VOTE_ICON} alt="vote-icon" />
            {number_people_liked}
          </button>
        )
      }
      else {
        return (
          <button
            onClick={() => this.handleVote(UNVOTE)}
            className="btn btn--didvote"
            disabled={this.state.isDisabled}
          >
            <img className="img-like" src={VOTE_ICON} alt="vote-icon" />
            {number_people_liked}
          </button>
        )
      }
    }
  }

  render() {
    const current_user = this.props.user_profile.current_user
    const { target_user_id, picture, type } = this.props
    const number_people_liked = picture.voted_user_ids ? picture.voted_user_ids.split(',').filter((v, i, a) => a.indexOf(v) === i).length : 0
    const userIndex = (this.state.user && this.props.photoStream.voted_users) ? this.props.photoStream.voted_users.findIndex(user => compareIdentifiers(user.identifier, this.state.user.identifier)) : ''
    const nextUser = (typeof userIndex !== 'undefined' && this.props.photoStream.voted_users) ? this.props.photoStream.voted_users[userIndex + 1] : ''
    const prevUser = (typeof userIndex !== 'undefined' && this.props.photoStream.voted_users) ? this.props.photoStream.voted_users[userIndex - 1] : ''

    return (
      <div>
        <div className={this.props.style == 1 ? "card__vote card__vote--1" : "card__vote card__vote--2"}>
          {this.renderVoteButton()}
        </div>
        { this.state.isModalOpen &&
        <Modal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.hideModal}
          className="VoteButton modal__content"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="well well--e txt-center txt-blue mbm txt-medium">{number_people_liked} người vote ảnh của bạn</div>
          <div className="card mbm">
            <UserPicture go_to_profile={true} user={this.state.user} />
            <button
              className={`pagi pagi--prev pagi--2${prevUser ? '' : ' is-hide'}`}
              onClick={this.nextUser_OnClick.bind(this, userIndex, -1)}
            >
              <i className="fa fa-chevron-left"></i>
            </button>
            <button
              className={`pagi pagi--next pagi--2${nextUser ? '' : ' is-hide'}`}
              onClick={this.nextUser_OnClick.bind(this, userIndex, 1)}
            >
              <i className="fa fa-chevron-right"></i>
            </button>
          </div>
          <div className="mbs">
            <LikeButton
              user={this.state.user}
              user_profile={current_user}
              forceLike
              msg=''
            />
            <StartChatButton user={this.state.user} shortText={false} page_source={VOTED_MODAL_PAGE}/>
          </div>
          <button className="modal__btn-close" onClick={this.hideModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
        }
      </div>
    )
  }
}

VoteButton.propTypes = {
  picture: PropTypes.object,
  page_source: PropTypes.string,
  style: PropTypes.number,
  type: PropTypes.number,
  user_profile: PropTypes.object,
  Enviroment: PropTypes.object,
  photoStream: PropTypes.object,
  gaSend: PropTypes.func,
  getVotedUsers: PropTypes.func,
  votePhoto: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    votePhoto: (picture_id, picture_type, action_type, current_user_id, page_source, target_user_id, current_user) => {
      const defer = q.defer()
      dispatch(votePhoto(picture_id, picture_type, action_type, current_user_id, page_source)).then((response) => {
        defer.resolve(response)
        if (current_user.user_status == ACTIVE_USER) {
          socket.emit('vote_picture', {
            room: target_user_id,
            picture_id,
            vote_from_id: current_user_id,
            vote_to_id: target_user_id,
            vote_from_name: current_user.nick_name,
            vote_type: action_type,
            vote_from_user: current_user,
          });
        }
      }).catch((err) => {
        defer.reject(err)
      });
      return defer.promise
    },
    gaSend: (eventAction, ga_track_data) => {
      dispatch(gaSend(eventAction, ga_track_data))
    },
    updatePreviousPage: (page) => {
      dispatch(updatePreviousPage(page));
    },
    getVotedUsers: (picture_type, picture_id) => {
      const defer = q.defer()
      dispatch(getVotedUsers(picture_type, picture_id)).then((response) => {
        defer.resolve(response)
      }).catch((err) => {
        defer.reject(err)
      });
      return defer.promise
    },
  }
}
const VoteContainer = connect(mapStateToProps, mapDispachToProps)(VoteButton);

export default VoteContainer;
