import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import Modal from 'components/modals/Modal'
import q from 'q';
import $ from 'jquery';
import {
  SUPER_LIKE_URL,
  WHAT_HOT_PAGE, PEOPLE_LIKED_ME_PAGE, NOTIFICATION_SUCCESS, CDN_URL,
  CLICK_NOTI_CATEGORY, CLICK_NOFI_LIKE
} from 'constants/Enviroment';
import { NUMBER_COINS_NEED_FOR_SUPER_LIKE } from 'constants/Like';
import { BLOCK_TYPE_BLOCKED } from 'constants/userProfile'
import SuperLikeModal from 'components/modals/SuperLikeModal';
import UserPicture from 'components/commons/UserPicture';
import * as ymmStorage from 'utils/ymmStorage';
import { coinConsume } from 'actions/Payment';
import {
  getPeopleMatchedMe, getCurrentUser, getLuckySpinnerGift
} from 'actions/userProfile';
import { showNextPeopleLikedMe } from 'pages/people-liked-me/Actions';
import { sendGoogleAnalytic } from 'actions/Enviroment';
import { showNextUser } from 'actions/WhatHot';
import * as showNotification from 'utils/notifications/alreadyUpgrade';
import { compareIdentifiers } from 'utils/common';
import { sendClickableNotification } from 'utils/errors/notification';
import GoogleOptimize from 'utils/google_analytic/google_optimize';
import socket from "utils/socket";
import { ACTIVE_USER } from 'constants/userProfile';

class SuperLikeButton extends React.Component {
  constructor(){
    super()
    this.state = {
      isOpenConfirmModal: false,
      super_like_success: false,
      super_like_clicked: false
    }

    this.addMoreCoinClick = this.addMoreCoinClick.bind(this);
    this.forceChatClick = this.forceChatClick.bind(this);
    this.rememberPaymentIntention = this.rememberPaymentIntention.bind(this);
}

  componentWillMount() {
    ymmStorage.removeItem('super_like_name');
  }

  @GoogleOptimize
  componentDidUpdate(){}

  getPageSource() {
    if (this.props.page_source && this.props.page_source !== '') {
      return this.props.page_source;
    }
    if (this.props.Enviroment.view_profile_source !== '') {
      return this.props.Enviroment.view_profile_source;
    }
    return this.props.Enviroment.previous_page;
  }

  superLikeClick = () => {
    const { user, user_profile } = this.props;
    const { current_user, matched_users, lucky_spinner_gifts } = user_profile;
    const { coins, permission, user_type, gender } = current_user;
    const totalSuperLike = permission.number_super_like_free + lucky_spinner_gifts.super_like_free_gift;

    if (!_.isEmpty(matched_users)) {
      if (matched_users.some(item => compareIdentifiers(item.identifier, user.identifier) && item.is_random_matched == false)) {
        const msg = 'Hai bạn đã ghép đôi thành công, vào Trò chuyện thôi!';
        const url = `${process.env.CHAT_SERVER_URL}chat/${user.identifier}`;
        const callback = this.sendGaClickNoti(CLICK_NOFI_LIKE);

        sendClickableNotification(NOTIFICATION_SUCCESS, DEFAULT_NOTI_LABEL, msg, url, 3000, true, callback);
        return;
      }
    }
    const gaTrackData = {
      page_source: this.getPageSource()
    };
    const eventAction = {
      eventCategory: 'SuperLike',
      eventAction: 'Click',
      eventLabel: 'Click Super Like'
    }

    this.props.sendGoogleAnalytic(eventAction, gaTrackData);
    if (gender === 'female') {
      this.setState ({
        isOpenConfirmModal: true,
      })
      //this.sendSuperLike(current_user, user);
      //browserHistory.push('/chat');
      // window.open(`${process.env.CHAT_SERVER_URL}chat/${user.identifier}`);
      return;
    }
    this.setState ({
      isOpenConfirmModal: true,
    })
    // if (user_type === 4 && totalSuperLike === 0 && coins < NUMBER_COINS_NEED_FOR_SUPER_LIKE) {
    //   showNotification.showAlreadyUpgrade()
    // }
	}

  closeModal = () => {
    this.setState ({
      isOpenConfirmModal: false,
    })
  }

  sendGaClickNoti(gaPageSource) {
    const gaTrackDataClickNoti = {
      page_source: gaPageSource
    }
    const eventActionClickNoti = {
      eventCategory: CLICK_NOTI_CATEGORY,
      eventAction: 'Click',
      eventLabel: CLICK_NOTI_CATEGORY
    }
    return () => this.props.sendGoogleAnalytic(eventActionClickNoti, gaTrackDataClickNoti);
  }

  rememberPaymentIntention(current_user, to_user) {
    let female_id = null;
    let male_id = null;
    if (current_user == null || to_user == null) {
      return;
    }
    if (current_user.gender == 'male') {
      male_id = current_user.identifier;
      female_id = to_user.identifier;
    } else {
      male_id = to_user.identifier;
      female_id = current_user.identifier;
    }
    ymmStorage.setItem('super_like_name', to_user.nick_name);
    ymmStorage.setItem('super_like_to_identifier', to_user.identifier);
    ymmStorage.setItem('super_like_male_id', male_id);
    ymmStorage.setItem('super_like_female_id', female_id);
  }

  addMoreCoinClick(current_user, to_user) {
    this.rememberPaymentIntention(current_user, to_user);
    const gaTrackData = {
      page_source: 'SUPER_LIKE_MODAL'
    };
    const eventAction = {
      eventCategory: 'Coin',
      eventAction: 'GetMoreCoin',
      eventLabel: 'Get more coin'
    }

    this.props.sendGoogleAnalytic(eventAction, gaTrackData);
    browserHistory.push('/payment/coin');
  }

  sendSuperLike(current_user, to_user) {
    const { identifier,  user_status, gender, user_type} = current_user;
    let female_id = null;
    let male_id = null;
    if (gender == 'male') {
      male_id = identifier;
      female_id = to_user.identifier;
    } else {
      male_id = to_user.identifier;
      female_id = identifier;
    }
    this.props.sendSuperLike(male_id, female_id).then(() => {
      if (user_status == ACTIVE_USER) {
        const new_current_user = Object.assign({}, current_user, {
          compatibility: to_user.compatibility
        })
        socket.emit('super_like', {room: to_user.identifier, to_user_identifier: to_user.identifier, super_like_info: new_current_user});
      }
      this.setState ({
        isOpenConfirmModal: false,
        super_like_success: true
      })
      const gaTrackData = {
        page_source: this.getPageSource()
      };
      const eventAction = {
        eventCategory: 'Coin',
        eventAction: 'Buy',
        eventLabel: 'Buy Super Like'
      }
      this.props.sendGoogleAnalytic(eventAction, gaTrackData)
      if (this.getPageSource() === WHAT_HOT_PAGE) {
        this.props.showNextUser(gender, user_type)
      }
      if (this.getPageSource() === PEOPLE_LIKED_ME_PAGE) {
        this.props.showNextPeopleLikedMe();
      }

      to_user.have_sent_like_to_me = true;
      to_user.have_sent_like = true;
      to_user.match_type = 3;
      this.props.getPeopleMatchedMe();
      this.props.getCurrentUser();
      this.props.getLuckySpinnerGift();
    }).catch(() => {});
  }

  forceChatClick(current_user, to_user) {
    const { matched_users } = this.props.user_profile;

    if (!_.isEmpty(matched_users)) {
      if (matched_users.some(item => compareIdentifiers(item.identifier, to_user.identifier) && item.is_random_matched == false)) {
        const msg = 'Hai bạn đã ghép đôi thành công, vào Trò chuyện thôi!';
        const url = `${process.env.CHAT_SERVER_URL}chat/${to_user.identifier}`;
        const callback = this.sendGaClickNoti(CLICK_NOFI_LIKE);

        sendClickableNotification(NOTIFICATION_SUCCESS, DEFAULT_NOTI_LABEL, msg, url, 3000, true, callback);
        return;
      }
    }
    this.setState ({
      super_like_clicked: true
    })
    this.sendSuperLike(current_user, to_user);
  }

  renderUpgradeAccount() {
    const { user, user_profile } = this.props;
    const { current_user, lucky_spinner_gifts } = user_profile;
    const { coins, permission, gender } = current_user;
    const totalSuperLike = permission.number_super_like_free + lucky_spinner_gifts.super_like_free_gift;
    if ( totalSuperLike > 0 || coins >= NUMBER_COINS_NEED_FOR_SUPER_LIKE || gender == 'female') {
      return '';
    }
    return (
      <div className="txt-center padding-t5">
        hoặc <Link to="/payment/upgrade" onClick={() => {this.rememberPaymentIntention(current_user, user)}} className="txt-underline">Nâng cấp tài khoản</Link>
      </div>
    )
  }

  render() {
    const { user, user_profile } = this.props;
    const { matched_users, current_user, lucky_spinner_gifts } = user_profile;
    const { coins, permission } = current_user;
    const totalSuperLike = permission.number_super_like_free + lucky_spinner_gifts.super_like_free_gift;
    const isBlockedUser = user.block_type ? user.block_type === BLOCK_TYPE_BLOCKED : false;
    const is_matched_user = matched_users.some(item => compareIdentifiers(item.identifier, user.identifier));
    const superLikeButtonTitle = current_user.gender === 'male' ? 'Chat luôn' : 'Làm quen!' ;
    if (is_matched_user) {
      return <div></div>
    }

		return (
			<div>
				<button
          className="btn btn--4 btn--b txt-center l-flex-vertical-center"
          onClick={this.superLikeClick}
          disabled={isBlockedUser}
        >
					<img src={SUPER_LIKE_URL} alt="SUPER_LIKE_URL" className="img-like"/>
					<span>{`${this.state.super_like_success ? 'Trò chuyện nào' : superLikeButtonTitle}`}</span>
				</button>
        <SuperLikeModal />
				<Modal
          transitionName="modal"
	        isOpen={this.state.isOpenConfirmModal}
	        onRequestClose={this.closeModal}
	        className="SuperLikeButton modal__content modal__content--2"
	        overlayClassName="modal__overlay"
	        portalClassName="modal"
          contentLabel=""
	      >
          {current_user.gender == 'male' ?
          <div>
            <div className="banner txt no-cursor mbm radius-top padding-t10">
              <div className="banner__content txt-center">
                <img src={`${CDN_URL}/super_like/Heart.png`} alt="super-like-cung.png" className="img-50"/>
                <h4 className="txt-uppercase txt-pink">Chat luôn không cần đợi thích</h4>
              </div>
            </div>
            <div className="modal__txt-content">
              <div className="txt-blue txt-bold txt-center txt-uppercase mbm">{`Chỉ với ${NUMBER_COINS_NEED_FOR_SUPER_LIKE} xu bạn sẽ được:`}</div>
              <div className="txt-medium row mbm l-flex-vertical-center">
                <div className="col-xs-3 col-md-2">
                  <img src={`${CDN_URL}/super_like/super-like-icon1.png`} alt="super_like"/>
                </div>
                <div className="col-xs-9 col-md-10">Chat với {user.nick_name} ngay mà không cần đợi cô ấy thích lại</div>
              </div>
              <div className="txt-medium row mbm l-flex-vertical-center">
                <div className="col-xs-3 col-md-2">
                  <img src={`${CDN_URL}/super_like/super-like-icon4.png`} alt="super_like"/>
                </div>
                <div className="col-xs-9 col-md-10">Nổi bật hơn những thành viên khác</div>
              </div>
              {totalSuperLike == 0 && <div className={`${coins >= NUMBER_COINS_NEED_FOR_SUPER_LIKE ? 'txt-blue' : 'txt-red' } padding-b10 txt-center txt-bold`}>Bạn còn {coins} xu.</div>}
              {totalSuperLike > 0 && <div className="padding-b10 txt-center txt-blue txt-bold">Bạn còn {totalSuperLike} lần sử dụng miễn phí</div>}
              <div className="mbs">
                {(coins >= NUMBER_COINS_NEED_FOR_SUPER_LIKE || totalSuperLike > 0) && <button disabled={this.state.super_like_clicked} className="btn txt-center l-flex-vertical-center btn--4 btn--b" onClick={() => {this.forceChatClick(current_user, user)}} >
                <img className="img-like" src={`${CDN_URL}/super_like/Heart_pink.png`} alt="tym-icon" /> Chat luôn</button>}
                {
                  (coins < NUMBER_COINS_NEED_FOR_SUPER_LIKE && totalSuperLike == 0) &&
                  <button className="btn btn--blue btn--b" onClick={() => {this.addMoreCoinClick(current_user, user)}} >Mua thêm xu</button>
                }
              </div>
            </div>
          </div>
          :
          <div>
            <div className="banner txt no-cursor mbm radius-top padding-t10">
              <div className="banner__content txt-center">
                <img src={`${CDN_URL}/super_like/Heart.png`} alt="super-like-cung.png" className="img-50"/>
                <h4 className="txt-uppercase txt-pink">Chat luôn không cần đợi thích</h4>
              </div>
            </div>
            <div className="modal__txt-content">
              <div className="txt-medium txt-center txt-blue row mbm l-flex-vertical-center">
                <div className="col-xs-9 col-md-7">Bạn muốn trò chuyện với {user.nick_name} ngay mà không muốn đợi {user.nick_name} gửi thích cho bạn?</div>
              </div>
              <button disabled={this.state.super_like_clicked} className="btn txt-center l-flex-vertical-center btn--4 btn--b" onClick={() => {this.forceChatClick(current_user, user)}} >
              <img className="img-like" src={`${CDN_URL}/super_like/Heart_pink.png`} alt="tym-icon" /> Chat luôn</button>
            </div>
          </div>
          }
	        <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
	      </Modal>
			</div>
		)
	}
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
  	sendSuperLike: (male_id, female_id) => {
  		const defer = q.defer();
  		dispatch(coinConsume('coin_consume_super_match', male_id, female_id)).then(res => {
  			defer.resolve(res);
  		}).catch(() => {
  			defer.reject(false);
  		})
  		return defer.promise;
  	},
  	getPeopleMatchedMe: () => {
      dispatch(getPeopleMatchedMe())
    },
    getCurrentUser: () => {
      dispatch(getCurrentUser())
    },
    sendGoogleAnalytic: (eventAction, ga_track_data) =>{
      dispatch(sendGoogleAnalytic(eventAction, ga_track_data))
    },
    showNextUser: (userGender, userType) => {
      dispatch(showNextUser(false, userGender, userType))
    },
    showNextPeopleLikedMe: () => {
      dispatch(showNextPeopleLikedMe())
    },
    getLuckySpinnerGift: () => {
      dispatch(getLuckySpinnerGift());
    }
  }
}

const SuperLikeButtonContainer = connect(mapStateToProps, mapDispatchToProps)(SuperLikeButton);

export default SuperLikeButtonContainer;