import React, { PropTypes } from 'react'
import $ from 'jquery'
import slick from '../../../public/scripts/slick.min.js'
import { CDN_URL } from '../../constants/Enviroment'

export default class SilderCommon extends React.Component {
  constructor(props) {
    super(props)
  }

  _carousel = () => {
    const {numberShowItem, arrows, dots, mobileShowItem, autoplaySpeed} = this.props
    // common carousel
    const carousel = $(`.${this.props.carouselName}`);
    if (!carousel) {
      return;
    }
    carousel.slick({
      slidesToShow: numberShowItem,
      slidesToScroll: 1,
      arrows: arrows,
      autoplay: true,
      autoplaySpeed: autoplaySpeed,
      initialSlide: 0,
      focusOnSelect: true,
      dots: dots,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: mobileShowItem,
            dots: dots
          },
        },
        {
          breakpoint: 425,
          settings: {
            slidesToShow: mobileShowItem,
          },
        },
      ],
    })
  }

  _unCarousel() {
    //if ($(`.carousel-form .${this.props.carouselName}`)) {
    if ($(`.${this.props.carouselName}`)) {
      $(`.${this.props.carouselName}`).slick('unslick');
    }
  }

  componentDidMount() {
    this._carousel()
  }

  componentWillUpdate(nextProps, nextState) {
    if ((nextProps.children == null && this.props.children != null) ||
      (nextProps.children != null && this.props.children == null) ||
      (nextProps.children.length !== this.props.children.length)) {
      this._unCarousel()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if ((prevProps.children == null && this.props.children != null) ||
      (prevProps.children != null && this.props.children == null) ||
      (prevProps.children.length !== this.props.children.length)) {
      this._carousel()
    }
  }

  render() {
    const {children, notUserClassGrid} = this.props;
    if(!children) {
      return <div></div>
    }
    return (
      <div className="panel">
        <div className={`clearfix ${notUserClassGrid || "row"}`}>
          <div className={notUserClassGrid || "col-xs-12"}>
            <div className={this.props.carouselName} >
              {children}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
SilderCommon.propTypes = {
  carouselName: PropTypes.string,
  children: PropTypes.node,
  numberShowItem: PropTypes.number,
  mobileShowItem: PropTypes.number,
  autoplaySpeed: PropTypes.number,
  arrows: PropTypes.bool,
  dots: PropTypes.bool,
  notUserClassGrid: PropTypes.bool,
}

SilderCommon.defaultProps = {
  carouselName: 'carousel-common',
  mobileShowItem: 1,
  numberShowItem: 1,
  arrows: true,
  dots: true,
  autoplaySpeed: 7000,
  notUserClassRow: false,
};
