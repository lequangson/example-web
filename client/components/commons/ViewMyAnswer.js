import React, { PropTypes } from 'react'

class ViewMyAnswer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      curentQuestion: 0,
    }
    this.next = this.next.bind(this)
    this.prev = this.prev.bind(this)
    this.chooseQuestion = this.chooseQuestion.bind(this)
  }

  next() {
    const totalQuestion = this.props.my_questions.length
    if (this.state.curentQuestion === totalQuestion - 1) {
      return
    }
    this.setState({ curentQuestion: this.state.curentQuestion + 1 })
  }

  prev() {
    if (this.state.curentQuestion === 0) {
      return
    }
    this.setState({ curentQuestion: this.state.curentQuestion - 1 })
  }

  renderDescription() {
    if (this.props.Description == null || this.props.Description === '') {
      return ''
    }
    return (
      <div>
        <div className="mbm txt-blue txt-bold">{this.props.Description} </div>


      </div>
    )
  }

  chooseQuestion(id, is_explore_question) {
    this.props.chooseExplorerQuestion(id, is_explore_question)
  }

  render() {
    const my_questions = this.props.my_questions
    const answers = this.props.Match_Questions.answers
    const total_choosen = this.props.Match_Questions.my_questions.filter(q => q.is_explore_question === 1).length

    return (
      <div className=" txt-center" >
        <div className="well well--e txt-blue txt-bold mbm">Câu hỏi vượt rào</div>
        <div className="mbm l-flex-ce">
          <button className="btn btn--5" style={this.state.curentQuestion === 0 ? {visibility: 'hidden'} : {}} onClick={this.prev}>
            <i className="fa fa-chevron-left"></i> Trước
          </button>
          <span className="txt-bold txt-blue l-flex-vertical-center"> {my_questions[this.state.curentQuestion].is_explore_question == 1 && <i className="fa fa-check-circle txt-xlg txt-green mrs"></i>} Câu {this.state.curentQuestion + 1}/20</span>
          <button className="btn btn--5" style={this.state.curentQuestion === my_questions.length - 1 ? {visibility: 'hidden'} : {}} onClick={this.next} >
            Tiếp <i className="fa fa-chevron-right"></i>
          </button>
        </div>
        <div className="mbm txt-blue txt-bold" style={{ minHfaeight: '110px' }}>
          {my_questions[this.state.curentQuestion].title}
          {this.renderDescription()}
        </div>
        <div className=" mbm" >
          Trả lời: {answers[my_questions[this.state.curentQuestion].answer_code-1].title}
        </div>
        <div className="mbs">
          {
            my_questions[this.state.curentQuestion].is_explore_question == 1 ?
            <button
              className="btn btn--b btn--p"
              onClick={() => {this.chooseQuestion(my_questions[this.state.curentQuestion].id, 0)}}
            >
              Bỏ chọn câu này ({total_choosen}/5)
            </button>
            :
            <button
              className="btn btn--b btn--p"
              onClick={() => {this.chooseQuestion(my_questions[this.state.curentQuestion].id, 1)}}
              disabled={(total_choosen == 5)}
            >
              Chọn câu này ({total_choosen}/5)
            </button>
          }

        </div>
      </div>
    )
  }
}

ViewMyAnswer.propTypes = {
}

export default ViewMyAnswer
