import React, { Component } from 'react';

class Headroom extends Component {
  constructor() {
    super();
    this.calculatePosition = this.calculatePosition.bind(this);
  }
  componentDidMount() {
    this.fixedDivOffsetBottom = this.fixedDivParent.offsetTop + this.fixedDivParent.offsetHeight + 50 ;
    this.previous = 0;
    this.previousDocumentHeight = document.body.clientHeight;

    window.addEventListener('scroll', this.calculatePosition);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.calculatePosition);
  }

  calculatePosition() {
    if (this.fixedDivOffsetBottom < window.pageYOffset) {
      this.fixedDiv.classList.add('is-fixed');
      this.fixedDiv.classList.add('is-show');
      if (this.previous + 8 < window.pageYOffset && this.previousDocumentHeight === document.body.clientHeight) {
        // scroll down
        // this.fixedDiv.classList.remove('is-show');  
        this.previous = window.pageYOffset;     
      } 
      if (this.previous - 8 > window.pageYOffset && this.previousDocumentHeight === document.body.clientHeight) {
        // scroll up
        // this.fixedDiv.classList.add('is-show');
        this.previous = window.pageYOffset;
      }
    } else {
      this.fixedDiv.classList.remove('is-fixed');
      this.fixedDiv.classList.remove('is-show');
    }
    this.previousDocumentHeight = document.body.clientHeight
  }

  render() {
    return (
      <div
        className={this.props.classHr ? this.props.classHr : "site-fixed-position mbm"}
        ref={(ref) => {
        this.fixedDivParent = ref
      }}>
        <div
          className="site-fixed-position-content"
          ref={(ref) => {
            this.fixedDiv = ref
          }}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Headroom
