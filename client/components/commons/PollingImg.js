import React from 'react';

class PollingImg extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      imageLoadCount: 0,
      waiting: false
    }
  }

  onImageLoadError(event) {
    const self = this;
    self.setState({waiting: true})
    setTimeout(() => {
      const newImageLoadCount = self.state.imageLoadCount + 1;
      // console.log(`reload image in PollingImg.js. src: ${this.props.src}, load count: ${newImageLoadCount}`)
      self.setState({imageLoadCount: newImageLoadCount, waiting: false})
    }, 2000);
  }

  render() {
    if (this.state.waiting) {
      return <i className="fa fa-spinner fa-5x"></i>
    }

    return (
      <img
        {...this.props}
        onError={this.onImageLoadError.bind(this)}
        src={`${this.props.src}?cnt=${this.state.imageLoadCount}`}
      />
    );
  }
}

export default PollingImg;
