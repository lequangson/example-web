import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import _ from 'lodash'
import RandomMatchModal from '../modals/RandomMatchModal'
import { getRandomMatch, updateModalState, clearModalData } from '../../actions/random_match'
import { GA_ACTION_RANDOM_MATCH_CLICK, RANDOM_MATCH_BANNER, RANDOM_MATCH_COST, GA_ACTION_RANDOM_MATCH_CLICK_BANNER, RANDOM_MATCH } from '../../constants/Enviroment'
// import { getRandomMatch, updateModalState, clearModalData } from '../../actions/random_match'
// import { GA_ACTION_RANDOM_MATCH_CLICK_BANNER, RANDOM_MATCH_BANNER, RANDOM_MATCH_COST } from '../../constants/Enviroment'
import { changeBannerStatus } from '../../actions/Enviroment'


class RandomMatchButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isModalOpen: false,
    }
    this.handleClick = this.handleClick.bind(this)
    this.showModal = this.showModal.bind(this)
    this.hideModal = this.hideModal.bind(this)
  }

  componentWillMount() {
    if(this.props.random_match.data.length == 0) {
      this.props.getRandomMatch()
    }
  }

  componentDidUpdate() {
    const coins = this.props.user_profile.current_user.coins
    const data = this.props.random_match.data
    const hiddenCard = this.props.random_match.hiddenCard
    const { random_match_free_gift } = this.props.user_profile.lucky_spinner_gifts;
    const user_free_random_match = !_.isEmpty(this.props.user_profile.current_user) ? this.props.user_profile.current_user.permission.number_random_match_free : 0;
    const number_random_match_free = random_match_free_gift + user_free_random_match;
    if (number_random_match_free <= 0 && coins < RANDOM_MATCH_COST && data.length > 0 && hiddenCard && !this.props.random_match.isUpdateRandomMatch) {
      this.props.clearModalData()
    }
  }

  componentWillUnmount() {
    this.props.updateModalState(true)
    if (this.props.random_match.isUpdateRandomMatch) {
      this.props.clearModalData() //clear data when user view other profile from random match modal
    }
  }

  showModal() {
    this.setState({ isModalOpen: true })
  }

  hideModal() {
    const state = !this.props.random_match.hiddenCard
    const coins = this.props.user_profile.current_user.coins
    const { random_match_free_gift } = this.props.user_profile.lucky_spinner_gifts;
    const user_free_random_match = !_.isEmpty(this.props.user_profile.current_user) ? this.props.user_profile.current_user.permission.number_random_match_free : 0;
    const number_random_match_free = random_match_free_gift + user_free_random_match;
    this.setState({ isModalOpen: false })
    if (number_random_match_free > 0 || coins >= RANDOM_MATCH_COST) {
      this.props.getRandomMatch()
    }
    else {
      this.props.clearModalData()
    }
    this.props.updateModalState(true) //hidden card when close popup
  }

  handleClick() {
    this.showModal()
    this.props.gaSend(GA_ACTION_RANDOM_MATCH_CLICK_BANNER, { page_source: 'Random Match Banner' })
  }

  render() {
    const data = this.props.random_match.data
    return (
      <div>
        <button
          className={this.props.className}
          onClick={this.handleClick}
        >
          {this.props.buttonName}
        </button>
        <RandomMatchModal
          current_user={this.props.user_profile.current_user}
          isOpen={this.state.isModalOpen}
          gaSend={this.props.gaSend}
          onRequestClose={this.hideModal}
          data={data}
          updateModalState={this.props.updateModalState}
        />
      </div>
    )
  }
}

RandomMatchButton.propTypes = {
  className: PropTypes.string.isRequired,
  buttonName: PropTypes.string.isRequired,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    getRandomMatch: () => {
      dispatch(getRandomMatch())
    },
    updateModalState: (state) => {
      dispatch(updateModalState(state))
    },
    clearModalData: () => {
      dispatch(clearModalData())
    },
    changeBannerStatus: (banner) => {
      dispatch(changeBannerStatus(banner))
    },
  }
}

const RandomMatchButtonContainer = connect(mapStateToProps, mapDispatchToProps)(RandomMatchButton);

export default RandomMatchButtonContainer
