import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import q from 'q';
import { browserHistory } from 'react-router';
import { BLOCK_TYPE_BLOCKED, BLOCK_TYPE_MUTED } from 'constants/userProfile';
import {
  GA_ACTION_BLOCK_USER, GA_ACTION_UNBLOCK_USER, GA_ACTION_MUTE_USER, GA_ACTION_UNMUTE_USER,
  SEARCH_PAGE, WHAT_HOT_PAGE, PEOPLE_LIKED_ME_PAGE,
} from 'constants/Enviroment';
import { getBlockedUsers } from 'actions/userProfile';
import * as ymmStorage from 'utils/ymmStorage';
import { showNextPeopleLikedMe } from 'pages/people-liked-me/Actions';

class BlockButton extends React.Component {

  constructor(props) {
    super(props)
    this.gotoBlockForm = this.gotoBlockForm.bind(this)
  }

  gotoBlockForm() {
    browserHistory.push(`/block-form/${this.props.user.identifier}`)
  }

  blockUser(identifier, current_block_type, change_to_block_type) {
    if (ymmStorage.getItem('inprogress')) {
      return
    }
    ymmStorage.setItem('inprogress', true);
    const props = this.props
    const { user } = props
    const that = this
    // user.block_type = change_to_block_type
    props.unBlockUser(identifier, current_block_type).then(() => {

      that.sendGaEvent(
        parseInt(current_block_type, 10) === BLOCK_TYPE_BLOCKED
        ?
        GA_ACTION_UNBLOCK_USER
        :
        GA_ACTION_UNMUTE_USER
      )
      props.blockUser(identifier, change_to_block_type).then(() => {
        that.props.getBlockedUsers(BLOCK_TYPE_MUTED);
        that.sendGaEvent(
          parseInt(change_to_block_type, 10) === BLOCK_TYPE_BLOCKED
          ?
          GA_ACTION_BLOCK_USER
          :
          GA_ACTION_MUTE_USER
        );
        user.block_type = change_to_block_type
        ymmStorage.removeItem('inprogress');
        that.setState({ block_type: change_to_block_type })// change state value to rerendering button component
        that.props.updateReducerState(props.Enviroment.previous_page, identifier, 'block_type', change_to_block_type)
        that.props.updateReducerState(SEARCH_PAGE, identifier, 'block_type', change_to_block_type)
        that.props.blockNotificationByIdentifier(identifier)
        if (this.props.Enviroment.previous_page === WHAT_HOT_PAGE) {
          this.props.showNextWhatHotUser();
        }
        if (that.props.Enviroment.previous_page === PEOPLE_LIKED_ME_PAGE) {
          that.props.showNextPeopleLikedMe();
        }
      })
    }).catch((e) => {
      ymmStorage.removeItem('inprogress');
    })
  }
  unBlockUser(identifier, block_type) {
    if (ymmStorage.getItem('inprogress')) {
      return
    }
    ymmStorage.setItem('inprogress', true);
    const props = this.props
    // user.block_type = 0
    const that = this
    this.props.unBlockUser(identifier, block_type).then(() => {
      // this.props.getBlockedUsers(BLOCK_TYPE_MUTED);
      that.sendGaEvent(
        parseInt(block_type, 10) === BLOCK_TYPE_BLOCKED
        ?
        GA_ACTION_UNBLOCK_USER
        :
        GA_ACTION_UNMUTE_USER
      )
      props.user.block_type = 0
      that.props.updateReducerState(props.Enviroment.previous_page, identifier, 'block_type', 0)
      that.props.updateReducerState(SEARCH_PAGE, identifier, 'block_type', 0)
      that.props.unBlockNotificationByIdentifier(identifier)
      ymmStorage.removeItem('inprogress');
      that.setState({ block_type: 0 }) // change state value to rerendering button component
    }).catch(() => {
      ymmStorage.removeItem('inprogress');
    })
  }

  sendGaEvent(gaAction) {
    const { user } = this.props
    const ga_track_data = {
      partner_residence: (user && user.residence) ? user.residence.value : '',
      partner_birth_place: (user && user.birth_place) ? user.birth_place.value : '',
      partner_identifier: user ? user.identifier : '',
      partner_age: user ? user.age : null,
    };
    this.props.gaSend(gaAction, ga_track_data);
  }

  render() {
    const { user } = this.props
    return (
      <div>
        { !user.block_type &&
          <button
            type="button"
            className="btn  btn--5"
            onClick={this.gotoBlockForm}
          >
            <i className="fa fa-ban txt-red padding-r5"></i>
            Chặn
          </button>
        }
        { user.block_type > 0 &&
          <button
            type="button"
            className="btn btn--5"
            onClick={() => { this.unBlockUser(user.identifier, user.block_type) }}
          >
            Bỏ chặn
          </button>
        }
      </div>
    )
  }
}
BlockButton.propTypes = {
  user: PropTypes.object,
  gaSend: PropTypes.func,
  unBlockUser: PropTypes.func,

}


function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    getBlockedUsers: (block_type) => dispatch(getBlockedUsers(block_type)),
    showNextPeopleLikedMe: () => dispatch(showNextPeopleLikedMe())
  }
}
export default connect(mapStateToProps, mapDispachToProps)(BlockButton);