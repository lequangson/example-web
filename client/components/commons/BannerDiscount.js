import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import { convertUTCToLocalTime } from 'utils/common'
import { CDN_URL } from 'constants/Enviroment'
import { isEmpty } from 'lodash'
import SilderCommon from './SilderCommon';
import BannerHasImgRight from 'components/commons/BannerHasImgRight';

class BannerDiscount extends Component {
	constructor(props) {
		super(props)
		this.state = {
      day: 0,
			hour: 0,
			minute: 0,
			second: 0,
			expired: true,
		}
		this.getTakeTime = this.getTakeTime.bind(this)
	}

	componentWillMount() {
		this.getTakeTime();
		this.props.setInterval(this.getTakeTime, 1000);
  }

	getTakeTime() {
    const timeStop = convertUTCToLocalTime(this.props.timeStop);
    const now = new Date();
    const timediff = timeStop - now;
    const milisecondInHour = 3600000;//1000*60*60
    const milisecondInDay = 86400000;//1000*60*60*24;

    const numberDay = Math.floor(timediff / milisecondInDay);
    const day = numberDay < 10 ? '0' + numberDay : numberDay;

    const timeDiffHour = timediff - (day * milisecondInDay);
    const numberHour = Math.floor(timeDiffHour / milisecondInHour);
    const hour = numberHour < 10 ? '0' + numberHour : numberHour;

    const a = timeDiffHour % milisecondInHour;
    const numberMinute = Math.floor(a / 60000);
    const minute = numberMinute < 10 ? '0' + numberMinute : numberMinute;
    const numberSecond = Math.floor((a % 60000) / 1000);
    const second = numberSecond < 10 ? '0' + numberSecond : numberSecond;

    let expired = false;
    if (day === 0 && hour === 0 && minute === 0 && second === 0) {
      expired = true;
    }
    this.setState({
      day,
      hour,
      minute,
      second,
      expired,
    });
  }

  bannerReferral() {
    const { type } = this.props.params;
    if (type == 'coin') {
      return (
        <div>
          <BannerHasImgRight
            pageSource="referral"
            to="/referral-page"
            customClass="mbm"
            // onClick={()=>console.log('that work')}
          />
        </div>
      )
    }
    return (
      <div></div>
    )
  }

	render() {
		const { url, isPaidUser } = this.props
    const { gender } = this.props.user_profile.current_user;
    if (gender === 'female' || (gender ==='male' && isPaidUser)) {
      return (
        <div className="mbs">
          <Link
            className="banner banner__background--1 banner__background--large-height"
            to="/events"
          >
            <div className="banner__content col-xs-12 col-xs-offset-3">
            </div>
            <div className="banner__meta">
            </div>
          </Link>
        </div>
      );
    }
		return (
			<div className="row">
				<div className="col-xs-12">
          <SilderCommon
            carouselName='carousel-common'
            numberShowItem={1}>
            <div>
              <Link
                className="banner pos-relative banner--l mbm"
                to={url}
              >
                <div className="banner__content">
                  <span className="banner__heading">
                    <img className="mt banner__icon--2" src={`${CDN_URL}/general/Banner/Upgrade_account.png`} alt="Khuyến mãi Noel" /> Nâng cấp thành viên, nhận liền thêm <img className="mt banner__icon--1" src={`${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`} alt="Khuyến mãi Valentine" /> xu
                  </span>
                  <div className="banner__sub-heading banner__sub-heading--2 margin-l20 l-flex-vertical-center">
                    Thời hạn còn
                    <div className="txt-center padding-t5">
                      <span className="time-span">{this.state.day}</span>
                      <span className="l-block txt-sm txt-bold">Ngày</span>
                    </div>
                    <div className="txt-center padding-t5">
                      <span className="time-span">{this.state.hour}</span>
                      <span className="l-block txt-sm txt-bold">Giờ</span>
                    </div>
                    <div className="txt-center padding-t5">
                      <span className="time-span">{this.state.minute}</span>
                      <span className="l-block txt-sm txt-bold">Phút</span>
                    </div>
                    <div className="txt-center padding-t5">
                      <span className="time-span">{this.state.second}</span>
                      <span className="l-block txt-sm txt-bold">Giây</span>
                    </div>
                  </div>
                </div>
                <div className="banner__meta">
                  <i className="fa fa-chevron-right"></i>
                </div>
              </Link>
            </div>
            { this.props.campaign && !isEmpty(this.props.campaign.data) &&
              <div>
                <Link
                  className="banner banner__background--1 banner__background--large-height"
                  to="/events"
                >
                  <div className="banner__content col-xs-8 col-xs-offset-3">
                  </div>
                  <div className="banner__meta">
                  </div>
                </Link>
              </div>
            }
            { this.bannerReferral() }
          </SilderCommon>
				</div>
			</div>
		);
	}
}

BannerDiscount.propTypes = {
	url: PropTypes.string.isRequired,
	timeStop: PropTypes.string.isRequired,
}

export default BannerDiscount;