import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import q from 'q';
import { isEmpty } from 'lodash';
import {
  MATCHED_PAGE,
} from '../../constants/Enviroment'
import { START_CHAT_BUTTON, START_CHAT_BUTTON_SHORT_TEXT, START_CHAT_BUTTON_RANDOM_MATCH } from '../../constants/TextDefinition'
import {
  compareIdentifiers,
} from '../../utils/common'
import { updateMatchModal } from '../../actions/Like'
import {
  getPeopleMatchedMe,
  getPeopleMatchedMeFailure,
  reduceNotificationNumber,
  startChat,
} from '../../actions/userProfile'
import { sendGoogleAnalytic } from '../../actions/Enviroment'
import { BLOCK_TYPE_BLOCKED } from '../../constants/userProfile'
import { MATCHED_TYPE_SUPER } from '../../constants/Like';

class StartChatButton extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
    }
    this.handleStartChatClick = this.handleStartChatClick.bind(this);
    this.openChatWindow = this.openChatWindow.bind(this)
  }

  handleStartChatClick(identifier, is_have_free_chat, permission, message_number) {
    if (permission) {
      this.openChatWindow(identifier)
    } else {
      this.props.getPeopleMatchedMe();
      this.props.reduceNotificationNumber(identifier, message_number);
      this.openChatWindow(identifier)
    }
  }

  openChatWindow(identifier) {
    window.open(`${process.env.CHAT_SERVER_URL}chat/${identifier}`);
    this.sendGoogleAnalytic()
    this.setState({ needStartChart: false })
    this.props.updateMatchModal(false)
    if (this.props.startChartCallback) {
      this.props.startChartCallback()
    }
    this.props.startChat();
  }

  sendGoogleAnalytic() {
    const gaTrackData = {
      page_source: this.props.page_source
    };
    const eventAction = {
      eventCategory: 'StartChat',
      eventAction: 'Click',
      eventLabel: 'StartChat Click'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
  }

  isMatchMade() {
    if (
      (this.props.user.have_sent_like && this.props.user.have_sent_like_to_me) || this.props.user.is_random_matched
    ) {
      return true;
    }
    return false;
  }

  renderButton() {
    const { user, shortText, page_source, isUserActive, super_like } = this.props
    const is_have_free_chat = this.props.user_profile.matched_users.filter(user => typeof user.have_permission != 'undefine' && user.have_permission === true && user.match_type !== null).length > 0 ? true : false
    const matched_user = this.props.user_profile.matched_users.concat(this.props.user_profile.random_match_users).find(u => compareIdentifiers(u.identifier, this.props.user.identifier))
    const isBlocked = parseInt(user.block_type, 10) === BLOCK_TYPE_BLOCKED
    const is_random_matched = user.is_random_matched
    const super_match_style = typeof super_like !== 'undefined' ? true : false;

    return (
      <div>
        <button
          className={`${!super_match_style ? (this.props.noBlock ? 'btn btn--p' : 'btn btn--p btn--b') : 'btn btn--4 btn--b'}`}
          onClick={() => { this.handleStartChatClick(user.identifier, is_have_free_chat, (typeof matched_user.have_permission != 'undefined' && matched_user.have_permission ? matched_user.have_permission : false), user.new_message_number) }}
          disabled={(typeof isUserActive != 'undefined' && !isUserActive) || isBlocked}
        >
          <i className="fa fa-comments" /> {is_random_matched ? START_CHAT_BUTTON_RANDOM_MATCH : shortText ? START_CHAT_BUTTON_SHORT_TEXT : START_CHAT_BUTTON}
          {(page_source === MATCHED_PAGE && typeof isUserActive == 'undefined') &&
            typeof user.new_message_number !== 'undefined' && user.new_message_number > 0 &&
            <span className="addon">{user.new_message_number}</span>
          }
        </button>
      </div>
    )
  }

  render() {
    const { user, user_profile } = this.props;
    const { matched_users } = user_profile;
    const is_matched_user = matched_users.some(item => compareIdentifiers(item.identifier, user.identifier));
    if (isEmpty(user)) {
      return <div></div>
    }
    return (
      this.isMatchMade() || user.match_type == 3 || user.match_type == 6 || is_matched_user ? this.renderButton() : <div></div>
    )
  }
}

StartChatButton.propTypes = {
  user: PropTypes.object,
  user_profile: PropTypes.object,
  getPeopleMatchedMe: PropTypes.func,
  page_source: PropTypes.string.isRequired,
  shortText: PropTypes.bool,
  startChartCallback: PropTypes.func,
  sendGoogleAnalytic: PropTypes.func,
  startChat: PropTypes.func.isRequired,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    getPeopleMatchedMe: () => {
      const defer = q.defer()
      dispatch(getPeopleMatchedMe()).then((response) => {
          defer.resolve(response)
      }).catch(() => {
        defer.reject()
      })
      return defer.promise
    },
    reduceNotificationNumber: (identifier, number_message) => {
      dispatch(reduceNotificationNumber(identifier, number_message));
    },
    updateMatchModal: (isOpen) => {
      dispatch(updateMatchModal(isOpen))
    },
    sendGoogleAnalytic: (eventAction, ga_track_data) =>{
      dispatch(sendGoogleAnalytic(eventAction, ga_track_data))
    },
    startChat: () => {
      dispatch(startChat());
    },
  }
}

const StartChatContainer = connect(mapStateToProps, mapDispachToProps)(StartChatButton);

export default StartChatContainer;
