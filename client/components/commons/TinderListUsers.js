import React, { Component, PropTypes } from 'react'
import DetailItem from '../searches/DetailItem'
import * as UserDevice from '../../utils/UserDevice';
import TinderAdCard from './TinderAdCard'
import {
  RECOMMENDATION_PAGE, PEOPLE_LIKED_ME_PAGE
} from '../../constants/Enviroment'

class TinderListUsers extends Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.listUsers.length !== this.props.listUsers.length) {
      return true
    }
    if (nextProps.selectedUserIndex !== this.props.selectedUserIndex) {
      return true
    }
    if (nextProps.reachEnd !== this.props.reachEnd) {
      return true
    }
    if (nextProps.pageSource !== this.props.pageSource) {
      return true
    }
    if (nextProps.showAdProgram !== this.props.showAdProgram) {
      return true;
    }
    if (nextProps.cardNotPaddingTop !== this.props.cardNotPaddingTop) {
      return true;
    }
    return false;
  }

  componentWillUpdate() {
    if (this.props.selectedUserIndex > 0 &&
      this.props.selectedUserIndex >= this.props.listUsers.length - 5 &&
      !this.props.isFetching &&
      !this.props.reachEnd) {
      this.props.loadMoreData();
    }
  }

  componentDidUpdate() {
    this.preloadImage();
  }

  renderEndList() {
    let message = '';
    if (this.props.isFetching) {
      message = 'Đang tải dữ liệu!';
    }
    else if (this.props.listUsers.length === 0) {
      message = 'Không có kết quả!';
    }
    else if (this.props.selectedUserIndex >= this.props.listUsers.length) {
      message = 'Bạn đã đến cuối danh sách!';
    }
    return (
      <div className="row">
        <div className="col-xs-12">
          <div className={`well txt-center ${this.props.smallSpaceBottom ? 'well--sm' : ''}`}>
            <p>
              {message}
            </p>
          </div>
        </div>
      </div>
    )
  }

  needShowAdvetismentProgram() {
    return this.props.showAdProgram === true;
  }

  renderAdvetismentProgram() {
    return (
      <div>
        <TinderAdCard
          adProgramIndex={this.props.adProgramIndex}
          showMoreCallback={this.props.hideAdProgram}
          skipCallBack={this.props.hideAdProgram}
        />
      </div>
    )
  }

  renderUserCard() {
    let selectedUser = null;
    let nextSelectedUser = null;
    if (this.props.selectedUserIndex >= 0 && this.props.selectedUserIndex <= this.props.listUsers.length) {
      selectedUser = this.props.listUsers[this.props.selectedUserIndex]
      nextSelectedUser = this.props.listUsers[this.props.selectedUserIndex + 1] ? this.props.listUsers[this.props.selectedUserIndex + 1] : null
    }
    if (selectedUser == null) {
      return '';
    }
    const pageSource = selectedUser.is_recommendation === true ? RECOMMENDATION_PAGE : this.props.pageSource;
    return (
      <DetailItem
        data={selectedUser}
        pageSource={pageSource}
        showNextUser={this.props.showNextUser}
        ignoreUser={this.props.ignoreUser}
        dataNext={nextSelectedUser}
        renderTopCard={true}
        tinderListUsers
        cardNotPaddingTop={this.props.cardNotPaddingTop}
        forceLike={this.props.forceLike}
        showMetaInfoUser={pageSource == PEOPLE_LIKED_ME_PAGE}
      />
    )
  }

  preloadImage() {
    const users = this.props.listUsers
    const nextUserIndex = this.props.selectedUserIndex + 1;
    let nextUser = null;
    if (users &&
      users.length > nextUserIndex) {
      nextUser = users[nextUserIndex]
    }

    if (nextUser == null ||
      nextUser.user_pictures.length <= 0) {
      return;
    }

    const smallImg = new Image();
    smallImg.src = nextUser.user_pictures[0].small_image_url;

    const largeImg = new Image()
    largeImg.src = UserDevice.isHeighResolution() ?
              nextUser.user_pictures[0].extra_large_image_url :
              nextUser.user_pictures[0].large_image_url;
  }

  render() {
    if (this.needShowAdvetismentProgram()) {
      return this.renderAdvetismentProgram();
    }

    return (
      <div>
        {this.renderUserCard()}
        {this.renderEndList()}
      </div>
    )
  }
}

TinderListUsers.propTypes = {
  listUsers: PropTypes.array.isRequired,
  selectedUserIndex: PropTypes.number.isRequired,
  showNextUser: PropTypes.func.isRequired,
  loadMoreData: PropTypes.func.isRequired,
  reachEnd: PropTypes.bool.isRequired,
  pageSource: PropTypes.string.isRequired,
  ignoreUser: PropTypes.func,
  showAdProgram: PropTypes.bool,
  smallSpaceBottom: PropTypes.bool,
  hideAdProgram: PropTypes.func,
  adProgramIndex: PropTypes.number,
}

export default TinderListUsers