import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { CDN_URL } from '../../constants/Enviroment'

class BannerHasImgRight extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      referral: {
        urlImg: `${CDN_URL}/general/Referral/heart-for-banner.png`,
        featureName: "Giới thiệu bạn bè về Ymeet.me",
        description: "Mời 1 người bạn, nhận ngay <strong class='zoom'>25</strong> xu"
      },
      sideNav: {
        urlImg: `${CDN_URL}/general/Referral/heart-for-banner.png`,
        featureName: "Giới thiệu bạn bè",
        description: "Mỗi người nhận <strong class='zoom'>25</strong> xu!"
      }
    }
  }

  render() {
    // (event)=>event.preventDefault();
    const { pageSource, customClass, ...otherProps } = this.props
    const page = this.state[pageSource]
    return (
      <Link  {...otherProps} className={`banner ${customClass}`}>
         <div className="banner__content">
          <span className="banner__heading banner__heading--1 txt-blue">{page.featureName}</span>
          <p
            className="banner__sub-heading banner__sub-heading--1 txt-bold"
            dangerouslySetInnerHTML={{ __html: page.description }}
          >
          </p>
        </div>
        <img src={page.urlImg} alt="remind-with-coin-img" className="banner__icon"/>
        <div className="banner__meta txt-blue">
          <i className="fa fa-chevron-right"></i>
        </div>
      </Link>
    )
  }
}

BannerHasImgRight.propTypes = {
  pageSource: PropTypes.string.isRequired,
}

export default BannerHasImgRight
