import React, { PropTypes } from 'react'
import $ from 'jquery'

class SelectEditable extends React.Component {

  static renderSelect(value, data) {
    return data.map((val, i) => {
      if (isNaN(val)) {
        return <option className="checkbox" value={val.key} key={i}>{val.value}</option>
      }
      return <option className="checkbox" value={val} key={i}>{val}</option>
    })
  }

  handleChangeColor() {
    if ($(`#${this.props.id}`).val() || this.state.selected) {
      $(`#${this.props.id}`).removeClass('txt-red').addClass('txt-blue')
    } else {
      $(`#${this.props.id}`).removeClass('txt-blue').addClass('txt-light')
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      selected: false,
    }
    this.customClass = this.customClass.bind(this)
  }

  componentWillMount() {
    this.setState({ name: this.props.name })
    const params = {}
    params[this.props.name] = this.props.value
    if (this.props.fromWelcomePage) {
      this.props.updateUserProfile(params)
    }
  }

  handleSelectChange(e, need_update_data) {
    const params = {}
    params[this.state.name] = e.target.value
    this.setState({ value: e.target.value, selected: true })
    if (typeof this.props.onChange !== 'undefined') {
      this.props.onChange(e.target.value, this.state.name)
    }
    if (need_update_data) {
      this.props.updateUserProfile(params)
    }
    this.handleChangeColor()
  }

  getComponentValue() {
    return $(`#${this.props.id}`).val()
  }

  customClass() {
    const { pageSource, className, value } = this.props
    let class_name = `'form__input--a' ${!value ? 'txt-light' : 'txt-blue'}`
    if (typeof className !== 'undefined' && pageSource ==='WelcomePageModal') {
      return class_name = className
    }
    else if (typeof className !== 'undefined') {
      class_name = `${className} ${!value ? 'txt-light' : 'txt-blue'}`
    }
  }

  render() {
    const {
      id, name, value, data, multiple,
      disabled_event, className, select_option_text,
      disabled_update_data, read_only, pageSource
    } = this.props
    let isMultiple = false
    if (typeof multiple !== 'undefined' && multiple) {
      isMultiple = true
    }
    let class_name = 'form__input--a'
    if (typeof className !== 'undefined') {
      class_name = className
    }
    let need_update_data = true
    if (typeof disabled_update_data !== 'undefined') {
      need_update_data = false
    }
    return (
      <select
        id={id}
        name={name}
        multiple={isMultiple}
        className={this.customClass()}
        // defaultValue={value}
        value={this.state.selected ? this.state.value : value}
        disabled={read_only}
        onChange={
          !disabled_event ?
          (e) => { this.handleSelectChange(e, need_update_data) } :
          (e) => { this.handleChangeColor() }
        }
      >
        <option value="">
          {typeof select_option_text !== 'undefined' ? select_option_text : '_'}
        </option>
        { SelectEditable.renderSelect(value, data) }
      </select>
    )
  }
}

SelectEditable.propTypes = {
  // id: PropTypes.string,
  name: PropTypes.string,
  // value: PropTypes.number,
  data: PropTypes.array,
  multiple: PropTypes.bool,
  disabled_event: PropTypes.bool,
  updateUserProfile: PropTypes.func,
  onChange: PropTypes.func,
}

export default SelectEditable;
