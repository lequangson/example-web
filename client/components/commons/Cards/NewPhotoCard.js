import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { ProgressiveImage } from '../ProgressiveImage';
import LikeButton from '../LikeButton';
import StartChatButton from '../StartChatButton';
import VoteButton from '../VoteButton'
import {
  CDN_URL, PHOTO_STREAM,
} from 'constants/Enviroment';
import { 
  VOTE_BUTTON_VOTE,
} from 'constants/userProfile'
import * as UserDevice from 'utils/UserDevice';
import {
  openPhotoStreamModal,
  updateInfoPhotoStreamModal,
} from 'actions/photoStream';

class NewPhotoCard extends React.Component {
  static propTypes = {
    index: PropTypes.number,
    photo: PropTypes.object,
  }

  handleClick(photo, index) {
    this.props.updateInfoPhotoStreamModal(photo, index);
    this.props.openPhotoStreamModal()
  }

  render() {
    const {
      photo, index, currentUserName, isLoaded
    } = this.props;
    const isHeighResolution = UserDevice.isHeighResolution();

    return (
      <div key={`${photo.picture.id}_${index}`}>
        <div className="col-xs-6 mbm">
          <div className="card__link">
            <a href="#" onClick={(e) => {
                e.preventDefault()
                this.handleClick(photo, index)
              }}
            >
              <ProgressiveImage
                preview={photo.picture.small_image_url}
                src={ isHeighResolution? photo.picture.extra_large_image_url : photo.picture.large_image_url}
                render={(src, style) => <img src={src} style={style} alt={photo.user_profile.nick_name} />}
              />
            </a>
          </div>
        {
          isLoaded &&
          <VoteButton
            type={VOTE_BUTTON_VOTE}
            style={2}
            picture={photo.picture}
            target_user_id={photo.user_profile.identifier}
            page_source={PHOTO_STREAM}
            vote_from_name={currentUserName}
          />
        }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUserName: state.user_profile.current_user.nick_name, 
    isLoaded: state.photoStream.isLoaded
  };
}

function mapDispatchToProps(dispatch) {
  return {
    openPhotoStreamModal: () => dispatch(openPhotoStreamModal()),
    updateInfoPhotoStreamModal: (photo, photoIndex) => dispatch(updateInfoPhotoStreamModal(photo, photoIndex)),
  }
}

const NewPhotoCardContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPhotoCard);

export default NewPhotoCardContainer;
