import React, { PropTypes } from 'react';

import NewPhotoCard from 'components/commons/Cards/NewPhotoCard';

class List2NewPhotoCard extends React.Component {
  static propTypes = {
    photos: PropTypes.array.isRequired,
    rowIndex: PropTypes.number.isRequired
  }

  render() {
    const {
      photos, rowIndex
    } = this.props;

    if (photos.length === 0) {
      return <div />;
    }

    return (
      <div className="container">
        <div className="row">
          { photos.map((photo, index) =>
            <NewPhotoCard
              key={index}
              index={rowIndex * 2 + index}
              photo={photo}
            />
          )}
        </div>  
      </div>
    )
  }
}

export default List2NewPhotoCard;