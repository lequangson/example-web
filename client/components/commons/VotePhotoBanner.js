import React, { PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { VOTE_PHOTO_BANNER } from '../../constants/Enviroment'


class VotePhotoBanner extends React.Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    browserHistory.push("/helps/6/11")
  }

  render() {
    return (
      <div className="container">
        <div onClick={this.handleClick} className="banner mbm">
          <div className="banner__content">
            <span className="banner__heading banner__heading--1 txt-blue">Tải ảnh đẹp - Nhận xu ngay</span>
            <p className="banner__sub-heading banner__sub-heading--1 txt-bold">
              Cơ hội nhận được tới 40 xu mỗi tuần!
            </p>
          </div>
          <img src={VOTE_PHOTO_BANNER} alt="vote photo banner" className="banner__icon"/>
          <div className="banner__meta txt-blue">
            <i className="fa fa-chevron-right"></i>
          </div>
        </div>
      </div>
    )
  }
}

VotePhotoBanner.propTypes = {
  updatePreviousPage: PropTypes.func,
  gaSend: PropTypes.func,
}

export default VotePhotoBanner
