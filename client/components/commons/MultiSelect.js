import React from 'react';
import { connect } from 'react-redux'
import Select from 'react-select';
import '../../../public/styles/react-select.css';
import { updateUserProfileFailure } from '../../actions/userProfile'

class MultiSelect extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      options: [],
      value: [],
      name: '',
      max_size: 0,
      max_size_msg: '',
    }
    this.handleSelectChange = this.handleSelectChange.bind(this)
  }

  componentWillMount() {
    const { data, name, value, max_size, max_size_msg } = this.props
    const options = data.map(d => ({ "label": d.value, "value": d.key.toString() }))
    const strValue = this.props.value.map(d => { return d.key}).join(",")
    this.setState({
      options,
      name,
      value: strValue,
      max_size,
      max_size_msg,
    })
  }

  handleSelectChange(value) {
    const selected_values = value.split(",")
    const { max_size, max_size_msg } = this.state
    if (max_size && selected_values.length > max_size) {
      updateUserProfileFailure(max_size_msg)
      return
    }
    this.setState({ value });
    const params = {};
    params[this.state.name] = []
    for (let i = 0; i < selected_values.length; i += 1) {
      params[this.state.name].push(selected_values[i])
    }
    if (params[this.state.name][0] == null) {
      params[this.state.name].push('');
    }
    this.props.updateUserProfile(params)
  }

  value() {
    return this.state.value
  }

  render() {
    return (
      <Select
        id={this.props.id}
        name={this.state.name}
        multi
        simpleValue
        searchable={false}
        value={this.state.value}
        placeholder={this.props.placeholder || 'Chưa cập nhật'}
        options={this.state.options}
        onChange={this.handleSelectChange}
      />
    );
  }
}

MultiSelect.propTypes = {
  data: React.PropTypes.array,
  name: React.PropTypes.string,
  search: React.PropTypes.object,
  placeholder: React.PropTypes.string,
  updateUserProfile: React.PropTypes.func,
}

export default MultiSelect;
