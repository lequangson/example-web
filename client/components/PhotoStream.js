import React, { Component } from 'react'
import { connect } from 'react-redux'
import Modal from 'components/modals/Modal'
import { ProgressiveImage } from './commons/ProgressiveImage'
import LikeButton from './commons/LikeButton'
import StartChatButton from './commons/StartChatButton'
import VotePhotoBanner from './commons/VotePhotoBanner'
import InfiniteListItem from 'components/infinite-list-items/InfiniteListItem';
import _ from 'lodash'
import {
  getPhotoStream,
  openPhotoStreamModal,
  closePhotoStreamModal,
  updateInfoPhotoStreamModal,
  getBestPhotos,
} from '../actions/photoStream'
import { PHOTO_STREAM } from '../constants/Enviroment'
import { DISPLAY_BANNER_AFTER_PHOTO_NO } from '../constants/photoStream'
import { VOTE_BUTTON_VOTE, VOTE_BUTTON_VIEW, AVATAR_PICTURE, OTHER_PICTURE } from '../constants/userProfile'
import VoteButton from './commons/VoteButton'
import * as ymmStorage from '../utils/ymmStorage';
import { shuffleArray } from '../utils/common'
import * as UserDevice from '../utils/UserDevice';
import GoogleOptimize from '../utils/google_analytic/google_optimize';

import {
  NEW_PHOTO_CARD
} from 'components/infinite-list-items/Constant';

class PhotoStream extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isSendingLike: false,
      shuffleBanners: [],
    }
    this.handleClick = this.handleClick.bind(this)
    this.showNextUser = this.showNextUser.bind(this)
  }

  componentWillMount() {
    window.scroll(0,0)
    this.props.getPhotoStream(1);
    if (!this.props.BestVotePhoto.banners || this.props.BestVotePhoto.banners.length === 0) {
      this.props.getBestPhotos();
    }
    this.props.updatePreviousPage(PHOTO_STREAM);
  }

  componentWillReceiveProps() {
    if(this.props.params.id && this.props.photoStream.photos.length > 0 && this.props.photoStream.isShowPictureById) {
      const photo = this.props.photoStream.photos.find(photo => photo.picture.id == this.props.params.id) || {}
      if (!_.isEmpty(photo)) {
        const index = this.props.photoStream.photos.findIndex(p => p.picture.id == photo.picture.id)
        this.props.updateInfoPhotoStreamModal(photo, index)
        this.props.openPhotoStreamModal()
      }
    }
    if (this.state.shuffleBanners.length === 0 && this.props.BestVotePhoto.banners.length > 0) {
      this.setState({
        shuffleBanners: shuffleArray(this.props.BestVotePhoto.banners),
      })
    }
  }

  @GoogleOptimize
  componentDidMount() {}

  handleClick(photo, index) {
    this.props.updateInfoPhotoStreamModal(photo, index);
    this.props.openPhotoStreamModal()
  }

  showNextUser(index) {
    const photoStream = this.props.photoStream
    const photo = photoStream.photos[index]
    this.props.updateInfoPhotoStreamModal(photo, index)
  }

  markSendingLike(isSendingLike) {
    this.setState({ isSendingLike })
  }

  isEqual(a, b) {
    return a === b
  }

  loadMorePhotoCallback = () => {
    this.props.getPhotoStream(this.props.photoStream.pageIndex);
  }

  render() {
    const props = this.props
    const current_user = this.props.user_profile.current_user
    const { 
      photos, isLoaded, modalInfo, currentPhotoIndex, isModalOpen, reachEnd, 
    } = this.props.photoStream
    const banners = this.state.shuffleBanners
    const isHeighResolution = UserDevice.isHeighResolution()
    const imgUrl = !_.isEmpty(modalInfo) ? isHeighResolution  ? modalInfo.picture.extra_large_image_url : modalInfo.picture.large_image_url : '';

    return (
      <div>
        <VotePhotoBanner />
        <div className="container">
          <div className="row">
            <InfiniteListItem
              listItems={photos}
              loadingDataStatus={!isLoaded}
              reachEnd={reachEnd}
              loadMoreDataCallback={this.loadMorePhotoCallback}
              viewMode={NEW_PHOTO_CARD}
              pageSource={PHOTO_STREAM}
              listTipsBanner={banners}
              numberRowToShowTipBanner={DISPLAY_BANNER_AFTER_PHOTO_NO}
            />
          </div>
        </div>
        <Modal
          transitionName="modal"
          isOpen={isModalOpen}
          onRequestClose={this.props.closePhotoStreamModal}
          className="modal__content"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
        {isModalOpen &&
          <div className="row">
            <div className="col-xs-12 mbm">
              <div className="modal__frame">
                <img src={imgUrl} />
                <VoteButton
                  type={VOTE_BUTTON_VOTE}
                  style={1}
                  picture={modalInfo.picture}
                  target_user_id={modalInfo.user_profile.identifier}
                  page_source={PHOTO_STREAM}
                />
                {!this.isEqual(currentPhotoIndex, 0) && (
                  <button
                    className="pagi pagi--prev pagi--2"
                    onClick={() => this.showNextUser(currentPhotoIndex - 1)}
                    >
                      <i className="fa fa-chevron-left"></i>
                    </button>
                )}

                {!this.isEqual(currentPhotoIndex, (photos.length - 1)) && (
                  <button
                    className="pagi pagi--next pagi--2"
                    onClick={() => this.showNextUser(currentPhotoIndex + 1)}
                    >
                      <i className="fa fa-chevron-right"></i>
                    </button>
                )}
              </div>
            </div>
            <div className="col-xs-6 txt-blue txt-bold mbm">
              {modalInfo.user_profile.nick_name} - {modalInfo.user_profile.age}
            </div>
            <div className="col-xs-6 txt-bold txt-right mbm">
              {modalInfo.user_profile.residence}
            </div>
            <div className="col-xs-12">
            <LikeButton
              user={modalInfo.user_profile}
              user_profile={current_user}
              isSendingLike={this.state.isSendingLike}
              markSendingLike={v => this.markSendingLike(v)}
              forceLike
              msg=''
            />
            <StartChatButton user={modalInfo.user_profile} shortText={false} page_source={PHOTO_STREAM}/>
            </div>
            <button
              className="modal__btn-close"
              onClick={this.props.closePhotoStreamModal}
            >
              <i className="fa fa-times" />
            </button>

          </div>
        }
        </Modal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    getPhotoStream: (pageIndex) => dispatch(getPhotoStream(pageIndex)),
    openPhotoStreamModal: () => dispatch(openPhotoStreamModal()),
    closePhotoStreamModal: () => dispatch(closePhotoStreamModal()),
    updateInfoPhotoStreamModal: (photo, photoIndex) => dispatch(updateInfoPhotoStreamModal(photo, photoIndex)),
    getBestPhotos: () => dispatch(getBestPhotos()),
  }
}

const PhotoStreamContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotoStream)

export default PhotoStreamContainer
