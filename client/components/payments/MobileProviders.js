import React, { Component } from 'react'
import { browserHistory } from 'react-router';
import Packages from './Packages';
import MobileCardsInfo from './MobileCardsInfo';
import { CDN_URL } from 'constants/Enviroment';

class MobileProviders extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(provider_code) {
    this.props.updatePaymentState('provider_code', provider_code);
    this.refs[provider_code].checked = true;
  }

  render() {
    return (
      <div className="nav-1__content">
        <div className="col-xs-12">
          <div className="padding-b20 padding-t20"><span>Bước 2: Chọn một nhà mạng</span></div>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('VTEL')}}>
          <div className="img-payment"><img src={`${CDN_URL}/general/viettel.png`} /></div>
          <label className="form__input--d txt-center radio-large">
            <input type="radio" name="mobile" id="provider_VTEL" ref="VTEL" />
            <div className="fake-radio"></div>
          </label>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('VMS')}}>
          <div className="img-payment"><img src={`${CDN_URL}/general/mobiphone.png`} /></div>
          <label className="form__input--d txt-center radio-large">
            <input type="radio" id="provider_VMS" name="mobile" ref="VMS" />
            <div className="fake-radio"></div>
          </label>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('GPC')}}>
          <div className="img-payment"><img src={`${CDN_URL}/general/Payment/vinaphone.png`} /></div>
          <label className="form__input--d txt-center radio-large">
            <input type="radio" name="mobile" id="provider_GPC" ref="GPC" />
            <div className="fake-radio"></div>
          </label>
        </div>
        <MobileCardsInfo {...this.props} />
      </div>
    )
  }
}

export default MobileProviders
