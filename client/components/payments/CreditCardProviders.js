import React, { Component } from 'react'
import { browserHistory } from 'react-router';
import Packages from './Packages';
import UserInfo from './UserInfo';
import $ from 'jquery';
import {
  CDN_URL
} from 'constants/Enviroment'

class CreditCardProviders extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(package_id) {
    this.props.updatePaymentState('provider_code', package_id);
    $(`#${package_id}`).click();
  }

  render() {
    return (
      <div className="nav-1__content">
        <div className="col-xs-12">
          <div className="padding-b20 padding-t20"><span>Bước 2: Chọn một loại thẻ</span></div>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('Visa')}}>
          <div className="img-payment"><img src={`${CDN_URL}/general/visa.png`} /></div>
          <label className="form__input--d txt-center radio-large">
            <input type="radio" name="Credit" id="Visa" onClick={() => {this.handleClick('Visa')}} />
            <div className="fake-radio"></div>
          </label>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('Master')}}>
          <div className="img-payment"><img src={`${CDN_URL}/general/master_card.png`} /></div>
          <label className="form__input--d txt-center radio-large">
            <input type="radio" name="Credit" id="Master" onClick={() => {this.handleClick('Master')}} />
            <div className="fake-radio"></div>
          </label>
        </div>
        <UserInfo {...this.props} />
      </div>
    )
  }
}

export default CreditCardProviders
