import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {
  PAYMENT_METHOD_BANK_TRANSFER,
  PAYMENT_METHOD_CREDIT_CARD,
  PAYMENT_METHOD_MOBILE_CARD
} from '../../constants/Payment';

class SelectMethod extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillMount() {
    switch(this.props.method) {
      case 'bank-card':
        this.props.updatePaymentState('payment_method', PAYMENT_METHOD_BANK_TRANSFER)
      break;
      case 'credit-card':
        this.props.updatePaymentState('payment_method', PAYMENT_METHOD_CREDIT_CARD)
      break;
      default:
        this.props.updatePaymentState('payment_method', PAYMENT_METHOD_MOBILE_CARD)
      break;
    }
    this.props.updatePaymentState('provider_code', '');
    this.props.updatePaymentState('order', {});
    this.props.updatePaymentState('shopping_cart', []);
    this.props.updatePaymentState('customer_name', '');
    this.props.updatePaymentState('customer_phone_number', '');
    this.props.updatePaymentState('mobile_card_info', [
      {
        input_id: 1,
        card_code: '',
        card_id: '',
        payment_info_id: '',
        payment_status: null
      }
    ]);
  }

  handleClick(payment_method) {
    // this.props.updatePaymentState('payment_method', payment_method)
  }
  render() {
    return <div className="nav-1__tabs">
      <Link className={`nav-1__link ${this.props.method == 'mobile-card' ? 'active' : ''}`} to={`/payment/${this.props.params.type}/mobile-card`} > Thẻ cào</Link>
      <Link className={`nav-1__link ${this.props.method == 'bank-card' ? 'active' : ''}`} to={`/payment/${this.props.params.type}/bank-card`} >Thẻ ngân hàng</Link>
      <Link className={`nav-1__link ${this.props.method == 'credit-card' ? 'active' : ''}`} to={`/payment/${this.props.params.type}/credit-card`} >Credit Card</Link>
    </div>
  }
}

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {}
}

const SelectMethodContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(SelectMethod)


export default SelectMethodContainer