import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { ICON_404 } from 'constants/Enviroment'
import { PAYMENT_PACKAGE_TYPE_COIN } from 'constants/Payment'
import Header from 'components/headers/Header';

class Failure extends Component {
  render() {
    const { error_message } = this.props.Payment;
    return(
      <div className="container">
        <Header {...this.props} /> <br /> <br />
        <div className="row txt-center l-flex-v-sp" style={{minHeight: 90 + 'vh' }}>
          <div>
            <img src={ICON_404} alt="" />
            {
              error_message == '' &&
              <p className="txt-light">Giao dịch thất bại!</p>
            }
            {error_message != '' && <p className="txt-red">{error_message}</p>} <br />
            <Link to={(this.props.Payment && this.props.Payment.shopping_cart && this.props.Payment.shopping_cart.shopping_cart && this.props.Payment.shopping_cart.shopping_cart.package_type == PAYMENT_PACKAGE_TYPE_COIN)? '/payment/coin' : '/payment/upgrade'}>
              Quay lại trang thanh toán!
            </Link>
          </div>
            {
              error_message == '' &&
              <div className="txt-center padding-b30">
                <div className="txt-light">Mọi thắc mắc xin liên hệ</div>
                <p className="txt-red txt-sm txt-bold">Hotline(7h-22h): 0936 332 079</p>
              </div>
            }
        </div>
      </div>
    )
  }
}
;

function mapStateToProps(state) {
  return state
}

const FailureContainer = connect(mapStateToProps)(Failure)

export default FailureContainer