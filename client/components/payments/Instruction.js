import React from 'react'
import { connect } from 'react-redux'
import DropdownPanel from '../commons/DropdownPanel';
import { Link } from 'react-router'
import {isEmpty} from 'lodash';
import HeaderContainer from '../../containers/HeaderContainer'
import {
  CDN_URL
} from 'constants/Enviroment'

const PaymentInstruction = props => {
  const { user_profile } = props;
  const { current_user } = user_profile;
  if (isEmpty(current_user)) {
    return <div></div>
  }
  return (
    <div className="site-content">
      <HeaderContainer {...props} />
      <div className={`panel`}>
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3 className="txt-heading">Nâng cấp tài khoản</h3>
              <p className="text-indent">Trở thành VIP ngay hôm nay để trò chuyện thoả thích với tất cả các thành viên trên Ymeet.me và trải nghiệm hẹn hò một cách hiệu quả nhất!</p>
              <ol className="padding-l30">
                {current_user.gender === 'male' && <li> Chat được với tất cả trong<span className="txt-blue padding-l5"><i className="fa fa-comment"></i> Trò chuyện</span></li>}
                <li>Đứng đầu trên mọi danh sách <span className="txt-blue padding-rl5">tìm kiếm</span></li>
                <li>Gây ấn tượng với người mình <span className="txt-blue padding-l5"><i className="fa fa-thumbs-up"></i> thích</span></li>
                <li>Không giới hạn số phòng<span className="txt-blue padding-l5"><i className="fa fa-comment"></i> Trò chuyện</span></li>
                <li>Mở ngay tính năng <span className="txt-blue padding-rl5">tìm kiếm</span> nâng cao</li>
                <li> Xem ai đang thầm  <span className="txt-blue padding-rl5">quan tâm</span>mình</li>
                <li> Tiếp cận nhiều người <span className="txt-blue padding-rl5"> đặc biệt</span>trong ngày hơn </li>
              </ol>
              <p className="text-indent"><span className="txt-blue padding-rl5">Ymeet.me</span> ĐẢM BẢO 100% bạn sẽ tìm được Người ấy <span className="txt-blue padding-rl5">CHỈ TRONG 90 NGÀY</span> nếu bạn làm theo những bí kíp sau:</p>
              <ol className="padding-l30">
                <li>Sử dụng gói VIP 3 tháng của <span className="txt-blue padding-rl5">Ymeet.me</span></li>
                <li>Đăng nhập <span className="txt-blue padding-rl5">Ymeet.me</span> liên tục mỗi ngày</li>
                <li>Gửi đi ít nhất 30 lượt thích liên tục mỗi ngày</li>
                <li>Thích lại tất cả những người đã thích bạn</li>
                <li> Chủ động nhắn tin với những người mình đã ghép đôi</li>
                <li> Nhanh chóng trả lời tất cả các cuộc trò chuyện</li>
                <li> Hẹn người ấy đi chơi sau 3 ngày chuyện trò</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xs-12 mbm">
        <Link to="/payment/upgrade"
          className="btn btn--p btn--b"
        >
          Nâng cấp tài khoản ngay
        </Link>
      </div>
      <DropdownPanel
        panelTitle="Hướng dẫn nâng cấp tài khoản"
        >
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="txt-blue txt-bold">Cách 1: Sử dụng thẻ cào điện thoại</div>
          <ol className="padding-l30">
            <li>  Click vào nút “Nâng cấp tài khoản ngay”.</li>
            <li> Chọn phương thức thanh toán “Thẻ cào”</li>
            <li>Chọn gói nâng cấp</li>
            <li> Chọn nhà mạng tương ứng với thẻ nạp của bạn. (Viettel, Mobifone hoặc Vinaphone)</li>
            <li> Nhập mã số thẻ và số seri
              <div className="padding-t10"><img src={`${CDN_URL}/general/Payment/Guide/mobi_guide.png`} alt="mobi_guide.png" /> </div>
            </li>
            <li> Click chọn “Thanh toán” để hoàn tất.</li>
          </ol>
          <div className="txt-underline">Chú ý: </div>
          <p>Hệ thống sẽ thông báo nâng cấp thành công nếu bạn nhập đúng thông tin trên thẻ cào, báo lỗi nếu bạn nhập sai bất kỳ 1 ô nào hoặc thẻ không hợp lệ.</p>
          <div className="txt-blue txt-bold">Cách 2: Sử dụng thẻ ngân hàng</div>
          <ol className="padding-l30">
            <li>  Click vào nút “Nâng cấp tài khoản ngay”.</li>
            <li> Chọn phương thức thanh toán “Thẻ ngân hàng”</li>
            <li>Chọn gói nâng cấp</li>
            <li>  Chọn ngân hàng bạn muốn sử dụng dịch vụ</li>
            <li> Nhập tên và số điện thoại của bạn</li>
            <li> Click chọn “Thanh toán” để để chuyển sang hệ thống thanh toán VTC Pay</li>
            <li> Nhập đầy đủ thông tin thẻ
              <div className="text-indent">
                <p>- Tên chủ thẻ</p>
                <p>- Số thẻ</p>
                <p>- Ngày phát hành</p>
              </div>
            </li>
            <li> Chờ VTC Pay thông báo giao dịch thành công. Click chọn [Đồng ý] để hoàn tất giao dịch</li>
          </ol>
          <div className="txt-underline">Chú ý: </div>
          <p>- Tùy ngân hàng sẽ có giao diện giao dịch khác nhau.</p>
          <p>- Sau khi xác thực giao dịch trên website riêng của ngân hàng đã thành công, vui lòng không đóng trình duyệt mà để trình duyệt tự động chuyển về Ymeet.me</p>
          <p>- Để sử dụng cách này, thẻ ngân hàng của bạn cần đăng ký Internet-Banking hoặc dịch vụ thanh toán trực tuyến tại ngân hàng.
          </p>

          <div className="txt-blue txt-bold">Cách 3: Sử dụng thẻ Credit Card</div>
          <ol className="padding-l30">
            <li>  Click vào nút “Nâng cấp tài khoản ngay”.</li>
            <li> Chọn phương thức thanh toán “Credit Card”</li>
            <li>Chọn gói nâng cấp</li>
            <li>  Chọn loại thẻ (Visa hoặc MasterCard)</li>
            <li> Nhập tên và số điện thoại của bạn</li>
            <li> Click chọn “Thanh toán” để để chuyển sang hệ thống thanh toán VTC Pay</li>
            <li> Nhập đầy đủ thông tin thẻ
              <div className="padding-t10"><img src={`${CDN_URL}/general/Payment/Guide/credit_guide.png`} alt="credit_guide.png" /> </div>
            </li>
            <li> Chờ VTC Pay thông báo giao dịch thành công. Lưu ý không được đóng trình duyệt mà chọn [Đồng ý] để hoàn tất giao dịch.</li>
          </ol>
          <div className="txt-underline">Chú ý: </div>
          <p>Khi nâng cấp tài khoản theo hình thức này, trạng thái tài khoản của bạn thông thường sẽ được cập nhật sau khoảng 20-30 phút.</p>
          <Link to="/payment/upgrade" className="btn btn--b btn--p mbm">Nâng cấp tài khoản ngay</Link>
          <p className="txt-red txt-sm txt-center">Hotline(7h-22h): 0936 332 079</p>
        </div>
      </DropdownPanel>
      </div>
  )
}

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {}
}

const PaymentInstructionContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(PaymentInstruction)


export default PaymentInstructionContainer