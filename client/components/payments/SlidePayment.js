import React, { PropTypes } from 'react'
import { connect } from 'react-redux';
import $ from 'jquery'
import slick from '../../../public/scripts/slick.min.js'
import { isEmpty } from 'lodash';
import { CDN_URL } from '../../constants/Enviroment'
import {
  PAYMENT_PACKAGE_TYPE_COIN, PAYMENT_PACKAGE_TYPE_MONTHLY,
} from '../../constants/Payment'

export default class SlidePayment extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      coin_package_instructions: [
        {
          male_icon_url : `${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`,
          female_icon_url : `${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`,
          featureName: 'Trò chuyện thỏa thích',
          description: 'Mở chát không giới hạn với người <br /> mình đã ghép đôi <br /> Trò chuyện trực tiếp mà không cần ghép đôi'
        },
        {
          male_icon_url : `${CDN_URL}/general/Payment/Coins/boost-male.png`,
          female_icon_url : `${CDN_URL}/general/Payment/Coins/boost-female.png`,
          featureName: 'Nổi bật ngay',
          description: 'Bạn sẽ nổi bật giữa hàng ngàn người khác nhận <br /> nhiều lượt thích hơn, được ghép đôi nhiều hơn'
        },
        {
          male_icon_url : `${CDN_URL}/general/random-sign.png`,
          female_icon_url : `${CDN_URL}/general/random-sign.png`,
          featureName: 'Ghép đôi ngẫu nhiên',
          description: 'Ghép đôi ngẫu nhiên để tâm sự với người lạ trong 24h. <br /> Biết đâu Xu se duyên giúp bạn!'
        }
      ],
      monthly_package_instructions: [
        {
          male_icon_url : `${CDN_URL}/general/Payment/Coins/lock-icon.png`,
          female_icon_url : `${CDN_URL}/general/Payment/Coins/lock-icon.png`,
          featureName: 'Trò chuyện thỏa thích',
          description: 'Trò chuyện không giới hạn với tất cả <br /> những thành viên đã kết đôi <br /> Xem được danh sách ai quan tâm bạn <br /> Nhắc cô ấy thích lại bạn'
        },
        {
          male_icon_url : `${CDN_URL}/general/Payment/Coins/boost-male.png`,
          female_icon_url : `${CDN_URL}/general/Payment/Coins/boost-female.png`,
          featureName: 'Nổi bật ngay',
          description: 'Bạn sẽ nổi bật giữa hàng ngàn người khác nhận <br /> nhiều lượt thích hơn, được ghép đôi nhiều hơn'
        },
        {
          male_icon_url : `${CDN_URL}/general/Payment/Coins/search-icon.png`,
          female_icon_url : `${CDN_URL}/general/Payment/Coins/search-icon.png`,
          featureName: 'Tìm kiếm nâng cao',
          description: 'Bộ lọc nhiều tiêu chí hơn, giúp tìm kiếm nửa kia <br /> dễ dàng và chính xác hơn'
        }
      ]
    }
  }

  _carousel() {

    // payment carousel
    const carousel = $('.carousel-payment');
    if (!carousel) {
      return;
    }
    carousel.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 7000,
      initialSlide: 0,
      focusOnSelect: true,
      dots: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            dots: true
          },
        },
        {
          breakpoint: 425,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    })
  }

  _unCarousel() {
    //if ($('.carousel-form .carousel-payment')) {
    if ($('.carousel-payment')) {
      $('.carousel-payment').slick('unslick')
    }
  }

  componentDidMount() {
    this._carousel()
  }


  componentWillUpdate(nextProps, nextState) {
    if (nextProps.package_type !== this.props.package_type ||
      nextProps.gender !== this.props.gender) {  
      this._unCarousel()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.package_type !== this.props.package_type ||
      prevProps.gender !== this.props.gender) {  
      this._carousel()
    }
  }


  render() {
    const data = this.props.package_type === 'coin'? this.state.coin_package_instructions : this.state.monthly_package_instructions;
    return (
      <div className="panel mbm">
        <div className="clearfix">
          <div className="col-xs-12">
            <div className="carousel-payment" >
              {
                !isEmpty(data) &&
                data.map((value, i) => {
                  return <div className="txt-center padding-r10" key={i}>
                      <div className="row">
                        <div className="col-xs-2 col-xs-offset-5">    
                          <div className="card__link mbl">
                            <img src={this.props.gender === 'male' ? value.male_icon_url : value.female_icon_url} alt="ymeet.me" className="mbs"/>
                          </div>
                        </div>
                      </div>
                      <div className="txt-blue txt-uppercase txt-xlg txt-medium">{value.featureName}</div>
                      <div className="" dangerouslySetInnerHTML={{ __html: value.description }}></div>
                    </div>
                  })
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

SlidePayment.propTypes = {
  package_type: PropTypes.string.isRequired,
  gender: PropTypes.string.isRequired,
}
