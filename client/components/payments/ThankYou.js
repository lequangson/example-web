import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';
import q from 'q';
import Dropzone from 'react-dropzone'
import moment from 'moment';
import { isEmpty } from 'lodash';
import { CDN_URL, PAYMENT_PAGE, PAYMENT_INTENTION_BOOST_RANK, BOOST_RANK_COST, RANDOM_MATCH_COST,
  PAYMENT_INTENTION_UNLOCK_WHO_LIKE_ME, UNLOCK_WHO_LIKE_ME_COST, PAYMENT_INTENTION_RANDOM_MATCH
} from 'constants/Enviroment';
import Header from 'components/headers/Header';
import PackagePaymentSteps from 'components/new-payments/PackagePaymentSteps';
import { getCurrentUser, getPeopleMatchedMe } from 'actions/userProfile';
import { getPaymentPackages, unlockChat, coinConsume } from 'actions/Payment'
import * as ymmStorage from 'utils/ymmStorage';
import BoostInRankButton from 'components/commons/BoostInRankButton';
import CoinConsumeButton from 'components/commons/CoinConsumeButton';
import RandomMatchButton from 'components/commons/RandomMatchButton';
import { NUMBER_COINS_NEED_FOR_SUPER_LIKE } from 'constants/Like';
import { VERIFY_PICTURE_TYPE, ACTIVE_USER } from 'constants/userProfile'
import socket from 'utils/socket';
import * as sendBannerNotification from 'utils/notifications/banner';
import { TAROT_COST } from 'constants/Tarot';
import { PAYMENT_RESULT } from 'constants/Payment';
import ContinueButton from './ContinueButton';

class Thankyou  extends Component {
    constructor(props) {
      super(props);
      this.state = {
        clickedBackToChat: false,
        accept_remind_with_coin: false,
      }
      this.handleUnlockChatButton = this.handleUnlockChatButton.bind(this);
      this.backToChat = this.backToChat.bind(this);
      this.forceChatClick = this.forceChatClick.bind(this);
    }

    componentWillMount() {
      const { order } = this.props.Payment;
      if (isEmpty(order)) {
        browserHistory.push('/whathot');
      } else {
        this.props.getCurrentUser();
        this.props.getPaymentPackages(order.package_type)
      }
    }

    componentWillUnmount() {
      ymmStorage.removeItem('Payment_Intention');
      ymmStorage.removeItem('require_payment_url');
      ymmStorage.removeItem('chat_to_username');
    }

    renderCoinConsumeButton() {
      const paymentIntention = ymmStorage.getItem('Payment_Intention')
      const { coins } = this.props.user_profile.current_user

      switch (paymentIntention) {
        case PAYMENT_INTENTION_UNLOCK_WHO_LIKE_ME:
          if (coins < UNLOCK_WHO_LIKE_ME_COST) {
            return (
              <div className="col-xs-6 col-xs-offset-3">
                <Link to="/payment/coin" className="btn btn--b btn--p col-xs-12" >Nạp thêm xu</Link>
              </div>
            )
          }
          else {
            return (
              <CoinConsumeButton current_user={this.props.user_profile.current_user} coinConsumeCallBack={() => {browserHistory.push('/people-like-me')}} page={'THANK_YOU'} gaSend={this.props.gaSend}/>
            )
          }
        case PAYMENT_INTENTION_BOOST_RANK :
          if (coins < BOOST_RANK_COST) {
            return (
              <div className="col-xs-6 col-xs-offset-3">
                <Link to="/payment/coin" className="btn btn--b btn--p col-xs-12" >Nạp thêm xu</Link>
              </div>
            )
          }
          else {
            return (
              <BoostInRankButton current_user={this.props.user_profile.current_user} boostRankCallBack={() => {browserHistory.push('/search/new')}} page={'THANK_YOU'} gaSend={this.props.gaSend}/>
            )
          }
        case PAYMENT_INTENTION_RANDOM_MATCH :
          if (coins < RANDOM_MATCH_COST) {
            return (
              <div className="col-xs-6 col-xs-offset-3">
                <Link to="/payment/coin" className="btn btn--b btn--p col-xs-12" >Nạp thêm xu</Link>
              </div>
            )
          }
          else {
            return (
              <RandomMatchButton className='btn btn--b btn--p' buttonName='Ghép đôi ngẫu nhiên nào' {...this.props} />
            )
          }
        case 'REMIND_BANNER_WITH_COIN':
          if (coins < 100) {
            return (
              <Link to="/payment/coin" className="btn btn--b btn--p col-xs-12" >Nạp thêm xu</Link>
            )
          }
          else {
            return (
              <button onClick={() => {this.handleRemindWithCoinClick()}} className="btn btn--b btn--p col-xs-12">Nhắc Người Ấy!</button>
            )
          }
        case 'LUCKY_SPINNER':
          return (
            <div className="col-xs-6 col-xs-offset-3">
              <Link to="/lucky-spinner" className="btn btn--b btn--p col-xs-12" >Chơi tiếp</Link>
            </div>
          )
        case 'SUPER_LIKE_SPINNER':
          return (
            <div className="col-xs-6 col-xs-offset-3">
              <Link to="/super-like-spinner" className="btn btn--b btn--p col-xs-12" >Chơi tiếp</Link>
            </div>
          )
        default :
          return '';
      }
    }

    renderMessage() {
      const { coins } = this.props.user_profile.current_user
      const paymentIntention = ymmStorage.getItem('Payment_Intention')
      switch (paymentIntention) {
        case PAYMENT_INTENTION_UNLOCK_WHO_LIKE_ME:
          if (coins < UNLOCK_WHO_LIKE_ME_COST) {
            return (
              <div>
                Số xu hiện có chưa đủ để mở danh sách Ai thích tôi. <br />
                Hãy nạp thêm xu để trải nghiệm tính năng này nhé!<br /><br /><br />
              </div>
            )
          }
          else {
            return (
              <div>
                Cảm ơn bạn đã tin dùng dịch vụ của chúng tôi! <br />
                Chúc bạn có những giờ phút hẹn hò vui vẻ!<br /><br /><br />
              </div>
            )
          }
        case 'REMIND_BANNER_WITH_COIN':
          if (coins < 100) {
            return (
              <div>
                Số xu hiện có chưa đủ để có thể dùng Nhắc Người Ấy.<br />
                Hãy nạp thêm xu để trải  nghiệm tính năng này nhé!<br />
              </div>
            )
          }
          else {
            return (
              <div>
                Cảm ơn bạn đã tin dùng dịch vụ của chúng tôi! <br />
                Chúc bạn có những giờ phút hẹn hò vui vẻ!<br />
              </div>
            )
          }
        default:
          return (
            <div className="mbm">
              Cảm ơn bạn đã tin dùng dịch vụ của chúng tôi! <br />
              Chúc bạn có những giờ phút hẹn hò vui vẻ!<br />
            </div>
          )
      }
    }

    renderBuyCoin() {
      const props = this.props;
      const { Payment, user_profile } = props;
      const { order } = Payment;
      const { current_user } = user_profile ;
      if (order.is_license) {
        return '';
      }
      return (
        <div className="txt-lg txt-light txt-center">
          Chúc mừng bạn vừa giao dịch thành công!<br /><br />
          <div className="txt-blue">
            Bạn đã thanh toán gói {order.coins} xu trị giá {order.amount} VND
            <div>TÀI KHOẢN CỦA BẠN CÓ {current_user.coins} XU</div>
          </div> <br />
          {this.renderMessage()}
          {this.renderCoinConsumeButton()}
        </div>
      )
    }

    renderButtonForMonthLicense() {
      const paymentIntention = ymmStorage.getItem('Payment_Intention')

      switch (paymentIntention) {
        case PAYMENT_INTENTION_UNLOCK_WHO_LIKE_ME:
          return <ContinueButton />
        case PAYMENT_INTENTION_BOOST_RANK :
          return (
            <BoostInRankButton current_user={this.props.user_profile.current_user} boostRankCallBack={() => {browserHistory.push('/search/new')}} page={'THANK_YOU'} gaSend={this.props.gaSend}/>
          )
        case PAYMENT_INTENTION_RANDOM_MATCH :
          return (
            <RandomMatchButton className='btn btn--b btn--p' buttonName='Ghép đôi ngẫu nhiên nào' {...this.props} />
          )
        case 'REMIND_BANNER_WITH_COIN':
          return (
            <button onClick={() => {this.handleRemindWithCoinClick()}} className="btn btn--b btn--p col-xs-12">Nhắc Người Ấy!</button>
          )
        default :
          return <ContinueButton />
      }
    }

    renderBuyMonthLicense() {
      const props = this.props;
      const paymentIntention = ymmStorage.getItem('Payment_Intention')
      const remind_banner_with_coin = (paymentIntention == 'REMIND_BANNER_WITH_COIN') || false;
      const { Payment } = props;
      const { order } = Payment;
      const package_name = Payment.packages.find(pack_name => pack_name.id === order.package_id) || []

      if (!order.is_license) {
        return '';
      }
      return (
        <div className="txt-lg txt-light txt-center mbm">
          Chúc mừng bạn vừa giao dịch thành công!<br /><br />
          <div className="txt-blue">
            Bạn đã thanh toán gói {package_name.name} trị giá {order.total_amount} VND
            <div>TÀI KHOẢN CỦA BẠN ĐÃ ĐƯỢC NÂNG CẤP</div>
            <div>Ngày hết hạn: {moment(order.end_at).format('DD/MM/YYYY')}.</div>
          </div> <br />
          <div className="mbm">
            Chúc bạn có những giờ phút hẹn hò vui vẻ!<br /><br /><br />
            </div>
          {this.renderButtonForMonthLicense()}
        </div>
      )
    }

    renderBackToChatApp() {
      const props = this.props;
      const { Payment, user_profile } = props;
      const { order } = Payment;
      const { current_user } = user_profile ;
      const chat_url = ymmStorage.getItem('require_payment_url');
      const chat_to_username = ymmStorage.getItem('chat_to_username');
      const number_coin_to_unlock_chat = ymmStorage.getItem('number_coin_to_unlock');
      // const number_coin_to_unlock_chat = 99999999999999;
      if (order.is_license) {
        return '';
      }
      return (
        <div className="txt-lg txt-light txt-center">
          Chúc mừng bạn vừa giao dịch thành công!<br /><br />
          <div className="txt-blue">
            Bạn đã thanh toán gói {order.coins} xu trị giá {order.amount} VND
            <div>TÀI KHOẢN CỦA BẠN CÓ {current_user.coins} XU</div>
          </div> <br />
          {current_user.coins >= number_coin_to_unlock_chat && <span>
            Cảm ơn bạn đã tin dùng dịch vụ của chúng tôi! <br />
            Chúc bạn có những giờ phút hẹn hò vui vẻ!<br />
          </span>}
          {current_user.coins < number_coin_to_unlock_chat && <span>
            Số xu hiện có chưa đủ để mở chat.<br />
            Chúc bạn có những giờ phút hẹn hò vui vẻ!<br />
          </span>}
          <br /><br />
          {current_user.coins >= number_coin_to_unlock_chat && <button onClick={() => this.handleUnlockChatButton(2)} className="btn btn--p btn--b">Trò chuyện với {chat_to_username} ngay!</button>}
          {current_user.coins < number_coin_to_unlock_chat && <button onClick={() => this.handleUnlockChatButton(1)} className="btn btn--p btn--b">Nạp thêm xu</button>}
        </div>
      )
    }

    handleUnlockChatButton(type) {
      if (type === 1) {
        const gaTrackData = {
          page_source: 'UNLOCK_CHAT_THANK_YOU_PAGE'
        };
        const eventAction = {
          eventCategory: 'Coin',
          eventAction: 'GetMoreCoin',
          eventLabel: 'Get more coin'
        }
        this.props.sendGoogleAnalytic(eventAction, gaTrackData)
        browserHistory.push('/payment/coin')
      }
      else {
        const gaTrackData = {
          page_source: 'THANK_YOU_PAGE'
        };
        const eventAction = {
          eventCategory: 'Coin',
          eventAction: 'Buy',
          eventLabel: 'Buy Unlock Chat'
        }
        this.props.sendGoogleAnalytic(eventAction, gaTrackData)
        this.backToChat()
      }

    }

    backToChat() {
      if (this.state.clickedBackToChat) {
        return ;
      }
      this.setState({clickedBackToChat: true});
      const chat_url = ymmStorage.getItem('require_payment_url');
      if (chat_url) {
        const male_id = ymmStorage.getItem('require_payment_male_id');
        const female_id = ymmStorage.getItem('require_payment_female_id');
        if (male_id && female_id) {
          this.props.unlockChat(male_id, female_id).then(res => {
            ymmStorage.removeItem('require_payment_url');
            ymmStorage.removeItem('require_payment_male_id');
            ymmStorage.removeItem('require_payment_female_id');
            window.location.href=decodeURIComponent(`${chat_url}?unlock_success`);
          }).catch(() => {
            this.setState({clickedBackToChat: false});
          })
        }
      }
      // window.location.href=decodeURIComponent(chat_url);
    }

    renderSuperLikeBtn() {
      const props = this.props;
      const { Payment, user_profile } = props;
      const { order } = Payment;
      const { current_user } = user_profile ;
      const chat_url = process.env.CHAT_SERVER_URL;
      const chat_to_username = ymmStorage.getItem('super_like_name');
      // const number_coin_to_unlock_chat = 99999999999999;
      if (order.is_license) {
        return '';
      }
      return (
        <div className="txt-lg txt-light txt-center">
          Chúc mừng bạn vừa giao dịch thành công!<br /><br />
          <div className="txt-blue">
            Bạn đã thanh toán gói {order.coins} xu trị giá {new Intl.NumberFormat().format(order.amount)} VND
            <div>TÀI KHOẢN CỦA BẠN CÓ {current_user.coins} XU</div>
          </div> <br />
          {current_user.coins >= NUMBER_COINS_NEED_FOR_SUPER_LIKE && <span>
            Cảm ơn bạn đã tin dùng dịch vụ của chúng tôi! <br />
            Chúc bạn có những giờ phút hẹn hò vui vẻ!<br />
          </span>}

          {current_user.coins < NUMBER_COINS_NEED_FOR_SUPER_LIKE && <span>
            Số xu hiện có chưa đủ để có thể dùng Làm quen.<br />
            Hãy nạp thêm xu để trải  nghiệm tính năng này nhé!<br />
          </span>}
          <br /><br />
          {current_user.coins >= NUMBER_COINS_NEED_FOR_SUPER_LIKE && <button onClick={() => {this.forceChatClick()}} className="btn btn--4 col-xs-12">Chat với {chat_to_username} ngay!</button>}
          {current_user.coins < NUMBER_COINS_NEED_FOR_SUPER_LIKE && <Link to="/payment/coin" className="btn btn--4 col-xs-12" >Nạp thêm xu</Link>}
        </div>
      )
    }

    forceChatClick() {
      const { user_profile } = this.props;
      const { current_user } = user_profile;
      const male_id = ymmStorage.getItem('super_like_male_id');
      const female_id = ymmStorage.getItem('super_like_female_id');
      const super_like_to_identifier = ymmStorage.getItem('super_like_to_identifier');
      this.props.sendSuperLike(male_id, female_id).then((res) => {
        if (current_user.user_status == ACTIVE_USER) {
          socket.emit('super_like', {room: super_like_to_identifier, to_user_identifier: super_like_to_identifier, super_like_info: current_user});
        }
        ymmStorage.removeItem('super_like_name');
        ymmStorage.removeItem('super_like_male_id');
        ymmStorage.removeItem('super_like_female_id');
        window.location.href=decodeURIComponent(`${process.env.CHAT_SERVER_URL}chat/${super_like_to_identifier}`);
      }).catch(() => {});
    }

    renderBannerRemindWithCoin() {
      const props = this.props;
      const { Payment, user_profile } = props;
      const { order } = Payment;
      const { current_user } = user_profile ;
      // const number_coin_to_unlock_chat = 99999999999999;
      if (order.is_license) {
        return '';
      }
      return (
        <div className="txt-lg txt-light txt-center">
          Chúc mừng bạn vừa giao dịch thành công!<br /><br />
          <div className="txt-blue">
            Bạn đã thanh toán gói {order.coins} xu trị giá {new Intl.NumberFormat().format(order.amount)} VND
            <div>TÀI KHOẢN CỦA BẠN CÓ {current_user.coins} XU</div>
          </div> <br />
          {current_user.coins >= 100 && <span>
            Cảm ơn bạn đã tin dùng dịch vụ của chúng tôi! <br />
            Chúc bạn có những giờ phút hẹn hò vui vẻ!<br />
          </span>}

          {current_user.coins < 100 && <span>
            Số xu hiện có chưa đủ để có thể dùng Nhắc Người Ấy.<br />
            Hãy nạp thêm xu để trải  nghiệm tính năng này nhé!<br />
          </span>}
          <br /><br />
          {current_user.coins >= 100 && <button onClick={() => {this.handleRemindWithCoinClick()}} className="btn btn--b btn--p col-xs-12">Nhắc Người Ấy!</button>}
          {current_user.coins < 100 && <Link to="/payment/coin" className="btn btn--b btn--p col-xs-12" >Nạp thêm xu</Link>}
        </div>
      )
    }

    handleRemindWithCoinClick() {
      if (this.state.accept_remind_with_coin) {
        return;
      }
      this.setState({accept_remind_with_coin: true})
      this.props.acceptRemindWithCoin().then(res => {
        sendBannerNotification.remindBannerWithCoinSuccess()
        this.setState({accept_remind_with_coin: false, isOpenConfirmModal: false})
        this.props.getCurrentUser()
        ymmStorage.removeItem('BannerRemindWithCoin')
      }).catch(() => {this.setState({accept_remind_with_coin: false})})
    }


    renderTarotOnline() {
      const props = this.props;
      const { Payment, user_profile } = props;
      const { order } = Payment;
      const { current_user } = user_profile ;
      // const number_coin_to_unlock_chat = 99999999999999;
      if (order.is_license) {
        return '';
      }
      return (
        <div className="txt-lg txt-light txt-center">
          Chúc mừng bạn vừa giao dịch thành công!<br /><br />
          <div className="txt-blue">
            Bạn đã thanh toán gói {order.coins} xu trị giá {new Intl.NumberFormat().format(order.amount)} VND
            <div>TÀI KHOẢN CỦA BẠN CÓ {current_user.coins} XU</div>
          </div> <br />
          {current_user.coins >= TAROT_COST && <span>
            Cảm ơn bạn đã tin dùng dịch vụ của chúng tôi! <br />
            Chúc bạn có những giờ phút hẹn hò vui vẻ!<br />
          </span>}

          {current_user.coins < TAROT_COST && <span>
            Số xu hiện có chưa đủ để tiếp tục rút thẻ bài.<br />
            Hãy nạp thêm xu để trải  nghiệm tính năng này nhé!<br />
          </span>}
          <br /><br />
          {current_user.coins >= TAROT_COST && <Link to="/tarot-page" className="btn btn--b btn--p col-xs-12">Tiếp tục rút thẻ bài</Link>}
          {current_user.coins < TAROT_COST && <Link to="/payment/coin" className="btn btn--b btn--p col-xs-12" >Nạp thêm xu</Link>}
        </div>
      )
    }

    renderOrderPayment() {
      const { order } = this.props.Payment;
      const paymentIntention = ymmStorage.getItem('Payment_Intention')
      // Let's refactor later :(
      const chat_url = ymmStorage.getItem('require_payment_url');
      const super_like_name = ymmStorage.getItem('super_like_name');

      // License package
      if (order.is_license) {
        return this.renderBuyMonthLicense();
      }
      else { // Coin package

        if (paymentIntention == "TAROT_ONLINE") {
          return this.renderTarotOnline();
        }

        if (paymentIntention == "VERIFY_PICTURE_SECOND_WAY") {
          return this.renderVerifyPictureSecondWayButton();
        }
        // Let's refactor later
        if (chat_url != null) {
          return this.renderBackToChatApp();
        }
        if (super_like_name != null) {
          return this.renderSuperLikeBtn();
        }

        return this.renderBuyCoin();
      }
    }

    render() {
      const props = this.props;
      const { current_user } = this.props.user_profile
      const { order } = this.props.Payment;
      if (typeof order !== 'undefined' && order.is_license) {
        ymmStorage.setItem('upgrade_success', 1);
      }
      return <div className="sticky-footer">
        <div className="site-content">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <div className="well">
                  <Header {...props} />
                  {
                    order.is_license &&
                    <PackagePaymentSteps stage={PAYMENT_RESULT} />
                  }
                </div>
                <div className="mbl"><img src={`${CDN_URL}/general/thankyou.png`} /></div>
                {this.renderOrderPayment()}
              </div>
            </div>
          </div>
        </div>
        {/*<footer className="site-footer">
          <p className="txt-center txt-light txt-sm">Copyright &copy; 2016 <a href="http://mmj.vn">Media Max Japan</a> All Rights Reserved.</p>
        </footer>*/}
      </div>
  }
}

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getCurrentUser: () => {
      dispatch(getCurrentUser());
    },
    getPaymentPackages: package_type => {
      dispatch(getPaymentPackages(package_type))
    },
    unlockChat: (male_id, female_id) => {
      const defer = q.defer();
      dispatch(unlockChat(male_id, female_id)).then((res) => {
        defer.resolve(res);
      }).catch((e)=> {
        defer.reject(false);
      })
      return defer.promise;
    },
    sendSuperLike: (male_id, female_id) => {
      const defer = q.defer();
      defer.resolve(true);
      dispatch(coinConsume('coin_consume_super_match', male_id, female_id)).then(res => {
        defer.resolve(res);
      }).catch(() => {
        defer.reject(false);
      })
      return defer.promise;
    },
    getPeopleMatchedMe: () => {
      dispatch(getPeopleMatchedMe())
    },
    acceptRemindWithCoin: () => {
      const defer = q.defer();
      dispatch(coinConsume('coin_consume_remind_liked_partners')).then(res => {
        defer.resolve(res);
      }).catch(() => {
        defer.reject(false);
      })
      return defer.promise;
    },
  }
}
const ThankyouContainer = connect(mapStateToProps, mapDispachToProps)(Thankyou)

export default ThankyouContainer
