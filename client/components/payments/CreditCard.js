import React, { Component } from 'react';
import { Link } from 'react-router';
import { browserHistory } from 'react-router';
import Packages from './Packages';
import SelectMethod from './SelectMethod';
import CreditCardProviders from './CreditCardProviders';
import PaymentSubmit from './PaymentSubmit';

class CreditCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (
      <div className="site-content">
        <div className="container">
          <SelectMethod method="credit-card" {...this.props} />
          <div className="col-xs-12 fix-padding-col nav-1">
            <Packages  {...this.props} />
            <CreditCardProviders  {...this.props} />
          </div>
          <PaymentSubmit  {...this.props} />
        </div>
      </div>
    )
  }
}

export default CreditCard
