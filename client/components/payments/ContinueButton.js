import React, { Component } from 'react'
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { WHO_ADD_ME_TO_FAVOURITE_PAGE, SEARCH_CONDITION_PAGE } from '../../constants/Enviroment'
import * as ymmStorage from '../../utils/ymmStorage';

class ContinueButton extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    if (ymmStorage.getItem('require_payment_url')) {
      window.location.href= decodeURIComponent(ymmStorage.getItem('require_payment_url'))
    } else if (ymmStorage.getItem('Payment_Intention') === SEARCH_CONDITION_PAGE) {
      browserHistory.push('/search-condition');
    }
    else if (ymmStorage.getItem('Payment_Intention') === WHO_ADD_ME_TO_FAVOURITE_PAGE) {
      browserHistory.push('/who-add-favourite');
    }
    else {
      browserHistory.push('/whathot');
    }
  }

  render() {
    return (
      <button onClick={() => this.handleClick()} className="btn btn--p btn--b">Tiếp tục hẹn hò!</button>
    )
  }
}


function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {}
}

const ContinueButtonContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(ContinueButton)

export default ContinueButtonContainer
