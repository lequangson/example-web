import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import ReactTimeout from 'react-timeout'
import _ from 'lodash'
import $ from 'jquery'
import { CDN_URL, PAYMENT_PAGE, GA_ACTION_PAYMENT_PACKAGE_SUBMIT, GA_ACTION_PAYMENT_PROCESS_PAYMENT, TRUST_MARK_URL } from '../../constants/Enviroment'
import { getUserIdFromIdentifier, getUserId } from '../../utils/common'
import PaymentInstruction from './Instruction';
import MobileCard from './MobileCard';
import BankCard from './BankCard';
import CreditCard from './CreditCard';
import SlidePayment from './SlidePayment';
import BannerHasImgRight from '../commons/BannerHasImgRight';
import * as ymmStorage from '../../utils/ymmStorage';
import BannerDiscount from 'components/commons/BannerDiscount';
import BannerSwapSite from 'components/commons/BannerSwapSite';

class Payment extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentWillMount() {
    // reset exist payment if any to prevent mixing payment method
    this.props.updatePaymentState('packages', []);
    this.props.updatePaymentState('shopping_cart', {});
    this.props.updatePaymentState('package_id', null);
    this.props.updatePaymentState('provider_code', null);
    this.props.updatePaymentState('shopping_cart', {});
    this.props.updatePaymentState('order', {});
  }

  textHeader() {
    const { user_profile } = this.props;
    const { current_user } = user_profile ;
    const { type } = this.props.params;
    if (type == 'coin') {
      return (
        <div className="col-xs-12">
          <h2 className="txt-heading">Nạp xu</h2>
          <p>
            Số xu hiện có:
            <span className="txt-blue padding-l5">{current_user.coins} xu</span>
          </p>
        </div>
      )
    }
    return (
      <div className="col-xs-12">
        <h2 className="txt-heading">Thanh toán</h2>
        {/*<Link to="/payment/upgrade">Upgrade</Link> |
        <Link to="/payment/coin">Coin</Link>*/}
      </div>
    )
  }

  renderBannerDiscount() {
    const campaign = this.props.campaign.data;
    const showSaleoffBanner = !_.isEmpty(campaign);
    const { type } = this.props.params;
    const { gender } = this.props.user_profile.current_user;

    if (type == 'coin') {
      return (
        <div className="col-xs-12">
          {showSaleoffBanner && gender == 'male' &&
            <BannerDiscount
            {...this.props}
            timeStop={campaign.end_at}
            url={"/payment/upgrade"} />
          }
        </div>
      )
    }
    return (
      <div></div>
    )

  }

  render() {

    const { type } = this.props.params
    const { gender } = this.props.user_profile.current_user
    const method = this.props.params.method || 'mobile-card';
    if (!gender || !type) {
      return <div></div>
    }

    return (
      <div>
        <div className="container">
          <BannerSwapSite />
          <div className="row">
            {/* {this.bannerHeader()} */}
            {this.renderBannerDiscount()}
            {this.textHeader()}
          </div>
        </div>
        <SlidePayment gender={gender} package_type={type} />
        {/*<Instruction />*/}
        {method === 'mobile-card' && <MobileCard {...this.props} />}
        {method === 'bank-card' && <BankCard {...this.props} />}
        {method === 'credit-card' && <CreditCard {...this.props} />}

        <div className="txt-center padding-b20">
          <p className="txt-red txt-sm txt-bold">Hotline(7h-22h): 0936 332 079</p>
          {type == 'coin' && <div> Xem hướng dẫn chi tiết cách sử dụng xu ở <Link to="/coin-instruction/">đây</Link></div>}
          {type == 'upgrade' && <div> Xem hướng dẫn chi tiết cách mua gói nâng cấp ở <Link to="/upgrade-instruction/">đây</Link></div>}
        </div>

        <div className="txt-center container padding-b30">
          <div className="box-bottom">Thanh toán của bạn sẽ được giữ an toàn và bảo mật.</div>
          <div className="mbs">Cam kết bởi</div>
          <div className="row">
             <div className="col-xs-2"></div>
             <div className="col-xs-8">
              <div className="col-xs-4">
                <img src={`${TRUST_MARK_URL}vtc+pay.png`} alt="vtc+pay.png"/>
              </div>
              <div className="col-xs-4">
                <img src={`${TRUST_MARK_URL}gmo.png`} alt="gmo.png"/>
              </div>
              <div className="col-xs-4">
                <img src={`${TRUST_MARK_URL}pci.png`} alt="pci.png"/>
              </div>
             </div>
             <div className="col-xs-2"></div>
          </div>
        </div>
      </div>
    )
  }
}
export default ReactTimeout(Payment)
