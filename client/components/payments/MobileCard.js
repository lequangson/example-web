import React, { Component } from 'react'
import { browserHistory } from 'react-router';
import Packages from './Packages';
import MobileProviders from './MobileProviders';
import SelectMethod from './SelectMethod';
import PaymentSubmit from './PaymentSubmit';

class MobileCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (
      <div className="site-content">
        <div className="container">
          <SelectMethod method="mobile-card" {...this.props} />
          <div className="col-xs-12 fix-padding-col nav-1">
            <Packages {...this.props} />
            <MobileProviders  {...this.props}  />
          </div>
          <PaymentSubmit  {...this.props} />
        </div>
      </div>
    )
  }
}

export default MobileCard
