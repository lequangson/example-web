import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { browserHistory } from 'react-router';
import Packages from './Packages';
import SelectMethod from './SelectMethod';
import BankProviders from './BankProviders';
import PaymentSubmit from './PaymentSubmit';

class BankCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (
      <div className="site-content">
        <div className="container">
          <SelectMethod method="bank-card" {...this.props} />
          <div className="col-xs-12 fix-padding-col nav-1">
            <Packages  {...this.props} />
            <BankProviders  {...this.props} />
          </div>
          <PaymentSubmit  {...this.props} />
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {}
}

const BankCardContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(BankCard)

export default BankCardContainer