import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { NOTIFICATION_ERROR, NOTIFICATION_ERROR_DEFAULT_TIME } from '../../constants/Enviroment';

class MobileCardsInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      current_input_id: 1,
      currentDate: null,
    }
    this.handleClick = this.handleClick.bind(this);
    this.showInput = this.showInput.bind(this);
    this.removeInput = this.removeInput.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
  }

  showInput() {
    const { Payment } = this.props;
    const { mobile_card_info } = Payment;
    this.setState({current_input_id: this.state.current_input_id + 1});
    mobile_card_info.push(
       {
        input_id: this.state.current_input_id + 1,
        card_code: '',
        card_id: '',
        payment_info_id: '',
        payment_status: null,
      }
    );
  }

  removeInput(input_id) {
    const { Payment } = this.props;
    const { mobile_card_info, shopping_cart } = Payment;
    const removed_input_index = mobile_card_info.findIndex(item => item.input_id == input_id);
    mobile_card_info.splice(removed_input_index, 1);
    // this.setState({currentDate: new Date()})
    this.props.updatePaymentState('mobile_card_info', mobile_card_info);
  }

  handleClick(package_id) {
    this.props.updatePaymentState('package_id', package_id)
  }

  handleKeyUp(input_id, payment_status) {
    if (payment_status == 1) {
      return;
    }
    const { Payment } = this.props;
    const { mobile_card_info, shopping_cart, error_required } = Payment;
    const entered_input = mobile_card_info.find(item => item.input_id == input_id)
    entered_input.card_code = this.refs[`card_code_${input_id}`].value;
    entered_input.card_id = this.refs[`card_id_${input_id}`].value;
    entered_input.payment_status = null;
    if(this.refs[`card_code_${input_id}`].value.trim().length && this.refs[`card_id_${input_id}`].value.trim().length) {
      const input_id_index = error_required.indexOf(input_id);
      error_required.splice(input_id_index, 1);
      this.setState({currentDate: new Date()})
    }
  }

  render() {
    const { Payment } = this.props;
    if (isEmpty(Payment)) {
      return <div></div>
    }
    const { error_required, shopping_cart, mobile_card_info, duplicated_code, duplicated_serial } = Payment;
    const serial_success = [];
    const serial_failure = [];
    const { shopping_cart_payment_infos } = shopping_cart;
    if (typeof shopping_cart_payment_infos !== 'undefined') {
      shopping_cart_payment_infos.map((payment_info, i) => {
        if( payment_info.payment_status == 1) {
          serial_success.push(payment_info.mobile_card_serial);
        } else {
          if (payment_info.payment_status != null){
            serial_failure.push(payment_info.mobile_card_serial);
          }
        }
      })
    }
    let has_card_success = [];
    let error_message = '';
    if( typeof shopping_cart.shopping_cart !== 'undefined') {
      has_card_success = shopping_cart.shopping_cart.shopping_cart_payment_infos.filter(item => item.payment_status == 1)
      if (
        typeof shopping_cart.error_message !== 'undefined' &&
        shopping_cart.error_message &&
        shopping_cart.error_message.length)
      {
        error_message = shopping_cart.error_message;
      }
    }
    // const { packages, payment_method, package_id } = Payment;
    return (
      <div className="col-xs-12">
        <div className="padding-t20"><span>Bước 3: Nhập thông tin thẻ</span></div>
        <div className="padding-t10 txt-light txt-sm  padding-b10">* Bấm <i className="fa fa-plus-square-o" aria-hidden="true"></i> khi cần nạp nhiều hơn một thẻ và chỉ thanh toán sau khi đã nhập đủ thông tin của tất cả các thẻ.</div>

        {
          mobile_card_info.map((card_input, i) =>
          <div className={`row l-flex-grid`} key={card_input.input_id} >
            <div className="col-xs-10 col-md-11 mbm">
              <div> Mã số</div>
              {/*<div className="form__input--rel"><input className="mbs border-r" type="text" placeholder="" ref={`card_code_${i}`} onKeyUp={this.handleKeyUp} /></div>
              <div className="txt-red mbs">Mã số thẻ không chính xác</div>
              <div> Số seri </div>
              <input  className="mbs" type="text" placeholder="" ref={`card_id_${i}`} onKeyUp={this.handleKeyUp} />
              <div className="txt-red">Số seri không chính xác</div>*/}
              {/*<input className="mbs border-r" type="text" placeholder="" ref={`card_code_${i}`} onKeyUp={this.handleKeyUp} />*/}
              <div className={ card_input.payment_status == 1 ? 'form__input--rel' : '' }>
              <input
                defaultValue={card_input.card_code}
                className={`mbs txt-blue ${duplicated_code.indexOf(card_input.card_code) > -1 || error_required.indexOf(card_input.input_id) > -1 || (card_input.payment_status && card_input.payment_status != 1) ? 'border-r' : ''}`}
                type="number"
                id={i}
                placeholder=""
                ref={`card_code_${card_input.input_id}`}
                onKeyUp={() => {this.handleKeyUp(card_input.input_id, card_input.payment_status)}}
                readOnly={card_input.payment_status == 1 ? true : false}
              />
              </div>
              {duplicated_code.indexOf(card_input.card_code) > -1 && <p className="txt-red">Số thẻ bị trùng</p> }
              {/*<p className="txt-red">Mã số thẻ không chính xác</p>*/ }
              <div> Số seri </div>
              <div className={ card_input.payment_status == 1 ? 'form__input--rel' : '' }>
              <input
                type="number"
                className={`mbs txt-blue ${duplicated_serial.indexOf(card_input.card_id) > -1 || error_required.indexOf(card_input.input_id) > -1 || (card_input.payment_status && card_input.payment_status != 1) ? 'border-r' : ''}`}
                placeholder=""
                ref={`card_id_${card_input.input_id}`}
                onKeyUp={() => {this.handleKeyUp(card_input.input_id, card_input.payment_status)}}
                readOnly={card_input.payment_status == 1 ? true : false}
              />
              </div>
              {card_input.payment_status && card_input.payment_status != 1 && <p className="txt-red">Thẻ hết hạn hoặc số seri hoặc số thẻ không chính xác</p> }
              {duplicated_serial.indexOf(card_input.card_id) > -1 && <p className="txt-red">Số seri bị trùng</p> }
            </div>
            <div className="col-xs-2 col-md-1">
              {(i != 0 || this.state.total_show_input > 1) && <i className="fa fa-minus-square-o txt-xxlg txt-light" aria-hidden="true" onClick={() => {this.removeInput(card_input.input_id)}} ></i>}
            </div>
          </div>)
        }
        {mobile_card_info.length < 12 && <div className="col-xs-12">
            <div className="l-flex-vertical-center"><i className="fa fa-plus-square-o txt-xxlg txt-light mrs" aria-hidden="true" onClick={this.showInput} ></i>Thêm thẻ</div>
          </div>
        }
        {error_message.length > 0 && <p className="txt-red"><br />{error_message}</p>}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {}
}

const MobileCardsInfoContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(MobileCardsInfo)


export default MobileCardsInfoContainer
