import React, { Component } from 'react'
import { browserHistory } from 'react-router';
import { VTC_GATEWAY_PAYMENT_STATUS_SUCCESS } from '../../constants/Payment';
import {
  GA_ACTION_PAYMENT_CALLBACK_PROCESS_FAILURE, GA_ACTION_PAYMENT_CALLBACK_PROCESS_SUCCESS,
} from '../../constants/Enviroment'

class PaymentCallback extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentWillMount() {
    /*if (!this.validateCallbackParameters()) {
      browserHistory.push('/payment/failure')
      return;
    }*/
    this.processPaymentCallback();
  }

  validateCallbackParameters() {
    const parameters = this.props.location.query; 
    // Enough parameters?
    if (parameters.status == null || parameters.website_id == null || parameters.reference_number == null ||
      parameters.amount == null || parameters.trans_ref_no == null || parameters.signature == null) {
      return false;
    }
    // Payment suscess?
    if (parameters.status !== VTC_GATEWAY_PAYMENT_STATUS_SUCCESS) {  
      return false;
    }
    return true;
  }

  sendGoogleAnalytic(actionCode, extra_information, extra_information2) {
    const parameters = this.props.location.query;
    const ga_track_data = {
      extra_information: parameters.status,
      extra_information2: extra_information,
      message: extra_information2
    }
    this.props.gaSend(actionCode, ga_track_data)   
  }

  processPaymentCallback() {
    const parameters = this.props.location.query;
    const data = parameters.amount + '|' + parameters.message + '|' + 
      parameters.payment_type + '|' + parameters.reference_number + '|' +
      parameters.status + '|' + parameters.trans_ref_no + '|' +
      parameters.website_id;

    this.props.paymentCallback(data, parameters.signature).then(() => {
      this.sendGoogleAnalytic(GA_ACTION_PAYMENT_CALLBACK_PROCESS_SUCCESS, data);
      browserHistory.push('/payment/thankyou');
    }).catch((ex) => {   
      console.log('PaymentCallback error', ex)
      this.sendGoogleAnalytic(GA_ACTION_PAYMENT_CALLBACK_PROCESS_FAILURE, data, ex.message)
      browserHistory.push('/payment/failure')
    })
  }

  render() {
    return (
      <div>
      </div>
    )
  }
}

export default PaymentCallback
