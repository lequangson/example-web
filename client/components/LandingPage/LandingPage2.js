import React from 'react';
import { Link } from 'react-router'
import '../../../public/landingpage-assets1/styles/main.css'
import '../../../public/styles/m-main.css'
import '../../../public/styles/notification.css'
import { LOGGING_TEXT } from '../../constants/TextDefinition'
import { LANDINGPAGE_NEW_BASE_URL, LANDINGPAGE_BASE_URL, BLOG_URL, ANDROID_URL, IOS_URL, CDN_URL } from '../../constants/Enviroment'
import * as UserDevice from '../../utils/UserDevice';
import { FacebookLoginButton, GoogleLoginButton } from '../auth';
import IosDownloadButton from 'components/commons/Buttons/IosDownloadButton'
import SilderCommon from 'components/commons/SilderCommon';
import AndroidDownloadButton from 'components/commons/Buttons/AndroidDownloadButton'
import BannerAppInstall from 'components/commons/BannerAppInstall'

class LandingPage2 extends React.Component {

  render() {
    const { auth } = this.props;
    const { isFetching, isAuthenticated } = auth;
    const app_url = UserDevice.getName() === 'ios' ? IOS_URL : ANDROID_URL;

    return (
      <div>
        {(UserDevice.getName() === 'ios' || UserDevice.getName() === 'android') &&
          <BannerAppInstall
            isOpen={true}
            {...this.props}
            url={app_url}
          />
        }
        <div className="lp">
          <header className="lp-header js-header">
            <div className="row">
              <div className="small-12 column l-flex l-flex--ac l-flex--header">
                <div className="lp-nav">
                  <button className="hamburger hamburger--elastic js-hamburger" type="button"><span className="hamburger-box"><span className="hamburger-inner"></span></span></button>
                  <h1 className="lp__logo">
                    <Link to="/">
                      <img src={LANDINGPAGE_BASE_URL + 'Logo_white.png'} alt="Ymeet.me - hẹn hò an toàn cho phụ nữ" />
                    </Link>
                  </h1>
                  <div className="lp-nav__main js-nav">
                    <h1 className="lp-nav__logo">
                      <Link to="/">
                        <img src={LANDINGPAGE_BASE_URL + 'Logo_white.png'} alt="Ymeet.me - hẹn hò an toàn cho phụ nữ" />
                      </Link>
                    </h1>
                    <nav className="lp-nav__list">
                      <a className="lp-nav__link l2" href="http://ymeet.me/branding/" target="_blank" rel="noopener nofollow noreferrer" >Câu chuyện Ymeet.me</a>
                      <Link className="lp-nav__link l2" to="/quizvui">Quiz Vui</Link>
                      <a className="lp-nav__link l2" href={BLOG_URL} target="_blank" rel="noopener nofollow noreferrer" >Blog</a>
                      <Link className="lp-nav__link l2" to="/contact">Liên hệ</Link>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
            <div className="lp-header__blur"></div>
          </header>
          <SilderCommon carouselName='carousel-common' numberShowItem={1} autoplaySpeed={3000} notUserClassGrid>
            <section className="lp-hero lp-hero--1 l2">
              <div className="lp-hero__content l2 full-width">
                <h2 className="lp-hero__heading mblg">Hẹn hò online nghiêm túc<br />miễn phí đăng ký, trò chuyện dễ dàng</h2>
                {!isAuthenticated && <FacebookLoginButton />}
                {!isAuthenticated && <GoogleLoginButton />}
              </div>
            </section>
            <Link href="/events" className="lp-hero lp-hero--1" style={{background: `url(${CDN_URL}/general/LandingPage/${(UserDevice.getName() === 'ios' || UserDevice.getName() === 'android')? "Landingpage_mobile.png" : "Landingpage-slider.png"}) no-repeat 100%`, backgroundSize: `${(UserDevice.getName() === 'ios' || UserDevice.getName() === 'android') ? 'contain' : 'cover'}`}}>
            </Link>
         </SilderCommon>

          <section className="lp-well lp-well--decor">
            <div className="row small-collapse">
              <div className="small-12 medium-12 large-9 large-offset-3 column column">
                <div className="lp-introduction-wrap">
                  <div className="lp-introduction__item txt-center medium-12">
                    <div className="lp-btn lp-btn--secondary mblg">
                      <div className="l-flex l-flex--ac">
                        <div className="icon-bg-1 icon-bg-1__user">
                          <img src={LANDINGPAGE_NEW_BASE_URL + '1.png'} className="img-70"/>
                        </div>
                        <h3 className="txt-h4 txt-left txt-thin mbzero margin-l10">Kết bạn, làm quen với<b className="txt-bold txt-color-1"> hàng nghìn</b><br/>thành viên mới mỗi ngày</h3>
                      </div>
                    </div>
                  </div>
                  <div className="lp-introduction__item txt-center medium-12">
                    <div className="lp-btn lp-btn--secondary mblg">
                      <div className="l-flex l-flex--ac">
                        <div className="icon-bg-1 icon-bg-1__hear">
                        <img src={LANDINGPAGE_NEW_BASE_URL + '2.png'} className="img-70"/>
                        </div>
                        <h3 className="txt-h4 txt-left txt-thin mbzero margin-l10">Tự động đề xuất những người<br/><b className="txt-bold txt-color-1">phù hợp nhất</b> với tiêu chí của bạn</h3>
                      </div>
                    </div>
                  </div>
                  <div className="lp-introduction__item txt-center medium-12">
                    <div className="lp-btn lp-btn--secondary mblg">
                      <div className="l-flex l-flex--ac">
                        <div className="icon-bg-1 icon-bg-1__broken">
                        <img src={LANDINGPAGE_NEW_BASE_URL + '3.png'} className="img-70"/>
                        </div>
                        <h3 className="txt-h4 txt-left txt-thin mbzero margin-l10">Cam kết tìm người yêu thành công<br/><b className="txt-bold txt-color-1">chỉ sau 3 tháng</b></h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <img className="lp-well__decor-2" src={LANDINGPAGE_NEW_BASE_URL + 'phone_decor.png'} alt="Tim-nguoi-yeu-de-dang-voi-Ymeetme" />
          </section>
          <section className="lp-well lp-well--2">
            <div className="row">
              <div className="small-12 column">
                <h2 className="txt-xxlg txt-center mbxlg">Tải ứng dụng hẹn hò miễn phí Ymeet.me</h2>
              </div>
            </div>
            <div className="row">
              <div className="small-12 column">
                <div className="lp-installation-wrap">
                  <div className="l-flex--jc l-flex--ac l-flex">
                    {(UserDevice.getName() === 'pc' || UserDevice.getName() === 'ios') &&
                      <IosDownloadButton redirect_url={IOS_URL} />
                    }
                    {(UserDevice.getName() === 'pc' || UserDevice.getName() === 'android') &&
                      <AndroidDownloadButton redirect_url={ANDROID_URL}/>
                    }
                  </div>
                </div>
              </div>
            </div>
          </section>
          <footer className="lp-footer l2">
            <div className="lp-footer__content">
              <div className="row">
                <div className="small-12 column">
                  <div className="lp-footer__facebook l-flex--jc l-flex--ac l-flex mblg">
                    <div className="fb-page" data-href="https://www.facebook.com/ymeet.me/"
                    data-width="280"
                    data-hide-cover="false"
                    data-show-facepile="true"></div>
                  </div>
                </div>
              </div>
              <div className="row small-collapse">
                <div className="small-12 medium-6 column">
                  <ul className="list-inline txt-left-mc">
                    <li><Link to="/terms-of-use">Điều khoản sử dụng</Link></li>
                    <li><Link to="/privacy">Bảo mật</Link></li>
                    <li><Link to="/helps">Trợ giúp</Link></li>
                    <li><Link to="/contact">Liên hệ</Link></li>
                    <li><a href="https://ymeet.me/blog/" rel='noopener noreferrer' target="_blank">Blog</a></li>
                  </ul>
                </div>
                <div className="small-12 medium-6 column">
                  <div className="txt-sm txt-right-mc">Copyright (c) 2016 <a href="http://mmj.vn" target="_blank" rel="noopener noreferrer" className="txt-bold">Media Max Japan</a> All Rights Reserved.</div>
                </div>
              </div>
            </div>
          </footer>
          <div className={isFetching ? 'loader is-full-screen-load' : 'hide loader is-full-screen-load'}>
            <span className="loader__text">{ LOGGING_TEXT }</span>
          </div>
        </div>
      </div>
    );
  }
}

LandingPage2.propTypes = {

}

export default LandingPage2
