import React, { PropTypes } from 'react';
import $ from 'jquery'
import { browserHistory, Link } from 'react-router'
import Siema from 'siema/dist/siema.min.js'
import '../../../public/styles/m-main.css'
import '../../../public/styles/notification.css'
import logo2 from '../../../public/images/logo-2.png'
import text from '../../../public/images/text.png'
import { LOGGING_TEXT, AUTO_LOGIN_ERROR_TITLE, AUTO_LOGIN_ERROR_MSG } from '../../constants/TextDefinition'
import { LANDINGPAGE_BASE_URL } from '../../constants/Enviroment'
import { BLOG_URL } from '../../constants/Enviroment'
import AnonymousFooterWithLogo from '.././footers/AnonymousFooterWithLogo';
import ScrollToTop from '../ScrollToTop'
import * as UserDevice from '../../utils/UserDevice';
import { FacebookLoginButton, GoogleLoginButton } from '../auth';

const responseFacebook = () => {
  // console.log('facebook response',response);
};
class LandingPage1 extends React.Component {
  render() {
    const { auth } = this.props;
    const { isFetching, isAuthenticated } = auth

    return (
      <div className="lp">
        <header className="lp-header js-header">
          <div className="row">
            <div className="small-12 column l-flex l-flex--ac l-flex--header">
              <div className="lp-nav">
                <button className="hamburger hamburger--elastic js-hamburger" type="button"><span className="hamburger-box"><span className="hamburger-inner"></span></span></button>
                <h1 className="lp__logo">
                  <Link to="/">
                    <img src={LANDINGPAGE_BASE_URL + 'logo.png'} alt="Ymeet.me - hẹn hò an toàn cho phụ nữ" />
                  </Link>
                </h1>
                <div className="lp-nav__main js-nav">
                  <h1 className="lp-nav__logo">
                    <Link to="/">
                      <img src={LANDINGPAGE_BASE_URL + 'logo.png'} alt="Ymeet.me - hẹn hò an toàn cho phụ nữ" />
                    </Link>
                  </h1>
                  <nav className="lp-nav__list">
                    <a className="lp-nav__link" href="http://ymeet.me/branding/" target="_blank" rel="noopener nofollow noreferrer" >Câu chuyện Ymeet.me</a>
                    <a className="lp-nav__link" href={BLOG_URL} target="_blank" rel="noopener nofollow noreferrer" >Blog</a>
                    <Link className="lp-nav__link" to="/contact">Liên hệ</Link>
                  </nav>
                </div>
              </div>
                {!isAuthenticated && <FacebookLoginButton icon_position='right' />}
                {!isAuthenticated && <GoogleLoginButton icon_position='right' />}
            </div>
          </div>
          <div className="lp-header__blur"></div>
        </header>
        <section className="lp-hero">
          <div className="lp-hero__content">
            <h2 className="lp-hero__heading mbmd">HẸN HÒ ONLINE<br />CHƯA BAO GIỜ AN TOÀN  ĐẾN THẾ</h2>
            {!isAuthenticated && <FacebookLoginButton />}
            {!isAuthenticated && <GoogleLoginButton />}
            <p className="txt-color-invert"> <img className="mrsm" src={LANDINGPAGE_BASE_URL + 'shield-with-lock.png'} alt="shield-with-lock" /> Cam kết bảo mật tuyệt đối</p>
            <p className="txt-color-invert hide">
              Nhấp vào nút là bạn đã đồng ý với
              <Link to="/terms-of-use"  className="txt-color-invert padding-l5">Điều khoản sử dụng</Link>
            </p>
          </div>
        </section>
        <section className="lp-well lp-well--decor">
          <div className="row">
            <div className="small-12 column">
              <h3 className="txt-color-1 txt-xxlg txt-center mblg">Chúng mình đã tìm thấy một nửa của nhau!</h3>
              <div className="txt-center mbxlg"><img src={LANDINGPAGE_BASE_URL + 'hr-decorate.png'} role="presentation" /></div>
            </div>
          </div>
          <div className="row">
            <div className="small-12 column">
              <div className="lp-carousel-wrap">
                <div className="lp-carousel" id="c1">
                  <div className="lp-carousel__item lp-carousel__item--3 padding-t0">
                    <div className="mbmd"><img className="img-full-width" src={LANDINGPAGE_BASE_URL + 'mariage-vietnamien-blog-vietnam.jpg'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" /></div>
                    <div className="txt-ground txt-left l-flex l-flex--vsb">
                      <p className="txt-color-1 txt-bold">"Cảm ơn Ymeet.me đã giúp chúng tôi tìm thấy nhau"</p>
                      <p className="txt-muted txt-sm">Anh Nguyễn Lê Duy - Chị Nguyễn Thúy An <br /> Thanh Xuân - Hà Nội</p>
                    </div>
                  </div>
                  <div className="lp-carousel__item lp-carousel__item--3 padding-t0">
                    <div className="mbmd"><img className="img-full-width" src={LANDINGPAGE_BASE_URL + 'young-vietnamese-newly-married-couple-have-their-wedding-photographs-F4FDBA.jpg'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" /></div>
                    <div className="txt-ground txt-left l-flex l-flex--vsb">
                      <p className="txt-color-1 txt-bold">"Chúng tôi đã quyết định đi đến hôn nhân sau khi tìm hiểu nhau qua online cũng như khi gặp thực tế"</p>
                      <p className="txt-muted txt-sm">Anh Nguyễn Đức Hoàng - Chị Vũ Thu Ngân <br /> Cầu Giấy - Hà Nội</p>
                    </div>
                  </div>
                  <div className="lp-carousel__item lp-carousel__item--3 padding-t0">
                    <div className="mbmd"><img className="img-full-width" src={LANDINGPAGE_BASE_URL + 'stock-photo-hanoi-vietnam-octorber-undefined-vietnamese-couple-shooting-for-wedding-photo-album-in-491999470.jpg'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" /></div>
                    <div className="txt-ground txt-left l-flex l-flex--vsb">
                      <p className="txt-color-1 txt-bold">"Trung thực chính là yếu tố giúp duy trì mối quan hệ của chúng mình, dù là yêu xa"</p>
                      <p className="txt-muted txt-sm">Anh Đoàn Minh Đức - Chị Phương Dung <br /> Bắc Ninh</p>
                    </div>
                  </div>
                </div>
                <button className="lp-carousel__btn lp-carousel__btn--prev" id="c1-prev"><i className="fa fa-chevron-left fa-3x"></i></button>
                <button className="lp-carousel__btn lp-carousel__btn--next" id="c1-next"><i className="fa fa-chevron-right fa-3x"></i></button>
              </div>
            </div>
          </div>
        </section>
        <section className="lp-well lp-well--2">
          <div className="row">
            <div className="small-12 column">
              <h3 className="txt-color-1 txt-lg txt-center mbxlg">Tìm một nửa với <span className="txt-bold">Ymeet.me</span> như thế nào?</h3>
            </div>
          </div>
          <div className="row">
            <div className="small-12 column">
              <div className="lp-carousel-wrap">
                <div className="lp-carousel" id="c2">
                  <div className="lp-carousel__item txt-center">
                    <div className="mbmd"><img className="bordered" src={LANDINGPAGE_BASE_URL + 'image-4.jpg'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" /></div><span className="lp-badge txt-bold mrsm">1</span><span className="txt-muted">Đăng nhập qua facebook</span>
                  </div>
                  <div className="lp-carousel__item txt-center">
                    <div className="mbmd"><img className="bordered" src={LANDINGPAGE_BASE_URL + 'image-5.jpg'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" /></div><span className="lp-badge txt-bold mrsm">2</span><span className="txt-muted">Chọn người bạn thích</span>
                    <button className="lp-carousel__btn lp-carousel__btn--1 lp-carousel__btn--next" id="c2-next"><i className="fa fa-chevron-right fa-3x"></i></button>
                  </div>
                  <div className="lp-carousel__item txt-center">
                    <div className="mbmd">
                      <img className="bordered" src={LANDINGPAGE_BASE_URL + 'image-6.jpg'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" />
                    </div>
                    <span className="lp-badge txt-bold mrsm">3</span>
                    <span className="txt-muted">Gửi và nhận thích</span>
                    <button className="lp-carousel__btn lp-carousel__btn--next lp-carousel__btn--1" id="c2-next"><i className="fa fa-chevron-right fa-3x"></i></button>
                  </div>
                  <div className="lp-carousel__item txt-center">
                    <div className="mbmd">
                      <img className="bordered" src={LANDINGPAGE_BASE_URL + 'image-7.jpg'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" />
                    </div>
                    <span className="lp-badge txt-bold mrsm">4</span>
                    <span className="txt-muted">Bắt đầu hẹn hò</span>
                    <button className="lp-carousel__btn lp-carousel__btn--nex lp-carousel__btn--1" id="c2-next"><i className="fa fa-chevron-right fa-3x"></i></button>
                  </div>
                </div>
                <button className="lp-carousel__btn lp-carousel__btn--prev" id="c2-prev"><i className="fa fa-chevron-left fa-3x"></i></button>
                <button className="lp-carousel__btn lp-carousel__btn--next" id="c2-next"><i className="fa fa-chevron-right fa-3x"></i></button>
              </div>
            </div>
          </div><img className="lp-well__decor" src={LANDINGPAGE_BASE_URL + 'bird.png'} role="presentation" />
        </section>
        <section className="lp-well lp-well--3">
          <div className="lp-poster mc">
            <h3 className="lp-poster__text">Cứ yên tâm hẹn hò<br />Việc an toàn cứ để <span className="txt-bold">Ymeet.me</span> lo!</h3>
          </div>
        </section>
        <section className="lp-well">
          <div className="row">
            <div className="small-12 column">
              <div className="lp-carousel-wrap">
                <div className="lp-carousel lp-carousel--2" id="c3">
                  <div className="lp-carousel__item lp-carousel__item--2">
                    <div className="mbmd txt-center" style={{height: 243}}>
                      <img src={LANDINGPAGE_BASE_URL + 'image-8.png'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" />
                    </div>
                    <div className="l-flex l-flex--ac l-flex--jc"><span className="txt-special mrsm">1</span>
                      <div>
                        <h4 className="txt-lg mbxs">Bí mật đến khi bạn công khai</h4>
                        <p className="txt-muted mbzero">Bạn bè trên Facebook của bạn hoàn toàn không biết</p>
                      </div>
                    </div>
                  </div>
                  <div className="lp-carousel__item lp-carousel__item--2">
                    <div className="mbmd txt-center">
                      <img src={LANDINGPAGE_BASE_URL + 'image-9.png'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" />
                    </div>
                    <div className="l-flex l-flex--ac l-flex--jc"><span className="txt-special mrsm">2</span>
                      <div>
                        <h4 className="txt-lg mbxs">Không hiển thị lên dòng thời gian</h4>
                        <p className="txt-muted mbzero">Ymeet.me sẽ không post bất cứ thứ gì <br /> lên timeline của bạn</p>
                      </div>
                    </div>
                  </div>
                  <div className="lp-carousel__item lp-carousel__item--2">
                    <div className="mbmd txt-center">
                      <img src={LANDINGPAGE_BASE_URL + 'image-10.png'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" />
                    </div>
                    <div className="l-flex l-flex--ac l-flex--jc"><span className="txt-special mrsm">3</span>
                      <div>
                        <h4 className="txt-lg mbxs">Xác minh độ tuổi</h4>
                        <p className="txt-muted mbzero">Chỉ bao gồm thành viên độc thân <br /> và 18 tuổi trở lên</p>
                      </div>
                    </div>
                  </div>
                  <div className="lp-carousel__item lp-carousel__item--2">
                    <div className="mbmd txt-center">
                        <img src={LANDINGPAGE_BASE_URL + 'image-11.png'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" />
                      </div>
                    <div className="l-flex l-flex--ac l-flex--jc"><span className="txt-special mrsm">4</span>
                      <div>
                        <h4 className="txt-lg mbxs">Giám sát 24 giờ/365 ngày</h4>
                        <p className="txt-muted mbzero">Tự động ngăn chặn và cảnh báo nội dung thô tục, vi phạm...</p>
                      </div>
                    </div>
                  </div>
                </div>
                <button className="lp-carousel__btn lp-carousel__btn--prev" id="c3-prev"><i className="fa fa-chevron-left fa-3x"></i></button>
                <button className="lp-carousel__btn lp-carousel__btn--next" id="c3-next"><i className="fa fa-chevron-right fa-3x"></i></button>
              </div>
            </div>
          </div>
        </section>
        <section className="lp-well lp-well--2 lp-well--bp">
          <div className="row">
            <div className="small-12 medium-7 medium-offset-3">
              <div className="txt-center">
                {!isAuthenticated && <FacebookLoginButton />}
                {!isAuthenticated && <GoogleLoginButton />}
                <p className="mbzero "><img className="mrsm" src={LANDINGPAGE_BASE_URL + 'Shield.png'} alt="shield-with-lock" />Cam kết bảo mật tuyệt đối</p>
                <p className="hide">
                  Nhấp vào nút là bạn đã đồng ý với
                  <Link to="/terms-of-use"  className="padding-l5">Điều khoản sử dụng</Link>
                </p>
              </div>
            </div>
          </div>
          <img className="lp-well__decor-2 hidden-sm" src={LANDINGPAGE_BASE_URL + 'iphone.png'} role="presentation" />
          <ScrollToTop />
        </section>
        <footer className="lp-footer">
          <div className="lp-footer__content">
            <div className="txt-center mblg"><img src={LANDINGPAGE_BASE_URL + 'Logo_white.png'} alt="Ymeet.me - hẹn hò toàn cho phụ nữ" /></div>
            <ul className="list-inline txt-center mbmd">
              <li><Link to="/terms-of-use">Điều khoản sử dụng</Link></li>
              <li><Link to="/privacy">Bảo mật</Link></li>
              <li><Link to="/helps">Trợ giúp</Link></li>
              <li><Link to="/contact">Liên hệ</Link></li>
              <li><a href="https://ymeet.me/blog/" target="_blank">Blog</a></li>
            </ul>
            <ul className="list-inline txt-center">
              <li><a href="https://facebook.com/ymeet.me" target="_blank" rel="noopener nofollow noreferrer"><i className="fa fa-facebook-square fa-2x"></i></a></li>
              <li><a href="https://twitter.com/Ymeetme" target="_blank" rel="noopener nofollow noreferrer"><i className="fa fa-twitter-square fa-2x"></i></a></li>
            </ul>
            <div className="txt-sm txt-center">Copyright (c) 2016 <a href="http://mmj.vn" target="_blank" rel="noopener noreferrer" className="txt-bold">Media Max Japan</a> All Rights Reserved.</div>
          </div>
        </footer>
        <div className={isFetching ? 'loader is-full-screen-load' : 'hide loader is-full-screen-load'}>
          <span className="loader__text">{ LOGGING_TEXT }</span>
        </div>
      </div>
    );
  }
}

LandingPage1.propTypes = {

}

export default LandingPage1
