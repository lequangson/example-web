import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

//import { socket } from 'components/socket';
import { SIMILAR_FACES_BY_PHOTO } from './Constant';
import { getSimilarFacesByPhoto } from 'pages/list-similar-to-my-photos/Action';
import { getIdentifierFromId } from 'utils/common';

class Socket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      callSimilarSocket: false
    };
    this.receive_event = this.receive_event.bind(this);
  }

  componentWillMount() {
    const { socket } = this.props;
    socket.on('receive_event', this.receive_event);
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps){
    const { isUploadingImageFromDevice } = this.props.upload_image;
    if(isUploadingImageFromDevice){
      const { socket } = this.props;
      socket.on('receive_event', this.receive_event);
      this.setState({ callSimilarSocket: false });
    }
  }

  receive_event(data) {
    switch (data.type) {
      case SIMILAR_FACES_BY_PHOTO:
        if(this.state.callSimilarSocket){
          return;
        }
        this._getSimilarFacesByPhoto(data);
        this.setState({ callSimilarSocket: !this.state.callSimilarSocket });
      default:
        break;
    }
  }

  _getSimilarFacesByPhoto(data){
    const { user_id, picture_id, picture_type, result } = data;
    const current_identifier = getIdentifierFromId(user_id);
    if(!result)
      return;
    this.props.getSimilarFacesByPhoto(current_identifier, picture_id);
  }

  render() {
    return (
      <div />
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSimilarFacesByPhoto: async (current_identifier, photo_id) => await dispatch(getSimilarFacesByPhoto(current_identifier, photo_id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Socket);

