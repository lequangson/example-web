import { connect } from 'react-redux';
import io from 'socket.io-client';

import Socket from './Socket';

class ConnectedSocket {
  constructor(url) {
    this.url = url;
    this.connectedStatus = false;
    this.emitQueues = [];
    this.socket = io(url, {
      transports: ['websocket'] // you need to explicitly tell it to use websockets
    });
    this.onConnect();
    this.onDisconnect();
  }

  onConnect() {
    this.socket.on('connect', () => {
      this.connectedStatus = true;
      this.process_all_queue();
    });
  }

  onDisconnect() {
    this.socket.on('disconnect', () => {
      this.connectedStatus = false;
      this.emitQueues = [];
    });
  }

  emit(eventName, data) {
    if (this.connectedStatus) {
      this.socket.emit(eventName, data);
    } else {
      this.emitQueues.push({eventName, data});
    }
  }

  on(eventName, data) {
    this.socket.on(eventName, data);
  }

  process_all_queue() {
    const webSocket = this.socket;
    if (!webSocket) {
      return;
    }

    if (this.emitQueues && this.emitQueues.length > 0) {
      this.emitQueues.forEach((queue) => {
        if (this.connectedStatus) {
          webSocket.emit(queue.eventName, queue.data);
        }
      });
    }
  }
}


const socket = new ConnectedSocket(process.env.SOCKET_URL);


export { socket };
