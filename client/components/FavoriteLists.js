/* global document:true */

import React, { Component, PropTypes } from 'react'
import $ from 'jquery'
import DetailItem from './searches/DetailItem'
import { FAVORITE_PAGE, DEFAULT_LOAD_FEMALE, DEFAULT_LOAD_MALE } from '../constants/Enviroment'
import { LOADING_DATA_TEXT, LOADING_DATA_TEXT_AFTER_4SECS } from '../constants/TextDefinition'
import * as ymmStorage from '../utils/ymmStorage';
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class FavoriteLists extends Component {
  componentWillMount() {
    ymmStorage.removeItem('keep')
    this.props.getFavorite()
    this.props.updatePreviousPage(FAVORITE_PAGE)
    setTimeout(() => {this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT_AFTER_4SECS)}, 4000)
  }

  @GoogleOptimize
  componentDidMount() {
    const userCardHeight = $('.element_height').height();
    const segmentIndex = this.props.Enviroment.profile_next_user_index * userCardHeight;
    window.scroll(0, this.props.Enviroment.favorite_scroll_top + segmentIndex);
  }

  componentWillUnmount() {
    this.props.updateScrollPossition(FAVORITE_PAGE, window.scrollY)
    this.props.setProfileUserNextIndex(0)
    this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT)
  }

  render() {
    const { isFetching, favorite_lists } = this.props.user_profile
    const isLoaded = this.props.user_profile.is_page_loading.favorite_lists
    const loadingText = this.props.Enviroment.isLoadingText
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE

    return (
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h2 className="txt-heading">Danh sách quan tâm</h2>
            </div>
          </div>
          <div className="row">
            {
              isLoaded ?
              favorite_lists.length > 0 ?
              (favorite_lists.map((user, i) =>
                <DetailItem
                  key={i}
                  id={`dtail-view-${i}`}
                  index={i}
                  data={user}
                  pageSource={FAVORITE_PAGE}
                />
              ))
              :
               <div className="row">
                 <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                   <article className="mbl mc">
                     <div className="card__upper">
                       <div href="#" className="card__link mbm">
                         <img src={default_image} alt="" className="card__img"/>
                       </div>
                       <div className="txt-center txt-light txt-lg">Những người bạn quan tâm sẽ được hiển thị ở đây. Hãy gửi thích tới họ ngay khi có thể nhé!</div>
                     </div>
                   </article>
                 </div>
               </div>
              :
              <div className="row">
                <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                  <article className="mbl mc">
                    <div className="card__upper">
                      <div href="#" className="card__link">
                        <img src={default_image} alt="" className="card__img"/>
                      </div>
                      <div className="card__center loader is-load">
                        <div className="mbs">{loadingText}</div>
                        <i className="fa fa-spinner fa-3x"></i>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
}

FavoriteLists.propTypes = {
  getFavorite: PropTypes.func.isRequired,
  updatePreviousPage: PropTypes.func.isRequired,
  updateScrollPossition: PropTypes.func.isRequired,
  Enviroment: PropTypes.object.isRequired,
  user_profile: PropTypes.object.isRequired,
  setProfileUserNextIndex: PropTypes.func,
}

export default FavoriteLists
