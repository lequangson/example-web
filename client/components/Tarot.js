import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import q from 'q';
import { Link, browserHistory } from 'react-router'
import Modal from 'components/modals/Modal'
import _ from 'lodash'
import { random } from 'lodash';
import $ from 'jquery'
import { CDN_URL } from '../constants/Enviroment'
import { TAROT_PAGE_1, TAROT_PAGE_2, TAROT_PAGE_3,TAROT_COST } from '../constants/Tarot'
import CryptoJS from 'crypto-js';
import * as ymmStorage from '../utils/ymmStorage';
import { coinConsume } from '../actions/Payment';
import { getCurrentUser } from '../actions/userProfile';
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class Tarot extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
      cardText: '',
      hadNumber: [],
      currentDate: null
    }
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
    this.currentDateUpdate = this.currentDateUpdate.bind(this);
    this.getCardContent = this.getCardContent.bind(this);
  }

  componentWillMount() {

    const { Tarot, params } = this.props
    const { option, card_id } = params
    if (typeof card_id !== 'undefined') {
      if (!ymmStorage.getItem('tarot_secret_key')) {
        browserHistory.push('/tarot-page');
        return;
      }
      const tarot_decode_number = CryptoJS.AES.decrypt(`${ymmStorage.getItem('tarot_secret_key')}`, process.env.ANONYMOUS_TOKEN).toString(CryptoJS.enc.Utf8);
      if (tarot_decode_number != `${option}${card_id}`) {
        browserHistory.push('/tarot-page');
        return;
      }
      const { tarot_card_codes } = Tarot
      // const end_random_range = tarot_card_codes.length - 1;
      // const randomNumber = random(0, end_random_range)
      const selectedQuestion = tarot_card_codes[card_id];
      const cardText = selectedQuestion.text[option-1];
      this.props.updateContentPage3(selectedQuestion.image, selectedQuestion.name)
      this.setState({cardText})
    }

  }

  @GoogleOptimize
  componentDidMount() {}

  currentDateUpdate() {
    browserHistory.push('/tarot-page')
    // this.setState({currentDate: 1})
  }

  openModal() {
    this.setState(
      {
        modalIsOpen: true
      }
    )
  }

  closeModal() {
    this.setState(
      {
        modalIsOpen: false
      }
    )
  }

  completePage(pageNunber) {
    return this.props.updateTarotPage(pageNunber)
  }

  addAnimation() {
    const tarotCard = document.querySelector('.tarot-card')
    tarotCard.classList.add('vanish-out')
    setTimeout(() => tarotCard.classList.remove('vanish-out'), 3000)
  }

  tarotStyle() {
    return {
      minHeight: 90 + 'vh',
      marginTop: '-1rem',
      backgroundImage: `url(${CDN_URL}/general/Tarot/Background.jpg)`,
      backgroundSize: 'cover'
    }
  }

  checkHadNumber(number) {
    const had = this.state.hadNumber.find(element => {
      return element === number;
    })
    return had ;
  }

  getCardContent(randomNumber) {
    const { params, Tarot } = this.props
    const { tarot_card_codes } = Tarot
    const { option, card_id } = params
    const number = randomNumber ? randomNumber : parseInt(card_id)
    const randomCardCode = tarot_card_codes[number];
    this.props.updateContentPage3(randomCardCode.image, randomCardCode.name)
    const currentCardText = randomCardCode.text[option];
    this.setState({cardText: currentCardText})
  }

  handleClick() {
    const { Tarot, params } = this.props
    const { option } = params
    const { tarot_card_codes } = Tarot
    const end_random_range = tarot_card_codes.length - 1;
    const randomNumber = random(0, end_random_range)
    const randomCardCode = tarot_card_codes[randomNumber];
    const currentCardText = randomCardCode.text[option-1];
    this.props.tarotSubmit().then(() => {
      this.props.getCurrentUser();
      this.setState({cardText: currentCardText})
      this.props.updateContentPage3(randomCardCode.image, randomCardCode.name)
      this.props.updateTarotPage(TAROT_PAGE_3)
      const tarto_secret_key = CryptoJS.AES.encrypt(`${option}${randomNumber}`, process.env.ANONYMOUS_TOKEN).toString();
      ymmStorage.removeItem('tarto_secret_key');
      ymmStorage.setItem('tarot_secret_key', tarto_secret_key);
      browserHistory.push(`/tarot-page/${option}/${randomNumber}`)
    }).catch(() => {})
  }

  // componentDidMount() {
    // this.getCardContent()
  // }

  rememberPaymentIntention() {
    ymmStorage.removeItem('Payment_Intention');
    ymmStorage.setItem('Payment_Intention', 'TAROT_ONLINE');
  }

  fixHeight() {
    const { params } = this.props;
    const { card_id } = params
    if (!card_id) {
      return {};
    }
    return {
      paddingBottom: 180 + '%'
    }
  }

  render() {
    const { Tarot, params, user_profile } = this.props
    const coins = user_profile.current_user ? user_profile.current_user.coins : 0;
    const { option, card_id } = params
    const { tarot_curent_page, tarot_page,tarot_card_codes } = Tarot
    const tarot_curent = card_id ? 3 : (option ? 2 : 1)

    return (
      <div className='site-content' style={this.tarotStyle()}>
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              {tarot_page.map((page, i) => {
                if (tarot_curent === page.pageNunber) {
                  return (
                    <div className="mbl padding-t30 txt-center" key={i}>
                      <div className="row clearfix">
                        <div className="col-xs-6 col-sm-4 col-xs-offset-3 col-sm-offset-4">
                          <div className="card__link" style={this.fixHeight()}>
                            <img src={page.url} alt="img-70" className="img-100 mbl padding-b10"/>
                          </div>
                        </div>
                      </div>
                      <div className="txt-bold txt-white txt-xxxlg txt-uppercase">{page.text1}</div>
                      <div className="txt-white txt-xlg txt-uppercase" dangerouslySetInnerHTML={{ __html: page.text2 }}></div>
                    </div>
                  )
                }
                return '';
              }
              )}
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <div className={`col-xs-12 padding-t20 ${tarot_curent === TAROT_PAGE_1 ? '' : 'hide'}`}>
                <Link to="/tarot-page/1" className="btn btn--b btn--bg-transparent mbm">
                  <i className="fa fa-question-circle txt-xxlg mrs" aria-hidden="true"></i>
                  Tình duyên 3 tháng tới sẽ như thế nào?
                </Link>
                <Link to="/tarot-page/2" className="btn btn--b btn--bg-transparent mbm">
                  <i className="fa fa-question-circle txt-xxlg mrs" aria-hidden="true"></i>
                  Mối quan hệ của bạn sẽ đi đến đâu?
                </Link>
                <Link to="/tarot-page/3" className="btn btn--b btn--bg-transparent mbm">
                  <i className="fa fa-question-circle txt-xxlg mrs" aria-hidden="true"></i>
                  Bạn nên làm gì dể mối quan hệ <br />đi đúng hướng
                </Link>
              </div>
              <div className={`txt-center ${tarot_curent === TAROT_PAGE_2 ? '' : 'hide'}`}>
                <img src={`${CDN_URL}/general/Tarot/Cards.png`} alt="Cards" className="img-150 cursor tarot-card" onClick={() => this.openModal()} />
              </div>
              <div className={`${tarot_curent === TAROT_PAGE_3 ? '' : 'hide'}`}>
                <div className="txt-white mbm">{this.state.cardText}</div>
                <div className="col-xs-12">
                  <button className="btn btn--p btn--b txt-medium" onClick={() => this.currentDateUpdate()}>Rút thêm bài</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          transitionName="modal"
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
           <div className="txt-center">
              <img className="img-firstlike" src={`${CDN_URL}/general/Tarot/banner-tarot.png`} alt="" />
            </div>
          <div className="modal__txt-content">
            <div className="txt-center padding-t20 padding-b20"> Số xu hiện có: <span className="txt-blue">{coins} xu</span></div>
            {(coins >= TAROT_COST) ?
              <button onClick ={() => {
                  this.closeModal()
                  this.addAnimation()
                  setTimeout(() => {this.handleClick()}, 100)
                  }
                }
                className='btn btn--p btn--b'>
                  Xem ngay!
                </button>
              :
              <Link
                to="/payment/coin"
                className='btn btn--p btn--b'
                onClick={() => {
                  // this.props.gaSend(GA_ACTION_GET_MORE_COIN, { page_source: 'Verify User' })
                  this.rememberPaymentIntention()
                }}>Nạp thêm xu</Link>
            }
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    )
  }
}

Tarot.propTypes = {
  updateUserProfile: PropTypes.func,
  dictionaries: PropTypes.object,
  user_profile: PropTypes.object,
  modalShowNext: PropTypes.func,
  validateImage: PropTypes.func,
  saveImage: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    tarotSubmit: () => {
      const defer = q.defer();
      dispatch(coinConsume("coin_consume_tarot_reading")).then( res => {
        defer.resolve(res);
      }).catch( e => {
        defer.reject(false);
      })
      return defer.promise;
    },
    getCurrentUser: () => {
      dispatch(getCurrentUser());
    },
  }
}

export default connect(mapStateToProps, mapDispachToProps)(Tarot);