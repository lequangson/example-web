import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import $ from 'jquery';
import _ from 'lodash';
import { Link, browserHistory } from 'react-router';
import {
  NOTIFICATION_ERROR, WHAT_HOT_PAGE, PEOPLE_LIKED_ME_PAGE,
  GA_ACTION_BLOCK_USER, GA_ACTION_MUTE_USER, SEARCH_PAGE } from 'constants/Enviroment';
import { NO_BLOCK_TYPE_CHOOSE_MSG } from 'constants/TextDefinition';
import { BLOCK_TYPE_BLOCKED, BLOCK_TYPE_MUTED } from 'constants/userProfile';
import { getUserByIdentifierFromAppState } from 'utils/common';
import * as ymmStorage from 'utils/ymmStorage';
import { getBlockedUsers } from 'actions/userProfile';
import { showNextPeopleLikedMe } from 'pages/people-liked-me/Actions';
import MediaProfile from './commons/MediaProfile';

class BlockForm extends React.Component {

  static handleClick() {
    ymmStorage.removeItem('inprogress');
  }

  constructor() {
    super()
    this.state = {
      identifier: '',
      block_type: 0,
      update_success: false,
      selected_user: {},
    }
  }

  componentWillMount() {
    this.setState({ identifier: this.props.params.identifier })
    const selected_user = getUserByIdentifierFromAppState(this.props.params.identifier, this.props, this.props.search.search_mode)
    if (_.isEmpty(selected_user)) {
      this.props.getUserProfileByIdentifier(this.props.params.identifier);
    } else {
      this.setState({ selected_user })
    }
  }

  componentDidMount() {
    $('input:radio:checked').data('chk', true);
    $('input[type="radio"]').click(() => {
      $(`input[name='${$(this).attr('name')}']:radio`).not(this).removeData('chk');
      $(this).data('chk', !$(this).data('chk'));
      $(this).prop('checked', $(this).data('chk'));
    })
  }

  componentWillReceiveProps() {
    const { user_profile } = this.props
    const { selected_user } = user_profile
    if (!_.isEmpty(selected_user) && _.isEmpty(this.state.selected_user) && selected_user.block_type) {
      browserHistory.push(`/profile/${this.state.identifier}`)
      return
    }
  }
  handleUpdate() {
    if (ymmStorage.getItem('inprogress')) {
      return
    }
    ymmStorage.setItem('inprogress', true);
    const selected_block_type = $("input[type='radio']:checked").val()
    const that = this
    if (typeof selected_block_type === 'undefined') {
      this.props.sendNotification(NO_BLOCK_TYPE_CHOOSE_MSG, 5000, NOTIFICATION_ERROR)
    } else {
      this.props.blockUser(this.state.identifier, selected_block_type).then(() => {
        this.props.getBlockedUsers(BLOCK_TYPE_MUTED);
        that.sendGaEvent(selected_block_type);
        that.setState({
          update_success: true,
          block_type: selected_block_type,
        })
        ymmStorage.removeItem('inprogress');
        that.props.updateReducerState(
          that.props.Enviroment.previous_page,
          that.props.params.identifier,
          'block_type',
          selected_block_type
        )
        that.props.updateReducerState(SEARCH_PAGE, that.props.params.identifier, 'block_type', selected_block_type)
        that.props.blockNotificationByIdentifier(that.props.params.identifier)
        if (that.props.Enviroment.previous_page === WHAT_HOT_PAGE) {
          that.props.showNextWhatHotUser();
        }
        if (that.props.Enviroment.previous_page === PEOPLE_LIKED_ME_PAGE) {
          that.props.showNextPeopleLikedMe();
        }
      }).catch((ex) => {
        browserHistory.push(`/profile/${that.state.identifier}`)
        ymmStorage.removeItem('inprogress');
      })
    }
  }

  sendGaEvent(blockType) {
    const user = !_.isEmpty(this.state.selected_user) ? this.state.selected_user : this.props.user_profile.selected_user
    const gaAction = parseInt(blockType, 10) === BLOCK_TYPE_BLOCKED ? GA_ACTION_BLOCK_USER : GA_ACTION_MUTE_USER
    const ga_track_data = {
      partner_residence: (user && user.residence) ? user.residence.value : '',
      partner_birth_place: (user && user.birth_place) ? user.birth_place.value : '',
      partner_identifier: this.state.identifier,
      partner_age: user ? user.age : null,
    };
    this.props.gaSend(gaAction, ga_track_data);
  }

  render() {
    if (_.isEmpty(this.state.selected_user) && _.isEmpty(this.props.user_profile.selected_user)) {
      return <div>Loading...</div>
    }
    let user = this.state.selected_user
    if (_.isEmpty(user)) {
      user = this.props.user_profile.selected_user
    }
    return (
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h2 className="txt-heading">Chặn tài khoản</h2>
              <MediaProfile user={user} />
              { !this.state.update_success && <p>Chọn hình thức chặn tài khoản mà bạn muốn</p> }
            </div>
          </div>
          { !this.state.update_success && <div>
            {/* Choose block options */}
            <div className="row">
              <div className="col-xs-6">
                <label className="form__input--c">
                  <input
                    type="radio"
                    name="block_user"
                    value={BLOCK_TYPE_MUTED}
                    onClick={() => BlockForm.handleClick()}
                  />
                  Chặn thông báo
                  <div className="fake-radio"></div>
                </label>
              </div>
            </div>
            <div className="row mbm">
              <div className="col-xs-12">
                <ul className="list-unstyled">
                  <li className="txt-light txt-italic">
                    * Không hiển thị thông báo nếu người này thích tôi.
                  </li>
                  <li className="txt-light txt-italic">
                    * Không hiển thị thông báo nếu người này nhắn tin cho tôi.
                  </li>
                  <li className="txt-light txt-italic">
                    * Không cập nhật động trạng thái từ người này lên danh sách khách thăm của tôi.
                  </li>
                </ul>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-6">
                <label className="form__input--c">
                  <input
                    type="radio"
                    name="block_user"
                    value={BLOCK_TYPE_BLOCKED}
                    onClick={() => { BlockForm.handleClick() }}
                  />
                  Chặn hai chiều
                  <div className="fake-radio"></div>
                </label>
              </div>
            </div>
            <div className="row mbl">
              <div className="col-xs-12">
                <ul className="list-unstyled">
                  <li className="txt-light txt-italic">
                    * Không hiển thị mọi thông tin tài khoản của tôi trên kết quả tìm kiếm,
                    danh sách yêu thích, danh sách khách thăm, danh sách quan tâm,
                    danh sách ghép đôi của người này.
                  </li>
                  <li className="txt-light txt-italic">
                    * Không hiển thị mọi thông tin tài khoản của người này trên kết quả tìm kiếm,
                    danh sách yêu thích, danh sách khách thăm, danh sách quan tâm,
                    danh sách ghép đôi của tôi.
                  </li>
                </ul>
              </div>
            </div>
            <div className="row mbl">
              <div className="col-xs-6">
                <Link to={`/profile/${this.state.identifier}`} className="btn btn--b">Hủy</Link>
              </div>
              <div className="col-xs-6">
                <button
                  className="btn btn--p btn--b"
                  onClick={() => { this.handleUpdate() }}
                >
                  Cập nhật
                </button>
              </div>
            </div>
            {/* End */}
          </div>
          }
          {/* Update block options */}
          { this.state.update_success && <div className="row mbl">
            <div className="col-xs-12">
              <p>
                Bạn đã chặn tài khoản thành công.<br />
                Tài khoản này đã được thêm vào Danh sách chặn
                {parseInt(this.state.block_type, 10) === BLOCK_TYPE_BLOCKED ? ' hai chiều ' : ' thông báo ' }
                của bạn.
              </p>
              <Link to={`/profile/${this.state.identifier}`} className="btn btn--b">Quay lại</Link>
            </div>
          </div>
          }

          {/* End */}
        </div>
      </div>
    )
  }
}

BlockForm.propTypes = {
  user_profile: PropTypes.object,
  params: PropTypes.object,
  blockUser: PropTypes.func,
  sendNotification: PropTypes.func,
  gaSend: PropTypes.func,
  getUserProfileByIdentifier: PropTypes.func,
  showNextWhatHotUser: PropTypes.func,

}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    getBlockedUsers: (block_type) => dispatch(getBlockedUsers(block_type)),
    showNextPeopleLikedMe: () => dispatch(showNextPeopleLikedMe()),
  }
}
export default connect(mapStateToProps, mapDispachToProps)(BlockForm);