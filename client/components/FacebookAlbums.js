import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

class FacebookAlbums extends Component {
  static renderAnAlbum(album, pictureType) {
    return (
      <div className="l-flex-grid__item" key={album.id}>
        <Link to={`/fb-pictures/${pictureType}/${album.id}`}>
          <div className="mbs"><img src={album.picture_url} alt="" /></div>
          <div>
            <span className="txt-blue txt-bold">{album.name}</span>
            <br />
            <span className="txt-light txt-sm">{album.photo_count} ảnh</span></div>
        </Link>
      </div>
    )
  }

  componentWillMount() {
    // We just get Facebook albums when it has not got yet
    if (this.props.user_profile.fb_albums.length === 0) {
      this.props.getFacebookAlbums()
    }
  }

  render() {
    const { pictureType } = this.props
    const { fb_albums, isFetching, current_user } = this.props.user_profile
    if (fb_albums.length === 0 && isFetching) {
      return <div>Loading...</div>
    }
    const isCompletedWelcome = current_user.welcome_page_completion > 2 || current_user.provider === 'facebook'
    return (
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3 className="txt-heading">Facebook Album</h3>
              <p className="txt-light">Chọn album</p>
            </div>
            <div className="col-xs-12 mbl">
              <div className="l-flex-grid">
                { fb_albums.map(album => this.constructor.renderAnAlbum(album, pictureType)) }
              </div>
            </div>
            <div className="col-xs-12">
              <Link to={`${isCompletedWelcome ? '/myprofile' : '/welcome'}`}>
                <button className="btn btn--b mbm">Hủy</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

FacebookAlbums.propTypes = {
  user_profile: PropTypes.object.isRequired,
  getFacebookAlbums: PropTypes.func.isRequired,
  pictureType: PropTypes.string.isRequired,
}

export default FacebookAlbums
