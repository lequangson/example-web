import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import Webcam from 'react-webcam';
import {
  CDN_URL
} from 'constants/Enviroment';

class CameraYmm extends Component {
  static propTypes = {
    onCloseCallback: PropTypes.func.isRequired,
    onTakePhotoSuccess: PropTypes.func.isRequired,
    gender: PropTypes.string.isRequired
  };

  static defaultProps = {
  };

  state = {
    isOpen: true,
    viewFreezedPhoto: false,
    photoData: ''
  }

  onRequestClose = () => {
    this.setState({
      isOpen: false
    });
    this.props.onCloseCallback();
  }

  setRef = (webcam) => {
    this.webcam = webcam;
  }

  capture = async () => {
    const  b64Data = await this.webcam.getScreenshot();
    if (b64Data) {
      this.setState({
        photoData: b64Data,
        viewFreezedPhoto: true
      });
    }
  }

  rejectFreezedPhoto = () => {
    this.setState({
      viewFreezedPhoto: false
    });
  }

  acceptFreezedPhoto = () => {
    const file = this.blobToBlobUrlArray();
    this.props.onTakePhotoSuccess(file);
  }

  b64toBlob(b64Data, contentType='', sliceSize=512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = [];
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, {type: contentType});
    const name = `${Date.now()}.${contentType.split('/')[1]}`;
    blob.name = name;
    blob.lastModifiedDate = new Date();
    return blob;
  }

  blobToBlobUrlArray() {
    const block = this.state.photoData.split(";");
    const contentType = block[0].split(":")[1]; //In this case "image/jpeg"
    const realData = block[1].split(",")[1];  // In this case "R0lGODlhPQBEAPeoAJosM...."
    const blob = this.b64toBlob(realData, contentType);
    const url = URL.createObjectURL(blob);
    const blobUrlArray = [];

    blob.preview = url;
    blobUrlArray.push(blob);
    return blobUrlArray;
  }

  renderCamera() {
    const { gender } = this.props;
    return (
      <div className="l-flex-v-c pos-relative">
        <div className="selfie-tut">
          {gender == 'male' ?
            <img className="img-100" src={`${CDN_URL}/general/Verify/sample-photo-male1.jpg`} alt="sample-photo-male1.jpg" />
            :
            <img className="img-100" src={`${CDN_URL}/general/Verify/sample-photo-female1.jpg`} alt="sample-photo-female1.jpg" />
          }
        </div>
        <Webcam
          audio={false}
          height={350}
          ref={this.setRef}
          screenshotFormat="image/jpeg"
          width={350}
        />
        <button className="mbm padding-0 no-border img-round" onClick={this.capture}>
          <img className="img-50" src={`${CDN_URL}/general/Verify/Camera.png`} alt="Chụp ảnh"/>
        </button>
      </div>
      );
  }

  renderFreezedPhoto() {
    return (
      <div>
        <img className="mbs" alt="Ảnh đã chụp" src={this.state.photoData} />
        <div className="row">
          <div className="col-xs-6 txt-right">
            <button className="mbm padding-0 no-border img-round" onClick={this.rejectFreezedPhoto}><img className="img-50" src={`${CDN_URL}/general/Verify/Button2.png`} alt="Quay lại"/></button>
          </div>
          <div className="col-xs-6 txt-left">
            <button className="mbm padding-0 no-border img-round" onClick={this.acceptFreezedPhoto}><img className="img-50" src={`${CDN_URL}/general/Verify/Button1.png`} alt="Chấp nhận"/></button>
          </div>
        </div>
      </div>
      );
  }

  render() {
    return (
      <Modal
        isOpen={this.state.isOpen}
        onRequestClose={this.onRequestClose}
        className="BoostRankModal modal__content modal__content--2"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=""
      >
        {this.state.viewFreezedPhoto ? this.renderFreezedPhoto() : this.renderCamera()}
      </Modal>
      );
  }
}

export default CameraYmm;