import React, { PropTypes } from 'react';
import $ from 'jquery'
import { PEOPLE_LIKED_ME_PAGE, BLOCK_USER_BLOCKED_PAGE, BLOCK_USER_MUTED_PAGE, DEFAULT_LOAD_MALE, DEFAULT_LOAD_FEMALE } from '../constants/Enviroment'
import { BLOCK_TYPE_BLOCKED, BLOCK_TYPE_MUTED } from '../constants/userProfile'
import { THERE_IS_NO_ONE_YOU_BLOCKED, THERE_IS_NO_ONE_YOU_MUTED, LOADING_DATA_TEXT, LOADING_DATA_TEXT_AFTER_4SECS } from '../constants/TextDefinition'
import ListItem from './searches/ListItem'
import * as ymmStorage from '../utils/ymmStorage';

class BlockUsers extends React.Component {
  constructor() {
    super()
    this.state = {
      page: '',
      no_record_found_msg: '',
    }
  }
  componentWillMount(pathnameParam = '') {
    ymmStorage.removeItem('keep')
    this.setState(
      {
        page: this.props.page,
      }
    )
    let pathname = this.props.location.pathname
    if (pathnameParam !== '') {
      pathname = pathnameParam
    }
    switch (pathname) {
      case '/blocked-users':
        this.setState({ page: 'blocked-users', no_record_found_msg: THERE_IS_NO_ONE_YOU_BLOCKED })
        this.props.getBlockedUsers(BLOCK_TYPE_BLOCKED)
        this.props.updatePreviousPage(BLOCK_USER_BLOCKED_PAGE)
        break
      case '/muted-users':
        this.setState({ page: 'muted-users', no_record_found_msg: THERE_IS_NO_ONE_YOU_MUTED })
        this.props.getBlockedUsers(BLOCK_TYPE_MUTED)
        this.props.updatePreviousPage(BLOCK_USER_MUTED_PAGE)
        break
      default:
        break
    }
    setTimeout(() => {this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT_AFTER_4SECS)}, 4000)
  }

  componentDidMount() {
    const userCardHeight = $('.element_height').height();
    const segmentIndex = this.props.Enviroment.profile_next_user_index * userCardHeight;
    if (this.state.page === 'blocked-users') {
      window.scroll(0, this.props.Enviroment.blocked_user_scroll_top + segmentIndex);
    } else {
      window.scroll(0, this.props.Enviroment.muted_user_scroll_top + segmentIndex);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.location.pathname !== nextProps.location.pathname) {
      this.componentWillMount(nextProps.location.pathname)
    }
  }

  componentWillUnmount() {
    const pageSource = this.state.page === 'blocked-users' ? BLOCK_USER_BLOCKED_PAGE : BLOCK_USER_MUTED_PAGE;
    this.props.updatePreviousPage(pageSource)
    this.props.updateScrollPossition(pageSource, window.scrollY)
    this.props.setProfileUserNextIndex(0)
    this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT)
  }

  render() {
    const blocked_users = this.state.page === 'blocked-users'
    ?
    this.props.user_profile.blocked_users
    :
    this.props.user_profile.muted_users
    const isLoaded = this.props.user_profile.is_page_loading.blocked_users
    const loadingText = this.props.Enviroment.isLoadingText
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE

    return (
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h2 className="txt-heading">
                Danh sách chặn
                {this.state.page === 'blocked-users' ? ' hai chiều' : ' thông báo'}
              </h2>
              {this.state.page === 'blocked-users'
                && <ul className="list-unstyled txt-light txt-italic">
                  <li>
                    * Mọi thông tin tài khoản của bạn không hiển thị trên kết quả tìm kiếm,
                    danh sách yêu thích, danh sách khách thăm,
                    danh sách thích, danh sách ghép đôi của người này.
                  </li>
                  <li>
                    * Mọi thông tin tài khoản của người này không hiển thị trên kết quả tìm kiếm,
                    danh sách yêu thích, danh sách khách thăm,
                    danh sách thích, danh sách ghép đôi của bạn.
                  </li>
                </ul>
              }
              {this.state.page !== 'blocked-users'
                && <ul className="list-unstyled txt-light txt-italic">
                  <li>* Không hiển thị thông báo nếu người này thích tôi</li>
                  <li>* Không hiển thị thông báo nếu người này nhắn tin cho tôi</li>
                  <li>* Không hiển thị thông báo nếu người này ghé thăm hồ sơ của tôi</li>
                </ul>
              }
            </div>
          </div>
          <div className="row">
            {
              isLoaded ?
              blocked_users.length > 0 ?
              (blocked_users.map((user, i) =>
                <ListItem
                  key={i}
                  id={`dtail-view-${i}`}
                  index={i}
                  data={user}
                />
              ))
              :
               <div className="row">
                 <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                   <article className="mbl mc">
                     <div className="card__upper">
                       <div href="#" className="card__link mbm">
                         <img src={default_image} alt="" className="card__img"/>
                       </div>
                       <div className="txt-center txt-light txt-lg">{this.state.no_record_found_msg}</div>
                     </div>
                   </article>
                 </div>
               </div>
              :
              <div className="row">
                <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                  <article className="mbl mc">
                    <div className="card__upper">
                      <div href="#" className="card__link">
                        <img src={default_image} alt="" className="card__img"/>
                      </div>
                      <div className="card__center loader is-load">
                        <div className="mbs">{loadingText}</div>
                        <i className="fa fa-spinner fa-3x"></i>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
}

BlockUsers.propTypes = {
  page: PropTypes.string,
  user_profile: PropTypes.object,
  Enviroment: PropTypes.object,
  location: PropTypes.object,
  updateScrollPossition: PropTypes.func,
  updatePreviousPage: PropTypes.func,
  getBlockedUsers: PropTypes.func,
  setProfileUserNextIndex: PropTypes.func,
}
export default BlockUsers;
