// The middleware to call the API for quotes
import axios from 'axios'
import { CALL_API } from '../../middleware/api'
import {
  FACEBOOK_LOGIN_REQUEST,
  FACEBOOK_LOGIN_FAILURE,
  GOOGLE_LOGIN_REQUEST,
  GOOGLE_LOGIN_FAILURE,
  YMEETME_LOGIN_REQUEST,
  YMEETME_LOGIN_SUCCESS,
  YMEETME_LOGIN_FAILURE,
  AUTH_LOGOUT,
} from './Constant';

export function facebookLoginRequest() {
  return {
    type: FACEBOOK_LOGIN_REQUEST,
  }
}

export function facebookLogin(response) {
  if (response.accessToken === undefined) {
    return {
      type: FACEBOOK_LOGIN_FAILURE,
      isFetching: true,
      isAuthenticated: false,
      errorMessage: 'user cancel login or auto login failure',
    }
  }

  // get api key
  const request = axios({
    method: 'POST',
    url: process.env.API_GENERATE_KEY_URL,
    headers: [],
    data: {
      ip_address: '127.0.0.1',
      issue_time: 12323232,
    },
  })

  return {
    type: FACEBOOK_LOGIN_REQUEST,
    payload: request,
  }
}

export function facebookLoginFailure() {
  return {
    type: FACEBOOK_LOGIN_FAILURE,
  }
}

export function facebookLoginFailurePrivateMode() {
  return {
    type: 'FACEBOOK_LOGIN_FAILURE_PRIVATE_MODE',
  }
}

export function googleLoginFailurePrivateMode() {
  return {
    type: 'GOOGLE_LOGIN_FAILURE_PRIVATE_MODE',
  }
}

export function googleLogin(response) {
  //if (response.accessToken === undefined) {
  //  return {
  //    type: GOOGLE_LOGIN_FAILURE,
  //    isFetching: true,
  //    isAuthenticated: false,
  //    errorMessage: 'user cancel login or auto login failure',
  //  }
  //}

  // get api key
  const request = axios({
    method: 'POST',
    url: process.env.API_GENERATE_KEY_URL,
    headers: [],
    data: {
      ip_address: '127.0.0.1',
      issue_time: 12323232,
    },
  })

  return {
    type: GOOGLE_LOGIN_REQUEST,
    payload: request,
  }
}

export function googleLoginFailure() {
  return {
    type: GOOGLE_LOGIN_FAILURE,
  }
}

export function ymeetmeLogin() {
  return {
    [CALL_API]: {
      endpoint: '',
      method: 'GET',
      params: '',
      authenticated: false,
      types: [YMEETME_LOGIN_REQUEST, YMEETME_LOGIN_SUCCESS, YMEETME_LOGIN_FAILURE],
    },
  }
}

export function ymeetmeLoginWithGoogle() {
  return {
    [CALL_API]: {
      endpoint: '',
      method: 'GET',
      params: '',
      authenticated: false,
      googleAuth: true,
      types: [YMEETME_LOGIN_REQUEST, YMEETME_LOGIN_SUCCESS, YMEETME_LOGIN_FAILURE],
      extentions: GOOGLE_LOGIN_FAILURE,
    },
  }
}

export function ymeetmeLoginFailure() {
  return {
    type: YMEETME_LOGIN_FAILURE,
  }
}

export function logout() {
  return {
    type: AUTH_LOGOUT
  }
}
