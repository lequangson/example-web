import React, { PropTypes } from 'react';

class FacebookLogin extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
      FBLoaded: false, // prevent user click before FB initialize successfully
      responsedAutoLoad: false,
    }
    this.checkFBstatus = this.checkFBstatus.bind(this);
  }

  componentWillMount() {
    if (typeof window.FB !== 'undefined') {
      this.setState({ FBLoaded: true })
    }
  }
  componentDidMount() {
    const { appId, xfbml, cookie, version, autoLoad, language } = this.props;
    const fbRoot = document.createElement('div');
    fbRoot.id = 'fb-root';

    document.body.appendChild(fbRoot);

    window.fbAsyncInit = () => {
      window.FB.init({
        version: `v${version}`,
        appId,
        xfbml,
        cookie,
      });

      if (autoLoad || window.location.search.includes('facebookdirect')) {
        const { scope, appId } = this.props;
        // window.FB.login(this.checkLoginState, { scope, auth_type: 'rerequest' });
        window.FB.getLoginStatus(this.checkLoginState);
      }
    };
    // Load the SDK asynchronously
    ((d, s, id) => {
      const element = d.getElementsByTagName(s)[0];
      const fjs = element;
      let js = element;
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = `//connect.facebook.net/${language}/all.js`;
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');
    setTimeout(this.checkFBstatus, 1000);
  }

  /**
  * Because, after document ready, login buttons are available but The FB functions are not assigned => error
  * To make sure that FB apis are loaded, the following function will check status every  1 seconds ultil window.FB is available
  */
  checkFBstatus() {
    if (typeof window.FB === 'undefined') {
      setTimeout(this.checkFBstatus, 1000);
    }
    if (typeof window.FB !== 'undefined') {
      this.setState({ FBLoaded: true })
    }
  }
  responseApi = (authResponse) => {
    this.setState({ clicked: false, responsedAutoLoad: true })
    window.FB.api('/me', { fields: this.props.fields }, (me) => {
      Object.assign(me, authResponse);
      this.props.callback(me);
      this.props.facebookLogin(me, this.props.button_identifier);
    });
  };

  checkLoginState = (response) => {
    if (response.authResponse) {
      this.responseApi(response.authResponse);
    } else {
      if (this.props.callback) {
        this.setState({ clicked: false, responsedAutoLoad: true })
        this.props.callback({ status: response.status, 'props': this.props });
        this.props.facebookLogin({ status: response.status, 'props': this.props }, this.props.button_identifier);
      }
    }
  };

  click = () => {
    if (this.state.clicked || typeof window.FB === 'undefined') {
      return
    }
    this.setState({clicked: true})
    const { scope, appId } = this.props;
    window.location.href = `https://www.facebook.com/dialog/oauth?client_id=${appId}&redirect_uri=${window.location.href}&state=facebookdirect&scope=${scope}&auth_type=rerequest`;
    // if (navigator.userAgent.match('CriOS')) {
    //   window.location.href = `https://www.facebook.com/dialog/oauth?client_id=${appId}&redirect_uri=${window.location.href}&state=facebookdirect&scope=${scope}&auth_type=rerequest`;
    // } else {
    //   try{
    //     window.FB.login(this.checkLoginState, { scope, auth_type: 'rerequest' });
    //   } catch(e) {
    //     console.log('error')
    //   }
    // }
  };

  renderWithFontAwesome() {
    const { cssClass, size, icon, textButton } = this.props;
    return (
      <button
        className={cssClass}
        onClick={this.click}
        disabled={!this.state.FBLoaded || this.props.Enviroment.disable3rdPartyCookies}
      >
        {this.props.children}
      </button>
    );
  }

  render() {
    return this.renderWithFontAwesome();
  }
}

FacebookLogin.propTypes = {
  callback: PropTypes.func.isRequired,
  appId: PropTypes.string.isRequired,
  xfbml: PropTypes.bool,
  cookie: PropTypes.bool,
  scope: PropTypes.string,
  textButton: PropTypes.string,
  autoLoad: PropTypes.bool,
  size: PropTypes.string,
  fields: PropTypes.string,
  cssClass: PropTypes.string,
  version: PropTypes.string,
  icon: PropTypes.string,
  language: PropTypes.string,
  button_identifier: PropTypes.string,
}

FacebookLogin.defaultProps = {
  textButton: 'Login with Facebook',
  scope: 'email, user_birthday, public_profile, user_friends, user_photos, user_relationships, user_likes',
  // scope: 'email, user_birthday, user_friends, user_relationships, user_photos',
  xfbml: false,
  cookie: false,
  size: 'metro',
  fields: 'name',
  cssClass: 'kep-login-facebook',
  version: '2.3',
  language: 'en_US',
  button_identifier: '1.1'
}

export default FacebookLogin;
