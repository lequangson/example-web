import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import cookie from 'react-cookie'
import { browserHistory } from 'react-router'
import $ from 'jquery';
import Raven from 'raven-js';
import uuid from 'uuid';
import FacebookButton from './facebook';
import {
  facebookLoginRequest,
  facebookLogin,
  facebookLoginFailure,
  facebookLoginFailurePrivateMode,
  ymeetmeLogin,
  ymeetmeLoginWithGoogle,
  ymeetmeLoginFailure,
} from '../Action';
import * as ymmStorage from 'utils/ymmStorage';
import { subscribeUser } from 'src/scripts/GoogleCloudMessage.js';
import {
  getCurrentUserRequest,
  getCurrentUser,
  getLuckySpinnerGift
} from 'actions/userProfile';
import { getDictionariesRequest, getDictionaries } from 'pages/dictionaries/Action';
import { getCampaign } from 'pages/campaign/Action';
import { AUTH_NEW_USER_REGISTRATION, AUTH_USER_LOGIN } from 'constants/auth';
import {
  GA_ACTION_LOGIN,
  GA_ACTION_REGISTRATION,
  GA_ACTION_LOGIN_FAILURE,
  API_EXECUTION_CODE_SUCCESS,
  MODAL_PROMOTION,
  CDN_URL
} from 'constants/Enviroment';
import {
  REFERRAL_ID, CLIENT_UUID,
} from 'constants/auth';
import {
  gaSend, modalShowNext, disable3rdPartyCookies
} from 'actions/Enviroment';

import socket from "utils/socket";
import { ACTIVE_USER } from 'constants/userProfile';
import { getUserIdFromIdentifier } from 'utils/common';

class FacebookLogin extends Component {
    static propTypes = {
        icon_position: PropTypes.string,
        className: PropTypes.string,
        cssClass: PropTypes.string,
    };
    static defaultProps = {
      icon_position: 'left'
    }

    constructor(props) {
        super(props);
    }

    render() {
      const { icon_position } = this.props;
      const cssClass = this.props.cssClass || null;
      const  isQuizPage = this.props.isQuizPage || false;
      if (isQuizPage) {
        return <FacebookButton
          appId={process.env.FACEBOOK_APP_ID}
          callback={() => {}}
          cssClass={`quiz__cover-image--button rounded-5 btn txt-center btn--second`}
          button_identifier = 'Facebook 2.2'
          {...this.props}
        >
          Xem kết quả
        </FacebookButton>
      }
      if (icon_position == 'left') {
        return <FacebookButton
              appId={process.env.FACEBOOK_APP_ID}
              callback={() => {}}
              cssClass={cssClass ? cssClass : 'lp-btn lp-btn--primary mbmd  lp-btn--body'}
              button_identifier = 'Facebook 2.2'
              {...this.props}
            >
              <div className="l-flex l-flex--ac">
                <div className="icon-bg pos-relative">
                  <img src={`${CDN_URL}/general/LandingPageNew/Facebook-white.png`} alt="Đăng nhập bằng facebook"/>
                </div>
                <div className="txt-left margin-l20">
                  <span className="txt-h4">Đăng nhập bằng Facebook</span>
                </div>
              </div>
            </FacebookButton>
      }

      return <FacebookButton
        appId={process.env.FACEBOOK_APP_ID}
        callback={() => {}}
        cssClass={cssClass ? cssClass : 'lp-btn lp-btn--primary lp-header__btn'}
        button_identifier='Facebook 1.1'
        {...this.props}
      >
        Đăng nhập bằng
        <div className="icon-bg margin-l10"><i className="fa fa-facebook"></i></div>
      </FacebookButton>
    }
}

function queryString() {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to queryString!
  const query_string = {}
  const query = window.location.search.substring(1)
  const vars = query.split('&')
  for (let i = 0; i < vars.length; i += 1) {
    const pair = vars[i].split('=')

    // If first entry with this name
    if (typeof query_string[pair[0]] === 'undefined') {
      query_string[pair[0]] = decodeURIComponent(pair[1])
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === 'string') {
      const arr = [query_string[pair[0]], decodeURIComponent(pair[1])]
      query_string[pair[0]] = arr
      // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]))
    }
  }
  return query_string
}

function mapStateToProps(state) {
  const urlParam = queryString()
  // write adsrc into locaStorage
  if (typeof urlParam._adsrc !== 'undefined') {
    ymmStorage.setItem('adsrc', urlParam._adsrc)
  }
  if (typeof urlParam.referralid !== 'undefined') {
    const referralId = getUserIdFromIdentifier(urlParam.referralid);
    ymmStorage.setItem(REFERRAL_ID, referralId)
  }
  if (!ymmStorage.getItem(CLIENT_UUID)) {
    const clientId = uuid.v1();
    ymmStorage.setItem(CLIENT_UUID, clientId);
  }
  return {
    auth: state.auth,
    Enviroment: state.Enviroment
  };
}

function mapDispachToProps(dispatch) {
  return {
    disable3rdPartyCookies: (disabled) => {
      dispatch(disable3rdPartyCookies(disabled))
    },
    facebookLogin: async (facebookResponse, buttonIdentifier) => {
      dispatch(facebookLoginRequest());
      if (typeof facebookResponse.accessToken !== 'undefined') {
        // remove notification message if any
        if ($('.notification-error').length) {
          $('.notification-error').hide()
        }
        if ($('.notification-info').length) {
          $('.notification-info').hide()
        }
        if (!facebookResponse.error) {
          try {
            dispatch(facebookLogin(facebookResponse)).then((response) => {
            // write api_key to sessionStorage
            try {
              ymmStorage.setItem('api_key', response.payload.data.token)
              ymmStorage.setItem('fb_token', facebookResponse.accessToken)
            } catch (e) {
              // show warning message here
              const ga_track_data = {
                extra_information: 'Browser in private mode'
              }
              dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_track_data))
              dispatch(facebookLoginFailurePrivateMode())
              return
            }
            !response.error ? dispatch(ymeetmeLogin(response.payload)).then((ymm_response) => {
              if (!ymm_response.error) {
                if (typeof ymm_response.response.token !== 'undefined') {
                  // Save current user token
                  ymmStorage.setItem('ymm_token', ymm_response.response.token)
                  // get current user
                  dispatch(getCurrentUserRequest())
                  dispatch(getCurrentUser()).then((currentUserResponse) => {
                                          // register socket room by user identifier
                    if (currentUserResponse.response.data.user_status == ACTIVE_USER) {
                      socket.emit('join', {room: currentUserResponse.response.data.identifier});
                    }

                    if (currentUserResponse.response.execution_result.execution_code === API_EXECUTION_CODE_SUCCESS) {
                      const registerSuccess = ymm_response.response.execution_code === AUTH_NEW_USER_REGISTRATION
                      // Send google analytic event
                      const gaAction = registerSuccess
                        ? GA_ACTION_REGISTRATION
                        : GA_ACTION_LOGIN

                      const ga_track_data = {
                        page_source: buttonIdentifier,
                      }
                      Raven.setUserContext({
                          email: currentUserResponse.response.data.email,
                          id: currentUserResponse.response.data.identifier,
                      });
                      dispatch(gaSend(gaAction, ga_track_data))
                      dispatch(getDictionariesRequest())
                      dispatch(getDictionaries())
                      // Remove advertisment source
                      ymmStorage.removeItem('adsrc')
                      // Remove REFERRAL_ID
                      ymmStorage.removeItem(REFERRAL_ID);
                      subscribeUser(dispatch, currentUserResponse.response.data.allow_ask_push_notification);
                      const previous_url = ymmStorage.getItem('previous_url');
                      if (previous_url) {
                        ymmStorage.removeItem('previous_url')
                        //window.location.href = previous_url
                        browserHistory.push(previous_url)
                        return
                      }
                      const isCompletedWelcome = currentUserResponse.response.data.welcome_page_completion > 3
                      const is_have_avatar = currentUserResponse.response.data.user_pictures.filter(picture => picture.is_main)
                      const hasDoneEditProfile = Boolean(is_have_avatar.length !== 0 && currentUserResponse.response.data.self_introduction)
                      const loginSuccess = ymm_response.response.execution_code === AUTH_USER_LOGIN;
                      // const number_lucky_spinner_free = campaignResponse.response.data.length ? campaignResponse.response.data[0].number_lucky_spinner_free : 0;
                      if (registerSuccess) {
                        browserHistory.push('/myprofile')
                      } else if (loginSuccess) {
                        dispatch(getLuckySpinnerGift());
                        if (hasDoneEditProfile) {
                          // case finished all - normal case
                          browserHistory.push('/whathot')
                          return;
                        }
                        browserHistory.push('/myprofile')
                      }
                    }

                  }).catch(() => {
                    const ga_track_data = {
                      extra_information: 'Get current user failure'
                    }
                    dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_track_data))
                  })
                }
              } else {
                dispatch(ymeetmeLoginFailure(ymm_response))
              }
            }) : dispatch(facebookLoginFailure(response.payload))
          })
        } catch (e) {
          const ga_track_data = {
            extra_information: 'Exception :' + e
          }
          dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_track_data))
          // alert(`error ${e}`)
        }
      }
      else {
        // when user already logout facebook
        const ga_track_data = {
          extra_information: 'Login Facebook fail: error'
        }
        dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_track_data))
        dispatch(facebookLoginFailure())
      }
      } else {
        browserHistory.push('/');
        const ga_track_data = {
          extra_information: 'Login Facebook fail - not authenticated'
        }
        dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_track_data))
        dispatch(facebookLoginFailure())
      }
    },
  };
}

export default connect(mapStateToProps, mapDispachToProps)(FacebookLogin);
