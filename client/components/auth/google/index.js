import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import cookie from 'react-cookie'
import { browserHistory } from 'react-router'
import $ from 'jquery';
import Raven from 'raven-js';
import uuid from 'uuid';
import GoogleLoginButton from './GoogleLogin';
import {
  googleLoginFailurePrivateMode,
  googleLoginRequest,
  googleLogin,
  googleLoginFailure,
  ymeetmeLogin,
  ymeetmeLoginWithGoogle,
  ymeetmeLoginFailure,
} from '../Action';
import * as ymmStorage from 'utils/ymmStorage';
import { subscribeUser } from 'src/scripts/GoogleCloudMessage.js';
import {
  getCurrentUserRequest,
  getCurrentUser,
  getLuckySpinnerGift
} from 'actions/userProfile';
import { getDictionariesRequest, getDictionaries } from 'pages/dictionaries/Action';
import { getCampaign } from 'pages/campaign/Action';
import { AUTH_NEW_USER_REGISTRATION, AUTH_USER_LOGIN } from 'constants/auth';
import {
  GA_ACTION_LOGIN,
  GA_ACTION_REGISTRATION,
  GA_ACTION_LOGIN_FAILURE,
  API_EXECUTION_CODE_SUCCESS,
  CDN_URL
} from 'constants/Enviroment';
import {
  REFERRAL_ID, CLIENT_UUID,
} from 'constants/auth';
import {
  gaSend, modalShowNext, disable3rdPartyCookies
} from 'actions/Enviroment';
import socket from "utils/socket";
import { getUserIdFromIdentifier } from 'utils/common';
import { ACTIVE_USER } from 'constants/userProfile';

class GoogleLogin extends Component {
    static propTypes = {
        icon_position: PropTypes.string,
        className: PropTypes.string,
    };
    static defaultProps = {
      icon_position: 'left'
    }
    constructor(props) {
        super(props);
    }
    render() {
        const { icon_position } = this.props;
        const cssClass = this.props.cssClass || null;
        if (icon_position == 'left') {
            return (
              <GoogleLoginButton
                appId={process.env.FACEBOOK_APP_ID}
                callback={() => {}}
                cssClass={cssClass ? cssClass : 'lp-btn lp-btn--google lp-btn--body mbmd'}
                button_identifier='Google 1.1'
                {...this.props}
              >
                <div className="l-flex l-flex--ac">
                <div className="icon-bg">
                  <img src={`${CDN_URL}/general/LandingPageNew/Google.png`} alt="Đăng nhập bằng google" />
                </div>
                <div className="txt-left margin-l20"><span className="txt-h4">Đăng nhập bằng Google</span></div>
              </div>
              </GoogleLoginButton>
            );
        }
        return (
          <GoogleLoginButton
            appId={process.env.FACEBOOK_APP_ID}
            callback={() => {}}
            cssClass={cssClass ? cssClass : 'lp-btn lp-btn--google lp-header__btn margin-l10'}
            button_identifier='Google 1.1'
            {...this.props}
          >
            Đăng nhập bằng
            <div className="icon-bg margin-l10"><i className="fa fa-google-plus"></i></div>
          </GoogleLoginButton>
        );
    }
}

function queryString() {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to queryString!
  const query_string = {}
  const query = window.location.search.substring(1)
  const vars = query.split('&')
  for (let i = 0; i < vars.length; i += 1) {
    const pair = vars[i].split('=')

    // If first entry with this name
    if (typeof query_string[pair[0]] === 'undefined') {
      query_string[pair[0]] = decodeURIComponent(pair[1])
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === 'string') {
      const arr = [query_string[pair[0]], decodeURIComponent(pair[1])]
      query_string[pair[0]] = arr
      // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]))
    }
  }
  return query_string
}
function mapStateToProps(state) {
  const urlParam = queryString()
  // write adsrc into locaStorage
  if (typeof urlParam._adsrc !== 'undefined') {
    ymmStorage.setItem('adsrc', urlParam._adsrc)
  }
  if (typeof urlParam.referralid !== 'undefined') {
    const referralId = getUserIdFromIdentifier(urlParam.referralid);
    ymmStorage.setItem(REFERRAL_ID, referralId)
  }
  if (!ymmStorage.getItem(CLIENT_UUID)) {
    const clientId = uuid.v1();
    ymmStorage.setItem(CLIENT_UUID, clientId);
  }
  return {
    auth: state.auth,
    Enviroment: state.Enviroment
  };
}
function mapDispachToProps(dispatch) {
  return {
    disable3rdPartyCookies: (disabled) => {
      dispatch(disable3rdPartyCookies(disabled))
    },
    googleLogin: (googleResponse, buttonIdentifier) => {
      //dispatch(facebookLoginRequest());

      if ($('.notification-error').length) {
        $('.notification-error').hide()
      }
      if ($('.notification-info').length) {
        $('.notification-info').hide()
      }

      const resourceName = googleResponse.result ? googleResponse.result.resourceName : null;

      if (resourceName == null) {
        const ga_track_data = {
          extra_information: 'Login Google fail'
        }
        dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_track_data))
        dispatch(googleLoginFailure())
        return
      }
      try {
        dispatch(googleLogin(googleResponse)).then((response) => {

          if (response.error) {
            // get api key failed
            dispatch(googleLoginFailure(response.payload))
            return
          }
          try {
            ymmStorage.setItem('api_key', response.payload.data.token)
            ymmStorage.setItem('google_token', gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token)
          } catch (e) {
            // show warning message here
            const ga_track_data = {
              extra_information: 'Browser in private mode'
            }
            dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_track_data))
            dispatch(googleLoginFailurePrivateMode())
            return
          }
          dispatch(ymeetmeLoginWithGoogle(response.payload)).then((ymm_response) => {
            if (ymm_response.error) {
              dispatch(ymeetmeLoginFailure(ymm_response))
              return;
            }

            if (typeof ymm_response.response.token === 'undefined') {
              return;
            }

            // Save current user token
            ymmStorage.setItem('ymm_token', ymm_response.response.token)
            // get current user
            dispatch(getCurrentUserRequest())
            dispatch(getCurrentUser()).then((currentUserResponse) => {

               // register socket room by user identifier
                if (currentUserResponse.response.data.user_status == ACTIVE_USER) {
                  socket.emit('join', {room: currentUserResponse.response.data.identifier});
                }

                if (currentUserResponse.response.execution_result.execution_code === API_EXECUTION_CODE_SUCCESS) {
                  const registerSuccess = ymm_response.response.execution_code === AUTH_NEW_USER_REGISTRATION
                  // Send google analytic event
                  const gaAction = registerSuccess
                    ? GA_ACTION_REGISTRATION
                    : GA_ACTION_LOGIN

                  const ga_track_data = {
                    page_source: buttonIdentifier,
                  }
                  Raven.setUserContext({
                      email: currentUserResponse.response.data.email,
                      id: currentUserResponse.response.data.identifier,
                  });
                  dispatch(gaSend(gaAction, ga_track_data))
                  // get latest dictionaries
                  dispatch(getDictionariesRequest())
                  dispatch(getDictionaries())
                  // Remove advertisment source
                  ymmStorage.removeItem('adsrc')
                  // Remove REFERRAL_ID
                  ymmStorage.removeItem(REFERRAL_ID);
                  subscribeUser(dispatch, currentUserResponse.response.data.allow_ask_push_notification);
                  const previous_url = ymmStorage.getItem('previous_url');
                  if (previous_url) {
                    ymmStorage.removeItem('previous_url')
                    //window.location.href = previous_url
                    browserHistory.push(previous_url)
                    return
                  }
                  const isCompletedWelcome = currentUserResponse.response.data.welcome_page_completion > 3
                  const is_have_avatar = currentUserResponse.response.data.user_pictures.filter(picture => picture.is_main)
                  const hasDoneEditProfile = Boolean(is_have_avatar.length !== 0 && currentUserResponse.response.data.self_introduction)
                  const loginSuccess = ymm_response.response.execution_code === AUTH_USER_LOGIN;
                  // const number_lucky_spinner_free = campaignResponse.response.data.length ? campaignResponse.response.data[0].number_lucky_spinner_free : 0;
                  if (registerSuccess) {
                    browserHistory.push('/myprofile')
                  } else if (loginSuccess) {
                    dispatch(getLuckySpinnerGift());
                    if (hasDoneEditProfile) {
                        // case finished all - normal case
                        browserHistory.push('/whathot')
                        return;
                    } else {
                      //case not completed welcome
                      browserHistory.push('/myprofile')
                    }
                  } else {
                    const ga_track_data = {
                      extra_information: 'Error: Not login nor register'
                    }
                    dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_track_data))
                  }
                }
            })
          })
        })
      } catch (e) {
        const ga_track_data = {
          extra_information: 'Exception :' + e
        }
        dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_track_data))
      }
    },
  };
}

export default connect(mapStateToProps, mapDispachToProps)(GoogleLogin);