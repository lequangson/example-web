import React, { PropTypes } from 'react';
import { sendClickableNotification } from '../../../utils/errors/notification'
import { NOTIFICATION_WARNING } from '../../../constants/Enviroment'
import { GOOGLE_LOGIN_DISABLE_3RD_PARTY_COOKIES_MESSAGE } from '../../../constants/TextDefinition'
import MobileDetect from 'mobile-detect';
var googleApiAdapterSingleton = null;

class GoogleApiAdapterSingleton {
  constructor(gapi) {
    this.gapi = gapi;
    this.gapi.load('client', this.loadOauthClient.bind(this));
    this.initialized = false;
    this.disabledThirdPartyCookie = false;
  }

  loadOauthClient() {
    if (this.gapi) {
      this.gapi.load('client:auth2', this.initOauthClient.bind(this));
    }
  };

  initOauthClient() {
    //var clientId = '74459069650-vnjsebpm1la0f23510i3m9ugf0jtifdp.apps.googleusercontent.com';
    var clientId = process.env.GOOGLE_API_CLIENT_ID;
    const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/people/v1/rest"];
    const SCOPES = "https://www.googleapis.com/auth/contacts.readonly";
    const adap = this;

    if (!this.gapi) {
      return;
    }
    
    this.gapi.client.init({
      discoveryDocs: DISCOVERY_DOCS,
      clientId: clientId,
      scope: SCOPES
    }).then(function () {
      adap.initialized = true;
      //this.onSignedIn(this.isSignedIn());
    },
    function(err) {
      if (err.error === 'idpiframe_initialization_failed'){
        sendClickableNotification(NOTIFICATION_WARNING, '', GOOGLE_LOGIN_DISABLE_3RD_PARTY_COOKIES_MESSAGE, '/helps/7/1', 15000)
        adap.disabledThirdPartyCookie = true;
console.log('InitGoogleError', err)
      }
    });
  }

  isInitialized() {
    return this.initialized;
  }

  isDisabledThirdPartyCookie() {
    return this.disabledThirdPartyCookie;
  }

  isSignedIn() {
    if (!this.gapi) {
      return false;
    }
    return this.gapi.auth2.getAuthInstance().isSignedIn.get();
  }

  signIn(cb, errorCb) {
    if (!this.gapi) {
      return;
    }

    const md = new MobileDetect(window.navigator.userAgent);
    let option = {
      ux_mode: 'redirect',
      redirect_uri: `${window.location.origin}/?loginstate=googledirect`
    };
    /*if (md.mobile() != null) {
      // When access from mobile, login with redirect. YM-2216
      option = {
        ux_mode: 'redirect',
        redirect_uri: `${window.location.origin}/?loginstate=googledirect`
      }
    }*/

    const pr = this.gapi.auth2.getAuthInstance().signIn(option);
    pr.then(function(response) {
      if (cb) {
        cb(response);
      }
    }, function(err) {
      if (errorCb) {
        errorCb(err);
      }
    });

  }

  getMe(cb) {
    if (!this.gapi) {
      return;
    }
    this.gapi.client.people.people.get({
       'resourceName': 'people/me'
     }).then(function(response) {
       cb(response);
     });
  }
}

class GoogleApiAdapter {

  constructor(gapi) {
    if (googleApiAdapterSingleton === null) {
      googleApiAdapterSingleton = new GoogleApiAdapterSingleton(gapi);
    }
  }

  isInitialized() {
    return googleApiAdapterSingleton.isInitialized();
  }

  isDisabledThirdPartyCookie() {
    return googleApiAdapterSingleton.isDisabledThirdPartyCookie();
  }

  isSignedIn() {
    return googleApiAdapterSingleton.isSignedIn();
  }

  signIn(cb, errorCb) {
    googleApiAdapterSingleton.signIn(cb, errorCb);
  }

  getMe(cb) {
    googleApiAdapterSingleton.getMe(cb);
  }
}



class GoogleLogin extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
      GoogleApiLoaded: false // prevent user click before Google API loaded
    }
    this.gapiAdapter = new GoogleApiAdapter(gapi);
    this.checkGoogleApiLoaded = this.checkGoogleApiLoaded.bind(this);
  }

  componentWillMount() {
  }

  componentDidMount() {
    setTimeout(this.checkGoogleApiLoaded, 1000);
  }

  checkGoogleApiLoaded() {
    const initialized = this.gapiAdapter.isInitialized();
    const disabled = this.gapiAdapter.isDisabledThirdPartyCookie();

    if (initialized) {
      this.setState({ GoogleApiLoaded: true })

      // redirect from google oauth page.
      if (window.location.search.includes('googledirect')) {
        this.loadProfile();
      }

    } else {
      setTimeout(this.checkGoogleApiLoaded, 1000);
    }

    if (disabled !== this.props.Enviroment.disable3rdPartyCookies) {
      this.props.disable3rdPartyCookies(disabled)
    }
  }

  loadProfile() {
    if (this.state.clicked) {
      return
    }

    const component = this;
    if (component.gapiAdapter.isSignedIn()) {
      component.setState({clicked: true})
      component.gapiAdapter.getMe((response) => {
        component.setState({clicked: false})
        component.props.googleLogin(response, component.props.button_identifier);
      });
    } else {
      component.setState({clicked: true})
      component.gapiAdapter.signIn(
        function(response) {
          component.gapiAdapter.getMe((response) => {
            component.setState({clicked: false})
            component.props.googleLogin(response, component.props.button_identifier);
          });
        },
        function(err) {
          component.setState({clicked: false})
          component.props.googleLogin({ status: err, 'props': component.props }, component.props.button_identifier);
        },
      );
    }
  };

  renderWithFontAwesome() {
    const { cssClass } = this.props;
    return (
      <button
        className={cssClass}
        onClick={this.loadProfile.bind(this)}
        disabled={!this.state.GoogleApiLoaded}
      >
        {this.props.children}
      </button>
    );
  }

  render() {
    return this.renderWithFontAwesome();
  }
}

GoogleLogin.propTypes = {
  callback: PropTypes.func.isRequired,
  autoLoad: PropTypes.bool,
  cssClass: PropTypes.string,
  button_identifier: PropTypes.string,
}

GoogleLogin.defaultProps = {
  cssClass: 'kep-login-facebook',
  button_identifier: 'Google 1.1'
}

export default GoogleLogin;

