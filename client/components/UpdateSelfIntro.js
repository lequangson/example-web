import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import Modal from 'react-modal'
import _ from 'lodash'
import $ from 'jquery'
import { MAXIMUM_SELF_INTRO_TEXT_SIZE } from '../constants/userProfile'
import { REGEX_MESSAGE } from '../constants/Enviroment'
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class UpdateSelfIntro extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
      track: 0,
      introTemplates: this.props.dictionaries.self_introduction_templates,
      numberCharacterLeft: MAXIMUM_SELF_INTRO_TEXT_SIZE + 1,
      isFormValid: false,
      modalType: 'modalOne',
    }

    this.closeModal = this.closeModal.bind(this)
    this.next = this.next.bind(this)
    this.prev = this.prev.bind(this)
    this.applySelfIntro = this.applySelfIntro.bind(this)
    this.showSelfIntro = this.showSelfIntro.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
    this.selfIntroductionChange = this.selfIntroductionChange.bind(this)
    this.openModal = this.openModal.bind(this)
    this.isFormValid = this.isFormValid.bind(this)
  }

  componentWillMount() {
    this.loadRecommendSelfIntroduction()
  }

  @GoogleOptimize
  componentDidMount() {
    this.selfIntroductionChange()
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.modalType === 'modalTwo' && 
      this.props.user_profile.self_introduction.length === 0
      && nextProps.user_profile.self_introduction.length > 0){
      this.setState({
        introTemplates: nextProps.user_profile.self_introduction,
      })
    }
  }

  componentDidUpdate() {
    if (this.refs.textareaModal) {
      this.showSelfIntro()
    }
  }

  getCharaterLeft() {
    if (!this.self_introduction) {
      return 0
    }
    return this.self_introduction.value.length > MAXIMUM_SELF_INTRO_TEXT_SIZE
      ? 0
      : MAXIMUM_SELF_INTRO_TEXT_SIZE - this.self_introduction.value.length
  }

  openModal(modal) {
    if (modal === 'modalOne') {
      this.setState({
        modalIsOpen: true,
        introTemplates: this.props.dictionaries.self_introduction_templates,
        track: 0,
        modalType: modal,
      })
    } else {
      this.setState({
        modalIsOpen: true,
        introTemplates: this.props.user_profile.self_introduction,
        track: 0,
        modalType: modal,
      })
    }
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  loadRecommendSelfIntroduction() {
    const selfIntro = this.props.user_profile.self_introduction
    if (!selfIntro || (selfIntro.length === 0)) {
      this.props.getSelfIntroduction()
    }
  }

  next() {
    $('textarea').scrollTop(0)
    if (this.state.track >= (this.state.introTemplates.length - 1)) return
    this.setState({
      track: this.state.track + 1,
    })
  }

  prev() {
    $('textarea').scrollTop(0)
    if (this.state.track <= 0) return
    this.setState({
      track: this.state.track - 1,
    })
  }

  showSelfIntro() {
    this.refs.textareaModal.value = 
      this.state.introTemplates.length > this.state.track? 
      this.state.introTemplates[this.state.track] : ''
  }

  applySelfIntro() {
    this.self_introduction.value = this.refs.textareaModal.value
    this.selfIntroductionChange()
    this.closeModal()
  }

  handleUpdate() {
    const self_introduction = this.self_introduction.value
    this.props.updateUserProfile({ 'personal[self_introduction]': self_introduction }).then(() => {
      browserHistory.push('/myprofile')
    })
  }

  isFormValid() {
    const state = this.state
    return state.isMessageValid
  }

  selfIntroductionChange() {
    const self_introduction = this.self_introduction ? this.self_introduction.value : ''
    if (self_introduction.replace(/(\r\n|\n| |\r)/gm, '').match(REGEX_MESSAGE)) {
      this.setState({ isMessageValid: true, numberCharacterLeft: this.getCharaterLeft() })
    } else {
      this.setState({ isMessageValid: false, numberCharacterLeft: this.getCharaterLeft() })
    }
  }

  resetCharaterLeft() {
    this.setState({ numberCharacterLeft: MAXIMUM_SELF_INTRO_TEXT_SIZE + 1 })
  }

  render() {
    const { user_profile } = this.props
    const { current_user } = user_profile
    if (_.isEmpty(current_user)) {
      return (
        <div className="sticky-content">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <h2 className="txt-heading">Giới thiệu bản thân</h2>
              </div>
            </div>
          </div>
        </div>
      )
    }
    const { self_introduction } = current_user
    const defaultText = `20 ~ ${MAXIMUM_SELF_INTRO_TEXT_SIZE.toString()} kí tự`
    return (
      <div>
        <div className="sticky-content">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <h2 className="txt-heading">Giới thiệu bản thân</h2>
                <button
                  className="btn btn--b mbm"
                  onClick={() => {
                    this.openModal('modalOne')
                  }}
                >
                  Sử dụng mặc định của Ymeet.me
                </button>
                <button
                  className="btn btn--b mbl"
                  onClick={() => {
                    this.openModal('modalTwo')
                  }}
                >
                  Tham khảo từ người dùng khác
                </button>
                <textarea
                  ref={(ref) => {
                    this.self_introduction = ref
                  }}
                  id="self_introduction"
                  className="mbs"
                  rows="10"
                  placeholder={defaultText}
                  defaultValue={self_introduction}
                  onChange={this.selfIntroductionChange}
                ></textarea>
                {this.isFormValid() || <span className="txt-sm txt-light txt-red">Từ 20 đến 1024 ký tự</span>}
                <div className="pull-right mbs txt-light txt-italic">
                  Còn lại {this.getCharaterLeft()} kí tự
                </div>
              </div>
              <div className="col-xs-12">
                <div className="col-xs-6">
                  <Link to="/myprofile">
                    <button className="btn btn--b">Hủy</button>
                  </Link>
                </div>
                <div className="col-xs-6">
                  <button
                    className="btn btn--p btn--b"
                    disabled={this.isFormValid() ? '' : 'disabled'}
                    onClick={this.handleUpdate}
                  >Cập nhật</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="modal__content"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="panel">
            <div className="panel__heading">
              <h2 className="panel__title txt-center">Sử dụng mặc định</h2>
            </div>
            <div className="panel__body">
              <div className="mbm l-flex-ce">
                <button className="btn" onClick={this.prev}>
                  <i className="fa fa-chevron-left"></i> Trước
                </button>
                <span className="txt-bold txt-light">Mẫu {this.state.track + 1}</span>
                <button className="btn" onClick={this.next}>
                  Tiếp <i className="fa fa-chevron-right"></i>
                </button>
              </div>

              <textarea
                ref="textareaModal"
                className="mbm"
                rows="10"
                defaultValue={this.state.introTemplates.length > this.state.track? this.state.introTemplates[this.state.track] : ''}
              ></textarea>

              <button className="btn btn--b btn--p" onClick={this.applySelfIntro}>Sử dụng</button>
            </div>
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}

UpdateSelfIntro.propTypes = {
  dictionaries: PropTypes.object.isRequired,
  user_profile: PropTypes.object.isRequired,
  getSelfIntroduction: PropTypes.func.isRequired,
  updateUserProfile: PropTypes.func.isRequired,
}

export default UpdateSelfIntro
