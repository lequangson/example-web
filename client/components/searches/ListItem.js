import React, { PropTypes } from 'react'
import { Link } from 'react-router'
//import picturefill from 'picturefill/dist/picturefill'
import { ProgressiveImage } from '../commons/ProgressiveImage'
import LikeButton from '../commons/LikeButton'
import StartChatButton from '../commons/StartChatButton'
import showOnlineStatus from '../../src/scripts/showStatus'
import { dateTimeToString } from '../../utils/common'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import { defaultAvatarObject } from '../../src/scripts/showDefaultAvatar'
import { BLOCK_TYPE_BLOCKED } from '../../constants/userProfile'
import {
  CDN_URL, MATCHED_PAGE
} from '../../constants/Enviroment'
import { isUserInBoostInRankPeriod } from '../../utils/BoostInRankService'
import { MATCHED_TYPE_SUPER } from '../../constants/Like';
import * as UserDevice from '../../utils/UserDevice';

class ListItem extends React.Component {

  renderVisitedTime(visited_time) {
    const showVisitedTime = this.props.showVisitedTime
    if (!visited_time) {
      return '';
    }
    if (!showVisitedTime) {
      return '';
    }
    return <div className="card__meta card__meta--1 txt-sm">{ dateTimeToString(visited_time)}</div>
  }

  renderNewUserIcon() {
    const user = this.props.data;
    const isNewUser = user ? user.is_new_user : false;
    if (!isNewUser) {
      return ''
    }
    return (
      <div>
        <div className="card__addon-2">
          <div className="card__addon-2__title">Mới!</div>
        </div>
        <div className="card__pad-2"></div>
      </div>
    )
  }

  renderHeartIcon() {
    const activeHeart = this.props.activeHeart;
    const heartActive = this.props.heartActive;
    const className = heartActive ? 'card__top-right card__top-right--heart txt-red' : 'card__top-right card__top-right--heart txt-white';
    if (!activeHeart) {
      return ''
    }
    return (
      <div className={className}>
        <i className="fa fa-heart"></i>
      </div>
    )
  }

  renderLockIcon() {
    const { activeLock, pageSource } = this.props
    const user = this.props.data;
    const isVerifiedUser = user ? user.verification_status.score > 0 : false;
    const className = isVerifiedUser ? 'card__top-right card__top-right--4' : 'card__top-right card__top-right--3';
    if (activeLock || pageSource !== MATCHED_PAGE) {
      return ''
    }
    return (
      <div className={className}>
        <i className="fa fa-lock icon-small"></i>
      </div>
    )
  }

  renderRandomMatchIcon() {
    const user = this.props.data;
    const isVerifiedUser = user? user.verification_status.score > 0 : false;
    const className = isVerifiedUser ? 'l-flex-center card__top-right card__top-right--bg2' : 'l-flex-center card__top-right card__top-right--bg1';
    if (!user.is_random_matched) {
      return ''
    }
    return (
      <div className={className}>
        <img src={`${CDN_URL}/24h_seal.png`} alt="24h_seal.png"  className="img-normal" />
      </div>
    )
  }

  renderSuperLikeIcon() {
    const activeSuperLike = this.props.activeSuperLike;
    const user = this.props.data;
    const isVerifiedUser = user ? user.verification_status.score > 0 : false;
    const className = isVerifiedUser ? 'l-flex-center card__top-right card__top-right--bg2' : 'l-flex-center card__top-right card__top-right--bg1';
    if (!activeSuperLike || user.match_type != MATCHED_TYPE_SUPER) {
      return ''
    }
    return (
      <div className={className}>
        <img src={`${CDN_URL}/bow-white.png`} alt="bow-white.png" />
      </div>
    )
  }

  renderVerifiedUserIcon() {
    const user = this.props.data;
    const score = user.verification_status ? user.verification_status.score : 0;
    let url,
        verifyAlt = ''
    if(score === undefined || score == 0) {
      return ''
    }

    if(score <= 30) {
      url = `${CDN_URL}/general/Verify/Bronze.png`
      verifyAlt = 'verify-bronze'
    }else if(score <= 70 && score > 30) {
      url = `${CDN_URL}/general/Verify/Silver.png`
      verifyAlt = 'verify-silver'
    }else {
      url = `${CDN_URL}/general/Verify/Gold.png`
      verifyAlt = 'verify-gold'
    }

    return (
      <div className="card__verify card__verify--1">
        <img className="img-round img-verify--list" src={url} alt={verifyAlt} />
      </div>
    )
  }

  renderBootInRankIcon() {
    const user = this.props.data;
    const isVerifiedUser = user? user.verification_status.score > 0 : false;
    const className = isVerifiedUser ? 'card__top-right card__top-right--2' : 'card__top-right card__top-right--1';
    const now = new Date()
    let isOntop_24h = isUserInBoostInRankPeriod(user);

    if (!isOntop_24h) {
      return ''
    }
    return (
      <div className={className}>
        <i className="fa fa-rocket icon-small"></i>
      </div>
    )
  }

  render() {
    const user = this.props.data
    const userPicture = user.user_pictures.length
      ? user.user_pictures[0]
      : defaultAvatarObject(user.gender, 'large')
    const { pageSource } = this.props
    const userStatus = user.user_status
    const isBlockedUser = user.block_me_type
      ? user.block_me_type === BLOCK_TYPE_BLOCKED
      : false;
    const isUserActive = ((userStatus === 1) || (userStatus === null)) && !isBlockedUser
    const jobCategory = user.job_category.value ? user.job_category.value.trim() : ''
    const distance = typeof(user.distance) !== 'undefined' ? user.distance < 1 ? 1 : Math.round(user.distance) : 0

    return (
      <div
        className="col-xs-6 col-sm-6 col-md-6 card-list element_height"
        key={user.identifier}
        id={`list-view-${this.props.index}`}
        data-index={this.props.index}
      >
        <article className="card mbm" key={user.identifier}>
          <div className="card__upper">
            {isUserActive
              ? <Link to={`/profile/${user.identifier}`} className="card__link">
                <ProgressiveImage
                  preview={userPicture.small_image_url}
                  src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                  render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                />
                <div className="card__addon">
                  <i className="fa fa-heart"></i>
                  <div className="txt-bold">{user.compatibility}%</div>
                </div>
                <div className="card__pad"></div>
                <div className="card__meta card__meta--1">
                  <div>
                    <span className={`dot dot--${showOnlineStatus(user.online_status)} mrs`}></span>
                    <span className="txt-bold txt-lg">{user.nick_name} - </span>
                    <span className="txt-bold txt-lg">{user.age ? user.age.toString() : '?'}</span>
                  </div>
                  <div className="txt-color-primary">
                    { distance > 0 ?
                      distance + ' km'
                      :
                      (
                        typeof user.residence !== 'undefined' &&
                        typeof user.residence.value !== 'undefined' &&
                        user.residence.value
                      ) ? user.residence.value : 'Không rõ'
                    }
                  </div>
                </div>
                {this.renderNewUserIcon()}
                {this.renderVisitedTime(user.visited_time)}
              </Link>
              : <div className="card__link">
                <img
                  src={showDefaultAvatar(user.gender, 'thumb')}
                  className="card__img"
                  alt={user.nick_name}
                />
                {this.renderVisitedTime(user.visited_time)}
                <div className="card__status">Người dùng đã khóa tài khoản</div>
              </div>
            }
          </div>
          {this.renderVerifiedUserIcon()}
          {this.renderBootInRankIcon()}
          {this.renderHeartIcon()}
          {this.renderLockIcon()}
          {this.renderSuperLikeIcon()}
          {this.renderRandomMatchIcon()}
          <div className="card__under">

            <div className="card__text card__text--2">
              <div className="txt-light txt-ellipsis">Nghề nghiệp</div>
              <div className="txt-blue txt-ellipsis">{jobCategory || 'Không rõ'}</div>
              <div className="txt-blue txt-ellipsis">{user.job_position || ''}</div>
            </div>
            { isUserActive
              ?
              <div>
                <LikeButton
                  user={user}
                  page_source={pageSource ? pageSource : ''}
                  forceLike={false}
                  msg=''
                />
                <StartChatButton user={user} page_source={this.props.pageSource} shortText />
              </div>
              :
              <div>
                <LikeButton
                  user={user}
                  page_source={pageSource ? pageSource : ''}
                  forceLike={false}
                  msg=''
                  isUserActive={isUserActive}
                />
                <StartChatButton user={user} page_source={this.props.pageSource} shortText isUserActive={isUserActive} />
              </div>
            }
          </div>
        </article>
      </div>
    )
  }
}

ListItem.propTypes = {
  index: PropTypes.number,
  pageSource: PropTypes.string,
  data: PropTypes.object,
  showVisitedTime: PropTypes.bool,
}

export default ListItem
