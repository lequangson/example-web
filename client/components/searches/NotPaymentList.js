import React, { PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import UnlockWhoLikeMeModal from '../modals/UnlockWhoLikeMeModal'
import PaymentModal from '../modals/PaymentModal'
import { defaultAvatarObject } from '../../src/scripts/showDefaultAvatar'
import { WHO_ADD_ME_TO_FAVOURITE_PAGE, PEOPLE_LIKED_ME_PAGE } from '../../constants/Enviroment'

class NotPaymentList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isModalOpen: false,
    }
    this.showModal = this.showModal.bind(this)
    this.hideModal = this.hideModal.bind(this)
  }

  showModal() {
    const { onDeletePage } = this.props
    if (onDeletePage) {
      browserHistory.push('/matched-list')
    }
    else {
      this.setState({ isModalOpen: true })
    }
  }

  hideModal() {
    this.setState({ isModalOpen: false })
  }

  render() {
    const { onDeletePage } = this.props
    const user = this.props.data ? this.props.data : []
    const index = user.length > 3 ? 3 : user.length - 1
    const lastUser = user.length ? user[user.length-1] : []
    let text = ''
    if (user.length === 1) {
      text = user[0].nick_name
    }
    else if (user.length === 2) {
      text = user[0].nick_name + ', ' + user[1].nick_name
    }
    else {
      text = user[0].nick_name + ', ' + user[1].nick_name + ' và ' + (user.length - 2) + ' người khác'
    }
    // const userPicture = user.user_pictures.length
    // ? user.user_pictures[0]
    // : defaultAvatarObject(user.gender, 'large')

    return (
      <div>
        <div className="col-xs-12">
        { onDeletePage
          ?
          <p className="txt-lg txt-bold txt-blue">{text} sẽ rất nhớ bạn đấy!</p>
          :
          this.props.page === WHO_ADD_ME_TO_FAVOURITE_PAGE ?
          <p className="txt-light txt-bg">{user.length} người đã quan tâm đến bạn</p>
          :
          <p className="txt-light txt-bg">{user.length} người khác cũng thích bạn</p>
        }
        </div>
        <div style={{cursor:'pointer'}} className="col-xs-12 mbl">
          <div className="row" onClick={this.showModal}>
            {
              user.slice(0, index).map((user) => (
                <div className="col-xs-3" key={user.identifier}>
                  <article className="card mbl mc">
                      <img
                        srcSet={user ? user.user_pictures.length ? user.user_pictures[0].thumb_image_url : defaultAvatarObject(user.gender, 'large').thumb_image_url : ''}
                        alt={user.nick_name}
                      />
                  </article>
                </div>
              ))
            }
            {
              <div className="col-xs-3">
                <article className="card mbl mc" key={user.identifier}>
                  <div className="card__link">
                    <img
                      srcSet={lastUser.user_pictures ? lastUser.user_pictures.length ? lastUser.user_pictures[0].thumb_image_url : defaultAvatarObject(lastUser.gender, 'large').thumb_image_url : defaultAvatarObject(lastUser.gender, 'large').thumb_image_url}
                      alt={lastUser.nick_name}
                    />
                    { user.length > 4 &&
                      <div className="card__add" >+</div>
                    }

                  </div>
                </article>
              </div>
            }
            {
              this.props.page === PEOPLE_LIKED_ME_PAGE ?
              <UnlockWhoLikeMeModal
                current_user={this.props.current_user}
                isOpen={this.state.isModalOpen}
                onRequestClose={this.hideModal}
              />
              :
              <PaymentModal
                current_user={this.props.current_user}
                isOpen={this.state.isModalOpen}
                onRequestClose={this.hideModal}
                page={this.props.page}
              />
            }
          </div>
        </div>
      </div>
    )
  }
}

NotPaymentList.propTypes = {
  index: PropTypes.number,
  pageSource: PropTypes.string,
  data: PropTypes.array,
}

export default NotPaymentList
