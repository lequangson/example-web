import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import $ from 'jquery';
//import picturefill from 'picturefill/dist/picturefill'
import { ProgressiveImage } from '../commons/ProgressiveImage'
import LikeButton from '../commons/LikeButton'
import InvisibleLikeButton from '../commons/InvisibleLikeButton'
import StartChatButton from '../commons/StartChatButton'
import showOnlineStatus from '../../src/scripts/showStatus'
import { dateTimeToString, getUserIdFromIdentifier } from '../../utils/common'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import { defaultAvatarObject } from '../../src/scripts/showDefaultAvatar'
import { PEOPLE_LIKED_ME_PAGE , DEFAULT_LOAD_MALE, DEFAULT_LOAD_FEMALE , RECOMMENDATION_PAGE, WHAT_HOT_PAGE, CDN_URL, LIKE_WITH_MESSAGE_ICON } from '../../constants/Enviroment'
import { NOT_AVAIL } from '../../constants/TextDefinition'
import {
  BLOCK_TYPE_BLOCKED,
} from '../../constants/userProfile'
import { isUserInBoostInRankPeriod } from '../../utils/BoostInRankService'
import * as UserDevice from '../../utils/UserDevice';

class DetailItem extends React.Component {
  constructor() {
    super()
    this.state = {
      isSendingLike: false,
    }
  }

  static defaultProps = {
    showMetaInfoUser: true
  }

  componentWillUpdate(nextProps) {
    if (nextProps.data != this.props.data) {
      this.setState({isSendingLike: false})
    }
  }

  markSendingLike(isSendingLike) {
    this.setState({ isSendingLike })
  }

  renderVisitedTime(visited_time) {
    const showVisitedTime = this.props.showVisitedTime
    if (!visited_time) {
      return '';
    }
    if (!showVisitedTime) {
      return '';
    }
    return <div className="txt-sm txt-italic">{ dateTimeToString(visited_time) }</div>
  }

  renderNewUserIcon(){
    const user = this.props.data;
    const isNewUser = user? user.is_new_user : false;
    if (!isNewUser){
      return ''
    }
    return (
      <div>
        <div className="card__addon-2">
          <div className="card__addon-2__title">Mới!</div>
        </div>
        <div className="card__pad-2"></div>
      </div>
    )
  }

  renderVerifiedUserIcon() {
    const user = this.props.data;
    const score = user.verification_status ? user.verification_status.score : 0;
    let url, verifyAlt = ''
    if(score === undefined || score == 0) {
      return ''
    }

    if(score <= 30) {
      url = `${CDN_URL}/general/Verify/Bronze.png`
      verifyAlt = 'verify-bronze'
    }else if(score <= 70 && score > 31) {
      url = `${CDN_URL}/general/Verify/Silver.png`
      verifyAlt = 'verify-silver'
    }else {
      url = `${CDN_URL}/general/Verify/Gold.png`
      verifyAlt = 'verify-gold'
    }

    return (
      <div className="card__verify card__verify--2">
        <img className="img-round img-verify--list" src={url} alt={verifyAlt} />
      </div>
    )
  }

  renderBootInRankIcon() {
    const user = this.props.data;
    const isVerifiedUser = user? user.verification_status.score > 0 : false;
    const className = isVerifiedUser ? 'card__top-right card__top-right--2 large' : 'card__top-right card__top-right--1 large';
    const now = new Date()
    let isOntop_24h = isUserInBoostInRankPeriod(user);

    if (!isOntop_24h) {
      return ''
    }
    return (
      <div className={className}>
        <i className="fa fa-rocket icon-small"></i>
      </div>
    )
  }

  renderLikeMessage() {
    const likeMsg = this.props.data.like_message
    if (likeMsg) {
      return (
        <div className="card__text-2">
          <img className='img-like' src={LIKE_WITH_MESSAGE_ICON} alt="tym-icon" />
          {likeMsg}
        </div>
      )
    }
    return '';
  }

  renderTopCard() {
    if(this.props.renderTopCard && !this.props.data.is_recommendation) {
      return (
        <div className="card__top"></div>
      )
    }
    return '';
  }

  renderRecommendationBanner = () => {
    if(this.props.data.is_recommendation) {
      return (
        <img src={`${CDN_URL}/general/Banner/recommendation-header-${this.props.data.gender}.png`} alt="banner" className="leson"/>
      )
    }
    return '';
  }

  cardIngore() {
    var $card = $(".site-content").find('.card.front');

    var swipe = new TimelineMax({repeat:0, yoyo:false, repeatDelay:0, onComplete: this.remove(), onCompleteParams:[$card]});
    swipe.staggerTo($card, 0.8, {bezier:[{left:"+=-350", top:"+=300", rotation:"-60"}], ease:Power1.easeInOut});
  }

  remove(card) {
    $(card).remove();
  }

  renderNextUserButton() {
    const user = this.props.data
    if (this.props.pageSource != RECOMMENDATION_PAGE && this.props.pageSource != WHAT_HOT_PAGE) {
      return ''
    }
    const divClassName = this.props.pageSource == WHAT_HOT_PAGE? 'col-xs-6 txt-center' : 'col-xs-6';
    // const buttonClassName = this.props.pageSource == WHAT_HOT_PAGE? "btn btn--5 txt-xxlg btn--round" : "btn btn--b txt-dark"

    return (
      <div className={divClassName}>
        <button
          className="btn btn--b txt-dark"
          onClick={
            () => {
              this.cardIngore();
              setTimeout(() => {
                this.props.ignoreUser(getUserIdFromIdentifier(user.identifier), this.props.pageSource);
                this.props.showNextUser();
              }, 1000)
            }
          }
        >
          {/*this.props.pageSource == WHAT_HOT_PAGE && <i className="fa fa-times"></i>*/}
          Bỏ qua
        </button>
      </div>
    )
  }

  renderBackCard() {
    const user = this.props.data
    const nextUser = this.props.dataNext
    let shortNextUserIntro = ''
    if (!_.isEmpty(nextUser) && nextUser.self_introduction != null) {
      shortNextUserIntro = (nextUser.self_introduction.trim().length > 85)
      ? `${nextUser.self_introduction.trim().substring(0, 85)}...`
      : nextUser.self_introduction.trim()
    }

    const isMatchMade = user.have_sent_like && user.have_sent_like_to_me ? true : false
    let nextUserPicture = ''
    const { pageSource } = this.props
    let isUserActive = true
    let distance = 0
    let jobDescription = ''

    if(_.isEmpty(nextUser) === false) {
      nextUserPicture = nextUser.user_pictures.length
      ? nextUser.user_pictures[0]
      : defaultAvatarObject(nextUser.gender, 'large')
      const isBlockedUser = nextUser.block_me_type ? nextUser.block_me_type === BLOCK_TYPE_BLOCKED : false;
      isUserActive =  ((nextUser.user_status === 1) || (nextUser.user_status === null)) && !isBlockedUser
      distance = typeof(nextUser.distance) !== 'undefined' ? nextUser.distance < 1 ? 1 : Math.round(nextUser.distance) : 0
      jobDescription = nextUser.job_category.value || ''
    }

    const invisibleLike = pageSource == PEOPLE_LIKED_ME_PAGE ? true : false
    const forceLike = this.props.forceLike;
    return (
      <article className="card mbm card__back element_height no-hover">
        <div className="card__upper">
          {isUserActive
          ? <Link to={""} style={_.isEmpty(nextUser) ? {visibility: 'hidden'} : {}} className="card__link">
              <ProgressiveImage
                preview={nextUserPicture.small_image_url}
                src={ UserDevice.isHeighResolution()? nextUserPicture.extra_large_image_url : nextUserPicture.large_image_url}
                render={(src, style) => <img src={src} style={style} alt={_.isEmpty(nextUser) ? "" : nextUser.nick_name} />}
              />
              <div className="card__addon">
                <i className="fa fa-heart"></i>
                <div className="txt-bold">{_.isEmpty(nextUser)? 0 : nextUser.compatibility}%</div>
              </div>
              <div className="card__pad"></div>
              {this.renderNewUserIcon()}
              <div className="card__meta card__meta--1">
                <span className={`dot dot--${showOnlineStatus(_.isEmpty(nextUser)? undefined : nextUser.online_status)} mrs`}></span>
                <span className="txt-bold txt-lg">
                  {`${_.isEmpty(nextUser) ? "" : nextUser.nick_name} - `}
                  {user.age
                    ? user.age.toString()
                    : '?'
                  }
                  {user.residence.value
                    ? ` - ${user.residence.value}`
                    : ''
                  }
                </span>
                <div>
                  <i className="fa fa-image"></i> {user.user_pictures.length} ảnh
                </div>
                {
                  distance > 0 &&
                  <div className="txt-color-primary">
                    {distance + ' km'}
                  </div>
                }
                {this.renderVisitedTime(user.visited_time)}
              </div>
              {this.renderLikeMessage()}
            </Link>
          : <div className="card__link" style={{visibility: 'hidden'}}>
              <img
                src={showDefaultAvatar(nextUser.gender, 'large')}
                className="card__img"
                alt={`${nextUser.nick_name}`}
              />
              <div className="card__meta card__meta--1">
                <span className="txt-bold txt-lg">{`${nextUser.nick_name} - `}</span>
                <span className="txt-bold txt-lg">
                  {nextUser.age
                    ? nextUser.age.toString()
                    : '?'
                  } tuổi
                </span>
                <span className="txt-lg">
                  {nextUser.residence.value
                    ? ` - ${nextUser.residence.value}`
                    : ''
                  }
                </span>
                <div>
                  <i className="fa fa-image"></i> {nextUser.user_pictures.length} ảnh
                </div>
                {this.renderVisitedTime(nextUser.visited_time)}
              </div>
              <div className="card__status">Người dùng đã khóa tài khoản</div>
            </div>
          }
          {_.isEmpty(nextUser) ?
            <div className="txt-center txt-light txt-lg msg-last">
              Bạn đã đến cuối danh sách!
            </div>
            : ""
          }
        </div>
        {this.renderVerifiedUserIcon()}
        {this.renderBootInRankIcon()}
        <div className="card__under">
          {this.props.showMetaInfoUser && <div>
            <div className="mbs card__text">{shortNextUserIntro}</div>
            <div className="l-card-info mbs">
              <div className="l-card-info__item">
                <div className="txt-light txt-ellipsis">
                  <span style={{
                    display: 'inline-block',
                    width: '6rem',
                  }}>Nghề nghiệp</span>
                  <span className="txt-blue txt-ellipsis">{jobDescription || NOT_AVAIL}</span>
                </div>
                <div className="txt-blue txt-ellipsis">
                  <span style={{ display: 'inline-block', width: '6rem' }}></span>
                  <span className="txt-blue txt-ellipsis">{user.job_position || ''}</span>
                </div>
                <span className="txt-light" style={{ display: 'inline-block', width: '6rem' }}>Chiều cao</span>
                <span className="txt-blue">{user.height}{user.height ? 'cm' : NOT_AVAIL}</span>
              </div>
              <div className="l-card-info__item">
                <div className="txt-light">Mong muốn kết hôn</div>
                <div className="txt-blue txt-ellipsis">{user.intention_marriage.value ? user.intention_marriage.value : NOT_AVAIL}</div>
                <div className="txt-ellipsis">
                  <span className="txt-light" style={{ display: 'inline-block', width: '6rem' }}>Tính cách</span>
                  <span className="txt-blue">{user.personality_types[0] ? user.personality_types[0].value : NOT_AVAIL}</span>
                </div>
              </div>
            </div>
          </div> }
          <div className="row">
            {this.renderNextUserButton()}
            {invisibleLike &&
              <div className={isUserActive ? 'col-xs-6' : 'col-xs-12'}>
                <InvisibleLikeButton user={user} />
              </div>}
            { isUserActive && parseInt(user.block_type, 10) !== BLOCK_TYPE_BLOCKED
              ?
              <div className={((invisibleLike && !isMatchMade) || this.props.tinderListUsers ) ? 'col-xs-6 txt-center' : 'col-xs-12'}>
                <LikeButton
                user={user}
                forceLike={forceLike}
                page_source= {pageSource ? pageSource : ''}
                msg=''
                isSendingLike={this.state.isSendingLike}
                markSendingLike={v => this.markSendingLike(v)}
                />
                <StartChatButton user={user} shortText={false} page_source={pageSource ? pageSource : ''} />
              </div>
              : ''
            }
          </div>
        </div>
        {this.renderTopCard()}
      </article>
    )

  }

  renderFrontCard() {
    const user = this.props.data
    const userPicture = user.user_pictures.length
    ? user.user_pictures[0]
    : defaultAvatarObject(user.gender, 'large')
    let shortSelfIntro = ''
    if (user.self_introduction) {
      shortSelfIntro = (user.self_introduction.trim().length > 85)
      ? `${user.self_introduction.trim().substring(0, 85)}...`
      : user.self_introduction.trim()
    }
    const { pageSource } = this.props
    const cardClassName = this.props.tinderListUsers ? "card mbm element_height no-hover" : "card mbm element_height"
    const isBlockedUser = user.block_me_type ? user.block_me_type === BLOCK_TYPE_BLOCKED : false;
    const isUserActive = ((user.user_status === 1) || (user.user_status === null)) && !isBlockedUser
    const distance = typeof(user.distance) !== 'undefined' ? user.distance < 1 ? 1 : Math.round(user.distance) : 0
    let jobDescription = user.job_category.value || ''
    const invisibleLike = pageSource == PEOPLE_LIKED_ME_PAGE ? true : false
    const forceLike = this.props.forceLike;
    const isMatchMade = user.have_sent_like && user.have_sent_like_to_me ? true : false

    return (
      <article className={`${cardClassName} z-index-1 front`}>
        <div className="card__upper">
          {isUserActive
          ? <Link to={`/profile/${user.identifier}`} className="card__link">
              <ProgressiveImage
                preview={userPicture.small_image_url}
                src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
              />
              <div className="card__addon">
                <i className="fa fa-heart"></i>
                <div className="txt-bold">{user.compatibility}%</div>
              </div>
              <div className="card__pad"></div>
              {this.renderNewUserIcon()}
              <div className="card__meta card__meta--1">
                <span className={`dot dot--${showOnlineStatus(user.online_status)} mrs`}></span>
                <span className="txt-bold txt-lg">
                  {`${user.nick_name} - `}
                  {user.age
                    ? user.age.toString()
                    : '?'
                  }
                  {user.residence.value
                    ? ` - ${user.residence.value}`
                    : ''
                  }
                </span>
                <div>
                  <i className="fa fa-image"></i> {user.user_pictures.length} ảnh
                </div>
                {
                  distance > 0 &&
                  <div className="txt-color-primary">
                    {distance + ' km'}
                  </div>
                }
                {this.renderVisitedTime(user.visited_time)}
              </div>
              {this.renderLikeMessage()}
            </Link>
          : <div className="card__link">
              <img
                src={showDefaultAvatar(user.gender, 'large')}
                className="card__img"
                alt={`${user.nick_name}`}
              />
              <div className="card__meta card__meta--1">
                <span className="txt-bold txt-lg">{`${user.nick_name} - `}</span>
                <span className="txt-bold txt-lg">
                  {user.age
                    ? user.age.toString()
                    : '?'
                  } tuổi
                </span>
                <span className="txt-lg">
                  {user.residence.value
                    ? ` - ${user.residence.value}`
                    : ''
                  }
                </span>
                <div>
                  <i className="fa fa-image"></i> {user.user_pictures.length} ảnh
                </div>
                {this.renderVisitedTime(user.visited_time)}
              </div>
              <div className="card__status">Người dùng đã khóa tài khoản</div>
            </div>
          }

        </div>
        {this.renderVerifiedUserIcon()}
        {this.renderBootInRankIcon()}
        <div className="card__under">
          {this.props.showMetaInfoUser && <div>
            <div className="mbs card__text">{shortSelfIntro}</div>
            <div className="l-card-info mbs">
              <div className="l-card-info__item">
                <div className="txt-light txt-ellipsis">
                  <span style={{
                    display: 'inline-block',
                    width: '6rem',
                  }}>Nghề nghiệp</span>
                  <span className="txt-blue txt-ellipsis">{jobDescription || NOT_AVAIL}</span>
                </div>
                <div className="txt-blue txt-ellipsis">
                  <span style={{ display: 'inline-block', width: '6rem' }}></span>
                  <span className="txt-blue txt-ellipsis">{user.job_position || ''}</span>
                </div>
                <span className="txt-light" style={{ display: 'inline-block', width: '6rem' }}>Chiều cao</span>
                <span className="txt-blue">{user.height}{user.height ? 'cm' : NOT_AVAIL}</span>
              </div>
              <div className="l-card-info__item">
                <div className="txt-light">Mong muốn kết hôn</div>
                <div className="txt-blue txt-ellipsis">{user.intention_marriage.value ? user.intention_marriage.value : NOT_AVAIL}</div>
                <div className="txt-ellipsis">
                  <span className="txt-light" style={{ display: 'inline-block', width: '6rem' }}>Tính cách</span>
                  <span className="txt-blue">{user.personality_types[0] ? user.personality_types[0].value : NOT_AVAIL}</span>
                </div>
              </div>
            </div>
          </div>}
          <div className="row">
            {this.renderNextUserButton()}
            {invisibleLike &&
              <div className={isUserActive ? 'col-xs-6' : 'col-xs-12'}>
                <InvisibleLikeButton user={user} />
              </div>}
            { isUserActive && parseInt(user.block_type, 10) !== BLOCK_TYPE_BLOCKED
              ?
              <div className={((invisibleLike && !isMatchMade) || this.props.tinderListUsers ) ? 'col-xs-6 txt-center' : 'col-xs-12'}>
                <LikeButton
                user={user}
                forceLike={forceLike}
                page_source= {pageSource ? pageSource : ''}
                msg=''
                isSendingLike={this.state.isSendingLike}
                markSendingLike={v => this.markSendingLike(v)}
                />
                <StartChatButton user={user} shortText={false} page_source={pageSource ? pageSource : ''} />
              </div>
              : ''
            }
          </div>
        </div>
        {this.renderTopCard()}
      </article>
    )
  }

  render() {
    const user = this.props.data

    if (_.isEmpty(user) || typeof user.identifier === 'undefined') {
      return <div>Loading...</div>
    }
    const { pageSource } = this.props
    const invisibleLike = pageSource == PEOPLE_LIKED_ME_PAGE ? true : false
    let shortSelfIntro = ''
    if (user.self_introduction) {
      shortSelfIntro = (user.self_introduction.trim().length > 85)
      ? `${user.self_introduction.trim().substring(0, 85)}...`
      : user.self_introduction.trim()
    }

    const userPicture = user.user_pictures.length
    ? user.user_pictures[0]
    : defaultAvatarObject(user.gender, 'large')

    let jobDescription = user.job_category.value || ''
    const paddingTop = this.props.cardNotPaddingTop === false ? 'padding-t10' : '';
    const isMatchMade = user.have_sent_like && user.have_sent_like_to_me ? true : false
    const userStatus = user.user_status
    const isBlockedUser = user.block_me_type ? user.block_me_type === BLOCK_TYPE_BLOCKED : false;
    const isUserActive = ((userStatus === 1) || (userStatus === null)) && !isBlockedUser
    const distance = typeof(user.distance) !== 'undefined' ? user.distance < 1 ? 1 : Math.round(user.distance) : 0
    const cardDetailClassName = this.props.tinderListUsers ? `col-xs-12 col-sm-12 col-md-8 col-md-offset-2 ${paddingTop}` : "col-sm-12 col-md-4 card-detail"
    const cardClassName = this.props.tinderListUsers ? "card mbm element_height no-hover" : "card mbm element_height"
    const forceLike = this.props.forceLike;

    return (
      <div
        className={cardDetailClassName}
        key={user.identifier}
        id={`detail-view-${this.props.index + 1}`}
        data-index={this.props.index + 1}
      >
        {this.renderRecommendationBanner()}

        {(pageSource === PEOPLE_LIKED_ME_PAGE ||
         pageSource === WHAT_HOT_PAGE ||
         pageSource === RECOMMENDATION_PAGE) ?
        <div className="flipper padding-0">
          {this.renderBackCard()}
          {this.renderFrontCard()}
        </div>
        :
        <article className={cardClassName}>
          <div className="card__upper">
            {isUserActive
            ? <Link to={`/profile/${user.identifier}`} className="card__link">
                <ProgressiveImage
                  preview={userPicture.small_image_url}
                  src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                  render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                />
                <div className="card__addon">
                  <i className="fa fa-heart"></i>
                  <div className="txt-bold">{user.compatibility}%</div>
                </div>
                <div className="card__pad"></div>
                {this.renderNewUserIcon()}
                <div className="card__meta card__meta--1">
                  <span className={`dot dot--${showOnlineStatus(user.online_status)} mrs`}></span>
                  <span className="txt-bold txt-lg">
                    {`${user.nick_name} - `}
                    {user.age
                      ? user.age.toString()
                      : '?'
                    }
                    {user.residence.value
                      ? ` - ${user.residence.value}`
                      : ''
                    }
                  </span>
                  <div>
                    <i className="fa fa-image"></i> {user.user_pictures.length} ảnh
                  </div>
                  {
                    distance > 0 &&
                    <div className="txt-color-primary">
                      {distance + ' km'}
                    </div>
                  }
                  {this.renderVisitedTime(user.visited_time)}
                </div>
                {this.renderLikeMessage()}
              </Link>
            : <div className="card__link">
                <img
                  src={showDefaultAvatar(user.gender, 'large')}
                  className="card__img"
                  alt={`${user.nick_name}`}
                />
                <div className="card__meta card__meta--1">
                  <span className="txt-bold txt-lg">{`${user.nick_name} - `}</span>
                  <span className="txt-bold txt-lg">
                    {user.age
                      ? user.age.toString()
                      : '?'
                    } tuổi
                  </span>
                  <span className="txt-lg">
                    {user.residence.value
                      ? ` - ${user.residence.value}`
                      : ''
                    }
                  </span>
                  <div>
                    <i className="fa fa-image"></i> {user.user_pictures.length} ảnh
                  </div>
                  {this.renderVisitedTime(user.visited_time)}
                </div>
                <div className="card__status">Người dùng đã khóa tài khoản</div>
              </div>
            }

          </div>
          {this.renderVerifiedUserIcon()}
          {this.renderBootInRankIcon()}
          <div className="card__under">
            {this.props.showMetaInfoUser && <div>
              <div className="mbs card__text">{shortSelfIntro}</div>
              <div className="l-card-info mbs">
                <div className="l-card-info__item">
                  <div className="txt-light txt-ellipsis">
                    <span style={{
                      display: 'inline-block',
                      width: '6rem',
                    }}>Nghề nghiệp</span>
                    <span className="txt-blue txt-ellipsis">{jobDescription || NOT_AVAIL}</span>
                  </div>
                  <div className="txt-blue txt-ellipsis">
                    <span style={{ display: 'inline-block', width: '6rem' }}></span>
                    <span className="txt-blue txt-ellipsis">{user.job_position || ''}</span>
                  </div>
                  <span className="txt-light" style={{ display: 'inline-block', width: '6rem' }}>Chiều cao</span>
                  <span className="txt-blue">{user.height}{user.height ? 'cm' : NOT_AVAIL}</span>
                </div>
                <div className="l-card-info__item">
                  <div className="txt-light">Mong muốn kết hôn</div>
                  <div className="txt-blue txt-ellipsis">{user.intention_marriage.value ? user.intention_marriage.value : NOT_AVAIL}</div>
                  <div className="txt-ellipsis">
                    <span className="txt-light" style={{ display: 'inline-block', width: '6rem' }}>Tính cách</span>
                    <span className="txt-blue">{user.personality_types[0] ? user.personality_types[0].value : NOT_AVAIL}</span>
                  </div>
                </div>
              </div>
            </div>}
            <div className="row">
              {this.renderNextUserButton()}
              {invisibleLike &&
                <div className={isUserActive ? 'col-xs-6' : 'col-xs-12'}>
                  <InvisibleLikeButton user={user} />
                </div>}
              { isUserActive && parseInt(user.block_type, 10) !== BLOCK_TYPE_BLOCKED
                ?
                <div className={((invisibleLike && !isMatchMade) || this.props.tinderListUsers ) ? 'col-xs-6 txt-center' : 'col-xs-12'}>
                  <LikeButton
                  user={user}
                  forceLike={forceLike}
                  page_source= {pageSource ? pageSource : ''}
                  msg=''
                  isSendingLike={this.state.isSendingLike}
                  markSendingLike={v => this.markSendingLike(v)}
                  />
                  <StartChatButton user={user} shortText={false} page_source={pageSource ? pageSource : ''} />
                </div>
                : ''
              }
            </div>
          </div>
          {this.renderTopCard()}
        </article>
        }


      </div>
    )
  }
}

DetailItem.propTypes = {
  index: PropTypes.number,
  data: PropTypes.object,
  showVisitedTime: PropTypes.bool,
  showNextUser: PropTypes.func,
  pageSource: PropTypes.string,
  renderTopCard: PropTypes.bool,
  showMetaInfoUser: PropTypes.bool,
}

export default DetailItem
