import React, { PropTypes } from 'react'
import ListItem from './ListItem'

class ListTwoItem extends React.Component {
  render() {
    const users = this.props.users
    const current_user = this.props.current_user
    const index = (this.props.index * 2) + 1
    const { pageSource } = this.props
    return (
      <div>
        { users.map((user, i) =>
          <ListItem key={i} index={index + i} data={user} current_user={current_user} pageSource={pageSource} />)
        }
      </div>
    )
  }
}

ListTwoItem.propTypes = {
  users: PropTypes.array,
  index: PropTypes.number,
}

export default ListTwoItem
