import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import Modal from 'components/modals/Modal'
import picturefill from 'picturefill/dist/picturefill'
import LikeButton from '../commons/LikeButton'
import StartChatButton from '../commons/StartChatButton'
import showOnlineStatus from '../../src/scripts/showStatus'
import { dateTimeToString, getUserIdFromIdentifier } from '../../utils/common'
import showDefaultAvatar from '../../src/scripts/showDefaultAvatar'
import { defaultAvatarObject } from '../../src/scripts/showDefaultAvatar'
import { BLOCK_TYPE_BLOCKED } from '../../constants/userProfile'
import {
  CDN_URL, MATCHED_PAGE
} from '../../constants/Enviroment'
import { isUserInBoostInRankPeriod } from '../../utils/BoostInRankService'
import { MATCHED_TYPE_SUPER } from '../../constants/Like';

class VipChatItem extends React.Component {
  constructor() {
    super()
    this.state = {
      isConfirmModalOpen: false
    }
    this.handleClick = this.handleClick.bind(this)
    this.closeConfirmModal = this.closeConfirmModal.bind(this)
  }

  openConfirmModal() {
    this.setState({ isConfirmModalOpen: true })
  }

  closeConfirmModal() {
    this.setState({ isConfirmModalOpen: false })
  }

  renderSuperLikeIcon() {
    const user = this.props.data;
    if (user.match_type !== MATCHED_TYPE_SUPER) {
      return ''
    }
    return (
        <div className="l-flex-center card__top-right card__top-right--bg1"><img src={`${CDN_URL}/bow-white.png`} alt="bow-white"/></div>
    )
  }

  handleClick(identifier) {
    if (this.props.startChartCallback) {
      this.props.startChartCallback()
    }
    this.sendGoogleAnalytic()
    window.open(`${process.env.CHAT_SERVER_URL}chat/${identifier}`)
  }

  sendGoogleAnalytic() {
    const gaTrackData = {
      page_source: this.props.pageSource
    };
    const eventAction = {
      eventCategory: 'StartChat',
      eventAction: 'Click',
      eventLabel: 'StartChat Click'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
  }

  render() {
    const user = this.props.data
    const isBlockedUser = user.block_me_type
      ? user.block_me_type === BLOCK_TYPE_BLOCKED
      : false;
    const isUserActive = user.user_status === 1 && !isBlockedUser
    const userPicture = user.user_pictures.length
      ? user.user_pictures[0]
      : defaultAvatarObject(user.gender, 'small')
    const last_message = this.props.chat_status.last_messages.find(msg => msg.user_id == getUserIdFromIdentifier(user.identifier))
    const online_status = this.props.chat_status.online_status.find(online => online.user_id == getUserIdFromIdentifier(user.identifier))

    return (
      <li
      data-index={this.props.index}
      className="col-xs-12"
      >
          {
            isUserActive ?
            <div onClick={() => this.handleClick(user.identifier)} className="media cursor" href="#">
              <div className="media__item media__item--1 card no-hover"><img className="bordered radius" src={userPicture.thumb_image_url}/>
                { this.renderSuperLikeIcon() }
              </div>
              <div className="media__item media__item--2">
                <div>
                  <div className="txt-medium txt-lg">{user.nick_name}   <span className={`dot dot--${online_status ? online_status.is_online ? 'active' : '' : ''} mrs`}></span> </div>
                  <div className="txt-italic txt-blue">{last_message ? last_message.last_message : 'Chúc mừng kết đôi. Hãy mở lời trước nhé!'}</div>
                </div>
              </div>
              <div className="media__item">
                { user.new_message_number > 0 &&
                <div className="media__noti">{user.new_message_number}</div>
                }
              </div>
            </div>
            :
            <div className="media" href="#">
              <div className="media__item media__item--1 card no-hover"><img className="bordered radius" src={defaultAvatarObject(user.gender, 'small').thumb_image_url}/>
                { this.renderSuperLikeIcon() }
              </div>
              <div className="media__item media__item--2">
                <div>
                  <div className="txt-medium txt-blue">{user.nick_name}</div>
                  <div className="txt-italic">Người dùng đã khóa tài khoản</div>
                </div>
              </div>
            </div>
          }
          {this.props.isEdit &&
            <div className="media__item--edit">
              <label>
                <input type="checkbox" name="message_ids" value={1} className="checkbox" />
                <div className="fake-checkbox"></div>
              </label>
            </div>
          }
          <Modal
            transitionName="modal"
            isOpen={this.state.isConfirmModalOpen}
            onRequestClose={this.closeConfirmModal}
            className="modal__content modal__content--3"
            overlayClassName="modal__overlay"
            portalClassName="modal"
            contentLabel=""
          >
            <div className="mbs padding-t10 txt-center">
              <div className="mbl txt-lg txt-light">Xóa cuộc trò chuyện<br /> với {user.nick_name}</div>
            </div>
            <div className="row padding-b10 mbl">
              <div className="col-xs-6 txt-right">
                <button className="btn mrm">
                  <i className="fa fa-times"></i>
                  <span className="padding-l5">Hủy</span>
                </button>
              </div>
              <div className="col-xs-6 txt-left">
                <button className="btn btn--blue mrm">
                  <i className="fa fa-trash"></i>
                  <span className="padding-l5">Xóa</span>
                </button>
              </div>
            </div>
            <button className="modal__btn-close" onClick={this.closeConfirmModal}><i className="fa fa-times"></i></button>
          </Modal>
      </li>
    )
  }
}

VipChatItem.propTypes = {
  index: PropTypes.number,
  pageSource: PropTypes.string.isRequired,
  data: PropTypes.object,
  isEdit: PropTypes.bool,
  showVisitedTime: PropTypes.bool,
  startChartCallback: PropTypes.func.isRequired,
  sendGoogleAnalytic: PropTypes.func.isRequired,
}

export default VipChatItem
