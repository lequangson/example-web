import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import q from 'q'
//import picturefill from 'picturefill/dist/picturefill'
import { ProgressiveImage } from '../commons/ProgressiveImage'
import showOnlineStatus from '../../src/scripts/showStatus'
import { getAvatar } from '../../src/scripts/showDefaultAvatar'
import { updateRandomMatch, updateRandomMatchRequest, showButton } from '../../actions/random_match'
import { getPeopleMatchedMe, getCurrentUser, getLuckySpinnerGift } from '../../actions/userProfile'
import { GA_ACTION_RANDOM_MATCH_OPEN_CARD, CDN_URL } from '../../constants/Enviroment';
import StartChatButton from '../commons/StartChatButton'
import { isUserInBoostInRankPeriod } from '../../utils/BoostInRankService'
import * as UserDevice from '../../utils/UserDevice';
import { ACTIVE_USER } from 'constants/userProfile';
import socket from "../../utils/socket";

class RandomMatchItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: true,
      heartActive: false,
      hiddenCard: true,
    }
    this.handleRandomMatch = this.handleRandomMatch.bind(this)
  }

  renderNewUserIcon() {
    const user = this.props.data;
    const isNewUser = user ? user.is_new_user : false;
    if (!isNewUser) {
      return ''
    }
    return (
      <div>
        <div className="card__addon-2">
          <div className="card__addon-2__title">Mới!</div>
        </div>
        <div className="card__pad-2"></div>
      </div>
    )
  }

  addClassButton() {
    const elt = document.querySelectorAll('article .card__center');
    // console.log('elt', elt)
    for (let value of elt) {
      value.classList.add('card__center--1')
    }
  }

  renderHeartIcon() {
    const activeHeart = this.props.activeHeart;
    const heartActive = this.state.heartActive;
    const className = heartActive ? 'card__top-right card__top-right--heart txt-red' : 'card__top-right card__top-right--heart txt-white';
    if (!activeHeart) {
      return ''
    }
    return (
      <div className={className}>
        <i className="fa fa-heart"></i>
      </div>
    )
  }

  renderVerifiedUserIcon() {
    const user = this.props.data;
    const score = user.verification_status ? user.verification_status.score : 0;
    let url, verifyAlt = ''
    if(score === undefined || score == 0) {
      return ''
    }

    if(score <= 30) {
      url = `${CDN_URL}/general/Verify/Bronze.png`
      verifyAlt = 'verify-bronze'
    }else if(score <= 70 && score > 31) {
      url = `${CDN_URL}/general/Verify/Silver.png`
      verifyAlt = 'verify-silver'
    }else {
      url = `${CDN_URL}/general/Verify/Gold.png`
      verifyAlt = 'verify-gold'
    }

    return (
      <div className="card__verify card__verify--2">
        <img className="img-round img-verify" src={url} alt={verifyAlt} />
      </div>
    )
  }

  renderBootInRankIcon() {
    const user = this.props.data;
    const isVerifiedUser = user? user.verification_status : false;
    const className = isVerifiedUser ? 'card__top-right card__top-right--2' : 'card__top-right card__top-right--1';
    const isOntop_24h = isUserInBoostInRankPeriod(user);
    if (!isOntop_24h) {
      return ''
    }
    return (
      <div className={className}>
        <i className="fa fa-check-circle"></i>
      </div>
    )
  }
  isHiddenCard() {
    if (!this.state.hiddenCard) {
      return false
    }
    else if (!this.props.hiddenCard && this.props.random_match.isUpdateRandomMatch) {
      return  false
    }
    return true
  }

  handleRandomMatch(identifier, nick_name, compatibility) {
    const { user_profile, random_match } = this.props;
    const { random_match_free_gift } = this.props.user_profile.lucky_spinner_gifts;
    const current_user  = Object.assign({}, user_profile.current_user, {
      is_random_matched: true,
      compatibility: compatibility,
    });
    this.props.updateRandomMatch(identifier, nick_name).then(() => {
      this.props.getPeopleMatchedMe();
      this.props.getCurrentUser();
      if (current_user.user_status == ACTIVE_USER) {
        socket.emit('random_match', {room: identifier, random_match_info: current_user, to_identifier: identifier});
      }
      if (random_match_free_gift > 0){
        this.props.getLuckySpinnerGift();
      }
    }).catch(e => {
      // console.log('error :', e);
    })
    this.setState({ heartActive: true, hiddenCard: false })
    setTimeout(() => {this.props.updateModalState(false)}, 1500)
    setTimeout(() => {this.addClassButton()}, 2400)
    setTimeout(() => {this.props.showButton(identifier)}, 2500)
    this.props.gaSend(GA_ACTION_RANDOM_MATCH_OPEN_CARD, {})
  }

  render() {
    const user = this.props.data
    const gender = this.props.current_user.gender
    const userPicture = getAvatar(user);
    const {page_source} = this.props
    const userStatus = user.user_status

    return (
      <div
        className="col-xs-6 col-sm-6 col-md-6 card-list element_height"
        key={user.identifier}
        id={`list-view-${this.props.index}`}
        data-index={this.props.index}
      >
        <div className="flipper mbm">
          <article className={this.isHiddenCard() ? gender === 'male' ? "card front wrap-image-female" : "card front wrap-image-male" : "card active front"} >
            {!this.props.isUpdateRandomMatch ?
              <div className="card__upper" onClick={() => this.handleRandomMatch(user.identifier, user.nick_name, user.compatibility)}>
                <div className="card__link">
                </div>
              </div>
              :
              <div className="card__upper">
                <div className="card__link">
                </div>
              </div>
            }
          </article>

          <article className={this.isHiddenCard() ? "card back" : "card back active"}>
            <div className="card__upper">
              <Link to={`/profile/${user.identifier}`} className="card__link">
                <ProgressiveImage
                  preview={userPicture.small_image_url}
                  src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                  render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                />
                <div className="card__addon">
                  <i className="fa fa-heart"></i>
                  <div className="txt-bold">{user.compatibility}%</div>
                </div>
                <div className="card__pad"></div>
                <div className="card__meta card__meta--1">
                  <div>
                    <span className={`dot dot--${showOnlineStatus(user.online_status)} mrs`}></span>
                    <span className="txt-bold txt-lg">{user.nick_name} - </span>
                    <span className="txt-bold txt-lg">{user.age ? user.age.toString() : '?'}</span>
                  </div>
                  <div className="txt-color-primary">
                    {
                      (
                        typeof user.residence !== 'undefined' &&
                        typeof user.residence.value !== 'undefined' &&
                        user.residence.value
                      ) ? user.residence.value : 'Không rõ'
                    }
                  </div>
                </div>
                {this.renderNewUserIcon()}
              </Link>
            </div>
            <div className="card__center">
              <StartChatButton user={user} noBlock={true} page_source={page_source ? page_source : ''}/>
            </div>
            {this.renderVerifiedUserIcon()}
            {this.renderBootInRankIcon()}
            {this.renderHeartIcon()}
          </article>
        </div>
      </div>
    )
  }
}

RandomMatchItem.propTypes = {
  index: PropTypes.number,
  page_source: PropTypes.string.isRequired,
  data: PropTypes.object,
  showVisitedTime: PropTypes.bool,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateRandomMatch: (identifier, nick_name) => {
      var defer = q.defer()
      dispatch(updateRandomMatchRequest(nick_name))
      dispatch(updateRandomMatch(identifier)).then((response) => {
        defer.resolve(response)
      }).catch((error) => {
        defer.reject(error)
      })
      return defer.promise
    },
    getPeopleMatchedMe: () => {
      dispatch(getPeopleMatchedMe())
    },
    showButton: (identifier) => {
      dispatch(showButton(identifier))
    },
    getCurrentUser: () => {
      dispatch(getCurrentUser())
    },
    getLuckySpinnerGift: () => {
      dispatch(getLuckySpinnerGift());
    }
  }
}

const RandomMatchItemContainer = connect(mapStateToProps, mapDispatchToProps)(RandomMatchItem);

export default RandomMatchItemContainer