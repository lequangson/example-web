/* global document: true */
import React, { Component, PropTypes } from 'react'
import $ from 'jquery'
import { Link } from 'react-router'
import ReactTimeout from 'react-timeout'
import moment from 'moment';
import _ from 'lodash';
import {
  CHAT_LIST_PAGE, DEFAULT_LOAD_FEMALE, DEFAULT_LOAD_MALE, CDN_URL,
  GA_ACTION_VIEW_ADMIN_MESSAGE, ACTIVITY_VIEW_ADMIN_MESSAGE, MODAL_REMIND_RANDOM_MATCH
} from '../constants/Enviroment'
import { MATCHED_TYPE_SUPER } from '../constants/Like';
import { THERE_IS_NO_ONE_I_MATCHED_MESSAGE, LOADING_DATA_TEXT_AFTER_4SECS, LOADING_DATA_TEXT } from '../constants/TextDefinition'
import { BLOCK_TYPE_BLOCKED, GHOST_USER } from '../constants/userProfile'
import ListSmallItem from './searches/ListSmallItem'
import VipChatItem from './searches/VipChatItem'
import ListItem from './searches/ListItem'
import IceBreakingModal from './modals/IceBreakingModal'
import ChatGuideModal from './modals/ChatGuideModal'
import * as ymmStorage from '../utils/ymmStorage'
import { getUserIdFromIdentifier } from '../utils/common'
import { subscribeUser } from '../src/scripts/userSubscription'
import RandomMatchButton from './commons/RandomMatchButton'
import BannerDiscount from 'components/commons/BannerDiscount'

class ChatList extends Component {
  static isInVipList(user) {
    return user.match_type === MATCHED_TYPE_SUPER &&
            user.match_created_by !== 0 && user.match_created_by == getUserIdFromIdentifier(user.identifier) &&
            moment.utc(user.match_created_at).add(7,'days').isAfter(moment.utc())
  }

  constructor() {
    super()
    this.state = {
      needShowSuggestUser: true,
      suggest_user: null,
      suggest_user_chat_online_status: false,
      needSkipShow: false,
      suggestedIndex: -1,
      isEdit: false,
      selected_friend_ids: [],
      selected_checkbox: false,
      isMatchTypeQuick: false,
    }
    this.showSuggestUser = this.showSuggestUser.bind(this)
    this.startChartCallback = this.startChartCallback.bind(this)
    this.closeModalCallback = this.closeModalCallback.bind(this)
  }

  componentWillMount() {
    ymmStorage.removeItem('keep')
    this.props.getPeopleMatchedMe()
    this.getChatSuggestedUsers();
    this.props.getLastMessages()
    if (!ymmStorage.getItem('no_need_remind_random_match')) {
      this.props.modalShowNext(MODAL_REMIND_RANDOM_MATCH)
    }
    this.props.setInterval(this.showSuggestUser, 45000);
    this.props.updatePreviousPage(CHAT_LIST_PAGE)

    setTimeout(() => {this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT_AFTER_4SECS)}, 4000)
  }

  componentDidMount() {
    this.setState({
      isEdit: false
    });
    const userCardHeight = $('.element_height').height();
    const segmentIndex = this.props.Enviroment.profile_next_user_index * userCardHeight;
    window.scroll(0, this.props.Enviroment.matched_list_scroll_top + segmentIndex);

    $(".checkbox-all").click(function () {
       $('input:checkbox').not(this).prop('checked', this.checked);
   });
  }

  componentWillUnmount() {
    this.props.updateScrollPossition(CHAT_LIST_PAGE, window.scrollY)
    this.props.setProfileUserNextIndex(0)
    this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT)
  }

  getChatSuggestedUsers() {
    if (this.props.user_profile.chat_suggested.chat_suggested_users.length > 0) {
      return;
    }
    this.props.getChatSuggestedUsers();
  }

  getUserForSuggest() {
    if (this.props.user_profile.chat_suggested.chat_suggested_users.length === 0) {
      return null;
    }
    let userIndex = this.state.suggestedIndex;
    if (this.state.suggestedIndex === -1) {
      userIndex = _.random(0, this.props.user_profile.chat_suggested.chat_suggested_users.length - 1);
      this.setState({suggestedIndex: userIndex})
    }
    return this.props.user_profile.chat_suggested.chat_suggested_users[userIndex];
  }

  showSuggestUser() {
    if (this.state.needShowSuggestUser === false ||
      this.props.user_profile.chat_suggested.numberTimesShown >= 3 ||
      this.state.suggest_user !== null ||
      this.props.user_profile.matched_users.length === 0 ||
      !ymmStorage.getItem('no_need_remind_random_match')) {
      return;
    }
    if (this.state.needSkipShow) {
      this.setState({needSkipShow: false})
      return;
    }
    const user = this.getUserForSuggest();
    if (user == null) {
      return;
    }
    const online_status = this.props.user_profile.chat_status.online_status.find(online => online.user_id == getUserIdFromIdentifier(user.identifier))
    this.setState({
      suggest_user: user,
      suggest_user_chat_online_status:  online_status? online_status.is_online : false
    });
  }

  startChartCallback() {
    const nextIndex = this.state.suggestedIndex >= this.props.user_profile.chat_suggested.chat_suggested_users.length - 1 ?
      0 :
      this.state.suggestedIndex + 1;
    this.setState({
      needShowSuggestUser: false,
      suggest_user: null,
      suggestedIndex: nextIndex,
    })
    this.props.startChat();
  }

  closeModalCallback() {
    const nextIndex = this.state.suggestedIndex >= this.props.user_profile.chat_suggested.chat_suggested_users.length - 1 ?
      0 :
      this.state.suggestedIndex + 1;
    this.setState({
      needSkipShow: true,
      suggest_user: null,
      suggestedIndex: nextIndex,
    });
  }

  openChatSupport(props) {
    this.props.gaSend(GA_ACTION_VIEW_ADMIN_MESSAGE, { page_source: 'New Message' });

    const chat_support_identifier = moment().format('DDMMYY') + process.env.CUSTOMER_SUPPORT_USER_ID + _.random(10, 99);
    // Add to activity audit
    this.props.addActivityAudit(ACTIVITY_VIEW_ADMIN_MESSAGE, chat_support_identifier, null, null);
    subscribeUser(props)
    window.open(`${process.env.CHAT_SERVER_URL}chat/${chat_support_identifier}`);
  }

  renderBr() {
    const br = (<br />)
    const width = window.innerWidth
    if (width > 1200){
      return br
    }
    return
  }

  renderRefundBanner() {
    const {current_user, random_match_users, matched_users} = this.props.user_profile;
    const { permission, gender, user_type } = current_user;
    if (typeof permission === 'undefined') {
      return <div/>;
    }
    const hasMatch = random_match_users.length > 0 || matched_users.length > 0  ? true : false
    const showBanner = hasMatch && gender == 'male' && permission.show_refund_banner && user_type !== 4 ? true : false
    if (!showBanner) {
      return <div/>;
    }

    return (
      <div className="row">
        <div className="col-xs-12">
          <Link
            style={{backgroundImage: `url(${CDN_URL}/general/Refund-banner-background.jpg)`, backgroundSize: '100% 100%', justifyContent: 'flex-end'}}
            className="banner mbm"
            to="/coin-instruction#refund"
          >
            <div className="banner__content banner__content--1 txt-white txt-center">
              <h3 className="banner__heading">{current_user.nick_name}</h3>
              <p className="banner__sub-heading txt-white txt-sm mbs">Ngại gì mà chưa mở chat với người mình thích? {this.renderBr()} Yên tâm! Bạn sẽ nhận lại 100 xu nếu không có hồi âm</p>
              <div className="btn btn--blue txt-xs txt-thin">Tìm hiểu thêm</div>
            </div>
          </Link>
        </div>
      </div>
    )
  }

  clickEditMessages(isActive) {
    this.setState({
      isEdit: isActive
    });
  }


  selectedAll = () => {
    const friend_ids = this.props.user_profile.matched_users.filter(user =>
      user.block_me_type !== BLOCK_TYPE_BLOCKED && user.user_status === 1).map(item => getUserIdFromIdentifier(item.identifier))
    if (!this.state.selected_checkbox) {
      this.setState({ selected_checkbox: true , selected_friend_ids: friend_ids});
    } else {
      this.setState({ selected_checkbox: false, selected_friend_ids: []});
    }
  }

  selectedItem = (item) => {
    if (this.state.selected_friend_ids.length && this.state.selected_friend_ids.indexOf(item) > -1) {
      const newListIds = this.state.selected_friend_ids.filter(x => x != item);
      this.setState({ selected_friend_ids: newListIds });
      // this.setState({selected_friend_ids: this.state.selected_friend_ids.filter(rc => rc != item)});
      this.forceUpdate();
    } else {
      const newListIds = this.state.selected_friend_ids;
      this.setState({ selected_friend_ids: [...newListIds, item] });
      // this.setState({selected_friend_ids: this.state.selected_friend_ids.filter(rc => rc != item)});
      this.forceUpdate();
    }
  }

  clickedDeleteMessage = async () => {
    await this.props.ignoreMessage(this.state.selected_friend_ids.toString());
    this.props.getLastMessages();
    this.setState({ isEdit: false });
  }

  renderTabFilterMessages() {

    const { isMatchTypeQuick } = this.state;

    return (
      <div className="btn-ground cursor">
        <a className={`${isMatchTypeQuick ? '' : 'active'} btn-ground__btn txt-bold txt-light btn-ground__btn--left`}
          onClick={() => {
            this.setState({
              isMatchTypeQuick: false
            })
        }}>
          <i className="fa mrs fa-comments"></i>Kết đôi thường
        </a>
        <a className={`${isMatchTypeQuick ? 'active' : ''} btn-ground__btn txt-bold txt-light btn-ground__btn--right`}
          onClick={() => {
            this.setState({
              isMatchTypeQuick: true
            })
        }}>
          <img className="img-supperlike" src={`${CDN_URL}/super_like/${isMatchTypeQuick ? 'Heart_white.png' : 'Heart_gray.png'}`} alt="Kết đôi nhanh" />Kết đôi nhanh
        </a>
      </div>
    )
  }

  render() {
    const campaign = this.props.campaign.data;
    const showSaleoffBanner = !_.isEmpty(campaign);
    const { user_profile, Enviroment } = this.props;
    const { current_user, matched_users } = user_profile;
    const { gender } = current_user;
    const isPaidUser = current_user && current_user.user_type === 4;

    const general_matched_list = user_profile.matched_users.filter(user => user.block_me_type !== BLOCK_TYPE_BLOCKED && user.user_status === 1)
    const vip_list = general_matched_list.filter(user => ChatList.isInVipList(user))
    const chat_list = general_matched_list.filter(user => vip_list.indexOf(user) < 0)

    const inactive_users = user_profile.matched_users.filter(user => user.block_me_type === BLOCK_TYPE_BLOCKED || user.user_status !== 1)
    const { has_message_from_admin } = user_profile;
    const isLoaded = user_profile.is_page_loading.matched_users
    const loadingText = Enviroment.isLoadingText
    const default_image = gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE
    const chat_status = user_profile.chat_status
    chat_list.forEach(user => {
      user.created_at = chat_status.last_messages.find(msg => msg.user_id == getUserIdFromIdentifier(user.identifier)) ? chat_status.last_messages.find(msg => msg.user_id == getUserIdFromIdentifier(user.identifier)).created_at : user.match_created_at
    })
    chat_list.sort((a, b) => moment.utc(a.created_at).isBefore(moment.utc(b.created_at)));
    return (
      <div className="site-content">
        <ChatGuideModal
          user_profile={this.props.user_profile}
        />
        <div className="container">
          {has_message_from_admin && <div className="row">
            <div className="col-xs-12">
              <a href="#" onClick={() => this.openChatSupport(this.props)} className="banner mbm">
                <div className="frame frame--xsm frame--1">
                  <img src={`${CDN_URL}/general/Mini_Logo_Ymeetme.png`} alt="Admin Support" />
                </div>
                <div className="banner__content">
                  <h3 className="banner__heading">Ymeet.me</h3>
                  <p className="banner__sub-heading">Bạn có tin nhắn mới từ hệ thống</p>
                </div>
                <div className="banner__meta">
                  <div className="banner__noti" id="notification-no">1</div>
                </div>
              </a>
            </div>
          </div>
          }
          {showSaleoffBanner === false && this.renderRefundBanner()}
          {showSaleoffBanner &&
            <BannerDiscount
              {...this.props}
              timeStop={campaign.end_at}
              isPaidUser={isPaidUser}
              url={"/payment/upgrade"}
            />
          }
          {/*
          <div className="edit-messages padding-b10 txt-blue">
            {
              this.state.isEdit ?
              <div className="txt-right cursor">
                <button className="btn mrm" onClick={() => {this.clickEditMessages(false)}}>
                  <span className="padding-l5">Hủy</span>
                </button>
                <button className="btn btn--blue mrm" onClick={this.clickedDeleteMessage} >
                  <i className="fa fa-trash"></i>
                  <span className="padding-l5">Xóa</span>
                </button>
                <label>
                  <input type="checkbox" name="message_ids" value={1} className="checkbox-all" defaultChecked={this.state.selected_checkbox} onClick={this.selectedAll} />
                  <div className="fake-checkbox"></div>
                </label>
              </div>
              :
              <div className="txt-right cursor" onClick={() => {this.clickEditMessages(true)}}>
                <i className="fa fa-edit"></i>
                <span className="padding-l5">Chỉnh sửa</span>
              </div>
            }
          </div>
          */}
          {this.renderTabFilterMessages()}
          <ul className="list-underline">
            {
              isLoaded &&
              vip_list.length > 0 && <div className="row wrap-bg-yellow padding-t10">{
              (vip_list.map((user, i) =>
                <VipChatItem
                  key={i}
                  id={`dtail-view-${i}`}
                  index={i}
                  data={user}
                  activeLock={!user.have_permission}
                  startChartCallback={this.startChartCallback}
                  chat_status={chat_status}
                  pageSource={ CHAT_LIST_PAGE }
                  sendGoogleAnalytic={ this.props.sendGoogleAnalytic }
                  matched_users={this.props.user_profile.matched_users}
                  isEdit={this.state.isEdit}
                />
              ))} </div>
            }
          {
            this.state.isMatchTypeQuick && <div className="row padding-t10">
              <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                <article className="mbl mc">
                  <div className="card__upper">
                    <div href="#" className="card__link mbm">
                      <img src={default_image} alt="" className="card__img"/>
                    </div>
                    <div className="txt-center txt-light txt-lg mbm">
                      <p className="txt-bold">Khi bạn sử dụng tính năng<br />
                      <img src={`${CDN_URL}/super_like/Heart_pink.png`} alt="Heart pink" className="img-supperlike"/>"Chat luôn không cần đợi thích"<br />
                      với thành viên nào, thành viên<br />
                      đó sẽ được hiển thị ở đây, nơi<br />
                      bạn có thể trò truyện.
                      </p>
                    </div>
                  </div>
                </article>
              </div>
            </div>
          }
          <div className="row">
            {
              isLoaded ?
              general_matched_list.length > 0 ?
              (chat_list.concat(inactive_users).map((user, i) =>
                <ListSmallItem
                  key={i}
                  id={`dtail-view-${i}`}
                  index={i}
                  data={user}
                  activeLock={!user.have_permission}
                  startChartCallback={this.startChartCallback}
                  chat_status={chat_status}
                  pageSource={ CHAT_LIST_PAGE }
                  sendGoogleAnalytic={ this.props.sendGoogleAnalytic }
                  matched_users={this.props.user_profile.matched_users}
                  isEdit={this.state.isEdit}
                  selected={this.state.selected_friend_ids.length && this.state.selected_friend_ids.indexOf(getUserIdFromIdentifier(user.identifier)) > -1}
                  selectedItem={this.selectedItem}
                />
              ))
              :
               <div className="row">
                 <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                   <article className="mbl mc">
                     <div className="card__upper">
                       <div href="#" className="card__link mbm">
                         <img src={default_image} alt="" className="card__img"/>
                       </div>
                       <div className="txt-center txt-light txt-lg mbm">
                         <p className="txt-bold">Những người kết đôi thành công và kết đôi ngẫu nhiên với bạn sẽ được hiển thị ở đây, nơi bạn có thể trò chuyện</p>
                         <br />Thử kết đôi ngẫu nhiên và trò chuyện ngay nhé!
                       </div>
                       {
                        current_user.user_status !== GHOST_USER &&
                        <RandomMatchButton className='btn btn--b btn--p' buttonName='Ghép đôi ngẫu nhiên nào' {...this.props} />
                       }
                     </div>
                   </article>
                 </div>
               </div>
                :
                <div className="row">
                  <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                    <article className="mbl mc">
                      <div className="card__upper">
                        <div href="#" className="card__link">
                          <img src={default_image} alt="" className="card__img"/>
                        </div>
                        <div className="card__center loader is-load">
                          <div className="mbs">{loadingText}</div>
                          <i className="fa fa-spinner fa-3x"></i>
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              }
            </div>
            </ul>
          </div>
        {showSaleoffBanner && this.renderRefundBanner()}
        {/*<IceBreakingModal
          modalIsOpen={this.state.needShowSuggestUser}
          user={this.state.suggest_user}
          chat_online_status={this.state.suggest_user_chat_online_status}
          startChartCallback={this.startChartCallback}
          closeModalCallback={this.closeModalCallback}
        />*/}

      </div>
    )
  }
}

ChatList.propTypes = {
  getPeopleMatchedMe: PropTypes.func.isRequired,
  updatePreviousPage: PropTypes.func.isRequired,
  user_profile: PropTypes.object.isRequired,
  Enviroment: PropTypes.object.isRequired,
  updateScrollPossition: PropTypes.func.isRequired,
  setProfileUserNextIndex: PropTypes.func,
  addActivityAudit: PropTypes.func,
  gaSend: PropTypes.func,
  startChat: PropTypes.func.isRequired,
}

export default ReactTimeout(ChatList)
