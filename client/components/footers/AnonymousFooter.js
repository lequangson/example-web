import React from 'react'
import { Link } from 'react-router'
import logo2 from '../../../public/images/logo-2.png'
import facebook from '../../../public/images/square-facebook.svg'
import twitter from '../../../public/images/square-twitter.svg'

const AnonymousFooter = () => (
  <div className="m-container">
    <footer className="m-txt-center">
      <p className="m-txt-blue">&nbsp;</p>
      <ul className="footer-nav">
        <li><Link to="/">Đăng nhập</Link></li>
        <li><Link to="/about">Giới thiệu</Link></li>
        <li><Link to="/terms-of-use">Điều khoản sử dụng</Link></li>
        <li><Link to="/privacy">Bảo mật</Link></li>
        <li><Link to="/helps">Trợ giúp</Link></li>
        <li><Link to="/contact">Liên hệ</Link></li>
      </ul>
      <p>
        <a
          href="https://www.facebook.com/ymeet.me"
          className="m-social-icon"
          rel="noopener noreferrer"
          target="_blank"
        >
          <img src={facebook} width="18" alt="" />
        </a>
        <a
          href="https://twitter.com/Ymeetme"
          className="m-social-icon"
          rel="noopener noreferrer"
          target="_blank"
        >
          <img src={twitter} width="18" alt="" />
        </a>
      </p>
      <p className="m-site-legal">
        Copyright (c) 2016 <a href="http://mmj.vn">Media Max Japan</a> All Rights Reserved.
      </p>
    </footer>
  </div>
)

export default AnonymousFooter
