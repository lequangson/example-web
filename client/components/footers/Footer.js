import React from 'react'
import $ from 'jquery'
import {
  CDN_URL
} from '../../constants/Enviroment'

function smoothScroll(el, options = {}) {
  const defaultSettings = {
    duration: 1000,
  }

  const settings = Object.assign({}, defaultSettings, options)

  el.on('click', (evt) => {
    evt.preventDefault()
    $('html, body').animate({
      scrollTop: 0,
    }, settings.duration)
  })
}

class Footer extends React.Component {
  componentDidMount() {
    const $btnTop = $('#scrollToTop')
    smoothScroll($btnTop, { duration: 500 })
  }

  render() {
    return (
      <footer className="site-footer">
        <div className="well well--a">
          <div className="container">
            <div className="row">
              <div className="col-xs-6 col-xs-offset-3">
                <ul className="list-unstyle">
                  <li>
                    <button className="btn btn--p btn--b mbm">
                      <i className="fa fa-paw"></i> Dấu chân
                    </button>
                  </li>
                  <li>
                    <button className="btn btn--p btn--b mbm">
                      <i className="fa fa-cog"></i> Cài đặt
                    </button>
                  </li>
                  <li>
                    <button className="btn btn--p btn--b">
                      <i className="fa fa-commenting"></i> Trạng thái
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="well__addon">
            <a id="scrollToTop" href="" className="scroll-to-top">
              <i className="fa fa-chevron-up"></i>
              <div>Top</div>
            </a>
          </div>
        </div>
        <div className="well">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <div className="txt-center mbl">
                  <a href="/">
                    <img
                      src={`${CDN_URL}/assets/logo_hover-651621f4fe2a2610751f64ef20d84131.png`}
                      alt="Media Max Japan"
                    />
                  </a>
                </div>
                <ul className="list-unstyle list-inline list-item-center mbm">
                  <li><a href="" className="txt-dark">Về chúng tôi</a></li>
                  <li><a href="" className="txt-dark">Liên hệ</a></li>
                  <li><a href="" className="txt-dark">Chính sách bảo mật</a></li>
                  <li><a href="" className="txt-dark">Iphone App</a></li>
                </ul>
                <p className="txt-center txt-blue">Copyright (c) 2016 MMJ. All Rights Reserved.</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer
