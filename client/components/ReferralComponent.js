import React, { Component, PropTypes } from 'react'
import CopyToClipboard from 'react-copy-to-clipboard';
import MetaTags from 'react-meta-tags';
import { LOADING_DATA_TEXT } from '../constants/TextDefinition'
import { CDN_URL } from '../constants/Enviroment'
import $ from 'jquery'
import ShareButton from 'components/commons/Buttons/ShareButton';

class ReferralComponent extends Component {

  constructor(props) {
    super(props)
    this.state = {
      seeMore: false,
    }
    this.seeMoreFunc = this.seeMoreFunc.bind(this);
    this.copyReferralLinkToClipboard = this.copyReferralLinkToClipboard.bind(this);
    this.sendLinkToEmail = this.sendLinkToEmail.bind(this);
  }

  seeMoreFunc() {
    this.setState({
      seeMore: true,
    });
  }

  copyReferralLinkToClipboard() {
    const resultNoti = $(`
      <div class="noti noti--bg-green mbm txt-bold">
        <div class="noti__body txt-md">
          <div class="noti__header mbs">
            <i class="fa fa-check-circle txt-xxxxlg"></i>
          </div>
          <div class="noti__content">Link đã được \n sao chép</div>
        </div>
      </div>
    `)
    setTimeout(function() {
      $('body').append(resultNoti)
    }, 200)

    setTimeout(function() {
      $(resultNoti).remove()
    }, 7000)
  }

  sendLinkToEmail() {
    const identifier = this.props.user_profile.current_user.identifier;
    const referralUrl = `${process.env.ROOT_URL}?referralid=${identifier}`;
    const email_content = 'Đăng ký ngay để kết nối với hàng ngàn thành viên độc thân. Hẹn hò lâu dài, an toàn, nghiêm túc!';
    const default_mail_url = `mailto:?subject=Tham gia Ymeet.me cùng tôi cho vui nhé!&body=${email_content}%0D%0A${referralUrl}`;
    const gmail_url = `https://mail.google.com/mail/u/0/?view=cm&fs=1&to=&tf=1&su=Tham gia Ymeet.me cùng tôi cho vui nhé!&body=${email_content}%0D%0A${referralUrl}`
    const sendMailUrl = this.props.user_profile.current_user.email.slice(-9) === 'gmail.com' ? gmail_url : default_mail_url
    window.open(sendMailUrl);
  }

  render() {
    const identifier = this.props.user_profile.current_user.identifier;
    const referralUrl = `${process.env.ROOT_URL}?referralid=${identifier}`;
    const displayUrl = process.env.DOMAIN ?
      `${process.env.DOMAIN}?referralid=${identifier}` :
      `ymeet.me?referralid=${identifier}`;

    return (
      <div className="container">
        <MetaTags>
          <meta id="og-image" property="og:image" content={`${CDN_URL}/general/LandingPage/shutterstock_360691904.jpg`} />
        </MetaTags>
        <div className="row">
          <div className="col-xs-12">
            <h3 className="txt-heading">Giới thiệu bạn bè</h3>
          </div>
        </div>

        <div className="row">
          <div className="col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3">
            <img src={`${CDN_URL}/general/Referral/img.jpg`} alt="Referral" className="full-width"/>
          </div>
        </div>

        <div className="txt-center txt-medium">Nhận ngay 25 xu với mỗi người bạn đăng ký <br /> thành công và nhiều ưu đãi khác.</div>
        {!this.state.seeMore &&
          <div className="txt-blue txt-center txt-underline cursor mbl" onClick={this.seeMoreFunc}>
            Xem thêm
          </div>
        }
        {this.state.seeMore &&
          <div className="mbm">
            <p>Có bạn đang ế thì mời tham gia Ymeet.me ngay. Vừa có thêm xu vừa giúp nhau thoát kiếp FA</p>
            <div className="txt-medium padding-t10">Đối với người mời</div>
            <ol>
              <li>Nhận ngay <span className="txt-blue">25 xu</span> với mỗi người bạn đăng ký thành công. Gửi càng nhiều bạn càng có nhiều xu.</li>
              <li>Nhận ngay <span className="txt-blue">65 xu</span> với mỗi người bạn trả phí (mua xu hoặc nâng cấp tài khoản)</li>
            </ol>
            <div className="txt-medium padding-t10">Đối với người được mời</div>
            <ol>
              <li>Nhận ngay <span className="txt-blue">65 xu</span> khi mua bất cứ gói xu nào.</li>
              <li>Nhận ngay <span className="txt-blue">180 xu</span> khi nâng cấp tài khoản.</li>
            </ol>
          </div>
        }

        <div className="clearfix mbl">
          <div className="col-xs-6">
            <ShareButton
                    title={''}
                    shareUrl={referralUrl}
                    facebookShare
                    buttonTitle='Qua Facebook'
                    round={false}
                    size={16}
                    titleClass='margin-l5'
                    buttonClass='l-flex-center btn-ref-facebook'
                    wrapperClass='btn radius-5 btn--blue btn--b'
                  />
          </div>
          <div className="col-xs-6">
            <div onClick={this.sendLinkToEmail} className="btn radius-5 btn--e btn--b">
              <i className="fa fa-envelope-o" aria-hidden="true"></i> Qua Email
            </div>
          </div>
        </div>

        <div className="txt-heading txt-heading--2 bg-dark"><span className="padding-rl10 txt-dark">hoặc copy đường link gửi cho bạn bè</span></div>
        <div className="row mbm">
          <div className="col-xs-8">
            <input disabled type="url" value={displayUrl} placeholder="can not change" className="txt-blue txt-center padding-t10 padding-b10" />
          </div>
          <div className="col-xs-4">
            <CopyToClipboard
              text={referralUrl}
              onCopy={this.copyReferralLinkToClipboard}
            >
              <button className="btn btn--s btn--b btn--has-border radius-5">Sao chép</button>
            </CopyToClipboard>
          </div>
        </div>
      </div>
    )
  }
}

ReferralComponent.propTypes = {}

export default ReferralComponent
