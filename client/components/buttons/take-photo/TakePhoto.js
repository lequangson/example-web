import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CameraYmm from 'components/camera/CameraYmm';

class TakePhoto extends Component {
  static propTypes = {
    customStyle: PropTypes.string,
    title: PropTypes.string.isRequired,
    onTakePhotoSuccess: PropTypes.func.isRequired,
    onSendGaTakePhoto: PropTypes.func,
    gender: PropTypes.string.isRequired,
  };

  static defaultProps  = {
    customStyle: 'btn btn--p btn--b mbm',
    title: 'Tải ảnh lên'
  }

  state = {
    switchToCamera: false
  };

  onClickTakePhotoBtn = () =>{
    this.setState({
      switchToCamera: true
    });
    this.props.onSendGaTakePhoto();
  }

  onCloseCameraCallback = () => {
    this.setState({
      switchToCamera: false
    });
  }

  renderCameraYmm() {
    return (
      <CameraYmm
        onCloseCallback={this.onCloseCameraCallback}
        onTakePhotoSuccess={this.props.onTakePhotoSuccess}
        gender={this.props.gender}
      />
      );
  }

  renderTakePhotoBtn() {
    return (
      <button
        className={this.props.customStyle}
        onClick={this.onClickTakePhotoBtn}
      >
        {this.props.title}
      </button>
      );
  }

  render() {
    return (
      <div>
        {this.renderTakePhotoBtn()}
        {this.state.switchToCamera && this.renderCameraYmm()}
      </div>
      );
    }
}

export default TakePhoto;