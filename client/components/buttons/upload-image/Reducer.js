import {
  GET_FACEBOOK_ALBUM_REQUEST,
  GET_FACEBOOK_ALBUM_SUCCESS,
  GET_FACEBOOK_ALBUM_FAILURE,
  GET_FACEBOOK_IMAGES_BY_ALBUM_REQUEST,
  GET_FACEBOOK_IMAGES_BY_ALBUM_SUCCESS,
  GET_FACEBOOK_IMAGES_BY_ALBUM_FAILURE,
  GET_FACEBOOK_FRIEND_REQUEST,
  GET_FACEBOOK_FRIEND_SUCCESS,
  GET_FACEBOOK_FRIEND_FAILURE,
  UPLOAD_IMAGE_FROM_FACEBOOK_REQUEST,
  UPLOAD_IMAGE_FROM_FACEBOOK_SUCCESS,
  UPLOAD_IMAGE_FROM_FACEBOOK_FAILURE,
  UPLOAD_IMAGE_FROM_DEVICE_REQUEST,
  UPLOAD_IMAGE_FROM_DEVICE_SUCCESS,
  UPLOAD_IMAGE_FROM_DEVICE_FAILURE
} from './Constant';

const initialCurrentUserState = {
  isGettingFacebookAlbum: false,
  isUploadingImageFromFacebook: false,
  isUploadingImageFromDevice: false,
  isGettingFacebookImage: false,
  isGettingFacebookFriend: false,
  facebookAlbum: [],
  facebookPictures: [],
  facebookFriends: []
};

export default function upload_image(state = initialCurrentUserState, action) {
  switch (action.type) {
    case GET_FACEBOOK_ALBUM_REQUEST:
      return { ...state, isGettingFacebookAlbum: true };
    case GET_FACEBOOK_ALBUM_SUCCESS:
      return { ...state, isGettingFacebookAlbum: false, facebookAlbum: action.res.data };
    case GET_FACEBOOK_ALBUM_FAILURE:
      return { ...state, isGettingFacebookAlbum: false };

    case GET_FACEBOOK_IMAGES_BY_ALBUM_REQUEST:
      return { ...state, isGettingFacebookImage: true };
    case GET_FACEBOOK_IMAGES_BY_ALBUM_SUCCESS:
      return { ...state, isGettingFacebookImage: false, facebookPictures: action.res.data };
    case GET_FACEBOOK_IMAGES_BY_ALBUM_FAILURE:
      return { ...state, isGettingFacebookImage: false };
    case GET_FACEBOOK_FRIEND_REQUEST:
      return { ...state, isGettingFacebookFriend: true };
    case GET_FACEBOOK_FRIEND_SUCCESS:
      return { ...state, isGettingFacebookFriend: false, facebookFriends: action.response.data };
    case GET_FACEBOOK_FRIEND_FAILURE:
      return { ...state, isGettingFacebookFriend: false };
    case UPLOAD_IMAGE_FROM_FACEBOOK_REQUEST:
      return { ...state, isUploadingImageFromFacebook: true }
    case UPLOAD_IMAGE_FROM_FACEBOOK_SUCCESS:
      return { ...state, isUploadingImageFromFacebook: false }
    case UPLOAD_IMAGE_FROM_FACEBOOK_FAILURE:
      return { ...state, isUploadingImageFromFacebook: false }
    case UPLOAD_IMAGE_FROM_DEVICE_REQUEST:
      return { ...state, isUploadingImageFromDevice: true }
    case UPLOAD_IMAGE_FROM_DEVICE_SUCCESS:
      return { ...state, isUploadingImageFromDevice: false }
    case UPLOAD_IMAGE_FROM_DEVICE_FAILURE:
      return { ...state, isUploadingImageFromDevice: false }
    default:
      return state;
  }
}
