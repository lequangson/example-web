import { CALL_API } from '../../../middleware/api';
import {
  GET_FACEBOOK_ALBUM_REQUEST,
  GET_FACEBOOK_ALBUM_SUCCESS,
  GET_FACEBOOK_ALBUM_FAILURE,
  GET_FACEBOOK_IMAGES_BY_ALBUM_REQUEST,
  GET_FACEBOOK_IMAGES_BY_ALBUM_SUCCESS,
  GET_FACEBOOK_IMAGES_BY_ALBUM_FAILURE,
  GET_FACEBOOK_FRIEND_REQUEST,
  GET_FACEBOOK_FRIEND_SUCCESS,
  GET_FACEBOOK_FRIEND_FAILURE,
  UPLOAD_IMAGE_FROM_FACEBOOK_REQUEST,
  UPLOAD_IMAGE_FROM_FACEBOOK_SUCCESS,
  UPLOAD_IMAGE_FROM_FACEBOOK_FAILURE,
  UPLOAD_IMAGE_FROM_DEVICE_REQUEST,
  UPLOAD_IMAGE_FROM_DEVICE_SUCCESS,
  UPLOAD_IMAGE_FROM_DEVICE_FAILURE
} from './Constant';

function getFacebookAlbum() {
  return {
    [CALL_API]: {
      endpoint: 'facebook_albums',
      method: 'GET',
      authenticated: true,
      types: [
        GET_FACEBOOK_ALBUM_REQUEST,
        GET_FACEBOOK_ALBUM_SUCCESS,
        GET_FACEBOOK_ALBUM_FAILURE,
      ],
    },
  };
}

function getFacebookFriendsRequest() {
  return {
    type: GET_FACEBOOK_FRIEND_REQUEST
  }
}
function getFacebookFriends() {
  return {
    [CALL_API]: {
      endpoint: 'facebook_friends',
      method: 'GET',
      authenticated: true,
      params: {},
      types: [
        GET_FACEBOOK_FRIEND_REQUEST,
        GET_FACEBOOK_FRIEND_SUCCESS,
        GET_FACEBOOK_FRIEND_FAILURE
      ],
    },
  };
}

function getFacebookImagesByAlbum(album_id = null) {
  return {
    [CALL_API]: {
      endpoint: 'facebook_pictures',
      method: 'get',
      params: {
        album_id
      },
      authenticated: true,
      types: [
        GET_FACEBOOK_IMAGES_BY_ALBUM_REQUEST,
        GET_FACEBOOK_IMAGES_BY_ALBUM_SUCCESS,
        GET_FACEBOOK_IMAGES_BY_ALBUM_FAILURE
      ],
    },
  };
}


function validateImage(pictureType, imageName, imageSize, ignoreMsg = false) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'upload_image',
      params: {
        'photo[picture_type]': pictureType.toString(),
        'photo[image_name]': imageName,
        'photo[image_size]': imageSize.toString(),
      },
      authenticated: true,
      types: [
        'UPLOAD_IMAGE_VALIDATE_REQUEST',
        'UPLOAD_IMAGE_VALIDATE_SUCCESS',
        'UPLOAD_IMAGE_VALIDATE_FAILURE',
      ],
      extentions: { ignoreMsg, pictureType },
    },
  }
}

/**
 * upload avatar from device
 * @param  {[type]} session_id [description]
 * @param  {[type]} image_data [base64]
 * @return {[type]}            [description]
 */
function uploadImage(session_id, image_data, upload_command) {
  const base64 = ';base64,'
  const startIndex = image_data.indexOf(base64) === -1
    ? 0
    : image_data.indexOf(base64) + base64.length
  const data = image_data.substring(startIndex);
  return {
    [CALL_API]: {
      endpoint: 'upload_image',
      method: 'POST',
      params: {
        'photo[session_id]': session_id,
        'photo[image_data]': data,
        'command': upload_command
      },
      authenticated: true,
      types: [
        UPLOAD_IMAGE_FROM_DEVICE_REQUEST,
        UPLOAD_IMAGE_FROM_DEVICE_SUCCESS,
        UPLOAD_IMAGE_FROM_DEVICE_FAILURE
      ],
    },
  };
}

function uploadImageRequest() {
  return {
    type: UPLOAD_IMAGE_FROM_DEVICE_REQUEST
  }
}
/**
 * upload avatar from facebook
 * @param  {[type]} session_id [description]
 * @param  {[type]} image_data [description]
 * @return {[type]}            [description]
 */
function uploadImageFromFacebook(select_item, picture_type) {
  return {
    [CALL_API]: {
      endpoint: 'facebook_pictures',
      method: 'POST',
      params: {
        'picture[picture_urls][]': select_item.avatar,
        'picture[picture_type]': picture_type,
        'picture[nick_name]': select_item.name,
        'picture[gender]': select_item.gender
      },
      authenticated: true,
      types: [
        UPLOAD_IMAGE_FROM_FACEBOOK_REQUEST,
        UPLOAD_IMAGE_FROM_FACEBOOK_SUCCESS,
        UPLOAD_IMAGE_FROM_FACEBOOK_FAILURE
      ],
    },
  };
}

function uploadImageFromFacebookRequest() {
  return {
    type: UPLOAD_IMAGE_FROM_FACEBOOK_REQUEST
  }
}

export {
  getFacebookAlbum,
  getFacebookImagesByAlbum,
  validateImage,
  uploadImage,
  uploadImageFromFacebook,
  getFacebookFriends,
  getFacebookFriendsRequest,
  uploadImageFromFacebookRequest,
  uploadImageRequest
};
