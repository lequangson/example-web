import React, { Component, PropTypes } from 'react';

class UploadImage extends Component {
    static propTypes = {
        className: PropTypes.string,
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>Upload Image</div>
        );
    }
}

export default UploadImage;
