import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import q from 'q';
import FileInput from 'react-file-reader-input';
import Modal from 'react-modal';
import { isEmpty } from 'lodash';
import {
  validateImage,
  uploadImage,
  uploadImageFromFacebook,
  getFacebookFriends,
  getFacebookFriendsRequest,
  uploadImageFromFacebookRequest
} from './Action';
import { AVATAR_PICTURE, OTHER_PICTURE, UPLOAD_DEVIDE_MODE } from '../../../constants/userProfile';
import { ProgressiveImage } from '../../../components/commons/ProgressiveImage'

class UploadImageFromFb extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isUploading: false,
      isOpenModal: false,
      select_item: {},
    };
  }

  openUploadFromDevice = async (e, results) => {
    if (this.state.isUploading) {
      return;
    }

    const { picture_type } = this.props;
  }

  openModal = () => {
    const { upload_image } = this.props;
    if (!upload_image.facebookFriends.length) {
      this.props.getFacebookFriends();
    }
    this.setState({ isOpenModal: true });
  }

  closeModal = () => {
    if (this.state.isUploading) {
      return;
    }
    this.setState({ isOpenModal: false });
  }

  select_item = (select_item) => {
    this.setState({ select_item })
  }

  view_similar_face = async ()  => {
    this.setState({ isOpenModal: false, isUploading: true });
    const uploaded_image = await this.props.uploadImageFromFacebook(this.state.select_item, 6); // 6: picture type for similar face
    this.props.upload_success(uploaded_image.response.data);
    this.setState({ isUploading: false });
  }

  render() {

    const { upload_image } = this.props;
    const { facebookFriends, isGettingFacebookFriend } = upload_image;
    const disabledButton = isEmpty(this.state.select_item);
    return <div onClick={this.openModal}  style={{display: 'inline'}} >
      {this.props.children}
      <Modal
          isOpen={this.state.isOpenModal}
          onRequestClose={() => {}}
          className="FromFacebookModal modal__content"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="modal__container">
            <div className="banner banner--blue-medium no-cursor txt-white mbm radius-top">
              <div className="banner__content">
                <div className="txt-bold txt-xlg txt-center txt-blue mbs">Chọn một người bạn</div>
              </div>
            </div>
            <div className="modal__txt-content">
              <div className="row" style={{'height': '500px', 'overflowY':'scroll' }}>
                {isGettingFacebookFriend && <div>Đang tải dữ liệu từ Facebook...</div>}
                {
                  facebookFriends.map((item, i) => {
                    const isActive = this.state.select_item.avatar === item.avatar
                    return (
                      <div className="col-xs-6 col-sm-4 col-md-4 txt-center" key={i}  onClick={() => this.select_item(item)} >
                      <div className={isActive?"category__item margin-0 active":"category__item margin-0"}>
                        <div className="category__image category__image--icon pos-relative margin-auto txt-center">
                          <img src={item.avatar} className="img-full perfect-center" alt="SimilarFace"/>
                        </div>
                        <div className="category__txt">{item.name}</div>
                      </div>
                    </div>
                    )
                  })
                }
              </div>
            </div>
            <div className="modal__txt-content">
              <div className="row">
                <button className="btn txt-center btn--p btn--b" onClick={this.view_similar_face} disabled={disabledButton} >Xem ngay</button>
              </div>
            </div>
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
      </Modal>
    </div>
  }
}

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    validateImage: async (picture_type, image_name, image_size) => await dispatch(validateImage(picture_type, image_name, image_size)),
    uploadImage: async (session_id, image_data) => dispatch(uploadImage(session_id, image_data)),
    uploadImageFromFacebook: async (select_item, picture_type) => {
      dispatch(uploadImageFromFacebookRequest());
      return await dispatch(uploadImageFromFacebook(select_item, picture_type));
    },
    getFacebookFriends: async () => { dispatch(getFacebookFriendsRequest()); return await dispatch(getFacebookFriends()) }
  };
};

UploadImageFromFb.propTypes = {
  picture_type: PropTypes.number.isRequired,
};

UploadImageFromFb.defaultProps = {

};

const UploadImageFromFbContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadImageFromFb);

export { UploadImageFromFbContainer as UploadImageFromFacebook };
