import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import q from 'q';
import FileInput from 'react-file-reader-input';
import {
  uploadImageRequest,
  validateImage,
  uploadImage,
} from './Action';
import { AVATAR_PICTURE, OTHER_PICTURE, UPLOAD_DEVIDE_MODE } from '../../../constants/userProfile'
// import { StatusUploading } from './modal';

class UploadImageFromDevice extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isUploading: false,
      uploaded_image_url: null
    };
  }

  openUploadFromDevice = async (e, results) => {
    if (this.state.isUploading) {
      return;
    }

    const { picture_type, upload_command } = this.props;
    const _this = this;
    for (let result of results) {
      const [, file] = result;
      _this.setState({ isUploading: true });
      const validateImage = await this.props.validateImage(picture_type, file.name, file.size);
      const reader = new FileReader()
      reader.addEventListener('load', async () => {
        const { response } = validateImage;
        const { session_id } = response.data;
        const uploaded_image = await _this.props.uploadImage(session_id, reader.result, upload_command);
        const { small_image_url } = uploaded_image.response.data;
        _this.props.upload_success(uploaded_image.response.data);
        _this.setState({ isUploading: false });
      });
      if (file) {
        reader.readAsDataURL(file);
      }
    }
  }

  render() {

    return <FileInput id="my-file-input" onChange={this.openUploadFromDevice} accept="image/*" >
      {this.props.children}
    </FileInput>
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    validateImage: async (picture_type, image_name, image_size) => await dispatch(validateImage(picture_type, image_name, image_size)),
    uploadImage: async (session_id, image_data, upload_command) => {
      dispatch(uploadImageRequest());
      return await dispatch(uploadImage(session_id, image_data, upload_command));
    },
  };
};

UploadImageFromDevice.propTypes = {
  picture_type: PropTypes.number.isRequired,
};

UploadImageFromDevice.defaultProps = {

};

const UploadImageFromDeviceContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadImageFromDevice);

export { UploadImageFromDeviceContainer as UploadImageFromDevice };
