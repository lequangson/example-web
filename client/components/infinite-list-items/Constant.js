export const GRID_VIEW_USER_CARD = 1;
export const DETAIL_VIEW_USER_CARD = 2;
export const NEW_PHOTO_CARD = 3;
export const SIMILAR_FACE_CARD = 4;
export const SHARE_SIMILAR_FACE_CARD = 5;

export const INFINITIVE_PAGE_SIZE = 20;