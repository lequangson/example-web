import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Infinite from 'react-infinite';
import debounce from "debounce";
import { Link, browserHistory } from 'react-router';
import $ from 'jquery';
import _ from 'lodash';
import DetailItem from '../searches/DetailItem';
import ListTwoItems from '../searches/ListTwoItems';
import ListCardItems from 'pages/similar-face/list-card/ListCardItems';
import ListSharingCardItem from 'pages/similar-face/list-card/ListSharingCardItem';
import List2NewPhotoCard from 'components/commons/Cards/List2NewPhotoCard';
import BestPhotoBanner from 'components/commons/BestPhotoBanner';
import {
  INFINITIVE_PAGE_SIZE, SHARE_SIMILAR_FACE_CARD,
  GRID_VIEW_USER_CARD, DETAIL_VIEW_USER_CARD,
  NEW_PHOTO_CARD, SIMILAR_FACE_CARD,
} from './Constant';

const gridViewCardHeight = 457;
const detailViewCardHeight = 747;
const similarFaceCardHeight = 303;
const shareSimilarFaceCardHeight = 198;
const newPhotoCardHeight = 305;
const newPhotoTipBannerHeight = 56;

class InfiniteListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalItemToShow: INFINITIVE_PAGE_SIZE,
      isInfiniteLoading: false,
      cardHeight: gridViewCardHeight,
      tipsBannerHeight: newPhotoTipBannerHeight,
      elementHeights: [],
    };
    this.handleInfiniteLoad = this.handleInfiniteLoad.bind(this);
    this.getRealCardHeight = this.getRealCardHeight.bind(this);
    this.callGetRealCardHeight = debounce(this.getRealCardHeight, 100, true);
  }

  componentWillMount() {
    this.setDefaultCardHeight(this.props.viewMode, this.props.listItems);
  }

  componentWillReceiveProps(nextProps) {
    if ((this.props.listItems.length === 0 && nextProps.listItems.length > 0) || 
      (nextProps.viewMode !== this.props.viewMode)) {
      this.setDefaultCardHeight(nextProps.viewMode, nextProps.listItems);
    }
  }

  componentDidMount() {
    this.callGetRealCardHeight();
  }

  componentDidUpdate() {
    this.callGetRealCardHeight();
  }

  setDefaultCardHeight(viewMode, listItems) {
    let cardHeight = gridViewCardHeight;
    let tipsBannerHeight = newPhotoTipBannerHeight;
    switch (viewMode) {
      case GRID_VIEW_USER_CARD:
        cardHeight = gridViewCardHeight;
        break;
      case DETAIL_VIEW_USER_CARD:
        cardHeight = detailViewCardHeight;
        break;
      case NEW_PHOTO_CARD:
        cardHeight = newPhotoCardHeight;
        tipsBannerHeight = newPhotoTipBannerHeight;
        break;
      case SIMILAR_FACE_CARD:
        cardHeight = similarFaceCardHeight;
        break;
      case SHARE_SIMILAR_FACE_CARD:
        cardHeight = shareSimilarFaceCardHeight;
        break;
      default:
        break;
    }
    let numberItems = listItems.length >= this.state.totalItemToShow ?
        this.state.totalItemToShow : listItems.length;

    if (numberItems === 0 && listItems.length > 0) {
      numberItems = listItems.length;
    }
    const elementHeights = this.calculateElementHeights(numberItems, cardHeight, tipsBannerHeight, viewMode);
    this.setState({
      cardHeight: cardHeight,
      tipsBannerHeight: tipsBannerHeight,
      totalItemToShow: numberItems,
      elementHeights: elementHeights
    });
  }

  getRealCardHeight() {
    const cardHeight = Math.round($('.element_height').height());
    if (!isNaN(cardHeight) && cardHeight != 0 && cardHeight + 5 != this.state.cardHeight) {
      this.setState({cardHeight: cardHeight + 5});
    }
  }

  calculateElementHeights(totalItemToShow, cardHeight, tipsBannerHeight, viewMode) {
    let newElementHeights = [];
    let numberRowToShow = totalItemToShow;
    switch (viewMode) {
      case GRID_VIEW_USER_CARD:
      case NEW_PHOTO_CARD:
      case SIMILAR_FACE_CARD:
        numberRowToShow = totalItemToShow / 2;
        break;
      case DETAIL_VIEW_USER_CARD:
      case SHARE_SIMILAR_FACE_CARD:
        numberRowToShow = totalItemToShow;
        break;
      default:
        numberRowToShow = totalItemToShow;
        break;
    }
    for(let i = 0; i < numberRowToShow; i++) {
      if (this.isNeedToShowTipBanner(i)) {
        // Tip banner + Card
        newElementHeights.push(tipsBannerHeight + cardHeight);
      }
      else {
        newElementHeights.push(cardHeight);
      }
    }

    return newElementHeights;
  }

  elementInfiniteLoad() {
    return (
      <div className="col-xs-12 txt-center loader is-load">
        <i className="fa fa-spinner fa-3x" />
      </div>
    )
  }

  handleInfiniteLoad() {
    const that = this;
    this.setState({
        isInfiniteLoading: true
    });

    setTimeout(() => {
      const expectItems = that.state.totalItemToShow + INFINITIVE_PAGE_SIZE;
      if (expectItems >= that.props.listItems.length &&
        !that.props.loadingDataStatus &&
        !that.props.reachEnd &&
        that.props.loadMoreDataCallback) {
        that.props.loadMoreDataCallback();
      }
      const nextTotalItemToShow = expectItems > that.props.listItems.length ?
        that.props.listItems.length : expectItems;
      const elementHeights = this.calculateElementHeights(nextTotalItemToShow, this.state.cardHeight, this.state.tipsBannerHeight, this.props.viewMode);
      that.setState({
          isInfiniteLoading: false,
          totalItemToShow: nextTotalItemToShow,
          elementHeights: elementHeights
      });
    }, 500);
  }

  getItemToRender() {
    return this.props.listItems.slice(0, this.state.totalItemToShow);
  }

  renderItems() {
    switch (this.props.viewMode) {
      case GRID_VIEW_USER_CARD:
        return this.renderGridViewUserCard();
      case DETAIL_VIEW_USER_CARD:
        return this.renderDetailViewUserCard();
      case NEW_PHOTO_CARD:
        return this.renderNewPhotoCard();
      case SIMILAR_FACE_CARD:
        return this.renderSimilarFaceCard();
      case SHARE_SIMILAR_FACE_CARD:
        return this.renderShareSimilarFaceCard();
      default:
        return this.renderGridViewUserCard();
    }
  }

  renderGridViewUserCard() {
    const listItems = _.chunk(this.getItemToRender(), 2);
    return (
      listItems.map((item, index) => {
        return (
          <div key={`ListItem-${index}`}>
            <ListTwoItems
              pageSource={this.props.pageSource}
              key={index}
              index={index}
              users={item}
              current_user={this.props.current_user}
            />
            {this.renderTipBanner(index)}
          </div>
        )
      })
    )
  }

  renderDetailViewUserCard() {
    const listItems = this.getItemToRender();
    return (
      listItems.map((item, index) => {
        return (
          <div key={`ListItem-${index}`}>
            <DetailItem
              pageSource={this.props.pageSource}
              key={index}
              index={index}
              data={item}
              current_user={this.props.current_user}
            />
            {this.renderTipBanner(index)}
          </div>
        )
      })
    )
  }

  renderNewPhotoCard() {
    const listItems =  _.chunk(this.getItemToRender(), 2);

    return (
      listItems.map((photos, index) => {      
        return (
          <div key={index} className="row">
            <List2NewPhotoCard
              rowIndex={index}
              photos={photos}
            />
            {this.renderTipBanner(index)}
          </div>
        )
      })
    )
  }

  renderSimilarFaceCard() {
    const listItems =  _.chunk(this.getItemToRender(), 2);
    return (
      listItems.map((items, index) => {
        return (
          <div key={`ListItem-${index}`}>
            <ListCardItems
              index={index}
              users={items}
              onClick={this.props.onClick}
            />
          </div>
        )
      })
    )
  }

  renderShareSimilarFaceCard() {
    const listItems =  _.chunk(this.getItemToRender(), 4);
    return (
      listItems.map((items, index) => {
        return (
          <div key={`ListItem-${index}`}>
            <ListSharingCardItem
              index={index}
              users={items}
              activeModalCallback={this.props.activeModalCallback}
              current_user={this.props.current_user}
            />
          </div>
        )
      })
    )
  }

  isNeedToShowTipBanner(index) {
    if (!this.props.listTipsBanner || this.props.listTipsBanner.length === 0) {
      return false;
    }
    if (!this.props.numberRowToShowTipBanner || this.props.numberRowToShowTipBanner <= 0) {
      return false;
    }

    if (index <= 0) {
      return false;
    }

    return (index + 1) % this.props.numberRowToShowTipBanner === 0;
  }

  renderTipBanner(index) {
    if (!this.isNeedToShowTipBanner(index)) {
      return;
    }

    const totalTipBanner = this.props.listTipsBanner.length;
    const numberRowToShowTipBanner = this.props.numberRowToShowTipBanner ? 
      this.props.numberRowToShowTipBanner : 1;
    const bannerIndex = (((index + 1) / numberRowToShowTipBanner) - 1) % totalTipBanner;

    switch (this.props.viewMode) {
      case NEW_PHOTO_CARD:
        return (
          <BestPhotoBanner
            banner={this.props.listTipsBanner[bannerIndex]}
          />
        );
      default:
        break;
    }
  }

  getLoadBeginEdgeOffset() {
    if ((this.props.reachEnd && this.state.totalItemToShow >= this.props.listItems.length) ||
      (this.props.listItems.length === 0 && !this.props.loadingDataStatus)) {
      return undefined;
    }
    return 200;
  }

  render() { 
    const haveContainerHeight = this.props.containerHeight != null && this.props.containerHeight > 0;     

//console.log('render', this.state.elementHeights.length, this.getItemToRender().length)    
    return (
      <div className="container">
        <Infinite
          elementHeight={this.state.elementHeights}
          infiniteLoadBeginEdgeOffset={this.getLoadBeginEdgeOffset()}
          onInfiniteLoad={this.handleInfiniteLoad}
          loadingSpinnerDelegate={this.elementInfiniteLoad()}
          isInfiniteLoading={this.state.isInfiniteLoading}
          containerHeight={this.props.containerHeight}
          useWindowAsScrollContainer={!haveContainerHeight}
        >
          {this.renderItems()}
        </Infinite>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    current_user: state.user_profile.current_user, 
  };
}

InfiniteListItem.propTypes = {
  listItems: PropTypes.array.isRequired,
  loadingDataStatus: PropTypes.bool.isRequired,
  reachEnd: PropTypes.bool.isRequired,
  viewMode: PropTypes.number.isRequired,
  pageSource: PropTypes.string.isRequired,
  containerHeight: PropTypes.number,
  loadMoreDataCallback: PropTypes.func,
  activeModalCallback: PropTypes.func,
  listTipsBanner: PropTypes.array,
  numberRowToShowTipBanner: PropTypes.number,
}

const InfiniteListItemContainer = connect(
  mapStateToProps,
  null
)(InfiniteListItem);
export default InfiniteListItemContainer;