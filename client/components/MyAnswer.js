import React, { Component } from 'react'
import { CDN, SMILE_FACE } from '../constants/Enviroment.js'
import DropdownPanel from './commons/DropdownPanel'
import {
  ANSWER_MY_EXPLORER_QUESTION, ANSWER_OTHER_EXPLORER_QUESTION
} from '../constants/Match_Questions'
import StartAnswerButtonContainer from './commons/Buttons/StartAnswerButton.js'
import SelectMyExploreQuestionButton from './commons/Buttons/SelectMyExploreQuestionButton.js'
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class MyAnswer extends Component {
	constructor() {
		super()
		this.state = {
		}
	}

  componentWillMount() {
    this.props.getUserQuestions()
  }

  scrollBottom() {
    const explore_yourself = this.props.user_profile.current_user.explore_yourself
    const dropdownPanel = document.querySelectorAll('.panel')
    if(dropdownPanel && explore_yourself){
      dropdownPanel[dropdownPanel.length - 1].addEventListener("click", () =>
        setTimeout( () => window.scrollTo(0, document.body.scrollHeight)
      , 500)
    )}
  }

  @GoogleOptimize
  componentDidMount() {}

  componentDidUpdate(prevProps) {
    if (prevProps.user_profile.current_user.explore_yourself !== this.props.user_profile.current_user.explore_yourself) {
      this.scrollBottom()
    }
  }

  render() {
    const explore_yourself = this.props.user_profile.current_user.explore_yourself
    const my_questions = this.props.Match_Questions.my_questions
    const answers = this.props.Match_Questions.answers
  	return (
  		<div className="container">
  			<div className="row">
  				<div className="col-xs-12">
  					<h2 className="txt-heading">Khám phá bản thân</h2>
    				{ !explore_yourself	?
              <div>
                <p>Bạn đã thật sự hiểu về bản thân mình chưa?</p>
    					  <p>
    					  	Hãy khám phá tính cách thú vị của bản thân và để người ấy hiểu thêm về bạn qua 20 câu hỏi! <img src={SMILE_FACE} alt="leson" className="img-small"/> <br />
  						   	Mỗi câu hỏi sẽ nắm giữ một phần bí mật về tính cách của bạn và giúp Ymeet.me đọc vị được mẫu người lý tưởng dành cho bạn dễ dàng hơn. <br />
  						   	Bạn chỉ thấy được kết quả khi trả lời đủ 20 câu hỏi.
    					  </p>
    					 <StartAnswerButtonContainer Mode={ANSWER_MY_EXPLORER_QUESTION} user={this.props.user_profile.current_user}/>
              </div>
              :
              <div>
                <div className="mbm">
                  <p className="txt-underline">Kết quả:</p>
                  {explore_yourself}
                </div>
                <DropdownPanel
                panelTitle="Chi tiết câu hỏi và trả lời của bạn"
                >
                {
                  my_questions.map((question,i) => (
                    <div className="col-xs-12" key={i}>
                       <div className="mbs"></div>
                       <div className="txt-blue mbs">Câu {i+1}/{my_questions.length}: {question.title}</div>
                       <div className="txt-light">Trả lời: {answers[question.answer_code-1].title}</div>
                     </div>
                  ))
                }
                </DropdownPanel>
                <DropdownPanel
                panelTitle="Câu hỏi thử thách vượt rào"
                >
                <div className="col-xs-12">
                  <div className="mbs"></div>
                  <p>Chọn ra 5 trong 20 câu hỏi mà bạn đã trả lời ở trên. Người ấy sẽ đoán câu trả lời của 5 câu hỏi mà bạn chọn. Nếu khớp toàn bộ với 5/5 câu trả lời của bạn, thật tuyệt vời, hai bạn sẽ được <span className="txt-blue txt-medium">ghép đôi ngay lập tức</span>. Xác suất để một người hiểu bạn đến mức ấy là rất nhỏ. Hai bạn đúng là một cặp trời sinh đấy!</p>
                  <SelectMyExploreQuestionButton my_questions={my_questions}/>
                  </div>
                </DropdownPanel>
              </div>
            }
  				</div>
        </div>
  		</div>

  	)
  }
}

export default MyAnswer;