import React, { Component, PropTypes } from 'react'
import $ from 'jquery'
import moment from 'moment';
import {
  WHO_ADD_ME_TO_FAVOURITE_PAGE, DEFAULT_LOAD_FEMALE, DEFAULT_LOAD_MALE, CDN_URL,
  GA_ACTION_VIEW_ADMIN_MESSAGE, ACTIVITY_VIEW_ADMIN_MESSAGE,
} from '../constants/Enviroment'
import { THERE_IS_NO_ONE_I_MATCHED_MESSAGE, LOADING_DATA_TEXT_AFTER_4SECS, LOADING_DATA_TEXT } from '../constants/TextDefinition'
import ListItem from './searches/ListItem'
import NotPaymentList from './searches/NotPaymentList'
import * as ymmStorage from '../utils/ymmStorage'
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class WhoAddMeToFavourite extends Component {
  constructor() {
    super()
  }

  @GoogleOptimize
  componentWillMount() {
    ymmStorage.removeItem('keep')
    this.props.getWhoAddMeToFavourire()
    this.props.updatePreviousPage(WHO_ADD_ME_TO_FAVOURITE_PAGE)
    setTimeout(() => {this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT_AFTER_4SECS)}, 4000)
  }

  componentDidMount() {
    const userCardHeight = $('.element_height').height();
    const segmentIndex = this.props.Enviroment.profile_next_user_index * userCardHeight;
    window.scroll(0, this.props.Enviroment.matched_list_scroll_top + segmentIndex);
  }

  componentWillUnmount() {
    this.props.updateScrollPossition(WHO_ADD_ME_TO_FAVOURITE_PAGE, window.scrollY)
    this.props.setProfileUserNextIndex(0)
    this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT)
  }


  render() {
    const who_add_favourite = this.props.user_profile.who_add_favourite.filter(user => user.have_permission === true)
    const not_payment_list = this.props.user_profile.who_add_favourite.filter(user => user.have_permission !== true)
    const isLoaded = this.props.user_profile.is_page_loading.who_add_favourite
    const loadingText = this.props.Enviroment.isLoadingText
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE

    return (
      <div className="site-content">
        <div className="container">
          <div className="row mbm">
            <div className="col-xs-12">
              <h3 className="txt-heading">Danh sách những người đã quan tâm bạn</h3>
            </div>
          </div>
          <div className="row">
            {
              isLoaded ?
              who_add_favourite.length > 0 || not_payment_list.length > 0 ?
              (who_add_favourite.map((user, i) =>
                <ListItem
                  key={i}
                  id={`dtail-view-${i}`}
                  index={i}
                  data={user}
                  pageSource={WHO_ADD_ME_TO_FAVOURITE_PAGE}
                />
              ))
              :
               <div className="row">
                 <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                   <article className="mbl mc">
                     <div className="card__upper">
                       <div href="#" className="card__link mbm">
                         <img src={default_image} alt="" className="card__img"/>
                       </div>
                       <div className="txt-center txt-light txt-lg">Những người đã quan tâm bạn được thể hiện ở đây. <br /> Gửi "Thích" tới họ để rút ngắn khoảng cách giữa hai bạn nhé! </div>
                     </div>
                   </article>
                 </div>
               </div>
              :
              <div className="row">
                <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                  <article className="mbl mc">
                    <div className="card__upper">
                      <div href="#" className="card__link">
                        <img src={default_image} alt="" className="card__img"/>
                      </div>
                      <div className="card__center loader is-load">
                        <div className="mbs">{loadingText}</div>
                        <i className="fa fa-spinner fa-3x"></i>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            }
            {
              not_payment_list.length > 0 &&
                <NotPaymentList current_user={this.props.user_profile.current_user} data={not_payment_list} page={WHO_ADD_ME_TO_FAVOURITE_PAGE}/>
            }

          </div>
        </div>
      </div>
    )
  }
}

WhoAddMeToFavourite.propTypes = {
  getPeopleMatchedMe: PropTypes.func.isRequired,
  updatePreviousPage: PropTypes.func.isRequired,
  user_profile: PropTypes.object.isRequired,
  Enviroment: PropTypes.object.isRequired,
  updateScrollPossition: PropTypes.func.isRequired,
  setProfileUserNextIndex: PropTypes.func,
  addActivityAudit: PropTypes.func,
  gaSend: PropTypes.func,
}

export default WhoAddMeToFavourite
