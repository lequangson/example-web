/* global document: true */

import React, { Component, PropTypes } from 'react'
import $ from 'jquery'
import { PEOPLE_I_LIKED_PAGE, DEFAULT_LOAD_MALE, DEFAULT_LOAD_FEMALE } from '../constants/Enviroment'
import { THERE_IS_NO_ONE_I_LIKED_MESSAGE, LOADING_DATA_TEXT, LOADING_DATA_TEXT_AFTER_4SECS } from '../constants/TextDefinition'
import DetailItem from './searches/DetailItem'
import * as ymmStorage from '../utils/ymmStorage';
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class PeopleILiked extends Component {
  componentWillMount() {
    ymmStorage.removeItem('keep')
    this.props.getPeopleILiked()
    this.props.updatePreviousPage(PEOPLE_I_LIKED_PAGE)
    setTimeout(() => {this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT_AFTER_4SECS)}, 4000)
  }

  @GoogleOptimize
  componentDidMount() {
    const userCardHeight = $('.element_height').height();
    const segmentIndex = this.props.Enviroment.profile_next_user_index * userCardHeight;
    window.scroll(0, this.props.Enviroment.people_i_liked_scroll_top + segmentIndex);
  }

  componentWillUnmount() {
    this.props.updateScrollPossition(PEOPLE_I_LIKED_PAGE, window.scrollY)
    this.props.setProfileUserNextIndex(0)
    this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT)
  }

  render() {
    const people_i_liked = this.props.PeopleILiked.data
    const loaded = this.props.PeopleILiked.loaded
    const loadingText = this.props.Enviroment.isLoadingText
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE

    return (
      <div className="site-content">
        <div className="container">
          <div className="row mbm">
            <div className="col-xs-12">
              <h3 className="txt-heading">Những người bạn đã thích</h3>
            </div>
          </div>
          <div className="row">
            {
              loaded ?
              people_i_liked.length > 0 ?
              (people_i_liked.map((user, i) =>
                <DetailItem
                  key={i}
                  id={`dtail-view-${i}`}
                  index={i}
                  data={user}
                  pageSource={PEOPLE_I_LIKED_PAGE}
                />
              ))
              :
               <div className="row">
                 <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                   <article className="mbl mc">
                     <div className="card__upper">
                       <div href="#" className="card__link mbm">
                         <img src={default_image} alt="" className="card__img"/>
                       </div>
                       <div className="txt-center txt-light txt-lg">
                        <p className="txt-bold">Mục dành cho những người bạn đã gửi thích. Chờ họ thích lại bạn và 2 bạn có thể trò chuyện!</p>
                        <br /> Gửi thêm nhiều thích để thêm nhiều cơ hội nhé!</div>
                     </div>
                   </article>
                 </div>
               </div>
              :
              <div className="row">
                <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                  <article className="mbl mc">
                    <div className="card__upper">
                      <div href="#" className="card__link">
                        <img src={default_image} alt="" className="card__img"/>
                      </div>
                      <div className="card__center loader is-load">
                        <div className="mbs">{loadingText}</div>
                        <i className="fa fa-spinner fa-3x"></i>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
}

PeopleILiked.propTypes = {
  getPeopleILiked: PropTypes.func.isRequired,
  updatePreviousPage: PropTypes.func.isRequired,
  Enviroment: PropTypes.object.isRequired,
  updateScrollPossition: PropTypes.func.isRequired,
  PeopleILiked: PropTypes.object.isRequired,
  setProfileUserNextIndex: PropTypes.func,
}

export default PeopleILiked
