import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import _ from 'lodash'
import $ from 'jquery'
import {
  REGEX_EMAIL,
  REGEX_NAME,
  REGEX_MESSAGE,
} from '../constants/Enviroment'
import * as ymmStorage from '../utils/ymmStorage';

class Contact extends Component {
  constructor() {
    super()

    this.state = {
      isNameValid: false,
      isEmailValid: false,
      isMessageValid: false,
      sendContactSuccess: false,
      isAnonymous: true,
    }

    this.isFormValid = this.isFormValid.bind(this)
    this.validateEmail = this.validateEmail.bind(this)
    this.validateName = this.validateName.bind(this)
    this.validateMessage = this.validateMessage.bind(this)
    this.handleSendContact = this.handleSendContact.bind(this)
    this.cancel = this.cancel.bind(this)
  }

  componentWillMount() {
    this.removeRequireFields()
  }

  componentDidMount() {
    this.validateName()
    this.validateEmail()
    this.validateMessage()
  }

  componentWillReceiveProps() {
    this.removeRequireFields()
  }

  removeRequireFields() {
    const { auth, user_profile } = this.props
    if (auth.isAuthenticated) {
      if (this.state.isAnonymous) {
        this.setState({ isAnonymous: false })
      }
      const { current_user } = user_profile
      if (!_.isEmpty(current_user) && !this.state.isNameValid) {
        $('#name').val(current_user.nick_name)
        if (current_user.email) {
          $('#email').val(current_user.email)
          $('#email').attr('disabled', true)
        }
        this.setState({ isNameValid: true, isEmailValid: true })
      }
    }
  }

  isFormValid() {
    return this.state.isNameValid && this.state.isEmailValid && this.state.isMessageValid
  }

  cancel() {
    ymmStorage.removeItem('name')
    ymmStorage.removeItem('email')
    ymmStorage.removeItem('message')
    browserHistory.goBack()
  }
  validateEmail() {
    // in case browser not support input 'email'
    // and add custom error style
    ymmStorage.setItem('email', this.userEmail.value)
    const bypassValidate = !this.state.isAnonymous && !this.userEmail.value
    if (this.userEmail.value.match(REGEX_EMAIL) || bypassValidate) {
      this.setState({ isEmailValid: true })
    } else {
      this.setState({ isEmailValid: false })
    }
  }

  validateName() {
    ymmStorage.setItem('name', this.userName.value)
    if (this.userName.value.match(REGEX_NAME)) {
      this.setState({ isNameValid: true })
    } else {
      this.setState({ isNameValid: false })
    }
  }

  validateMessage() {
    ymmStorage.setItem('message', this.userMessage.value)
    if (this.userMessage.value.replace(/(\r\n|\n| |\r)/gm, '').match(REGEX_MESSAGE)) {
      this.setState({ isMessageValid: true })
    } else {
      this.setState({ isMessageValid: false })
    }
  }

  handleSendContact() {
    if (ymmStorage.getItem('inprogress')) {
      return
    }
    const params = {
      'contact_us[user_name]': this.userName.value,
      'contact_us[user_email]': this.userEmail.value,
      'contact_us[content]': this.userMessage.value,
    }
    ymmStorage.setItem('inprogress', true)
    const that = this
    this.props.sendContact(params, this.state.isAnonymous).then(() => {
      ymmStorage.removeItem('name')
      ymmStorage.removeItem('email')
      ymmStorage.removeItem('message')
      that.setState({ sendContactSuccess: true })
      ymmStorage.removeItem('inprogress')
    }).catch(() => {
      that.setState({ sendContactSuccess: false })
      ymmStorage.removeItem('inprogress')
    })
  }

  render() {
    const { auth, user_profile } = this.props
    const { current_user } = user_profile
    const userName = ymmStorage.getItem('name') || ''
    const userEmail = ymmStorage.getItem('email') || ''
    const userMessage = ymmStorage.getItem('message') || ''
    const isFormValid = this.isFormValid()

    return (
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h2 className="txt-heading">Liên hệ</h2>
              <p>Nếu bạn gặp vấn đề khó khăn khi sử dụng trang web Ymeet.me và bạn vẫn không thể giải quyết sau khi đã tham khảo <Link to="/helps">Trợ Giúp</Link>, hãy liên hệ với chúng tôi theo mẫu dưới đây để được hỗ trợ thêm</p>
              {!this.state.sendContactSuccess &&
                <p>
                  {/* will replace text later by constant or something? */}
                </p>
              }
              {this.state.sendContactSuccess &&
                <p>
                  Thông tin của bạn đã được gửi đi thành công.
                </p>
              }
            </div>
          </div>

          {!this.state.sendContactSuccess && <div className="row">
            <div className="col-xs-12 mbm">
              <label className="txt-blue" htmlFor="name">Họ tên</label>
              <input
                type="text"
                id="name"
                placeholder="Minh Hà"
                ref={(ref) => {
                  this.userName = ref
                }}
                onChange={this.validateName}
                disabled={auth.isAuthenticated}
                defaultValue={!this.state.isAnonymous ? current_user.nick_name : userName}
              />
              {(this.state.isNameValid || auth.isAuthenticated) || <span className="txt-sm txt-light txt-red">Từ 2 đến 10 ký tự</span>}
            </div>
            <div className="col-xs-12 mbm">
              <label className="txt-blue" htmlFor="email">Email</label>
              <input
                type="text"
                ref={(ref) => {
                  this.userEmail = ref
                }}
                onChange={this.validateEmail}
                id="email"
                placeholder="minhha@example.com"
                defaultValue={!this.state.isAnonymous ? current_user.email : userEmail}
                disabled={!this.state.isAnonymous && current_user.email}
              />
              {this.state.isEmailValid || <span className="txt-sm txt-light txt-red">Email không hợp lệ</span>}
            </div>
            <div className="col-xs-12 mbm">
              <label className="txt-blue" htmlFor="message">Tin nhắn</label>
              <textarea
                ref={(ref) => {
                  this.userMessage = ref
                }}
                onChange={this.validateMessage}
                id="message"
                value={userMessage}
                rows="4"
              ></textarea>
              {this.state.isMessageValid || <span className="txt-sm txt-light txt-red">Từ 20 đến 1024 ký tự</span>}
            </div>
          </div>
          }

          <div className="row">
            <div className="col-xs-6">
              <button className="btn btn--b" onClick={this.cancel}>{this.state.sendContactSuccess ? 'Quay lại' : 'Hủy'}</button>
            </div>
            {!this.state.sendContactSuccess &&
              <div className="col-xs-6">
                <button
                  className="btn btn--p btn--b"
                  disabled={!isFormValid}
                  onClick={this.handleSendContact}
                >
                  Gửi
                </button>
              </div>
            }
          </div>

        </div>
      </div>
    )
  }
}

Contact.propTypes = {
  auth: PropTypes.object,
  user_profile: PropTypes.object,
  sendContact: PropTypes.func,

}

export default Contact
