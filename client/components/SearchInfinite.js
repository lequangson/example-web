import React, { PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import _ from 'lodash';
import Modal from 'react-modal';
import BannerRemind from 'components/commons/BannerRemind';
import BannerRemindWithCoin from 'components/commons/BannerRemindWithCoin';
import BoostRankBanner from 'components/commons/BoostRankBanner';
import RandomMatchBanner from 'components/commons/RandomMatchBanner';
import InfiniteLoad from 'components/commons/InfiniteLoad';
import SubMenu from 'components/headers/SubMenu';
import { REMIND_FEMALE, REMIND_MALE } from 'constants/TextDefinition';
import * as ymmStorage from 'utils/ymmStorage';
import * as UserDevice from 'utils/UserDevice';
import {
  MAP_NEARBY_ACTIVE, SMILE_FACE, RANDOM_MATCH, BOOST_RANK, REMIND_BANNER, REMIND_BANNER_WITH_COIN
} from 'constants/Enviroment';
import { convertUTCToLocalTime } from 'utils/common';
import PhotoStream from './PhotoStream';

class InfiniteList extends React.Component {
  static elementInfiniteLoad() {
    return (
      <div className="col-xs-12 txt-center loader is-load">
        <i className="fa fa-spinner fa-3x" />
      </div>
    )
  }

  constructor(props) {
    super(props);
    this.state = {
      mode: '',
      error: '',
      isModalOpen: false,
    }
    this.handleGetLocation = this.handleGetLocation.bind(this)
    this.getLocation = this.getLocation.bind(this)
    this.showModal = this.showModal.bind(this)
    this.hideModal = this.hideModal.bind(this)
    this.handleShowInstruction = this.handleShowInstruction.bind(this)
  }

  componentWillMount() {
    window.scrollTo(0, 0)
    this.setState({ mode: this.props.params.mode})
    ymmStorage.removeItem('keep')
    if (this.props.params.mode === 'default') {
      ymmStorage.setItem('searchmode', 1)
    }
    if (!this.props.search.loaded_condition) {
      this.props.getSearchCondition();
    }
    this.props.getRemind()
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ mode: nextProps.params.mode})
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (success) => {
          this.props.updateLocation(success.coords.longitude, success.coords.latitude).then(() => {
            this.props.updateAskedLocationStatus(true)
            ymmStorage.setItem('long', success.coords.longitude)
            ymmStorage.setItem('lat', success.coords.latitude)
            ymmStorage.setItem('allowed', true)
          })
          },
        (error) => {
          console.log(error.message)
          switch (error.code)
            {
              case error.PERMISSION_DENIED:
                ymmStorage.setItem('allowed', false)
                this.props.updateAskedLocationStatus(true)
                break;
              case error.POSITION_UNAVAILABLE:
                ymmStorage.setItem('allowed', true)
                this.props.updateAskedLocationStatus(true)
                this.setState({ error: 'POSITION_UNAVAILABLE', })
                break;
              case error.PERMISSION_DENIED_TIMEOUT:
                ymmStorage.setItem('allowed', true)
                break;
              case error.UNKNOWN_ERROR:
                ymmStorage.setItem('allowed', true)
                break;
            }
        },
        options
      );
      var options = {
        enableHighAccuracy: true,
        timeout: 7000,
        maximumAge: 0
      };
    }
  }

  handleGetLocation() {
    if (this.props.params.mode === 'nearby') {
      this.getLocation()
      if (ymmStorage.getItem('allowed') && ymmStorage.getItem('allowed').toString() == 'false') {
        this.showModal()
      }
    }
  }

  componentWillUpdate() {
    const asked = this.props.user_profile.current_user.asked
    if (this.props.params.mode === 'nearby' && !asked) {
      this.getLocation()
    }
  }

  showModal() {
    this.setState({ isModalOpen: true })
  }

  hideModal() {
    this.setState({ isModalOpen: false })
  }

  handleShowInstruction() {
    browserHistory.push('/geo-instruction')
  }

  renderModalContent() {
    if (UserDevice.getName() === 'ios'  && UserDevice.getBrowserName() === 'safari') {
      return(
        <div className="mbs padding-l10">
          <ul className="list-unstyle">
            <li className="mbl"><span className="bg-li">1</span> Cài đặt / Setting</li>
            <li className="mbl"><span className="bg-li">2</span> Cài đặt chung/ Genenal</li>
            <li className="mbl"><span className="bg-li">3</span> Cài đặt lại/ Reset</li>
            <li className="mbl"><span className="bg-li">4</span> Cài đặt vị trí và quyền riêng tư/ Reset Location & Privacy</li>
            <li className="mbl"><span className="bg-li">5</span> Nhập mật khẩu điện thoại / Enter passcode</li>
            <li className="mbl"><span className="bg-li">6</span> Cài đặt lại/ Reset setting</li>
          </ul>
        </div>
      )
    }
    else {
      return(
        <div className="mbs padding-l10">
          <ul className="list-unstyle">
            <li className="mbl"><span className="bg-li">1</span> Click icon <i className="fa fa-lock"></i></li>
            <li className="mbl"><span className="bg-li">2</span> Cài đặt trang/ Site Settings</li>
            <li className="mbl"><span className="bg-li">3</span> Vị trí/ Location</li>
            <li className="mbl"><span className="bg-li">4</span> Cho phép/ Allow</li>
          </ul>
        </div>
      )
    }
  }

  renderRemindBanner() {
    const { remind } = this.props.Enviroment;
    const current_user = this.props.user_profile.current_user;
    return(
      <div className="container">
        <BannerRemind
          updatePreviousPage={this.props.updatePreviousPage}
          gaSend={this.props.gaSend}
          changeBannerStatus={this.props.changeBannerStatus}
          updateRemind={this.props.updateRemind}
          data={remind.data}
          remindText={current_user.gender === 'female' ? REMIND_MALE : REMIND_FEMALE}
        />
      </div>
    )
  }

  render() {
    const { search, user_profile } = this.props;
    const { total_elements, total_result, isFetching } = search;
    const enviroment = this.state.mode;
    const asked = this.props.user_profile.current_user.asked;
    const remind = this.props.Enviroment.remind.data || [];
    const scrollToRight= enviroment === 'online' || enviroment === 'default';

    if (ymmStorage.getItem('allowed') && ymmStorage.getItem('allowed').toString() !== 'true' && enviroment === 'nearby' || ymmStorage.getItem('allowed') && ymmStorage.getItem('allowed').toString() == 'true' && enviroment === 'nearby' && !asked) {
      return (
        <div className="site-content">
          <SubMenu mode={enviroment} {...{scrollToRight}}/>
            {(enviroment !== 'photo-stream' && enviroment !== 'nearby' && remind !== 'unload' && remind.length > 0) &&
              this.renderRemindBanner()
            }
          <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3 className="txt-heading">Bật vị trí</h3>
            </div>
          </div>
            <div className="row">
              <div className="col-xs-12 mbl">
                <p className="txt-lg txt-light">
                  Nhất cự ly, nhì tốc độ! Các cụ dạy rồi. <br />
                  Bật tính năng định vị trên trình duyệt của bạn và gặp người mới xung quanh! Biết đâu một nửa của bạn lại ở ngay bên cạnh thì sao <img className="img-small" src={SMILE_FACE} />
                  <img src={MAP_NEARBY_ACTIVE} onClick={this.handleGetLocation}/>
                </p>
              </div>
            </div>
          </div>
          <Modal
            isOpen={this.state.isModalOpen}
            onRequestClose={this.hideModal}
            className="modal__content"
            overlayClassName="modal__overlay modal__overlay--2"
            portalClassName="modal"
            contentLabel=""
          >
            <div className="well well--e txt-center mbm txt-medium">Vui lòng bật truy cập vị trí</div>
            {this.renderModalContent()}
            <div className="mbs">
              <button className="btn txt-center btn--p btn--b" onClick={this.handleShowInstruction}>
                  Xem hướng dẫn chi tiết
              </button>
            </div>
            <button className="modal__btn-close" onClick={this.hideModal}>
              <i className="fa fa-times"></i>
            </button>
          </Modal>
        </div>
      )
    }
    else {
      return (
        <div className="site-content">
          <SubMenu mode={enviroment} {...{scrollToRight}}/>
          {
            (enviroment !== 'photo-stream' && enviroment !== 'nearby' && remind !== 'unload' && remind.length > 0) &&
            this.renderRemindBanner()
          }
          {
            enviroment == 'photo-stream' ?
            <PhotoStream
            {...this.props}
            />
            :
            this.state.error === 'POSITION_UNAVAILABLE' && !ymmStorage.getItem('long') ?
            <p className="txt-lg txt-light">
              Hiện tại hệ thống không thể lấy được vị trí của bạn, xin hãy thử lại sau ít phút!
            </p>
            :
            <InfiniteLoad
              {...{enviroment}}
              {...this.props}
            />
          }
        </div>
      )
    }
  }
}

InfiniteList.propTypes = {
  search: PropTypes.object,
  user_profile: PropTypes.object,
  Enviroment: PropTypes.object,
  // scrollToTop: PropTypes.number,
  updateScrollPossition: PropTypes.func,
  updateViewMode: PropTypes.func,
  updatePagingInformation: PropTypes.func,
  updatePreviousPage: PropTypes.func,
  searchUser: PropTypes.func,
  updateElementHeight: PropTypes.func,
  setProfileUserNextIndex: PropTypes.func,
  simpleFilterClick: PropTypes.func,
  gaSend: PropTypes.func,
}

export default InfiniteList