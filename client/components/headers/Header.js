import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ReactTimeout from 'react-timeout';
import { browserHistory } from 'react-router';
import moment from 'moment';
import showDefaultAvatar from 'src/scripts/showDefaultAvatar';
import SideNav from 'components/SideNav';
import {
  DELAY_TIME_TO_UPDATE_LOCATION, NOTIFICATION_SUCCESS,
  CLICK_NOFI_WHO_ADDED_ME_FAVORITE, CLICK_NOTI_CATEGORY
} from 'constants/Enviroment';
import * as ymmStorage from 'utils/ymmStorage';
import RegularCronJob from 'components/RegularJobs/RegularCronJob';
import {
  getCurrentUser, increaseNotificationNumber, getPeopleMatchedMe,
  getUserLicence
} from 'actions/userProfile';
import { compareIdentifiers, isBlockedUser } from 'utils/common';
import PollingImg from 'components/commons/PollingImg';
import { updateMatchQuestionState } from 'actions/Match_Questions';
import { updateLastUpdate } from 'actions/Enviroment';
import { getPeopleILiked } from 'actions/PeopleILiked';
import {
  ACTIVE_USER, NOTIFICATION_TYPE_WHO_ADD_ME_FAVOURITE
} from 'constants/userProfile';
import { sendClickableNotification } from 'utils/errors/notification';
import { DEFAULT_NOTI_LABEL } from 'constants/TextDefinition';
import { getPeopleLikedMe } from 'pages/people-liked-me/Actions';
import NavLink from './NavLink';
import NavChatButton from './NavChatButton';
import NavLikeButton from './NavLikeButton';

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showSideNav: false,
      notification_number: 0
    }
  }

  componentWillMount() {
    const lastUpdate = this.props.Enviroment.lastUpdate;
    const date = new Date();
    if (date.getTime() - lastUpdate > DELAY_TIME_TO_UPDATE_LOCATION) {
      this.updateLocation();
    }
    this.props.setInterval(this.updateLocation, DELAY_TIME_TO_UPDATE_LOCATION);
  }

  componentDidMount() {
    const { socket } = this.props;
    socket.on('receive_event', this.receive_event.bind(this));
  }

  componentWillUnmount() {
    const { socket } = this.props;
    socket.off('receive_event');
  }

  async receive_event(event) {
    const { user_profile } = this.props;
    const { current_user, blocked_notification, blocked_users, muted_users, unblocked_notification } = user_profile;

    if (event.type == 'force-refresh') {
      location.reload();
    }
    if (event.type == 'reload_user') {
      this.props.getCurrentUser();
    }
    if (current_user.user_status != ACTIVE_USER) {
      return;
    }
    if (event.type == 'match_question_hurdle' &&
        compareIdentifiers(current_user.identifier, event.to_identifier)) {
      this.props.updateMatchQuestionState('isOpenHurdleNotification', true);
      this.props.updateMatchQuestionState('hurdle_matched_answers', event.matched_answers);
    }

    if (event.type == 'add_favorite' &&
        !isBlockedUser(event.from_identifier, blocked_users, muted_users, blocked_notification, unblocked_notification) &&
        compareIdentifiers(current_user.identifier, event.to_identifier)) {
      await this.props.getUserLicence();

      const isPaidUserOrFemale = current_user.user_type == 4 ||
                                                          current_user.gender == 'female';
      const msg = isPaidUserOrFemale ?
                              `${event.from_nick_name} vừa thêm bạn vào danh sách quan tâm!` :
                              'Có người vừa thêm bạn vào danh sách quan tâm!';
      const url = isPaidUserOrFemale ?
                          `/profile/${event.from_identifier}` : '/who-add-favourite';
      const gaTrackDataClickNoti = {
        page_source:CLICK_NOFI_WHO_ADDED_ME_FAVORITE
      }
      const eventActionClickNoti = {
        eventCategory: CLICK_NOTI_CATEGORY,
        eventAction: 'Click',
        eventLabel: CLICK_NOTI_CATEGORY
      }
      const callbackClickNoti = () => this.props.sendGoogleAnalytic(eventActionClickNoti, gaTrackDataClickNoti);

      sendClickableNotification(NOTIFICATION_SUCCESS, DEFAULT_NOTI_LABEL, msg, url, 3000, false, callbackClickNoti);
      this.props.increaseNotificationNumber(NOTIFICATION_TYPE_WHO_ADD_ME_FAVOURITE);
    }
  }

  activeClass() {
    if ([
        '/myprofile', '/upload-image', '/myprofile/update-self-intro',
        '/update/education', '/update/job', '/myprofile/update-hobbies',
        '/fb-album', '/myprofile/update-basic-info'
      ].indexOf(this.props.location.pathname) > -1) {
        return true;
      }
      return false;
  }

  showSideNav = (evt) => {
    evt.preventDefault();
    this.setState({
      showSideNav: true,
    })
  }

  hideSideNav = () => {
    this.setState({
      showSideNav: false,
    })
  }

  updateLocation = () => {
    if (ymmStorage.getItem('allowed')) {
      navigator.geolocation.getCurrentPosition(
        (success) => {
            ymmStorage.setItem('long', success.coords.longitude);
            ymmStorage.setItem('lat', success.coords.latitude);
            this.props.updateLocation(success.coords.longitude, success.coords.latitude);
            const time = new Date();
            this.props.updateLastUpdate(time.getTime());
        },
        (error) => {
          this.props.updateLocation(ymmStorage.getItem('long'), ymmStorage.getItem('lat'));
          const time = new Date();
          this.props.updateLastUpdate(time.getTime());
          // ymmStorage.removeItem('allowed')
        }, options
      );
      var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };
    }
  }
  // temporarily commented
  // resetSearchData() {
  //   this.props.resetSearchData()
  //   browserHistory.push('/search')
  //   sessionStorage.removeItem('scrollTop')
  // }

  render() {
    const { location, user_profile } = this.props;
    const { pathname } = location;
    const { current_user } = user_profile;
    let userAvatar = '';
    const nickName = current_user.nick_name;

    if (current_user.user_pictures && current_user.user_pictures.length && current_user.user_pictures[0].is_main) {
      userAvatar = current_user.user_pictures[0].thumb_image_url;
    } else {
      userAvatar = showDefaultAvatar(current_user.gender, 'thumb');
    }

    return (
      <div>
        <div className="site-header mbm">
          <nav className="nav nav--2 is-fixed">
            <div className="nav__lists">
              <NavLink to="/whathot" pathname={pathname}>
                <div className="nav__icon">
                  <i className="fa fa-heart"></i>
                </div>
                <span className="nav__text">Một nửa</span>
              </NavLink>
              <NavLikeButton {...this.props} />
              <NavChatButton {...this.props} />
              <a
                href="/"
                rel="nofollow"
                className={this.activeClass() ? "nav__link nav__link--2 active" : "nav__link nav__link--2"}
                onClick={this.showSideNav}
              >
                <div className="nav__icon nav__icon--has-img">
                  <PollingImg
                    className="rounded"
                    src={userAvatar}
                    alt={nickName}
                  />
                  {(current_user.notification_number_visitants + current_user.notification_number_added_me_favorite) ? (
                    <div className="nav__addon rounded">
                      {current_user.notification_number_visitants + current_user.notification_number_added_me_favorite}
                    </div>
                  ) : ''}
                </div>
              </a>
            </div>
          </nav>
        </div>
        <SideNav
          {...this.props}
          showSideNav={this.state.showSideNav}
          hideSideNav={this.hideSideNav}
        />
        <RegularCronJob {...this.props} />
      </div>
    )
  }
}

Header.propTypes = {
  location: PropTypes.object.isRequired,
  user_profile: PropTypes.object.isRequired,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
    return {
      getCurrentUser: () => dispatch(getCurrentUser()),
      updateMatchQuestionState: (key, value) => dispatch(updateMatchQuestionState(key, value)),
      getPeopleMatchedMe: () => {
        dispatch(getPeopleMatchedMe()).then(() => {
          dispatch(getPeopleILiked()).then((response) => {
          })
        }).catch(() => {})
      },
      updateLastUpdate: (time) => {
        dispatch(updateLastUpdate(time))
      },
      increaseNotificationNumber: (notification_type, reset=false, notification_num = 0) => {
        dispatch(increaseNotificationNumber(notification_type, reset, notification_num))
      },
      getUserLicence: async () => {
         await dispatch(getUserLicence());
      },
      getPeopleLikedMe: (pageIndex) => (
        dispatch(getPeopleLikedMe(pageIndex))
      ),
    }
}

const HeaderContainer = connect(mapStateToProps, mapDispachToProps)(Header);

export default ReactTimeout(HeaderContainer);
