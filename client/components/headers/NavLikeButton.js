import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {
  compareIdentifiers, isBlockedUser, inLikedList, inVisitList, getIdentifierFromId
} from 'utils/common';
import { gaSend, updatePushNotiModal, getRemind } from 'actions/Enviroment';
import {
  updateVotedUser, peopleLikedMeRealtime, visitantRealtime,
  increaseVisitantNumber
} from 'actions/userProfile';
import {
  GA_ACTION_OPEN_LIKE, SEARCH_PAGE, PUSH_NOTI_MODAL_FOOTPRINT,
  NOTIFICATION_SUCCESS, CLICK_NOTI_FOOTPRINT, CLICK_NOFI_LIKE,
  CLICK_NOTI_VOTE_PHOTO, CLICK_NOTI_CATEGORY
} from 'constants/Enviroment';
import { NOTIFICATION_TYPE_LIKE } from 'constants/userProfile';
import { updateFirstLikeModal, updateMatchModal } from 'actions/Like';
import { updateDetailRandomMatch, deactiveRandomMatch } from 'actions/random_match';
import * as ymmStorage from 'utils/ymmStorage';
import { ACTIVE_USER } from 'constants/userProfile';
import { sendClickableNotification } from 'utils/errors/notification';
import { DEFAULT_NOTI_LABEL } from 'constants/TextDefinition';

class NavLikeButton extends React.Component {

  showNotification(user) {
    const { user_profile, location } = this.props;
    const { current_user, blocked_notification, blocked_users, muted_users, unblocked_notification } = user_profile;
    const { identifier } = current_user;

    if(ymmStorage.getItem('ymm_token') && !isBlockedUser(user.like_from, blocked_users, muted_users, blocked_notification, unblocked_notification)) {
      this.props.peopleLikedMeRealtime(user.like_from);
      let likeMessage = '';
      if (user.message) {
        likeMessage = ` "${user.message ? user.message : ''}"`;
      }
      if(user.have_sent_like_to_me){
        this.props.updateMatchModal(true, user);
      } else {
        const url = `/profile/${user.like_from}`;
        const msg = `${user.nick_name} vừa mới thích bạn ${likeMessage}`;
        const callback = this.sendGaClickNoti(CLICK_NOFI_LIKE);

        sendClickableNotification(NOTIFICATION_SUCCESS, DEFAULT_NOTI_LABEL, msg, url, 3000, false, callback);
        this.props.increaseNotificationNumber(NOTIFICATION_TYPE_LIKE);
      }
      this.props.updateReducerState(SEARCH_PAGE , user.like_from, 'have_sent_like_to_me', true);
      //reload the people like  me page on people-like-me only
      if(location.pathname == '/people-like-me') {
        this.props.getPeopleLikedMe(1);
      }
    }
  }

  show_view_profile_notification(identifier, user) {
    const { user_profile, people_liked_me } = this.props;
    const { current_user, visitants, visitant_real_time, blocked_notification, blocked_users, muted_users, unblocked_notification, people_liked_me_real_time, matched_users } = user_profile;


    if(ymmStorage.getItem('ymm_token') && !inLikedList(user.identifier, people_liked_me.users, people_liked_me_real_time) &&
      !isBlockedUser(user.identifier, blocked_users, muted_users, blocked_notification, unblocked_notification) &&
      compareIdentifiers(identifier, current_user.identifier)) {
      const url = `/profile/${user.identifier}`;
      const msg = `${user.nick_name} vừa mới xem hồ sơ của bạn!`;
      const callback = this.sendGaClickNoti(CLICK_NOTI_FOOTPRINT);

      sendClickableNotification(NOTIFICATION_SUCCESS, DEFAULT_NOTI_LABEL, msg, url, 3000, false, callback);
      if (!inVisitList(user.identifier, visitants, visitant_real_time)) {
        this.props.visitantRealtime(user.identifier);
        this.props.increaseVisitantNumber();
      }
      if (this.props.user_profile.visitants.length == 0 && people_liked_me.users.length == 0 && matched_users.length == 0) {
        if (typeof Notification != 'undefined' && Notification !== undefined && Notification.permission !== 'granted') {
          this.props.user_profile.visitants = this.props.user_profile.visitants.concat(user);
          this.props.updatePushNotiModal(true, PUSH_NOTI_MODAL_FOOTPRINT);
        }
      }
    }
  }

  sendGaClickNoti(gaPageSource) {
    const gaTrackDataClickNoti = {
      page_source: gaPageSource
    }
    const eventActionClickNoti = {
      eventCategory: CLICK_NOTI_CATEGORY,
      eventAction: 'Click',
      eventLabel: CLICK_NOTI_CATEGORY
    }
    return () => this.props.sendGoogleAnalytic(eventActionClickNoti, gaTrackDataClickNoti);
  }

  sendGaEvent(){
    const ga_track_data = {};
    this.props.gaSend(GA_ACTION_OPEN_LIKE, ga_track_data);
  }

  handleClick = () => {
      if(this.props.user_profile.current_user.notification_number_likes) {
        this.sendGaEvent();
      }
      this.props.clearNotification();
      var resetNotification = true;
      this.props.increaseNotificationNumber(NOTIFICATION_TYPE_LIKE, resetNotification);
      this.setState({date: new Date().getTime()});
  }

  showVoteNotification(vote) {
    const { user_profile } = this.props;
    const { current_user } = user_profile;
    const { identifier } = current_user;

    if (compareIdentifiers(identifier, vote.vote_to_id)) {
      this.props.updateVotedUser(vote.picture_id, vote.vote_from_id, vote.vote_type);
      if (vote.vote_type == 1) {
        const url = `/profile/${getIdentifierFromId(vote.vote_from_id)}`;
        const msg = `${vote.vote_from_name} vừa thích ảnh của bạn`;
        const callback = this.sendGaClickNoti(CLICK_NOTI_VOTE_PHOTO);

        sendClickableNotification(NOTIFICATION_SUCCESS, DEFAULT_NOTI_LABEL, msg, url, 3000, false, callback);
      }
    }
  }

  componentWillMount() {
    const { location, user_profile } = this.props;
    const { pathname } = location;
    const { current_user } = user_profile;
    const { notification_number_likes } = current_user;
    if(pathname == '/people-like-me') {
      this.props.clearNotification();
    }

    const socket = this.props.socket;
    socket.on('receive_event', this.receive_event.bind(this));
    // socket.on('get_remind', this.getRemindData.bind(this));
  }

  componentWillUnmount() {
    const socket = this.props.socket;
    socket.off('receive_event');
  }
  receive_event(data) {
    const { user_profile } = this.props
    const { current_user } = user_profile;
    if (current_user.user_status != ACTIVE_USER) {
      return;
    }
    switch(data.type) {
      case 'show_like_notification':
        this.showNotification(data);
      break;
      case 'show_view_profile_notification':
      case 'fake_visitant_notification':
        this.show_view_profile_notification(data.room, data);
      break;
      case 'show_vote_notification':
        this.showVoteNotification(data);
      break;
      case 'show_random_match':
        this.showRandomMatch(data);
      break;
      case 'get_remind':
        this.getRemindData(data);
      break;
      case 'show_super_like_spinner_notification':
        this.showSuperLikeSpinnerNotification(data);
        break;
      default:
        break;
    }
  }

  showSuperLikeSpinnerNotification(data) {
    const user = data.super_like_spinner_user;
    const message = `Cling! Mũi tên Cupid đã bắn trúng bạn và ${user.nick_name}. Trò chuyện ngay và luôn nào!`;
    const url = `/profile/${user.identifier}`;
    sendClickableNotification(NOTIFICATION_SUCCESS, DEFAULT_NOTI_LABEL, message, url, 3000, false, null);
  }

  showRandomMatch(data) {
    const { user_profile } = this.props;
    const { current_user } = user_profile;
    this.props.getPeopleMatchedMe(); // get user who have just matched with me to chat
    this.props.updateDetailRandomMatch(data.random_match_info);
    this.props.deactiveRandomMatch(data.random_match_info.identifier);
  }

  getRemindData(data) {
    const { user_profile } = this.props;
    const { current_user } = user_profile;
    if (data.remind_data.findIndex(user => compareIdentifiers(user.identifier, current_user.identifier)) > -1) {
      this.props.getRemind();
    }
  }

  render() {
    const { current_user } = this.props.user_profile
    const { notification_number_likes } = current_user
    return (
      <Link to="/people-like-me" className='nav__link nav__link--2' activeClassName='active' onClick={this.handleClick}>
        <div className="nav__icon">
          <i className="fa fa-thumbs-up"></i>
          { notification_number_likes > 0 && <div className="nav__addon" >{notification_number_likes}</div> }
        </div>
        <span className="nav__text">Ai thích tôi</span>
      </Link>
    )
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
    return {
      gaSend: (eventAction, ga_track_data) =>{
        dispatch(gaSend(eventAction, ga_track_data))
      },
      updateFirstLikeModal: (isOpen = false, firstLikeSent = false, firstLikeReceived = false, firstLikeInfo = {}) => {
        dispatch(updateFirstLikeModal(isOpen, firstLikeSent, firstLikeReceived, firstLikeInfo));
      },
      updateVotedUser: (picture_id, vote_user_id, vote_type) => {
        dispatch(updateVotedUser(picture_id, vote_user_id, vote_type));
      },
      updatePushNotiModal: (isOpen, modalType, nick_name) => {
        dispatch(updatePushNotiModal(isOpen, modalType, nick_name))
      },
      updateDetailRandomMatch: (random_match) => dispatch(updateDetailRandomMatch(random_match)),
      deactiveRandomMatch: (user_identifier) => {
        dispatch(deactiveRandomMatch(user_identifier));
      },
      getRemind: () => {
        dispatch(getRemind())
      },
      peopleLikedMeRealtime: (identifier) => {
        dispatch(peopleLikedMeRealtime(identifier))
      },
      visitantRealtime: (identifier) => {
        dispatch(visitantRealtime(identifier))
      },
      increaseVisitantNumber: () => {
        dispatch(increaseVisitantNumber())
      },
      updateMatchModal: (isMatchModalOpen, matchInfo) => {
        dispatch(updateMatchModal(isMatchModalOpen, matchInfo))
      },
    }
}
const NavLikeButtonContainer = connect(mapStateToProps, mapDispachToProps)(NavLikeButton);

export default NavLikeButtonContainer;
