import React, { Component } from 'react';
import { Link } from 'react-router';
import Headroom from '../commons/Headroom';
import * as ymmStorage from '../../utils/ymmStorage';

class SubMenu extends Component {

  componentDidMount() {
    this.enableMouseWheelToScroll();
    this.handleClick();
    this.scrollToRight(this.props.scrollToRight);
  }

  componentWillReceiveProps(nextProps){
    this.scrollToRight(nextProps.scrollToRight);
  }

  componentWillUnmount() {
    const hsdiv = document.querySelectorAll('.hsdiv');
    const arrayHsdiv = Array.from(hsdiv);
    arrayHsdiv.map((element) => element.removeEventListener('wheel', this.horwheel, false));
  }

  handleClick() {
    const sub = this.horizontal.querySelectorAll('.sub-menu__link');
    const average = Math.floor(sub.length/2);
    for (let [index, value] of sub.entries()) {
      if (index <= average) {
        value.addEventListener('click', () => this.horizontal.scrollLeft = -1, false);
      }
      else {
        value.addEventListener('click', () => this.horizontal.scrollLeft = 800, false);
      }
    }
  }

  scrollToRight(val) {
    const horizontal = document.querySelector('.horizontal');
    horizontal.scrollLeft = val ? 800 : -1;
  }

  horwheel(e) {
    e.preventDefault();
    e.currentTarget.scrollLeft += e.deltaY;
  }

  enableMouseWheelToScroll() {
    const hsdiv = document.querySelectorAll('.hsdiv');
    const arrayHsdiv = Array.from(hsdiv);
    arrayHsdiv.map((element) => {
      element.addEventListener('wheel', this.horwheel, false)
    })
  }

  render() {
    const mode = this.props.mode
    const searchmode = ymmStorage.getItem('searchmode')
    return (
      <Headroom classHr={"site-fixed-position mbm mbt submenu-component"}>
        <div
          className="hsdiv horizontal sub-menu"
          ref={(ref) => {
            this.horizontal = ref
          }}
        >
          <Link to='/whathot' className={mode == 'WhatHot' ? 'sub-menu__link active' : 'sub-menu__link'}>HOT</Link>
          <Link to="/similar-face-to-me" className={mode == 'similar-face' ? 'sub-menu__link active' : 'sub-menu__link'}>BẢN SAO</Link>
          <Link to="/search/photo-stream" className={mode == 'photo-stream' ? 'sub-menu__link active' : 'sub-menu__link'}>Ảnh mới</Link>
          <Link to="/search/online" className={mode == 'online' ? 'sub-menu__link active' : 'sub-menu__link'}>Online</Link>
          {/*<Link to="/search/harmony_age"  className={mode == 'harmony_age' || mode == 'harmony_fate' || mode == 'harmony_horoscope' ? 'sub-menu__link active' : 'sub-menu__link'}>Hợp nhau</Link>
          <Link to="/similar-face" className={mode == 'nearby' ? 'sub-menu__link active' : 'sub-menu__link'}>Gần tôi</Link>*/}
{/*       <Link to="/search/matching_ratio" className={mode == 'matching_ratio' ? 'sub-menu__link active' : 'sub-menu__link'}>Hợp gu</Link>*/}
          <Link to={searchmode == 1 ? '/search/default' : '/search-condition'} className={mode == 'default' ? 'sub-menu__link active' : 'sub-menu__link'}>Tìm kiếm</Link>
        </div>
      </Headroom>
    )
  }
}

export default SubMenu;
