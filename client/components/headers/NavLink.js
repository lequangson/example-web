import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const NavLink = ({ to, pathname, children, idHtml }) => {
  let active_class = false
  const searchLogicalNames = [
    '/search/default',
    '/search/new',
    '/search/nearby',
    '/search/online',
    '/search/matching_ratio',
    '/search-condition',
    '/search/photo-stream',
    '/search/harmony_age',
    '/search/harmony_fate',
    '/search/harmony_horoscope',
    '/whathot',
    '/similar-face-to-me',
    '/similar-face-to-my-fb-friends',
    '/similar-face-to-my-ymm-friends',
    '/similar-face-to-my-photos',
  ];

  switch (to) {
    case '/whathot':
      if (searchLogicalNames.indexOf(pathname) > -1) {
        active_class = true
      }
      break
    case '/myprofile':
      if ([
        '/myprofile', '/upload-image', '/myprofile/update-self-intro',
        '/update/education', '/update/job', '/myprofile/update-hobbies',
        '/fb-album',
      ].indexOf(pathname) > -1) {
        active_class = true
      }
      break
    default:
      break
  }

  return (
    <Link
      to={to}
      className={`nav__link nav__link--2 ${active_class && 'active'}`}
    >{children}</Link>
  )
}

NavLink.propTypes = {
  to: PropTypes.string.isRequired,
  pathname: PropTypes.string.isRequired,
  children: PropTypes.array.isRequired,
  // onClick: PropTypes.func,
}

export default NavLink
