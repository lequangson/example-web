import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import ReactTimeout from 'react-timeout';
import $ from 'jquery';
import _ from 'lodash';
import { compareIdentifiers } from 'utils/common';
import ChatGuideModal from 'components/modals/ChatGuideModal';
import {
  CHAT_SERVER_URL,
  NOTIFICATION_INFOR,
  DELAY_TIME_TO_CHECK_NOTIFICATION,
  CLICK_NOTI_CHAT, CLICK_NOTI_CATEGORY
} from 'constants/Enviroment';
import { NOTIFICATION_TYPE_CHAT, NOTIFICATION_TYPE_CHAT_APP } from 'constants/userProfile';
import { sendClickableNotification } from 'utils/errors/notification';
import {
  CANNOT_START_CHAT_BECAUSE_NO_MATCHED_FOUND_MSG,
  DEFAULT_NOTI_LABEL
} from 'constants/TextDefinition';
import { getNotificationMessages } from 'actions/ChatMessage';
import { updateNotificationMessageForMatches } from 'actions/userProfile';
import * as ymmStorage from 'utils/ymmStorage';
import { ACTIVE_USER } from 'constants/userProfile';
import { sendGoogleAnalytic } from 'actions/Enviroment'

class NavChatButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      needStartChart: false
    }
  }

  componentWillMount() {
    const socket = this.props.socket;
    // every 5 minutes, call api to detect if there is any new chat message
    // this.props.setInterval(this.showChatNotification, DELAY_TIME_TO_CHECK_NOTIFICATION)
    this.props.setInterval(this.showChatNotification, DELAY_TIME_TO_CHECK_NOTIFICATION);
    socket.on('receive_event', this.receive_event.bind(this));
    // We need to know is there any people who has matched with this user yet?
    if (this.props.user_profile.matched_users.length === 0 && !this.props.user_profile.haveLoadedMatch) {
      this.props.getPeopleMatchedMe();
    }
  }

  componentWillUnmount() {
    const socket = this.props.socket;
    socket.off('receive_event');
  }

  receive_event(data) {
    if (data.type == 'show_chat_notification') {
      this._showChatNotification(data);
    }
  }

  componentDidUpdate() {
    if (typeof Notification != 'undefined' && Notification !== undefined &&
        Notification.permission !== ymmStorage.getItem('mobile_notification_permission')) {
      ymmStorage.setItem('mobile_notification_permission', Notification.permission);
    }
  }

  handleClick = () => {
    this.props.clearNotification(NOTIFICATION_TYPE_CHAT);
    const resetNotification = true;
    this.props.increaseNotificationNumber(NOTIFICATION_TYPE_CHAT, resetNotification);
    this.setState({ date: new Date().getTime() })// force reload component
/*    if (this.props.user_profile.matched_users.length === 0) {
      // If do not find out this user in matched user list
      // --> We need to reload matched users list for getting chat_session_id
      this.props.getPeopleMatchedMe();
      this.setState({ needStartChart: true });
      return;
    }
    this.openChatWindow()*/
  }

  _showChatNotification(user) {
    if (window.location.href.indexOf('/profile/') !== -1) {
      return;
    }
    const { user_profile } = this.props;
    const { current_user } = user_profile;
    const { identifier, user_status } = current_user;
    if (user_status != ACTIVE_USER) {
      return;
    }
    if (compareIdentifiers(identifier, user.like_to) || compareIdentifiers(identifier, user.like_from)) {
      this.props.increaseNotificationNumber(NOTIFICATION_TYPE_CHAT);
    }
  }

  showChatNotification = () => { // this function run every 5 minues (in config) to check number of new message and inform to user
    const { total_unread_message, matched_users } = this.props.user_profile;
    this.props.getNotificationMessages(total_unread_message, matched_users);
  }

  fireOpenChatEvent() {
    if (this.state.needStartChart &&
        !this.props.user_profile.isLoadingMatchedUser) {
      this.openChatWindow();
    }
  }

  openChatWindow() {
    if (this.props.user_profile.matched_users.length === 0) {
      notification(NOTIFICATION_INFOR, CANNOT_START_CHAT_BECAUSE_NO_MATCHED_FOUND_MSG);
      this.setState({ needStartChart: false });
      return;
    }
    const matchedUser = this.props.user_profile.matched_users[0];
    window.open(`${CHAT_SERVER_URL}t/${matchedUser.chat_session_id}/${matchedUser.user_fb_id}`);
    this.setState({ needStartChart: false });
  }

  render() {
    const { current_user, number_chat_room_has_message } = this.props.user_profile;
    const { notification_number_messages } = current_user;
    let total_chat_notification = number_chat_room_has_message;
    const { pathname } = this.props.location
    return (
      <Link to="/chat-list" activeClassName="active" id="tooltip" className={`${_.startsWith(pathname, '/chat-list') ? 'active ' : ''}nav__link nav__link--2 tooltip`} onClick={this.handleClick} >
        <div className="nav__icon">
          <i className="fa fa-comments"></i>
          {total_chat_notification ? (
            <div className="nav__addon" id="notification-no">
              {total_chat_notification}
            </div>
          ) : ''}
        </div>
        <span className="nav__text">Trò chuyện</span>
      </Link>
    )
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    getNotificationMessages: (current_total_unread_message, matched_users) => {
      dispatch(getNotificationMessages()).then((response) => {
        const data = response.payload.data.data;
        if (data) {
          dispatch(updateNotificationMessageForMatches(data))
          let total_unread_messages = 0;
          matched_users.map((user) => {
            const new_message_number = data.filter(
                item => compareIdentifiers(item.user_identifier, user.identifier)
            )
            .map(item2 => item2.number_new_message).reduce((a, b) => a + b, 0);
            total_unread_messages += new_message_number;
            return user;
          })
          // const total_unread_messages = data.map(item => item.number_new_message).reduce((a, b) => a + b, 0)
          if (total_unread_messages > current_total_unread_message) {
            const new_message_number = total_unread_messages - current_total_unread_message;
            const msg = `Bạn có ${new_message_number} tin nhắn mới.`;
            const url = '/chat';
            const gaTrackDataClickNoti = {
              page_source:CLICK_NOTI_CHAT
            }
            const eventActionClickNoti = {
              eventCategory: CLICK_NOTI_CATEGORY,
              eventAction: 'Click',
              eventLabel: CLICK_NOTI_CATEGORY
            }
            const callbackClickNoti = () => dispatch(sendGoogleAnalytic(eventActionClickNoti, gaTrackDataClickNoti));

            sendClickableNotification(NOTIFICATION_INFOR, DEFAULT_NOTI_LABEL, msg, url, 3000, false, callbackClickNoti);
          }
        }
      });
    }
  }
}

NavChatButton.propTypes = {
  user_profile: PropTypes.object,
  socket: PropTypes.object,
  getPeopleMatchedMe: PropTypes.func,
  clearNotification: PropTypes.func,
  increaseNotificationNumber: PropTypes.func,
}

const NavChatButtonContainer = connect(mapStateToProps, mapDispachToProps)(NavChatButton);

export default ReactTimeout(NavChatButtonContainer);
