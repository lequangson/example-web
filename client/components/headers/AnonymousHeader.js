import React from 'react'
import logo2 from '../../../public/images/logo-2.png'

class AnonymousHeader extends React.Component {

  render() {
    return (
      <div className="m-container">
        <div className="row">
          <div className="col-xs-12">
            <div className="well well--sm txt-center">
              <img src={logo2} className="mbs" width="118" alt="logo" />
              <p className="m-txt-blue">Hẹn hò an toàn cho phụ nữ</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default AnonymousHeader
