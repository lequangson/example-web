import React, { PropTypes } from 'react';
import _ from 'lodash'
import $ from 'jquery'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { browserHistory } from 'react-router'
import ProfileNavigation from './Profile/ProfileNavigation'
import ProfilePictures from './Profile/ProfilePictures'
import ProfileInformation from './Profile/ProfileInformation'
import ProfileLikeBanner from './Profile/ProfileLikeBanner'
import Description from './Profile/Description'
import showDefaultAvatar from '../src/scripts/showDefaultAvatar'
import SilderCommon from 'components/commons/SilderCommon'
import showOnlineStatus from 'scripts/showStatus'
import SameFbLike from './Profile/SameFbLike'
import UserVerification from './Profile/UserVerification'
import {
  SEARCH_PAGE, PEOPLE_LIKED_ME_PAGE, FAVORITE_PAGE, FOOTPRINT_PAGE, MATCHED_PAGE,
  ACTIVITY_VIEW_PROFILE, BLOCK_USER_BLOCKED_PAGE, BLOCK_USER_MUTED_PAGE,
  OTHER_PROFILE_PAGE, REMIND_BANNER, RECOMMENDATION_PAGE,
  EMAIL_TYPE_LIKE_NOTIFICATION, GA_ACTION_VIEW_OTHER_PROFILE, WHAT_HOT_PAGE,
} from '../constants/Enviroment'
import { ACCESS_DENIED_ERROR_MSG } from '../constants/TextDefinition'
import { SEARCH_PAGE_SIZE } from '../constants/search'
import { BLOCK_TYPE_BLOCKED, BLOCK_TYPE_MUTED } from '../constants/userProfile'
import { isBlockedUser, getUserByIdentifierFromAppState, getUserByIndex, getUserIndex } from 'utils/common'
import { isNormalOtherUser } from '../utils/UserBussinessUtils'
import HeaderContainer from '../containers/HeaderContainer'
import RemindUploadAvatarModal from './modals/RemindUploadAvatarModal'
import * as ymmStorage from '../utils/ymmStorage';
import GoogleOptimize from '../utils/google_analytic/google_optimize';
import { ACTIVE_USER } from 'constants/userProfile';
import GiftForUsers from 'pages/campaign/GiftForUsers';

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      carouselOffsetBottom: undefined,
      likeBannerOffset: undefined,
      needReloadUserProfile: false,
      needSendGA: true,
      isAvatarModalOpen: false,
    }
    this.setCarouselOffsetBottom = this.setCarouselOffsetBottom.bind(this)
    this.showAvatarModal = this.showAvatarModal.bind(this)
    this.hideAvatarModal = this.hideAvatarModal.bind(this)
  }

  componentWillMount() {
    let viewPageSource = this.props.Enviroment.previous_page;
    // Reset Next index when user open Profile
    this.props.setProfileUserNextIndex(0);
    if (this.isNeedToReloadUserProfile()) { // in case user refresh browser
      this.props.getUserProfileByIdentifier(this.props.identifier);
      viewPageSource = OTHER_PROFILE_PAGE;
      this.setState({ needReloadUserProfile: true })
      /*switch (this.props.Enviroment.previous_page) {
        case SEARCH_PAGE: {
          const current_page = ymmStorage.getItem('current_page')
          const page_size = current_page ? current_page * SEARCH_PAGE_SIZE : SEARCH_PAGE_SIZE
          this.props.searchUser(1, page_size)
          break;
        }
        case FAVORITE_PAGE:
          this.props.getFavorite();
          break;
        case FOOTPRINT_PAGE:
          this.props.getVisitant();
          break;
        case BLOCK_USER_BLOCKED_PAGE:
          this.props.getBlockedUsers(BLOCK_TYPE_BLOCKED);
          break;
        case BLOCK_USER_MUTED_PAGE:
          this.props.getBlockedUsers(BLOCK_TYPE_MUTED);
          break;
        case MATCHED_PAGE:
          this.props.getPeopleMatchedMe();
          break;
        default:
          break;
      }*/
    }
    const { pageSource } = this.props.location ? this.props.location.query : ''
    if (pageSource) {
      viewPageSource = pageSource
    }
    this.props.setViewProfileSource(viewPageSource);
    // Add to activity audit
    this.props.addActivityAudit(ACTIVITY_VIEW_PROFILE, this.props.identifier, null, null);
    ymmStorage.setItem('isNeedSendGA', true)
    const user = this.getProfileUser();
    if (!_.isEmpty(user) && ymmStorage.getItem('isNeedSendGA')) {
      this.handleSendGA()
      this.remindUploadAvatar(this.props.user_profile.current_user.user_pictures)
      ymmStorage.removeItem('isNeedSendGA')
    }
    this.sendViewProfileNotification()
  }

  @GoogleOptimize
  componentDidMount() {
    window.scroll(0, 0)
  }

  getPageSource() {
    if (this.props.Enviroment.previous_page === WHAT_HOT_PAGE) {
      const user = this.props.WhatHot.users[this.props.WhatHot.selectedUserIndex];
      if (!_.isEmpty(user) && user.is_recommendation) {
        return RECOMMENDATION_PAGE;
      }
      else {
        return WHAT_HOT_PAGE;
      }
    }
    if (this.props.Enviroment.view_profile_source !== '') {
      return this.props.Enviroment.view_profile_source;
    }
    return this.props.Enviroment.previous_page;
  }

  componentDidUpdate() {
    if (!this.validateUser()) {
      browserHistory.replace('/404')
    }
    const user = this.getProfileUser();
    if (!_.isEmpty(this.props.user_profile.current_user) && !_.isEmpty(user) && ymmStorage.getItem('isNeedSendGA')) {

      this.props.addActivityAudit(ACTIVITY_VIEW_PROFILE, this.props.identifier, null, null);
      this.sendViewProfileNotification()
      window.scroll(0, 0)
      this.handleSendGA()
      ymmStorage.removeItem('isNeedSendGA')
      this.remindUploadAvatar(this.props.user_profile.current_user.user_pictures)
    }

  }

  componentWillUnmount() {
    this.props.setViewProfileSource('');
  }

  setCarouselOffsetBottom(value) {
    this.setState({
      carouselOffsetBottom: value,
    })
  }

  getProfileUser() {
    let user = getUserByIdentifierFromAppState(this.props.params.identifier, this.props, this.props.search.search_mode);
    if (!user || user.length == 0) {
      user = this.props.user_profile.selected_user;
    }
    return user;
  }

  handleSendGA() {
    const user = this.getProfileUser();
    const ga_track_data = {
      partner_residence: user.residence ? user.residence.value : '',
      partner_birth_place: user.birth_place ? user.birth_place.value : '',
      page_source: this.getPageSource(),
      partner_age: user.age,
      partner_identifier: user.identifier,
    };
    this.props.gaSend(GA_ACTION_VIEW_OTHER_PROFILE, ga_track_data);
  }

  sendViewProfileNotification() {
    const { user_profile } = this.props
    const {
      current_user,
      blocked_notification,
      unblocked_notification,
      people_liked_me_real_time,
    } = user_profile
    const user = this.getProfileUser();
    if (!_.isEmpty(current_user) && !_.isEmpty(user)) {
      const isInBlockedList = isBlockedUser(
        this.props.params.identifier,
        [], [],
        blocked_notification,
        unblocked_notification
      )

      if (
        !isInBlockedList &&
        !user.block_me_type &&
        !user.block_type &&
        current_user.user_status == ACTIVE_USER
      ) {
        const socket = this.props.socket;
        const userPicture = current_user && current_user.user_pictures && current_user.user_pictures.length > 0 ?
          current_user.user_pictures[0].small_image_url : '';
        socket.emit(
          'view_profile',
          this.props.params.identifier,
          { nick_name: current_user.nick_name, identifier: current_user.identifier, room: this.props.params.identifier, avatar: userPicture });
      }
    }
  }

  validateUser() {
    const user = this.getProfileUser();
    if (user !== null && user.length !== 0) {
      return isNormalOtherUser(user);
    }
    // Is loading
    if (this.state.needReloadUserProfile && this.props.user_profile.isFetching_Selected_User) {
      return true;
    }
    return false;
  }

  isNeedToReloadUserProfile() {
    const user = getUserByIdentifierFromAppState(this.props.params.identifier, this.props, this.props.search.search_mode)
    if (!_.isEmpty(user)) {
      return false
    }
    return true;
  }

  remindUploadAvatar(userPictures) {
    const isUploadingAvatar = this.props.user_profile.isUploadingAvatar
    if (!userPictures) {
      return
    }

    if (((userPictures.length === 0) || (!userPictures[0].is_main)) && !isUploadingAvatar) {
      this.showAvatarModal()
    }
  }

  renderNewUserIcon(user) {
    const isNewUser = user ? user.is_new_user : false
    if (!isNewUser) {
      return ''
    }
    return (
      <div>
        <div className="card__addon-2">
          <div className="card__addon-2__title">Mới!</div>
        </div>
        <div className="card__pad-2"></div>
      </div>
    )
  }

  showAvatarModal() {
    this.setState({ isAvatarModalOpen: true })
  }

  hideAvatarModal() {
    this.setState({ isAvatarModalOpen: false })
  }
  // typeUser is next or preview index user
  renderPreLoadUser(typeUser) {
console.log('renderPreLoadUser', typeUser, this.props)
    const { user_profile, search, dictionaries, Enviroment, PeopleILiked, campaign } = this.props
    const mode = search.search_mode
    const user = this.getProfileUser();
    const index = getUserIndex({...this.props, data: search[mode] ? search[mode].data : [], user, enviroment: Enviroment})
    const preLoadUser = getUserByIndex(index + typeUser, {...this.props, data: search[mode] ? search[mode].data : [], user, enviroment: Enviroment})

    if (_.isEmpty(preLoadUser) || _.isEmpty(user_profile.current_user)) {
      return <div></div>
    }

    const score = preLoadUser.verification_status ? preLoadUser.verification_status.score : 0;
    return (
      <div className={`profile-slide-item ${typeUser == 1? 'first' : 'last'}`}>
        <div className="container padding-t10">
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 user-main-carousel">
              <article className="card mbm">
                <div className="card__upper">
                  <div className="card__link">
                    <div className="row">
                      {
                        (typeof preLoadUser.user_pictures !== 'undefined' && preLoadUser.user_pictures.length > 0) ?
                          <div>
                            <picture>
                              <source
                                srcSet={preLoadUser.user_pictures[0].extra_large_image_url}
                                media="(min-resolution: 2dppx)"
                                alt={user.nick_name}
                              />
                              <source
                                srcSet={preLoadUser.user_pictures[0].extra_large_image_url}
                                media="(min-width: 1200px) and (min-resolution: 1dppx)"
                                alt={user.nick_name}
                              />
                              <img src={preLoadUser.user_pictures[0].large_image_url} alt={preLoadUser.nick_name} />
                            </picture>
                          </div>
                        : (
                          <div>
                            <img src={showDefaultAvatar(preLoadUser.gender, 'large')} alt={preLoadUser.nick_name} />
                          </div>
                        )
                      }
                    </div>
                    <div className="card__meta card__meta--1">
                      <span className={`dot dot--${showOnlineStatus(preLoadUser.online_status)} mrs`}></span>
                      <span className="txt-bold txt-lg">{`${preLoadUser.nick_name} - `}</span>
                      <span className="txt-bold txt-lg">
                        {preLoadUser.age
                          ? preLoadUser.age.toString()
                          : '?'
                        }
                      </span>
                      <span className="txt-lg">
                        {preLoadUser.residence.value
                          ? ` - ${preLoadUser.residence.value}`
                          : ''
                        }
                      </span>
                      <div>
                        <i className="fa fa-image"></i> {preLoadUser.user_pictures.length} ảnh
                      </div>
                    </div>

                    <div className="card__addon">
                      <i className="fa fa-heart"></i>
                      <div className="txt-bold">{preLoadUser.compatibility}%</div>
                    </div>
                    <div className="card__pad"></div>
                    {this.renderNewUserIcon(preLoadUser)}
                  </div>
                </div>
              </article>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 user-sub-carousel">
              <div className="row mbm">
                <SilderCommon
                  carouselName={`carousel-nav-${preLoadUser.identifier}`}
                  mobileShowItem={5}
                  numberShowItem={5}>
                {
                  (typeof preLoadUser.user_pictures !== 'undefined' && preLoadUser.user_pictures.length > 0)
                  ? preLoadUser.user_pictures.map((picture, i) => (
                      <div key={i}>
                        <img src={picture.thumb_image_url} alt={preLoadUser.nick_name} />
                      </div>
                    ))
                  : <div>
                      <img src={showDefaultAvatar(preLoadUser.gender)} alt={preLoadUser.nick_name} />
                    </div>
                }
                </SilderCommon>
              </div>
            </div>
          </div>
        </div>
        <ProfileNavigation user={preLoadUser} enviroment={Enviroment} {...this.props} />
        {!_.isEmpty(campaign.data) &&
          <GiftForUsers
            current_user={user_profile.current_user}
            user={preLoadUser}
            openModal={this.openModal}
          />
        }
        <UserVerification score={score} />
        <Description user={preLoadUser} />
        <ProfileInformation user={preLoadUser} openModal={this.openModal} mode="read" dictionaries={dictionaries} current_user={user_profile.current_user} {...this.props} />
        <ProfileLikeBanner
          target={this.state.carouselOffsetBottom}
          search={search}
          enviroment={Enviroment}
          user={preLoadUser}
          needScrollToPosition={this.props.needScrollToPosition}
          current_user={user_profile.current_user}
        />
      </div>
    )
  }

  render() {
    const {
      user_profile, search, dictionaries,  Enviroment,
      PeopleILiked, campaign, people_liked_me
    } = this.props
    const mode = search.search_mode
    const user = this.getProfileUser();
    if (_.isEmpty(user) || _.isEmpty(user_profile.current_user)) {
      return <div>Loading...</div>
    }
    const score = user.verification_status ? user.verification_status.score : 0;

    return (
      <div className="site-content">
        <HeaderContainer {...this.props} />
        <div className="profile-slide slide slide-appear">

          {this.renderPreLoadUser(1)}
          <div className="profile-slide-item">
            <ProfilePictures
              setCarouselOffsetBottom={(value) => {
                this.setCarouselOffsetBottom(value)
              }}
              gaSend={this.props.gaSend}
              updateUploadPicturePageSource={this.props.updateUploadPicturePageSource}
              user={user}
              data={search[mode] ? search[mode].data : []}
              validateImage={this.props.validateImage}
              saveImage={this.props.saveImage}
              enviroment={Enviroment}
              user_profile={user_profile}
              PeopleILiked={PeopleILiked}
              people_liked_me={people_liked_me}
              setProfileUserNextIndex={this.props.setProfileUserNextIndex}
              saveFilesUpload={this.props.saveFilesUpload}
            />
            <ProfileNavigation user={user} enviroment={Enviroment} {...this.props} />
            {!_.isEmpty(campaign.data) &&
              <GiftForUsers
                current_user={user_profile.current_user}
                user={user}
              />
            }
            <UserVerification score={score} />
            <SameFbLike user={user} user_profile={user_profile} />
            <Description user={user} />
            <ProfileInformation user={user} openModal={this.openModal} mode="read" dictionaries={dictionaries} current_user={user_profile.current_user} {...this.props} />
            <ProfileLikeBanner
              target={this.state.carouselOffsetBottom}
              search={search}
              enviroment={Enviroment}
              user={user}
              needScrollToPosition={this.props.needScrollToPosition}
              current_user={user_profile.current_user}
            />
            <RemindUploadAvatarModal
              updateUploadPicturePageSource={this.props.updateUploadPicturePageSource}
              identifier={user.identifier}
              validateImage={this.props.validateImage}
              saveImage={this.props.saveImage}
              isOpen={this.state.isAvatarModalOpen}
              onRequestClose={this.hideAvatarModal}
              gender={user_profile.current_user.gender}
              gaSend={this.props.gaSend}
              user_profile={user_profile}
            />
          </div>
          {this.renderPreLoadUser(-1)}
        </div>
      </div>
    )
  }
}


Profile.propTypes = {
  identifier: PropTypes.string,
  Enviroment: PropTypes.object,
  params: PropTypes.object,
  socket: PropTypes.object,
  location: PropTypes.object,
  user_profile: PropTypes.object,
  searchUser: PropTypes.func,
  getFavorite: PropTypes.func,
  getVisitant: PropTypes.func,
  getBlockedUsers: PropTypes.func,
  addActivityAudit: PropTypes.func,
  getUserProfileByIdentifier: PropTypes.func,
  setViewProfileSource: PropTypes.func,
  setProfileUserNextIndex: PropTypes.func,
  gaSend: PropTypes.func,
  getPeopleMatchedMe: PropTypes.func,
}

export default Profile;
