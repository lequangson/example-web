import React from 'react'
import {
  FACEBOOK_URL,
  TWITTER_URL,
} from '../constants/Enviroment'

const SocialList = () => (
  <ul className="social-list">
    <li>
      <a
        href={FACEBOOK_URL}
        className="social-list__item facebook"
        target="_blank"
        rel="nofollow noopener noreferrer"
      >
        <i className="fa fa-facebook"></i>
      </a>
    </li>
    <li>
      <a
        href={TWITTER_URL}
        className="social-list__item twitter"
        target="_blank"
        rel="nofollow noopener noreferrer"
      >
        <i className="fa fa-twitter"></i>
      </a>
    </li>
  </ul>
)

export default SocialList
