import React, { PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import $ from 'jquery';
import _ from 'lodash';
import ValidateRange from 'src/scripts/validateRange';
import SubMenu from 'components/headers/SubMenu';
import {
  GA_ACTION_SAVE_SEARCH_CONDITION, SEARCH_CONDITION_PAGE
} from 'constants/Enviroment';
import * as ymmStorage from 'utils/ymmStorage';
import {SEARCH_MAX_AGE} from 'constants/search';
import DropdownPanel from './commons/DropdownPanel';
import SelectMultiple from './commons/SelectMultiple';

class SearchConditionSimple extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      resetSelected: false,
      resetClickStatus: true
    };
  }
  componentWillMount() {
    ymmStorage.setItem('searchmode', 2);
    this.props.getSearchCondition();
  }

    shouldComponentUpdate(nextProps, nextState) {
    if (this.state !== nextState) {
      return true;
    }

    if (this.props.search.condition !== nextProps.search.condition) {
      return true;
    }

    return false;
  }

  componentDidMount() {
    // List all locations user selected
    //this._displayLocations()
    window.scroll(0, 0);

    const form = this.refs.form
    const submitBtn = this.refs.searchBtn
    const age = new ValidateRange({element: '.js-validate-age'})

    form.addEventListener('change', function() {
      if (age.validate()) {
        submitBtn.removeAttribute('disabled')
      } else {
        submitBtn.setAttribute('disabled', 'disabled')
      }
    })
    //the following is hacking for hide keyboard on ios when unfocus textbox
    $( '*' ).click( function ( ) { } );
    $( 'html' ).css( "-webkit-tap-highlight-color", "rgba(0, 0, 0, 0)" );
  }

  componentDidUpdate() {
    const condition = this.props.search.condition.find((el) => el.search_type == 0);
    let selectNode = this.refs.age_min;
    selectNode.value = condition.age_min ? condition.age_min : 0;
    let selectNode2 = this.refs.age_max;
    selectNode2.value = condition.age_max ? condition.age_max : 0;
  }

  _renderAge(){
    return _.times(SEARCH_MAX_AGE).map(function(i) {
      if(i > 17){
        return <option className="checkbox" value={i} key={i}>{i}</option>
      }
    })
  }

  _renderSearchSort(){
    //search_sorts
    return this.props.dictionaries.search_sorts.map((sort) => {
      return(
        <option value={sort.key} key={sort.key} >{sort.value}</option>
      )
    })
  }

  handleClick(event) {

    event.preventDefault()

    var location_ids = [];
    $.each($("input[name='location_ids']"), function(){
        location_ids.push($(this).val());
    });

    const params = {
      "search_condition[age_min]": this.refs.age_min.value != "0" ? this.refs.age_min.value : '',
      "search_condition[age_max]": this.refs.age_max.value != "0"  ? this.refs.age_max.value : '',
      "search_condition[location_ids]": location_ids.join(','),
      "search_type": 0
    }

    const ga_params = {
      "age_min": this.refs.age_min.value != "0" ? this.refs.age_min.value : '',
      "age_max": this.refs.age_max.value != "0"  ? this.refs.age_max.value : '',
      "location_ids": location_ids.join(','),
      "search_type": 0
    }

    const ga_new_params = _.pickBy(ga_params);

      const ga_track_data = {
          search_condition: ga_new_params
      };
      this.props.gaSend(GA_ACTION_SAVE_SEARCH_CONDITION, ga_track_data);
      this.props.searchConditionClick(params)
  }

  resetSelected = () => {
    this.props.resetSelectedConditions();
    const newResetClickStatus = !this.state.resetClickStatus;
    this.setState({
      resetSelected: true,
      resetClickStatus: newResetClickStatus
    });
  }

  storeCallbackUrl = () => {
    ymmStorage.setItem('Payment_Intention', SEARCH_CONDITION_PAGE);
  }

  render() {
    const condition = this.props.search.condition.find((el) => el.search_type == 0);
    return (
      <div className="site-content">
        <SubMenu mode='default' scrollToRight/>
        <form action="/" ref="form" className="form mbl" onSubmit={(event) => this.handleClick(event)}>
          <div className="panel mbs">
            <a
              href=""
              className="panel__heading panel__heading--2"
            >
              <h3 className="panel__title">Tìm kiếm cơ bản</h3>
            </a>
          </div>
          <div className="container">
            <h3 className="form__sec-title">Tuổi</h3>
            <div className="row js-validate-age">
              <div className="col-xs-6">
                <select ref="age_min" id="age_min" name="age_min" className="js-validate-to form__input--b">
                  <option value="0">Bất kỳ</option>
                    {this._renderAge()}
                </select>
              </div>
              <div className="col-xs-2 txt-center"></div>
              <div className="col-xs-6">
                <select ref="age_max" id="age_max" name="age_max" className="js-validate-from form__input--b">
                  <option value="0">Bất kỳ</option>
                    {this._renderAge(condition.age_max)}
                </select>
              </div>
            </div>
            <hr />

            <h3 className="form__sec-title">Nơi ở hiện tại <small className="txt-light">(Có thể chọn nhiều)</small></h3>
            <div className="row">
              <div className="col-xs-12">
                <SelectMultiple name="location_ids" data={this.props.dictionaries.cities} search={this.props.search} type="0" reset={this.state.resetSelected}/>
              </div>
            </div>
            <br/>
            <br/>
          </div>
          <DropdownPanel
            panelTitle="Tìm kiếm nâng cao"
          >
            <div className="container">
              <div id="secondStep">
                <h3 className="form__sec-title"></h3>
                <div className="col-xs-12 padding-b30 mbl">
                  <div className="row">
                    <div className="txt-light">
                      Bạn chỉ có thể sử dụng thêm nhiều bộ lọc tìm kiếm nâng cao sau khi nâng cấp tài khoản.
                    </div>
                  </div>
                  <div className="row">
                    <Link to="/upgrade-instruction" onClick={this.storeCallbackUrl}>Tìm hiểu thêm</Link>
                  </div>
                </div>
              </div>
            </div>
          </DropdownPanel>

          <div className="fixed-bottom">
            <div className="container">
              <div className="row">
                <div className="col-xs-6">
                  <button type="reset" onClick={this.resetSelected} className="btn btn--b">Thiết lập lại</button>
                </div>
                <div className="col-xs-6">
                  <button
                    type="submit"
                    ref="searchBtn"
                    className="btn btn--p btn--b"
                    id="fourthStep"
                  >Tìm kiếm</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default SearchConditionSimple;
