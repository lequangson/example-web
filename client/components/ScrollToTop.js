import React, { Component } from 'react'

class ScrollToTop extends Component {
  scrollTop(evt) {
    evt.preventDefault()
    const scrollStep = -window.scrollY / (200 / 15)
    const scrollInterval = setInterval(() => {
      if (window.scrollY !== 0) {
        window.scrollBy(0, scrollStep)
      } else {
        clearInterval(scrollInterval)
      }
    }, 15)
  }

  render() {
    return (
      <a
        href="/"
        rel="nofollow"
        className="scroll-top"
        ref={(ref) => { this.scrTBtn = ref }}
        onClick={this.scrollTop}
      >
        <i className="fa fa-chevron-up"></i>
      </a>
    )
  }
}

export default ScrollToTop
