import React, { Component, PropTypes } from 'react'
import _ from 'lodash';
import $ from 'jquery'
import { Link } from 'react-router';
import Modal from 'components/modals/Modal'
import ReactSlider from 'react-slider'
import SelectMultiple from 'components/commons/SelectMultiple'
import SubMenu from 'components/headers/SubMenu';
import IconLinkSuperLikeSpinner from 'pages/super-like-spinner/card-item/IconLinkSuperLikeSpinner';
import { WHAT_HOT_PAGE, CDN_URL, GA_ACTION_SIMPLE_FILTER_CONDITION } from '../constants/Enviroment'
import TinderListUsers from './commons/TinderListUsers'
import GoogleOptimize from '../utils/google_analytic/google_optimize';
import SilderCommon from './commons/SilderCommon';
import { isEmpty } from 'lodash';
import RandomMatchBanner from './commons/RandomMatchBanner'

class WhatHot extends Component {
  constructor() {
    super()
    this.state = {
      data: [],
      isOpen: false,
      locationValues: 1,
      startAge: 18,
      endAge: 70,
      didRunSliderData: false,
    }
    this.loadMoreData = this.loadMoreData.bind(this)
    this.renderSliderData = this.renderSliderData.bind(this)
  }

  componentWillMount() {
    if (this.props.WhatHot.currentPageIndex <= 0 &&
      !this.props.WhatHot.isFetching) {
      this.loadMoreData();
    }
    if (!this.props.search.loaded_condition) {
      this.props.getSearchCondition();
    }
    if (!this.props.list_boost_in_rank.isFetching && this.props.list_boost_in_rank.boostInRankUser.length === 0) {
      this.props.getListBoostInRank();
    }
    this.initialSimpleFilterParameter(this.props.search, this.props.user_profile.current_user);
    this.props.updatePreviousPage(WHAT_HOT_PAGE)
  }

  componentWillReceiveProps(nextProps) {
    if(!isEmpty(this.props.user_profile.current_user) &&
      (this.state.didRunSliderData === false) ||
      (this.state.didRunSliderData && this.state.data.length > 0)){
      this.renderSliderData()
    }

    if ((nextProps.search.loaded_condition && !this.props.search.loaded_condition) ||
      (!_.isEmpty(nextProps.user_profile.current_user) && _.isEmpty(this.props.user_profile.current_user))) {
      this.initialSimpleFilterParameter(nextProps.search, nextProps.user_profile.current_user);
    }
  }

  @GoogleOptimize
  componentDidMount(){}

  loadMoreData() {
    this.props.getWhatHot(this.props.WhatHot.currentPageIndex + 1)
  }

  initialSimpleFilterParameter = (searchData, current_user) => {
    let startAge = this.state.startAge;
    let endAge = this.state.endAge;

    if (searchData.loaded_condition &&
      searchData.condition.length > 1) {
      if (searchData.condition[1].age_min && searchData.condition[1].age_min > 0 &&
        searchData.condition[1].age_max && searchData.condition[1].age_max > 0) {
        startAge = searchData.condition[1].age_min;
        endAge = searchData.condition[1].age_max;
        this.setState({startAge, endAge});
        return;
      }
    }
    this.defaultSimpleFilterParameter(current_user);
  }

  defaultSimpleFilterParameter = (current_user) => {
    let startAge = this.state.startAge;
    let endAge = this.state.endAge;

    if (!_.isEmpty(current_user)){
      if (current_user.gender === 'male') {
        startAge = current_user.age - 10;
        endAge = current_user.age + 5;
      }
      else {
        startAge = current_user.age - 5;
        endAge = current_user.age + 10;
      }

      if (startAge < 18) {
        startAge = 18;
      }
      if (endAge > 70) {
        endAge = 70;
      }
    }
    if (startAge != this.state.startAge ||
      endAge != this.state.endAge) {
      this.setState({startAge, endAge});
    }
  }

  renderSliderData() {
    const {has_first_like, has_first_match, has_conversation} = this.props.user_profile.current_user;
    const hasLike = has_first_like === true || this.props.PeopleILiked.data.length > 0;
    const hasMatch = has_first_match === true || this.props.user_profile.matched_users.length > 0;
    const hasConversation = has_conversation === true || this.props.user_profile.has_start_chat === true;
    const data1 = []
    if(hasLike === false) {
      data1.push({
        icon_url : `${CDN_URL}/general/Icon/guidance_image_1.png`,
        question: 'Muốn tìm bạn trò chuyện?',
        answer: 'Chủ động gửi Thích!'
      })
    }

    if(hasMatch === false) {
      data1.push({
        icon_url : `${CDN_URL}/general/Icon/guidance_image_2.png`,
        question: 'Muốn tìm bạn trò chuyện?',
        answer: 'Thích lại những người trong "Ai thích tôi"!'
      },)
    }

    if(hasConversation === false) {
      data1.push({
        icon_url : `${CDN_URL}/general/Icon/guidance_image_3.png`,
        question: 'Muốn tìm bạn trò chuyện?',
        answer: 'Nhắn tin với những người trong "Trò chuyện"!'
      })
    }
    if (this.state.data.length !== data1.length || this.state.didRunSliderData === false) {
      this.setState({
        data: data1,
        didRunSliderData: true,
      });
    }
  }

  renderSlider() {
    if (!isEmpty(this.state.data)) {
      return (
        <SilderCommon carouselName='carousel-common' numberShowItem={1}>
          {
            this.state.data.map((value, i) => {
              return (
                <div key={i}>
                  <div className="banner banner--white no-cursor">
                   <img src={value.icon_url} className="banner__icon" />
                   <div className="banner__content">
                     <h3 className="banner__heading txt-blue txt-uppercase">{value.question}</h3>
                     <p className="banner__sub-heading">{value.answer}</p>
                   </div>
                  </div>
                </div>
              )
            })
          }
        </SilderCommon>
      );
    }
    return <div />;
  }

  closeFilterModal = () => {
    this.setState({ isOpen: false })
  }

  handleClick(event) {
    event.preventDefault()

    var location_ids = [];
    $.each($("input[name='location_ids']"), function(){
      location_ids.push($(this).val());
    });

    this.setState({locationValues: location_ids.join(',')})

    const params = {
      'search_condition[age_min]': this.state.startAge + "",
      'search_condition[age_max]': this.state.endAge + "",
      'search_condition[location_ids]': location_ids.join(','),
      'search_condition[search_type]': 1
    }

    const ga_params = {
      'age_min': this.state.startAge + "",
      'age_max': this.state.endAge + "",
      'location_ids': location_ids.join(','),
      'search_type': 1
    }

    const ga_new_params = _.pickBy(ga_params);

    const ga_track_data = {
        search_condition: ga_new_params
    };
    this.props.gaSend(GA_ACTION_SIMPLE_FILTER_CONDITION, ga_track_data);
    this.props.simpleFilterClick(params)
    this.closeFilterModal()
  }

  render() {
    const { current_user } = this.props.user_profile;
    const { users, selectedUserIndex, reachEnd, showAdProgram, adProgramIndex } = this.props.WhatHot;
    return (
      <div className="site-content">
        <SubMenu mode='WhatHot'/>
        { ((!_.isEmpty(current_user) && !_.isEmpty(this.props.campaign.data) && current_user.permission.number_random_match_free > 0) ||
          this.props.random_match.isUpdateRandomMatch)
          &&
          <div className="col-xs-12">
            <RandomMatchBanner
              inCampaign
              current_user={current_user}
              gaSend={this.props.gaSend}
            />
          </div>
        }
        {isEmpty(this.props.campaign.data) &&
          this.renderSlider()
        }
        <div className="simple-filter col-xs-12 col-sm-12 col-md-8 col-md-offset-2 mbm" onClick={() => {this.setState({isOpen: true})}}>
          <div className="l-flex-vertical-center txt-right-mc">
            <span className="mrm">Bộ lọc</span>
            <i className="fa fa-sliders txt-blue fa-2x"></i>
          </div>
        </div>
        <TinderListUsers
          listUsers = {users}
          selectedUserIndex = {selectedUserIndex}
          showNextUser = {this.props.showNextUser}
          loadMoreData = {this.loadMoreData}
          reachEnd = {reachEnd}
          pageSource = {WHAT_HOT_PAGE}
          ignoreUser = {this.props.ignoreUser}
          forceLike
          smallSpaceBottom
          cardNotPaddingTop={this.state.data.length > 0}
          showAdProgram = {showAdProgram}
          hideAdProgram = {this.props.hideAdProgram}
          adProgramIndex = {adProgramIndex}
        />
        {!_.isEmpty(this.props.campaign.data) &&
          this.renderSlider()
        }
        {
          current_user.gender === 'male' &&
          <IconLinkSuperLikeSpinner />
        }
        <Modal
          transitionName="modal"
          isOpen={this.state.isOpen}
          onRequestClose={this.closeFilterModal}
          className="FilterModal modal__content"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          <div>
            <div className="well well--e txt-center txt-blue txt-medium txt-lg mbm">Tiêu chí</div>
            <form action="/" ref="form" className="form" onSubmit={(event) => this.handleClick(event)}>
              <h3 className="form__sec-title l-flex-ce">
                Tuổi
                <span className="txt-right">{this.state.startAge} - {this.state.endAge}</span>
              </h3>
              <div className="row mbm">
                <div className="col-xs-12">
                <ReactSlider
                  ref="slider"
                  min={18}
                  max={70}
                  defaultValue={[this.state.startAge, this.state.endAge]}
                  className="horizontal-slider"
                  withBars
                  onAfterChange={() => {
                    let rangeAge = this.refs.slider.state.value;
                    this.setState({startAge: rangeAge[0]})
                    this.setState({endAge: rangeAge[1]})
                }}/>
                </div>
              </div>
              <h3 className="form__sec-title">Nơi ở hiện tại (Có thể chọn nhiều)</h3>
              <div className="row mbm">
                <div className="col-xs-12">
                  <SelectMultiple
                    value={this.state.locationValues}
                    name="location_ids"
                    data={this.props.dictionaries.cities}
                    search={this.props.search}
                    type={1}
                    current_user={current_user}
                    defaultSimpleFilter
                  />
                </div>
              </div>
              <div className="mbm">
                <button
                  className="btn btn--p btn--b"
                  type="submit"
                >
                  Đồng Ý
                </button>
              </div>
            </form>
            <button className="modal__btn-close" onClick={this.closeFilterModal}>
              <i className="fa fa-times"></i>
            </button>
          </div>
        </Modal>
      </div>
    )
  }
}

WhatHot.propTypes = {
  getWhatHot: PropTypes.func.isRequired,
  showNextUser: PropTypes.func.isRequired,
  hideAdProgram: PropTypes.func.isRequired,
  simpleFilterClick: PropTypes.func,
}

export default WhatHot