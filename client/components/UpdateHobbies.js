import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'
import $ from 'jquery'
import toggleBackdrop from '../src/scripts/toggleBackdrop'
import { updateUserProfileFailure } from '../actions/userProfile'
import { MAXIMUM_USER_HOBBIES, MAXIMUM_HOBBY_TEXT_SIZE } from '../constants/userProfile'
import { MAXIMUM_HOBBY_TEXT_ERROR_MSG } from '../constants/TextDefinition'

class UpdateHobbies extends React.Component {
  constructor() {
    super()
    this.state = {
      showAddMore: true,
      showInput0: true,
      showInput1: false,
      showInput2: false,
    }
    this.showInput = this.showInput.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
  }

  componentWillMount() {
    const { current_user } = this.props.user_profile
    const { user_hobbies } = current_user
    let key = ''
    let obj = {}
    if (typeof user_hobbies !== 'undefined') {
      for (let i = 0; i < user_hobbies.length; i += 1) {
        key = `showInput${i}`
        obj = {}
        obj[key] = true
        this.setState(obj)
      }
      if (user_hobbies.length === 3) {
        this.setState({ showAddMore: false })
      }
    }
  }

  componentDidMount() {
    toggleBackdrop($)
  }

  showInput() {
    if (this.state.showInput1 || this.state.showInput2) {
      this.setState({
        showAddMore: false,
      })
    }
    if (!this.state.showInput1) {
      this.setState({
        showInput1: true,
      })
      return
    }

    if (!this.state.showInput2) {
      this.setState({
        showInput2: true,
      })
      return
    }
  }

  hideInput(number) {
    const hobby = this.refs[`hobby${number}`]
    hobby.value = ''
    if (Number(number) === 1) {
      this.setState({
        showInput1: false,
        showAddMore: true,
      })
      return
    }

    if (Number(number) === 2) {
      this.setState({
        showInput2: false,
        showAddMore: true,
      })
      return
    }
  }

  handleUpdate() {
    let params = {}
    let value = ''
    const values = []
    for (let i = 0; i < MAXIMUM_USER_HOBBIES; i += 1) {
      value = this.refs[`hobby${i}`].value
      if (value && $.trim(value).length) {
        if ($.trim(value).length > MAXIMUM_HOBBY_TEXT_SIZE) {
          this.props.showErrorMessage(MAXIMUM_HOBBY_TEXT_ERROR_MSG)
          return
        }
        values.push(this.refs[`hobby${i}`].value)
      }
    }
    params['personal[user_hobbies][][value]'] = values
    if (!values.length) {
      params = { 'personal[user_hobbies][]': '' }
    }
    this.props.updateUserProfile(params).then(() => {
      browserHistory.push('/myprofile')
    })
  }

  render() {
    const { current_user } = this.props.user_profile
    const { user_hobbies } = current_user
    if (typeof user_hobbies === 'undefined') {
      return <div>Loading...</div>
    }
    const remain_slot_hobbies = []
    for (let j = user_hobbies.length; j < MAXIMUM_USER_HOBBIES; j += 1) {
      remain_slot_hobbies.push({ key: j, value: '' })
    }
    return (
      <div className="sticky-footer">
        <div className="sticky-content">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <div id="hobbiesForm">
                  <h3>Thêm sở thích</h3>
                  <div id="hobbiesField" className="form__group">
                    {
                      user_hobbies.map((hobbies, i) =>
                        <div className={`row mbm ${(this.state[`showInput${i}`]) ? '' : 'hidden'}`} key={i} >
                          <div className="col-xs-10">
                            <input
                              className="txt-blue"
                              type="text"
                              ref={`hobby${i}`}
                              defaultValue={hobbies.value}
                            />
                          </div>
                          { i > 0 &&
                            <div className="col-xs-2">
                              <button className="btn btn--b" onClick={() => { this.hideInput(i) }}>
                                <i className="fa fa-times" />
                              </button>
                            </div>
                          }
                        </div>
                      )
                    }
                    {
                      remain_slot_hobbies.map((hobbies, i) =>
                        <div
                          className={`row mbm ${(this.state[`showInput${hobbies.key}`]) ? '' : 'hidden'}`}
                          key={hobbies.key}
                        >
                          <div className="col-xs-10">
                            <input
                              className="txt-blue"
                              type="text"
                              ref={`hobby${hobbies.key}`}
                              defaultValue={hobbies.value}
                            />
                          </div>
                          { i >= 0 &&
                            <div
                              className={`col-xs-2 ${(!i && !user_hobbies.length) ? 'hidden' : ''}`}
                            >
                              <button
                                className="btn btn--b"
                                onClick={() => { this.hideInput(hobbies.key) }}
                              >
                                <i className="fa fa-times" />
                              </button>
                            </div>
                          }
                        </div>
                      )
                    }
                    <button
                      id="addMore"
                      className={`btn ${(this.state.showAddMore) ? '' : 'hidden'}`}
                      onClick={this.showInput}
                    >
                      <i className="fa fa-plus" /> Thêm
                    </button>
                  </div>
                  <div className="row">
                    <div className="col-xs-6">
                      <Link to="/myprofile">
                        <button className="btn btn--b">Hủy</button>
                      </Link>
                    </div>
                    <div className="col-xs-6">
                      <button className="btn btn--p btn--b" onClick={this.handleUpdate} >
                        Cập nhật
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

UpdateHobbies.propTypes = {
  // dictionaries: PropTypes.object,
  user_profile: PropTypes.object,
  updateUserProfile: PropTypes.func,
  showErrorMessage: PropTypes.func,

}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    showErrorMessage: (msg) => {
      dispatch(updateUserProfileFailure(msg))
    },
  }
}
const UpdateHobbiesContainer = connect(mapStateToProps, mapDispachToProps)(UpdateHobbies);

export default UpdateHobbiesContainer
