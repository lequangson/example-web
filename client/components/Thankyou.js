import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { CDN_URL, PAYMENT_PAGE } from '../constants/Enviroment'
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class Thankyou extends Component {
  componentWillMount() {
    if (this.props.user_profile.current_user.user_status !== 3) {
      // browserHistory.push('/')
    }
  }

  @GoogleOptimize
  componentDidMount() {}


  getCurrentTime(pack) {
    const date = new Date();
    const pack_type = [1,3,6]
    date.setMonth(date.getMonth() + pack_type[pack-1])
    return date
  }

  render() {
    const previous_page = this.props.Enviroment.previous_page
    const pack = this.props.user_profile.pack
    const d =  this.getCurrentTime(pack)
    const day = d.getDate()+ '/' + (d.getMonth() + 1) + '/' + d.getFullYear()

    return (
      <div className="sticky-footer">
        <div className="site-content txt-center">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <div className="well">
                  <header><a href="/"><img src={`${CDN_URL}/general/logo-ymeet.png`} width="100" /></a>
                    <p className="txt-blue txt-bold txt-lg">Hẹn hò an toàn cho phụ nữ</p>
                  </header>
                </div>
                <div className="mbl"><img src={`${CDN_URL}/general/thankyou.png`} /></div>
                { this.props.user_profile.current_user.user_status === 3
                  ?
                  <p className="txt-lg txt-light">
                    Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi!<br />
                    Bạn có thể quay trở lại bất cứ lúc nào,<br />
                    mọi thông tin về tài khoản sẽ được bảo lưu,<br />
                    trừ hình ảnh.<br />
                  </p>
                  :
                  previous_page === PAYMENT_PAGE && pack || ( this.props.user_profile.current_user.user_type === 4 && this.props.user_profile.current_user.user_status !== 3)
                  ?
                  <p className="txt-lg txt-light">
                    Cảm ơn bạn đã tin dùng dịch vụ của chúng tôi!<br /><br />
                    Để tri ân, Ymeet.me xin tặng bạn <span className="txt-blue">MIỄN PHÍ</span> gói nâng cấp tài khoản bạn vừa lựa chọn thanh toán, <span className="txt-blue">sử dụng đến hết ngày 30/03/2017.</span> <br /><br />
                    Hy vọng bạn sẽ tiếp tục đồng hành cùng Ymeet.me trong tương lai! <br />
                    Chúc bạn có những giờ phút hẹn hò vui vẻ!
                  </p>
                  :
                  ''
                }
              </div>
            </div>
          </div>
        </div>
        <footer className="site-footer">
          <p className="txt-center txt-light txt-sm">Copyright &copy; 2016 <a href="http://mmj.vn">Media Max Japan</a> All Rights Reserved.</p>
        </footer>
      </div>
    )
  }
}

// const Thankyou = (props) => {
//   const isDeleted = props.user_profile.current_user.user_status || null
//
//   if ((isDeleted === 1) && (isDeleted === null)) {
//     console.log('still alive', isDeleted)
//   } else {
//     console.log('has deleted', isDeleted)
//   }
//
//   return (
//     <div className="sticky-footer">
//       <div className="site-content txt-center">
//         <div className="container">
//           <div className="row">
//             <div className="col-xs-12">
//               <div className="well">
//                 <header><a href="/"><img src="https://s3.amazonaws.com/ymmtest/general/logo-ymeet.png" width="100" /></a>
//                   <p className="txt-blue txt-bold txt-lg">Ứng dụng hẹn hò thông qua Facebook đầu tiên tại Việt Nam</p>
//                 </header>
//               </div>
//               <div className="mbl"><img src="https://s3.amazonaws.com/ymmtest/general/thankyou.png" /></div>
//               <p className="txt-lg txt-light">
//                 Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi!<br />
//                 Bạn có thể quay trở lại bất cứ lúc nào,<br />
//                 mọi thông tin về tài khoản sẽ được bảo lưu,<br />
//                 trừ hình ảnh.
//               </p>
//             </div>
//           </div>
//         </div>
//       </div>
//       <footer className="site-footer">
//         <p className="txt-center txt-light txt-sm">Copyright © 2016 <a href="http://mmj.vn">Media Max Japan</a> All Rights Reserved.</p>
//       </footer>
//     </div>
//   )
// }

export default Thankyou
