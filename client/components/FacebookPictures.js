import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import $ from 'jquery'
import { AVATAR_PICTURE, OTHER_PICTURE, UPLOAD_FACEBOOK_MODE } from '../constants/userProfile'
import notification from '../utils/errors/notification'
import { NOTIFICATION_ERROR } from '../constants/Enviroment'

class FacebookPictures extends Component {
  static remainPictures(pictures = [], max) {
    if (max < 1) {
      throw new Error('Số ảnh tối đa phải lớn hơn hoặc bằng 1')
    }

    let msg = ''

    // exclude 1st picture (avatar)
    const picsLeng = (pictures.length && pictures[0].is_main) ? pictures.length - 1 : pictures.length

    if ((max - picsLeng) === 0) {
      msg = `* Bạn không thể tải lên quá ${max} ảnh, vui lòng xóa ảnh cũ để tải lên ảnh mới`
      return msg
    }

    msg = `* Bạn còn tải lên được ${max - picsLeng} ảnh`
    return msg
  }

  static renderFacebookPictureForOtherPicture(pictureUrl, key) {
    return (
      <div key={key} className="l-flex-grid__item">
        <label htmlFor={`fbp-${key}`} className="toggle toggle--1">
          <img src={pictureUrl} role="presentation" />
          <input id={`fbp-${key}`} type="checkbox" name="chkFacebookPicture" value={pictureUrl} />
          <div className="toggle__fake toggle__fake--1"></div>
        </label>
      </div>
    )
  }

  constructor(props) {
    super(props);
    this.state = {
      page_index: 1,
      page_size: 10,
    }

    this.synchFacebookPictureForOtherPicture = this.synchFacebookPictureForOtherPicture.bind(this)
    this.showMorePictures = this.showMorePictures.bind(this)
    this.synchFacebookPictureForAvatar = this.synchFacebookPictureForAvatar.bind(this)
  }

  componentWillMount() {
    const { albumId } = this.props
    this.props.getFacebookPictures(albumId)
  }

  synchFacebookPictureForAvatar(pictureUrl) {
    const image = {
      'mode': UPLOAD_FACEBOOK_MODE,
      'url': pictureUrl,
    }
    const { welcome_page_completion, provider } = this.props.user_profile.current_user
    const isFromWelcome = welcome_page_completion < 3 && provider === 'google'
    this.props.saveImage(image, isFromWelcome)
    //this.props.saveFilesUpload([pictureUrl], AVATAR_PICTURE, UPLOAD_FACEBOOK_MODE)
  }

  synchFacebookPictureForOtherPicture() {
    const selectedPictures = []
    $.each($("input[name='chkFacebookPicture']:checked"), function upload() {
      selectedPictures.push($(this).val())
    })

    if (selectedPictures.length === 0) {
      notification(NOTIFICATION_ERROR, 'Bạn chưa chọn ảnh nào để tải lên Ymeet.me!')
      return
    }

    this.props.saveFilesUpload(selectedPictures, OTHER_PICTURE, UPLOAD_FACEBOOK_MODE)
  }

  showMorePictures() {
    this.setState({ page_index: this.state.page_index + 1 })
  }

  renderFacebookPictureForAvatar(pictureUrl, key) {
    return (
      <div className="l-flex-grid__item" key={key}>
        <div
          className="mbs"
          onClick={() => {
            this.synchFacebookPictureForAvatar(pictureUrl)
          }}
        ><img src={pictureUrl} role="presentation" /></div>
      </div>
    )
  }

  render() {
    const { fb_pictures, isFetching, current_user } = this.props.user_profile
    const { pictureType } = this.props.params
    if (fb_pictures.length === 0 && isFetching) {
      return (<div>Loading...</div>)
    }
    const isCompletedWelcome = current_user.welcome_page_completion > 2 || current_user.provider === 'facebook'
    const fb_pictureForShow = fb_pictures.slice(0, this.state.page_index * this.state.page_size)

    return (
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3 className="txt-heading">Thêm ảnh từ Facebook</h3>
              <p className="txt-light">Vui lòng chọn ảnh<br />
                <span className="txt-italic">
                  {(parseInt(pictureType, 10) !== 2) && this.constructor.remainPictures(current_user.user_pictures, 20)}
                </span>
              </p>
              <div className="col-xs-12 mbl">
                <div className="l-flex-grid">
                  {fb_pictureForShow.map((pictureUrl, i) => (
                    parseInt(pictureType, 10) === AVATAR_PICTURE
                      ? this.renderFacebookPictureForAvatar(pictureUrl, i)
                      : this.constructor.renderFacebookPictureForOtherPicture(pictureUrl, i)
                  ))}
                </div>
              </div>
            </div>

            <div className="col-xs-12">
              {(parseInt(pictureType, 10) !== 2) &&
                <button
                  className="btn btn--b btn--p mbs"
                  onClick={this.synchFacebookPictureForOtherPicture}
                >Đồng ý</button>
              }
              <button
                className="btn btn--b mbs"
                disabled={this.state.page_index * this.state.page_size >= fb_pictures.length}
                onClick={this.showMorePictures}
              >Tải thêm ảnh</button>
              <Link to={`${isCompletedWelcome ? '/myprofile' : '/welcome'}`}>
                <button className="btn btn--b mbl">Hủy</button>
              </Link>
            </div>

          </div>
        </div>
      </div>
    )
  }
}

FacebookPictures.propTypes = {
  user_profile: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  albumId: PropTypes.string.isRequired,
  getFacebookPictures: PropTypes.func.isRequired,
  saveFilesUpload: PropTypes.func.isRequired,
}

export default FacebookPictures
