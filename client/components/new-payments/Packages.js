import React, { Component } from 'react';
import { connect } from 'react-redux'
import { browserHistory, Link } from 'react-router'
import { isEmpty } from 'lodash';
import $ from 'jquery';
import ReactTooltip from 'react-tooltip'
import { CDN_URL, ANDROID_URL, IOS_URL } from 'constants/Enviroment';
import { PAYMENT_PACKAGE_TYPE_COIN, PAYMENT_PACKAGE_TYPE_MONTHLY } from '../../constants/Payment';
import * as UserDevice from '../../utils/UserDevice';

class Packages extends Component {
  constructor(props) {
    super(props)
    this.state = {
      packageSelected: 0,
    }
    this.handleOnchange = this.handleOnchange.bind(this);
  }

  componentWillMount() {
    const { Payment, params } = this.props;
    const { type, method } = params;
    // if (typeof type === 'undefined') {
    //   browserHistory.push('/myprofile');
    // }
    const { packages, package_type } = Payment;
    const url_package_type = type == 'coin' ? 0 : 1;
    if (!packages.length || package_type != url_package_type) {
      this.props.updatePaymentState('package_type', url_package_type)
      this.props.getPaymentPackages(url_package_type);
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.params.type != nextProps.params.type) {
      const url_package_type = nextProps.params.type == 'coin' ? 0 : 1;
      this.props.updatePaymentState('package_type', url_package_type)
      this.props.getPaymentPackages(url_package_type);
    }
    if (this.props.Payment.packages != nextProps.Payment.packages || (this.props.Payment.package_type != nextProps.Payment.package_type && this.props.Payment.package_type !== null)) {
       const { Payment } = nextProps;
       const { packages, payment_method, package_id, package_type, mobile_card_info } = Payment;
       if (package_type == PAYMENT_PACKAGE_TYPE_MONTHLY) {
        let package_id = 0;
        if (Payment.default_package_id) {
          package_id = Payment.default_package_id;
        }
        else {
          // default selected package  popular
          const popularPackage = packages.find(pk1 => (pk1.license_type == 'license_type_monthly' && pk1.name == '1 tháng'));
          package_id = parseInt(popularPackage.id)
        }
        this.props.updatePaymentState('package_id', package_id);
        setTimeout(function(){ $(`#package_${package_id}`).click(); }, 500);

        if (Payment.default_package_id) {
          this.props.updatePaymentState('default_package_id', null);
        }
       }
    }
  }

  handleClick = (package_id, event) => {
    this.props.updatePaymentState('package_id', package_id);
    const { packages } = this.props.Payment;
    const selectedPackageName = packages.find(p => p.id == package_id) && packages.find(p => p.id == package_id).name;
    if(selectedPackageName == '3 tháng' || selectedPackageName == '6 tháng') {
      this.setState({packageSelected: package_id})
    } else { this.setState({packageSelected: 0}) }
    $(`#package_${package_id}`).click();
    // this.addActive(event);
  }

  handleOnchange(event) {
    this.props.updatePaymentState('package_id', parseInt(event.target.value));
  }

  addActive(event) {
    if (typeof event == 'undefined' || typeof event.currentTarget == 'undefined') {
      return;
    }
    const elm = event.currentTarget || null;
    const child = elm.querySelector('.box');
    $('div.box-active').removeClass('box-active');
    child.classList.add('box-active');
  }

  fixHeight() {
    const { type } = this.props.params;
    if (type == 'coin') {
      return {};
    }
    return {
      minHeight: 13.33333 + 'rem'
    }
  }

  renderLinkDownload(userDevice) {
    switch (userDevice) {
      case 'ios':
        return(
          <a className="txt-blue" href={IOS_URL}>Tải ngay</a>
        )
        break
      case 'android':
        return(
          <a className="txt-blue" href={ANDROID_URL}>Tải ngay</a>
        )
        break
      default:
        return ('Tải ngay')
        break
    }
  }
  render() {
    const { Payment } = this.props;
    const { packages, payment_method, package_id, package_type, mobile_card_info } = Payment;
    const selectedPackageName = packages.find(p => p.id == package_id) && packages.find(p => p.id == package_id).name;
    const isDisabledPackages = selectedPackageName == '3 tháng' || selectedPackageName == '6 tháng';
    if (isEmpty(Payment) || !packages.length) {
      return <div></div>
    }
    let real_unit_amount_1_week = 0;
    let real_unit_amount_1_month = 0;
    let real_unit_amount_3_month = 0;
    let real_unit_amount_6_month = 0;
    if (package_type == PAYMENT_PACKAGE_TYPE_MONTHLY ) {
      const package_1_week = packages.find(pk1 => pk1.quantity == 1 && pk1.license_type == 'license_type_weekly')
      real_unit_amount_1_week = package_1_week ? (package_1_week.package_prices[0].amount + package_1_week.payment_method_fees[payment_method-1].fee) : 0
      const package_1_month = packages.find(pk1 => pk1.quantity == 1 && pk1.license_type == 'license_type_monthly');
      real_unit_amount_1_month = package_1_month ? (package_1_month.package_prices[0].amount + package_1_month.payment_method_fees[payment_method-1].fee) : 0
      const package_3_month = packages.find(pk3 => pk3.quantity == 3 && pk3.license_type == 'license_type_monthly');
      real_unit_amount_3_month = package_3_month ? (package_3_month.package_prices[0].amount + package_3_month.payment_method_fees[payment_method-1].fee) : 0
      const package_6_month = packages.find(pk6 => pk6.quantity == 6 && pk6.license_type == 'license_type_monthly');
      real_unit_amount_6_month = package_6_month ? (package_6_month.package_prices[0].amount + package_6_month.payment_method_fees[payment_method-1].fee) : 0

    }
    const has_successful_card = mobile_card_info.filter(card => card.payment_status == 1);
    return (
      <div className="payment-segments container active">
        {
          package_type == PAYMENT_PACKAGE_TYPE_COIN &&  packages.map((item, i) => {
            const real_unit_amount = (item.package_prices[0].unit_amount + item.payment_method_fees[payment_method-1].unit_fee) / 1000;

            return <div className="col-xs-4"  key={item.id} onClick={(event) => {this.handleClick(item.id, event)}}>
              <div className="box" style={this.fixHeight()} >
                <div className="box__month txt-center padding-b10 txt-light">{item.name}</div>
                <div className="box__new-price txt-center padding-b10 txt-blue"><b> {`${real_unit_amount}k`} </b>{package_type ? ' / tháng' : ''}</div>
                <div className="box__old-price txt-center padding-b10 txt-light">{`${item.package_prices[0].old_unit_amount/1000} k`}</div>
                {item.is_popular && <div className="card__addon-2">
                      <div className="card__addon-2__title txt-lg">Hot!</div>
                  </div>
                }
                {item.is_popular && <div className="card__pad-2 card__pad-2--yellow"></div>}
              </div>
              <label className="form__input--d txt-center radio-large">
                <input type="radio" name="radio" id={`package_${item.id}`} disabled={has_successful_card.length ? true : false} onClick={() => {this.handleClick(item.id)}} defaultChecked={item.id == package_id ? true : false } />
                <div className="fake-radio"></div>
              </label>
            </div>
          })
        }

        {package_type == PAYMENT_PACKAGE_TYPE_MONTHLY && <div className="row padding-0">
            {
              packages.map((item, i) => {
                const real_unit_amount = (item.package_prices[0].unit_amount + item.payment_method_fees[payment_method-1].unit_fee);
                const license_type_weekly = item.license_type == 'license_type_weekly' ? 'tuần' : 'tháng';
                return (
                  <div className={`col-xs-12 box no-border ${this.state.packageSelected === item.id && 'animated shake'} ${item.id == package_id ? 'active' : ''}`} onClick={(event) => {this.handleClick(item.id, event)}} key={item.id}>
                    {this.state.packageSelected === item.id &&
                      <div className="col-xs-10 col-xs-offset-2 mbt txt-red">
                        <span className="txt-sm">Gói nâng cấp này chỉ có trên<br/>ứng dụng.
                        {this.renderLinkDownload(UserDevice.getName())} để hoàn tất thanh toán.</span>
                      </div>
                    }
                    {item.name == '1 tháng' && <div>
                    <div className="card__addon-2">
                      <div className="card__addon-2__title txt-lg">HOT</div>
                    </div>
                    <div className="card__pad-2 card__pad-2--yellow">
                    </div></div>}
                    <div className="col-xs-2">
                      <label className="form__input--d radio-large margin-0">
                        <input type="radio" name="packages" id={`package_${item.id}`} disabled={has_successful_card.length ? true : false} onClick={() => {this.handleClick(item.id)}} defaultChecked={item.id == package_id ? true : false } />
                        <div className="fake-radio"></div>
                      </label>
                    </div>
                    <div className="col-xs-4">
                      <div className="txt-blue txt-medium">{item.name}</div>
                      {item.license_type == 'license_type_weekly' &&
                      <div className="txt-sm txt-medium txt-muted">Thanh toán: {new Intl.NumberFormat().format(real_unit_amount/1000)}k</div>}
                      {item.quantity == 1 && item.license_type == 'license_type_monthly' &&
                      <div className="txt-sm txt-medium txt-muted">Thanh toán: {new Intl.NumberFormat().format(real_unit_amount_1_month/1000)}k</div>}
                      {item.quantity == 3 && item.license_type == 'license_type_monthly' &&
                      <div className="txt-sm txt-medium txt-muted">Thanh toán: {new Intl.NumberFormat().format(real_unit_amount_3_month/1000)}k</div>}
                      {item.quantity == 6 && item.license_type == 'license_type_monthly' &&
                      <div className="txt-sm txt-medium txt-muted">Thanh toán: {new Intl.NumberFormat().format(real_unit_amount_6_month/1000)}k</div>}
                      {item.license_type == 'license_type_weekly' &&
                        <div className="txt-xs">
                          <div className="txt-muted txt-bold"><span className="txt-warning">+ 1 FREE</span> Tán luôn</div>
                          <div className="txt-muted txt-bold"><span className="txt-warning">+ 3 FREE</span> Ghép đôi ngẫu nhiên</div>
                        </div>
                      }

                    </div>
                    <div className="col-xs-6 box__month txt-medium">
                      <div className="txt-warning">{new Intl.NumberFormat().format(real_unit_amount/1000)}k/ {license_type_weekly}</div>
                      {!!item.number_coin_bonus && <div className="l-flex-vertical-center">
                        <img src={`${CDN_URL}/general/Campaign/box-gift.png`} alt="coin" className="img-small"/>
                        <span>&nbsp;+&nbsp;</span>
                        <img src={`${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`} alt="coin" className="img-small"/>
                        <span>&nbsp;{item.number_coin_bonus}</span>
                      </div>}
                    </div>
                  </div>
                )
              })
            }
            <div className="col-xs-12 well"></div>
          </div>
        }
        <div className="fixed-bottom">
          <div className="container">
            <div className="row l-flex-vertical-center">
              <div className="col-xs-12">
                <button
                  className="btn txt-center btn--p btn--b"
                  onClick={this.props.fulfillPayment}
                  disabled={isDisabledPackages}
                >
                  Chọn gói nâng cấp này
                </button>
              </div>
            </div>
          </div>
        </div>
        <ReactTooltip
          className='customeTheme' // customTheme in desktop.css
          place="right"
          type="light"
          border= {true}
          offset={{'right': 10}}
          globalEventOff='click'
        />
      </div>
    )
  }
}


function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {}
}

const PackagesContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(Packages)


export default PackagesContainer
