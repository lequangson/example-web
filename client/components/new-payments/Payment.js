import React, { Component } from 'react';
import { CDN_URL } from 'constants/Enviroment';
import { Link, browserHistory } from 'react-router';
import HeaderContainer from 'containers/HeaderContainer';
import Packages from './Packages';
import PackagePaymentSteps from './PackagePaymentSteps';
import SelectMethod from './SelectMethod';
import ThankYou from 'components/payments/ThankYou';
import SlidePayment from './SlidePayment';
import { INSTRUCTION, CHOOSING_PACKAGE, FULFILL_PAYMENT, PAYMENT_RESULT } from 'constants/Payment';
import BannerSwapSite from 'components/commons/BannerSwapSite';

const TOTAL_STAGE = 4;
export default class Payment extends Component {
  state = {
    stage: 1,
  }

  static stepTitles = [`Chọn gói<br />nâng cấp`, 'Hình thức<br />thanh toán', 'Hoàn tất']
  static stepIcons = [`${CDN_URL}/general/Payment/StepsIcon/step1.png`, `${CDN_URL}/general/Payment/StepsIcon/step2.png`, `${CDN_URL}/general/Payment/StepsIcon/step3.png`]
  static stepIconsActive = [`${CDN_URL}/general/Payment/StepsIcon/step1.png`, `${CDN_URL}/general/Payment/StepsIcon/step2-active.png`, `${CDN_URL}/general/Payment/StepsIcon/step3-active.png`]
  static STEP_COUNT = 3

  choosePackage = () => {
    this.setState({ stage: CHOOSING_PACKAGE });
  }

  fulfillPayment = (params) => {
    this.setState({ stage: FULFILL_PAYMENT });
  }

  goBack = () => {
    const currentStage = this.state.stage;
    if (currentStage > INSTRUCTION) {
      this.setState({ stage: (currentStage - 1)});
    } else {
      browserHistory.goBack();
    }
  }

  renderSteps() {
    return (
      <PackagePaymentSteps stage={this.state.stage} />
    )
  }

  renderPayment() {
    const { stage } = this.state;
    switch (stage) {
      case CHOOSING_PACKAGE:
        return (
          <Packages
            {...this.props}
            fulfillPayment={this.fulfillPayment}
          />
        );
      case FULFILL_PAYMENT:
        return (
          <SelectMethod {...this.props} />
        );
      case PAYMENT_RESULT:
        return (
          <ThankYou {...this.props} />
        );
      default:
        return null;
    }
  }

  renderBackMenu() {
    const { stage } = this.state;
    let backMenuTitle = '';
    switch (stage) {
      case FULFILL_PAYMENT:
        backMenuTitle = 'Hình thức thanh toán';
        break;
      case CHOOSING_PACKAGE:
        backMenuTitle = 'Nâng cấp tài khoản';
        break;
      default :
        backMenuTitle = 'Hoàn thành thanh toán';
    }
    return (
      <div className="site-header is-fixed site-header__blue mbm">
        <nav className="nav nav--3 txt-xlg">
          <Link className="txt-white padding-l10 padding-r10" onClick={this.goBack}>
            <i className="fa fa-arrow-left"></i>
          </Link>
          <span className="txt-xlg">{backMenuTitle}</span>
        </nav>
      </div>
    )
  }

  renderInstruction() {
    return (
      <div className="site-content" style={{marginTop: '-1rem'}}>
        <HeaderContainer {...this.props} />
        <div className="background-blue">
          <SlidePayment />
          <div className="row">
            <div className="col-xs-12 l-flex-center mbm">
              <button
                className="btn btn--t btn--lg l-flex-center"
                onClick={this.choosePackage}
              >
                <img className="img-like margin-r10" src={`${CDN_URL}/general/Payment/Coins/upgrade-icon.png`} alt="tym-icon" />
                Nâng cấp tài khoản ngay
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { stage } = this.state;
    if (stage === INSTRUCTION) {
      return this.renderInstruction();
    }
    return (
      <div className="site-content">
        {this.renderBackMenu()}
        <div className="container">
          <BannerSwapSite />
          {this.renderSteps()}
          <div className='payment-forms'>
            {this.renderPayment()}
          </div>
        </div>
      </div>
    );
  }
}