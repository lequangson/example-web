import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {
  PAYMENT_METHOD_BANK_TRANSFER,
  PAYMENT_METHOD_CREDIT_CARD,
  PAYMENT_METHOD_MOBILE_CARD
} from '../../constants/Payment';
import PaymentSubmit from './PaymentSubmit';
import MobileProviders from './MobileProviders';
import CreditCardProviders from './CreditCardProviders';
import BankProviders from './BankProviders';
import { CDN_URL } from '../../constants/Enviroment';
import { formatNumber } from './Service';

class SelectMethod extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      isMobileCard: false,
      isCreditCard: false,
      isBankTransfer: false,
    }
  }

  componentWillMount() {
    switch(this.props.method) {
      case 'bank-card':
        this.props.updatePaymentState('payment_method', PAYMENT_METHOD_BANK_TRANSFER)
      break;
      case 'credit-card':
        this.props.updatePaymentState('payment_method', PAYMENT_METHOD_CREDIT_CARD)
      break;
      default:
        this.props.updatePaymentState('payment_method', PAYMENT_METHOD_MOBILE_CARD)
      break;
    }
    this.props.updatePaymentState('provider_code', '');
    this.props.updatePaymentState('order', {});
    this.props.updatePaymentState('shopping_cart', []);
    this.props.updatePaymentState('customer_name', '');
    this.props.updatePaymentState('customer_phone_number', '');
    this.props.updatePaymentState('mobile_card_info', [
      {
        input_id: 1,
        card_code: '',
        card_id: '',
        payment_info_id: '',
        payment_status: null
      }
    ]);
  }

  handleClick(payment_method) {
    this.props.updatePaymentState('payment_method', payment_method)
    switch(payment_method) {
      case PAYMENT_METHOD_BANK_TRANSFER:
        this.setState({
          isMobileCard: false,
          isCreditCard: false,
          isBankTransfer: true,
        })
      break;
      case PAYMENT_METHOD_CREDIT_CARD:
        this.setState({
          isMobileCard: false,
          isCreditCard: true,
          isBankTransfer: false,
        })
      break;
      default:
        this.setState({
          isMobileCard: true,
          isCreditCard: false,
          isBankTransfer: false,
        })
      break;
    }
  }

  render() {
    const { package_id, packages, payment_method } = this.props.Payment;
    const { isMobileCard, isCreditCard, isBankTransfer } = this.state;
    const isSelectedPaymentMethod = isMobileCard || isCreditCard || isBankTransfer;
    const current_package = packages.find(pk => pk.id == package_id);
    const real_unit_amount = current_package.package_prices[0].amount + current_package.payment_method_fees[payment_method - 1].fee;

    return (
      <div className="container active payment-segments">
        <div className="row">
          <div className="col-xs-12 padding-0">
            <label className="form__input--p no-hover btn l-flex-vertical-center">
              <input
                type="radio"
                name="payment_method"
                value={PAYMENT_METHOD_MOBILE_CARD}
                checked={this.state.isMobileCard}
                onClick={() => { this.handleClick(PAYMENT_METHOD_MOBILE_CARD) }}
              />
              <div className="fake-radio"></div>
              <img src={`${CDN_URL}/general/Payment/Coins/The-cao.png`} className="img-like" alt={`Thẻ cào điện thoại`}/>
              <span className="padding-l5 txt-blue txt-medium txt-lg">Thẻ cào điện thoại</span>
            </label>
          </div>
          {this.state.isMobileCard && <MobileProviders {...this.props} />}
          <div className="col-xs-12 padding-0">
            <label className="form__input--p no-hover btn l-flex-vertical-center">
              <input
                type="radio"
                name="payment_method"
                value={PAYMENT_METHOD_BANK_TRANSFER}
                checked={this.state.isBankTransfer}
                onClick={() => { this.handleClick(PAYMENT_METHOD_BANK_TRANSFER) }}
              />
              <div className="fake-radio"></div>
              <img src={`${CDN_URL}/general/Payment/Coins/The-ngan-hang.png`} className="img-like" alt={`Thẻ tín dụng`}/>
              <span className="padding-l5 txt-blue txt-medium txt-lg">Thẻ ngân hàng</span>
            </label>
          </div>
          {this.state.isBankTransfer && <BankProviders {...this.props} />}
          <div className="col-xs-12 padding-0">
            <label className="form__input--p no-hover btn l-flex-vertical-center">
              <input
                type="radio"
                name="payment_method"
                value={PAYMENT_METHOD_CREDIT_CARD}
                checked={this.state.isCreditCard}
                onClick={() => { this.handleClick(PAYMENT_METHOD_CREDIT_CARD) }}
              />
              <div className="fake-radio"></div>
              <img src={`${CDN_URL}/general/Payment/Coins/The-tin-dung.png`} className="img-like" alt={`Thẻ tín dụng`}/>
              <span className="padding-l5 txt-blue txt-medium txt-lg">Thẻ Tín dụng</span>
            </label>
          </div>
          {this.state.isCreditCard && <CreditCardProviders {...this.props} />}
          <div className="col-xs-12 well"></div>
          <div className="col-xs-12 well"></div>
        </div>
        {
          <div className="fixed-bottom">
            <div className="container">
              <div className="row">
                <div className="col-xs-8 txt-right">
                  Gói nâng cấp tài khoản
                </div>
                <div className="col-xs-4 txt-red txt-bold">
                  <span>{current_package.name}</span>
                </div>
              </div>
              <div className="row mbs">
                <div className="col-xs-8 txt-right">
                  Thanh toán
                </div>
                <div className="col-xs-4 txt-red txt-bold">
                  <span>{formatNumber(real_unit_amount)} đ</span>
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12">
                  <PaymentSubmit
                    {...this.props}
                    isSelectedPaymentMethod={isSelectedPaymentMethod}
                  />
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {}
}

const SelectMethodContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(SelectMethod)


export default SelectMethodContainer