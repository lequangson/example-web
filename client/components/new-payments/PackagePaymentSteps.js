import React, { Component } from 'react';
import { CDN_URL } from 'constants/Enviroment';

class PackagePaymentStep extends Component {

  static stepTitles = [`Chọn gói<br />nâng cấp`, 'Hình thức<br />thanh toán', 'Hoàn tất']
  static stepIcons = [`${CDN_URL}/general/Payment/StepsIcon/step1.png`, `${CDN_URL}/general/Payment/StepsIcon/step2.png`, `${CDN_URL}/general/Payment/StepsIcon/step3.png`]
  static stepIconsActive = [`${CDN_URL}/general/Payment/StepsIcon/step1.png`, `${CDN_URL}/general/Payment/StepsIcon/step2-active.png`, `${CDN_URL}/general/Payment/StepsIcon/step3-active.png`]
  static STEP_COUNT = 3

  getClassName(className) {
		if (this.props.indexStep) {
			return className + this.props.indexStep;
		}
  }

  getStyle() {
    const { stage } = this.props;
		const completion = stage ? (stage - 2) * 50 : 0;
		return {
  		width: completion + '%'
  	}
	}

  render() {
    const { stage } = this.props;
    const steps = PackagePaymentStep.stepTitles.map((title, index) => ({
      title,
      active: stage - 2 >= index,
      index,
      iconImage: PackagePaymentStep.stepIcons[index],
      iconImageActive: PackagePaymentStep.stepIconsActive[index],
    }))

    return (
      <div className="payment-steps col-xs-12 pos-relative mbm">
        <div className="payment-steps__bar">
          <div className={this.getClassName('payment-steps__fill payment-steps__fill--')} style={this.getStyle()}></div>
        </div>
        <div className="payment-steps__txt l-flex-sb" style={{ alignItems: 'flex-start'}}>
        {
          steps.map((step, index)=> {
            return (
              <div key={index} className="payment-steps__item txt-center">
                <div className="txt-center mbs">
                  <img src={step.active ? step.iconImageActive : step.iconImage} className="img-50" alt={`Bước ${step.index}`}/>
                </div>
                <span className={step.active ? "txt-blue txt-medium" : "txt-medium" } dangerouslySetInnerHTML={{ __html: step.title }}></span>
              </div>
            )
          })
        }
        </div>
      </div>
    );
  }
}

export default PackagePaymentStep;