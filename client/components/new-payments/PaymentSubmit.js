import React, { Component } from 'react';
import { connect } from 'react-redux';
import q from 'q';
import { browserHistory } from 'react-router';
import { NOTIFICATION_ERROR, NOTIFICATION_ERROR_DEFAULT_TIME, GA_ACTION_COIN_BUY } from '../../constants/Enviroment';
import { processPayment } from '../../actions/Payment';
import { PAYMENT_METHOD_BANK_TRANSFER, PAYMENT_METHOD_CREDIT_CARD, PAYMENT_METHOD_MOBILE_CARD, PAYMENT_PACKAGE_TYPE_COIN, PAYMENT_PACKAGE_ID_WEEKLY } from '../../constants/Payment';
import { isEmpty } from 'lodash';
import { getCurrentUser } from '../../actions/userProfile';
import { sendGoogleAnalytic } from '../../actions/Enviroment';

class PaymentSubmit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inputRequired: [],
      paymentProcessClicked: false
    }
    this.handlePaymentSubmit = this.handlePaymentSubmit.bind(this);
  }
  componentWillMount() {
    this.setState({paymentProcessClicked: false});
  }

  validatePayment(paymentInput) {
    if (!paymentInput.package_id) {
      this.props.sendNotification('Bạn chưa chọn gói thanh toán', NOTIFICATION_ERROR_DEFAULT_TIME, NOTIFICATION_ERROR);
      return false;
    }
    if (!paymentInput.provider_code) {
      let error_msg = 'Bạn chưa chọn loại thẻ';
      if (paymentInput.payment_method == PAYMENT_METHOD_BANK_TRANSFER) {
        error_msg = 'Bạn chưa chọn ngân hàng';
      }
      if (paymentInput.payment_method == PAYMENT_METHOD_MOBILE_CARD) {
        error_msg = 'Bạn chưa chọn nhà mạng';
      }
      this.props.sendNotification(error_msg, NOTIFICATION_ERROR_DEFAULT_TIME, NOTIFICATION_ERROR);
      return false;
    }
    if (paymentInput.payment_method != PAYMENT_METHOD_MOBILE_CARD) { // bank card or visa bat
      if (!paymentInput.customer_name.trim().length || !paymentInput.customer_phone_number.trim().length) {
        if (!paymentInput.customer_name.trim().length) {
          document.getElementById("customer_name").focus();
        } else {
          document.getElementById("customer_phone_number").focus();
        }
        this.props.sendNotification('Vui lòng nhập đủ tên và số điện thoại trước khi thanh toán', NOTIFICATION_ERROR_DEFAULT_TIME, NOTIFICATION_ERROR);
        return false;
      }
      if(/^[a-zA-Z0-9- ]*$/.test(paymentInput.customer_phone_number) == false) {
        document.getElementById("customer_phone_number").focus();
        this.props.sendNotification('Số diện thoại không hợp lệ', NOTIFICATION_ERROR_DEFAULT_TIME, NOTIFICATION_ERROR);
        return false;
      }
    }
    if (paymentInput.payment_method == PAYMENT_METHOD_MOBILE_CARD) { // bank card or visa bat
      // const inputsError = paymentInput.mobile_card_info.filter(input => !input.card_code.length || !input.card_id.length).map(item => item);
      const inputsError = [];
      const card_codes = [];
      const card_serial = [];
      let no_change_after_process = false;
      let total_valid_input_card = 0;
      paymentInput.mobile_card_info.map((item, i) => {
        if (item.payment_status != 1) {
          no_change_after_process = true;
          total_valid_input_card ++;
        }
        card_codes.push(item.card_code);
        card_serial.push(item.card_id);
        if (!item.card_code.trim().length || !item.card_id.trim().length) {
          inputsError.push(item.input_id);
        }
      })

      if (!total_valid_input_card) {
        this.props.sendNotification('Bạn chưa nhập thêm thẻ', NOTIFICATION_ERROR_DEFAULT_TIME, NOTIFICATION_ERROR);
        return false;
      }
       if (!no_change_after_process) {
        this.props.sendNotification('Bạn chưa cập nhật thông tin bị sai', NOTIFICATION_ERROR_DEFAULT_TIME, NOTIFICATION_ERROR);
        return false;
      }
      if (inputsError.length) {
        this.props.sendNotification('Vui lòng nhập đủ thông tin thẻ trước khi thanh toán', NOTIFICATION_ERROR_DEFAULT_TIME, NOTIFICATION_ERROR);
        this.props.updatePaymentState('error_required', inputsError);
        return false;
      }
      const duplicated_code = card_codes.filter(function(value,index,self){ return (self.indexOf(value) !== index )});
      this.props.updatePaymentState('duplicated_code', duplicated_code);
      const duplicated_serial = card_serial.filter(function(value,index,self){ return (self.indexOf(value) !== index )});
      // this.props.updatePaymentState('duplicated_serial', []);
      this.props.updatePaymentState('duplicated_serial', duplicated_serial);
      if (duplicated_code.length || duplicated_serial.length) {
        return false;
      }
    }

    return true;
  }

  handlePaymentSubmit() {
    if (this.state.paymentProcessClicked) {
      return;
    }
    this.setState({paymentProcessClicked: true});
    const { Payment } = this.props;
    const {
      payment_method, package_id, provider_code, customer_name,
      customer_phone_number, mobile_card_info, shopping_cart, package_type,
    } = Payment;

    const paymentInput = {
      payment_method,
      package_id,
      provider_code,
      customer_name,
      customer_phone_number,
      mobile_card_info
    }
    if (!isEmpty(shopping_cart.shopping_cart)) {
      paymentInput.shopping_cart_id = shopping_cart.shopping_cart.id;
    } else {
      paymentInput.shopping_cart_id = null;
    }
    if (this.validatePayment(paymentInput)) {
      if (payment_method != PAYMENT_METHOD_MOBILE_CARD) {
        paymentInput.mobile_card_info = null;
      } else {
        paymentInput.mobile_card_info = paymentInput.mobile_card_info.filter(card => card.payment_status != 1);
      }
      const that = this;
      that.props.createShoppingCart(paymentInput).then(response => {
        if (typeof response.shopping_cart_id !== 'undefined') {
          that.props.processPayment(response.shopping_cart_id).then(payment_process_response => {
            const { order,  payment_gateway_url } = payment_process_response;
            that.props.getCurrentUser()
            if (package_type === PAYMENT_PACKAGE_TYPE_COIN) {
              this.props.gaSend(GA_ACTION_COIN_BUY, {})
            }
            if (package_id === PAYMENT_PACKAGE_ID_WEEKLY) {
              const gaTrackData = {
                page_source: 'PAYMENT_PAGE'
              };
              const eventAction = {
                eventCategory: 'PAYMENT',
                eventAction: 'Buy',
                eventLabel: 'Buy weekly package'
              }
              this.props.sendGoogleAnalytic(eventAction, gaTrackData)
            }

            if (payment_method == PAYMENT_METHOD_MOBILE_CARD && order) {
              // success and redirect to thankyou page
              browserHistory.push('/payment/thankyou');
              return;
            }
            if (payment_method != PAYMENT_METHOD_MOBILE_CARD && typeof payment_gateway_url != 'undefined') {
              window.location.href = payment_gateway_url;
              return;
            }
          }).catch(payment_response => {
            const payment_process_response = payment_response.response.data;
            let has_card_success = [];
            if( typeof payment_process_response.shopping_cart !== 'undefined') {
              has_card_success = payment_process_response.shopping_cart.shopping_cart_payment_infos.filter(item => item.payment_status == 1)
            }
            // if (
            //   has_card_success.length &&
            //   typeof payment_process_response.error_message !== 'undefined' &&
            //   payment_process_response.error_message.length
            // ) {
            //   this.props.sendNotification(payment_process_response.error_message, NOTIFICATION_ERROR_DEFAULT_TIME, NOTIFICATION_ERROR);
            // }
            this.setState({paymentProcessClicked: false});
            const { order,  payment_gateway_url } = payment_process_response;
            if (payment_method == PAYMENT_METHOD_MOBILE_CARD && order) {
              // success and redirect to thankyou page
              browserHistory.push('/payment/thankyou');
              return;
            }
            if (payment_method != PAYMENT_METHOD_MOBILE_CARD && typeof payment_gateway_url != 'undefined') {
              window.location.href = payment_gateway_url;
              return;
            }

            this.setState({paymentProcessClicked: false});
          })
        }
      }).catch(error => {
        this.setState({paymentProcessClicked: false});
      })
    } else {
      this.setState({paymentProcessClicked: false});
    }

  }

  render() {
    const { payment_method } = this.props.Payment;
    const buttonTitle = payment_method == PAYMENT_METHOD_MOBILE_CARD ? 'Hoàn tất thanh toán' : 'Đến cổng VTC Pay để hoàn tất';
    return (
      <div className="col-xs-12 mbs">
        <button
          className="btn btn--p btn--b"
          disabled={this.state.paymentProcessClicked || !this.props.isSelectedPaymentMethod}
          onClick={this.handlePaymentSubmit}
        >
          {buttonTitle}
        </button>
        <div className={this.state.paymentProcessClicked ? 'loader is-full-screen-load' : 'hide loader is-full-screen-load'}>
          <span className="loader__text">Đang xử lý giao dịch</span>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    processPayment: (shopping_cart_id) => {
      const defer = q.defer()
      dispatch(processPayment(shopping_cart_id)).then(response => {
        defer.resolve(response.response.data)
      }).catch((err) => {
        defer.reject(err)
      });
      return defer.promise
    },
    getCurrentUser: () => {
      dispatch(getCurrentUser());
    },
    sendGoogleAnalytic: (eventAction, ga_track_data) =>{
      dispatch(sendGoogleAnalytic(eventAction, ga_track_data))
    },
  }
}

const PaymentSubmitContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(PaymentSubmit)


export default PaymentSubmitContainer

