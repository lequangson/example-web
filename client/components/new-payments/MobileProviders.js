import React, { Component } from 'react'
import { browserHistory } from 'react-router';
import Packages from './Packages';
import MobileCardsInfo from './MobileCardsInfo';
import {
  CDN_URL
} from 'constants/Enviroment'

class MobileProviders extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isViettel : false,
      isMobilephone : false,
      isVinaphone : false,
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(provider_code) {
    this.props.updatePaymentState('provider_code', provider_code);
    if(provider_code === 'VTEL') {
      this.setState({isViettel: true, isMobilephone: false, isVinaphone : false})
    }
    if(provider_code === 'VMS') {
      this.setState({isViettel: false, isMobilephone: true, isVinaphone : false})
    }
    if(provider_code === 'GPC') {
      this.setState({isViettel: false, isMobilephone: false, isVinaphone : true})
    }
  }

  render() {
    return (
      <div className="nav-1__content">
        <div className="col-xs-12">
          <div className="padding-b20 padding-t20 txt-blue txt-bold"><span>Chọn một nhà mạng</span></div>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('VTEL')}}>
          <div className={this.state.isViettel ? "img-payment active" : "img-payment"}><img src={`${CDN_URL}/general/viettel.png`} /></div>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('VMS')}}>
          <div className={this.state.isMobilephone ? "img-payment active" : "img-payment"}><img src={`${CDN_URL}/general/mobiphone.png`} /></div>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('GPC')}}>
          <div className={this.state.isVinaphone ? "img-payment active" : "img-payment"}><img src={`${CDN_URL}/general/Payment/vinaphone.png`} /></div>
        </div>
        <MobileCardsInfo {...this.props} />
      </div>
    )
  }
}

export default MobileProviders
