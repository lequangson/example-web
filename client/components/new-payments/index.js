import { connect } from 'react-redux';
import q from 'q';
import Payment from './Payment';
import {
  getPaymentPackages, updatePaymentState, createShoppingCart,
} from 'actions/Payment';

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    getPaymentPackages: package_type => {
      dispatch(getPaymentPackages(package_type))
    },
    updatePaymentState: (key, value) => {
      dispatch(updatePaymentState(key, value))
    },
    createShoppingCart: payment_input => {
      const defer = q.defer()
      dispatch(createShoppingCart(payment_input)).then(response => {
        defer.resolve(response.response.data)
      }).catch((err) => {
        defer.reject(err)
      });
      return defer.promise
    },
  }
}

export default connect(mapStateToProps, mapDispachToProps)(Payment)
