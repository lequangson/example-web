import React, { Component } from 'react'
import { connect } from 'react-redux'

class UserInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    this.handleKeyUpName = this.handleKeyUpName.bind(this);
    this.handleKeyUpPhoneNumber = this.handleKeyUpPhoneNumber.bind(this);
  }

  handleKeyUpName(event) {
    this.props.updatePaymentState('customer_name', event.target.value);
  }


  handleKeyUpPhoneNumber(event) {
    this.props.updatePaymentState('customer_phone_number', event.target.value);
  }

  render() {
    const { Payment } = this.props;
    const { customer_name, customer_phone_number } = Payment;
    return (
      <div className="col-xs-12 mbm">
        <div className="padding-t20 txt-blue txt-bold mbm"><span>Nhập thông tin của bạn</span></div>
        <div>Tên</div>
        <input id="customer_name" className="mbs txt-blue" type="text" placeholder="" defaultValue={customer_name} onKeyUp={this.handleKeyUpName}/>
        <div>Số điện thoại </div>
        <div className="form__input--rel-before">
          <input id="customer_phone_number" className="txt-blue" type="text" placeholder="" defaultValue={customer_phone_number} onKeyUp={this.handleKeyUpPhoneNumber} />
          <label>(+84)</label>
        </div>
      </div>
    )
  }
}


function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {}
}

const UserInfoContainer = connect(
  mapStateToProps,
  mapDispachToProps,
)(UserInfo)

export default UserInfoContainer
