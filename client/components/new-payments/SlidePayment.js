import React, { PropTypes } from 'react'
import { connect } from 'react-redux';
import $ from 'jquery'
import slick from '../../../public/scripts/slick.min.js'
import { isEmpty } from 'lodash';
import { CDN_URL } from '../../constants/Enviroment'
import {
  PAYMENT_PACKAGE_TYPE_COIN, PAYMENT_PACKAGE_TYPE_MONTHLY,
} from '../../constants/Payment'

export default class SlidePayment extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      monthly_package_instructions: [
        {
          icon_url : `${CDN_URL}/general/Payment/Coins/unlock-chat.png`,
          featureName: 'Trò chuyện không giới hạn',
          description: 'Trò chuyện không giới hạn với tất cả các <br /> thành viên đã kết đôi <br /> Xem được danh sách ai đã quan tâm bạn <br /> Nhắc cô ấy thích lại bạn'
        },
        {
          icon_url : `${CDN_URL}/general/Payment/Coins/boost-in-rank.png`,
          featureName: 'Nổi bật ngay',
          description: 'Hồ sơ của bạn sẽ nổi bật giữa hàng<br /> ngàn người khác và nhận nhiều lượt thích <br /> hơn và được ghép đôi nhiều hơn! <br /> Cơ hội tìm được nửa kia chưa bao giờ <br /> dễ dàng đến thế'
        },
        {
          icon_url : `${CDN_URL}/general/Payment/Coins/advanced-search.png`,
          featureName: 'Tìm kiếm nâng cao',
          description: 'Bộ lọc nhiều tiêu chí hơn giúp tìm kiếm <br /> nửa kia dễ dàng và chính xác hơn!'
        }
      ]
    }
  }

  _carousel() {

    // payment carousel
    const carousel = $('.carousel-payment');
    if (!carousel) {
      return;
    }
    carousel.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 7000,
      initialSlide: 0,
      focusOnSelect: true,
      dots: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            dots: true
          },
        },
        {
          breakpoint: 425,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    })
  }

  _unCarousel() {
    //if ($('.carousel-form .carousel-payment')) {
    if ($('.carousel-payment')) {
      $('.carousel-payment').slick('unslick')
    }
  }

  componentDidMount() {
    this._carousel()
  }


  componentWillUpdate(nextProps, nextState) {
    if (nextProps.package_type !== this.props.package_type ||
      nextProps.gender !== this.props.gender) {
      this._unCarousel()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.package_type !== this.props.package_type ||
      prevProps.gender !== this.props.gender) {
      this._carousel()
    }
  }


  render() {
    return (
      <div className="panel mbm">
        <div className="clearfix">
          <div className="col-xs-12">
            <div className="carousel-payment" >
              {
                this.state.monthly_package_instructions.map((value, i) => {
                  return <div className="txt-center padding-r10" key={i}>
                      <div className="row">
                        <div className="col-xs-4 col-xs-offset-4">
                          <div className="card__image mbl">
                            <img src={value.icon_url} alt="ymeet.me" className="mbs img-100"/>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-xs-12">
                          <div className="txt-white txt-uppercase txt-xlg txt-medium mbm">{value.featureName}</div>
                          <div className="txt-white" dangerouslySetInnerHTML={{ __html: value.description }}></div>
                        </div>
                      </div>
                    </div>
                  })
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

SlidePayment.propTypes = {
  package_type: PropTypes.string,
  gender: PropTypes.string
}
