import React, { Component } from 'react'
import { browserHistory } from 'react-router';
import Packages from './Packages';
import UserInfo from './UserInfo';
import { BANK_CODES } from '../../constants/Payment';
import $ from 'jquery';

class BankProviders extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(package_id) {
    this.props.updatePaymentState('provider_code', package_id)
    $(`#bank_${package_id}`).click();
  }

  render() {
    const { Payment } = this.props;
    const { bank_codes } = Payment;
    return (
      <div className="nav-1__content padding-col-2">
      <div className="col-xs-12">
        <div className="padding-b20 padding-t20 txt-blue txt-bold"><span>Chọn một ngân hàng</span></div>
      </div>
      {
        bank_codes.map((bank, i) => <div className="col-xs-3" key={i} onClick={() => {this.handleClick(bank.code)}}>
          <div className="bank__border">
            <div className="bank__img"><img src={bank.image} /></div>
            <label className="form__input--d bank__radio">
              <input type="radio" name="Bank" id={`bank_${bank.code}`} onClick={() => {this.handleClick(bank.code)}} />
              <div className="fake-radio"></div>
            </label>
          </div>
        </div>)
      }
      <UserInfo {...this.props} />
    </div>
    )
  }
}

export default BankProviders
