import React, { Component } from 'react'
import { browserHistory } from 'react-router';
import Packages from './Packages';
import UserInfo from './UserInfo';
import $ from 'jquery';
import {
  CDN_URL
} from 'constants/Enviroment'

class CreditCardProviders extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isVisa : false,
      isMaster : false,
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(package_id) {
    this.props.updatePaymentState('provider_code', package_id);
    if(package_id === 'Visa') {
      this.setState({isVisa: true, isMaster: false})
    }
    if(package_id === 'Master') {
      this.setState({isVisa: false, isMaster: true})
    }
  }

  render() {
    return (
      <div className="nav-1__content">
        <div className="col-xs-12">
          <div className="padding-b20 padding-t20 txt-blue txt-bold"><span>Chọn một loại thẻ</span></div>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('Visa')}}>
          <div className={this.state.isVisa ? "img-payment active" : "img-payment"}><img src={`${CDN_URL}/general/visa.png`} /></div>
        </div>
        <div className="col-xs-4 txt-center" onClick={() => {this.handleClick('Master')}}>
          <div className={this.state.isMaster ? "img-payment active" : "img-payment"}><img src={`${CDN_URL}/general/master_card.png`} /></div>
        </div>
        <UserInfo {...this.props} />
      </div>
    )
  }
}

export default CreditCardProviders
