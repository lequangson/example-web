import React, { Component, PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import Cropper from 'react-cropper';
import _ from 'lodash';
import 'cropperjs/dist/cropper.css';
import DropdownPanel from 'components/commons/DropdownPanel'
import {
  AVATAR_PICTURE, UPLOAD_FACEBOOK_MODE, UPLOAD_DEVIDE_MODE
} from 'constants/userProfile';
import { sendNotification } from 'utils/errors/notification';
import {
  NOTI_LABEL_AVATAR_PROCESSING, NOTI_MSG_AVATAR_PROCESSING,
  NOTI_LABEL_AVATAR_UPLOADED_SUCCESS, SUCCESS_MSG_HAPPY_TIME
} from 'constants/TextDefinition';
import { NOTIFICATION_SUCCESS, NOTIFICATION_INFOR } from 'constants/Enviroment';
import { WELCOME_PAGE_2 } from 'containers/welcome/Constants';

class ChangeAvatar extends Component {

  constructor(props) {
    super(props)
    this.state = {
      imageLoaded: false,
      url: '',
      mode: 0,
      fileName: '',
      fileSize: ''
    }
  }

  componentDidMount() {
    const file = this.props.Enviroment.file;
    const pictureId = this.props.params.mode;

    if (!_.isEmpty(file)) {
      if (file.mode == UPLOAD_FACEBOOK_MODE) {
        this.setState({ url: file.url, mode: UPLOAD_FACEBOOK_MODE });
      }
    }
    else {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        this.setState({
          url: reader.result,
          mode: UPLOAD_DEVIDE_MODE,
          fileName: file.name,
          fileSize: file.size,
        })
      }, false);
      if (_.isEmpty(file) && pictureId !== 'other-picture') {
        reader.readAsDataURL(file);
      }
    }
  }

  onImageFullyLoaded = () => {
    if (!this.state.imageLoaded) {
      this.setState({ imageLoaded: true });
    }
  }

  handleSetAvatar = () => {
    const mode = this.props.params.mode;
    const pictureId = this.props.params.id;
    const cropBoxData = this.avatar.getData(true);
    const cropData = {
      x: cropBoxData.x,
      y: cropBoxData.y,
      w: cropBoxData.width,
      h: cropBoxData.height,
    };

    if (mode == 'other-picture') {
      this.props.setAvatar(pictureId, cropData).then(() => {
        //this.props.getCurrentUser()
        sendNotification(NOTIFICATION_SUCCESS, NOTI_LABEL_AVATAR_UPLOADED_SUCCESS, SUCCESS_MSG_HAPPY_TIME, 7000);
      })
      browserHistory.push('/myprofile');
      sendNotification(NOTIFICATION_INFOR, NOTI_LABEL_AVATAR_PROCESSING, NOTI_MSG_AVATAR_PROCESSING, 7000);
    } else {
      if (this.state.mode === UPLOAD_DEVIDE_MODE) {
        this.props.uploadImage(AVATAR_PICTURE, this.state.fileName, this.state.fileSize, this.state.url, cropData, this.props.Enviroment.upload_picture_page_source).then(() => {
          sendNotification(NOTIFICATION_SUCCESS, NOTI_LABEL_AVATAR_UPLOADED_SUCCESS, SUCCESS_MSG_HAPPY_TIME, 7000);
        })
        browserHistory.push('/myprofile');
        sendNotification(NOTIFICATION_INFOR, NOTI_LABEL_AVATAR_PROCESSING, NOTI_MSG_AVATAR_PROCESSING, 7000);
      }
      if (this.state.mode === UPLOAD_FACEBOOK_MODE) {
        this.props.synchFacebookPictures([this.state.url], AVATAR_PICTURE, cropData, this.props.Enviroment.upload_picture_page_source).then(() => {
          sendNotification(NOTIFICATION_SUCCESS, NOTI_LABEL_AVATAR_UPLOADED_SUCCESS, SUCCESS_MSG_HAPPY_TIME, 7000);
        })
        browserHistory.push('/myprofile');
        sendNotification(NOTIFICATION_INFOR, NOTI_LABEL_AVATAR_PROCESSING, NOTI_MSG_AVATAR_PROCESSING, 7000);
      }
    }
  }

  render() {
    const mode = this.props.params.mode;
    const user = this.props.user_profile.current_user;
    const userPicture = (user && user.user_pictures) ? user.user_pictures.filter(picture => picture.id === parseInt(this.props.id, 10)) : '';
    const pic = userPicture.length > 0 ? userPicture[0] : '';
    const isUploadingAvatar = this.props.user_profile.isUploadingAvatar;

    return (
      <div className="site-content" >
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3 className="txt-heading">Thay ảnh đại diện</h3>
            </div>
          </div>
          <DropdownPanel
            panelTitle="Bí quyết để có bức ảnh đại diện thu hút"
            isOpen={false}
          >
            <div className="col-xs-12">
              <div className="mbs"></div>
              <ol>
                <li><span className="txt-blue txt-bold">Hãy là chính mình. </span>
                  Tránh để ảnh người khác, vật nuôi, phong cảnh, đồ vật, hình vẽ...
                </li>
                <li><span className="txt-blue txt-bold">Cười </span>lên nào.</li>
                <li><span className="txt-blue txt-bold">Tập trung vào bạn. </span>
                  Nếu là ảnh nhóm, hãy làm mờ những người khác.
                </li>
                <li><span className="txt-blue txt-bold">Ảnh rõ ràng </span>và không chỉnh sửa thái quá.</li>
                <li>Một bức <span className="txt-blue txt-bold">ảnh mới </span>luôn luôn tốt hơn một bức ảnh cũ.</li>
              </ol>
            </div>
          </DropdownPanel>
          <div className="row">
            <div className="col-xs-12 mbm">
              <article className={isUploadingAvatar || !this.state.imageLoaded ? 'is-loading cropper-card' : ''} >
                <Cropper
                  ref={(c) => { this.avatar = c }}
                  aspectRatio={1}
                  src={(this.props.params.mode === 'other-picture') ? pic.original_image_url : this.state.url}
                  checkCrossOrigin={false}
                  viewMode={2}
                  dragMode="move"
                  autoCropArea={0.8}
                  ready={this.onImageFullyLoaded}
                  background={false}
                  className="cropper-container"
                  // zoomable={false}
                />
              </article>
            </div>
            <div className="col-xs-6">
              <button
                className="btn btn--b"
                onClick={() => window.history.back()}
                disabled={mode === 'welcome' ? isUploadingAvatar : false}
              >
                Quay Lại
              </button>
            </div>
            <div className="col-xs-6 mbm">
              <button
                className="btn btn--p btn--b"
                onClick={this.handleSetAvatar}
                disabled={isUploadingAvatar || !this.state.imageLoaded}
              >
                Đồng ý
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ChangeAvatar.propTypes = {
  id: PropTypes.string.isRequired,
  Enviroment: PropTypes.object,
  params: PropTypes.object,
  user_profile: PropTypes.object,
  setAvatar: PropTypes.func,
  getCurrentUser: PropTypes.func,
  uploadImage: PropTypes.func,
  synchFacebookPictures: PropTypes.func,
}

export default ChangeAvatar;
