/* global document: true */

import React, { Component, PropTypes } from 'react'
import $ from 'jquery'
import { Link } from 'react-router'
import moment from 'moment';
import {
  MATCHED_PAGE, DEFAULT_LOAD_FEMALE, DEFAULT_LOAD_MALE, CDN_URL,
  GA_ACTION_VIEW_ADMIN_MESSAGE, ACTIVITY_VIEW_ADMIN_MESSAGE,
} from '../constants/Enviroment'
import { THERE_IS_NO_ONE_I_MATCHED_MESSAGE, LOADING_DATA_TEXT_AFTER_4SECS, LOADING_DATA_TEXT } from '../constants/TextDefinition'
import { GHOST_USER } from '../constants/userProfile';
import ListItem from './searches/ListItem'
import * as ymmStorage from '../utils/ymmStorage'
import RandomMatchBanner from './commons/RandomMatchBanner'
import { subscribeUser } from '../src/scripts/userSubscription'
import RandomMatchButton from './commons/RandomMatchButton'
import GoogleOptimize from '../utils/google_analytic/google_optimize';

class MatchedList extends Component {
  constructor() {
    super()

    this.openChatSupport = this.openChatSupport.bind(this)
  }

  componentWillMount() {
    ymmStorage.removeItem('keep')
    this.props.getPeopleMatchedMe()
    this.props.updatePreviousPage(MATCHED_PAGE)
    setTimeout(() => {this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT_AFTER_4SECS)}, 4000)
  }

  @GoogleOptimize
  componentDidMount() {
    const userCardHeight = $('.element_height').height();
    const segmentIndex = this.props.Enviroment.profile_next_user_index * userCardHeight;
    window.scroll(0, this.props.Enviroment.matched_list_scroll_top + segmentIndex);
  }

  componentWillUnmount() {
    this.props.updateScrollPossition(MATCHED_PAGE, window.scrollY)
    this.props.setProfileUserNextIndex(0)
    this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT)
  }

  openChatSupport(props) {
    this.props.gaSend(GA_ACTION_VIEW_ADMIN_MESSAGE, { page_source: 'New Message' });

    const chat_support_identifier = moment().format('DDMMYY') + process.env.CUSTOMER_SUPPORT_USER_ID + _.random(10, 99);
    // Add to activity audit
    this.props.addActivityAudit(ACTIVITY_VIEW_ADMIN_MESSAGE, chat_support_identifier, null, null);
    subscribeUser(props)
    window.open(`${process.env.CHAT_SERVER_URL}chat/${chat_support_identifier}`);
  }

  render() {
    const match_list = this.props.user_profile.matched_users
    const isLoaded = this.props.user_profile.is_page_loading.matched_users
    const { has_message_from_admin, current_user } = this.props.user_profile;
    const loadingText = this.props.Enviroment.isLoadingText
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE

    return (
      <div className="site-content">
        <div className="container">

          {/*has_message_from_admin && <div className="row">
            <div className="col-xs-12">
              <a href="#" onClick={() => this.openChatSupport(this.props)} className="banner mbm">
                <div className="frame frame--xsm frame--1">
                  <img src={`${CDN_URL}/general/Mini_Logo_Ymeetme.png`} alt="Admin Support" />
                </div>
                <div className="banner__content">
                  <h3 className="banner__heading">Ymeet.me</h3>
                  <p className="banner__sub-heading">Bạn có tin nhắn mới từ hệ thống</p>
                </div>
                <div className="banner__meta">
                  <div className="banner__noti" id="notification-no">1</div>
                </div>
              </a>
            </div>
          </div>
          */}

          <div className="row">
            <div className="col-xs-12">
              <RandomMatchBanner
                current_user={this.props.user_profile.current_user}
                gaSend={this.props.gaSend}
                {...this.props}
              />
            </div>
          </div>
          <div className="row mbm">
            <div className="col-xs-12">
              <h3 className="txt-heading">Danh sách kết đôi thành công</h3>
            </div>
          </div>
          <div className="">
            {
              isLoaded ?
              match_list.length > 0 ?
              (match_list.map((user, i) =>
                <ListItem
                  key={i}
                  id={`dtail-view-${i}`}
                  index={i}
                  data={user}
                  pageSource={MATCHED_PAGE}
                  activeLock={user.have_permission}
                  activeSuperLike={true}
                />
              ))
              :
               <div className="row">
                 <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                   <article className="mbl mc">
                     <div className="card__upper">
                       <div href="#" className="card__link mbm">
                         <img src={default_image} alt="" className="card__img"/>
                       </div>
                       <div className="txt-center txt-light txt-lg mbm">
                         Những người kết đôi thành công và kết đôi ngẫu nhiên với bạn sẽ được hiển thị ở đây.<br />Thử kết đôi ngẫu nhiên và trò chuyện ngay nhé!
                       </div>
                       {
                        current_user.user_status !== GHOST_USER &&
                        <RandomMatchButton className='btn btn--b btn--p' buttonName='Ghép đôi ngẫu nhiên nào' {...this.props} />
                       }
                     </div>
                   </article>
                 </div>
               </div>
              :
              <div className="row">
                <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                  <article className="mbl mc">
                    <div className="card__upper">
                      <div href="#" className="card__link">
                        <img src={default_image} alt="" className="card__img"/>
                      </div>
                      <div className="card__center loader is-load">
                        <div className="mbs">{loadingText}</div>
                        <i className="fa fa-spinner fa-3x"></i>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
}

MatchedList.propTypes = {
  getPeopleMatchedMe: PropTypes.func.isRequired,
  updatePreviousPage: PropTypes.func.isRequired,
  user_profile: PropTypes.object.isRequired,
  Enviroment: PropTypes.object.isRequired,
  updateScrollPossition: PropTypes.func.isRequired,
  setProfileUserNextIndex: PropTypes.func,
  addActivityAudit: PropTypes.func,
  gaSend: PropTypes.func,
}

export default MatchedList
