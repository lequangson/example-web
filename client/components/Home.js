import React, { PropTypes } from 'react';
import cookie from 'react-cookie';
import { NotificationManager } from 'components/commons/Notifications';
import { browserHistory, Link } from 'react-router'

/*import express from "express";
import cookieParser from "cookie-parser";
import React from "react/addons";*/
import { Experiment, Variant } from "react-ab"
import _ from 'lodash'
import $ from 'jquery'
import Siema from 'siema/dist/siema.min.js'
import '../../public/styles/m-main.css'
import '../../public/styles/notification.css'
import logo2 from '../../public/images/logo-2.png'
import text from '../../public/images/text.png'
import { LOGGING_TEXT, AUTO_LOGIN_ERROR_TITLE, AUTO_LOGIN_ERROR_MSG } from '../constants/TextDefinition'
import { BLOG_URL } from '../constants/Enviroment'
import AnonymousFooterWithLogo from './footers/AnonymousFooterWithLogo';
import LandingPage1 from './LandingPage/LandingPage1'
import LandingPage2 from './LandingPage/LandingPage2'
import * as ymmStorage from '../utils/ymmStorage';

const responseFacebook = () => {
  // console.log('facebook response',response);
};

class Home extends React.Component {

  componentWillMount() {
      const { user_profile } = this.props;
      const { current_user } = user_profile;
      if (!_.isEmpty(current_user)) {
        const is_have_avatar = current_user.user_pictures.filter(picture => picture.is_main)
        const hasDoneEditProfile = Boolean(is_have_avatar.length !== 0 && current_user.self_introduction);
        if (hasDoneEditProfile) {
          // case finished all - normal case
          browserHistory.push('/whathot')
        } else {
          // case not filled in intro or set avatar
          browserHistory.push('/myprofile')
        }
      }
  }

  componentDidMount() {
    this.landingPageScript();
  }

  componentDidUpdate() {
    const { errorMessage } = this.props.auth
    const is_have_avatar = !_.isEmpty(this.props.user_profile.current_user) ? this.props.user_profile.current_user.user_pictures.filter(picture => picture.is_main) : false
    const hasDoneEditProfile = Boolean(is_have_avatar.length !== 0 && this.props.user_profile.current_user.self_introduction)
    if (errorMessage) {
      if ($('.notification-error').length) {
          $('.notification-error').hide()
      }
      if ($('.notification-info').length) {
        $('.notification-info').hide()
      }
      if (window.location.href.indexOf('autoLogin=true') !== -1) {
        NotificationManager.info(AUTO_LOGIN_ERROR_MSG, AUTO_LOGIN_ERROR_TITLE, 600000);
        browserHistory.push('/');
        return;
      } else {
        NotificationManager.error(errorMessage, 'Đăng nhập không thành công.', 600000);
      }
    }

    if (ymmStorage.getItem('ymm_token') && this.props.user_profile.current_user.welcome_page_completion != null) {
      if (this.props.user_profile.current_user.welcome_page_completion > 3) {
        if (hasDoneEditProfile) {
          browserHistory.push('/whathot')
        }
        else {
          browserHistory.push('/myprofile')
        }
      }
      else {
          browserHistory.push('/myprofile')
      }
    }
  }

  componentWillUnmount() {
    const h = document.getElementsByTagName('html')[0];
    h.classList.remove('landingpage');
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      const html = document.getElementsByTagName('html')[0];
      html.classList.add('no-touch');
    }
  }

  choice(experiment, variant, index) {
    // Sending the pageview no longer requires passing the page
    // value since it's now stored on the tracker object.
    /*ga('send', 'pageview', {
      'dimension8' : variant
    });*/
  }

  landingPageScript() {
    // remove no-touch class
    const html = document.getElementsByTagName('html')[0];
    html.classList.remove('no-touch');
    html.classList.add('landingpage');

    // hamburger button
    const ham = document.querySelector('.js-hamburger');
    const nav = document.querySelector('.js-nav');
    if (ham) {
      ham.addEventListener('click', () => {
        ham.classList.toggle('is-active');
        nav.classList.toggle('is-show');
      });
    }

  }

  render() {
    if (this.props.params.mode || true) {
      return (
        <div>
          {this.props.params.mode === '1'? <LandingPage2 {...this.props} /> : <LandingPage2 {...this.props} />}
        </div>
      )
    }

    return (
      <div>
        <Experiment {...this.props} onChoice={this.choice} name="LandingPage">
          <Variant name="LandingPage1">
            <LandingPage1 {...this.props} />
          </Variant>
          <Variant name="LandingPage2">
            <LandingPage2 {...this.props} />
          </Variant>
        </Experiment>
      </div>
    );
  }
}

Home.propTypes = {
  auth: PropTypes.object,
}
export default Home;
