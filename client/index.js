import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory} from 'react-router';
import routes from './routes';
import { createStore, applyMiddleware } from 'redux'
import reducers from './reducers'
import {
  getCurrentUserRequest, getCurrentUser, getCurrentUserFailure, getBlockedUsers,
  getLuckySpinnerGift,
} from './actions/userProfile'
import { getPeopleILiked } from './actions/PeopleILiked'
import { getDictionariesRequest, getDictionaries, getDictionariesFailure } from 'pages/dictionaries/Action'
import { BLOCK_TYPE_BLOCKED, BLOCK_TYPE_MUTED} from './constants/userProfile'
import { getFirstLikeInfor, updateFirstLikeModal } from './actions/Like';
import { updateDetailRandomMatch, getDetailRandomMatch } from './actions/random_match';
// import thunkMiddleware from 'redux-thunk'
import promise from 'redux-promise'
import api from './middleware/api';
import chat_api from 'chat/middleware/api';
import { isEmpty } from 'lodash';
import * as ymmStorage from './utils/ymmStorage';
import { getUserId } from './utils/common'
import socket from "./utils/socket";
import Raven from 'raven-js';
import { ACTIVE_USER } from 'constants/userProfile';
Raven.config('https://d9459f879dd44f20a3f8d7a3db6b5b3b@sentry.io/213259', {
  ignoreErrors: [
  ],
  ignoreUrls: [
    'localhost', 'test.ymeet.me'
  ]
}).install();
// console.log('process.env', process.env)
let createStoreWithMiddleware = applyMiddleware(promise, api, chat_api)(createStore)
let store = createStoreWithMiddleware(reducers)

// Check internet connection status
/*
function updateIndicator() {
  const application_state = store.getState();
  // Show a different icon based on offline/online
  if( navigator.onLine && !isEmpty(application_state.user_profile.current_user)) {
    // Reconnect internet after disconnect
    // reget current user to check has_first_like status
    setTimeout(function(){
      store.dispatch(getCurrentUser()).then((currentUserResponse) => {
          const { response } = currentUserResponse
          const current_user = response.data
           // check current_user.has_first_like to show first like modal
          if (!current_user.has_first_like) {
            store.dispatch(getFirstLikeInfor()).then((firstLikeResponse) => {
              const { response } = firstLikeResponse;
              const first_like = response.data;
              if (!isEmpty(first_like)) {
                const isOpenFirstLikeModal = true;
                const firstLikeSent = false;
                const firstLikeReceived = true;
                const firstLikeInfo = first_like;
                store.dispatch(updateFirstLikeModal(isOpenFirstLikeModal, firstLikeSent, firstLikeReceived, firstLikeInfo));
              }
            });
          }
      });
    }, 3000); // this timeout in  1 second to make sure connection with API is already established
  }
}
// Update the online status icon based on connectivity
window.addEventListener('online',  updateIndicator);
window.addEventListener('offline', updateIndicator);
updateIndicator();
// End of check internet connection status
*/


//get current user when user refresh
try {
    if(ymmStorage.getItem('ymm_token') && window.location.pathname !== '/logout') {
      if (ymmStorage.getItem('read_chat_notification')) { // this for show chat notification in the first time reload
        ymmStorage.removeItem('read_chat_notification');
      }
        store.dispatch(getCurrentUser()).then((currentUserResponse) => {
          const { response } = currentUserResponse
          const current_user = response.data;
          //join to a specifix room
          if (current_user.user_status == ACTIVE_USER) {
            socket.emit('join', {room: current_user.identifier});
          }

          Raven.setUserContext({
              email: current_user.email,
              id: current_user.identifier,
          });
          // check current_user.has_first_like to show first like modal
          if (!current_user.has_first_like) {
            store.dispatch(getFirstLikeInfor()).then((firstLikeResponse) => {
              const { response } = firstLikeResponse;
              const first_like = response.data;
              if (!isEmpty(first_like)) {
                const isOpenFirstLikeModal = true;
                const firstLikeSent = false;
                const firstLikeReceived = true;
                const firstLikeInfo = first_like;
                store.dispatch(updateFirstLikeModal(isOpenFirstLikeModal, firstLikeSent, firstLikeReceived, firstLikeInfo));
              }
            });
          }


          //TODO
          store.dispatch(getDetailRandomMatch()).then((res) => {
            const random_user = res.response.data;
            if (!isEmpty(random_user)) {
              // has_random_matched
              store.dispatch(updateDetailRandomMatch(random_user));
            }
          })


          const isCompletedWelcome = current_user.welcome_page_completion > 3
          if (!isCompletedWelcome) {
            browserHistory.replace('/myprofile')
          }
          // the following data are mandatory to handle notification process
          // in the case user does not placed in blocked or liked list, we need
          // have these kind data to check notification
          store.dispatch(getBlockedUsers(BLOCK_TYPE_BLOCKED))
          store.dispatch(getBlockedUsers(BLOCK_TYPE_MUTED))
          //get dictionaries
          store.dispatch(getDictionaries())
          store.dispatch(getLuckySpinnerGift())
        })
    }
} catch(e) {
  // Raven.captureException(e)
}

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('./reducers', () => {
    const nextReducer = require('./reducers');
    store.replaceReducer(nextReducer);
  });
}

function handleUpdate() {
  let {
    action
  } = this.state.location;
  if (action === 'PUSH') {
    window.scrollTo(0, 0);
  }
}

const history = browserHistory;

history.listen(function (location) {
  const current_user = store.getState().user_profile.current_user;
  const dimensions = {
    dimension1: current_user.age ? current_user.age : '',
    dimension2: current_user.gender ? current_user.gender || '' : '',
    dimension3: (current_user.residence && current_user.residence.value) ? current_user.residence.value : '',
    dimension4: (current_user.birth_place && current_user.birth_place.value) ? current_user.birth_place.value : '',
    dimension11: current_user.identifier ? getUserId(current_user.identifier) : '',
  };
  window.ga('ymm.send', 'pageview', dimensions);
  const dataParameters = {
    'pathname': location? location.pathname : ''
  };
  dataLayer.push(dataParameters);
});

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} onUpdate={handleUpdate} />
  </Provider>
  , document.getElementById('root'));
