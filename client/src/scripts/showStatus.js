const status = {
  online: 'active',
  semionline: 'busy'
}

function showOnlineStatus(value) {
  if (typeof value === 'undefined') {
    return 'offline';
  }
  value = value.toLowerCase().replace(/\-/, '')
  return status[value] || 'offline'
}

export default showOnlineStatus
