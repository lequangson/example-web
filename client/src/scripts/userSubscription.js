function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

export function subscribeUser(props) {
  if ('serviceWorker' in navigator && 'PushManager' in window && 'Notification' in window && Notification.permission === 'default') {
    navigator.serviceWorker.ready.then(function(reg) {
      const applicationServerPublicKey = process.env.FCM_SERVER_PUBLIC_KEY;
      const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
      reg.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: applicationServerKey
      })
      .then(function(sub) {
        props.userSubscription(JSON.stringify(sub))
      })
      .catch(function(err) {
      });
    })
  }
}