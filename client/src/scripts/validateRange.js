class ValidateRange {
  constructor(options = {}) {
    const defaults = {
      element: '.js-validate-age',
      rangeTo: '.js-validate-to',
      rangeFrom: '.js-validate-from',
      message : 'Giá trị cận dưới phải nhỏ hơn hoặc bằng giá trị cận trên'
    }

    this.opts = Object.assign({}, defaults, options)

    // init value
    this.numTo = 0
    this.numFrom = 0

    this.elm = document.querySelector(this.opts.element)
    this.rangeTo = this.elm.querySelector(this.opts.rangeTo)
    this.rangeFrom = this.elm.querySelector(this.opts.rangeFrom)

    // construct message placeholder
    this.msg = document.createElement('p')
    this.msg.classList.add('form__msg', 'form--msg-err', 'col-xs-12')

    this.elm.appendChild(this.msg)

    this._init()
  }

  _isZero(value) {
    return value === 0
  }

  validate() {
    // check 2 case
    // 1. a = '' || b = ''
    if (this._isZero(this.numTo) || this._isZero(this.numFrom)) {
      this.rangeTo.classList.remove('form--err')
      this.rangeFrom.classList.remove('form--err')

      this.msg.innerHTML = ''

      return true
    }

    // 2. a <= b
    if (this.numTo <= this.numFrom) {
      this.rangeTo.classList.remove('form--err')
      this.rangeFrom.classList.remove('form--err')

      this.msg.innerHTML = ''

      return true
    } else {
      this.rangeTo.classList.add('form--err')
      this.rangeFrom.classList.add('form--err')

      this.msg.innerHTML = this.opts.message

      return false
    }
  }

  _init() {
    this.rangeTo.addEventListener('change', (evt) => {
      this.numTo = Number(evt.target.value)
      this.validate()
    })

    this.rangeFrom.addEventListener('change', (evt) => {
      this.numFrom = Number(evt.target.value)
      this.validate()
    })
  }
}

export default ValidateRange
