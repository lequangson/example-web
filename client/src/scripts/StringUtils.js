function clipLongText(text, maxLength) {
	return text.length > maxLength ? text.substr(0,maxLength-3) + '...' : text
}

export default clipLongText