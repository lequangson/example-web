const toggleBackdrop = (function($) {
  // toggle backdrop profile page
  const navBackdrop = $('#navBackdrop')
  const activeBackdrop = $('#activeBackdrop')
  const icon = activeBackdrop.find('.fa')

  activeBackdrop.on('click', () => {
    // change icon class
    if (icon.hasClass('fa-ellipsis-h')) {
      icon
        .removeClass('fa-ellipsis-h')
        .addClass('fa-close')
    } else {
      icon
        .removeClass('fa-close')
        .addClass('fa-ellipsis-h')
    }
    // toggle backdrop
    navBackdrop.toggleClass('is-show')

    return false
  })
})

export default toggleBackdrop
