import {
  DEFAULT_AVATAR_MALE,
  DEFAULT_AVATAR_FEMALE,
  NON_GENDER_AVATAR
} from '../../constants/Enviroment'

export default function showDefaultAvatar(gender, size = 'small') {
  if (!gender)
    return NON_GENDER_AVATAR;

  if (['small', 'thumb', 'large'].indexOf(size) === -1) {
    throw new Error('Image size not valid')
  }

  if (gender === 'female') {
    return `${DEFAULT_AVATAR_FEMALE}${size}.jpg`
  } else {
    return `${DEFAULT_AVATAR_MALE}${size}.jpg`
  }
}

export function defaultAvatarObject(gender, size = 'small') {
  if (!gender) return ''

  if (['small', 'thumb', 'large'].indexOf(size) === -1) {
    throw new Error('Image size not valid')
  }

  if (gender === 'female') {
    return {
      large_image_url: `${DEFAULT_AVATAR_FEMALE}${size}.jpg`,
      extra_large_image_url: `${DEFAULT_AVATAR_FEMALE}${size}.jpg`,
      thumb_image_url: `${DEFAULT_AVATAR_FEMALE}${size}.jpg`,
      small_image_url: `${DEFAULT_AVATAR_FEMALE}${size}.jpg`,
    }
  } else {
    return {
      large_image_url: `${DEFAULT_AVATAR_MALE}${size}.jpg`,
      extra_large_image_url: `${DEFAULT_AVATAR_MALE}${size}.jpg`,
      thumb_image_url: `${DEFAULT_AVATAR_MALE}${size}.jpg`,
      small_image_url: `${DEFAULT_AVATAR_MALE}${size}.jpg`,
    }
  }
}

export function getAvatar(user, size='large') {
  const userPicture = (!_.isEmpty(user) && user.user_pictures.length)
    ? user.user_pictures[0]
    : defaultAvatarObject(user.gender, size);
    return userPicture;
}