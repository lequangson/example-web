import {
  GET_SIMILAR_FACES_BY_FACEBOOK_REQUEST,
  GET_SIMILAR_FACES_BY_FACEBOOK_SUCCESS,
  GET_SIMILAR_FACES_BY_FACEBOOK_FAILURE,
  UPDATE_SELECTED_USER_FROM_FACEBOOK
} from './Constant';
import { MODAL_SIMILAR_TO_MY_FB_FRIENDS } from 'pages/similar-face/Constant';

const defaultState = {
  isFetching: false,
  users: [],
  reachEnd: false,
  totalItems: 0,
  modalType: MODAL_SIMILAR_TO_MY_FB_FRIENDS,
  headerTitle: 'Bạn bè Facebook',
  shareTitle: 'Kết bạn trò chuyện với 0 người giống tôi trên Ymeet.me!',
  mode: 2,
  target_id: '',
  target_name: '',
  selected_user: {},
  sharing_picture: ''
}

export default function similar_face_to_my_fb_friends(state = defaultState, action) {
  switch (action.type) {
    case GET_SIMILAR_FACES_BY_FACEBOOK_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      })
    case GET_SIMILAR_FACES_BY_FACEBOOK_SUCCESS:
      const totalItem = action.response.data.similar_pictures ?
        action.response.data.similar_pictures.length : state.totalItems;
      return Object.assign({}, state, {
        users: action.response.data.similar_pictures,
        reachEnd: true,
        isFetching: false,
        totalItems: totalItem,
        target_id: action.extentions.target_id,
        shareTitle: `Kết bạn trò chuyện với ${totalItem} người giống ${action.response.data.user_picture.name} trên Ymeet.me!`,
        target_name: action.response.data.user_picture.name,
        sharing_picture: action.response.data.sharing_picture.url
      })
    case GET_SIMILAR_FACES_BY_FACEBOOK_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      })
    case UPDATE_SELECTED_USER_FROM_FACEBOOK:
      return Object.assign({}, state, {
        selected_user: action.selected_user
      })
    default:
      return state
  }
}