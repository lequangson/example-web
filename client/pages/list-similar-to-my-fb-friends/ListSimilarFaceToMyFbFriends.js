import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { isEmpty } from 'lodash';
import InfiniteListItem from '../../components/infinite-list-items/InfiniteListItem';
import {
  GRID_VIEW_USER_CARD, DETAIL_VIEW_USER_CARD,
  NEW_PHOTO_CARD, SIMILAR_FACE_CARD,
} from '../../components/infinite-list-items/Constant';
import addHeader from '../similar-face/addHeader';
import SimilarToMyFbFriendsEmpty from '../similar-face/empty-page/SimilarToMyFbFriendsEmpty';
import {
  UploadImageFromFacebook
} from 'components/buttons/upload-image';
class ListSimilarFaceToMyFbFriends extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    const { current_user } = this.props.user_profile;
    const { picture_id  } = this.props.params;
    if (!current_user.identifier || !picture_id) {
      return;
    }
    this.props.getSimilarFacesByPhoto(current_user.identifier, picture_id);
  }

  componentWillReceiveProps(nextProps) {
    const { current_user } = nextProps.user_profile;
    const { picture_id  } = this.props.params;

    if (
      isEmpty(this.props.user_profile.current_user) && !isEmpty(current_user) &&
      current_user.provider != 'facebook'
    ) {
      browserHistory.push('/');
      return;
    }

    if (!nextProps.params.picture_id) {
      return;
    }

    if (
      picture_id != nextProps.params.picture_id ||
      (isEmpty(this.props.user_profile.current_user) && !isEmpty(current_user))
    ) {
      this.props.getSimilarFacesByPhoto(current_user.identifier, nextProps.params.picture_id);
    }
  }

  uploadFromFacebookSuccess = (data = null) => {
    if (data) {
      this.props.updateSelectedUser(data[0]);
      browserHistory.push(`/similar-face-to-my-fb-friends/${data[0].id}`);
    }
  }

  render() {
    const { users } = this.props.similar_face_to_my_fb_friends;
    if (users.length === 0) {
      return <SimilarToMyFbFriendsEmpty {...this.props} />
    }
    return (
      <div className="sticky-footer">
        <InfiniteListItem
          listItems={this.props.similar_face_to_my_fb_friends.users}
          loadingDataStatus={this.props.similar_face_to_my_fb_friends.isFetching}
          reachEnd={this.props.similar_face_to_my_fb_friends.reachEnd}
          viewMode={SIMILAR_FACE_CARD}
          current_user={this.props.user_profile.current_user}
          pageSource='SIMILAR_FACE_TO_MY_FB_FRIENDS_PAGE'
          onClick={this.props.onClick}
        />

        <div className="fixed-bottom">
          <div className="container padding-t5">
            <UploadImageFromFacebook
              picture_type={6}
              upload_success={this.uploadFromFacebookSuccess}
            >
              <button className="btn txt-center btn--p btn--b">Tìm cho người bạn khác</button>
            </UploadImageFromFacebook>
          </div>
        </div>

      </div>
    );
  }
}

export default addHeader(ListSimilarFaceToMyFbFriends, 'similar_face_to_my_fb_friends');
