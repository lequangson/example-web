import { connect } from 'react-redux';
import { shuffleSimilarFace } from 'pages/similar-face/Action';
import { updatePreviousPage } from 'actions/Enviroment';
import ListSimilarFaceToMyFbFriends from './ListSimilarFaceToMyFbFriends';
import {
  getSimilarFacesByPhoto,
  updateSelectedUser
} from './Action';

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    getSimilarFacesByPhoto: async (current_user_id, photo_id) => await dispatch(getSimilarFacesByPhoto(current_user_id, photo_id)),
    updateSelectedUser: (data) => dispatch(updateSelectedUser(data)),
    shuffleSimilarFace: (props) => {
      dispatch(shuffleSimilarFace(props));
    },
    updatePreviousPage: (page) => {
      dispatch(updatePreviousPage(page))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListSimilarFaceToMyFbFriends)