import React from 'react';
import EditProfileContainer from 'containers/EditProfileContainer';
import HeaderContainer from 'containers/HeaderContainer';
import PromotionContainer from 'containers/PromotionContainer';
import RemindUpdateIntroContainer from 'containers/RemindUpdateIntroContainer';
import WelcomePageModal from 'components/modals/WelcomePageModal';
import WelcomePageContainer from 'containers/welcome';

const EditProfile = props => {
  return (
  <div>
    <HeaderContainer {...props} />
    <EditProfileContainer {...props} />
    <PromotionContainer {...props} />
    <RemindUpdateIntroContainer {...props} />
    <WelcomePageModal {...props} />
    <WelcomePageContainer {...props}/>
  </div>
  )
}
export default EditProfile;
