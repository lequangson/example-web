import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import _ from 'lodash';

import {CDN_URL} from 'constants/Enviroment';
import { AVATAR_PICTURE, OTHER_PICTURE, SIMILAR_FACE_PICTURE } from 'constants/userProfile';
import { getAvatar } from 'src/scripts/showDefaultAvatar';
import { compareIdentifiers, getUserIdFromIdentifier } from 'utils/common';
import {
  UploadImageFromDevice,
  UploadImageFromFacebook
} from 'components/buttons/upload-image';
import * as ymmStorage from 'utils/ymmStorage';

class CategorySimilarFace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      similarUploadedFromDevice: null,
      similarUploadedFromFacebook: null,
      isUpdated: false,
    };
  }

  componentDidMount() {
    this.handleClickIcon();
    if (this.props.scrollRight){
      this.handleScrollHorizontal();
    }
  }

  handleClickIcon = () => {
    const tabLinks = document.querySelectorAll('.category__item')
    const arrayTabLinks = [].slice.call(tabLinks);
    const average = Math.floor(arrayTabLinks.length/2)
    function showLink(el, i) {
      // arrayElements = Array.from(elements);
      arrayTabLinks.map((element) => {element.classList.remove('active')})
      el.classList.add('active')
      this.handleScrollHorizontal(i, average)
    }
    for (let i = 0; i < tabLinks.length; i++) {
      tabLinks[i].addEventListener('click', showLink.bind(this, tabLinks[i], i))
    }
  }

  handleScrollHorizontal = (i, average) => {
    const category = document.querySelector('.category')
    if (i <= average) {
      category.scrollLeft = -1
    }
    else {
      category.scrollLeft = 800
    }
  }

  uploadFromDeviceSuccess = (data = null) => {
    if (data) {
      this.setState({ similarUploadedFromDevice: data});
      ymmStorage.setItem('similarUploadedFromDevice', data);
      setTimeout(() => {
        browserHistory.push(`/similar-face-to-my-photos/${data.id}`);
      }, 200);
    }
  }

  uploadFromFacebookSuccess = (data = null) => {
    if (data) {
      this.setState({ similarUploadedFromFacebook: data[0]});
      ymmStorage.setItem('similarUploadedFromFacebook', data[0]);
       setTimeout(() => {
        browserHistory.push(`/similar-face-to-my-fb-friends/${data[0].id}`);
      }, 200);
    }
  }

  activateMenuItem(pathName, user_identifier, user_identifier_param) {
    const isTarget = compareIdentifiers(user_identifier, user_identifier_param);
    const pathReg = new RegExp(pathName, 'i');
    return pathReg.test(this.props.location.pathname) || isTarget ? 'category__item active' : 'category__item';
  }

  render() {
    const { current_user } = this.props.user_profile;
    const selected_user_fb = this.props.similar_face_to_my_fb_friends.selected_user;
    const selected_user_fb_id = this.props.similar_face_to_my_fb_friends.selected_user.id || '';
    const selected_user_my_photo = this.props.similar_face_to_my_photos.selected_user;
    const selected_user_my_photo_id = this.props.similar_face_to_my_photos.selected_user.id || '';
    const userPicture = getAvatar(current_user);
    const {
      isUploadingImageFromDevice,
      isUploadingImageFromFacebook
    } = this.props.upload_image;
    const pathNameSimilarFb = `/similar-face-to-my-fb-friends/${selected_user_fb_id}`;
    const pathnameSimilarDevice = `/similar-face-to-my-photos/${selected_user_my_photo_id}`;
    return (
      <div className="hsdiv category padding-t5 mbm" >
        <Link to='/similar-face-to-me' >
          <div className={this.activateMenuItem('similar-face-to-me')}>
            <div className="category__image">
              <img src={userPicture.large_image_url} className="img-full img-round" alt="SimilarFace"/>
            </div>
          <div className="category__txt">Giống tôi</div>
        </div>
        </Link>
        {
          this.props.similarArray.map((user, i) => {
            const avatar = getAvatar(user);
            const picture_type = avatar.is_main ? AVATAR_PICTURE : OTHER_PICTURE;
            const pathName = `/similar-face-to-my-ymm-friends/${user.identifier}`;
            const user_identifier_param = this.props.params.user_id;
            return (
              <Link key={i} to={pathName} >
                <div className={this.activateMenuItem(pathName, user.identifier, user_identifier_param)}>
                  <div className="category__image">
                    <img src={avatar.large_image_url} className="img-full img-round" alt="SimilarFace"/>
                  </div>
                  <div className="category__txt">{user.nick_name}</div>
                </div>
              </Link>
            )
          })
        }
        {/*current_user.provider == 'facebook' && <Link to={pathNameSimilarFb} >
            {isUploadingImageFromFacebook && <div className={this.activateMenuItem(pathNameSimilarFb)}>
                  <div className="category__image category__image--icon pos-relative  txt-center  loader full-icon-load"></div>
                  <div className="category__txt">Bạn bè FB</div>
               </div>
             }

            {!isUploadingImageFromFacebook && <div className={this.activateMenuItem(pathNameSimilarFb)}>
                <div className="category__image category__image--icon pos-relative">
                  {!selected_user_fb.extra_large_image_url && <i className="fa fa-facebook perfect-center" aria-hidden="true"></i>}
                  {selected_user_fb.extra_large_image_url && <img src={selected_user_fb.extra_large_image_url} className="img-full perfect-center" alt="SimilarFace"/>}
                </div>
                <div className="category__txt">Bạn bè FB</div>
              </div>
            }
          </Link>
        */}

        <Link to={pathnameSimilarDevice} >
          {isUploadingImageFromDevice && <div className={this.activateMenuItem('similar-face-to-my-photos')}>
                <div className="category__image category__image--icon pos-relative  txt-center  loader full-icon-load"></div>
                <div className="category__txt">Tìm theo ảnh</div>
             </div>
           }
          {!isUploadingImageFromDevice && <div className={this.activateMenuItem('similar-face-to-my-photos')}>
              <div className="category__image category__image--icon pos-relative  txt-center">
                {!selected_user_my_photo.extra_large_image_url && <i className="fa fa-user-circle-o perfect-center" aria-hidden="true"></i>}
                {selected_user_my_photo.extra_large_image_url && <img src={selected_user_my_photo.extra_large_image_url} className="img-full perfect-center" alt="SimilarFace"/>}
              </div>
              <div className="category__txt">Tìm theo ảnh</div>
            </div>
          }
        </Link>
      </div>
    );
  }
}

export default CategorySimilarFace;