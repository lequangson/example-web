import {
  GET_SIMILAR_SHARING_REQUEST,
  GET_SIMILAR_SHARING_SUCCESS,
  GET_SIMILAR_SHARING_FAILURE,
  GET_FACE_PARTNERS_REQUEST,
  GET_FACE_PARTNERS_SUCCESS,
  GET_FACE_PARTNERS_FAILURE,
  SHUFFLE_FACE_PARTNER
} from './Constant';
import { CALL_API } from 'middleware/api';
import {
  API_GET,
} from 'constants/Enviroment';
import * as ymmStorage from 'utils/ymmStorage';

export function getSimilarSharingRequest() {
  return {
    type: GET_SIMILAR_SHARING_REQUEST,
  }
}

export function getSimilarSharingByUser(current_user_id, user_id, filter_mode) {
  const isAnonymous = !ymmStorage.getItem('ymm_token');
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'similar_faces_by_user',
      params: {
        user_id,
        filter_mode,
        current_user_id
      },
      authenticated: !isAnonymous,
      types: [
        GET_SIMILAR_SHARING_REQUEST,
        GET_SIMILAR_SHARING_SUCCESS,
        GET_SIMILAR_SHARING_FAILURE
      ],
      isAnonymous
    },
  }
}

export function getSimilarSharingByPhoto(current_user_id, photo_id) {
  const isAnonymous = !ymmStorage.getItem('ymm_token');
  return {
    [CALL_API]: {
      endpoint: 'similar_faces_by_photo',
      method: 'GET',
      authenticated: !isAnonymous,
      params: {
        photo_id,
        current_user_id
      },
      types: [
        GET_SIMILAR_SHARING_REQUEST,
        GET_SIMILAR_SHARING_SUCCESS,
        GET_SIMILAR_SHARING_FAILURE
      ],
      isAnonymous
    },
  };
}

export function getSimilarSharingFailure() {
  return {
    type: GET_SIMILAR_SHARING_FAILURE
  }
}

export function shuffleFacePartners() {
  return {
        type: SHUFFLE_FACE_PARTNER,
    }
}

export function getFacePartnersRequest() {
  return {
    type: GET_FACE_PARTNERS_REQUEST
  }
}

export function getFacePartners(props) {
    return {
    [CALL_API]: {
      endpoint: 'have_face_partners',
      method: 'GET',
      authenticated: true,
      params:{},
      types: [
        GET_FACE_PARTNERS_REQUEST,
        GET_FACE_PARTNERS_SUCCESS,
        GET_FACE_PARTNERS_FAILURE
      ],
      extentions:{
        partner_identifier: props.params.user_id
      }
    },
  };
  }