import React, { Component } from 'react';
import { browserHistory } from 'react-router'
import HeaderContainer from '../../containers/HeaderContainer'
import SubMenu from 'components/headers/SubMenu';
import CategorySimilarFace from './CategorySimilarFace'
import HeaderSimilarFace from './HeaderSimilarFace'
import ViewSimilarUserModal from './modal/ViewSimilarUserModal';
import { compareIdentifiers } from 'utils/common';

export default function addHeader(Wrapcomponent, reducer) {
  return class SimilarFace extends Component {
    constructor() {
      super();
      this.state = {
        isOpen: false,
        user: {},
        similarUserIndex: 0,
      }
    }

    componentWillMount() {
      if (!this.props.similar_sharing.similarArray.length) {
        this.props.getFacePartners(this.props);
      }
      else if (/\/similar-face-to-me/i.test(this.props.location.pathname) &&
        this.props.Enviroment.previous_page !== 'similar-page') {
        this.props.shuffleFacePartners();
      }
    }

    componentWillUpdate(nextProps) {
      if (this.props.similar_sharing.similarArray.length === 0 &&
        nextProps.similar_sharing.similarArray.length > 0 &&
        !/\/similar-face-to-my-photos/i.test(this.props.location.pathname)) {
        const { facePartners } = nextProps.similar_sharing;
        const user_identifier = nextProps.params.user_id;
        const userInFacePartners = facePartners.find((user) => {
          return compareIdentifiers(user_identifier, user.identifier)
        });
        if(!userInFacePartners){
          browserHistory.push('/similar-face-to-me');
        }
      }
    }

    componentWillUnmount() {
      this.props.updatePreviousPage('similar-page');
    }

    openModal = (user) => {
      const { users } = this.props[reducer];
      const similarUserIndex = users.findIndex(u => u === user);
      this.setState({ isOpen: true, user, similarUserIndex });
    }

    closeModal = () => {
      this.setState({ isOpen: false })
    }

    showNextSimilarUser = (stepIndex) => {
      const { users } = this.props[reducer];
      const currentIndex = users.findIndex(user => user === this.state.user);
      const newSimilarUserIndex = currentIndex + stepIndex;
      this.setState({
        user: users[newSimilarUserIndex],
        similarUserIndex: newSimilarUserIndex
      });
    }

    render() {
      const { users, modalType, mode, target_id, target_name, headerTitle, shareTitle, isFetching, sharing_picture } = this.props[reducer];
      const { isOpen, user, similarUserIndex } = this.state;
      const { similarArray } = this.props.similar_sharing;
      const isFetchingFacePartner = this.props.similar_sharing.isFetching;
      const scrollRight = ['similar_face_to_my_fb_friends', 'similar_face_to_my_photos'].includes(reducer);
      const { current_user } = this.props.user_profile;
      return (
        <div>
          <HeaderContainer {...this.props} />
          <div className="site-content">
            <SubMenu mode='similar-face'/>
            <CategorySimilarFace
              {...this.props}
              scrollRight={scrollRight}
              similarArray={similarArray}
            />
            <HeaderSimilarFace
              title={headerTitle}
              shareTitle={shareTitle}
              totalUser={users.length}
              mode={mode}
              id={target_id}
              current_user_id={current_user.identifier}
              sharingPictureUrl={sharing_picture}
            />
            <Wrapcomponent {...this.props} onClick={this.openModal} />
            <ViewSimilarUserModal
              modalType={modalType}
              isOpen={isOpen}
              closeModal={this.closeModal}
              user={user}
              totalUser={users.length}
              similarUserIndex={similarUserIndex}
              showNextSimilarUser={this.showNextSimilarUser}
              current_user={current_user}
              target_name={target_name}
              shareTitle={shareTitle}
              mode={mode}
              id={target_id}
            />
          </div>
          <div className={isFetching || isFetchingFacePartner ? 'loader is-load is-full-screen-load' : 'hide loader is-full-screen-load'}>
            <div className="loader__text">
              <i className="fa fa-3x fa-spinner"></i>
            </div>
          </div>
        </div>
      )
    }
  }
}