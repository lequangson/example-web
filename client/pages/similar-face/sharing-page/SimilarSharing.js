import React, { Component, PropTypes } from 'react';
import { browserHistory, Link } from 'react-router';
import { NotificationManager } from 'components/commons/Notifications';
import MetaTags from 'react-meta-tags';
import InfiniteListItem from 'components/infinite-list-items/InfiniteListItem';
import {
  SHARE_SIMILAR_FACE_CARD
} from 'components/infinite-list-items/Constant';
import { LOGGING_TEXT, AUTO_LOGIN_ERROR_TITLE, AUTO_LOGIN_ERROR_MSG } from 'constants/TextDefinition'
import { SIMILARSHARING_BASE_URL, LANDINGPAGE_BASE_URL, BLOG_URL } from 'constants/Enviroment'
import ScrollToTop from 'components/ScrollToTop'
import { SIMILAR_LOGIN_TO_UPLOAD,　SIMILAR_LOGIN_TO_FRIENDS, SIMILAR_LOGIN_TO_MEMBERPAGE } from '../Constant'
import ShareButton from 'components/commons/Buttons/ShareButton'
import { FacebookLoginButton, GoogleLoginButton } from 'components/auth';
import Modal from 'react-modal'
import * as ymmStorage from 'utils/ymmStorage';

class SimilarSharing extends Component {

  constructor() {
    super();
    this.state = {
      isOpenModal: false,
      modalTitle: null
    }
  }

  componentWillMount() {
      const { current_user_id, mode, id } = this.props.params;
      const { similar_sharing } = this.props;
      if (!similar_sharing.users.length) {
        if (mode == 0 || mode == 1) { // get similar face to me
          this.props.getSimilarSharingByUser(current_user_id, id, mode);
        } else {
          this.props.getSimilarSharingByPhoto(current_user_id, id);
        }
      }
  }
  componentDidMount() {
    this.landingPageScript();
  }

  componentDidUpdate() {
    const { errorMessage } = this.props.auth

    if (errorMessage) {
      if ($('.notification-error').length) {
          $('.notification-error').hide()
      }
      if ($('.notification-info').length) {
        $('.notification-info').hide()
      }
      if (window.location.href.indexOf('autoLogin=true') !== -1) {
        NotificationManager.info(AUTO_LOGIN_ERROR_MSG, AUTO_LOGIN_ERROR_TITLE, 600000);
        browserHistory.push('/');
        return;
      } else {
        NotificationManager.error(errorMessage, 'Đăng nhập không thành công.', 600000);
      }
    }

  }

  componentWillUnmount() {
    const h = document.getElementsByTagName('html')[0];
    h.classList.remove('similar-sharing');
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      const html = document.getElementsByTagName('html')[0];
      html.classList.add('no-touch');
    }
  }

  landingPageScript() {
    // remove no-touch class
    const html = document.getElementsByTagName('html')[0];
    html.classList.remove('no-touch');
    html.classList.add('similar-sharing');

    // hamburger button
    const ham = document.querySelector('.js-hamburger');
    const nav = document.querySelector('.js-nav');
    if (ham) {
      ham.addEventListener('click', () => {
        ham.classList.toggle('is-active');
        nav.classList.toggle('is-show');
      });
    }

  }

  handleOpenModal = () => {
    if (ymmStorage.getItem('ymm_token')) {
      browserHistory.push('/similar-face-to-me');
      return;
    }

    this.setState({ isOpenModal: true });
  }

  closeModal = () => {
    this.setState({ isOpenModal: false });
  }

  activeModal = () => {
    this.handleOpenModal();
    this.setState({modalTitle: SIMILAR_LOGIN_TO_MEMBERPAGE});
  }

  render() {
    const { auth, similar_sharing } = this.props;
    const { shareForUserName, shareForUserPicture, sharingPictureUrl } = similar_sharing;
    const { isFetching, isAuthenticated } = auth
    const { current_user_id, mode, id } = this.props.params;
    const sharingPictureName = sharingPictureUrl.split(/(\\|\/)/g).pop();
    const shareUrl = `${process.env.ROOT_URL}/similar-sharing/${current_user_id}/${mode}/${id}/${sharingPictureName}`;
    const title = `NGƯỜI CÓ TƯỚNG PHU THÊ VỚI ${shareForUserName ? shareForUserName : 'NGƯỜI CÓ ẢNH TRÊN'}!`;

    return (
      <div className="sp">
        <header className="sp__header js-header">
          <MetaTags>
            <meta id="og-image" property="og:image" content={sharingPictureUrl} />
          </MetaTags>
          <div className="row">
            <div className="col-xs-12 l-flex l-flex--ac l-flex--header lp">
              <div className="sp-nav">
                <button className="hamburger hamburger--elastic js-hamburger" type="button"><span className="hamburger-box"><span className="hamburger-inner"></span></span></button>
                <h1 className="sp__logo">
                  <Link to="/">
                    <img src={SIMILARSHARING_BASE_URL + 'logo-white.png'} alt="Ymeet.me - hẹn hò an toàn cho phụ nữ" />
                  </Link>
                </h1>
                <div className="sp-nav__main js-nav">
                  <h1 className="sp-nav__logo">
                    <Link to="/">
                    <img src={SIMILARSHARING_BASE_URL + 'logo-white.png'} alt="Ymeet.me - hẹn hò an toàn cho phụ nữ" />
                    </Link>
                  </h1>
                  <nav className="sp-nav__list">
                    <Link className="sp-nav__link l2" to="/about">Câu chuyện Ymeet.me</Link>
                    <a className="sp-nav__link l2" href={BLOG_URL} target="_blank" rel="noopener nofollow noreferrer" >Blog</a>
                    <Link className="sp-nav__link l2" to="/contact">Liên hệ</Link>
                  </nav>
                </div>
              </div>
              <div className="sp__header--social">
                <ul className="list-inline text-center">
                  <li><a href="https://facebook.com/ymeet.me" target="_blank" rel="noopener nofollow noreferrer"><i className="fa fa-facebook fa-2x"></i></a></li>
                  <li><a href="https://twitter.com/Ymeetme" target="_blank" rel="noopener nofollow noreferrer"><i className="fa fa-twitter fa-2x"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div className="sp-header__blur"></div>
        </header>
        <section className="sp-well sp-well--decor">
          <div className="row">
            <div className="col-xs-8">
              <h1>Tìm người giống bạn</h1>
              <p>Tìm người có gương mặt giống bạn trên khắp Việt Nam</p>
              <ul className="list-unstyle">
                <li>Bước 1: Upload ảnh có gương mặt bạn</li>
                <li>Bước 2: Bằng công nghệ Trí tuệ nhân tạo AI, chúng tôi sẽ tìm kiếm<br /> những người có gương mặt giống bạn</li>
              </ul>
            </div>
          </div><img className="sp-well__decor" src={SIMILARSHARING_BASE_URL + 'similar-face.png'} alt="Avata 1" />
        </section>
        <section className="sp-well sp-well--decor-1">
          <div className="row l-flex-ce">
              <div className="col-xs-6 col-md-3">
                <div className="txt-center">
                  <article className="card mbl">
                    <Link className="card__link" href="#">
                      <img className="card__img--1 bordered" src={shareForUserPicture} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" />
                    </Link>
                  </article>
                  <button className="btn btn-red"
                    onClick={() => {this.handleOpenModal();
                    this.setState({modalTitle: SIMILAR_LOGIN_TO_UPLOAD })}}>Tải ảnh của bạn</button>
                </div>
              </div>
{/*              <div className="col-xs-6 col-md-3 txt-left">
                <div className="txt-center">
                  <article className="card wrap-similar-face mbl">
                    <a className="card__link" href="#">
                      <img className="card__img--1" src={SIMILARSHARING_BASE_URL + 'similar-friend.png'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" /></a>
                  </article>
                  <button className="btn btn-blue"
                    onClick={() => {this.handleOpenModal();
                    this.setState({modalTitle: SIMILAR_LOGIN_TO_FRIENDS})}}>
                    <div className="l-flex l-flex--ac">
                      <div className="icon padding-r10">
                        <i className="fa fa-facebook"></i>
                        </div>Xem bản sao của bạn bè
                    </div>
                  </button>
                </div>
              </div>*/}
            </div>
        </section>
        <section className="sp-well sp-well--2 sp-well--bp">
          <div className="row">
            <div className="col-xs-12">
              <div className="sp__list-cards--heading txt-center">
                <h2>{title}</h2>
                <p>{this.props.similar_sharing.users.length} kết quả</p>
              </div>
              <div className="sp__sharing--panel mbl l-flex-vertical-center txt-right-mc">
                  <span className="btn-share--title mrs">Chia sẻ</span>
                  <ShareButton
                    title={title}
                    shareUrl={shareUrl}
                    round
                    size={40}
                    facebookShare
                    twitterShare
                    copyLink
                  />
              </div>
              <div className="txt-center">
                <InfiniteListItem
                  listItems={this.props.similar_sharing.users}
                  loadingDataStatus={this.props.similar_sharing.isFetching}
                  reachEnd={this.props.similar_sharing.reachEnd}
                  viewMode={SHARE_SIMILAR_FACE_CARD}
                  current_user={this.props.user_profile.current_user}
                  pageSource='SHARE_SIMILAR_FACE_TO_ME_PAGE'
                  activeModalCallback={this.activeModal}
                  containerHeight={500}
                />
              </div>
            </div>
          </div>
          <ScrollToTop />
        </section>

        <footer className="sp-footer">
          <div className="footer-top padding-b30 padding-t30">
            <div className="row">
              <div className="col-xs-12 col-md-6">
                <div className="txt-left mbm"><a href="#"><img src={SIMILARSHARING_BASE_URL + 'logo-white.png'} alt="" /></a></div>
                <div className="txt-left mbl">
                  <h2>Hẹn hò online chưa bao giờ<br />
                  an toàn đến thế</h2>
                </div>
                <div className="banner-download-img mbl">
                  <a className="rounded-5 mrm" target="_blank" href="https://l.facebook.com/l.php?u=https%3A%2F%2Fitunes.apple.com%2Fus%2Fapp%2Fymeet-me%2Fid1280333225&h=ATMdmjWow7TfW8yo04kMu-ZRL2Cze5H9E6DXcKNAOCMBkWPc9CIhy6ihaul_a20kETafTW687HxqIeDt-T964UfOJXRE6tn7iaU6GQE7Nx4Hu_sM3p3TN6oh7V2QIM8LyGGRyvFuA7k8wEcYkswEplx1zKwDWLDyvpJoKSrwgGIqEnF-CeRRvK7gpxEVmA-OoJd3NxVPbsWcmchYefWG45rhMmNZn4Wan-5rp6Q5PH2aSfz1H0_iIUS-vgGHjbuWjlpYzRcdSEA-PbmAzjCOH5eZGdv2xtqpbyzfnsHRgxhzq1X5yQ">
                    <img src={SIMILARSHARING_BASE_URL + 'app_store.png'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" />
                  </a>
                  <a className="rounded-5" target="_blank" href="https://play.google.com/store/apps/details?id=com.ymmrnt">
                    <img src={SIMILARSHARING_BASE_URL + 'google_play.png'} alt="Ymeet.me hẹn hò trực tuyến an toàn cho phụ nữ" />
                  </a>
                </div>
              </div>
              <div className="col-xs-12 col-md-6">
                <div className="txt-center padding-t30">
                  {!isAuthenticated && <FacebookLoginButton cssClass="sp-btn sp-btn--primary sp-btn--body rounded-5 mbm" />}
                  {!isAuthenticated && <GoogleLoginButton cssClass="sp-btn sp-btn--google sp-btn--body rounded-5 mbm" />}
                </div>
              </div>
            </div>
          </div>
          <div className="footer-bottom padding-b10 padding-t10">
            <div className="row">
              <div className="col-xs-12">
                <div className="footer-link__group l-flex-grow l-flex-ce">
                  <ul className="list-unstyle list-inline">
                    <li className="sp-nav__link"><Link className="sp-footer__link" to="/terms-of-use">Điều khoản sử dụng</Link></li>
                    <li className="sp-nav__link"><Link className="sp-footer__link" to="/privacy">Bảo mật</Link></li>
                    <li className="sp-nav__link"><Link className="sp-footer__link" to="/hesps">Trợ giúp</Link></li>
                    <li className="sp-nav__link"><Link className="sp-footer__link" to="/contact">Liên hệ</Link></li>
                    <li className="sp-nav__link"><a className="sp-footer__link" href="https://ymeet.me/blog/" target="_blank">Blog</a></li>
                  </ul>
                  <ul className="list-unstyle list-inline mbm">
                    <li className="margin-0"><a className="sp-nav__link mrm" href="https://facebook.com/ymeet.me" target="_blank" rel="noopener nofollow noreferrer"><i className="fa fa-facebook"></i></a></li>
                    <li className="margin-0"><a className="sp-nav__link mrm" href="https://twitter.com/Ymeetme" target="_blank" rel="noopener nofollow noreferrer"><i className="fa fa-twitter"></i></a></li>
                  </ul>
                  <div className="txt-sm txt-copyright">Copyright (c) 2016 <a href="http://mmj.vn" target="_blank" rel="noopener noreferrer" className="txt-bold">Media Max Japan</a> All Rights Reserved.</div>
                </div>
              </div>
            </div>
          </div>
        </footer>

        <div className={isFetching ? 'loader is-full-screen-load' : 'hide loader is-full-screen-load'}>
          <span className="loader__text">{ LOGGING_TEXT }</span>
        </div>
        <Modal
          isOpen={this.state.isOpenModal}
          onRequestClose={this.closeModal}
          className="LoginModal modal__content modal__content--2"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="well txt-center">
            <div className="padding-b10">{ this.state.modalTitle }</div>
            <div className="padding-rl10">
              {!isAuthenticated && <FacebookLoginButton cssClass="sp-btn sp-btn--primary sp-btn--body rounded-5 mbm" />}
              {!isAuthenticated && <GoogleLoginButton cssClass="sp-btn sp-btn--google sp-btn--body rounded-5 mbm" />}
            </div>
            <p>Đăng nhập là bạn đồng ý với <Link to='/terms-of-use'>điều khoản</Link></p>
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    );
  }
}

SimilarSharing.propTypes = {

}

export default SimilarSharing
