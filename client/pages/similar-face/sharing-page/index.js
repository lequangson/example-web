import { connect } from 'react-redux';
import SimilarSharing from './SimilarSharing';
import {
  getSimilarSharingRequest,
  getSimilarSharingByUser,
  getSimilarSharingByPhoto
} from '../Action';

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    getSimilarSharingByUser: async (current_user_id, user_id, filter_mode) => {
      dispatch(getSimilarSharingRequest());
      return await dispatch(getSimilarSharingByUser(current_user_id, user_id, filter_mode))
    },
    getSimilarSharingByPhoto: async (current_user_id, photo_id) => {
      dispatch(getSimilarSharingRequest());
      return await dispatch(getSimilarSharingByPhoto(current_user_id, photo_id))
    }
  };
}

export default connect(mapStateToProps, mapDispachToProps)(SimilarSharing);
