import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import {
  DEFAULT_LOAD_FEMALE, DEFAULT_LOAD_MALE
} from '../../../constants/Enviroment';
import { SIMILAR_FACE_PICTURE } from '../../../constants/userProfile'
import { SIMILAR_TO_MY_PHOTOS_PAGE, PUSH_TO_SOCKET_COMMAND } from '../Constant';
import {
  UploadImageFromDevice
} from 'components/buttons/upload-image';


class SimilarToMyPhotosEmpty extends Component {

  uploadFromDeviceSuccess = (data = null) => {
    if (data) {
      this.props.updateSelectedUser(data);
      browserHistory.push(`/similar-face-to-my-photos/${data.id}`);
    }
  }


  render() {
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE
    return (
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
              <article className="mbm mc">
                <div className="card__upper">
                  <div href="#" className="card__link mbm">
                    <img src={default_image} alt="" className="card__img"/>
                  </div>
                  <div className="txt-center txt-light txt-lg mbm">
                    Hãy tải lên 1 bức ảnh khuôn mặt bất kỳ, hệ thống sẽ tìm ra những bản sao của người đó
                  </div>
                </div>
              </article>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-10 col-xs-offset-1">
              <UploadImageFromDevice
                  picture_type={6}
                  upload_success={this.uploadFromDeviceSuccess}
                  upload_command={PUSH_TO_SOCKET_COMMAND}
                >
                  <button className="btn txt-center btn--p btn--b mbl">Tải ảnh lên</button>
                </UploadImageFromDevice>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <div className="well mbl" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SimilarToMyPhotosEmpty;
