import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import {
  DEFAULT_LOAD_FEMALE, DEFAULT_LOAD_MALE
} from 'constants/Enviroment';
import { OTHER_PICTURE } from 'constants/userProfile';

class SimilarToMyYmmFriendsEmpty extends Component {

  uploadFromFacebookSuccess = (data = null) => {
    if (data) {
      this.props.updateSelectedUser(data[0]);
      browserHistory.push(`/similar-face-to-my-fb-friends/${data[0].id}`);
    }
  }


  render() {
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE
    return (
      <div className="site-content">
        <div className="container">
          <div className="row mbl">
            <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
              <article className="mbl mc">
                <div className="card__upper">
                  <div href="#" className="card__link mbm">
                    <img src={default_image} alt="" className="card__img"/>
                  </div>
                  <div className="txt-center txt-light txt-lg mbm">
                    Hãy 1 người bạn bất kỳ, hệ thống sẽ tìm ra những bản sao của người đó. Đừng quên share kết quả với bạn ấy nhé!
                  </div>
                </div>
              </article>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <div className="well mbl" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SimilarToMyYmmFriendsEmpty;
