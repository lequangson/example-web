import React, { Component } from 'react';
import {
  DEFAULT_LOAD_FEMALE, DEFAULT_LOAD_MALE
} from '../../../constants/Enviroment';
import { OTHER_PICTURE } from '../../../constants/userProfile'
import { SIMILAR_TO_ME_PAGE } from '../Constant';
import UploadImageButton from '../../../components/commons/Buttons/UploadImageButton';


class SimilarToMeEmpty extends Component {
  render() {
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE
    return (
      <div className="site-content">
        <div className="container">
          <div className="row mbl">
            <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
              <article className="mbl mc">
                <div className="card__upper">
                  <div href="#" className="card__link mbm">
                    <img src={default_image} alt="" className="card__img"/>
                  </div>
                  <div className="txt-center txt-light txt-lg mbm">
                    Bạn cần có ít nhất một bức ảnh rõ mặt để sử dụng được tính năng này
                  </div>
                  <UploadImageButton className='btn btn--b btn--p' buttonName='Tải ảnh ngay' page_source={SIMILAR_TO_ME_PAGE} pictureType={OTHER_PICTURE} {...this.props} />
                </div>
              </article>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <div className="well mbl" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SimilarToMeEmpty;
