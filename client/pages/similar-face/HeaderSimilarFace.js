import React, { Component } from 'react';
import ShareButton from 'components/commons/Buttons/ShareButton';

HeaderSimilarFace.propTypes = {
  title: React.PropTypes.string,
  shareTitle: React.PropTypes.string,
  totalUser: React.PropTypes.number,
  current_user_id: React.PropTypes.string,
  mode: React.PropTypes.number,
  id: React.PropTypes.string,
}

function HeaderSimilarFace({title, shareTitle, totalUser, current_user_id, mode, id, sharingPictureUrl}) {
  const sharingPictureName = sharingPictureUrl ? sharingPictureUrl.split(/(\\|\/)/g).pop() : '';
  const shareUrl = `${process.env.ROOT_URL}/similar-sharing/${current_user_id}/${mode}/${id}/${sharingPictureName}`;
  return (
    <div className="container mbm">
      <div className="l-flex-ce">
        <h3 className="txt-heading">{totalUser ? `${totalUser} kết quả` : title}</h3>
         {!!totalUser && <div className="l-flex-vertical-center">
            <span className="mrs txt-dark">Chia sẻ:</span>
            <ShareButton
              title={shareTitle}
              shareUrl={shareUrl}
              round
              size={40}
              facebookShare
              twitterShare
              copyLink
            />
          </div>}
      </div>
    </div>
  )
}

export default HeaderSimilarFace;
