import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'components/modals/Modal'
import { Link, browserHistory } from 'react-router'
import _ from 'lodash';
import { ProgressiveImage } from 'components/commons/ProgressiveImage';
import { getAvatar } from 'scripts/showDefaultAvatar';
import * as UserDevice from 'utils/UserDevice';
import * as ymmStorage from 'utils/ymmStorage';
import GoogleOptimize from 'utils/google_analytic/google_optimize';
import UserPicture from 'components/commons/UserPicture';
import LikeButton from 'components/commons/LikeButton';
import { CDN_URL } from 'constants/Enviroment';
import StartChatButton from 'components/commons/StartChatButton';
import ShareButton from '../../../components/commons/Buttons/ShareButton';
import { MODAL_SIMILAR_TO_ME, MODAL_SIMILAR_TO_MY_YMM_FRIENDS,
  MODAL_SIMILAR_TO_MY_FB_FRIENDS, MODAL_SIMILAR_TO_MY_PHOTOS
} from '../Constant';

class ViewSimilarUserModal extends Component {
  constructor() {
    super();
    this.state = {
      modalTitle: [
        'Tướng phu thê quá cơ',
        'Người giống',
        'Người giống với',
        'Người giống với ảnh bạn tải lên',
      ]
    };
  }

  @GoogleOptimize
  componentDidUpdate(){}

  renderCard = () => {
    const { current_user, user } = this.props;
    let currentUserPicture = current_user.user_pictures.find((picture) => picture.is_most_similar);
    if (_.isEmpty(currentUserPicture)) {
      currentUserPicture = getAvatar(current_user);
    }
    let userPicture = user.user_pictures.find((picture) => picture.is_most_similar);
    if (_.isEmpty(userPicture)) {
      userPicture = getAvatar(user);
    }
    return (
      <div className="container pos-relative">
        <div className="row l-flex-vertical-center">
          <div className="col-xs-7 card-list element_height">
            <article className="card no-hover rotate-350" key={current_user.identifier}>
              <div className="card__upper">
                <div className="card__link">
                  <ProgressiveImage
                    preview={currentUserPicture.small_image_url}
                    src={ UserDevice.isHeighResolution()? currentUserPicture.extra_large_image_url : currentUserPicture.large_image_url}
                    render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                  />
                </div>
              </div>
            </article>
          </div>
          <div className="col-xs-5">
            <div>
              <div className="txt-medium txt-lg txt-blue mbs">{current_user.nick_name} </div>
              <div className="mbs">{current_user.age ? `${current_user.age.toString()} tuổi` : '?'}</div>
              <div className="mbs">{current_user.residence.value} </div>
            </div>
          </div>
        </div>

        <div className="perfect-center z-index-1400">
          <img className="img-50" src={`${CDN_URL}/general/Icon/heart.png`} />
        </div>

        <div className="row l-flex-vertical-center mbt">
          <div className="col-xs-5">
            <div className="txt-right">
              <div className="txt-medium txt-lg txt-blue mbs">{user.nick_name} </div>
              <div className="mbs">{user.age ? `${user.age.toString()} tuổi` : '?'}</div>
              <div className="mbs">{user.residence.value} </div>
            </div>
          </div>
          <div className="col-xs-7 card-list element_height">
            <article className="card no-hover rotate-10" key={user.identifier}>
              <div className="card__upper">
                <div className="card__link">
                  <ProgressiveImage
                    preview={userPicture.small_image_url}
                    src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                    render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                  />
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    )
  }

  renderShareButton() {
    const { modalType, shareTitle, mode, id, current_user, user } = this.props;
    const shareUrl = process.env.DOMAIN ?
      `https://m.${process.env.DOMAIN}/similar-sharing/${current_user.identifier}/${mode}/${id}` :
      `https://m.ymeet.me/similar-sharing/${current_user.identifier}/${mode}/${id}`;
    if (modalType === MODAL_SIMILAR_TO_MY_FB_FRIENDS) {
      return (
        <div className="txt-center l-flex">
          <ShareButton
            shareUrl={shareUrl}
            title={shareTitle}
            copyLink
            round
            facebookShare
            twitterShare
            copyLink
          />
        </div>
      );
    }
    return <div />;
  }

  renderLikeButton() {
    const { user, current_user, modalType } = this.props;
    if (modalType === MODAL_SIMILAR_TO_MY_YMM_FRIENDS ||
      modalType === MODAL_SIMILAR_TO_MY_PHOTOS
    ) {
      return (
        <div>
          <LikeButton
            user={user}
            user_profile={current_user}
            forceLike
            msg=''
            page_source={'VIEW_SIMILAR_FACE_MODAL'}
          />
          <StartChatButton user={user} shortText={false} page_source={'VIEW_SIMILAR_FACE_MODAL'} />
        </div>
      );
    }
  }
  rendertwoButton() {
    const { user, current_user, modalType } = this.props;
    if (modalType === MODAL_SIMILAR_TO_ME) {
      return (
        <div className="row">
          <div className="col-xs-6 txt-center">
            <LikeButton
              user={user}
              user_profile={current_user}
              forceLike
              msg=''
              page_source={'VIEW_SIMILAR_FACE_MODAL'}
            />
            <StartChatButton user={user} shortText={false} page_source={'VIEW_SIMILAR_FACE_MODAL'} />
          </div>
          <div className="col-xs-6 txt-center">
            <button
              className="btn btn--b btn--5"
              onClick={() => browserHistory.push(`/profile/${user.identifier}`)}
            >
              Xem hồ sơ
            </button>
          </div>
        </div>
      );
    }
    return <div />;
  }

  render() {
    const { similarUserIndex, totalUser, modalType, current_user, user, target_name } = this.props;
    const isSimilarToMeModal = !!(modalType === MODAL_SIMILAR_TO_ME);
    const modalTitle = `${this.state.modalTitle[modalType]} ${target_name}`;
    const isHaveOppositeGender = user.gender !== current_user.gender;

    return (
      <Modal
          transitionName="modal"
          isOpen={this.props.isOpen}
          onRequestClose={this.props.closeModal}
          className="VoteButton modal__content"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          { this.props.isOpen &&
            <div>
              <div className={`well well--e txt-center txt-blue txt-medium txt-lg ${isSimilarToMeModal ? 'mbl' : 'mbl'}`}>{modalTitle}</div>
              <div className="pos-relative mbl">
                {
                  isSimilarToMeModal
                  ?
                  this.renderCard()
                  :
                  <UserPicture
                    go_to_profile={isHaveOppositeGender}
                    user={user}
                    show_similar_picture
                  />
                }
                <button
                  className={`pagi pagi--prev pagi--2${similarUserIndex > 0 ? '' : ' is-hide'}`}
                  onClick={() => this.props.showNextSimilarUser(-1)}
                >
                  <i className="fa fa-chevron-left"></i>
                </button>
                <button
                  className={`pagi pagi--next pagi--2${similarUserIndex !== totalUser - 1 ? '' : ' is-hide'}`}
                  onClick={() => this.props.showNextSimilarUser(1)}
                >
                  <i className="fa fa-chevron-right"></i>
                </button>
              </div>
              <div className="mbs">
              {this.renderShareButton()}
              {isHaveOppositeGender && this.renderLikeButton()}
              {isHaveOppositeGender && this.rendertwoButton()}
              </div>
              <button className="modal__btn-close" onClick={this.props.closeModal}>
                <i className="fa fa-times"></i>
              </button>
            </div>
          }
        </Modal>
    )
  }
}

ViewSimilarUserModal.propTypes = {
  modalShowNext: PropTypes.func,
  modeImageModal: PropTypes.string,
}

export default ViewSimilarUserModal;
