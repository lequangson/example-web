import React ,{ PropTypes } from 'react';
import { Link } from 'react-router';
import _ from 'lodash';
import { ProgressiveImage } from '../../../components/commons/ProgressiveImage';
import showOnlineStatus from '../../../src/scripts/showStatus';
import { defaultAvatarObject } from '../../../src/scripts/showDefaultAvatar';
import * as UserDevice from '../../../utils/UserDevice';

export default class CardItem extends React.Component {
  static propTypes = {
    user: PropTypes.object
  }

  render() {
    const { user } = this.props;
    let userPicture = user.user_pictures.find((picture) => picture.is_most_similar);
    if (_.isEmpty(userPicture)) {
      userPicture = user.user_pictures.length ?
      user.user_pictures[0] :
      defaultAvatarObject(user.gender, 'large')
    }

    return(
      <div className="col-xs-6 col-sm-6 col-md-6 card-list element_height">
        <article className="card mbm" key={user.identifier}>
          <div className="card__upper">
            <div onClick={() => this.props.onClick(user) } className="card__link">
              <ProgressiveImage
              preview={userPicture.small_image_url}
              src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
              render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
              />
              <div className="card__addon">
                <i className="fa fa-heart"></i>
                <div className="txt-bold">{user.compatibility}%</div>
              </div>
              <div className="card__pad"></div>
              <div className="card__meta card__meta--1">
                <div>
                  <span className={`dot dot--${showOnlineStatus(user.online_status)} mrs`}></span>
                  <span className="txt-bold txt-lg">{user.nick_name} - </span>
                  <span className="txt-bold txt-lg">{user.age ? user.age.toString() : '?'}</span>
                </div>
                <div className="txt-color-primary">
                {
                  (
                    typeof user.residence !== 'undefined' &&
                    typeof user.residence.value !== 'undefined' &&
                    user.residence.value
                    ) ? user.residence.value : 'Không rõ'
                }
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
      )
  }
}