import React, { PropTypes } from 'react'
import SharingCardItem from './SharingCardItem'

class ListSharingCardItem extends React.Component {
  render() {
    const { users, current_user } = this.props;
    const index = (this.props.index * 4) + 1;
    return (
      <div>
        { users.map((user, i) =>
          <SharingCardItem
            key={index + i}
            user={user}
            activeModalCallback={this.props.activeModalCallback}
            current_user={current_user}
          />
        )}
      </div>
    )
  }
}

ListSharingCardItem.propTypes = {
  users: PropTypes.array,
  index: PropTypes.number,
  activeModalCallback: PropTypes.func.isRequired,
}

export default ListSharingCardItem
