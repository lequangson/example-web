import React ,{ Component, PropTypes } from 'react';
import { Link } from 'react-router'
import { ProgressiveImage } from '../../../components/commons/ProgressiveImage'
import { defaultAvatarObject } from '../../../src/scripts/showDefaultAvatar'
import GoogleOptimize from '../../../utils/google_analytic/google_optimize';
import * as UserDevice from '../../../utils/UserDevice';
import * as ymmStorage from '../../../utils/ymmStorage';
import { TOKEN_LOGIN } from '../Constant'

class SharingCardItem extends Component {
  static propTypes = {
      user: PropTypes.object.isRequired,
      activeModalCallback: PropTypes.func.isRequired,
  }

  showLoginModal = () => {
    this.props.activeModalCallback()
  }

  render() {
    const { user, current_user } = this.props;
    let userPicture = user.user_pictures.find((picture) => picture.is_most_similar);
    if (_.isEmpty(userPicture)) {
      userPicture = user.user_pictures.length ?
        user.user_pictures[0] :
        defaultAvatarObject(user.gender, 'large')
    }
    const isLogged = ymmStorage.getItem(TOKEN_LOGIN)
    return (
      <div className="col-xs-6 col-sm-6 col-md-3 card-list">
        <article className="card mbl shadow element_height" key={user.identifier}>
          <div className="card__upper">
            { (() => {
              if (!isLogged) {
                return (
                  <div className="card__link" onClick={this.showLoginModal}>
                    <ProgressiveImage
                      preview={userPicture.small_image_url}
                      src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                      render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                    />
                  </div>
                )
              } else if (current_user.gender !== user.gender) {
                return (
                  <Link to={`/profile/${user.identifier}`} className="card__link">
                    <ProgressiveImage
                      preview={userPicture.small_image_url}
                      src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                      render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                    />
                  </Link>
                )
              } else {
                return (
                  <div className="card__link">
                    <ProgressiveImage
                      preview={userPicture.small_image_url}
                      src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                      render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                    />
                  </div>
                )
              }
            })()}
          </div>
          <div className="card__under card__under--bg-white">
            <span className="card__text txt-lg">{user.nick_name}, </span>
            <span className="card__text txt-lg">{user.age} tuổi</span>
          </div>

        </article>
      </div>
    )
  }
}
export default SharingCardItem;