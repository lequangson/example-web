import React, { PropTypes } from 'react'
import CardItem from './CardItem'

class ListCardItems extends React.Component {
  render() {
    const users = this.props.users;
    const index = (this.props.index * 2) + 1;
    return (
      <div>
        { users.map((user, i) =>
          <CardItem
            key={index + i}
            user={user}
            onClick={this.props.onClick}
          />
        )}
      </div>
    )
  }
}

ListCardItems.propTypes = {
  users: PropTypes.array,
  index: PropTypes.number,
}

export default ListCardItems
