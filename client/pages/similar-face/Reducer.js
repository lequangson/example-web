import {
  GET_SIMILAR_SHARING_REQUEST,
  GET_SIMILAR_SHARING_SUCCESS,
  GET_SIMILAR_SHARING_FAILURE,
  GET_FACE_PARTNERS_REQUEST,
  GET_FACE_PARTNERS_SUCCESS,
  GET_FACE_PARTNERS_FAILURE,
  SHUFFLE_FACE_PARTNER
} from './Constant';
import { compareIdentifiers, shuffleArray } from 'utils/common';

const defaultState = {
  isFetching: false,
  users: [],
  reachEnd: false,
  totalItems: 0,
  shareForUserName: '',
  shareForUserPicture: '',
  sharingPictureUrl: '',
  facePartners: [],
  similarArray: [],
}

const partnersInTargetMe = (facePartners) => {
  return shuffleArray(facePartners).slice(0, 3);
}

const partnersInTargetPartner = (targetPartner, facePartners) => {
  const partnersWithoutTarget = facePartners.filter((partner) => {
    return !compareIdentifiers(partner.identifier, targetPartner.identifier);
  });
  return  shuffleArray(partnersWithoutTarget).slice(0, 2).concat(targetPartner) ;
}

const shuffledPartners = (partner_identifier, facePartners) => {
    const targetPartner = facePartners.find((partner) => {
      return compareIdentifiers(partner.identifier, partner_identifier);
    });
    return !partner_identifier || !targetPartner ? partnersInTargetMe(facePartners) : partnersInTargetPartner(targetPartner, facePartners);
  }

export default function similar_sharing(state = defaultState, action) {
  switch (action.type) {
    case GET_SIMILAR_SHARING_REQUEST:
      return Object.assign({}, state, {
        users: [],
        isFetching: true
      });
    case GET_SIMILAR_SHARING_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_SIMILAR_SHARING_SUCCESS:
    {
      const totalItem = action.response.data.similar_pictures ?
        action.response.data.similar_pictures.length : state.totalItems;
      return Object.assign({}, state, {
        users: action.response.data.similar_pictures,
        reachEnd: true,
        isFetching: false,
        totalItems: totalItem,
        shareForUserName: action.response.data.user_picture.name,
        shareForUserPicture: action.response.data.user_picture.url,
        sharingPictureUrl: action.response.data.sharing_picture.url,
      });
    };
    case GET_FACE_PARTNERS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case GET_FACE_PARTNERS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_FACE_PARTNERS_SUCCESS:
    {
      const facePartners = action.response.data;
      const partner_identifier = action.extentions.partner_identifier;
      const similarArray = shuffledPartners(partner_identifier, facePartners);

      return Object.assign({}, state, {
        similarArray,
        facePartners,
        isFetching: false
      });
    };
    case SHUFFLE_FACE_PARTNER:
      const similarArray = shuffleArray(state.facePartners).slice(0, 3);
      return Object.assign({}, state, {
        similarArray,
    });
    default:
      return state;
  }
}