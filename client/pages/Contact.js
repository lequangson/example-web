import React from 'react'
import { connect } from 'react-redux'
import HeaderContainer from '../containers/HeaderContainer'
import ContactContainer from '../containers/ContactContainer'
import AnonymousHeader from '../components/headers/AnonymousHeader'
import AnonymousFooter from '../components/footers/AnonymousFooter'
import * as ymmStorage from '../utils/ymmStorage';

const Contact = (props) => {
  window.scroll(0, 0)
  let isAnonymousUser = true
  try {
    if (props.user_profile && props.user_profile.current_user && props.user_profile.current_user.welcome_page_completion === 4) {
      isAnonymousUser = false
    }
  } catch (e) {}
  return (
    <div>
      {isAnonymousUser && <AnonymousHeader />}
      {!isAnonymousUser && <HeaderContainer {...props} /> }
      <ContactContainer {...props} />
      {isAnonymousUser && <AnonymousFooter />}
    </div>
  )
};

function mapStateToProps(state) {
  return state
}

const ContactPage = connect(mapStateToProps)(Contact)
export default ContactPage
