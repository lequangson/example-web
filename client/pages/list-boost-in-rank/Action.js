import { CALL_API } from 'middleware/api';
import { SEARCH_PAGE_NEW } from 'constants/Enviroment';
import {
	GET_LIST_BOOST_IN_RANK_REQUEST, GET_LIST_BOOST_IN_RANK_SUCCESS,
  GET_LIST_BOOST_IN_RANK_FAILURE, SHOW_NEXT_BOOST_USER,
} from './Constant';

export function getListBoostInRank() {
  return {
    [CALL_API]: {
      endpoint: 'boost_in_rank',
      method: 'get',
      params: {},
      authenticated: true,
      types: [
        GET_LIST_BOOST_IN_RANK_REQUEST,
        GET_LIST_BOOST_IN_RANK_SUCCESS,
        GET_LIST_BOOST_IN_RANK_FAILURE]
    },
  };
}

export function showNextBoostUsers(pageSource) {
  return {
    type: SHOW_NEXT_BOOST_USER,
    pageSource
  }
}
