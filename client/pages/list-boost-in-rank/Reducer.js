import {
	GET_LIST_BOOST_IN_RANK_REQUEST, GET_LIST_BOOST_IN_RANK_SUCCESS,
  GET_LIST_BOOST_IN_RANK_FAILURE, SHOW_NEXT_BOOST_USER
} from './Constant';
import {makeShortList} from './Helpers';

const defaultState = getDefaultState();

function getDefaultState() {
  return {
    boostInRankUser: [],
    boostInRankUserNew: [],
    boostInRankUserOnline: [],
    isFetching: false,
    currentIndexNew: 0,
    currentIndexOnline: 0
  };
}

export default function ListBoostInRank(state = defaultState, action) {
  switch (action.type) {
    case GET_LIST_BOOST_IN_RANK_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case GET_LIST_BOOST_IN_RANK_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_LIST_BOOST_IN_RANK_SUCCESS: {
      const listBoostInRankNew = action.response.data.filter(item => item.is_new_user);
      const listBoostInRankOnline = action.response.data.filter(item => item.online_status === 'online');
      const shortListBoostInRankNew = makeShortList(listBoostInRankNew);
      const shortListBoostInRankOnline = makeShortList(listBoostInRankOnline);
      return Object.assign({}, state, {
        boostInRankUser: [...action.response.data],
        boostInRankUserNew: [...shortListBoostInRankNew],
        boostInRankUserOnline: [...shortListBoostInRankOnline],
        isFetching: false
      });
    }
    case SHOW_NEXT_BOOST_USER:
    {
      let currentIndex;
      switch (action.pageSource) {
        case 'online':
        {
          currentIndex = state.currentIndexOnline < state.boostInRankUserOnline.length - 1 ?
            state.currentIndexOnline + 1 : 0;
          return Object.assign({}, state, {
            currentIndexOnline: currentIndex
          });
        }
        case 'new':
          currentIndex = state.currentIndexNew < state.boostInRankUserNew.length - 1 ?
            state.currentIndexNew + 1 : 0;
          return Object.assign({}, state, {
            currentIndexNew: currentIndex
          });
        default:
          return state;
      }
    }
    default:
      return state;
  }
}
