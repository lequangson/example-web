export function makeShortList(listUser) {
  const shortListBoostInRank = []; // wrapper every short list online
  const numberShortList = 6; //total item in a short list
  const lengthList = listUser.length;
  const totalItemInShotList = Math.ceil(lengthList / numberShortList);
  if (totalItemInShotList <= 1) { //if total item in listUser less than numberShortList
    shortListBoostInRank[0] = listUser;
  } else {
    for (let i = 0; i < totalItemInShotList; i++) {
      if (i + 1 === totalItemInShotList && lengthList % numberShortList !== 0) {
        // create finally in array short list
        const surplus = lengthList % numberShortList;
        shortListBoostInRank[i] = listUser.slice(i * numberShortList).concat(listUser.slice(0, numberShortList - surplus));
      } else {
      shortListBoostInRank[i] = listUser.slice(i * numberShortList, (i + 1) * numberShortList);
      }
    }
  }
  return shortListBoostInRank;
}
