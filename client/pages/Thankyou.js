import React from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import ThankyouContainer from '../containers/ThankyouContainer'
import * as ymmStorage from '../utils/ymmStorage';

const Thankyou = props => {
  let isAnonymousUser = true
  try {
    // iOS does not support client storage in private mode
    if (ymmStorage.getItem('ymm_token')) {
      isAnonymousUser = false
    }
  } catch (e) {}
  return (
    <div>
      {!isAnonymousUser && <HeaderContainer {...props} />}
      <ThankyouContainer />
    </div>
  )
}

export default Thankyou
