import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import { browserHistory, Link } from 'react-router';
import 'public/landingpage-assets1/styles/main.css'
import 'public/styles/m-main.css'
import 'public/styles/notification.css'
import { LOGGING_TEXT } from 'constants/TextDefinition'
import { ANDROID_URL, IOS_URL } from 'constants/Enviroment'
import QuizHeader from './common/QuizHeader'
import QuizFooter from './common/QuizFooter'
import * as UserDevice from 'utils/UserDevice';
import { FacebookLoginButton, GoogleLoginButton } from 'components/auth';
import IosDownloadButton from 'components/commons/Buttons/IosDownloadButton'
import AndroidDownloadButton from 'components/commons/Buttons/AndroidDownloadButton'
import BannerAppInstall from 'components/commons/BannerAppInstall'
import ListQuestion from './list-item/ListQuestion'
import * as ymmStorage from 'utils/ymmStorage';
import { ITEM_PER_PAGE } from './Constant';

class QuizVuiPage extends Component {

  static propTypes = {
    auth: PropTypes.object.isRequired,
    quiz_vui: PropTypes.object.isRequired
  }
  componentWillMount() {
      const { quiz_vui } = this.props;
      if (!quiz_vui.data.length) {
        this.props.getQuizList();
      }
      if (!ymmStorage.getItem('ymm_token')) {
        ymmStorage.setItem('previous_url', '/quizvui');
      }
  }
  componentDidMount() {
    this.landingPageScript();
    if (window.FB) {
      FB.XFBML.parse();
    }
  }

  componentWillUnmount() {
    const h = document.getElementsByTagName('html')[0];
    h.classList.remove('landingpage');
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      const html = document.getElementsByTagName('html')[0];
      html.classList.add('no-touch');
    }
    ymmStorage.removeItem('previous_url');
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.quiz_vui.data.length && nextProps.quiz_vui.data.length) {
      this.preloadImages(nextProps.quiz_vui.data);
    }
  }

  landingPageScript() {
    // remove no-touch class
    const html = document.getElementsByTagName('html')[0];
    html.classList.remove('no-touch');
    html.classList.add('landingpage');

    // hamburger button
    const ham = document.querySelector('.js-hamburger');
    const nav = document.querySelector('.js-nav');
    if (ham) {
      ham.addEventListener('click', () => {
        ham.classList.toggle('is-active');
        nav.classList.toggle('is-show');
      });
    }
  }

  handlePageClick = (page) => {
    browserHistory.push(`/quizvui/${page.selected+1}`);
  }

  preloadImages(data) {
    const questionImgs = [];
    const answerImgs = [];
    data.forEach((item, i) => {
      questionImgs[i] = new Image();
      questionImgs[i].src = item.image_url;
      item.sorted_quiz_answers.forEach((item2, j) => {
        answerImgs[j] = new Image();
        answerImgs[j].src = item2.image_url;
      })
    })
  }

  render() {
    const { auth, quiz_vui, params } = this.props;
    const { isFetching, isAuthenticated } = auth;
    const app_url = UserDevice.getName() === 'ios' ? IOS_URL : ANDROID_URL;
    const totalItem = quiz_vui.data.length;
    const perPage = ITEM_PER_PAGE;
    const currentPage = params.page || 1;
    const pageCount = Math.ceil(totalItem / perPage);
    return (
      <div>
        {UserDevice.getName() === 'ios' && // || UserDevice.getName() === 'android'
          <BannerAppInstall
            isOpen={true}
            {...this.props}
            url={app_url}
          />
        }
        <div className="lp">
          <QuizHeader {...this.props}/>
          <section className="lp-hero lp-hero--1 l2">
            <div className="lp-hero__content l2 full-width">
              <h2 className="lp-hero__heading mblg">Hẹn hò online nghiêm túc<br />miễn phí đăng ký, trò chuyện dễ dàng</h2>
              {!isAuthenticated && <FacebookLoginButton />}
              {!isAuthenticated && <GoogleLoginButton />}
            </div>
          </section>
          <section className="lp-well lp-well--1">
            <div className="row">
              <div className="small-12 column">
                <ListQuestion quizQuestion={quiz_vui.data} />
                {pageCount > 1 && <div className="txt-center">
                    <ReactPaginate previousLabel={"<<"}
                        nextLabel={">>"}
                        breakLabel={<a href="">...</a>}
                        breakClassName={"break-me"}
                        pageCount={pageCount}
                        forcePage={currentPage - 1}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={this.handlePageClick}
                        containerClassName={"quiz-list__pagination list-unstyled"}
                        subContainerClassName={"pages pagination"}
                        activeClassName={"active"}
                    />
                  </div>
                }
              </div>
            </div>
          </section>
          <QuizFooter {...this.props}/>

          <div className={isFetching || quiz_vui.isFetching ? 'loader is-full-screen-load' : 'hide loader is-full-screen-load'}>
            <span className="loader__text">Đang tải dữ liệu...</span>
          </div>
        </div>
      </div>
    );
  }
}

export default QuizVuiPage
