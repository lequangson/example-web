import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import QuizVuiPage from './QuizVuiPage';
import QuizQuestion from './QuizQuestion';
import QuizAnswer from './QuizAnswer';
import {
  getQuizRequest,
  getQuizList
} from './Action';
import { QUIZ_VUI_ACTIVITY_CODE } from './Constant';
import {
  addActivityAudit
} from 'actions/Enviroment';

const QuizVui = props => (
  <div>
    <QuizVuiPage {...props} />
  </div>
)

function mapStateToProps(state) {
  return {
    auth: state.auth,
    quiz_vui: state.quiz_vui
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getQuizList: async () => {
      dispatch(getQuizRequest());
      return await dispatch(getQuizList());
    },
    addActivityAudit: async (originalValue) => await dispatch(addActivityAudit(QUIZ_VUI_ACTIVITY_CODE, null, originalValue, null))
  }
}

const QuizVuiContainer = connect(mapStateToProps, mapDispatchToProps)(QuizVui)
const QuizQuestionContainer = connect(mapStateToProps, mapDispatchToProps)(QuizQuestion)
const QuizAnswerContainer = connect(mapStateToProps, mapDispatchToProps)(QuizAnswer)
export default QuizVuiContainer;
export { QuizQuestionContainer as QuizQuestion, QuizAnswerContainer as QuizAnswer }