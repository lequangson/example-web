import React, { PropTypes } from 'react'
import GridItem from './GridItem'

class ListSimilarQuiz extends React.Component {
  render() {
    const quizs = this.props.quizs;
    return (
      <div className="lp-carousel-wrap">
        <div className="lp-carousel-title">
          <h3 className="txt-white">Có thể bạn quan tâm</h3>
        </div>
        <div className="lp-carousel" id="c1">
          { quizs.sort((a, b) => a.total_player < b.total_player).map((quiz, i) =>
            <GridItem
              key={i}
              quiz={quiz}
              onClick={this.props.onClick}
            />
          )}
        </div>
        <button className="lp-carousel__btn lp-carousel__btn--prev" id="c1-prev"><i className="fa fa-chevron-left fa-3x"></i></button>
        <button className="lp-carousel__btn lp-carousel__btn--next" id="c1-next"><i className="fa fa-chevron-right fa-3x"></i></button>
      </div>
    )
  }
}

ListSimilarQuiz.propTypes = {
  quizs: PropTypes.array,
}

export default ListSimilarQuiz
