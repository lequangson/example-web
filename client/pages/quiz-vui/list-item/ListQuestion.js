import React, { PropTypes } from 'react'
import ListItem from './ListItem'

class ListQuestion extends React.Component {
  render() {
    const quizQuestion = this.props.quizQuestion;
    return (
      <div className="row mbl" id="c1">
        {quizQuestion.sort((a, b) => a.total_player < b.total_player).map((question, i) =>
          <ListItem
            key={i}
            question={question}
            onClick={this.props.onClick}
          />
        )}
      </div>
    )
  }
}

ListQuestion.propTypes = {
  quizQuestion: PropTypes.array,
}

export default ListQuestion
