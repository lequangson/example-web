import React ,{ PropTypes } from 'react';
import { Link } from 'react-router';
import _ from 'lodash';
import { ProgressiveImage } from 'components/commons/ProgressiveImage';
import showOnlineStatus from 'src/scripts/showStatus';
import { defaultAvatarObject } from 'src/scripts/showDefaultAvatar';
import * as UserDevice from 'utils/UserDevice';

export default class CardItem extends React.Component {
	static propTypes = {
	  quiz: PropTypes.object
	}

	render() {
		const { question } = this.props;

		return(
			<div className="small-12 column mbm">
				<div className="quiz-wrap full-width rounded-5">
          <div className="row">
            <div className="column small-12 medium-12 large-4">
              <Link to={`/quizvui/question/${question.id}`} className="quiz__link">
                <img src={ question.image_url } alt={ question.title } />
              </Link>
            </div>
            <div className="column small-12 medium-12 large-8">
              <div className="quiz-list__item--content">
                <div className="quiz__text">
                  <div className="mbm quiz-list__text--heading">
                    <Link className="txt-black txt-lg txt-medium" to={`/quizvui/question/${question.id}`}>{ question.title }</Link>
                  </div>
                  {/*<div className="mbm quiz__text--description txt-light txt-thin">
                  { question.description }
                  </div>
                */}
                  <div className="l-flex-vertical-center txt-sm txt-light mbm">
                    <i className="quiz__liked--icon fa-users fa fa-2x"></i>
                    {question.total_player} người đã chơi
                  </div>
                </div>
                <div className="txt-center quiz-list__button padding-r20">
                  <Link className="btn txt-medium rounded-5 txt-center btn--second" to={`/quizvui/question/${question.id}`} >Bắt đầu</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		)
	}
}