import React ,{ PropTypes } from 'react';
import { Link } from 'react-router';
import _ from 'lodash';
import { ProgressiveImage } from '../../../components/commons/ProgressiveImage';
import showOnlineStatus from '../../../src/scripts/showStatus';
import { defaultAvatarObject } from '../../../src/scripts/showDefaultAvatar';
import * as UserDevice from '../../../utils/UserDevice';

export default class CardItem extends React.Component {
	static propTypes = {
	  quiz: PropTypes.object
	}

	render() {
		const { quiz } = this.props;

		return(
			<div className="lp-carousel__item lp-carousel__item--3">
				<article className="card">
          <div className="card__upper">
	         	<Link to={`/quizvui/question/${quiz.id}`}>
             <img src={ quiz.image_url } alt={ quiz.title } />
	          </Link>
          </div>
          <div className="card__under">
            <div className="quiz__text">
              <div className="mbm quiz__text--heading">
                <Link className="txt-black txt-h4 txt-bold" to={`/quizvui/question/${quiz.id}`}>{ quiz.title }</Link>
              </div>
              <div className="l-flex-vertical-center txt-sm txt-light mbm">
                <i className="quiz__liked--icon fa-users fa fa-2x"></i>
                {quiz.total_player} người đã chơi
              </div>
            </div>
            <div className="txt-center mbl">
              <Link
                className="btn txt-medium rounded-5 txt-center btn--second"
                to={`/quizvui/question/${quiz.id}`}
              >
                Bắt đầu
              </Link>
            </div>
          </div>
        </article>
			</div>
		)
	}
}