import {
  GET_QUIZ_REQUEST,
  GET_QUIZ_SUCCESS,
  GET_QUIZ_FAILURE
} from './Constant';

const defaultState = {
  isFetching: false,
  data: [],
}

export default function similar_sharing(state = defaultState, action) {
  switch (action.type) {
    case GET_QUIZ_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case GET_QUIZ_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_QUIZ_SUCCESS:
      return Object.assign({}, state, {
        data: action.response.data,
        isFetching: false,
      });
    default:
      return state;
  }
}