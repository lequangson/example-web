import { CALL_API } from 'middleware/api';
import {
  GET_QUIZ_REQUEST,
  GET_QUIZ_SUCCESS,
  GET_QUIZ_FAILURE
} from './Constant';
import {
  API_GET,
} from 'constants/Enviroment';
import { shuffleArray } from 'utils/common';
import * as ymmStorage from 'utils/ymmStorage';

export function getQuizRequest() {
  return {
    type: GET_QUIZ_REQUEST,
  }
}

export function getQuizList() {
  const isAnonymous = !ymmStorage.getItem('ymm_token');
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'get_quiz_list',
      params: {},
      authenticated: !isAnonymous,
      types: [
        GET_QUIZ_REQUEST,
        GET_QUIZ_SUCCESS,
        GET_QUIZ_FAILURE
      ],
      isAnonymous
    },
  }
}