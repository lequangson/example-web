import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { isEmpty, random } from 'lodash';
import CryptoJS from 'crypto-js';
import 'public/landingpage-assets1/styles/main.css'
import 'public/styles/m-main.css'
import 'public/styles/notification.css'
import Siema from 'siema/dist/siema.min.js'
import QuizHeader from './common/QuizHeader'
import QuizFooter from './common/QuizFooter'
import ListSimilarQuiz from './list-item/ListSimilarQuiz'
import { FacebookLoginButton } from 'components/auth';
import * as ymmStorage from 'utils/ymmStorage';
import { MINI_GAME_CDN_URL } from './Constant';

class QuizQuestion extends Component {

  componentWillMount() {
      const { quiz_vui } = this.props;
      if (!quiz_vui.data.length) {
        this.props.getQuizList();
      }
  }
  componentDidMount() {
    this.landingPageScript();
    if (window.FB) {
      FB.XFBML.parse();
    }
  }

  componentWillUnmount() {
    const h = document.getElementsByTagName('html')[0];
    h.classList.remove('landingpage');
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      const html = document.getElementsByTagName('html')[0];
      html.classList.add('no-touch');
    }
  }

  landingPageScript() {
    // remove no-touch class
    const html = document.getElementsByTagName('html')[0];
    html.classList.remove('no-touch');
    html.classList.add('landingpage');

    // hamburger button
    const ham = document.querySelector('.js-hamburger');
    const nav = document.querySelector('.js-nav');
    if (ham) {
      ham.addEventListener('click', () => {
        ham.classList.toggle('is-active');
        nav.classList.toggle('is-show');
      });
    }

    function initCarousel(id, nextID, prevID) {
      const carousel = document.getElementById(id);
      const next = document.getElementById(nextID);
      const prev = document.getElementById(prevID);
      const totalItem = carousel.querySelectorAll('.lp-carousel__item').length;

      if (!carousel || !next || !prev) {
        throw new Error('Only accept id, please check again!');
      }

      const c = new Siema({
        selector: carousel
      });

      function prevShouldShowOrHide(currentSlide) {
        if (c.currentSlide === 0) {
          prev.style.display = 'none';
        } else {
          prev.style.display = 'block';
        }
      }

      function nextShouldShowOrHide(currentSlide) {
        if (currentSlide === (totalItem - 1)) {
          next.style.display = 'none';
        } else {
          next.style.display = 'block';
        }
      }

      prevShouldShowOrHide(c.currentSlide);

      next.addEventListener('click', () => {
        c.next();
        nextShouldShowOrHide(c.currentSlide);
        prevShouldShowOrHide(c.currentSlide);
      });

      prev.addEventListener('click', () => {
        c.prev();
        nextShouldShowOrHide(c.currentSlide);
        prevShouldShowOrHide(c.currentSlide);
      });

      carousel.addEventListener('touchend', () => {
        nextShouldShowOrHide(c.currentSlide);
        prevShouldShowOrHide(c.currentSlide);
      });

      return;
    }

     // only init carousel for mobile device
     if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      initCarousel('c1', 'c1-next', 'c1-prev');
    }
  }

  viewResult = (question_id, answer_id, answerIdForMale, answerIdForFemale) => {
    const resultUrl =  `/quizvui/answer/${question_id}/${answer_id}/${answerIdForMale}/${answerIdForFemale}`;
    console.log('resultUrl', resultUrl);
    const quizAnswerSecretKey = CryptoJS.AES.encrypt(`${question_id}${answer_id}`, process.env.ANONYMOUS_TOKEN).toString();
    ymmStorage.removeItem('quizAnswerSecretKey');
    ymmStorage.setItem('quizAnswerSecretKey', quizAnswerSecretKey);
    browserHistory.push(resultUrl);
    return;
  }

  render() {
    const { quiz_vui, params } = this.props;
    const { question_id, share_img } = params;
    let share_img_name = null;
    if (share_img) {
      share_img_name = share_img.split(/(_=_)/g).pop();
    }
    if (!question_id) {
      browserHistory.push('/quizvui');
      return null;
    }
    const currentQuestion = quiz_vui.data.find(item => item.id == question_id);
    if (isEmpty(currentQuestion)) {
      // browserHistory.push('/quizvui');
      return null;
    }
    const randomNumber = random(0, currentQuestion.sorted_quiz_answers.length -1);
    const randomAnswer = currentQuestion.sorted_quiz_answers[randomNumber];
    const quizAnswerForMale = currentQuestion.sorted_quiz_answers.filter(item => item.gender == 1);
    const quizAnswerForFemale = currentQuestion.sorted_quiz_answers.filter(item => item.gender == 2);
    if (isEmpty(randomAnswer)) {
      // browserHistory.push('/quizvui');
      return null;
    }
    const similarQuizs = quiz_vui.data.filter(item => item.id != question_id);
    const isAuthenticated = ymmStorage.getItem('ymm_token');
    let answerIdForMale = 0;
    if (quizAnswerForMale.length) {
      const randomAnswerForMale = quizAnswerForMale[random(0, quizAnswerForMale.length -1)];
      answerIdForMale = randomAnswerForMale.id;
    }

    let answerIdForFemale = 0;
    if (quizAnswerForFemale.length) {
      const randomAnswerForFemale = quizAnswerForFemale[random(0, quizAnswerForFemale.length -1)];
      answerIdForFemale = randomAnswerForFemale.id;
    }

    let backToUrlAfterLogin = `/quizvui/answer/${question_id}/${randomAnswer.id}/${answerIdForMale}/${answerIdForFemale}`;
    if (!isAuthenticated) {
      ymmStorage.removeItem('previous_url');
      ymmStorage.setItem('previous_url', encodeURI(backToUrlAfterLogin));
      // The following for cheat url parameter from user
      const quizAnswerSecretKey = CryptoJS.AES.encrypt(`${question_id}${randomAnswer.id}${answerIdForMale}${answerIdForFemale}`, process.env.ANONYMOUS_TOKEN).toString();
      if (ymmStorage.getItem('quizAnswerSecretKey') != quizAnswerSecretKey) {
        ymmStorage.removeItem('quizAnswerSecretKey');
        ymmStorage.setItem('quizAnswerSecretKey', quizAnswerSecretKey);
      }
    }
    return (
      <div className="lp">
        <QuizHeader {...this.props}/>
        <section className="lp-hero lp-hero--1 qv">
        </section>
        {!isEmpty(currentQuestion) && <section className="lp-well">
            <div className="row">
              <div className="small-12 column">
                <div className="mbm quiz__cover-image--title">
                  <h3 className="txt-xlg txt-center">{currentQuestion.title}</h3>
                </div>
                <div className="quiz__cover-image txt-center">
                  {isAuthenticated && <Link className="quiz__cover-image--link"
                    onClick={() => this.viewResult(question_id, randomAnswer.id, answerIdForMale, answerIdForFemale)}

                  >
                      <img src={share_img_name ? `${MINI_GAME_CDN_URL}/${share_img_name}` : currentQuestion.image_url} alt={currentQuestion.title} />
                      <button className="quiz__cover-image--button rounded-5 btn txt-center btn--second">Xem kết quả</button>
                    </Link>
                  }

                  {!isAuthenticated && <Link className="quiz__cover-image--link">
                      <img src={share_img_name ? `${MINI_GAME_CDN_URL}/${share_img_name}` : currentQuestion.image_url} alt={currentQuestion.title} />
                      <FacebookLoginButton isQuizPage />
                    </Link>
                  }
                </div>
              </div>
            </div>
          </section>
        }
        <section className="lp-well">
          <div className="row">
            <div className="small-12 column">
              <ListSimilarQuiz quizs={similarQuizs} />
            </div>
          </div>
        </section>
        <QuizFooter {...this.props}/>
      </div>
    )

  }

}

QuizQuestion.propTypes = {

}

function mapStateToProps(state) {
  return state
}


export default connect(
  mapStateToProps,
)(QuizQuestion)
