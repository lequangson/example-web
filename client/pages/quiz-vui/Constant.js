import { CDN_URL } from 'constants/Enviroment';
export const GET_QUIZ_REQUEST = 'GET_QUIZ_REQUEST';
export const GET_QUIZ_SUCCESS = 'GET_QUIZ_SUCCESS';
export const GET_QUIZ_FAILURE = 'GET_QUIZ_FAILURE';
export const ITEM_PER_PAGE = 3;
export const QUIZ_VUI_ACTIVITY_CODE = 76;
export const MINI_GAME_CDN_URL = `${CDN_URL}/general/MiniGame/QuizVui/Answer`;
