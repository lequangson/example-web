import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router';
import { LANDINGPAGE_BASE_URL, BLOG_URL } from 'constants/Enviroment'

class QuizHeader extends Component {
  state = {  }
  render() {
    return (
      <header className="lp-header js-header">
        <div className="row">
          <div className="small-12 column l-flex l-flex--ac l-flex--header">
            <div className="lp-nav">
              <button className="hamburger hamburger--elastic js-hamburger" type="button"><span className="hamburger-box"><span className="hamburger-inner"></span></span></button>
              <h1 className="lp__logo">
                <Link to="/">
                  <img src={LANDINGPAGE_BASE_URL + 'Logo_white.png'} alt="Ymeet.me - hẹn hò an toàn cho phụ nữ" />
                </Link>
              </h1>
              <div className="lp-nav__main js-nav">
                <h1 className="lp-nav__logo">
                  <Link to="/">
                    <img src={LANDINGPAGE_BASE_URL + 'Logo_white.png'} alt="Ymeet.me - hẹn hò an toàn cho phụ nữ" />
                  </Link>
                </h1>
                <nav className="lp-nav__list">
                  <a className="lp-nav__link l2" href="https://ymeet.me/branding/" target="_blank" rel="noopener nofollow noreferrer" >Câu chuyện Ymeet.me</a>
                  <Link className="lp-nav__link l2 active" to="/quizvui">Quiz Vui</Link>
                  <a className="lp-nav__link l2" href={BLOG_URL} target="_blank" rel="noopener nofollow noreferrer" >Blog</a>
                  <Link className="lp-nav__link l2" to="/contact">Liên hệ</Link>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <div className="lp-header__blur"></div>
      </header>
    );
  }
}

export default QuizHeader;
