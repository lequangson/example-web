import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { isEmpty } from 'lodash';
import CryptoJS from 'crypto-js';
import 'public/landingpage-assets1/styles/main.css'
import 'public/styles/m-main.css'
import 'public/styles/notification.css'
import Siema from 'siema/dist/siema.min.js'
import QuizHeader from './common/QuizHeader'
import QuizFooter from './common/QuizFooter'
import ListSimilarQuiz from './list-item/ListSimilarQuiz'
import ShareButton from 'components/commons/Buttons/ShareButton';
import * as ymmStorage from 'utils/ymmStorage';

class QuizAnswer extends Component {

  componentDidMount() {
    // remove no-touch class
    const html = document.getElementsByTagName('html')[0];
    html.classList.remove('no-touch');
    html.classList.add('landingpage');
    const _this = this;
    setTimeout(function() {
      _this.landingPageScript();
    }, 200);
    if (window.FB) {
      FB.XFBML.parse();
    }
  }

  componentWillMount() {
    if(!ymmStorage.getItem('ymm_token')) {
      browserHistory.push('/quizvui');
      return;
    }
    const { quiz_vui, params } = this.props;
    const { question_id, answer_id } = params;
    // The following for cheat url parameter from user
    const answerIdDecode = CryptoJS.AES.decrypt(`${ymmStorage.getItem('quizAnswerSecretKey')}`, process.env.ANONYMOUS_TOKEN).toString(CryptoJS.enc.Utf8);
      if (answerIdDecode != `${question_id}${answer_id}`) {
        // browserHistory.push('/quizvui');
        // return;
      }
     this.props.addActivityAudit(question_id);
    if (!quiz_vui.data.length) {
      this.props.getQuizList();
    }
  }
  componentWillUnmount() {
    const h = document.getElementsByTagName('html')[0];
    h.classList.remove('landingpage');
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      const html = document.getElementsByTagName('html')[0];
      html.classList.add('no-touch');
    }
  }

  landingPageScript() {


    // hamburger button
    const ham = document.querySelector('.js-hamburger');
    const nav = document.querySelector('.js-nav');
    if (ham) {
      ham.addEventListener('click', () => {
        ham.classList.toggle('is-active');
        nav.classList.toggle('is-show');
      });
    }

    function initCarousel(id, nextID, prevID) {
      const carousel = document.getElementById(id);
      const next = document.getElementById(nextID);
      const prev = document.getElementById(prevID);
      const totalItem = carousel.querySelectorAll('.lp-carousel__item').length;

      if (!carousel || !next || !prev) {
        throw new Error('Only accept id, please check again!');
      }

      const c = new Siema({
        selector: carousel
      });

      function prevShouldShowOrHide(currentSlide) {
        if (c.currentSlide === 0) {
          prev.style.display = 'none';
        } else {
          prev.style.display = 'block';
        }
      }

      function nextShouldShowOrHide(currentSlide) {
        if (currentSlide === (totalItem - 1)) {
          next.style.display = 'none';
        } else {
          next.style.display = 'block';
        }
      }

      prevShouldShowOrHide(c.currentSlide);

      next.addEventListener('click', () => {
        c.next();
        nextShouldShowOrHide(c.currentSlide);
        prevShouldShowOrHide(c.currentSlide);
      });

      prev.addEventListener('click', () => {
        c.prev();
        nextShouldShowOrHide(c.currentSlide);
        prevShouldShowOrHide(c.currentSlide);
      });

      carousel.addEventListener('touchend', () => {
        nextShouldShowOrHide(c.currentSlide);
        prevShouldShowOrHide(c.currentSlide);
      });

      return;
    }

     // only init carousel for mobile device
     if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      initCarousel('c1', 'c1-next', 'c1-prev');
    }
  }

  render() {
    const { params, quiz_vui, user_profile } = this.props;
    if (isEmpty(quiz_vui)) {
      return null;
    }
    const { question_id, answer_id, answer_id_for_male, answer_id_for_female } = params;
    const { current_user } = user_profile;
    const selectedQuestion = quiz_vui.data.find(item => item.id == question_id);
    if (isEmpty(selectedQuestion)) {
      return null;
      // return <div>Không tìm thấy dữ liệu</div>;
    }
    let selectedId = answer_id;
    if (current_user.gender == 'male' && answer_id_for_male > 0) {
      selectedId = answer_id_for_male;
    }

    if (current_user.gender == 'female' && answer_id_for_female > 0) {
      selectedId = answer_id_for_female;
    }
    const selectedAnswer = selectedQuestion.sorted_quiz_answers.find(item => item.id == selectedId);
    if (isEmpty(selectedAnswer)) {
      browserHistory.push('/quizvui');
      return null;
    }
    selectedQuestion.total_player = selectedQuestion.total_player + 1;
    const similarQuizs = quiz_vui.data.filter(item => item.id != question_id);
    const sharingPictureName = selectedAnswer.image_url.split(/(\\|\/)/g).pop();
    const shareUrl = `${process.env.ROOT_URL}/quizvui/question/${question_id}/${encodeURI(selectedQuestion.title.replace('?', '_._'))}_=_${sharingPictureName}`;
    return (
      <div className="lp">
        <QuizHeader {...this.props}/>
        <section className="lp-hero lp-hero--1 qv">
        </section>
        <section className="lp-well">
          <div className="row">
            <div className="small-12 column">
              <div className="mbm quiz__cover-image--title l-flex-ce">
                <h3 className="txt-xlg txt-left">{selectedQuestion.title}</h3>
                <div className="txt-right-mc quiz__text">
                  <ShareButton
                    title={''}
                    shareUrl={shareUrl}
                    facebookShare
                    buttonTitle="Share"
                    round={false}
                    size={50}
                  />
                </div>
              </div>
              <div className="quiz-wrap">
                <div className="row">
                  <div className="small-12 medium-12 large-12 column">
                    <div className="quiz__cover-image txt-center">
                      <Link className="quiz__cover-image--link">
                        <img className="full-width" src={selectedAnswer.image_url} alt={selectedAnswer.title} />
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="lp-well">
          <div className="row">
            <div className="small-12 column">
              <ListSimilarQuiz quizs={similarQuizs} />
            </div>
          </div>
        </section>
        <QuizFooter {...this.props}/>
      </div>
    )

  }

}

QuizAnswer.propTypes = {

}

function mapStateToProps(state) {
  return state
}


export default connect(
  mapStateToProps,
)(QuizAnswer)