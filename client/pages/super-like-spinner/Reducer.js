import {
  GET_SUPER_LIKE_SPINNER_USERS_REQUEST, GET_SUPER_LIKE_SPINNER_USERS_SUCCESS, GET_SUPER_LIKE_SPINNER_USERS_FAILURE,
  UPDATE_SUPER_SPINNER_REDUCER
} from './Constant';

const defaultState = getDefaultState();

function getDefaultState() {
  return {
    isFetching: false,
    users: [],
    list_users_match: [],
    spinner_image_url: null,
    result_user: {},
  };
}

export default function super_like_spinner(state = defaultState, action) {
  switch (action.type) {
    case GET_SUPER_LIKE_SPINNER_USERS_REQUEST:
      return Object.assign({}, state, {
        spinner_image_url: null,
        isFetching: true
      });
    case GET_SUPER_LIKE_SPINNER_USERS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_SUPER_LIKE_SPINNER_USERS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        users: action.response.data.users,
        spinner_image_url: action.response.data.spinner_image_url,
      });
    case UPDATE_SUPER_SPINNER_REDUCER:
      return Object.assign({}, state, {
        ...action.obj
      });
    default:
      return state;
  }
}
