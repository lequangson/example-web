import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import HeaderContainer from 'containers/HeaderContainer';
import {
  increaseCurrentCoin, getPeopleMatchedMe
} from 'actions/userProfile';
import {
  getSuperLikeSpinnerUsers, sendSuperLikeSpinner, updateSuperSpinnerReducer
} from './Action';
import SuperLikeSpinnerPage from './SuperLikeSpinnerPage';

const SuperLikeSpinner = props => (
  <div>
    <HeaderContainer {...props} />
    <SuperLikeSpinnerPage {...props} />
  </div>
)

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    getSuperLikeSpinnerUsers: async (create_new) => {
      return await dispatch(getSuperLikeSpinnerUsers(create_new));
    },
    increaseCurrentCoin: (coin) => {
      dispatch(increaseCurrentCoin(coin));
    },
    sendSuperLikeSpinner: async (male_id, female_id) => {
      await dispatch(sendSuperLikeSpinner(male_id, female_id));
      await dispatch(getPeopleMatchedMe());
    },
    updateSuperSpinnerReducer: (obj) => {
      dispatch(updateSuperSpinnerReducer(obj));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SuperLikeSpinner);
