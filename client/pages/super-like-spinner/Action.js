import {
  GET_SUPER_LIKE_SPINNER_USERS_REQUEST, GET_SUPER_LIKE_SPINNER_USERS_SUCCESS, GET_SUPER_LIKE_SPINNER_USERS_FAILURE,
  SUPER_LIKE_SPINNER_REQUEST, SUPER_LIKE_SPINNER_SUCCESS, SUPER_LIKE_SPINNER_FAILURE,
  UPDATE_SUPER_SPINNER_REDUCER
} from './Constant';
import { CALL_API } from 'middleware/api';
import {
  API_GET,
  API_POST
} from 'constants/Enviroment';

export function getSuperLikeSpinnerUsers(create_new) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'super_match_spinner',
      params: {
        create_new
      },
      authenticated: true,
      types: [
        GET_SUPER_LIKE_SPINNER_USERS_REQUEST, GET_SUPER_LIKE_SPINNER_USERS_SUCCESS, GET_SUPER_LIKE_SPINNER_USERS_FAILURE,
      ]
    },
  }
}

export function sendSuperLikeSpinner(male_id, female_id) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'coin_consume',
      params: {
        coin_consume_type: 'coin_consume_play_super_match_spinner',
        male_id,
        female_id
      },
      authenticated: true,
      types: [
        SUPER_LIKE_SPINNER_REQUEST, SUPER_LIKE_SPINNER_SUCCESS, SUPER_LIKE_SPINNER_FAILURE
      ]
    },
  };
}

export function updateSuperSpinnerReducer(obj) {
  return {
    type: UPDATE_SUPER_SPINNER_REDUCER,
    obj
  };
}