import React, { PropTypes, Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { isEmpty } from 'lodash'
import { ProgressiveImage } from 'components/commons/ProgressiveImage'
import * as UserDevice from 'utils/UserDevice';
import { getCurrentUser } from 'actions/userProfile'
import { getAvatar } from 'scripts/showDefaultAvatar'
import StartChatButton from 'components/commons/StartChatButton'
import Modal from 'components/modals/Modal';
import { CDN_URL } from 'constants/Enviroment';
import { compareIdentifiers } from 'utils/common';
import * as ymmStorage from 'utils/ymmStorage';
import { SUPER_LIKE_SPINNER_COST } from '../Constant';

class SuperLikeSpinnerItemResult extends Component {
  state = {
    modalIsOpen: false,
  }

  openModal = () => {
    this.setState({ modalIsOpen: true });
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  handleStartChatClick = () => {
    const { matched_users, result_user } = this.props;
    const isAlreadyMatched = matched_users.filter(u => compareIdentifiers(u.identifier, result_user.identifier)).length > 0;
    if (isAlreadyMatched) {
      window.open(`${process.env.CHAT_SERVER_URL}chat/${result_user.identifier}`);
      return;
    }
    this.setState({ modalIsOpen: true });
  }

  handleSuperLike = () => {
    this.props.superLike();
    this.closeModal();
  }

  render() {
    const { page_source, result_user, coins } = this.props;
    if (isEmpty(result_user)) {
      return <div></div>
    }
    const userPicture = result_user.avatar;

    return (
      <div>
        <article className="card bg-transparent no-border">
          <div className="card__upper padding-b10" style={{'maxWidth': '120px',
          'margin': 'auto'}}>
            <div className="card__link">
              <ProgressiveImage
                preview={userPicture.small_image_url}
                src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                render={(src, style) => <img src={src} className="rounded" style={style} alt={result_user.nick_name} />}
              />
            </div>
          </div>
          <div className="card__bottom">
            <div className="card__meta txt-white padding-b10">
              <span className="txt-bold txt-lg">{result_user.nick_name} - </span>
              <span className="txt-bold txt-lg">{result_user.age ? result_user.age.toString() : '?'}</span>
              <span className="txt-bold txt-lg">{result_user.residence ? ` - ${result_user.residence}` : ''}</span>
            </div>
            <button
              className='btn btn--6 btn--p'
              onClick={this.handleStartChatClick}
            >
              <i className="fa fa-comments" /> Trò chuyện nào
            </button>
          </div>
        </article>
        <Modal
          transitionName="modal"
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="txt-center txt-blue padding-t10 padding-b10 txt-lg txt-bold">
            Tiêu <img className="mt banner__icon--1" src={`${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`} alt="Xu" /> 100 xu để trò chuyện
            <br /> không giới hạn với {result_user.nick_name}
          </div>
          <div className="modal__txt-content">
            <div className="txt-center padding-b20"> Số xu hiện có: <span className="txt-blue">{coins} xu</span></div>
            {(coins >= SUPER_LIKE_SPINNER_COST) ?
              <button
                onClick ={this.handleSuperLike}
                className='btn btn--p btn--b mbm'
              >
                Đồng ý
              </button>
              :
              <Link
                to="/payment/coin"
                className='btn btn--p btn--b mbm'
                onClick={() => {
                  ymmStorage.setItem('Payment_Intention', 'SUPER_LIKE_SPINNER');
                }}
              >
                Nạp thêm xu
              </Link>
            }
              <button
                onClick ={this.closeModal}
                className='btn btn--p btn--b'
              >
                Hủy
              </button>
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    );
  }
}

SuperLikeSpinnerItemResult.propTypes = {
  page_source: PropTypes.string.isRequired,
  data: PropTypes.object,
  coins_number: PropTypes.number,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    getCurrentUser: () => {
      dispatch(getCurrentUser())
    },
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(SuperLikeSpinnerItemResult);