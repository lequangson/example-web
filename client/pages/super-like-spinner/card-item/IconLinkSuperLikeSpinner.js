import React from 'react'
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { CDN_URL } from 'constants/Enviroment';
import { isEmpty } from 'lodash';

class IconLinkSuperLikeSpinner extends React.Component {

  render() {
    const { campaign } = this.props;
    if (isEmpty(campaign.data) || campaign.data.end_super_match_spinner_at == null) {
      return null;
    }
    return (
      <Link
        className="open-prize z-index-2"
        to="/super-like-spinner">
          <img className="img-50" src={`${CDN_URL}/general/SuperMatch/Spinner_button.png`} alt="Mở Vòng quay Cupid" />
      </Link>
    )
  }
}

function mapStateToProps(state) {
  return {
    campaign: state.campaign
  };
}

export default connect(mapStateToProps)(IconLinkSuperLikeSpinner);