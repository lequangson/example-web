import React, { Component, PropTypes } from 'react';
import Winwheel from 'winwheel';
import { CDN_URL } from 'constants/Enviroment';
import { Link, browserHistory } from 'react-router';
import { isEmpty, random } from 'lodash';
import * as UserDevice from 'utils/UserDevice';
import * as ymmStorage from 'utils/ymmStorage';
import Modal from 'components/modals/Modal';
import {
  THREE_MONTH_SUBCRIPTION_FREE_GIFT, RANDOM_MATCH_FREE_GIFT,
  SUPER_LIKE_FREE_GIFT, UNLOCK_CHAT_FREE_GIFT, BOOST_IN_RANK_FREE_GIFT
} from 'constants/userProfile';
import socket from 'utils/socket';
import * as deductionNofi from 'utils/notifications/campaign';
import SuperLikeSpinnerItemResult from './card-item/SuperLikeSpinnerItemResult';

var theWheel = null

var wheelSpinning = false;

class SuperLikeSpinnerPage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
      isShowResult: false,
      isSpinning: false,
      needReloadImage: false,
      imageLoaded: false,
    }
  }

  async componentWillMount() {
    const response_spinner = await this.props.getSuperLikeSpinnerUsers(0);
    const { spinner_image_url, users } = response_spinner.response.data;
    this.setState({ spinner_image_url, users });
    const { result_user } = this.props.super_like_spinner;
    if (!isEmpty(result_user)) {
      this.setState({ isShowResult: true });
    }
  }

  componentDidMount() {
    this.loadSpinnerScript(this.props.super_like_spinner.spinner_image_url);
  }

  componentDidUpdate() {
    if (this.props.user_profile.current_user.gender === 'female') {
      browserHistory.push('/whathot');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.super_like_spinner.spinner_image_url == null &&
      nextProps.super_like_spinner.spinner_image_url !== null
    ) {
      this.loadSpinnerScript(nextProps.super_like_spinner.spinner_image_url);
    }
  }

  loadSpinnerScript = (spinner_image_url) => {
    const widthScreen = UserDevice.getName() === 'pc' ? 434 : window.innerWidth - 30
    theWheel = new Winwheel({
      'numSegments'       : 6,
      'outerRadius'       : UserDevice.getName() !== 'pc' ? (widthScreen / 2 - 30) * 0.85 : (widthScreen / 2 - 13),
      'drawMode'          : 'image',
      'drawText'          : false,      // Need to set this true if want code-drawn text on image wheels.
      'textFontSize'      : 0,
      'textOrientation'   : 'curved',
      'textDirection'     : 'reversed',
      'textAlignment'     : 'outer',
      'textMargin'        : 5,
      'textFontFamily'    : 'monospace',
      'textStrokeStyle'   : 'black',
      'textLineWidth'     : 2,
      'textFillStyle'     : 'white',
      'segments'          :             // Define segments including image and text.
      [
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
      ],
      'animation' :
      {
        'type'     : 'spinToStop',
        'duration' : 5,
        'spins'    : 8,
        'callbackFinished' : this.callBack
      }
    });

    // Create new image object in memory.
    const loadedImg = new Image();

    // Create callback to execute once the image has finished loading.
    const that = this;
    loadedImg.onload = function()
    {
        theWheel.wheelImage = loadedImg;    // Make wheelImage equal the loaded image object.
        theWheel.drawWheelImage();          // Also call draw function to render the wheel.
        that.setState({ imageLoaded: true })
    }

    loadedImg.onerror = function()
    {
        that.loadSpinnerScript(that.state.spinner_image_url);
    }
    // Set the image source, once complete this will trigger the onLoad callback (above).
    if(spinner_image_url !== null){
      loadedImg.src = `${spinner_image_url}`;
    }
  }

  callBack = async () => {
    this.setState({ isShowResult: true, isSpinning: false, needReloadImage: true });
    window.scroll(0, 0);
  }
  // -------------------------------------------------------
  // Click handler for spin button.
  // -------------------------------------------------------
  startSpin = async () => {
    // Ensure that spinning can't be clicked again while already running.
    this.setState({ isSpinning : true });
    if (!this.props.super_like_spinner.isFetching) {
      this.props.getSuperLikeSpinnerUsers(1);
    }
    const result_user_index = random(0, 5);
    const { users } = this.state;
    this.props.updateSuperSpinnerReducer({ result_user: users[result_user_index] });
    if (wheelSpinning == false)
    {
        // Based on the power level selected adjust the number of spins for the wheel, the more times is has
        // to rotate with the duration of the animation the quicker the wheel spins.
        theWheel.animation.spins = 5;

        const stopAt = theWheel.getRandomForSegment(result_user_index + 1);

        theWheel.animation.stopAngle = stopAt;

        // Begin the spin animation by calling startAnimation on the wheel object.
        theWheel.startAnimation();
        // Set to true so that power can't be changed and spin button re-enabled during
        // the current animation. The user will have to reset before spinning again.
        wheelSpinning = true;
    }
  }

  superMatchStyle() {
    let heightScreen = 93;
    if(window.innerHeight < 980 && window.innerHeight > 640) {
      heightScreen = 118;
    }
    return {
      minHeight: heightScreen + 'vh',
      marginTop: '-1rem',
      backgroundImage: `url(${CDN_URL}/general/SuperMatch/Background.png)`,
      backgroundSize: 'cover'
    }
  }

  superMatchWrapStyle() {
    let heightScreen = 121;
    let backgroundImage = null;
    if(window.innerHeight < 995 && window.innerHeight > 680) {
      heightScreen = 100;
    }
    if(window.innerHeight <= 680) {
      heightScreen = 132;
    }
    if(!this.state.imageLoaded) {
      backgroundImage = 'none';
    }
    return {
      minHeight: heightScreen + 'vh',
      background: backgroundImage
    }
  }

  playSpinner = async () => {
    if (this.state.isSpinning) { return; }
    if (this.state.needReloadImage) {
      const { spinner_image_url, users } = this.props.super_like_spinner;
      this.setState({ spinner_image_url, users, needReloadImage: false, imageLoaded: false, isShowResult: false });
      this.loadSpinnerScript(this.props.super_like_spinner.spinner_image_url);
      return;
    }
    this.setState({ isShowResult: false });
    wheelSpinning = false;
    this.startSpin();
  }

  superLike = async () => {
    const { current_user } = this.props.user_profile;
    const { result_user } = this.props.super_like_spinner;
    try {
      window.open(`${process.env.CHAT_SERVER_URL}chat/${result_user.identifier}`);
      await this.props.sendSuperLikeSpinner(current_user.identifier, result_user.identifier);
      this.setState({ isShowResult: false });
      this.props.updateSuperSpinnerReducer({ result_user: {} });
      socket.emit('send_event', {
        type: 'show_super_like_spinner_notification',
        room: result_user.identifier,
        super_like_spinner_user: current_user,
      });
      this.props.increaseCurrentCoin(-100);
    } catch (err) {
      return;
    }
  }

  goBack = () => {
    browserHistory.push('/whathot');
  }

  render() {
    const { user_profile, campaign, super_like_spinner } = this.props;
    const { result_user } = super_like_spinner;
    const coins = user_profile.current_user ? user_profile.current_user.coins : 0;
    const buttonLink = this.state.needReloadImage ? `${CDN_URL}/general/SuperMatch/play_again.png` : `${CDN_URL}/general/SuperMatch/spin.png`
    return (
      <div>
        <div className="site-content lucky-spinner-content" style={this.superMatchStyle()}>
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <div className="spinner-wrap" style={this.superMatchWrapStyle()}>
                  <div className="spinner-result pos-relative padding-t20 txt-center">
                    {!this.state.isShowResult &&
                      <div className="txt-bold txt-white">
                        <img className="img-100" src={`${CDN_URL}/general/SuperMatch/2_hearts.png`} alt="Heart" />
                        <div className="txt-uppercase txt-xxlg padding-b10">Vòng quay cupid</div>
                        <div className="txt-lg">Quay thôi là có người yêu</div>
                      </div>
                    }
                    {(this.state.isShowResult && !isEmpty(result_user)) &&
                    <SuperLikeSpinnerItemResult
                      matched_users={this.props.user_profile.matched_users}
                      result_user={result_user}
                      superLike={this.superLike}
                      page_source='SUPER_MATCH_PAGE'
                      coins={coins}
                    />}
                    <button className="spinner__close txt-white" onClick={this.goBack}>
                      <i className="fa fa-times fa-3x"></i>
                    </button>
                  </div>
                  <canvas id="canvas" width='750' height='750'>
                    <p>Xin lỗi trình duyệt của bạn không hỗ trợ canvas, Vui lòng dùng trình duyệt khác.</p>
                  </canvas>
                  {this.state.imageLoaded && <img id="spin_button" className="img-50" src={buttonLink} alt="Spiner" onClick={this.playSpinner} />}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={!this.state.imageLoaded ? 'loader__1 loader is-load is-full-screen-load' : 'hide loader loader__1 is-full-screen-load'}>
          <div className="loader__text">
            <i className="fa fa-3x fa-heart"></i>
            <i className="fa fa-3x fa-heart"></i>
            <i className="fa fa-3x fa-heart"></i>
          </div>
        </div>
      </div>
    );
  }
}

SuperLikeSpinnerPage.propTypes = {
}

export default SuperLikeSpinnerPage;