import { CALL_API } from 'middleware/api';
import { API_DELETE } from 'constants/Enviroment';
import {
  DELETE_MY_ACCOUNT_REQUEST,
  DELETE_MY_ACCOUNT_SUCCESS,
  DELETE_MY_ACCOUNT_FAILURE,
} from './Constant';


export function deleteMyAccountRequest() {
  return {
    type: DELETE_MY_ACCOUNT_REQUEST,
  }
}

export function deleteMyAccount(reasons) {
  return {
    [CALL_API]: {
      method: API_DELETE,
      endpoint: 'users',
      params: {
        reasons: reasons,
      },
      authenticated: true,
      types: [
        DELETE_MY_ACCOUNT_REQUEST,
        DELETE_MY_ACCOUNT_SUCCESS,
        DELETE_MY_ACCOUNT_FAILURE,
      ],
    },
  }
}

export function deleteMyAccountFailure() {
  return {
    type: DELETE_MY_ACCOUNT_FAILURE,
  }
}