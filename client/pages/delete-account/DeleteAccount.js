import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import $ from 'jquery'
import {
  REGEX_MESSAGE,
} from 'constants/Enviroment'
import MediaProfile from 'components/commons/MediaProfile'
import NotPaymentList from 'components/searches/NotPaymentList'
import Header from 'components/headers/Header'

class DeleteAccount extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isHidden: true,
      isActiveSubmit: false,
    }
    this.deleteMyAccount = this.deleteMyAccount.bind(this)
    this.handleOnChange = this.handleOnChange.bind(this)
    this.validateMessage = this.validateMessage.bind(this)
  }

  handleOnChange() {
    const reasons = [];
    $.each($("input[name='reasons']:checked"), function() {
      reasons.push($(this).val());
    });
    if ($('#other')[0].checked) {
      this.validateMessage()
      this.setState({ isHidden: false })
    }
    else {
      this.setState({ isHidden: true })
      if (reasons.length > 0) {
        this.setState({ isActiveSubmit: true })
      }
      else {
        this.setState({ isActiveSubmit: false })
      }
    }
  }

  validateMessage() {
    if (this.textarea.value.replace(/(\r\n|\n| |\r)/gm, '').match(REGEX_MESSAGE)) {
      this.setState({ isActiveSubmit: true })
    } else {
      this.setState({ isActiveSubmit: false })
    }
  }

  deleteMyAccount() {
    const reasons = []
    $.each($("input[name='reasons']:checked"), function() {
      reasons.push($(this).val())
    })
    const reasonText = this.textarea.value
    if (reasonText) {
      reasons.push(reasonText)
    }
    const params = reasons.join(',').substring(0,250)
    // console.log(params)
    this.props.deleteMyAccount(params)
  }

  render() {
    const {current_user, matched_users } = this.props.user_profile;

    return (
      <div>
        <Header {...this.props} />
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3 className="txt-heading">Bạn có chắc chắn muốn xóa tài khoản?</h3>
              <MediaProfile user={current_user} />
            </div>
          </div>
          <div className="mbs"></div>
          <div className="row mbm">
            <div className="col-xs-12">
              <ul className="list-unstyled">
                <li>* Tất cả hình ảnh sẽ bị xóa hoàn toàn.</li>
                <li>* Tài khoản sẽ không hiển thị trong kết quả tìm kiếm.</li>
                <li>* Người khác không thể xem tài khoản của bạn nữa.</li>
              </ul>
            </div>
          </div>
          <div className="panel__heading mbm">
            <h3 className="panel__title">
              Lý do xóa tài khoản <span className="txt-sm txt-thin txt-italic txt-red"> (bắt buộc)</span>
            </h3>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="1" /> Đây là hành động tạm thời. Tôi sẽ quay lại
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="2" /> Tài khoản của tôi đã bị hack
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="3" /> Tôi mất quá nhiều thời gian cho việc sử dụng
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="4" /> Tôi nhận được quá nhiều thích
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="5" /> Tôi lo ngại về quyền riêng tư
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="6" /> Tôi không cảm thấy an toàn
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="7" /> Tôi không hiểu cách sử dụng
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="8" /> Tôi không thấy Ymeet.me hữu ích
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="9" /> Tôi có một tài khoản Ymeet.me khác
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} name="reasons" value="10" /> Tôi đã tìm được một nửa của mình
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="l-flex-grid__item l-flex-grid__item--1">
            <label className="form__input--c">
              <input type="checkbox" onChange={this.handleOnChange} id="other" name="reasons" value="11" /> Lí do khác
              <div className="fake-checkbox"></div>
            </label>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <textarea
                placeholder="Vui lòng giải thích thêm (bắt buộc)"
                rows="5"
                hidden={this.state.isHidden}
                ref={(ref) => {
                  this.textarea = ref
                }}
                onChange={this.validateMessage}
              ></textarea>
              {(!this.state.isHidden && !this.state.isActiveSubmit) && <span className="txt-sm txt-light txt-red">Từ 20 đến 1024 ký tự</span>}
            </div>
          </div>
          { matched_users.length > 0 &&
            <NotPaymentList onDeletePage={true} data={matched_users} />
          }
          <div className="col-xs-12">
            <button
              className="btn btn--p btn--b mbs"
              disabled={this.state.isActiveSubmit ? '' : 'disabled'}
              onClick={this.deleteMyAccount}
            >
              Tôi đồng ý xóa tài khoản
            </button>
            <button
              className="btn btn--b mbl"
              onClick={browserHistory.goBack}
            >Hủy</button>
          </div>
        </div>
      </div>
    )
  }
}

DeleteAccount.propTypes = {
  deleteMyAccount: PropTypes.func,
  user_profile: PropTypes.object,
}

export default DeleteAccount
