import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import DeleteAccount from './DeleteAccount';
import { deleteMyAccount } from './Action';
import * as ymmStorage from 'utils/ymmStorage';

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    deleteMyAccount: async (reasons) => {
      await dispatch(deleteMyAccount(reasons));
      if(ymmStorage.getItem('ymm_token')) {
        ymmStorage.removeItem('ymm_token')
        browserHistory.push('/thankyou')
      }
      else {
        browserHistory.push('/')
      }
    },
  }
}

export default connect(mapStateToProps, mapDispachToProps)(DeleteAccount);
