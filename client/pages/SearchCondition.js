import React from 'react'
import SearchConditionContainer from '../containers/SearchConditionContainer'
import HeaderContainer from '../containers/HeaderContainer'

const SearchCondition = props => (
  <div className="sticky-footer">
    <HeaderContainer {...props} />
    <SearchConditionContainer {...props} />
  </div>
)

export default SearchCondition
