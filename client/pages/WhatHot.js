import React from 'react'
import WhatHotContainer from '../containers/WhatHotContainer'
import HeaderContainer from '../containers/HeaderContainer'

const WhatHot = props => (
  <div className="sticky-footer">
    <HeaderContainer {...props} />
    <WhatHotContainer {...props} />
  </div>
)

export default WhatHot
