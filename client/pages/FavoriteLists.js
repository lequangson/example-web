import React from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import FavoriteListsContainer from '../containers/FavoriteListsContainer'

const FavoriteLists = props => (
  <div>
    <HeaderContainer {...props} />
    <FavoriteListsContainer {...props} />
  </div>
)

export default FavoriteLists
