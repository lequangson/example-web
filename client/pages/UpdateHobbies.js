import React from 'react'
import UpdateHobbiesContainer from '../containers/UpdateHobbiesContainer'
import HeaderContainer from '../containers/HeaderContainer'

const UpdateHobbies = props => (
  <div>
    <HeaderContainer {...props} />
    <UpdateHobbiesContainer {...props} />
  </div>
)

export default UpdateHobbies
