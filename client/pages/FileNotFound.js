import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import AnonymousHeader from '../components/headers/AnonymousHeader'
import HeaderContainer from '../containers/HeaderContainer'
import { ICON_404 } from '../constants/Enviroment'
import * as ymmStorage from '../utils/ymmStorage';

const FileNotFound = props => {
  let isAnonymousUser = true
  try {
    if (props.user_profile && props.user_profile.current_user && props.user_profile.current_user.welcome_page_completion === 4) {
      isAnonymousUser = false
    }
  } catch (e) {}
  return (
  <div>
    {!isAnonymousUser && <HeaderContainer {...props} /> }
    <div className="container">
      <div className="row mbl">
        <div className="col-xs-12">
          <h3 className="txt-heading">Trang này không tồn tại</h3>
        </div>
      </div>
      <div className="row txt-center">
        <img src={ICON_404} alt="" />
        <p>Rất tiếc, không tìm thấy trang!</p>
        <p>Vui lòng kiểm tra lại đường dẫn hoặc<br /><Link to="/contact">Liên hệ với chúng tôi</Link></p>
        {isAnonymousUser && <p><Link to="/"> Quay về trang chủ!</Link></p>}
      </div>
    </div>
  </div>
  )
}

function mapStateToProps(state) {
  return state
}

const FileNotFoundPage = connect(mapStateToProps)(FileNotFound)
export default FileNotFoundPage
