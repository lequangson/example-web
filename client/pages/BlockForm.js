import React from 'react'
import BlockFormContainer from '../containers/BlockFormContainer'
import HeaderContainer from '../containers/HeaderContainer'

const BlockForm = props => (
  <div>
    <HeaderContainer {...props} />
    <BlockFormContainer {...props} />
  </div>
)

export default BlockForm
