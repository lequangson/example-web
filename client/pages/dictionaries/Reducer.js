import notification from 'utils/errors/notification';
import { NOTIFICATION_ERROR } from 'constants/Enviroment';
import { shuffle } from 'lodash';
import { getAgeGroup } from 'utils/UserBussinessUtils';
import {
  GET_DICTIONARIES_REQUEST, GET_DICTIONARIES_SUCCESS, GET_DICTIONARIES_FAILURE,
  SHUFFLE_DEFAULT_MESSAGE
} from './Constant';

function dictionaries(state = {
    self_introduction_templates: [],
    "income_types":[
      {
        "key": 0,
        "value": "Không có"
      },
      {
        "key": 1,
        "value": "Dưới 5 Triệu VNĐ"
      },
      {
        "key": 2,
        "value": "5 đến 10 Triệu VNĐ"
      },
      {
        "key": 3,
        "value": "10 đến 15 Triệu VNĐ"
      },
      {
        "key": 4,
        "value": "15 đến 20 Triệu VNĐ"
      },
      {
        "key": 5,
        "value": "Trên 20 Triệu VNĐ"
      }
    ],
    "constellation_types":[
      {
        "key": 1,
        "value": "Bạch Dương"
      },
      {
        "key": 2,
        "value": "Kim Ngưu"
      },
      {
        "key": 3,
        "value": "Song Tử"
      },
      {
        "key": 4,
        "value": "Cự Giải"
      },
      {
        "key": 5,
        "value": "Sư Tử"
      },
      {
        "key": 6,
        "value": "Xử Nữ"
      },
      {
        "key": 7,
        "value": "Thiên Bình"
      },
      {
        "key": 8,
        "value": "Bọ cạp"
      },
      {
        "key": 9,
        "value": "Nhân Mã"
      },
      {
        "key": 10,
        "value": "Ma Kết"
      },
      {
        "key": 11,
        "value": "Bảo Bình"
      },
      {
        "key": 12,
        "value": "Song Ngư"
      },
    ],
    "blood_types": [
      {
        "key": 1,
        "value": "A"
      },
      {
        "key": 2,
        "value": "B"
      },
      {
        "key": 3,
        "value": "O"
      },
      {
        "key": 4,
        "value": "AB"
      }
    ],
    "smoking_types": [
      {
        "key": 2,
        "value": "Có"
      },
      {
        "key": 1,
        "value": "Không"
      },
      {
        "key": 5,
        "value": "Thi thoảng"
      }
    ],
    "intention_marriages": [
      {
        "key": 1,
        "value": "Kết hôn sớm"
      },
      {
        "key": 2,
        "value": "Trong vòng 2-3 năm tới"
      },
      {
        "key": 3,
        "value": "Khi gặp người phù hợp"
      },
      {
        "key": 4,
        "value": "Không muốn kết hôn"
      },
      {
        "key": 5,
        "value": "Chưa rõ lắm"
      }
    ],
    "willing_have_children": [
      {
        "key": 2,
        "value": "Không muốn có con"
      },
      {
        "key": 1,
        "value": "Có từ 1 đến 2 con"
      },
      {
        "key": 4,
        "value": "Có từ 3 con trở lên"
      },
      {
        "key": 3,
        "value": "Chưa rõ"
      }
    ],
    "house_works": [
      {
        "key": 1,
        "value": "Tự làm"
      },
      {
        "key": 2,
        "value": "Có thể làm"
      },
      {
        "key": 3,
        "value": "Muốn cùng làm"
      },
      {
        "key": 4,
        "value": "Muốn đối tác làm"
      }
    ],
    "hope_meets": [
      {
        "key": 1,
        "value": "Sau khi kết bạn"
      },
      {
        "key": 2,
        "value": "Kết bạn và thấy quan tâm"
      },
      {
        "key": 3,
        "value": "Kết bạn và nói chuyện hợp"
      }
    ],
    "first_dating_costs": [
      {
        "key": 1,
        "value": "Nam giới trả"
      },
      {
        "key": 2,
        "value": "Nam giới trả nhiều hơn"
      },
      {
        "key": 3,
        "value": "Chia đều"
      },
      {
        "key": 4,
        "value": "Không quan trọng"
      },
      {
        "key": 5,
        "value": "Sẽ thảo luận rồi quyết định sau"
      }
    ],
    "sociabilities": [
      {
        "key": 1,
        "value": "Thích chỗ đông người"
      },
      {
        "key": 2,
        "value": "Thích chỗ vắng người"
      },
      {
        "key": 3,
        "value": "Thích một mình"
      },
      {
        "key": 4,
        "value": "Dễ dàng để trở thành bạn bè"
      },
      {
        "key": 5,
        "value": "Cần thời gian để trở thành bạn bè"
      }
    ],
    "house_mates": [
      {
        "key": 1,
        "value": "Một mình"
      },
      {
        "key": 2,
        "value": "Cùng bạn"
      },
      {
        "key": 3,
        "value": "Cùng vật nuôi"
      },
      {
        "key": 4,
        "value": "Cùng gia đình"
      },
      {
        "key": 5,
        "value": "Khác"
      }
    ],
    "holidays": [
      {
        "key": 1,
        "value": "Thứ 7 và chủ nhật"
      },
      {
        "key": 2,
        "value": "Ngày trong tuần"
      },
      {
        "key": 3,
        "value": "Không cố định"
      },
      {
        "key": 4,
        "value": "Khác"
      }
    ],
    "drinkings": [
      {
        "key": 1,
        "value": "Có"
      },
      {
        "key": 2,
        "value": "Không"
      },
      {
        "key": 3,
        "value": "Thi thoảng"
      }
    ],
    "presence_child_statuses": [
      {
        "key": 1,
        "value": "Không có"
      },
      {
        "key": 2,
        "value": "Đã có con"
      },
      {
        "key": 3,
        "value": "Có. Không sống cùng"
      }
    ],
    "family_relation_types": [
      {
        "key": 1,
        "value": "Con trai cả"
      },
      {
        "key": 2,
        "value": "Con trai thứ 2"
      },
      {
        "key": 3,
        "value": "Con trai thứ 3"
      },
      {
        "key": 4,
        "value": "Con trai út"
      }
    ],
    "last_visits": [
      {
        "key": 1,
        "value": "Trực tuyến"
      },
      {
        "key": 2,
        "value": "Trong 24 giờ"
      },
      {
        "key": 3,
        "value": "Trong 3 ngày"
      },
      {
        "key": 4,
        "value": "Trong 1 tuần"
      },
      {
        "key": 5,
        "value": "Trong 2 tuần"
      },
      {
        "key": 6,
        "value": "Trong 1 tháng"
      },
      {
        "key": 7,
        "value": "Trong 3 tháng"
      },
      {
        "key": 8,
        "value": "Hơn 3 tháng"
      }
    ],
    "search_sorts": [
      {
        "key": 1,
        "value": "Mức điểm phù hợp"
      },
      {
        "key": 2,
        "value": "Thời gian đăng nhập"
      }
    ],
    "bad_user_report_types": [
      {
        "key": 1,
        "value": "Đã kết hôn"
      },
      {
        "key": 2,
        "value": "Đang có một mối quan hệ"
      },
      {
        "key": 3,
        "value": "Vì mục đích kinh doanh"
      },
      {
        "key": 4,
        "value": "Có giả mạo hay gian lận"
      },
      {
        "key": 5,
        "value": "Chỉ muốn tiền"
      },
      {
        "key": 6,
        "value": "Chỉ muốn một mối quan hệ ngắn hạn"
      },
      {
        "key": 7,
        "value": "Nói dối"
      },
      {
        "key": 8,
        "value": "Ảnh giả mạo"
      },
      {
        "key": 9,
        "value": "Thông tin cá nhân giả mạo"
      },
      {
        "key": 10,
        "value": "Tài khoản giả mạo"
      },
      {
        "key": 11,
        "value": "Tài khoản không phù hợp"
      },
      {
        "key": 12,
        "value": "Hủy cuộc hẹn vào phút cuối"
      },
      {
        "key": 13,
        "value": "Có lời lẽ lăng mạ hay tấn công"
      },
      {
        "key": 14,
        "value": "Rình rập"
      },
      {
        "key": 15,
        "value": "Có lời lẽ thô tục"
      },
      {
        "key": 16,
        "value": "Khác"
      }
    ],
    "body_types": [
      {
        "key": 1,
        "value": "Mảnh mai"
      },
      {
        "key": 2,
        "value": "Trung bình"
      },
      {
        "key": 3,
        "value": "Thể thao"
      },
      {
        "key": 4,
        "value": "Cơ bắp"
      },
      {
        "key": 5,
        "value": "Hơi mập"
      },
      {
        "key": 6,
        "value": "Đậm"
      },
      {
        "key": 7,
        "value": "Cao lớn"
      }
    ],
    "educations": [
      {
        "key": 1,
        "value": "Chưa tốt nghiệp PT"
      },
      {
        "key": 2,
        "value": "Tốt nghiệp PT"
      },
      {
        "key": 3,
        "value": "Học nghề"
      },
      {
        "key": 6,
        "value": "Trung cấp"
      },
      {
        "key": 8,
        "value": "Cao đẳng"
      },
      {
        "key": 10,
        "value": "Đại học"
      },
      {
        "key": 12,
        "value": "Thạc sĩ"
      },
      {
        "key": 14,
        "value": "Tiến sĩ"
      }
    ],
    "relationship_statuses": [
      {
        "key": 1,
        "value": "Độc thân"
      },
      {
        "key": 10,
        "value": "Độc thân(Đã ly hôn)"
      },
      {
        "key": 11,
        "value": "Độc thân (Người kia đã mất)"
      }
    ],
    "countries": [
      {
        "key": 1,
        "value": "Việt Nam"
      }
    ],
    "cities": [
      {
        "key": 1,
        "value": "An Giang",
        "parent_id": 1
      },
      {
        "key": 2,
        "value": "Bà Rịa - Vũng Tàu",
        "parent_id": 1
      },
      {
        "key": 3,
        "value": "Bạc Liêu",
        "parent_id": 1
      },
      {
        "key": 4,
        "value": "Bắc Kạn",
        "parent_id": 1
      },
      {
        "key": 5,
        "value": "Bắc Giang",
        "parent_id": 1
      },
      {
        "key": 6,
        "value": "Bắc Ninh",
        "parent_id": 1
      },
      {
        "key": 7,
        "value": "Bến Tre",
        "parent_id": 1
      },
      {
        "key": 8,
        "value": "Bình Dương",
        "parent_id": 1
      },
      {
        "key": 9,
        "value": "Bình Định",
        "parent_id": 1
      },
      {
        "key": 10,
        "value": "Bình Phước",
        "parent_id": 1
      },
      {
        "key": 11,
        "value": "Bình Thuận",
        "parent_id": 1
      },
      {
        "key": 12,
        "value": "Cà Mau",
        "parent_id": 1
      },
      {
        "key": 13,
        "value": "Cao Bằng",
        "parent_id": 1
      },
      {
        "key": 14,
        "value": "Cần Thơ",
        "parent_id": 1
      },
      {
        "key": 15,
        "value": "Đà Nẵng",
        "parent_id": 1
      },
      {
        "key": 16,
        "value": "Đắk Lắk",
        "parent_id": 1
      },
      {
        "key": 17,
        "value": "Đắk Nông",
        "parent_id": 1
      },
      {
        "key": 18,
        "value": "Đồng Nai",
        "parent_id": 1
      },
      {
        "key": 19,
        "value": "Đồng Tháp",
        "parent_id": 1
      },
      {
        "key": 20,
        "value": "Điện Biên",
        "parent_id": 1
      },
      {
        "key": 21,
        "value": "Gia Lai",
        "parent_id": 1
      },
      {
        "key": 22,
        "value": "Hà Giang",
        "parent_id": 1
      },
      {
        "key": 23,
        "value": "Hà Nam",
        "parent_id": 1
      },
      {
        "key": 24,
        "value": "Hà Nội",
        "parent_id": 1
      },
      {
        "key": 25,
        "value": "Hà Tĩnh",
        "parent_id": 1
      },
      {
        "key": 26,
        "value": "Hải Dương",
        "parent_id": 1
      },
      {
        "key": 27,
        "value": "Hải Phòng",
        "parent_id": 1
      },
      {
        "key": 28,
        "value": "Hòa Bình",
        "parent_id": 1
      },
      {
        "key": 29,
        "value": "Hậu Giang",
        "parent_id": 1
      },
      {
        "key": 30,
        "value": "Hưng Yên",
        "parent_id": 1
      },
      {
        "key": 31,
        "value": "TP. Hồ Chí Minh",
        "parent_id": 1
      },
      {
        "key": 32,
        "value": "Khánh Hòa",
        "parent_id": 1
      },
      {
        "key": 33,
        "value": "Kiên Giang",
        "parent_id": 1
      },
      {
        "key": 34,
        "value": "Kon Tum",
        "parent_id": 1
      },
      {
        "key": 35,
        "value": "Lai Châu",
        "parent_id": 1
      },
      {
        "key": 36,
        "value": "Lào Cai",
        "parent_id": 1
      },
      {
        "key": 37,
        "value": "Lạng Sơn",
        "parent_id": 1
      },
      {
        "key": 38,
        "value": "Lâm Đồng",
        "parent_id": 1
      },
      {
        "key": 39,
        "value": "Long An",
        "parent_id": 1
      },
      {
        "key": 40,
        "value": "Nam Định",
        "parent_id": 1
      },
      {
        "key": 41,
        "value": "Nghệ An",
        "parent_id": 1
      },
      {
        "key": 42,
        "value": "Ninh Bình",
        "parent_id": 1
      },
      {
        "key": 43,
        "value": "Ninh Thuận",
        "parent_id": 1
      },
      {
        "key": 44,
        "value": "Phú Thọ",
        "parent_id": 1
      },
      {
        "key": 45,
        "value": "Phú Yên",
        "parent_id": 1
      },
      {
        "key": 46,
        "value": "Quảng Bình",
        "parent_id": 1
      },
      {
        "key": 47,
        "value": "Quảng Nam",
        "parent_id": 1
      },
      {
        "key": 48,
        "value": "Quảng Ngãi",
        "parent_id": 1
      },
      {
        "key": 49,
        "value": "Quảng Ninh",
        "parent_id": 1
      },
      {
        "key": 50,
        "value": "Quảng Trị",
        "parent_id": 1
      },
      {
        "key": 51,
        "value": "Sóc Trăng",
        "parent_id": 1
      },
      {
        "key": 52,
        "value": "Sơn La",
        "parent_id": 1
      },
      {
        "key": 53,
        "value": "Tây Ninh",
        "parent_id": 1
      },
      {
        "key": 54,
        "value": "Thái Bình",
        "parent_id": 1
      },
      {
        "key": 55,
        "value": "Thái Nguyên",
        "parent_id": 1
      },
      {
        "key": 56,
        "value": "Thanh Hóa",
        "parent_id": 1
      },
      {
        "key": 57,
        "value": "Thừa Thiên - Huế",
        "parent_id": 1
      },
      {
        "key": 58,
        "value": "Tiền Giang",
        "parent_id": 1
      },
      {
        "key": 59,
        "value": "Trà Vinh",
        "parent_id": 1
      },
      {
        "key": 60,
        "value": "Tuyên Quang",
        "parent_id": 1
      },
      {
        "key": 61,
        "value": "Vĩnh Long",
        "parent_id": 1
      },
      {
        "key": 62,
        "value": "Vĩnh Phúc",
        "parent_id": 1
      },
      {
        "key": 63,
        "value": "Yên Bái",
        "parent_id": 1
      }
    ],
    "occupations": [
      {
        "key": 1,
        "value": "Nghệ thuật"
      },
      {
        "key": 2,
        "value": "Kinh doanh"
      },
      {
        "key": 3,
        "value": "Kỹ thuật"
      },
      {
        "key": 4,
        "value": "Luật pháp"
      },
      {
        "key": 5,
        "value": "Y dược"
      },
      {
        "key": 6,
        "value": "Giáo dục"
      },
      {
        "key": 7,
        "value": "Báo chí"
      },
      {
        "key": 8,
        "value": "Giải trí"
      },
      {
        "key": 16,
        "value": "Truyền thông"
      },
      {
        "key": 9,
        "value": "Ẩm thực"
      },
      {
        "key": 10,
        "value": "Hành chính"
      },
      {
        "key": 11,
        "value": "Thể thao"
      },
      {
        "key": 12,
        "value": "Công an"
      },
      {
        "key": 17,
        "value": "Quân đội"
      },
      {
        "key": 18,
        "value": "Xây dựng"
      },
      {
        "key": 19,
        "value": "Tài chính"
      },
      {
        "key": 20,
        "value": "Ngân hàng"
      },
      {
        "key": 21,
        "value": "CNTT"
      },
      {
        "key": 22,
        "value": "Nông nghiệp"
      },
      {
        "key": 23,
        "value": "Vận tải"
      },
      {
        "key": 24,
        "value": "Du lịch"
      },
      {
        "key": 15,
        "value": "Sinh viên"
      },
      {
        "key": 25,
        "value": "Chưa đi làm"
      },
      {
        "key": 13,
        "value": "Khác"
      }
    ],
    "personal_types": [
      {
        "key": 1,
        "value": "Tốt bụng"
      },
      {
        "key": 2,
        "value": "Thẳng thắn"
      },
      {
        "key": 3,
        "value": "Quyết đoán"
      },
      {
        "key": 4,
        "value": "Ôn hòa"
      },
      {
        "key": 7,
        "value": "Hướng nội"
      },
      {
        "key": 8,
        "value": "Hướng ngoại"
      },
      {
        "key": 9,
        "value": "Kỹ tính"
      },
      {
        "key": 10,
        "value": "Thông minh"
      },
      {
        "key": 11,
        "value": "Trung thực"
      },
      {
        "key": 12,
        "value": "Chu đáo"
      },
      {
        "key": 13,
        "value": "Lạc quan"
      },
      {
        "key": 14,
        "value": "Nhút nhát"
      },
      {
        "key": 16,
        "value": "Thanh lịch"
      },
      {
        "key": 17,
        "value": "Bình tĩnh"
      },
      {
        "key": 18,
        "value": "Khiêm tốn"
      },
      {
        "key": 20,
        "value": "Quan tâm"
      },
      {
        "key": 23,
        "value": "Lạnh lùng"
      },
      {
        "key": 24,
        "value": "Sáng tạo"
      },
      {
        "key": 27,
        "value": "Trách nhiệm"
      },
      {
        "key": 28,
        "value": "Hoạt ngôn"
      },
      {
        "key": 29,
        "value": "Biết lắng nghe"
      },
      {
        "key": 31,
        "value": "Năng động"
      },
      {
        "key": 33,
        "value": "Hiếu thắng"
      },
      {
        "key": 35,
        "value": "Nóng tính"
      },
      {
        "key": 36,
        "value": "Khoan dung"
      },
      {
        "key": 39,
        "value": "Tự lập"
      },
      {
        "key": 41,
        "value": "Cảm tính"
      }
    ],
    "self_introduction_templates": [
      "Xin chào!\nTôi hiện đang sống ở.....,xxx tuổi.\nCám ơn bạn đã xem profile của tôi!\nCông việc của tôi là... Thi thoảng xem fb tôi lại thấy pairs nên đã đăng ký dùng thử!\nVào ngày nghỉ tôi thường cùng mọi người làm..., tôi thích đi khám phá những nơi mới, vào những ngày đẹp trời thì tôi thường đi mua đồ.\nTrong tình yêu thì tôi là mẫu người.... nên tôi khá kém khoản .... ( cười)\nTôi mong muốn có thể gặp được những bạn mà trong cuộc sống thường nhật mình không có cơ hội gặp.",
      "Xin chào!\nTôi hiện đang sống ở ... và tôi ... tuổi.\nCám ơn bạn đã xem profile của tôi.\nTôi đăng ký trang web này với hi vọng sẽ cgặp được người thích hợp.\nDo tính tôi ... nên hay bị bạn bè trêu là....(cười)\nTôi thích nghe nhạc, và thích nhất là nghe ...\nTôi yêu thích được thử sức với những thứ mới , dạo gần đây tôi đang thử sức với xxx\nNếu có bạn nào biết rõ về xxx  thì hãy chỉ cho tôi nhé.\nTôi muốn xây dựng tình yêu với người có thể cùng tôi trải qua những giây phút vui vẻ.\nRất vui được làm quen với bạn.",
      "Xin chào!\nDo thấy trang web này trên facebook nên tôi đã đăng ký dùng thử.\nTôi hiện đang sống ở ... và tôi xxx tuổi. Tôi đang làm công việc...\nSở thích của tôi là ... và ... ( vd< xem phim)\nTôi thích .... ( vd xem phim)\nThật tình thì do công việc của tôi quá bận rộn nên tôi có rất ít cơ hội gặp gỡ các mối quan hệ mới.\nLần này hi vọng sẽ có cơ hội được làm quen và gặp gỡ với các bạn cùng sở thích.\nNếu có bạn nào cùng sở thích với tôi thì ấn Like nhé.",
      "Cám ơn bạn đã đọc profile của tôi.\nXin chào. Tôi hiện đang sống ở... và tôi .. tuổi. Quê tôi là...\ntừ khi còn là học sinh tôi đã thích..., bây giờ mỗi dịp cối tuần tôi vẫn duy trì sở thích này của mình.\nNgoài ra, tôi rất thích được chơi cùng với trẻ con, tôi thường hay chơi với trẻ con tại nhà bạn tôi ( cười)\nGần đây mọi người quanh tôi đều đã kết hôn  nên tôi nhận thấy mình cũng cần phải kết hôn.\nĐương nhiên tiền đề là phải có đối tượng trước , nên tôi rất hi vọng sẽ gặp gỡ và làm quen với người có thể cùng tôi trải qua những giấy phút quan trọng trong cuộc đời.\nRất vui được làm quen với bạn",
      "Xin chào.\nTôi tên là... , xxx tuổi. Tôi hiện đang làm...\nSở thích của tôi là.... tôi cũng khá thích những hoạt động ....( trong nhà hoặc ngoài trời).\nGần đây tôi hầu như không có thời gian rảnh nên nếu có thời gian thì tôi muốn làm....\nTôi cũng thích đi du lịch, nhưng mà dạo gần đây cũng không đi đâu xa được.\nTôi thích những giây phút thư giãn như.. (vd tắm suối nước nóng)\nTôi hi vọng sẽ được nói chuyện với người cùng sở thích nên nếu bạn cùng sở thích thì hãy ấn Like tôi nhé.\nRất vui được làm quen với bạn."
    ],
    like_msg_temp: {
      female: {
        like: {
          group_18_22: [
            {
              key: 0,
              value: '1,2,3,5... A có đánh rơi nhịp nào ko ^^? Có thì mình làm quen nhau nha!'
            },
            {
              key: 1,
              value: 'E thấy ảnh a ngầu cực ấy ^^. E có thể làm quen với a ko ạ?'
            },
            {
              key: 2,
              value: 'Soái ca ơiii cho e làm quen với nha ^^ Hị hị'
            },
            {
              key: 3,
              value: 'Hello oppa đẹp trai :D! Kết đôi với e nhé ạ hihi'
            },
            {
              key: 4,
              value: 'A gì ơi cho e làm quen nha! (E ngại quá mãi mới dám mở lời :P)'
            },
            {
              key: 5,
              value: 'E là cô gái ko thả thính, nhắn tin liều lĩnh để quen a ^^ hì hì'
            },
            {
              key: 6,
              value: 'Hi a! E tò mò về a ghê ^^. Mình làm quen được ko ạ?'
            },
            {
              key: 7,
              value: 'Chào aaa ^^ Cho e làm quen nhéee hihi'
            },
            {
              key: 8,
              value: 'A cool ghê ^^. Cho e làm quen với được ko ạ?'
            },
            {
              key: 9,
              value: 'Chào bạn. Cho mình làm quen ha ^^!'
            },
            {
              key: 10,
              value: 'Hi cậu. Tớ rất quan tâm đến cậu đó ^^. Cho tớ làm quen nha?'
            }
          ],
          group_23_26: [
            {
              key: 0,
              value: 'E chào a ạ, cho e làm quen với nhé! (Ngại ghê, hì hì)'
            },
            {
              key: 1,
              value: 'Nhìn anh ngầu thật đấy! Đồng ý kết bạn với e nhé ^^?'
            },
            {
              key: 2,
              value: 'Cho e làm quen a nhé, nhé, nhé (Chuyện quan trọng phải nhắc 3 lần :D)'
            },
            {
              key: 3,
              value: 'Hi a! Hình như mình chung nhiều sở thích đó ạ. E có thể tìm hiểu a thêm ko?'
            },
            {
              key: 4,
              value: 'E chào a. Hôm nay a có gì vui ko? Kể cho e nghe với'
            },
            {
              key: 5,
              value: 'Hi a. E rất thích các tấm ảnh của a ^^. E muốn tìm hiểu a thêm đc ko ạ?'
            },
            {
              key: 6,
              value: 'E chào a ạ! Chúng mình làm quen chút nha!'
            },
            {
              key: 7,
              value: 'Hi a. Hôm nay của a thế nào ạ? E rất muốn tìm hiểu thêm về a ^^'
            },
            {
              key: 8,
              value: 'Nhìn a e cứ quen quen ^^. Ko biết đã gặp chưa, nếu chưa thì mình cũng làm quen nhé'
            },
            {
              key: 9,
              value: 'Tớ nghĩ tớ và cậu rất hợp nhau đấy! Kết bạn nhé!'
            },
            {
              key: 10,
              value: 'Mình cảm thấy bạn rất đặc biệt. Cho mình làm quen được ko?'
            }
          ],
          group_27_35: [
            {
              key: 0,
              value: 'Ảnh a phong cách ghê. Cho e làm quen với nhé!'
            },
            {
              key: 1,
              value: 'Hình như mình có kha khá điểm chung đó ^^. E rất muốn tìm hiểu thêm về a'
            },
            {
              key: 2,
              value: 'Chào a ạ. Hôm nay a có đi làm ko? Nói chuyện với e cho bớt căng thẳng nè'
            },
            {
              key: 3,
              value: 'Chào a. Một ngày thế này a thường làm gì? Tâm sự với e chút đi'
            },
            {
              key: 4,
              value: 'Hi a. A thú vị thật đó. Mình cùng tìm hiểu nhau được ko ạ?'
            },
            {
              key: 5,
              value: 'Hi a. A có hình mẫu bạn gái nào ko? E rất tò mò đó ạ'
            },
            {
              key: 6,
              value: 'Hi a. E rất ấn tượng với những bức ảnh của a. Mình làm quen nhé?'
            },
            {
              key: 7,
              value: 'Hi a. Hình như e gặp a ở đâu rồi ^^. Mình trò chuyện nhé, biết đâu có duyên thật'
            },
            {
              key: 8,
              value: 'Chào a. Nhìn ảnh a e đã quý a rồi. Mình tìm hiểu nhau thêm được ko ạ?'
            },
            {
              key: 9,
              value: 'Hi bạn. Mình thấy bạn thú vị lắm ^^. Làm quen nhau nha!'
            },
            {
              key: 10,
              value: 'Cậu ơi, tớ ấn tượng về cậu lắm. Mình làm quen đi'
            }
          ],
          group_older_than_35: [
            {
              key: 0,
              value: 'Chào anh. Hôm nay của anh thế nào? Tâm sự với em được ko?'
            },
            {
              key: 1,
              value: 'Em chào anh! Nhìn ảnh anh em rất ấn tượng. Chúng mình làm quen nhé!'
            },
            {
              key: 2,
              value: 'Em chào anh. Em thấy anh rất cuốn hút, mình kết bạn được ko anh?'
            },
            {
              key: 3,
              value: 'Em chào anh. Em nghĩ mình rất hợp nhau, trò chuyện với em nhé'
            },
            {
              key: 4,
              value: 'Anh có tin vào duyên số ko? Em nghĩ mình có duyên đấy anh ạ. Kết bạn với em nhé'
            },
            {
              key: 5,
              value: 'Em chào anh. Mong anh có một ngày thật vui. Trò chuyện với em nhé!'
            },
            {
              key: 6,
              value: 'Em chào anh. Em thấy anh là 1 người đàn ông sâu sắc. Mình tìm hiểu nhau được ko ạ?'
            },
            {
              key: 7,
              value: 'Chào anh ạ. Dù hơi ngượng nhưng cho em làm quen với anh nhé!'
            },
            {
              key: 8,
              value: 'Em rất thích phong cách của anh! Mình làm quen nhé anh!'
            },
            {
              key: 9,
              value: 'Em chào anh. Em thấy anh trẻ hơn tuổi ấy. Làm quen với em nhé!'
            },
            {
              key: 10,
              value: 'Chào bạn. Mình rất ấn tượng với bạn, chúng ta kết bạn đi'
            }
          ],
        },
        like_back: {
          group_18_22: [
            {
              key: 0,
              value: 'E chào a ạ. Tự dưng e có linh cảm mình siêu hợp nhau ấy :))'
            },
            {
              key: 1,
              value: 'Uầy, ai like e thế này :))) Chào a đẹp zai ạ!'
            },
            {
              key: 2,
              value: 'Hi a, e đang định tìm a thì a tìm được trước rồi hì hì'
            },
            {
              key: 3,
              value: 'Tks a đã like e nhé! Mình làm quen nhau luôn nha?'
            },
            {
              key: 4,
              value: 'Cảm ơn a đã like e nhé :D. Nói chuyện với em nhé?'
            },
            {
              key: 5,
              value: 'Oaa :D E chào a ạ. A có onl đấy ko?'
            },
            {
              key: 6,
              value: 'Thả tim lại nè ^^, trúng a chưa hihi? Mình nói chuyện nha'
            },
            {
              key: 7,
              value: 'Ơ a từ đâu rơi xuống trái tim e thế này :))). E đùa chút ạ, rất vui được làm quen với a ^^~'
            },
            {
              key: 8,
              value: 'E chào a :D. E rất tò mò về a đấy, mình tâm sự chút đi'
            },
            {
              key: 9,
              value: 'Cảm ơn cậu nhé ^^. Tớ cũng rất vui được làm quen với cậu'
            },
            {
              key: 10,
              value: 'Chào bạn ^^ hì. Mình sẽ nói chuyện nhiều nhiều nha!'
            }
          ],
          group_23_26: [
            {
              key: 0,
              value: 'Hi a ^^. Rất vui được biết a ạ! Ngày hôm nay của a thế nào ạ?'
            },
            {
              key: 1,
              value: 'E rất vui được quen a. Ai giới thiệu trước bây giờ nhỉ, hihi?'
            },
            {
              key: 2,
              value: 'Hiiii. Cảm ơn a đã quan tâm e. Hôm nay a có gì vui ko ạ?'
            },
            {
              key: 3,
              value: 'Hi a. E tò mò về a rồi ấy. A giới thiệu mình trước đi, hì hì'
            },
            {
              key: 4,
              value: 'Đồng ý luôn ạ! Mình trò chuyện luôn bây giờ được ko a?'
            },
            {
              key: 5,
              value: 'Cảm ơn a đã khen e! E cũng rất vui được quen a :D'
            },
            {
              key: 6,
              value: 'Hi a. Hình như a là người hướng nội ạ? E thấy a có vẻ sâu sắc'
            },
            {
              key: 7,
              value: 'E rất thích con trai thẳng thắn như a nhé :D. Rất vui được làm quen với a ạ!!!'
            },
            {
              key: 8,
              value: 'E chào a. Cảm ơn a đã khen e :D. Mình trò chuyện nha!'
            },
            {
              key: 9,
              value: 'Hi cậu. Cảm ơn đã quan tâm tớ nhé. Sở thích của cậu là gì đó?'
            },
            {
              key: 10,
              value: 'Cảm ơn bạn vì đã quan tâm mình ^^. Chúng ta trò chuyện luôn đc chứ?'
            }
          ],
          group_27_35: [
            {
              key: 0,
              value: 'E chào a ạ. E cũng rất vui được làm quen với a!'
            },
            {
              key: 1,
              value: 'Dạ e cũng rất vui được quen a ^^. Ngày hôm nay của a thế nào ạ?'
            },
            {
              key: 2,
              value: 'E cũng thấy may mắn khi được quen a. Mình tìm hiểu nhau thêm a nhé!'
            },
            {
              key: 3,
              value: 'Cảm ơn a đã quan tâm e. E rất vui, mình có thể nói chuyện thêm ko ạ?'
            },
            {
              key: 4,
              value: 'Hi a. A có thể online lúc nào ạ? E muốn nói chuyện thường xuyên với a '
            },
            {
              key: 5,
              value: 'Chào a. E cũng rất vui khi thấy a chủ động làm quen. Mình nói chuyện giờ nha?'
            },
            {
              key: 6,
              value: 'Chào a. E cũng thấy a rất thú vị ^^. Mình tâm sự nhé!'
            },
            {
              key: 7,
              value: 'Hì hì, a quá lời rồi. Cảm ơn a nhé. Giờ mình trò chuyện luôn nha?'
            },
            {
              key: 8,
              value: 'Đồng ý ạ! E thấy a rất vui tính. Nói chuyện với a chắc sẽ vui lắm đây, hì'
            },
            {
              key: 9,
              value: 'Chào cậu. Cảm ơn lời khen nha. Cậu có sở thích gì nhỉ?'
            },
            {
              key: 10,
              value: 'Hi bạn. Rất vui được làm quen với bạn. Sở thích của bạn là gì nè?'
            }
          ],
          group_older_than_35: [
            {
              key: 0,
              value: 'Chào anh. Anh hay online lúc nào? Em có rất nhiều điều tò mò về anh ^^'
            },
            {
              key: 1,
              value: 'Em cảm ơn anh. Em cũng rất vui được biết anh. Mình tìm hiểu nhé!'
            },
            {
              key: 2,
              value: 'Em cảm ơn anh! Ngày hôm nay của anh thế nào ạ? Kể cho em nhé'
            },
            {
              key: 3,
              value: 'Anh quá lời rồi ^^. Em cảm ơn anh nhé. Em cũng muốn nói chuyện với anh nhiều'
            },
            {
              key: 4,
              value: 'Cảm ơn anh đã ghép đôi với em nhé. Mình nói chuyện giờ được ko vậy ạ?'
            },
            {
              key: 5,
              value: 'Em đồng ý ạ! Anh rảnh ko mình nói chuyện giờ nhé?'
            },
            {
              key: 6,
              value: 'Anh vui tính quá! Rất vui được biết anh ạ! '
            },
            {
              key: 7,
              value: 'Em đồng ý ạ! Được anh quan tâm em rất vui. Mình tìm hiểu nhau nhé!'
            },
            {
              key: 8,
              value: 'Em chào anh. Nhìn anh trẻ hơn tuổi nhiều ấy, có ai nói anh vậy chưa ạ? '
            },
            {
              key: 9,
              value: 'Em chào anh ạ. Em cũng có linh cảm mình rất hợp nhau. Nói chuyện với em được ko?'
            },
            {
              key: 10,
              value: 'Chào bạn. Mình cũng cảm thấy chúng ta rất hợp nhau. Sở thích của bạn là gì nhỉ?'
            }
          ],
        }
      },
      male: {
        like: {
          group_18_22: [
            {
              key: 0,
              value: 'Cô gái dễ thương ơi, cho a làm quen với! Năn nỉ đó :D!'
            },
            {
              key: 1,
              value: 'Người lạ ơi, e xinh thế ^^! Cho a làm quen với!'
            },
            {
              key: 2,
              value: 'Nhìn e quen quá ^^ Hay mình từng gặp ở đâu rồi nhỉ, trong giấc mơ chẳng hạn?'
            },
            {
              key: 3,
              value: 'Chào e. Cho a làm quen được ko? A hứa sẽ ngoan :P hì hì'
            },
            {
              key: 4,
              value: 'E xinh gái còn a thì đẹp trai, làm quen nhau có gì là sai? Hehe ^^'
            },
            {
              key: 5,
              value: 'Hi baby girl ^^. Cho a làm quen với!'
            },
            {
              key: 6,
              value: 'Nhìn ảnh bạn mà tim mình đập mạnh quá! Cho mình làm quen nhé?'
            },
            {
              key: 7,
              value: 'E có nụ cười đẹp quá ^^. Đồng ý ghép đôi với a được ko?'
            },
            {
              key: 8,
              value: 'E dễ thương quá ^^!! A rất muốn làm quen với e, e đồng ý nhé?'
            },
            {
              key: 9,
              value: 'Chào bạn, cho mình làm quen được ko? ^^ '
            },
            {
              key: 10,
              value: 'Hi cậu. Tớ rất ấn tượng về cậu ^^. Cho tớ làm quen nhé?'
            }
          ],
          group_23_26: [
            {
              key: 0,
              value: 'Chào cô gái, ngày hôm nay của e thế nào? Có thể chia sẻ cùng a ko?'
            },
            {
              key: 1,
              value: 'A ko giỏi ăn nói nhưng chân thành thì số 1 luôn. Cho a làm quen nhé!'
            },
            {
              key: 2,
              value: 'Hi e. Nói chuyện với a chút nha, cô gái xinh đẹp!'
            },
            {
              key: 3,
              value: 'Chào e. A có cảm giác chúng mình rất đẹp đôi. Kết bạn với a nhé!'
            },
            {
              key: 4,
              value: 'A rất tò mò về e. Cho phép a tìm hiểu e thêm được ko nè?'
            },
            {
              key: 5,
              value: 'Hello e gái. E cười xinh mà hình như hơi buồn... Có muốn tâm sự gì với a ko?'
            },
            {
              key: 6,
              value: 'Chào e! E đang làm gì đó? Nói chuyện với a nhé'
            },
            {
              key: 7,
              value: 'Công việc của e có bận lắm ko ^^? Nói chuyện với a cho bớt căng thẳng nhé'
            },
            {
              key: 8,
              value: 'Chào e. Nhìn e quen lắm. Cho a làm quen nhé, biết đâu có duyên'
            },
            {
              key: 9,
              value: 'Chào cậu. Hôm nay của cậu thế nào? Mình làm quen nhé!'
            },
            {
              key: 10,
              value: 'Mình cảm thấy bạn rất đặc biệt ^^. Cho mình làm quen được ko?'
            }
          ],
          group_27_35: [
            {
              key: 0,
              value: 'Chào cô gái :). Rất vui được làm quen với e'
            },
            {
              key: 1,
              value: 'Chào e gái. E đang làm gì đó? Trò chuyện với a chút nhé'
            },
            {
              key: 2,
              value: 'Hi e. Nhìn e năng động đáng yêu ghê. Cho a làm quen nhé'
            },
            {
              key: 3,
              value: 'Chào e. Hôm nay e có đi làm ko? Nói chuyện với a cho bớt căng thẳng nhé'
            },
            {
              key: 4,
              value: 'Chào e gái dễ thương. Đồng ý ghép đôi với a được ko?'
            },
            {
              key: 5,
              value: 'Chào e gái. A là 1 người nội tâm và sống chân thành. Cho a làm quen e nhé?'
            },
            {
              key: 6,
              value: 'Cô gái xinh đẹp! Cho a cơ hội tìm hiểu e nhé!'
            },
            {
              key: 7,
              value: 'A phải lòng nụ cười của e mất rồi. Cho a làm quen e nhé?'
            },
            {
              key: 8,
              value: 'Chào e, thật là tò mò về e đấy ^^. Cho a cơ hội tìm hiểu e nhé?'
            },
            {
              key: 9,
              value: 'Chào bạn. Mình rất ấn tượng về bạn đấy. Cho mình làm quen đi'
            },
            {
              key: 10,
              value: 'Hi cậu. Tớ rất ấn tượng về cậu. Cho tớ làm quen nhé'
            }
          ],
          group_older_than_35: [
            {
              key: 0,
              value: 'Chào em gái. Anh là người rất chín chắn và chân thành. Cho anh làm quen em được ko?'
            },
            {
              key: 1,
              value: 'Chào cô gái, em có nụ cười rất đẹp. Cho anh làm quen e nhé?'
            },
            {
              key: 2,
              value: 'Chào em. Hôm nay em có gì vui ko? Kể anh được ko, anh sẵn sàng nghe đó'
            },
            {
              key: 3,
              value: 'Chào em gái. Anh ế lắm rồi nè ^^ vì chờ em đó! Đồng ý kết bạn với anh nhé'
            },
            {
              key: 4,
              value: 'Chào em. Anh thật thà ko biết thả thính, nhưng anh rất muốn làm quen với em. Em đồng ý đi'
            },
            {
              key: 5,
              value: 'Chào cô gái. Em đang làm gì đó? Đồng ý kết bạn với anh nhé'
            },
            {
              key: 6,
              value: 'Chào em. Thực ra anh trẻ hơn tuổi đó. Mình làm quen nhau nhé!'
            },
            {
              key: 7,
              value: 'Cơn gió nào đưa em đến đây? Cho anh làm quen nhé, em gái xinh đẹp'
            },
            {
              key: 8,
              value: 'Chào em gái cá tính. Anh rất muốn tìm hiểu em thêm, em đồng ý nhé?'
            },
            {
              key: 9,
              value: 'Anh chào em. Mong hôm nay của em thật vui. Nói chuyện với anh nhé?'
            },
            {
              key: 10,
              value: 'Chào bạn. Hình như chúng ta có nhiều điểm chung, kết bạn với mình nhé'
            }
          ],
        },
        like_back: {
          group_18_22: [
            {
              key: 0,
              value: 'Chào e, a cũng rất vui vì được làm quen với e :D'
            },
            {
              key: 1,
              value: 'A thấy mình rất may mắn vì được làm quen cô gái xinh như e ^^ hì'
            },
            {
              key: 2,
              value: 'Cảm ơn e đã cho a cơ hội tìm hiểu. Mình sẽ nói chuyện với nhau nhiều nhé :D'
            },
            {
              key: 3,
              value: 'Chào e. E muốn hỏi gì a ko, a hứa sẽ trả lời thật lòng! Hứa luôn ^^'
            },
            {
              key: 4,
              value: 'Ai gọi a đấy có a đây ^^!!! Hi cô gái xinh đẹp!'
            },
            {
              key: 5,
              value: 'Có cô gái xinh đẹp like vui ghê ^^! Hì. Chào bạn, mình làm quen nhé!'
            },
            {
              key: 6,
              value: 'Lâu lắm mới gặp cô gái chủ động như e. Cho a làm quen nha!'
            },
            {
              key: 7,
              value: 'Ok baby girl ^^. Cảm ơn e vì đã like. Mình nói chuyện nhé'
            },
            {
              key: 8,
              value: 'Tks e vì đã để ý đến a ^^! Nói chuyện với a nhé'
            },
            {
              key: 9,
              value: 'Tớ cũng rất ấn tượng với cậu đó ^^! Hôm nay của cậu thế nào?'
            },
            {
              key: 10,
              value: 'Cảm ơn đã like, mình đợi bạn mãi đấy ^^. Hôm nay của bạn thế nào?'
            }
          ],
          group_23_26: [
            {
              key: 0,
              value: 'Cô gái dễ thương! Sao a ko tìm ra e trước nhỉ. Nói chuyện nhiều với a nhé'
            },
            {
              key: 1,
              value: 'Hi cô gái! Thấy e là a phải like lại liền ^^. E đang làm gì đó?'
            },
            {
              key: 2,
              value: 'Chào ngày mới cô gái. Hôm nay e có rảnh ko, mình trò chuyện thêm nhé!'
            },
            {
              key: 3,
              value: 'A bất ngờ quá :D. Cô gái dễ thương ơi, cho a tìm hiểu e nhé'
            },
            {
              key: 4,
              value: 'Rất vui được làm quen với e. E có sở thích gì nhỉ?'
            },
            {
              key: 5,
              value: 'Chào e. E muốn hỏi gì a ko, a hứa sẽ trả lời thật lòng! Hứa luôn'
            },
            {
              key: 6,
              value: 'E dễ thương ghê! A cũng rất muốn hiểu thêm về e'
            },
            {
              key: 7,
              value: 'E gái cute thế. Bình thường e rảnh lúc nào? A rất muốn nói chuyện với e'
            },
            {
              key: 8,
              value: 'Chào e, cô gái cười duyên ^^. A rất vui khi e chủ động làm quen a thế này'
            },
            {
              key: 9,
              value: 'Chào cậu. Cảm ơn đã quan tâm tớ nhé. Hôm nay cậu làm gì thế?'
            },
            {
              key: 10,
              value: 'Rất vui vì được biết bạn. Giờ bạn có rảnh ko, chúng mình nói chuyện luôn?'
            }
          ],
          group_27_35: [
            {
              key: 0,
              value: 'Cảm ơn e đã quan tâm a. Ngày hôm nay của e thế nào?'
            },
            {
              key: 1,
              value: 'Chào e. A rất vui vì được quen e. Nói chuyện với a nhé?'
            },
            {
              key: 2,
              value: 'Cảm ơn vì đã ghép đôi với a :D. A rất muốn trò chuyện với e, e hay rảnh lúc nào?'
            },
            {
              key: 3,
              value: 'A rất thích các cô gái chủ động như e! Cho a tìm hiểu e nhé'
            },
            {
              key: 4,
              value: 'Nhìn e a cũng thấy quen quen, hì. Chắc chúng ta có duyên thật đó'
            },
            {
              key: 5,
              value: 'A cảm thấy e chính là mẫu bạn gái của a đó. Giờ a có thể nói chuyện với e ko nhỉ?'
            },
            {
              key: 6,
              value: 'Cảm ơn e đã để ý a. A cũng rất ấn tượng với e. Hôm nay của e thế nào?'
            },
            {
              key: 7,
              value: 'Cảm ơn lời khen của e ^^! A rất vui, mình nói chuyện với nhau thêm nhé?'
            },
            {
              key: 8,
              value: 'A cũng rất muốn tìm hiểu e. A đoán e là cô gái hướng nội, phải vậy ko?'
            },
            {
              key: 9,
              value: 'Chào cậu. Cảm ơn lời khen nhé. Hôm nay cậu rảnh chứ, mình nói chuyện nha'
            },
            {
              key: 10,
              value: 'Chào bạn. Rất vui được làm quen với bạn. Lúc rảnh bạn thường làm gì?'
            }
          ],
          group_older_than_35: [
            {
              key: 0,
              value: 'Chào em, cảm ơn em đã quan tâm anh nhé. Mình trò chuyện luôn nhé?'
            },
            {
              key: 1,
              value: 'Cảm ơn em đã khen. Mình sẽ nói chuyện nhiều nhé! Em hay online lúc nào?'
            },
            {
              key: 2,
              value: 'Chào em. Cảm ơn em đã like anh nhé. Mình trò chuyện luôn được ko nhỉ?'
            },
            {
              key: 3,
              value: 'Em gái xinh đẹp!. Anh tin là mình có duyên. Nói chuyện thêm với anh đi'
            },
            {
              key: 4,
              value: 'Cảm ơn lời khen của em nhé! Nói chuyện với anh được ko?'
            },
            {
              key: 5,
              value: 'Chào em. Rất vui được làm quen với em. Ngày hôm nay của em vui chứ?'
            },
            {
              key: 6,
              value: 'Chào em, anh cũng rất ấn tượng với em. Nói chuyện với anh nhé?'
            },
            {
              key: 7,
              value: 'Chào em, hy vọng hôm nay em cảm thấy vui. Mình nói chuyện nhé?'
            },
            {
              key: 8,
              value: 'Em có vẻ là một cô gái rất tinh tế ^^. Anh cảm thấy may mắn khi quen được em!'
            },
            {
              key: 9,
              value: 'Cảm ơn đã ghép đôi với anh. Mình nói chuyện giờ được luôn nhé'
            },
            {
              key: 10,
              value: 'Chào bạn. Rất vui được biết bạn. Hôm nay của bạn thế nào?'
            }
          ],
        }
      }
    },
  }, action) {
    switch (action.type) {
      case GET_DICTIONARIES_REQUEST:
        return Object.assign({}, state, {
          isFetching: true,
          error_message: null
        })
      case GET_DICTIONARIES_SUCCESS:
        return Object.assign({}, state, {isFetching: false}, {...action.response.data})
      case GET_DICTIONARIES_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
          isFetching: false,
          error_message: action.error_message
        })
      case SHUFFLE_DEFAULT_MESSAGE:
        const age_group = getAgeGroup(action.age);
        state.like_msg_temp[action.gender].like[age_group] = shuffle(state.like_msg_temp[action.gender].like[age_group]);
        state.like_msg_temp[action.gender].like_back[age_group] = shuffle(state.like_msg_temp[action.gender].like_back[age_group]);
        return state;
      default:
          return state
      }
}

export default dictionaries;