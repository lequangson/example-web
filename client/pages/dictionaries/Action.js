import { CALL_API } from 'middleware/api';
import {
  GET_DICTIONARIES_REQUEST, GET_DICTIONARIES_SUCCESS, GET_DICTIONARIES_FAILURE, SHUFFLE_DEFAULT_MESSAGE
} from './Constant';

export function getDictionariesRequest() {
  return {
    type: GET_DICTIONARIES_REQUEST,
  }
}

export function getDictionaries() {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'dictionaries',
      params: '',
      authenticated: true,
      types: [GET_DICTIONARIES_REQUEST, GET_DICTIONARIES_SUCCESS, GET_DICTIONARIES_FAILURE],
    },
  }
}

export function getDictionariesFailure() {
  return {
    type: GET_DICTIONARIES_FAILURE,
  }
}

export function shuffleMessage(gender, age) {
  return {
    type: SHUFFLE_DEFAULT_MESSAGE,
    gender,
    age
  }
}