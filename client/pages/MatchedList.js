import React from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import MatchedListContainer from '../containers/MatchedListContainer'

const MatchedList = props => (
  <div>
    <HeaderContainer {...props} />
    <MatchedListContainer {...props} />
  </div>
)

export default MatchedList
