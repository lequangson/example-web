import {
  FB_LOGIN_REQUEST, FB_LOGIN_SUCCESS, FB_LOGIN_FAILURE, FB_LOGIN_CLEAR_ERROR,
} from '../constants';

const initialState = {
  isFetching: false,
  status_code: undefined,
  errorMsg: [],
};

export default function fbLogin(state = initialState, action) {
  switch (action.type) {
    case FB_LOGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FB_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case FB_LOGIN_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        errorMsg: action.errorMsg.map(err => err),
        status_code: action.code,
      });

    case FB_LOGIN_CLEAR_ERROR:
      return Object.assign({}, state, {
        isFetching: false,
        errorMsg: [],
        status_code: undefined,
      });
    default:
      return state;
  }
}
