import * as nav from '../constants/navigation';

const initialState = {
  showNav: false
};

export default function navigation(state = initialState, action) {
  switch (action.type) {
    case nav.SHOW_NAVIGATION:
      return Object.assign({}, state, {
        showNav: true,
      });

    case nav.HIDE_NAVIGATION:
      return Object.assign({}, state, {
        showNav: false,
      });

    default:
      return state;
  }
}
