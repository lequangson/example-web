import { combineReducers } from 'redux';
import messages from './messages';
import messageBox from './messageBox';
import chatList from './chatList';
import fbLogin from './fbLogin';
import navigation from './navigation';
import user from './user';
import iceBreakingQuestions from './iceBreakingQuestions';

export default combineReducers({
  messages,
  messageBox,
  chatList,
  fbLogin,
  navigation,
  user,
  iceBreakingQuestions
});
