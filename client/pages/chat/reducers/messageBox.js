import {
  TEXT_BOX_HAS_VALUE,
  ENABLE_KEYBOARD,
  TOGGLE_EMOJI_SELECTION,
  UPDATE_TEXT_BOX_VALUE,
  INSERT_EMOJI,
  UPDATE_CARET_POSITION,
  UPDATE_MESSAGE,
} from '../constants';

const initialState = {
  textBoxhasValue: false,
  caretPosition: 0,
  emojiSelectionIsOpen: false,
  shouldClearMessage: false,
  shouldEnableKeyboard: false,
  shouldUpdateMessage: false,
  messageContent: '',
  emojiSelected: '',
};

export default function messageBox(state = initialState, action) {
  switch (action.type) {
    case TEXT_BOX_HAS_VALUE:
      return Object.assign({}, state, {
        textBoxhasValue: action.flag,
      });

    case ENABLE_KEYBOARD:
      return Object.assign({}, state, {
        shouldEnableKeyboard: action.flag,
      });

    case TOGGLE_EMOJI_SELECTION:
      return Object.assign({}, state, {
        emojiSelectionIsOpen: !state.emojiSelectionIsOpen,
      });

    case UPDATE_MESSAGE:
      return Object.assign({}, state, {
        shouldUpdateMessage: action.flag,
      });

    case UPDATE_TEXT_BOX_VALUE:
      return Object.assign({}, state, {
        messageContent: action.value,
      });

    case INSERT_EMOJI:
      return Object.assign({}, state, {
        emojiSelected: action.emoji,
        shouldUpdateMessage: true,
      });

    case UPDATE_CARET_POSITION:
      return Object.assign({}, state, {
        caretPosition: action.position,
      });

    default:
      return state;
  }
}
