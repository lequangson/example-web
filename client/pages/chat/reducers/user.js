import {
  GET_CHAT_USER_PROFILE_REQUEST, GET_CHAT_USER_PROFILE_SUCCESS, GET_CHAT_USER_PROFILE_FAILURE,
  GET_DETAIL_USER_PROFILE_REQUEST, GET_DETAIL_USER_PROFILE_SUCCESS, GET_DETAIL_USER_PROFILE_FAILURE,
  SEND_GOOGLE_ANALYTIC, UPDATE_PUSH_NOTI_MODAL_STATUS, IS_ASKED_PUSH_NOTI_REQUEST
} from '../constants';
import getUserIdFromIdentifier from '../helpers/getUserIdFromIdentifier';

import {
  sendGoogleAnalytic
} from '../helpers/sendGoogleAnalytic';

const initialState = {
  isFetching: false,
  data: {},
  errorMsg: '',
  keyWordFilter: '',
  selected_user: [],
  isPushNotiModalOpen: false,
  list_users_showed_push_noti_modal: [],
  isAskedPushNotiRequest: false,
};

export default function chatList(state = initialState, action) {
  switch (action.type) {
    case GET_CHAT_USER_PROFILE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case GET_CHAT_USER_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        data: action.res.data,
      });

    case GET_CHAT_USER_PROFILE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        errorMsg: action.errorMsg,
      });

    // case SEND_GOOGLE_ANALYTIC:
    //   sendGoogleAnalytic(action.eventAction, state.data, action.ga_track_data);
    //   return state;

    case GET_DETAIL_USER_PROFILE_REQUEST: //get user profile by identifier
        return Object.assign({}, state, {
            isFetching: true,
        })

    case GET_DETAIL_USER_PROFILE_SUCCESS:
        const new_user = action.payload.data.data
        const index = state.selected_user.findIndex(user => getUserIdFromIdentifier(new_user.identifier) === getUserIdFromIdentifier(user.identifier))
        return Object.assign({}, state, {
            selected_user: index === -1 ? state.selected_user.concat(new_user) : state.selected_user,
            isFetching: false,
        })

    case GET_DETAIL_USER_PROFILE_FAILURE:
         return Object.assign({}, state, {
            isFetching: false,
            errorMsg: action.error_message,
        })

    case UPDATE_PUSH_NOTI_MODAL_STATUS:
      return Object.assign({}, state, {
        isPushNotiModalOpen: action.isOpen,
        list_users_showed_push_noti_modal: action.to_user_id ? state.list_users_showed_push_noti_modal.concat(action.to_user_id) : state.list_users_showed_push_noti_modal,
      })

    case IS_ASKED_PUSH_NOTI_REQUEST:
      return Object.assign({}, state, {
        isAskedPushNotiRequest: action.isAsked,
      })
    default:
      return state;
  }
}
