import {
} from '../constants';
import getUserIdFromIdentifier from '../helpers/getUserIdFromIdentifier';

const initialState = {
  isFetching: false,
  data: [
    {
      question: 'Bạn có nghĩ XY là người thích du lịch không?',
      type: 'Sở thích',
      topic: 'Du lịch',
      answer: {
        yes: {
          male: [
            'Chào em. Anh thấy em là người cũng thích đi du lịch nhỉ?',
            'Chào cô gái. Chắc em cũng thích đi du lịch phải không? Em đã đi du lịch được nhiều chưa?',
            'Chào em. Có vẻ em cũng là người thích đi đây đi đó ha?'
          ],
          female: [
            'Chào anh. Đoán là anh cũng thích đi du lịch nhỉ? Không biết anh đã từng đến quê em chưa ^^',
            'Hi anh, thấy anh cũng có nhiều ảnh đi du lịch ghê, chắc là anh cũng đi được nhiều nơi rồi nhỉ?',
            'Nè, chắc anh cũng là người cuồng chân, hay đi giống em rồi ^^ Anh đã đi được những nơi nào rồi đó?'
          ]
        },
        no: {
          male: [
            'Chào cô gái. Anh thấy em khá bận rộn nên chắc không có nhiều thời gian đi du lịch đâu nhỉ?',
            'Chào em. Anh đoán là em có vẻ không thích đi phượt lắm, không biết có sai không nhỉ?',
            'Chào em, không biết em đã đi du lịch nước ngoài bao giờ chưa nhỉ?'
          ],
          female: [
            'Chào anh! Em đoán anh là người không thích du lịch mấy nhỉ? Nếu không đúng thì cho em làm quen nhé ^^ ',
            'Hm, có phải anh không thích đi phượt mấy nhỉ? E đoán vậy thôi, không biết có đúng không ^^',
            'Hm, có vẻ anh không đi du lịch nhiều lắm đúng không ạ? Hì hì, vì em không thấy anh chia sẻ nhiều ảnh đi chơi lắm ^^ '
          ]
        }
      }
    },
    {
      question: 'Bạn có nghĩ XY thích đọc sách không?',
      type: 'Sở thích',
      topic: 'Đọc sách',
      answer: {
        yes: {
          male: [
            'Chào cô gái. Anh nghĩ em là người thích đọc sách, phải không ta?',
            'Chào em. Anh đoán em rất mê đọc sách đúng không?',
            'Chào em. Đọc sách chắc hẳn là một sở trường của em phải không cô gái?'
          ],
          female: [
            'Hì hì, chắc anh cũng là người thích đọc sách ạ? Anh thường thích đọc những loại sách gì vậy?',
            'Hì, anh có thích đọc sách không ạ? Chắc là có nhỉ, em đoán thế ^^',
            'Chào anh. Mắt anh cận chắc hẳn là vì đọc nhiều sách rồi ^^ Dù đúng hay không thì nhớ trả lời em nhé ^^'
          ]
        },
        no: {
          male: [
            'Chào em. Anh đoán em không phải là mọt sách, không biết có đúng không nhỉ?',
            'Chào cô gái. Trông em có vẻ không giống mọt sách nhỉ, anh đoán có sai không ta?',
            'Chào em, không biết em thích đọc sách nhiều chữ hay ít chữ thế haha'
          ],
          female: [
            'Hi hi, anh chắc chắn không phải mấy chàng mọt sách nhỉ ^^ Lúc rảnh rỗi thì anh thường làm gì vậy ạ?',
            'Hi hì, trông anh không giống mấy chàng mọt sách lắm ^^ Sở thích của anh là gì vậy ạ?',
            'Chào anh, không biết anh thích đọc sách nhiều chữ hay ít chữ vậy, hìhi'
          ]
        }
      }
    },
    {
      question: 'Liệu XY có phải tuýp người thích nấu ăn, làm bánh không nhỉ?',
      type: 'Sở thích',
      topic: 'Nấu ăn, làm bánh',
      answer: {
        yes: {
          male: [
            'Chào em. Chắc em là người rất khéo tay và thích nấu ăn làm bánh nhỉ?',
            'Chào cô gái. Không biết em thích nấu món Tây hay món Việt hơn nhỉ?',
            'Chào em. Nấu ăn làm bánh chắc là một sở trường của em đấy nhỉ, cô gái?'
          ],
          female: [
            'Chào anh! Em tin anh là người biết nấu ăn giỏi! Haha, nếu đúng thì dạy cho em mấy món tủ của anh với nha ^^',
            'Hihi, rất thích những chàng trai biết nấu ăn, nên em đoán anh cũng thế ^^ Anh nấu được tất cả bao nhiêu món vậy ạ ^^',
            'Em đoán anh cũng biết nấu ăn đấy! Hihi, không biết có đúng không ạ, chỉ là e đoán vậy thôi ^^'
          ]
        },
        no: {
          male: [
            'Chào cô gái. Công việc của em có vẻ bận, chắc không có nhiều thời gian vào bếp đâu nhỉ?',
            'Anh đoán em cũng thích đi ăn ngoài hơn là tự nấu nhở ^^ Mong sau này có cơ hội được ra ngoài ăn cùng em ^^',
            'Rât vui được làm quen với em. Anh thấy nấu ăn cũng khó lắm, anh đoán em cũng thấy thế, nhỉ? ^^'
          ],
          female: [
            'Hihi, chắc con trai tụi anh thường không thích nấu ăn mấy nhỉ ^^ Thế bình thường anh có thể nấu những món nào vậy ạ?',
            'Chào anh ^^ Em đoán chắc anh cùng team "thích ăn ngoài hơn tự nấu" giống em rồi! Hihi, có đúng không?',
            'Em cũng thấy nấu ăn ngon khó ghê à ^^ Nên chắc anh cũng không biết nấu nhỉ, haha'
          ]
        }
      }
    },
    {
      question: 'Bạn thấy XY có vẻ thích thể thao không?',
      type: 'Sở thích',
      topic: 'Thể thao',
      answer: {
        yes: {
          male: [
            'Chào em. Một cô gái mạnh mẽ như em chắc cũng thích thể thao nhỉ?',
            'Chào cô gái. Anh nghĩ là em cũng thích thể thao phải không ta?',
            'Chào em. Thể thao chắc cũng là một trong những sở trường của em nhỉ?'
          ],
          female: [
            'Chào cậu! Con trai hẳn phải thích thể thao rồi nhỉ ^^ Cậu thích bóng đá, bơi hay là môn gì?',
            'Cậu hẳn là thích bóng đá, hay bóng rổ, hay môn thể thao nào rồi nhỉ ^^ Hừm, tóm lại là cậu có thích thể thao không?',
            'Hừm, thật là vô lý nếu anh không thích thể thao. Nói với em là anh cũng thích thể thao đi, hihi'
          ]
        },
        no: {
          male: [
            'Chào em. Một cô gái dịu dàng như em, chắc không thích mấy hoạt động thể thao đâu nhỉ?',
            'Chào cô gái. Thể thao chắc không phải là sở trường của em rồi nhỉ, hay là anh đoán sai ta?',
            'Chào cô gái. Hì hì, nhìn mỏng manh yếu đuối thế này chắc thể thao không phải sở trường rồi nhỉ ^^'
          ],
          female: [
            'E hèm, đoán cậu không thích thể thao cho lắm ^^ Không biết đúng không ^^',
            'Hiuhiu, tớ không thích thể thao cho lắm, nên đoán cậu cũng vậy ^^ nếu không phải thì nói tớ biết cậu thích môn gì đi nào ^^',
            'Hừm, mặc dù con trai hay thích thể thao nhưng em đoán anh thì không, hahaa'
          ]
        }
      }
    },
    {
      question: 'Bóng đá liệu có phải là sở thích của XY không nhỉ?',
      type: 'Sở thích',
      topic: 'Bóng đá',
      answer: {
        yes: {
          male: [
            'Chào cô gái. Em có là fan của đội bóng nào không?',
            'Chào em. Chắc em cũng thích bóng đá nhỉ?',
            'Chào cô gái. Em có thích cầu thủ bóng đá nào không?'
          ],
          female: [
            'Anh kiểu gì cũng là fan của bóng đá rồi! Em đoán không sai chứ nhỉ, hehe?',
            'Chào anh, chắc hẳn là anh cũng hay xem bóng đá nhỉ ^^ Anh thích câu lạc bộ nào nhất đấy, hihi?',
            'Rất vui được làm quen với anh ^^ Hihi, anh có thích thể thao, bóng đá không ạ? Chắc là có nhỉ ^^'
          ]
        },
        no: {
          male: [
            'Chào cô gái. Anh nghĩ em không thích bóng đá, không biết đúng không ta?',
            'Chào em. Cô gái dịu dàng như em chắc không thích bóng đá đâu nhỉ, sở trường của em là gì vậy?',
            'Ầy, chắc là con gái tụi em không thích bóng bánh gì đâu nhỉ ^^ Thế em có thích môn thể thao nào không em?'
          ],
          female: [
            'Haha, em đoán anh thích bóng bay, bóng nước hơn là bóng đá ^^ Thế anh thích bóng gì nhất vậy^^',
            'Hừm, e không biết mấy đội bóng đá cho lắm nên em đoán anh cũng không biết nốt nhở, haha',
            'Hì, con trai có ai không thích bóng đá không anh nhỉ? Em đoán là có anh, hihi ^^'
          ]
        }
      }
    },
    {
      question: 'Bạn có nghĩ XY là người yêu chó mèo không?',
      type: 'Sở thích',
      topic: 'Động vật (chó, mèo)',
      answer: {
        yes: {
          male: [
            'Chào em. Chắc cũng là cô gái yêu động vật đây nhỉ?   ',
            'Chào cô gái. Anh nghĩ chắc em rất thích chó mèo phải không ta?',
            'Chào em. Em ơi, em có thích chó mèo không?'
          ],
          female: [
            'Chắc là anh cũng có nuôi chó nhỉ ^^ Gấu thì có thể không có, nhưng chó thì phải có một con mà, hihi',
            'Hihi, có phải anh cũng thích động vật giống như chó, mèo... không ^^ E đoán vậy ^^',
            'Em thấy những người yêu động vật thường rất tình cảm ^^ Chắc là anh cũng thích động vật chứ ạ?'
          ]
        },
        no: {
          male: [
            'Chào cô gái. Làm việc bận rộn thế chắc em không có thời gian nuôi chó mèo đâu nhỉ? ',
            'Chào em. Chắc em không thích chó mèo nhỉ? Anh đoán có sai không huhu',
            'Anh đang nghĩ em cũng không thích động vật cho lắm, hihi, có phải vậy không em?'
          ],
          female: [
            'Không biết anh có thích chó mèo gì không ạ? Em đoán là không, hihi ^^',
            'Chắc là anh thường không thích mấy con thú cưng mấy nhỉ? Em thấy con trai thường thế ^^',
            'Em thấy con trai không thích chó mèo, không giống con gái tụi em nhỉ ^^ Anh thì sao ạ?   '
          ]
        }
      }
    },
    {
      question: 'Chụp ảnh liệu có phải là sở trường của XY không nhỉ?',
      type: 'Sở thích',
      topic: 'Chụp ảnh',
      answer: {
        yes: {
          male: [
            'Chào em. Một cô gái có nhiều ảnh đẹp như em chắc cũng thích chụp ảnh lắm nhỉ?',
            'Chào cô gái. Anh rất thích những bức ảnh của em đó. Em có thích chụp ảnh không?',
            'Chào cô gái. Những bức ảnh của em thật dễ thương. Hẳn em cũng thích chụp ảnh nhỉ?'
          ],
          female: [
            'Chào anh. Anh có thích chụp ảnh không? Em, bạn em đều thích chụp ảnh cả, nên nếu là bạn em thì anh cũng thích rồi, haha',
            'Hm, thấy anh cũng có nhiều ảnh ghê ha, chắc là cũng mê chụp ảnh rồi, hihi',
            'Hihi, em thấy con gái tụi em hay thích chụp ảnh lắm. Nhưng em đoán anh cũng thế à, haha'
          ]
        },
        no: {
          male: [
            'Anh không thấy em chia sẻ nhiều ảnh trên đây nhỉ? Hay tại em chưa có hứng chụp ảnh mấy à, phải hem ^^',
            'Hello em, làm sao để anh có cơ hội ngắm nhiều bức ảnh xinh đẹp của em hơn nhỉ ^^ Hình như em không hay chụp ảnh mấy ha?',
            'Thật là tò mò về em đấy! Em có vẻ hơi e ngại và không hay chụp ảnh nhiều, phải hem nhỉ ^^'
          ],
          female: [
            'Hm, không thấy anh chia sẻ nhiều ảnh trên nè lắm. Chắc là anh không thích chụp ảnh roài nhỉ ^^',
            'Đoán là anh cũng không thích chụp ảnh lắm ạ, không biết có đúng không ạ?',
            'Chắc là cậu không thích chụp ảnh lắm nhỉ? Nếu không phải thì up thêm nhiều ảnh lên đây cho tớ xem với nào, hihi'
          ]
        }
      }
    },
    {
      question: 'Theo bạn XY có thích ăn các món ăn Việt hơn món ăn Âu không? ',
      type: 'Sở thích',
      topic: 'Ăn uống',
      answer: {
        yes: {
          male: [
            'Chào cô gái, anh nghĩ là em thích các món ăn Việt hơn là mấy món ăn Âu nhỉ? Em thường thích ăn món gì vậy?',
          ],
          female: [
            'Hihi, chắc là anh thích ăn "cơm mẹ nấu" hơn là mấy món Âu nhỉ ^^ Không biết có đúng không ta?'
          ]
        },
        no: {
          male: [
            'Anh đoán em cũng thích các món ăn Âu lạ lạ nhỉ? Em có thể chia sẻ sở thích ăn uống của em là gì không ^^ '
          ],
          female: [
            'Cảm ơn anh đã quan tâm ^^ Chắc là anh cũng thích ăn pizza hay mấy món ăn Âu chứ ạ?   '
          ]
        }
      }
    },
    {
      question: 'Liệu XY có thích ăn vặt không nhỉ?',
      type: 'Sở thích',
      topic: 'Ăn uống',
      answer: {
        yes: {
          male: [
            'Con gái chắc là cũng thích ăn vặt đây ^^ Hehe, em có thích uống trà sữa hay mấy loại chè gì không?',
          ],
          female: [
            'Em thấy con trai cũng nhiều người thích ăn vặt lắm à! Anh cũng thế đúng không, hihi ^^ '
          ]
        },
        no: {
          male: [
            'Thật vui nếu mình có thể có chung sở thích ăn uống ^^ Em có thích ăn vặt gì đó không?'
          ],
          female: [
            'Hừm, con trai chắc là không hay ăn vặt lắm đây! hihi, thế không biết anh thích ăn những món gì nhỉ?    '
          ]
        }
      }
    },
    {
      question: 'Bạn có nghĩ XY cũng thường nghe các loại nhạc thị trường phổ biến hiện nay không?',
      type: 'Sở thích',
      topic: 'Hát...',
      answer: {
        yes: {
          male: [
            'Ầy, chắc là em cũng dễ nghe nhạc, không kén chọn thể loại nhỉ? Không biết anh đoán có đúng không em?'
          ],
          female: [
            'Anh ơi anh có thường nghe nhạc không? Hihi, chắc là anh cũng dễ nghe nhạc, không kén chọn cho lắm nhỉ?'
          ]
        },
        no: {
          male: [
            'Anh đoán em thích thưởng thức các loại nhạc "có chất" hơn là nhạc thị trường nhỉ? Không biết em thích những bài hát nào đó ^^'
          ],
          female: [
            'Ấy, đoán anh cũng thích các thể loại nhạc "có chất" đúng không ạ? Anh thường nghe những bài gì vậy ạ ^^ '
          ]
        }
      }
    },
    {
      question: 'Theo bạn XY có thích nghe nhạc Trịnh hơn là nhạc của Sơn Tùng không nhỉ?',
      type: 'Sở thích',
      topic: 'Hát...',
      answer: {
        yes: {
          male: [
            "Hehe, anh đoán em không phải là Sky's rồi! Nhưng không biết em có thích nhạc Trịnh không nhỉ?",
          ],
          female: [
            'Em đoán anh không phải fan của Sơn Tùng nhỉ! Không biết anh thường nghe những thể loại nhạc gì vậy?'
          ]
        },
        no: {
          male: [
            'Hehe, có vẻ cũng là fan của Sơn Tùng đây. Em thích bài nào của Sơn Tùng nhất đấy, hihi?'
          ],
          female: [
            "Haha, anh đích thị là Sky's rồi! Anh có thuộc hết mấy bài của Sơn Tùng không ^^"
          ]
        }
      }
    },
    {
      question: 'Bạn có nghĩ XY là người tự tin không?',
      type: 'Tính cách',
      topic: 'Tự tin',
      answer: {
        yes: {
          male: [
            'Chào em. Anh rất thích con gái có vẻ tự tin giống như em. Mình làm quen nhé!',
          ],
          female: [
            'Em rất là thích con trai tự tin đấy! Hihi, chắc là anh cũng tự tin ha?'
          ]
        },
        no: {
          male: [
            'Chào em, nhìn nụ cười em e thẹn quá, chắc ngoài đời em cũng ít nói nhỉ?'
          ],
          female: [
            'Hihi, có phải anh là chàng trai hơi nhút nhát và hướng nội không? ^^'
          ]
        }
      }
    },
    {
      question: 'Liệu XY có thuộc tuýp người năng động?',
      type: 'Tính cách',
      topic: 'Năng động',
      answer: {
        yes: {
          male: [
            'Chắc là cô gái năng động, hòa đồng đây ^^ Cho anh làm quen với nhé!'
          ],
          female: [
            'Chào anh năng động và dễ mến ạ ^^ Không biết anh có năng động thật không nhỉ, hihi'
          ]
        },
        no: {
          male: [
            'Thấy em mang nhiều vẻ đẹp của con gái truyền thống ghê ^^ Không biết em có tham gia nhiều hoạt động xã hội, từ thiện bên ngoài không nhỉ?'
          ],
          female: [
            'Chào anh ạ! Hihi, không biết anh có năng động, tham gia nhiều hoạt động vui chơi không?'
          ]
        }
      }
    },
    {
      question: 'Bạn có nghĩ XY là người mạnh mẽ?',
      type: 'Tính cách',
      topic: 'Mạnh mẽ',
      answer: {
        yes: {
          male: [
            'Chào em. Anh đoán em là một cô gái mạnh mẽ, biết vượt qua khó khăn. Đúng không nhỉ ^^    '
          ],
          female: [
            'Con trai là phải đàn ông và mạnh mẽ anh nhở? Nên em đoán anh cũng vậy đó, hihi'
          ]
        },
        no: {
          male: [
            'Bị thu hút bởi sự duyên dáng của em rồi! Không biết con gái nữ tính như nè có mạnh mẽ hay thích được chở che hơn nhỉ ^^'
          ],
          female: [
            'Haha, anh có sợ gián, chuột hay là con gì không? Không biết anh có phải người mạnh mẽ không nhỉ?'
          ]
        }
      }
    },
    {
      question: 'Bạn có thấy XY là người cá tính không?',
      type: 'Tính cách',
      topic: 'Cá tính',
      answer: {
        yes: {
          male: [
            'Anh rất ấn tượng với em ^^ Em có phải cô gái hiện đại, cá tính không? '
          ],
          female: [
            'Chắc anh cũng là chàng trai cá tính đây ^^ Phải không ạ?'
          ]
        },
        no: {
          male: [
            'Thật vui vì tình cờ gặp em trên đây ^^ Thấy em dịu dàng mà cũng có điều gì đó thật bí ẩn à! Phải không ^^'
          ],
          female: [
            'Anh là tuýp người cá tính hay là một chàng trai bình thường như mọi người nhỉ? Kể cho em biết đôi điều thêm về anh nhé ạ ^^'
          ]
        }
      }
    },
    {
      question: 'Tự tin, phóng khoáng liệu có phải là tính cách nổi bật của XY?',
      type: 'Tính cách',
      topic: 'Tự do, phóng khoáng',
      answer: {
        yes: {
          male: [
            'Chào em, cô gái thích tự do, tự tại như cơn gió? Không biết anh nói vậy có sai không nhỉ?'
          ],
          female: [
            'Hihi, có phải anh là chàng trai tự do và phóng khoáng không ạ? '
          ]
        },
        no: {
          male: [
            'Chào cô gái, anh thấy có vẻ em cũng khá cẩn thận và chu đáo, có đúng không em?'
          ],
          female: [
            'Hihi,anh có thể chia sẻ thêm nhiều điều với em được không? Thấy anh khá kín đáo ý ^^'
          ]
        }
      }
    },
    {
      question: 'Bạn có thấy XY là người hoà nhã không?',
      type: 'Tính cách',
      topic: 'Hoà nhã',
      answer: {
        yes: {
          male: [
            'Chào cô gái. Anh thấy em rất dịu dàng hoà nhã, cho anh làm quen với em nhé!'
          ],
          female: [
            'Hihi, chắc anh là người hòa nhã rồi. Nếu phải thì nói chuyện và chia sẻ tiếp với em về anh xem nào ^^'
          ]
        },
        no: {
          male: [
            'Chào cô gái, có vẻ khó gần và kiêu kỳ à nha! Nếu không phải thì đáp lại anh câu chào này nhé ^^'
          ],
          female: [
            'Hừm, anh cũng khó gần ghê! Hihi, nếu không phải thì hãy nói chuyện nhiều với em khi rảnh vào nhé ^^'
          ]
        }
      }
    }
  ],
  errorMsg: '',
};

export default function chatList(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
