import {
  FETCH_CHATLIST_REQUEST, FETCH_CHATLIST_SUCCESS, FETCH_CHATLIST_FAILURE,
  UPDATE_CHAT_LIST_KEY_WORD_FILTER, UPDATE_CHAT_LIST_LAST_ACTIVITY_TIME,
  INCREASE_CHAT_LIST_NUMBER_MESSAGE,
} from '../constants';
import getUserIdFromIdentifier from '../helpers/getUserIdFromIdentifier';
import { UNLOCK_CHAT_ROOM_IN_STATE, IGNORE_REFUND_IN_STATE } from '../constants/Payment';
import moment from 'moment';

const initialState = {
  isFetching: false,
  data: [],
  errorMsg: '',
  keyWordFilter: '',
  currentDate: new Date()
};

export default function chatList(state = initialState, action) {
  switch (action.type) {
    case FETCH_CHATLIST_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_CHATLIST_SUCCESS:
      const current_chat_lists = state.data;
      const new_chat_list = action.res.data.map(item => {
        const index = current_chat_lists.findIndex(oldItem =>
          getUserIdFromIdentifier(oldItem.identifier) == getUserIdFromIdentifier(item.identifier) &&
          typeof oldItem.last_activity_date !== 'undefined'
        )
        if (index > -1) {
          item.last_activity_date = current_chat_lists[index].last_activity_date
        }
        return item;
      });

      return Object.assign({}, state, {
        isFetching: false,
        data: new_chat_list,
      });

    case FETCH_CHATLIST_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        errorMsg: action.errorMsg,
      });

    case UPDATE_CHAT_LIST_KEY_WORD_FILTER:
      return Object.assign({}, state, {
        keyWordFilter: action.keyword,
      });
     case UPDATE_CHAT_LIST_LAST_ACTIVITY_TIME:
      const current_chat_list = state.data;
      const friend_by_identifier = current_chat_list.find(item => getUserIdFromIdentifier(item.identifier) == getUserIdFromIdentifier(action.identifier))
      if (friend_by_identifier) {
        friend_by_identifier.last_activity_date = new Date().toISOString()
      }
      return Object.assign({}, state, {
        data: current_chat_list,
        currentDate: new Date()
      });
    case INCREASE_CHAT_LIST_NUMBER_MESSAGE:
      const current_chat_listsx = state.data;
      const chatRoomUpdate = current_chat_listsx.find(item => item.chat_room_id == action.chat_room_id);
      if (typeof chatRoomUpdate !== 'undefined') {
        chatRoomUpdate.number_new_message += 1;
      }
      return Object.assign({}, state, {
        isFetching: false,
        data: current_chat_listsx,
      });
    case UNLOCK_CHAT_ROOM_IN_STATE:
      const current_chat_listsz = state.data;
      const chatRoomUpdatez = current_chat_listsz.find(item => item.chat_room_id == action.chat_room_id);
      if (typeof chatRoomUpdatez !== 'undefined') {
        chatRoomUpdatez.have_permission = true;
      }
      return Object.assign({}, state, {
        isFetching: false,
        data: current_chat_listsz,
        currentDate: new Date()
      });
    case IGNORE_REFUND_IN_STATE:
      const chatRoomUpdate1 = state.data.find(item => item.chat_room_id == action.chat_room_id);
      if (typeof chatRoomUpdate1 !== 'undefined') {
        chatRoomUpdatez.ignore_refund = true;
      }
      return Object.assign({}, state, {
        isFetching: false,
        data: chatRoomUpdate1,
        currentDate: new Date()
      });
    default:
      return state;
  }
}
