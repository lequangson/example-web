import React from 'react';
import { connect } from 'react-redux';
import User from './User';
import moment from 'moment';
/*
enum match_type: {
  match_type_normal: 0,
  match_type_free_chat: 1,
  match_type_unlock_chat: 2,
  match_type_super_match: 3,
  match_type_random_match: 4
}*/
const ListUsers = props => {
  const { chatList, params, data, isEdit, selectedItem, selected_friend_ids } = props;
  let type = 'default';
  if (typeof props.params.type !== 'undefined') {
    type = props.params.type;
  }

  return <ul className="ymc-list-underline">
    {data.map((user, index) => (
      <User isEdit={isEdit} {...user} key={index} type={type} selectedItem={selectedItem} selected_friend_ids={selected_friend_ids} />
    ))}
  </ul>
};
export default ListUsers;
