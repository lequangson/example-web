import React, { Component, PropTypes } from 'react';
import $ from 'jquery';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import getUserIdFromIdentifier from '../../helpers/getUserIdFromIdentifier';
import PaymentModal from '../PaymentModal'
import * as ymmStorage from '../../helpers/ymmStorage';
import moment from 'moment';
import { CDN_URL } from '../../constants/Enviroment'
import {
  MATCH_TYPE_NORMAL, MATCH_TYPE_FREE, MATCH_TYPE_UNLOCK,
  MATCH_TYPE_SUPER, MATCH_TYPE_RANDOM, MATCH_TYPE_CHAT_TRIAL, MATCH_TYPE_EXPLORER_QUESTION
} from '../../constants';
import * as userNotification from '../../helpers/notifications/user';
import * as userDevice from '../../helpers/UserDevice';

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      selected: false
    }
    this.handleClick = this.handleClick.bind(this)
    this.hideModal = this.hideModal.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selected_friend_ids.length != nextProps.selected_friend_ids.length) {
      const user_id = getUserIdFromIdentifier(this.props.identifier);
      if (nextProps.selected_friend_ids.some(item => item == user_id)) {
        $(`.checkbox-${user_id}`).prop('checked', true);
      } else {
        $(`.checkbox-${user_id}`).prop('checked', false);
      }

    }
  }

   handleClick(identifier, have_permission, begin_match_at, end_match_at, user_status) {
    if (user_status != 1) {
      userNotification.lockedAccount();
      return
    };
    const { type } = this.props;
    ymmStorage.setItem('type', type);
    browserHistory.push(`/chat/${identifier}`);
  }

  hideModal() {
    this.setState({ isModalOpen: false })
  }

  renderLockIcon() {
    const { activeLock } = this.props
    return (
      <div className="l-flex-center">
        <img src={`${CDN_URL}/chat_ui/Lock1.png`} className="img-23"  alt="lock user"/>
      </div>
    )
  }

  onSelect = (user_id) => {
    this.setState({selected: !this.state.selected});
    this.props.selectedItem(user_id);
  }

  render() {
    const {
      identifier,
      small_avatar_url,
      thumb_avatar_url,
      large_avatar_url,
      name,
      gender,
      address,
      age,
      number_new_message,
      user_status,
      have_permission,
      last_activity_date,
      begin_match_at,
      end_match_at,
      end_unlock_at,
      type,
      for_free,
      match_type,
      friend_match_type,
      is_super_match,
      last_message,
      selected_friend_ids
    } = this.props;
    let isOnline = false;
    let haveChatPermission = false;
    // the following for random match
    if (end_match_at && moment(new Date()).isBefore(moment.utc(end_match_at).local().format())) {
      haveChatPermission = true;
    }
    if (
      end_unlock_at ||
      have_permission ||
      for_free ||
      getUserIdFromIdentifier(identifier) == process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID ||
      [MATCH_TYPE_FREE, MATCH_TYPE_UNLOCK, MATCH_TYPE_SUPER, MATCH_TYPE_CHAT_TRIAL, MATCH_TYPE_EXPLORER_QUESTION].indexOf(match_type) > -1
    ) {
      haveChatPermission = true;
    }
    if (typeof last_activity_date !== 'undefined') {
      const last_activity_time = new Date(last_activity_date).getTime()
      if (new Date().getTime() - last_activity_time <= 15000) {
        isOnline = true;
      }
    }
    return (
      <li className="list-message-item col-xs-12 pos-relative"
          style={{marginLeft: `${this.props.isEdit ? '-30px' : '0px' }`}}>
        <div className="ymc-media" onClick={() => {
          this.handleClick(
            identifier,
            getUserIdFromIdentifier(identifier) == process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID ? true : have_permission,
            begin_match_at,
            end_match_at,
            user_status
          )
        }}>
          <div className="ymc-media__item ymc-media__item--1 card">
            <img
              className="ymc-bordered ymc-radius"
              src={
                user_status == 1 ?
                (small_avatar_url ? small_avatar_url :
                  (
                    gender == 1 ?
                    `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_MALE_AVATAR}` :
                    `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_FEMALE_AVATAR}`
                  )
                ) :
                (
                  gender == 1 ?
                  `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_MALE_AVATAR}` :
                  `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_FEMALE_AVATAR}`
                )
              }
              alt={name}
            />
            <div className={`l-flex-center card__top-right card__top-right--bg1 ${match_type != MATCH_TYPE_RANDOM ? 'is-hide' : ''}`}>
              <img src={`${CDN_URL}/24h_seal.png`} alt="24h_seal" className="img-normal" />
            </div>

          </div>
          <div className={`ymc-media__item ymc-media__item--2 ${type == 'random-match' ? 'txt-blue' : ''}`}>
            {getUserIdFromIdentifier(identifier) != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID && <div>

                <div className="txt-medium">
                  {name}
                  <span className="ymc-messages__meta padding-l5 padding-r5">
                    <i className="fa fa-circle" style={{'color': `${isOnline ? '#66ff66' : 'gray'}`}}/>
                  </span>
                  { number_new_message > 0 &&
                    <span className="txt-red">{number_new_message} tin nhắn mới</span>
                  }
                </div>
                { last_message && <span>{`"${last_message.substr(0, userDevice.getName() != 'pc' ? 40 : 85)}${last_message && last_message.length > (userDevice.getName() != 'pc' ? 40 : 85) ? '...' : ''}"`}</span>}
                { !last_message && <span>Chúc mừng kết đôi. Hãy mở lời trước nhé!</span>}
                { /*age && `${age} tuổi`} {address && `- ${address}`  */}

              </div>
            }
            {getUserIdFromIdentifier(identifier) == process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID && <div>
                <div className="txt-medium">Ymeet.me</div>
                Đội ngũ phát triển
              </div>
            }
          </div>
          <div className={`ymc-media__item ymc-media__item--3 meta ${haveChatPermission ? 'is-hide' : ''}`}>
            { this.renderLockIcon() }
          </div>
          {user_status == 1 && Boolean(number_new_message) && haveChatPermission && (
            <div className="ymc-media__item">
              <div className="ymc-noti">{number_new_message}</div>
            </div>
          )}
          &nbsp;

        </div>
        {this.props.isEdit &&
          <div className="ymc-media__item--edit">
            <label className="form__input--d padding-0">
              <input
                type="checkbox"
                name="message_ids"
                className={`checkbox checkbox-${getUserIdFromIdentifier(identifier)}`}
                onClick={() => this.onSelect(getUserIdFromIdentifier(identifier))}
              />
              <div className="fake-checkbox"></div>
            </label>
          </div>
        }
        <PaymentModal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.hideModal}
        />
      </li>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
  }
}

const UserContainer = connect(mapStateToProps, mapDispachToProps)(User);

export default UserContainer;

