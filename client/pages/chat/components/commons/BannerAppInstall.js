import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { CDN_URL } from '../../constants/Enviroment';

class BannerAppInstall extends Component {
  static propTypes = {
    isOpen: PropTypes.bool
  }

  constructor(props) {
    super(props)
    this.state = {
      isOpen: this.props.isOpen,
    }
  }
  render() {

    return (
      <div className={`${this.state.isOpen? 'is-show' : '' }
        banner banner--installion l-flex--ac`}
        >
          <div className="banner-install__close padding-rl5">
            <div className="txt-dark"
                onClick={()=> {
                  this.setState({ isOpen: false
                })}}
            >
              <i className="fa fa-times"></i>
            </div>
          </div>
          <div className="frame frame--sm frame--1">
            <a href={this.props.url} target="_blank">
              <img src={`${CDN_URL}/general/LandingPageNew/Logo_banner.png`} alt="Admin Support" />
            </a>
          </div>
          <div className="banner__content padding-l10 txt-sm">
            <a href={this.props.url} target="_blank">
              <div className="txt-bold txt-dark">Ymeet.me</div>
              <span className="banner__sub-heading">
                Media Max Japan Vietnam
              </span>
              <div>
                <i className="fa txt-warning fa-star"></i>
                <i className="fa txt-warning fa-star"></i>
                <i className="fa txt-warning fa-star"></i>
                <i className="fa txt-warning fa-star-half-o"></i>
                <i className="fa txt-warning fa-star-o"></i>
              </div>
              <span className="banner__sub-heading">Tải App miễn phí</span>
            </a>
          </div>
          <div className="banner__meta">
            <div className="banner__message">
              <a className="btn btn--1 txt-blue txt-bold"
                href={this.props.url} target="_blank">Mở</a>
            </div>
          </div>
      </div>
    );
  }
}



export default BannerAppInstall;