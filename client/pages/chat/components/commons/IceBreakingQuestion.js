import React from 'react';
import shuffle from 'shuffle-array';
import { isEmpty, random, replace } from 'lodash';

class IceBreakingQuestion extends React.Component {
	constructor() {
		super()
		this.state = {
			random_number: 0,
			sent_ice_breaking: false
		}
		this.handleYesButton = this.handleYesButton.bind(this);
		this.handleNoButton = this.handleNoButton.bind(this);
	}

	componentWillMount() {
		const { user, iceBreakingQuestions, chatRoom} = this.props;
		const end_random_range = iceBreakingQuestions.data.length -1;
		this.setState({random_number: random(0, end_random_range)})
	}

 /**
 * type: 1 defaul message
 * type: 2 user to user
 * type: 3 ice breaking
 * type: 4 admin message
 **/
	handleYesButton() {
		const { user, iceBreakingQuestions, chatRoom} = this.props;
		const random_question = iceBreakingQuestions.data[this.state.random_number];
		let answer = [];
		if (user.data.gender == 1) {
			answer = random_question.answer.yes.male;
		} else {
			answer = random_question.answer.yes.female;
		}
		const random_answer = answer[random(0, answer.length -1)] || ':thumbsup:';
		if (random_answer.length) {
			this.props.sendMessage(random_answer, 3);
		}
		const gaTrackData = {
		  extra_information: random_question.question,
		};
		const eventAction = {
		  eventCategory: 'IceBreakingQuestion',
		  eventAction: 'Click Yes',
		  eventLabel: 'Click Yes'
		}
		this.props.sendGoogleAnalytic(eventAction, gaTrackData)
		this.setState({sent_ice_breaking: true})
	}

 /**
 * type: 1 defaul message
 * type: 2 user to user
 * type: 3 ice breaking
 * type: 4 admin message
 **/
	handleNoButton() {
		const { user, iceBreakingQuestions, chatRoom} = this.props;
		const random_question = iceBreakingQuestions.data[this.state.random_number];
		let answer = [];
		if (user.data.gender == 1) {
			answer = random_question.answer.no.male;
		} else {
			answer = random_question.answer.no.female;
		}
		const random_answer = answer[random(0, answer.length -1)] || ':thumbsup:';
		if (random_answer.length) {
			this.props.sendMessage(random_answer, 3);
		}
		const gaTrackData = {
		  extra_information: random_question.question,
		};
		const eventAction = {
		  eventCategory: 'IceBreakingQuestion',
		  eventAction: 'Click No',
		  eventLabel: 'Click No'
		}
		this.props.sendGoogleAnalytic(eventAction, gaTrackData)
		this.setState({sent_ice_breaking: true})
	}

	render() {
		const { user, iceBreakingQuestions, chatRoom, messages, params, sent_or_received_msg} = this.props;
		if (this.state.sent_ice_breaking || sent_or_received_msg) {
			return <div></div>
		}

		let messages_data = messages.data[params.id];
    if (typeof messages_data === 'undefined') {
      messages_data = [];
    }
    let is_sent_ice_breaking = false;
    let inactive_chat_room = false;
    // detect if user already sent ice breaking message
  	is_sent_ice_breaking = messages_data.some((sendMsg, i) => sendMsg.user_id == user.data._id && sendMsg.type == 3);
  	inactive_chat_room = !messages_data.some((sendMsg, i) => sendMsg.user_id == user.data._id);
  	// inactive_chat_room = !messages_data.some((sendMsg, i) => sendMsg.user_id == user.data._id && sendMsg.type != 1);
  	if (!inactive_chat_room) {
  		// inactive_chat_room = !messages_data.some((sendMsg, i) => sendMsg.user_id != user.data._id && sendMsg.type != 1);
  		inactive_chat_room = !messages_data.some((sendMsg, i) => sendMsg.user_id != user.data._id);
  	}

    if (is_sent_ice_breaking || !inactive_chat_room) {
    	return <div></div>
    }
		let gender = 'male';
		if (!isEmpty(user.data)) {
			if (user.data.gender == 2) {
				gender = 'female';
			}
		}
		const random_question = iceBreakingQuestions.data[this.state.random_number];
		const ice_breaking_question = replace(random_question.question, 'XY', chatRoom.name);
		return (
			<div className="container">
				<div className="well well--border-dashed txt-center  ">
					<div className="well__header mbsm"> Câu hỏi nhanh</div>
					<p className="txt-light"> {ice_breaking_question}</p>
					<div className="row l-flex-ce">
						<div className="col-xs-5 col-md-3">
							<button className="btn btn--5 btn--b" onClick={this.handleNoButton}>Không</button>
						</div>
						<div className="col-xs-5 col-md-3">
							<button className="btn btn--blue btn--b" onClick={this.handleYesButton}>Có</button>
						</div>
					</div>
				</div>

			</div>
		)
	}
}

export default IceBreakingQuestion