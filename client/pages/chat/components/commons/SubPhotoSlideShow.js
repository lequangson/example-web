import React, { PropTypes } from 'react'
import { connect } from 'react-redux';
import $ from 'jquery'
import Modal from 'react-modal'
import slick from 'public/scripts/slick.min.js'
import { getDetailUserProfile } from 'chat/actions/user';
import getUserIdFromIdentifier from 'chat/helpers/getUserIdFromIdentifier';
import { isEmpty } from 'lodash';
var Preload = require('react-preload').Preload;

class SubPhotoSlideShow extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      openPreview: false,
      previewIndex: 0
    }
    this.showPrevUser = this.showPrevUser.bind(this);
    this.showNextUser = this.showNextUser.bind(this);
    this.showPreview = this.showPreview.bind(this);
    this.closePreview = this.closePreview.bind(this);
    this._unCarousel = this._unCarousel.bind(this);
  }

  componentWillMount() {
    const { user, params } = this.props;
    const { selected_user } = user;
    const user_to = selected_user.find(item => getUserIdFromIdentifier(item.identifier) == getUserIdFromIdentifier(params.id)) || {};
    if (isEmpty(user_to)) {
      this.props.getDetailUserProfile(params.id);
    }
    // this.props.getDetailUserProfile(params.id);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.previewIndex != nextState.previewIndex || this.state.openPreview != nextState.openPreview) {
      return true;
    }
    if (this.props.user.selected_user.length && this.props.user.selected_user.length == nextProps.user.selected_user.length) {
      return false;
    }
    return true;
  }

  _carousel() {

    // fb carousel
    $('.carousel-fb').slick({
      slidesToShow: 6,
      slidesToScroll: 3,
      arrows: true,
      infinite: true,
      initialSlide: 0,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 6,
          },
        },
        {
          breakpoint: 425,
          settings: {
            slidesToShow: 4,
          },
        },
      ],
    })
  }

  _unCarousel() {
    if ($('.carousel-form .carousel-fb')) {
      $('.carousel-fb').slick('unslick')
    }
  }

  componentDidMount() {
      this._carousel()
  }


  componentWillUpdate() {
    this._unCarousel()
  }

  componentDidUpdate() {
    this._carousel()
    $('.carousel-fb').slick('slickGoTo', 0)
  }


  // shouldComponentUpdate(nextProps, nextState) {
  //   return !_.isEqual(nextProps.user, this.props.user);
  // }
  showPrevUser() {
    if (this.state.previewIndex == 0) {
      return;
    }
    this.setState({ previewIndex: this.state.previewIndex -1})
  }

  showNextUser() {
    const { user, params } = this.props;
    const { selected_user } = user;
    const user_to = selected_user.find(item => getUserIdFromIdentifier(item.identifier) == getUserIdFromIdentifier(params.id)) || {};
    if (isEmpty(user_to)) {
      return;
    }
    if (this.state.previewIndex == user_to.user_pictures.length - 1) {
      return;
    }
    this.setState({ previewIndex: this.state.previewIndex +1})
  }
  showPreview(index) {
    this.setState({previewIndex: index, openPreview: true})
  }
  closePreview() {
    this.setState({openPreview: false})
  }

  render() {
    const { user, params } = this.props;
    const { selected_user } = user;

    const user_to = selected_user.find(item => getUserIdFromIdentifier(item.identifier) == getUserIdFromIdentifier(params.id)) || {};
    const images = [];
    const loadingIndicator = (<div>Ảnh đang được tải, vui lòng đợi trong giây lát...</div>)
    return (
      <div className="panel mbm">
        <div className="clearfix">
          <div className="user-chat-sub-carousel">
            <div className="carousel-fb" >
              {
                !isEmpty(user_to) &&
                user_to.user_pictures.length > 0 &&
                user_to.user_pictures.map((user_picture, i) => {
                  images.push(user_picture.extra_large_image_url);
                  return <div className="txt-center padding-r10" key={i} onClick={() => {this.showPreview(i)}}>
                      <div className="brick">
                        <img src={user_picture.thumb_image_url} alt={user_to.nick_name} />
                      </div>
                    </div>
                  })
              }
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.openPreview}
          onRequestClose={this.props.closePhotoStreamModal}
          className="modal__content"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="row">
            <div className="col">
              <div className="modal__frame">
                {
                  !isEmpty(user_to) &&
                   user_to.user_pictures.length > 0 && <Preload
                    loadingIndicator={loadingIndicator}
                    images={images}
                    autoResolveDelay={3000}
                    onError={this._handleImageLoadError}
                    onSuccess={this._handleImageLoadSuccess}
                    resolveOnError={true}
                    mountChildren={true}
                    >
                    {/* content to be rendered once loading is complete*/}
                    <img className="full-width" src={user_to.user_pictures[this.state.previewIndex].extra_large_image_url} />
                  </Preload>
                }
                {!isEmpty(user_to) && this.state.previewIndex > 0 && <button
                  className="pagi pagi__prev pagi--2"
                  onClick={this.showPrevUser}
                >
                  <i className="fa fa-chevron-left"></i>
                </button>}
                {!isEmpty(user_to) && this.state.previewIndex < user_to.user_pictures.length -1 && <button
                  className="pagi pagi__next pagi--2"
                  onClick={this.showNextUser}
                >
                  <i className="fa fa-chevron-right"></i>
                </button>}
              </div>
            </div>
            <button
              className="modal__btn-close"
              onClick={this.closePreview}
            >
              <i className="fa fa-times" />
            </button>

          </div>
        </Modal>
      </div>
    )
  }
}

SubPhotoSlideShow.propTypes = {
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    getDetailUserProfile: (identifier = '') => {
      dispatch(getDetailUserProfile(identifier))
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SubPhotoSlideShow);
