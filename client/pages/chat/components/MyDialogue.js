import React, { PropTypes } from 'react';
import DialogueContent from './dialogue/DialogueContent';
import { Divider } from './Divider';
import chatFullTime from '../helpers/chatFullTime';

const MyDialogue = props => (
  <div id={props.id}>
    <Divider text={chatFullTime(props.created_at)} />
    <div className="ymc-dialogue ymc-dialogue--2">
      <DialogueContent {...props} />
    </div>
  </div>
);

MyDialogue.propTypes = {
  time: PropTypes.string,
};

export default MyDialogue;
