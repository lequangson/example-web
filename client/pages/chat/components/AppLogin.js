import React, { Component, PropTypes } from 'react';
import ButtonFacebookLoader from './buttons/ButtonFacebookLoader';
import NotiDanger from './notifications/NotiDanger';
import NotiWarning from './notifications/NotiWarning';
import FacebookLoginButton from './buttons/FacebookLoginButton';
import GoogleLoginButton from './buttons/GoogleLoginButton';
import { AUTH_YMM_TOKEN_NAME, CDN_URL } from '../constants/index';
import * as ymmStorage from '../helpers/ymmStorage';

const responseFacebook = (response) => {
};

class AppLogin extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    if (ymmStorage.getItem(AUTH_YMM_TOKEN_NAME)) {
      window.location.href = '/chat-list';
      return;
    }
  }

  componentWillUnmount() {
    const h = document.getElementsByTagName('html')[0];
    h.classList.remove('landingpage');
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      const html = document.getElementsByTagName('html')[0];
      html.classList.add('no-touch');
    }
  }

  errorType(code, type) {
    return code === type;
  }

  listError(errors) {
    return (
      <ul className="ymc-list-unstyled">
        {errors.map((err, index) => (
          <li key={index}>{err}</li>
        ))}
      </ul>
    );
  }

  render() {
    const props = this.props;

    return (
      <div className="ymc-hero">
        <div className="ymc-hero__image txt-center">
          <img className="padding-t10 padding-b20 img-220" src={`${CDN_URL}/general/Chat/Logo_Chat.png`} alt="logo chat"/>
          <h1 className="ymc-hero__heading txt-thin txt-xxlg txt-uppercase">Hẹn hò an toàn<br />Chưa bao giờ<br />dễ dàng đến thế !</h1>
        </div>
        <div className="ymc-hero__content">

          {props.fbLogin.isFetching ? (
            <ButtonFacebookLoader />
          ) : (
            <div>
              { /*<ButtonFacebookLogin
                  onClick={this.handleClick}
                />
              */ }
              <FacebookLoginButton
                appId={process.env.REACT_APP_FACEBOOK_APP_ID}
                autoLoad={false}
                callback={responseFacebook}
                icon="fa-facebook"
                {...this.props}
              />
            </div>
          )}

          <GoogleLoginButton
            appId={process.env.REACT_APP_FACEBOOK_APP_ID}
            autoLoad={false}
            callback={responseFacebook}
            icon="fa-facebook"
            {...this.props}
          />

          {this.errorType(props.fbLogin.status_code, 2) && (
            <NotiWarning title="Cảnh báo">
              {this.listError(props.fbLogin.errorMsg)}
            </NotiWarning>
          )}

          {this.errorType(props.fbLogin.status_code, 3) && (
            <NotiDanger title="Lỗi xác thực">
              {this.listError(props.fbLogin.errorMsg)}
            </NotiDanger>
          )}

        </div>
        <div className="ymc-hero__footer txt-center">
          <ul className="ymc-list-footer">
            <li><a href="https://m.ymeet.me/terms-of-use" target="blank" className="txt-lg txt-white">Điều khoản sử dụng</a></li>
            <li><a href="https://m.ymeet.me/privacy" target="blank" className="txt-lg txt-white">Bảo mật</a></li>
            <li><a href="https://m.ymeet.me/contact" target="blank" className="txt-lg txt-white">Liên hệ</a></li>
            <li><a href="https://m.ymeet.me/helps" target="blank" className="txt-lg txt-white">Trợ giúp</a></li>
            <li><a href="https://ymeet.me/blog/" target="blank" className="txt-lg txt-white">Blogs</a></li>
          </ul>
          <div className="txt-md txt-right-mc txt-white">Copyright (c) 2017 <a href="http://mmj.vn" target="_blank" rel="noopener noreferrer" className="txt-bold txt-white">Media Max Japan</a> All Rights Reserved.</div>
        </div>
      </div>
    );
  }
}

AppLogin.propTypes = {
};

export default AppLogin;
