import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import $ from 'jquery'
import Modal from 'react-modal'
import { CDN_URL } from '../../constants/Enviroment'
import { subscribeUser } from '../../helpers/userSubscription';
import { updatePushNotiModalStatus } from '../../actions/user'

class ConfirmModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modalOpen: true
    }
    this.handleClick = this.handleClick.bind(this)
    this.closeModal = this.closeModal.bind(this);
    this.sendGA = this.sendGA.bind(this);
  }

  handleClick() {
    this.setState({ modalOpen: false })
    this.sendGA()
    return this.showRewardNoti();
  }

  closeModal() {
    if (this.props.currentModal == 'pushNoti') {
      this.props.updatePushNotiModalStatus(false)
    }
    this.setState({ modalOpen: false })
  }

  showPushNotiRequest(props) {
    subscribeUser(props)
    this.props.updatePushNotiModalStatus(false)
  }

  sendGA(type) {
    const source = this.props.currentModal
    let modal = 'PUSH_NOTI_CHAT_MODAL'
    let eventCategory = 'Push Notification'
    let eventLabel = type === 1 ? 'Click Yes' : 'Click No'
    if (source === 'enoughMoney') {
      modal = 'ENOUGH_MONEY_MODAL'
      eventLabel = 'Click Open Chat '
      eventCategory = 'Refund'
    }
    else if (source === 'lackMoney') {
      modal = 'LACK_MONEY_MODAL'
      eventLabel = 'Click Add Coins'
      eventCategory = 'Refund'
    }
    else if (source === 'refund') {
      modal = 'REFUND_MODAL'
      eventLabel = 'Click Refund Coins'
      eventCategory = 'Refund'
    }
    const gaTrackData = {
      page_source: modal,
    };
    const eventAction = {
      eventCategory: eventCategory,
      eventAction: 'Click',
      eventLabel: eventLabel
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
  }

  showRewardNoti() {
    const RewardNoti = $(`
      <div class="noti noti--bg-blue mbm">
        <div class="noti__body">
          <div class="noti__header mbm">
            <img src="${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png" alt="coin_reward_icon+coin.png" />
            <span> +100 </span>
          </div>
          <div class="noti__content">Bạn vừa nhận 100 xu!</div>
        </div>
      </div>
    `)

    setTimeout(function() {
      $('body').append(RewardNoti)
    }, 200)

    setTimeout(function() {
      $(RewardNoti).remove()
    }, 405000)
  }

  renderTextBody() {
    switch(this.props.currentModal) {
      case "enoughMoney" :
        return (
          <p className="modal__body">Số xu hiện có: <span className="txt-blue">3 triệu xu</span></p>
        )
      case "lackMoney" :
        return (
          <p className="modal__body">
            Số xu hiện có: <span className="txt-blue">3 triệu xu</span> <br />
            Nhận lại <span className="txt-blue">100 xu</span> nếu không có hồi âm
          </p>
        )
      case "refund" :
        return (
          <p className="modal__body">Bạn đã gửi tin nhắn mà chưa thấy hồi âm? Đừng lo!</p>
        )
      case "pushNoti" :
        return (
          <div>
            <h2 className="txt-blue mbm">Nhận thông báo tin nhắn mới</h2>
            <p className="modal__body txt-thin">
              Biết ngay khi {this.props.name} trả lời bạn! <br />
              Nhấn Cho phép <br />
              khi Ymeet.me hỏi bạn nhé!
            </p>
          </div>
        )
      default:
        return '';
    }
  }

  renderTextBottom() {
    switch(this.props.currentModal) {
      case "enoughMoney" :
        return (
          <div className="modal__bottom">
            Nhận lại ngay <span className="txt-blue">100 xu</span> nếu không có hồi âm <br />
            <Link to="/" className="txt-underline">Tìm hiểu thêm</Link>
          </div>
        )
      case "lackMoney" :
        return (
          <div className="modal__bottom">
            Bạn cũng có thể mở chat bằng cách <br />
            <Link to="/" className="txt-underline">Nâng cấp tài khoản</Link>
          </div>
        )
      case "refund" :
        return (
          <div className="modal__bottom">
            <Link to="/" className="txt-underline">Tìm hiểu thêm</Link>
          </div>
        )
      default:
        return '';
    }
  }

  renderImgHeader() {
    let url = '';
    switch(this.props.currentModal) {
      case "enoughMoney" :
        url = `${CDN_URL}/chat_ui/img.png`
        return url
      case "lackMoney" :
        url = `${CDN_URL}/chat_ui/chat-trial-expired.png`
        return url
      case "refund" :
        url = `${CDN_URL}/chat_ui/popup-refund-img.png`
        return url
      case "pushNoti" :
        url = `${CDN_URL}/chat_ui/Push-noti-chat.png`
        return url
      default:
        return url;
    }
  }

  renderButton() {
    switch(this.props.currentModal) {
      case "enoughMoney" :
        return (
          <button className="btn btn--blue btn--b mbm" onClick={this.sendGA}>Mở chat với USER</button>
        )
      case "lackMoney" :
        return (
          <button className="btn btn--blue btn--b mbm" onClick={this.sendGA}>bạn còn 10 xu - Nạp thêm xu</button>
        )
      case "refund" :
        return (
          <button className="btn btn--blue btn--b mbm" onClick={this.handleClick}>Nhận 100 xu ngay!</button>
        )
      case "pushNoti" :
        return (
          <div className="mbm">
            <div className="row">
              <div className="col-xs-6">
                <button
                  className="btn btn--5 btn--b"
                  onClick={() => {
                    this.sendGA(2)
                    this.closeModal()
                  }}
                >
                  Bỏ qua
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  className="btn btn--blue btn--b "
                  onClick={() => {
                    this.sendGA(1)
                    this.showPushNotiRequest(this.props)
                  }}
                >
                  Đồng ý
                </button>
              </div>
            </div>
          </div>
        )
      default:
        return '';
    }
  }

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.isOpen}
          onRequestClose={this.closeModal}
          className="modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <img className="img-firstlike" src={this.renderImgHeader()} alt="img.png" />
          <div className="modal__txt-content">
            <div className="txt-center padding-t10" >
              {this.renderTextBody()}
            </div>
            {this.renderButton()}
            {this.renderTextBottom()}
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    )
  }
}

ConfirmModal.propTypes = {
  updatePreviousPage: PropTypes.func,
  gaSend: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    updatePushNotiModalStatus: (isOpen) => {
      dispatch(updatePushNotiModalStatus(isOpen, ''))
    }
  }
}
const ConfirmModalContainer = connect(mapStateToProps, mapDispachToProps)(ConfirmModal);

export default ConfirmModalContainer;