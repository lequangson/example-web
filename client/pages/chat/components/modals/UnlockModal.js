import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import $ from 'jquery'
import Modal from 'react-modal'
import { CDN_URL } from '../../constants/Enviroment'

class UnlockModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modalOpen: false
    }
    this.handleClick = this.handleClick.bind(this)
    this.closeModal = this.closeModal.bind(this);
  }

  handleClick() {
    this.setState({ modalOpen: false })
    return this.showRewardNoti();
  }

  closeModal() {
    this.setState({ modalOpen: false })
  }

  showRewardNoti() {
    const RewardNoti = $(`
      <div class="noti noti--bg-blue mbm">
        <div class="noti__body">
          <div class="noti__header mbm">
            <img src="${CDN_URL}/chat_ui/unlocked.png" alt="coin_reward_icon+coin.png" />
          </div>
        </div>
      </div>
    `)

    setTimeout(function() {
      $('body').append(RewardNoti)
    }, 200)

    setTimeout(function() {
      $(RewardNoti).remove()
    }, 405000)
  }

  render() {
    return (
      <div>
        <Modal
          isOpen={true}
          onRequestClose={this.closeModal}
          className="modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <img className="img-firstlike" src={`${CDN_URL}/chat_ui/img.png`} alt="img.png" />
          <div className="modal__txt-content">
            <div className="txt-center padding-t20 padding-b10" >
              <p className="banner__sub-heading">Số xu hiện có: 3 triệu xu</p>
            </div>
            <button className="btn btn--blue btn--b" onClick={this.handleClick}>Nạp thêm xu</button>
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    )
  }
}

UnlockModal.propTypes = {
  updatePreviousPage: PropTypes.func,
  gaSend: PropTypes.func,
}

export default UnlockModal;