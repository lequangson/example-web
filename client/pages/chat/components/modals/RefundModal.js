import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import $ from 'jquery'
import Modal from 'react-modal'
import { CDN_URL } from '../../constants/Enviroment'
import { ymm_refunds, chat_refunds } from '../../actions/Payment';
import getUserIdFromIdentifier from '../../helpers/getUserIdFromIdentifier';
import { fetchChatList } from '../../actions/chatList';
import q from 'q';

const socket = require('socket.io-client')(process.env.REACT_APP_MOBILE_SOCKET);

class RefundModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modalOpen: false
    }
    this.handleClick = this.handleClick.bind(this)
    this.closeModal = this.closeModal.bind(this);
  }

  sendGA() {
    const gaTrackData = {
      page_source: 'REFUND_MODAL',
    };
    const eventAction = {
      eventCategory: 'Refund',
      eventAction: 'Click',
      eventLabel: 'Click Refund Coins'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
  }

  handleClick() {
    // this.setState({ modalOpen: false })
    // return this.showRewardNoti();
    const { params, user } = this.props;
    const friend_user_id = getUserIdFromIdentifier(params.id);
    this.sendGA()
    this.props.refunds(user.data._id, friend_user_id).then(() => {
      this.props.fetchChatList();
      this.props.closeRefundModal();
      this.showRewardNoti()
      socket.emit('send_event', {type: 'reload_user'})
    });
  }

  closeModal() {
    this.props.closeRefundModal();
  }

  showRewardNoti() {
    const RewardNoti = $(`
      <div class="noti noti--bg-blue mbm">
        <div class="noti__body">
          <div class="noti__header mbm">
            <img src="${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png" alt="coin_reward_icon+coin.png" />
            <span> +100 </span>
          </div>
          <div class="noti__content">Bạn vừa nhận 100 xu!</div>
        </div>
      </div>
    `)

    setTimeout(function() {
      $('body').append(RewardNoti)
    }, 200)

    setTimeout(function() {
      $(RewardNoti).remove()
    }, 5000)
  }

  renderTextBody() {
    return <p className="modal__body txt-light txt-bold">Bạn đã gửi tin nhắn mà chưa thấy hồi âm? Đừng lo!</p>
  }

  renderTextBottom() {
    return (
      <div className="modal__bottom txt-center">
        <a href={`${process.env.REACT_APP_YMM_MOBILE_URL}coin-instruction/`} className="txt-underline">Tìm hiểu thêm</a>
      </div>
    )
  }

  renderImgHeader() {
    return `${CDN_URL}/chat_ui/popup-refund-img.png`;
  }

  renderButton() {
    return <button className="btn btn--blue btn--b mbm" onClick={this.handleClick}>Nhận 100 xu ngay!</button>
  }

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.open}
          onRequestClose={this.closeModal}
          className="modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <img className="img-firstlike" src={this.renderImgHeader()} alt="img.png" />
          <div className="modal__txt-content">
            <div className="txt-center padding-t10" >
              {this.renderTextBody()}
            </div>
            {this.renderButton()}
            {this.renderTextBottom()}
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    )
  }
}

RefundModal.propTypes = {
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    refunds: (user_id, friend_user_id) => {
      const defer = q.defer();
      dispatch(ymm_refunds(friend_user_id)).then(res => {
        dispatch(chat_refunds(user_id, friend_user_id)).then(() => {
          defer.resolve(res);
        }).catch(() => {
          defer.reject(false);
        })
      }).catch(() => {
        defer.reject(false);
      })
      return defer.promise;
    },
    fetchChatList: () => dispatch(fetchChatList()),
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RefundModal);