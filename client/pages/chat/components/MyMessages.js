import React, { PropTypes } from 'react';
import MyMessage from './messages/MyMessage';

const MyMessages = props => (
	<div className="row justify-content-end">
	  <div className="col-xs-11 col-xs-offset-1">
		  <div className="ymc-messages">
		    <div className="ymc-messages__content ymc-messages__content--2">
		      <MyMessage
		      	text={props.message}
		      	{...props}
		      />
		    </div>
		  </div>
	  </div>
	</div>
);

MyMessages.propTypes = {
  message: PropTypes.string,
};

export default MyMessages;
