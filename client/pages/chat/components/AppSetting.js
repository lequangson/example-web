import React, { Component, PropTypes } from 'react';
import { isEmpty } from 'lodash';
import findChat from '../helpers/findChat';
import lastOnlineTime from '../helpers/lastOnlineTime';
import SettingList from './setting/SettingList';
import { Seperate } from './Divider';
import findChatRoomByIdentifier from '../helpers/findChatRoomByIdentifier';

class AppSetting extends Component {
  constructor() {
    super();

    this.getRecipientInfo = this.getRecipientInfo.bind(this);
  }

  componentWillMount() {
    // fetchChatList
    const { params, chatList } = this.props;
    const { id } = params;
    if (isEmpty(findChatRoomByIdentifier(chatList, id))) {
      this.props.fetchChatList();
    }
  }

  componentDidMount() {
    // get receipient info
    //this.getRecipientInfo();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      user: findChat(nextProps.chatList.data, this.props.params.id),
    });
  }

  getRecipientInfo() {
    const { params, chatList, messages } = this.props;
    const { id } = params;
    const chatRoom = findChatRoomByIdentifier(chatList, id);
    if (!isEmpty(chatRoom)) {
      this.setState({
        user: chatRoom,
      });
    }
  }

  render() {
    const { params, chatList } = this.props;
    const { id } = params;
    const user = findChatRoomByIdentifier(chatList, id);

    return (
      <div>
        {!isEmpty(user) && (
          <div className="ymc-media">
            <div className="ymc-media__item ymc-media__item--1">
              <img className="ymc-bordered ymc-radius" src={user.thumb_avatar_url} alt={user.name} />
            </div>
            <div className="ymc-media__item">
              <div>
                {user.name}
                <br />
                {user.age && `${user.age} tuổi`} {user.residence && `- ${user.residence}`}
                <br />
                { /*Online: {lastOnlineTime(user.last_visited)} */}
              </div>
            </div>
          </div>
        )}

        <Seperate />

        <SettingList />

      </div>
    );
  }
}

AppSetting.propTypes = {
  params: PropTypes.object,
  chatList: PropTypes.object,
  fetchChatList: PropTypes.func,
};

export default AppSetting;
