import React from 'react';
import DialogueChathead from './dialogue/DialogueChathead';

const Typing = () => (
  <div className="ymc-dialogue ymc-c-f__fix-pos">
    <DialogueChathead />
    <div className="ymc-dialogue__content">
      <div className="ymc-messages">
        <div className="ymc-messages__content">
          <div className="ymc-messages__msg">
            <div className="ymc-messages__msg__content">
              <i className="dot dot-1"></i>
              <i className="dot dot-2"></i>
              <i className="dot dot-3"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Typing;
