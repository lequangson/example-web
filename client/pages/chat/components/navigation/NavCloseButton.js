import React from 'react';

const NavCloseButton = props => (
  <button className="ymc-nav__close" onClick={props.hideNavigation}>
    <i className="fa fa-times fa-lg"></i>
  </button>
)

export default NavCloseButton;
