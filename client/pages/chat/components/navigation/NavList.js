import React from 'react';

const NavList = props => (
  <ul className="ymc-list-ec">
    <li><a href="https://m.ymeet.me/about" target="_blank" >Giới thiệu</a></li>
    <li><a href="https://m.ymeet.me/terms-of-use" target="_blank" >Điều khoản</a></li>
    <li><a href="https://m.ymeet.me/privacy" target="_blank" >Bảo mật</a></li>
    <li><a href="https://m.ymeet.me/helps" target="_blank" >Trợ giúp</a></li>
    <li><a href="https://m.ymeet.me/contact" target="_blank" >Liên hệ</a></li>
    <li><a href="https://ymeet.me/blog/" target="_blank" >Blog</a></li>
  </ul>
)

export default NavList;
