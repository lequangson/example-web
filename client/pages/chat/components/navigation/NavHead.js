import React from 'react';
import _ from 'lodash';
import moment from 'moment';

const NavHead = ({user}) => (
  <a className="ymc-media" href={`${process.env.REACT_APP_YMM_MOBILE_URL}myprofile`}>
    <div className="ymc-media__item ymc-media__item--1">
      <img
        className="ymc-bordered ymc-radius"
        src={
          !_.isEmpty(user.data) ?
          (
            user.data.small_avatar_url ? user.data.small_avatar_url :
            (
                user.data.gender == 1 ?
                `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_MALE_AVATAR}` :
                `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_FEMALE_AVATAR}`
            )
          ) :
          (
              user.data.gender == 1 ?
              `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_MALE_AVATAR}` :
              `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_FEMALE_AVATAR}`
            )
        }
        alt=""
      />
    </div>
    <div className="ymc-media__item ymc-media__item--2">
      <div>
        {!_.isEmpty(user.data) ? user.data.name : ''}<br />
        {!_.isEmpty(user.data) ? (user.data.birthday ? moment().diff(user.data.birthday, 'years') : '') : ''} tuổi
        {!_.isEmpty(user.data) && user.data.address.length ? ` - ${user.data.address}` : ''}
      </div>
    </div>
  </a>
)

export default NavHead;
