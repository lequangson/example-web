import React from 'react';
import { Link } from 'react-router';

const NavOptions = props => (
  <ul className="ymc-list-underline">
    {/*<li>
      <label className="ymc-media mbzero">
        <input className="ymc-media__item mrmd" type="checkbox" />
        <div className="ymc-media__item">Nhận thông báo</div>
      </label>
    </li>*/}
    <li>
      <Link className="ymc-media" to="/logout">
        <i className="fa fa-sign-out ymc-media__item mrmd"></i>
        <div className="ymc-media__item">Đăng xuất</div>
      </Link>
    </li>
  </ul>
)

export default NavOptions;
