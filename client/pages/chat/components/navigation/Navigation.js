import React from 'react';
import NavHead from './NavHead';
import NavCloseButton from './NavCloseButton';
import NavList from './NavList';
import NavOptions from './NavOptions';

const Navigation = props => (
  <aside className={props.showNav ? 'ymc-nav is-show' : 'ymc-nav'}>
    <div className="ymc-nav__main">
      <NavHead {...props} />

      <div className="ymc-nav__hr"></div>

      <NavOptions {...props} />

      <div className="ymc-nav__hr"></div>

      <NavList {...props} />

    </div>

    <NavCloseButton {...props} />

    <div className="ymc-nav__ghost" onClick={props.hideNavigation}></div>
  </aside>
)

export default Navigation;
