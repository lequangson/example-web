import React, { Component, PropTypes } from 'react';
import { browserHistory, Link } from 'react-router';
// import Infinite from 'react-infinite';
import $ from 'jquery';
import _ from 'lodash';
import Dialogue from './Dialogue';
import MyDialogue from './MyDialogue';
import LoaderMessages from './loaders/LoaderMessages';
import scrollToBottom from 'chat/helpers/scrollToBottom';
import getUserIdFromIdentifier from 'chat/helpers/getUserIdFromIdentifier';
import Typing from './Typing';
import AppBottomBarContainer from 'chat/containers/AppBottomBarContainer';
import findChatRoomByIdentifier from 'chat/helpers/findChatRoomByIdentifier';
import { uniqChatMessages, getCurrentUserIdFromChatRoomAndIdentifier } from 'chat/helpers/common';
import { DEFAULT_PAGE_SIZE, PRE_LOAD_MORE_DATA_NUMBER, TOTAL_PER_PAGE } from 'chat/constants/messages';
import getIdentifierFromId from 'chat/helpers/getIdentifierFromId';
import { NUMBER_COIN_TO_UNLOCK } from 'chat/constants/Payment';

import Modal from 'react-modal'
import { CDN_URL } from 'chat/constants/Enviroment'
import * as ymmStorage from 'chat/helpers/ymmStorage';
import * as paymentNotification from 'chat/helpers/notifications/payment';
import * as chatNotification from 'chat/helpers/notifications/chat';
import * as chatTrial from 'chat/helpers/chatTrial';
import moment from 'moment';
import DialogueDefault from './dialogue/DialogueDefault';
import SubPhotoSlideShow from './commons/SubPhotoSlideShow';
import IceBreakingQuestion from './commons/IceBreakingQuestion';
import RefundModal from './modals/RefundModal';
// import { subscribeUser } from 'chat/helpers/userSubscription';
import ConfirmModal from './modals/ConfirmModal'
import { DEFAULT_TITLE } from 'chat/constants';
var xss = require('xss');

const socket = require('socket.io-client')(process.env.REACT_APP_SOCKET_URL);
const mobile_socket = require('socket.io-client')(process.env.REACT_APP_MOBILE_SOCKET);

class ChatFrame extends Component {
  static elementInfiniteLoad() {
    return (
      <div className="col-xs-12 txt-center loader is-load">
        <center>Đang tải thêm dữ liệu ...</center>
      </div>
    )
  }

  constructor() {
    super();
    this.renderAllDialogue = this.renderAllDialogue.bind(this);
    this.state = {
      current_user_id: null,
      isOpendPaymentModal: false,
      remaining_coins: 0,
      chatRoom: {},
      clickedUnlockChat: false,
      sent_or_received_msg: false,
      openRefundModal: false,
      selectedPackage: 'package',
    }
    this.handleSendMessage = this.handleSendMessage.bind(this);
    this.initChat = this.initChat.bind(this);
    // this.listenScrollEvent = this.listenScrollEvent.bind(this);
    this.markMessageAsRead = this.markMessageAsRead.bind(this);
    this.onClickChatFrame = this.onClickChatFrame.bind(this);
    this.showPaymentPopup = this.showPaymentPopup.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleUnlockChat = this.handleUnlockChat.bind(this);
    this.addCoin = this.addCoin.bind(this);
    this.onclickVerify = this.onclickVerify.bind(this);
    this.openRefundModal = this.openRefundModal.bind(this);
    this.closeRefundModal = this.closeRefundModal.bind(this);
  }

  componentWillMount() {
    ymmStorage.removeItem('is_chat_trial');
    const { user, location } = this.props;
    if (_.isEmpty(user.data)) {
      this.props.getUserProfile();
    }
    if(sessionStorage.getItem('distanceFromBottom')) {
      sessionStorage.removeItem('distanceFromBottom');
    }

    const { params, chatList } = this.props;
    const { id } = params;
    const chatRoom = findChatRoomByIdentifier(chatList, id);
    const to_user_id = getUserIdFromIdentifier(id);
    if (chatRoom) {
      const { chat_room_id } = chatRoom;
      socket.emit('read_message', {
         room: chat_room_id,
        identifier: id,
        from_user_id: getCurrentUserIdFromChatRoomAndIdentifier(chatRoom.chat_room_id, to_user_id),
        to_user_id: to_user_id,
        is_sent: true,
      });
      // set document title
      document.title = chatRoom.name;
    }
    // if (!this.props.user.isAskedPushNotiRequest) {
    //   subscribeUser(this.props)
    // }
    ymmStorage.removeItem('require_payment_url');
    ymmStorage.removeItem('require_payment_male_id');
    ymmStorage.removeItem('require_payment_female_id');
    ymmStorage.removeItem('chat_to_username');
  }
  componentDidUpdate() {
    const { params} = this.props;
    const { id } = params;
    const user_id = getUserIdFromIdentifier(id);
    const distanceFromBottom = document.body.scrollHeight - window.innerHeight - window.scrollY;
    if (distanceFromBottom <= window.innerHeight && user_id != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID) {
      // scrollToBottom();
    }
  }

  componentDidMount() {
    const { params, location } = this.props;
    const { id } = params;
    const user_id = getUserIdFromIdentifier(id);
    // scroll to botton in the first time when user access chat frame
/*    if (user_id != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID) {
      setTimeout(() => {
        scrollToBottom();
      }, 500);
    }*/
    setTimeout(() => {
        scrollToBottom();
      }, 500);
    this.initChat(this.props);
     //window.addEventListener('scroll', this.listenScrollEvent);
     const props = this.props;
     setTimeout(() => {
      props.markMessageAsReadInState(props.params.id);
     }, 5000);

     if (location.search == '?unlock_success') {
      this.showRewardNoti();
      browserHistory.push(`/chat/${id}`)
    }
    const interval = setTimeout(() => { this.showPaymentPopupInterval() }, 5000);
    this.setState({ interval });
  }

  showPaymentPopupInterval = () => {
    const existForFree = this.props.chatList.data.some(x => x.for_free);
    if (!this.props.valid_user && !this.state.isOpendPaymentModal && existForFree) {
      this.showPaymentPopup();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.chatList.data.length && nextProps.chatList.data.length) {
      this.initChat(nextProps);
    }
  }

  componentWillUnmount() {
      document.title = DEFAULT_TITLE;
      clearInterval(this.state.interval);
  }
  initChat(props) {
    const { params, chatList, messages } = props;
    const { id } = params;
    const chatRoom = findChatRoomByIdentifier(chatList, id);
    //if fake identifier or chatRoom is not exist
    if (typeof chatRoom == 'undefined') {
      browserHistory.push('/chat-list');
      return;
    }
    const user_id = getUserIdFromIdentifier(id);
    if (chatRoom) {
      const { chat_room_id } = chatRoom;
      const current_user_id = getCurrentUserIdFromChatRoomAndIdentifier(chat_room_id, user_id);
      this.setState({chatRoom, current_user_id});
      this.props.fetchMessages(chat_room_id, id);
      const that = this;
      socket.off('join');
      socket.off('new_msg');
      socket.emit('join', { room: chat_room_id });
      socket.on('new_msg', (data) => {
        if (window.location.href.indexOf('/chat/') === -1) { // does not recall api when user open chat room
          return;
        }
        if (data.msg.from_user_id == this.state.current_user_id && data.msg.to_user_id == user_id) { // user open 2 browser tabs
          const self_message = {
            _id: new Date().toISOString(), // currently, we use this field to check duplicate
            from_user_id: this.state.current_user_id,
            to_user_id: user_id,
            user_id: data.msg.user_id,
            friend_user_id: user_id,
            chat_room_id: chatRoom.chat_room_id,
            message: data.msg.message,
            read_at: new Date().toISOString(),
            is_deleted: null,
            created_at: new Date().toISOString(),
            is_sent: false,
            type: data.msg.type
          };
          const saveToDb = false;
          props.updateMessages(id, self_message, saveToDb);

        }

        if (
          data.msg.from_user_id == this.state.current_user_id || // prevent current user open chat in 2 broser tab
          data.msg.to_user_id != this.state.current_user_id || // prevent current user open chat in 2 broser tab
          data.msg.from_user_id != user_id // prevent to send message to other user
        ) {
          return;
        }

        const message = {
          _id: new Date().toISOString(),
          user_id: data.msg.user_id,
          friend_user_id: data.msg.friend_user_id,
          chat_room_id,
          message: data.msg.message,
          read_at: new Date().toISOString(),
          is_deleted: null,
          created_at: data.msg.created_at,
          _created_at: data.msg.created_at,
          is_sent: false,
          type: data.msg.type
        };
        const saveToDb = false;
        props.updateTypingStatus(false, id);
        props.updateMessages(id, message, saveToDb);
        props.markAllMessageAsRead(user_id, chat_room_id);
        const distanceFromBottom = document.body.scrollHeight - window.innerHeight - window.scrollY;
        if (data.msg.type != 3) { // is not ice breaking
          this.setState({sent_or_received_msg : true})
        }
        if (distanceFromBottom < 200 ) {
          setTimeout(() => {
            scrollToBottom();
          }, 1000);
        }
      });
      socket.off('typing');
      socket.on('typing', (data) => {
        if (data.from_user_id != user_id) {
          return;
        }
        if (getUserIdFromIdentifier(data.identifier) !== user_id) {
          props.updateTypingStatus(true, id);
          // setTimeout(() => {
          //   scrollToBottom();
          // }, 200);
          if (!messages.isTyping) {
            setTimeout(() => {
              props.updateTypingStatus(false, id);
            }, 15000);
          }
        }
      })

      socket.off('message_sent');
      socket.on('message_sent', (data) => {
        if (data.from_user_id != user_id) {
          return;
        }
        if (getUserIdFromIdentifier(data.identifier) !== user_id) {
          props.updateTypingStatus(false, id);
        }
      })
    }
  }

  onClickChatFrame() {

     const { params, chatList } = this.props;
      const { id } = params;
      const chatRoom = findChatRoomByIdentifier(chatList, id);
      const to_user_id = getUserIdFromIdentifier(id);
      if (chatRoom) {
        const { chat_room_id } = chatRoom;
        socket.emit('read_message', {
           room: chat_room_id,
          identifier: id,
          from_user_id: getCurrentUserIdFromChatRoomAndIdentifier(chatRoom.chat_room_id, to_user_id),
          to_user_id: to_user_id,
          is_sent: true,
        });
      }
  }

 /**
 * type: 1 defaul message
 * type: 2 user to user
 * type: 3 ice breaking
 * type: 4 admin message
 **/
  handleSendMessage(msg='', type = 2) {
    const props = this.props;
    // const { chatRoom } = this.state;
    const { messageBox, params, chatRoom, user, is_chat_trial } = props;
    const { messageContent } = messageBox;  // message you are entered
    const { id } = params;
    const to_user_id = getUserIdFromIdentifier(id);
    const messageText = type == 3 ? msg : $.trim(messageContent) ? $.trim(xss(messageContent)) : xss(msg);
    let is_random_match = false;
     if (typeof chatRoom.end_match_at !== 'undefined' && chatRoom.end_match_at) {
        if (moment(new Date()).isBefore(moment.utc(chatRoom.end_match_at).local().format())) {
          is_random_match = true;
        }
      }

    if (
      (
        (is_chat_trial && user.data.gender == 1) ||
        (user.data.gender == 2 && !chatRoom.paid_user_to && !chatRoom.for_free && !is_random_match)
      ) &&
      chatTrial.containSuspendText(messageText)
    ) {
      chatNotification.suspendMessage();
      return;
    }

    const message = {
      _id: new Date().toISOString(), // currently, we use this field to check duplicate
      from_user_id: this.state.current_user_id,
      to_user_id: to_user_id,
      user_id: this.state.current_user_id,
      friend_user_id: to_user_id,
      chat_room_id: chatRoom.chat_room_id,
      message: messageText,
      is_deleted: null,
      created_at: new Date().toISOString(),
      _created_at: new Date().toISOString(),
      type: type
    };
    // send message to friend via socket
    socket.emit('send_msg', { room: chatRoom.chat_room_id , msg: message });
    if (
      typeof Notification != 'undefined' &&
      this.props.user.list_users_showed_push_noti_modal.findIndex(user => user == to_user_id) == -1
      && Notification.permission == 'default'
      && ymmStorage.getItem('mobile_notification_permission') == 'denied'
    ) {
      props.updatePushNotiModalStatus(true, to_user_id) //only show popup 1 time
    }
    const saveToDb = true;
    props.updateMessages(id, message, saveToDb);
    const chat_room_identifier = getIdentifierFromId(this.state.current_user_id);
    const avatar = user.data.thumb_avatar_url ? user.data.thumb_avatar_url :
    (
        user.data.gender == 1 ?
        `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_MALE_AVATAR}` :
        `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_FEMALE_AVATAR}`
    )
    props.sendChatNotification(to_user_id, user.data.name, messageText, chat_room_identifier, avatar)
    this.setState({sent_or_received_msg : true})
    scrollToBottom();
  }

  // get read message based on scroll position
  markMessageAsRead(messages, index, user_id) {
    if (messages.length) {
      const messages_length = messages.length;
      const message_ids = []; // this for update database
      const unread_messages_ids = []; // this for update state
      for(var i = messages_length - 1; i >= index - 5; i--) {
        if (messages[i].user_id == user_id && messages[i].read_at == null) {
          unread_messages_ids.push(messages[i]._id);
          message_ids.push('chat_message_ids[]=' + messages[i]._id)
        }
      }
      if (message_ids.length) {
        this.props.markMessageAsRead(message_ids);
      }
    }
  }

  closeModal() {
    ymmStorage.removeItem('is_chat_trial');
    this.setState({isOpendPaymentModal: false})
  }

  handleClick() {
    this.setState({ modalOpen: false })
    return this.showRewardNoti();
  }

  handleRadioClick = (type_unlock, event) => {
    this.setState({ selectedPackage: type_unlock })
    $(`#unlock_${type_unlock}`).click();
    // if (type_unlock == 'package') {
    //   window.location.href = `${process.env.REACT_APP_YMM_URL}payment/upgrade`;
    // }
  }

  handleContinueClick = () => {
    if (this.state.selectedPackage == 'package') {
      window.location.href = `${process.env.REACT_APP_YMM_URL}payment/upgrade`;
      return;
    }
    // the following for unlock chat by coin
    if (this.state.remaining_coins < NUMBER_COIN_TO_UNLOCK) {
      window.location.href = `${process.env.REACT_APP_YMM_URL}payment/coin`;
      return;
    }
    if (this.state.clickedUnlockChat) {
      return;
    }
    this.setState({clickedUnlockChat: true});
    // Send google analytic
    const gaTrackData = {
      page_source: 'Chat List'
    };
    const eventAction = {
      eventCategory: 'Coin',
      eventAction: 'Unlock Chat',
      eventLabel: 'Unlock chat room by coin'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)

    let male_id = null;
    let female_id = null;
    const { current_user_id, chatRoom } = this.state;
    if (chatRoom.gender == 1) {
      male_id = chatRoom.id;
      female_id = current_user_id;
    } else {
      male_id = current_user_id;
      female_id = chatRoom.id;
    }
    this.props.unlockChat(getIdentifierFromId(male_id), getIdentifierFromId(female_id)).then(res => {
      if (typeof res.payload.data !== 'undefined' && res.payload.data.execution_result.execution_code == "01") {
        this.showRewardNoti();
        this.setState({isOpendPaymentModal: false})
        this.props.unlockChatRoomInState(chatRoom.chat_room_id);
        this.props.fetchChatList();
      }
      this.setState({clickedUnlockChat: false});
      mobile_socket.emit('send_event', {type: 'reload_user'})
    }).catch(() => {
      this.setState({clickedUnlockChat: false});
    })

  }

  showRewardNoti() {
    const reward_coins_url = `${CDN_URL}/chat_ui/unlock-success.png`
    const RewardNoti = $(`
      <div class="noti noti--bg-green mbm">
        <div class="noti__body">
          <div class="noti__header mbm">
            <img src= ${reward_coins_url} alt="coin_reward_icon+coin.png" />
          </div>
          <div class="noti__content txt-bold">Mở chat <br /> thành công</div>
        </div>
      </div>
    `)

    setTimeout(function() {
      $('body').append(RewardNoti)
    }, 200)

    setTimeout(function() {
      $(RewardNoti).remove()
    }, 5000)
  }

  showPaymentPopup() {
    this.props.getRemainingCoin().then(res => {
      const remaining_coins = res.payload.data.data.remaining_coins;
      this.props.updateYmmPermission();
      this.setState({remaining_coins: remaining_coins, isOpendPaymentModal: true})
    })
  }

  handleUnlockChat() {

  }

  addCoin(chat_to_username) {
    let male_id = null;
    let female_id = null;
    const { current_user_id, chatRoom } = this.state;
    if (chatRoom.gender == 1) {
      male_id = chatRoom.id;
      female_id = current_user_id;
    } else {
      male_id = current_user_id;
      female_id = chatRoom.id;
    }
    let ymm_url_payment = `${process.env.REACT_APP_YMM_URL}payment/coin`;
    let chat_url = process.env.REACT_APP_CHAT_URL;
    if(chat_url.substr(-1) == '/') {
        chat_url = chat_url.substr(0, chat_url.length - 1);
    }

    // Send google analytic
    const gaTrackData = {
      page_source: 'Chat List'
    };
    const eventAction = {
      eventCategory: 'Coin',
      eventAction: 'GetMoreCoin',
      eventLabel: 'Get more coin'
    }
    this.props.sendGoogleAnalytic(eventAction, gaTrackData)
    const current_url = `${chat_url}${this.props.location.pathname}`;
    ymmStorage.setItem('require_payment_url', current_url);
    ymmStorage.setItem('chat_to_username', chat_to_username);
    ymmStorage.setItem('number_coin_to_unlock', NUMBER_COIN_TO_UNLOCK);
    ymmStorage.setItem('require_payment_male_id', getIdentifierFromId(male_id));
    ymmStorage.setItem('require_payment_female_id', getIdentifierFromId(female_id));
    // const add_more_coin_url = `${ymm_url_payment}?require_payment_url=${encodeURIComponent(current_url)}`;
    window.location.href = ymm_url_payment;
  }

  onclickVerify() {
    const { chatRoom } = this.props;
    if (chatRoom.verify_selfie_picture) {
      paymentNotification.alreadyVerify();
      return;
    }
    window.location.href = `${process.env.REACT_APP_YMM_URL}verify`;
  }

  renderAllDialogue(chatRoom, identifier, data, paging_model, valid_user, is_valid_refund) {
    const { user } = this.props;
    if (typeof data === 'undefined' || _.isEmpty(user)) {
      return '';
    }

    let current_friend_user_id = null;
    let guilde_message = '';
    if (user.data.gender == 1) {
      guilde_message = `Bí kíp phá băng đây! <br /> Hãy chủ động giới thiệu về bản thân <br /> để ${chatRoom.name} hiểu bạn hơn nhé!`;
    } else {
      guilde_message = `${chatRoom.name} rất muốn được trò chuyện cùng bạn đấy. <br /> Hãy trả lời tin nhắn của anh ấy nhé!`;
    }
    const user_id = getUserIdFromIdentifier(identifier);
    let is_admin_support = user_id == process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID;
    // if (!is_admin_support) {
    //   data.push({
    //     _id: new Date().toISOString(),
    //     chat_room_id: chatRoom.chat_room_id,
    //     created_at: new Date().toISOString(),
    //     // friend_user_id: getCurrentUserIdFromChatRoomAndIdentifier(chatRoom.chat_room_id, getUserIdFromIdentifier(identifier)),
    //     is_deleted: null,
    //     is_sent: true,
    //     message: guilde_message,
    //     read_at: true,
    //     user_id: getUserIdFromIdentifier(identifier)
    //   });
    // }

    const data2 = data.reverse().map((message) => {
      if (message.friend_user_id == current_friend_user_id) {
        message.created_at = null;
      } else {
        current_friend_user_id = message.friend_user_id;
      }

      return message;
    });

    const avatar = chatRoom.small_avatar_url ? chatRoom.small_avatar_url : // default avatar of friend
    (
        user.data.gender == 2 ? //gender of current user
        `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_MALE_AVATAR}` :
        `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_FEMALE_AVATAR}`
    )
    // this.props.markAllMessageAsRead(getUserIdFromIdentifier(identifier));
    return data2.map((m, index) => {
      if (parseInt(m.user_id, 10) === getUserIdFromIdentifier(identifier)) {
        return (<Dialogue key={index} id={index} {...m} avatarUrl={avatar} identifier={identifier} is_admin_support={is_admin_support} />);
      }

      return (<MyDialogue
        key={index}
        total_msg={data2.length}
        index={index} id={index} {...m}
        identifier={identifier}
        chatRoom={chatRoom}
        is_valid_refund={is_valid_refund}
        openRefundModal={this.openRefundModal}
        closeRefundModal={this.closeRefundModal}
      />);
    });
  }

  openRefundModal() {
    this.setState({openRefundModal: true})
  }

  closeRefundModal() {
    // setTimeout(() => { this.showPaymentPopupInterval() }, 3000);
    this.setState({openRefundModal: false})
  }

  render() {
    const { params, messages, chatRoom, valid_user, showPaymentPopup, user } = this.props;
    const { paging_model } = messages;
    let data = messages.data[params.id];
    if (typeof data === 'undefined') {
      data = [];
    }
    const user_id = getUserIdFromIdentifier(params.id);
    const isShowLoader = !chatRoom;
    let guilde_message = '';
    if (user.data.gender == 1) {
      guilde_message = `Bí kíp phá băng đây! <br /> Hãy chủ động giới thiệu về bản thân <br /> để ${chatRoom.name} hiểu bạn hơn nhé!`;
    } else {
      guilde_message = `${chatRoom.name} rất muốn được trò chuyện cùng bạn đấy. <br /> Hãy trả lời tin nhắn của anh ấy nhé!`;
    }
    // calculate for refund
    let is_valid_refund = false;
    if (chatRoom.match_type == 2 && user.data.gender == 1 && !chatRoom.ignore_refund) { // unlock chat room
      const is_sent_msg_after_unlock = data.some((msg, i) => {
        // valid refund when there is no  received message after unlock time
        return msg.user_id == user.data._id && // received message
          moment.utc(chatRoom.unlock_chat_created_at).isBefore(moment.utc(msg._created_at));
      });
      is_valid_refund = is_sent_msg_after_unlock && !data.some((msg, i) => {
        // valid refund when there is no  received message after unlock time
        return msg.user_id != user.data._id && // received message
          moment.utc(chatRoom.unlock_chat_created_at).isBefore(moment.utc(msg._created_at));
      })
    }

    const avatar = chatRoom.small_avatar_url ? chatRoom.small_avatar_url : // default avatar of friend
    (
        user.data.gender == 2 ? //gender of current user
        `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_MALE_AVATAR}` :
        `${process.env.REACT_APP_PUBLIC_CDN}${process.env.REACT_APP_DEFAULT_FEMALE_AVATAR}`
    )

    const activePayment = this.state.remaining_coins >= NUMBER_COIN_TO_UNLOCK
    // const  = data.some((sendMsg, i) => sendMsg.user_id == user.data._id && sendMsg.type == 3);
    // const isShowLoader = true;
    return (
      <div>
        <section className="ymc-c-f" onClick={this.onClickChatFrame}>
          {user_id != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID && <DialogueDefault avatarUrl={avatar} message={guilde_message} chatRoom={chatRoom} />}
          {user_id != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID && <SubPhotoSlideShow {...this.props} />}
          {isShowLoader ? (
            <LoaderMessages />
          ) : this.renderAllDialogue(chatRoom, params.id, uniqChatMessages(data), paging_model, valid_user, is_valid_refund)}
          {user_id != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID && <IceBreakingQuestion {...this.props} sendMessage={this.handleSendMessage} sent_or_received_msg={this.state.sent_or_received_msg} />}
          {messages.isTyping && <Typing />}
        </section>
        {user_id != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID && <AppBottomBarContainer {...this.props} valid_user={valid_user} showPaymentPopup={this.showPaymentPopup} sendMessage={this.handleSendMessage} />}
        <Modal
          isOpen={this.state.isOpendPaymentModal}
          onRequestClose={this.closeModal}
          className="modal__content modal__content--7 padding-0"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
          shouldCloseOnOverlayClick={false}
        >
          <div className="modal__top padding-t20 padding-b20 txt-center">
            <img className="img-100 padding-b10" src={`${CDN_URL}/chat_ui/unlock-chat.png`} alt="img.png" />
            <div className="txt-xlg txt-uppercase txt-bold txt-blue">
              Để trò chuyện tiếp với {this.state.chatRoom.name}<br />
              chọn 1 trong 2 cách sau
            </div>
          </div>
          <div className="modal__txt-content padding-t10">
            <div className="row padding-b10 txt-bold l-flex-vertical-center" onClick={(event) => {this.handleRadioClick('coin', event)}}>
              <div className="col-xs-2">
                <label className="form__input--d radio-large margin-0">
                  <input type="radio" name="unlockChat" id="unlock_coin" value="on" onClick={() => {this.handleRadioClick('coin')}}/>
                  <div className="fake-radio"></div>
                </label>
              </div>
              <div className="col-xs-10">
                <div className="txt-blue txt-xlg">{activePayment ? "Trừ" : "Cần"} <img className="banner__icon--4" src={`${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`} alt="Xu" /> 125 xu để mở chat với <br />
                {this.state.chatRoom.name}. {activePayment ? "Mở ngay!" : "Nạp thêm ngay!"}</div>
              </div>
            </div>
            <div className="row txt-bold l-flex-vertical-center" onClick={(event) => {this.handleRadioClick('package', event)}}>
              <div className="col-xs-2">
                <label className="form__input--d radio-large margin-0">
                  <input type="radio" name="unlockChat" id="unlock_package" defaultChecked onClick={() => {this.handleRadioClick('package')}}/>
                  <div className="fake-radio"></div>
                </label>
              </div>
              <div className="col-xs-10 mbs">
                <div className="txt-blue l-flex-vertical-center txt-xlg"><img className="mt img-40" src={`${CDN_URL}/chat_ui/Upgrade-account.png`} alt="Xu" /> Nâng cấp tài khoản
                </div>
                <div className="txt-light">Mở chat với toàn bộ người đã ghép<br />
                đôi cùng nhiều tiện ích khác</div>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-6 col-xs-offset-3">
                <div className={`${activePayment ? "txt-blue" : "txt-red"} txt-xlg`}>
                  Bạn còn <img className="banner__icon--4" src={`${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`} alt="Xu" /> {this.state.remaining_coins} xu
                </div>
              </div>
            </div>
            <div className="padding-t10 txt-center">
              <button className="btn btn--blue btn--b txt-lg" onClick={this.handleContinueClick}>Đồng ý</button>
            </div>
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
        </Modal>
        <RefundModal openRefundModal={this.openRefundModal} closeRefundModal={this.closeRefundModal} open={this.state.openRefundModal} {...this.props} />
        <ConfirmModal
          {...this.props}
          currentModal='pushNoti'
          isOpen={this.props.user.isPushNotiModalOpen}
          name={chatRoom.name}
        />
      </div>
    );
  }
}

ChatFrame.propTypes = {
  messages: PropTypes.object,
  params: PropTypes.object,
};

export default ChatFrame;
