import React from 'react';

const DialogueChathead = ({ avatarUrl, id, is_admin_support, gender }) => (
  <div className={`ymc-dialogue__chathead ymc-dialogue__chathead--1`}>
    <div className={`chathead chathead--1`}>
      <img src={avatarUrl} role="presentation" />
    </div>
  </div>
);

export default DialogueChathead;
