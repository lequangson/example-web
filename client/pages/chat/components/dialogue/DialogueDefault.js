import React from 'react';
import { random } from 'lodash';
import { MATCH_TYPE_RANDOM, CDN_URL } from '../../constants';
class DialogueDefault extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      default_message: ''
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }
  render() {
    const {avatarUrl, message, chatRoom} = this.props;
    const { match_type, is_super_match, friend_match_type, match_created_by, id } = chatRoom;
    let name = '';
    if (chatRoom.gender == 1) {
      name = 'anh ấy';
    } else {
      name = 'cô ấy';
    }

    const default_messages = [
      `Bạn có thể hỏi ${name} xem ${name} đã đi du lịch được nhiều chưa!`,
      `Mở lời trò chuyện bằng cách hỏi ${name} có thích đọc sách không cũng là một ý hay đó!`,
      `Hãy hỏi ${name} về món ăn ${name} thích nhất chẳng hạn!`,
      `Bạn có thể bắt đầu bằng một lời khen về phong cách của ${name}.`,
      `Gửi lời khen về tính cách của ${name} cũng là cách mở lời hiệu quả đó.`
    ];
    let default_message = '';
    if (match_type == MATCH_TYPE_RANDOM) {
      default_message = `Duyên số đã chọn ${chatRoom.name} ghép đôi ngẫu nhiên với bạn. Hãy nói gì với ${name} nhé!`;
    } else if ( friend_match_type == 3 && match_created_by == id ) {
      // default_message = `${chatRoom.name} rất muốn tìm hiểu về bạn đấy!`;
      default_message = `${chatRoom.name} đã sử dụng tính năng\n"Chat luôn không cần đợi thích"\n với bạn`;
    } else {
      const random_number = random(0, 4);
      default_message = default_messages[random_number];
    }
    if (chatRoom && match_type == 3 && is_super_match) {
      return (
        <div className="ymc-dialogue">
          <div className="ymc-dialogue__content">
            <div className="ymc-messages">
              <div className="ymc-messages__content">
                <div className="ymc-messages__msg">
                  <div className="ymc-messages__msg__content ymc-messages__msg__content--1">
                    <div className="ymc-dialogue__chathead ymc-dialogue__chathead--2 card">
                      <div className="chathead chathead--2">
                        <img src={avatarUrl} alt="60x60"/>
                      </div>
                    </div>
                    <span className='txt-light txt-lg' dangerouslySetInnerHTML={{__html: default_message.replace(/(?:\r\n|\r|\n)/g, '<br />'),}}></span> <br />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
    return (
      <div className="ymc-dialogue">
        <div className="ymc-dialogue__content">
          <div className="ymc-messages">
            <div className="ymc-messages__content">
              <div className="ymc-messages__msg">
                <div className="ymc-messages__msg__content ymc-messages__msg__content--1">
                  <div className="ymc-dialogue__chathead ymc-dialogue__chathead--2">
                    <div className="chathead chathead--2">
                      <img src={avatarUrl} alt="60x60"/>
                    </div>
                  </div>
                  <span dangerouslySetInnerHTML={{__html: default_message.replace(/(?:\r\n|\r|\n)/g, '<br />'),}}></span> <br />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default DialogueDefault;
