import React, { PropTypes } from 'react';
import Messages from '../Messages';
import MyMessages from '../MyMessages';
import getUserIdFromIdentifier from '../../helpers/getUserIdFromIdentifier';

const DialogueContent = props => (
  <div className="ymc-dialogue__content">
    {parseInt(props.user_id, 10) === getUserIdFromIdentifier(props.identifier) ? (
      <Messages {...props} />
    ) : (
      <MyMessages {...props} />
    )}
  </div>
);

DialogueContent.propTypes = {
  from_recipient: PropTypes.bool,
};

export default DialogueContent;
