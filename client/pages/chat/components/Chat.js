import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { isEmpty } from 'lodash';
import moment from 'moment';
import AppTopBarContainer from 'chat/containers/AppTopBarContainer';
import ChatFrame from './ChatFrame';
import SocketCom from './Socket';
import $ from 'jquery'
import AppBottomBarContainer from 'chat/containers/AppBottomBarContainer';
import findChatRoomByIdentifier from 'chat/helpers/findChatRoomByIdentifier';
import getUserIdFromIdentifier from 'chat/helpers/getUserIdFromIdentifier';
import * as chatTrial from 'chat/helpers/chatTrial';
import * as ymmStorage from 'chat/helpers/ymmStorage';
import ConfirmModal from './modals/ConfirmModal';
import {
  MATCH_TYPE_NORMAL, MATCH_TYPE_FREE, MATCH_TYPE_UNLOCK,
  MATCH_TYPE_SUPER, MATCH_TYPE_RANDOM
} from 'chat/constants';

class Chat extends Component {

  constructor(props) {
    super(props);
    this.state = {
      chatRoom: {},
      valid_user: false,
      chatTrialExpiredRequest: false,
    }
    this.initChat = this.initChat.bind(this);
  }
  componentWillMount() {
    const { chatList, params, user } = this.props;
    if (!chatList.data.length) {
      this.props.fetchChatList();
    }
  }
  // @TODO need to check if list friend exist before make chat connect
  componentDidMount() {
    this.initChat(this.props);
    this.fixFixedOnMobile()
  }

  fixFixedOnMobile() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      const body = $('body');

      /* bind events */
      $(document)
        .on('focus', '.ymc-bottombar form', function() {
          body.addClass('fixfixed');
      })
        .on('blur', '.ymc-bottombar form', function() {
          body.removeClass('fixfixed');
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.chatList.data.length && nextProps.chatList.data.length) {
      this.initChat(nextProps);
    }
    const { chatList, params, user, messages } = nextProps;
    if (chatList.data.length && !isEmpty(user.data) && user.data.gender == 1) { // female  is free chat
      const chatRoom = chatList.data.find(item => getUserIdFromIdentifier(item.identifier) == getUserIdFromIdentifier(params.id))
      if (chatRoom.match_type == 5) {
        let messages_data = messages.data[params.id];
        if (typeof messages_data === 'undefined') {
          messages_data = [];
        }
        if (
          (
            messages_data.length &&
            !chatTrial.isValid(user.data, this.state.chatRoom, messages_data) &&
            !this.state.chatTrialExpiredRequest
          ) ||
          (typeof user.data.user_paid_licences_to != 'undefined' && user.data.user_paid_licences_to) ||
          (typeof chatRoom.end_match_at != 'undefined' && chatRoom.end_match_at)
        ) {
          this.setState({chatTrialExpiredRequest: true})
          nextProps.chatTrialExpired(chatRoom.chat_room_id).then(() => {
            nextProps.fetchChatList();
          });
        }
      }
    }
  }

  initChat(props) {
    const { params, chatList, messages } = props;
    const { id } = params;
    const chatRoom = findChatRoomByIdentifier(chatList, id);
    if (chatRoom) {
      // const { chat_room_id } = chatRoom;
      this.setState({chatRoom});
    }
  }

  render() {
    const props = this.props;
    const { chatList, params, user, messages } = props;
    const { id } = params;
    const chatRoom = findChatRoomByIdentifier(chatList, id);
    if (!chatList.data.length) {
      return <div>Đang tải dữ liệu</div>
    }
    let valid_user = false;

    let messages_data = messages.data[params.id];
    if (typeof messages_data === 'undefined') {
      messages_data = [];
    }
    if (chatList.data.length) {
      const user_to = chatList.data.find(item => getUserIdFromIdentifier(item.identifier) == getUserIdFromIdentifier(params.id))
      if (isEmpty(user_to) || user_to.user_status != 1) {
        browserHistory.push('/chat-list');
        return;
      }
      if (!isEmpty(user_to)) {
        if (user_to.have_permission || user_to.for_free) {
          valid_user = true;
        }
        if (getUserIdFromIdentifier(user_to.identifier) == process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID) {
          valid_user = true;
        }
      }
      if (typeof user.data.user_paid_licences_to !== 'undefined' && user.data.user_paid_licences_to) {
        if (moment(new Date()).isBefore(moment.utc(user.data.user_paid_licences_to).local().format())) {
          valid_user = true;
        }
      }
      if (user_to.end_match_at) {
        if (moment(new Date()).isBefore(moment.utc(user_to.end_match_at).local().format())) {
          valid_user = true;
        }
      }
      if (user_to.end_unlock_at) {
        // if (moment(new Date()).isBefore(moment.utc(user_to.end_unlock_at).local().format())) {
        //   valid_user = true;
        // }
        valid_user = true;
      }
      /*
      enum match_type: {
        match_type_normal: 0,
        match_type_free_chat: 1,
        match_type_unlock_chat: 2,
        match_type_super_match: 3,
        match_type_random_match: 4,
        match_type_chat_trial: 5,
        match_type_explorer_question: 6
      }*/
      if ([MATCH_TYPE_FREE, MATCH_TYPE_UNLOCK, MATCH_TYPE_SUPER].indexOf(user_to.match_type) > -1) {
        valid_user = true;
      }
    }
    // Chat Trial
    let is_chat_trial = false;
    let excluded_chat_trial = false;
    if (
        (typeof user.data.user_paid_licences_to != 'undefined' && user.data.user_paid_licences_to) ||
        (typeof this.state.chatRoom.end_match_at != 'undefined' && this.state.chatRoom.end_match_at)
    ) {
      excluded_chat_trial = true;
    }
    if (!excluded_chat_trial && !valid_user && !isEmpty(user.data) && user.data.gender == 1 &&  chatTrial.isValid(user.data, this.state.chatRoom, messages_data)) {
      valid_user = true;//need to remove
      is_chat_trial = true;
      ymmStorage.setItem('is_chat_trial', 1);
    }
    const total_sent_messages = messages_data.filter(
        item => item.user_id == user.data._id
    ).length;
    return (
      <div className="container-detail">
        <SocketCom />
        <AppTopBarContainer {...props} valid_user={valid_user} />
        {/*<ConfirmModal currentModal="refund" isOpen="true" {...props}/>*/}
        <ChatFrame {...props} chatRoom={chatRoom} valid_user={valid_user} is_chat_trial={is_chat_trial} total_sent_messages={total_sent_messages} />
      </div>
    );
  }
}

Chat.propTypes = {
  fetchMessages: PropTypes.func,
  params: PropTypes.object,
};

export default Chat;
