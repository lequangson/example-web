import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import Modal from 'react-modal'

class ProfileNavigation extends React.Component {

  constructor() {
    super()
    this.state = {
      isConfirmModalOpen: false
    }
    this.closeConfirmModal = this.closeConfirmModal.bind(this)
  }

  openConfirmModal() {
    this.setState({ isConfirmModalOpen: true })
  }

  closeConfirmModal() {
    this.setState({ isConfirmModalOpen: false })
  }

  render() {
    const user = this.props.user
    return (
      <div className="row txt-center justify-content-between">
        <div className="col-3">
           <a href={`${process.env.REACT_APP_YMM_MOBILE_URL}profile/${user.identifier}`} target="_blank"
             className="btn btn--5 mrs btn--b"
           >
              Xem hồ sơ
            </a>
        </div>
        <div className="col-3">
          <a
            href={`${process.env.REACT_APP_YMM_MOBILE_URL}report/${user.identifier}`}
            target="_blank"
            className="btn btn--5 txt-center mrs btn--b"
          >
            <i className="fa fa-warning txt-warning padding-r5"></i>
            Báo cáo
          </a>
        </div>
        <div className="col-3">
          <a
            href={`${process.env.REACT_APP_YMM_MOBILE_URL}block-form/${user.identifier}`}
            target="_blank"
            className="btn btn--5 txt-center  btn--b"
          >
            <i className="fa fa-ban txt-red padding-r5"></i>
            Chặn
          </a>
        </div>
        <div className="col-3">
          <button
            onClick={() => {this.openConfirmModal()}}
            target="_blank"
            className="btn btn--5 txt-center  btn--b"
          >
            <i className="fa fa-trash txt-red padding-r5"></i>
            Xóa trò chuyện
          </button>
        </div>
        <Modal
          isOpen={this.state.isConfirmModalOpen}
          onRequestClose={this.closeConfirmModal}
          className="modal__content modal__content--3"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="mbmd padding-t10 txt-center">
            <div className="mbl txt-lg txt-light">Xóa cuộc trò chuyện<br /> với {user.nick_name}</div>
          </div>
          <div className="row padding-b10 mbmd">
            <div className="col-6 txt-right">
              <button className="btn mrm">
                <i className="fa fa-times"></i>
                <span className="padding-l5">Hủy</span>
              </button>
            </div>
            <div className="col-6 txt-left">
              <button className="btn btn--blue mrm">
                <i className="fa fa-trash"></i>
                <span className="padding-l5">Xóa</span>
              </button>
            </div>
          </div>
          <button className="modal__btn-close" onClick={this.closeConfirmModal}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    )
  }
}

export default ProfileNavigation
