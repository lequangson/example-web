import React, { Component } from 'react';
import { connect } from 'react-redux';
import $ from 'jquery';
const socket = require('socket.io-client')(process.env.REACT_APP_SOCKET_URL);
// const socket = require('socket.io-client')('http://192.168.1.133:8097/');
import findChatRoomByIdentifier from '../../helpers/findChatRoomByIdentifier';
import getUserIdFromIdentifier from '../../helpers/getUserIdFromIdentifier';
import { getCurrentUserIdFromChatRoomAndIdentifier } from '../../helpers/common';
import getIdentifierFromId from '../../helpers/getIdentifierFromId';
import { unlockChat, unlockChatRoomInState, use_gift, updateChatRoomFree } from '../../actions/Payment';
import { fetchChatList } from '../../actions/chatList';

class TextBox extends Component {
  constructor() {
    super();
    this.state = {
      valid_user: false
    }
    this.handleKeyup = this.handleKeyup.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.clearMessage = this.clearMessage.bind(this);
    this.isInputEmpty = this.isInputEmpty.bind(this);
    this.enableKeyboad = this.enableKeyboad.bind(this);
    this.updateCaretPosition = this.updateCaretPosition.bind(this);
    this.messageWithEmoji = this.messageWithEmoji.bind(this);
    this.handleOnBlur = this.handleOnBlur.bind(this);
  }

  componentWillMount() {
    this.props.updateTextBoxValue('');
    const { valid_user } = this.props;
    this.setState({valid_user: valid_user})
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.messageBox.shouldUpdateMessage) {
      const emoji = nextProps.messageBox.emojiSelected;
      const newMessage = this.messageWithEmoji(emoji);

      this.props.updateTextBoxValue(newMessage);
      this.props.updateMessage(false);

      const newCaretPosition = this.props.messageBox.caretPosition + emoji.length;
      this.props.updateCaretPosition(newCaretPosition);
    }
  }

  componentDidUpdate(nextProps) {
    if (this.props.messageBox.shouldEnableKeyboard) {
      this.enableKeyboad();

      // set caret selection correctly
      if (nextProps.messageBox.shouldUpdateMessage) {
        this.msgInput.selectionEnd = this.props.messageBox.caretPosition;
      }
    }
  }

  handleKeyup(e) {
    const { valid_user, messageBox, user, params, chatList } = this.props;
    const { messageContent } = messageBox;
    if (e.which == 13 && !e.shiftKey)
    {
      $('#chat-guide').addClass('is-hide');

      if (!valid_user) {
        this.props.showPaymentPopup()
        return;
      }
      if (!messageContent.trim().split("\u21b5").join('').length) {
        return;
      }
      e.preventDefault();
      const { messageBox } = this.props
      // call send Message event which will be handled in Chat component
      this.props.sendMessage();
      this.props.updateTextBoxValue('');
      if (messageBox.emojiSelectionIsOpen) {
        this.props.toggleEmojiSelection();
      }
      this.props.textBoxhasValue(false);
      this.props.enableKeyboad(true);
      // $('#chat-guide').addClass('is-hide');
    }
  }

  handleChange(evt) {
    if (this.isInputEmpty()) {
      this.props.textBoxhasValue(false);
    } else {
      this.props.textBoxhasValue(true);
    }

    this.updateCaretPosition(evt);
    const { params, chatList } = this.props;
    const { id } = params;
    const chatRoom = findChatRoomByIdentifier(chatList, id);
    const to_user_id = getUserIdFromIdentifier(id);
    if (chatRoom) {
      const { chat_room_id } = chatRoom;
      socket.emit('typing', {
        room: chat_room_id,
        identifier: id,
        from_user_id: getCurrentUserIdFromChatRoomAndIdentifier(chatRoom.chat_room_id, to_user_id),
        to_user_id: to_user_id
      });
    }
    this.props.updateTextBoxValue(this.msgInput.value);
  }

  handleClick(valid_user) {
    if (valid_user) {
      this.setState({valid_user: true})
    }
    const { total_sent_messages } = this.props;
    if (total_sent_messages <= 1) {
      $('#chat-guide').removeClass('is-hide');
    }
    // this.updateCaretPosition(evt);
    const { params, chatList } = this.props;
    const { id } = params;
    const chatRoom = findChatRoomByIdentifier(chatList, id);
    const to_user_id = getUserIdFromIdentifier(id);
    if (chatRoom) {
      const { chat_room_id } = chatRoom;
      socket.emit('read_message', {
         room: chat_room_id,
        identifier: id,
        from_user_id: getCurrentUserIdFromChatRoomAndIdentifier(chatRoom.chat_room_id, to_user_id),
        to_user_id: to_user_id,
        is_sent: true,
      });
    }
  }

  updateCaretPosition() {
    const caretPosition = this.msgInput.selectionStart;
    this.props.updateCaretPosition(caretPosition);
  }

  clearMessage() {
    this.msgInput.value = '';
    this.props.textBoxhasValue(false);
  }

  isInputEmpty() {
    return !this.msgInput.value.trim();
  }

  enableKeyboad(emoji) {
    this.msgInput.focus();
  }

  messageWithEmoji(emoji) {
    const caretPosition = this.props.messageBox.caretPosition;
    const message = this.msgInput.value;

    const a = message.substr(0, caretPosition);
    const b = message.substr(caretPosition, message.length);

    return `${a}${emoji}${b}`;
  }

  handleOnBlur() {
    $('#chat-guide').addClass('is-hide');
  }
  render() {
    const { valid_user } = this.props;
    return (
      <div className="ymc-form__group">
        <div className="txt-blue is-hide" id="chat-guide" style={{'width': '150%'}}>
          Hãy hỏi về sở thích, đồ ăn, thú cưng chẳng hạn...
        </div>
        <textarea
          ref={(ref) => {
            this.msgInput = ref;
          }}
          className="ymc-form__input"
          placeholder="Nhập tin nhắn..."
          rows="1"
          onKeyUp={this.handleKeyup}
          value={this.props.messageBox.messageContent}
          onChange={this.handleChange}
          onClick={() => {this.handleClick(valid_user)}}
          onBlur={this.handleOnBlur}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    unlockChat: async (male_id, female_id) => await dispatch(unlockChat(male_id, female_id)),
    unlockChatRoomInState: (chat_room_id) => {
      dispatch(unlockChatRoomInState(chat_room_id));
    },
    fetchChatList: () => dispatch(fetchChatList()),
    use_gift: async (gift_id, detail, to_user_id) => await dispatch(use_gift(gift_id, detail, to_user_id)),
    updateChatRoomFree: async (from_user_id, to_user_id, description) => await dispatch(updateChatRoomFree(from_user_id, to_user_id, description))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TextBox);
