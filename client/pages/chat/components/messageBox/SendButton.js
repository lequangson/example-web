import React, { Component } from 'react';
import FaPaperPlane from 'react-icons/lib/fa/paper-plane';
import { connect } from 'react-redux';
import {
  ymmUpdateChatFree,
  updateChatFree
} from '../../actions/chatList';
import getUserIdFromIdentifier from '../../helpers/getUserIdFromIdentifier';
import getIdentifierFromId from '../../helpers/getIdentifierFromId';
import { unlockChat, unlockChatRoomInState, use_gift, updateChatRoomFree } from '../../actions/Payment';
import { fetchChatList } from '../../actions/chatList';

class SendButton extends Component {
  constructor() {
    super();
    this.state = {
      isClicked: false
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    if (this.state.isClicked) {
      return;
    }
    this.setState({ isClicked: true });
    const { messageBox, valid_user, user, params, chatList } = this.props;
    if (!valid_user) {
      this.props.showPaymentPopup();
      this.setState({ isClicked: false });
      return;
    }
    // call send Message event which will be handled in Chat component
    this.props.sendMessage();
    const _this = this;
    setTimeout(function(){
      _this.setState({ isClicked: false });
      _this.props.updateTextBoxValue('');
      _this.props.textBoxhasValue(false);
      _this.props.enableKeyboad(true);

    }, 100);

    if (messageBox.emojiSelectionIsOpen) {
      this.props.toggleEmojiSelection();
    }
  }

  render() {
    return (
      <button
        type="submit"
        className="ymc-form__send-btn"
        onClick={() => this.handleClick()}
        >
          <FaPaperPlane size={24} />
        </button>
    );
  }
}


function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    ymmUpdateChatFree: async (partner_user_identifier) => await dispatch(ymmUpdateChatFree(partner_user_identifier)),
    updateChatFree: async (from_user_id, to_user_id, description) => await dispatch(updateChatFree(from_user_id, to_user_id, description)),
    unlockChat: async (male_id, female_id) => await dispatch(unlockChat(male_id, female_id)),
    unlockChatRoomInState: (chat_room_id) => {
      dispatch(unlockChatRoomInState(chat_room_id));
    },
    fetchChatList: () => dispatch(fetchChatList()),
    use_gift: async (gift_id, detail, to_user_id) => await dispatch(use_gift(gift_id, detail, to_user_id)),
    updateChatRoomFree: async (from_user_id, to_user_id, description) => await dispatch(updateChatRoomFree(from_user_id, to_user_id, description))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SendButton);
