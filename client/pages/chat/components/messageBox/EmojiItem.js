import React,  { Component } from 'react';

class EmojiItem extends Component {
  constructor() {
    super();

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const emojiShortname = this.emoji.getAttribute('data-emoji');
    this.props.enableKeyboad(true);
    this.props.insertEmoji(emojiShortname);
    this.props.updateMessage(true);
    this.props.textBoxhasValue(true);
  }

  render() {
    const props = this.props;

    return (
      <div
        className="ymc-emoji__item"
        data-emoji={props.emoji}
        onClick={this.handleClick}
        ref={(ref) => {
          this.emoji = ref;
        }}
      >
        {props.emoji}
      </div>
    );
  }
}

export default EmojiItem;
