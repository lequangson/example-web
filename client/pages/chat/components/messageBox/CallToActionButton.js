import React, { Component } from 'react';
import { connect } from 'react-redux';
import EmojiThumbsUp from 'emojione/assets/png/1f44d.png';
import getIdentifierFromId from '../../helpers/getIdentifierFromId';
import getUserIdFromIdentifier from '../../helpers/getUserIdFromIdentifier'
import { unlockChat, unlockChatRoomInState, use_gift, updateChatRoomFree } from '../../actions/Payment';
import { fetchChatList } from '../../actions/chatList';

const style = {
  display: 'block',
}

class CallToActionButton extends Component {
  constructor() {
    super();

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(evt) {
     const { valid_user, user, params, chatList } = this.props;
    if (!valid_user) {
      this.props.showPaymentPopup()
      return;
    }
    this.props.sendMessage(':thumbsup:');
    evt.preventDefault();
    this.props.enableKeyboad(true);
  }

  render() {
    return (
      <button
        type="button"
        className="ymc-form__cta-btn"
        onClick={this.handleClick}
      >
        <img src={EmojiThumbsUp} style={style} role="presentation" />
      </button>
    );
  }
}


function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    unlockChat: async (male_id, female_id) => await dispatch(unlockChat(male_id, female_id)),
    unlockChatRoomInState: (chat_room_id) => {
      dispatch(unlockChatRoomInState(chat_room_id));
    },
    fetchChatList: () => dispatch(fetchChatList()),
    use_gift: async (gift_id, detail, to_user_id) => await dispatch(use_gift(gift_id, detail, to_user_id)),
    updateChatRoomFree: async (from_user_id, to_user_id, description) => await dispatch(updateChatRoomFree(from_user_id, to_user_id, description))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CallToActionButton);
