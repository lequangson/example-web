import React, { Component } from 'react';
import { connect } from 'react-redux';
import EmojiGrinning from 'emojione/assets/png/1f600.png';
import EmojiJoy from 'emojione/assets/png/1f602.png';
import EmojiHeartEyes from 'emojione/assets/png/1f60d.png';
import EmojiSob from 'emojione/assets/png/1f62d.png';
import getIdentifierFromId from '../../helpers/getIdentifierFromId';
import getUserIdFromIdentifier from '../../helpers/getUserIdFromIdentifier'
import { unlockChat, unlockChatRoomInState, use_gift, updateChatRoomFree } from '../../actions/Payment';
import { fetchChatList } from '../../actions/chatList';

const style = {
  display: 'block',
  width: '45%',
  float: 'left',
}

class EmojiButton extends Component {
  constructor() {
    super();

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const { valid_user, user, params, chatList } = this.props;
    if (!valid_user) {
      this.props.showPaymentPopup()
      return;
    }
    this.props.toggleEmojiSelection();
    this.props.enableKeyboad(true);
  }

  render() {
    return (
      <button
        type="button"
        className={this.props.messageBox.emojiSelectionIsOpen ? 'ymc-form__emoji is-active' : 'ymc-form__emoji'}
        onClick={this.handleClick}
      >
        <img src={EmojiGrinning} style={style} role="presentation" />
        <img src={EmojiHeartEyes} style={style} role="presentation" />
        <img src={EmojiJoy} style={style} role="presentation" />
        <img src={EmojiSob} style={style} role="presentation" />
      </button>
    );
  }
}


function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    unlockChat: async (male_id, female_id) => await dispatch(unlockChat(male_id, female_id)),
    unlockChatRoomInState: (chat_room_id) => {
      dispatch(unlockChatRoomInState(chat_room_id));
    },
    fetchChatList: () => dispatch(fetchChatList()),
    use_gift: async (gift_id, detail, to_user_id) => await dispatch(use_gift(gift_id, detail, to_user_id)),
    updateChatRoomFree: async (from_user_id, to_user_id, description) => await dispatch(updateChatRoomFree(from_user_id, to_user_id, description))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EmojiButton);
