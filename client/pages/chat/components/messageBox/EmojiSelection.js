import React, { Component } from 'react';
import emojione from 'emojione/lib/js/emojione';
import EmojiItem from './EmojiItem';
import emojiCheatSheet from '../../constants/emojione';

class EmojiSelection extends Component {
  componentDidMount() {
    // convert emoji short name to image when DOM ready
    this.renderEmojiImage();
  }

  renderEmojiImage() {
    const emojiItems = document.querySelectorAll('.ymc-emoji__item');
    for (let i = 0; i < emojiItems.length; i += 1) {
      const e = emojiItems[i];
      // get emoji shortname
      const shortname = e.getAttribute('data-emoji');
      // const emojiImage = emojione.shortnameToImage(shortname);
      let emojiImage = '';
      if (typeof emojiCheatSheet.ymm_emoji[shortname] !== 'undefined') {
        emojiImage = '<img class="emojione" alt="' + shortname + '" src="' + emojiCheatSheet.ymm_emoji[shortname] + '"/>';
      } else {
        emojiImage = emojione.shortnameToImage(shortname);
      }

      e.innerHTML = emojiImage;
    }
  }

  render() {
    return (
      <div className="ymc-emoji">
        {emojiCheatSheet.people.map((emoji, index) => (
          <EmojiItem emoji={emoji} {...this.props} key={index} />
        ))}
      </div>
    );
  }
}

export default EmojiSelection;
