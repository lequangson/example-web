import React, { PropTypes } from 'react';
import DialogueChathead from './dialogue/DialogueChathead';
import DialogueContent from './dialogue/DialogueContent';
import { Divider } from './Divider';
import chatFullTime from '../helpers/chatFullTime';
import DialogueDefault from './dialogue/DialogueDefault'

const Dialouge = props => (
	<div>
		{/*props.id == 0 && !props.is_admin_support && <DialogueDefault avatarUrl={props.avatarUrl} message={props.message} />*/}
	  <div id={props.id} >
	    <Divider text={chatFullTime(props.created_at)} />
	    <div className="ymc-dialogue">
	      <DialogueChathead avatarUrl={props.avatarUrl} id={props.id}/>
	      <DialogueContent {...props} />
	    </div>
	  </div>
	</div>
);

Dialouge.propTypes = {
  time: PropTypes.string,
  avatarUrl: PropTypes.string,
};

export default Dialouge;
