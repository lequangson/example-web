import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import Modal from 'react-modal'
import FaArrowLeft from 'react-icons/lib/fa/arrow-left';
import FaInfoCircle from 'react-icons/lib/fa/info-circle';
import findChatRoomByIdentifier from '../helpers/findChatRoomByIdentifier';
import getUserIdFromIdentifier from '../helpers/getUserIdFromIdentifier';
import { CDN_URL } from '../constants/Enviroment'
import { fetchChatList, updateLastActivityTimeByIdentifier } from '../actions/chatList';
import * as ymmStorage from '../helpers/ymmStorage';
import {
  ignoreMessages
} from 'chat/actions/chatList';
const socket = require('socket.io-client')(process.env.REACT_APP_SOCKET_URL);


class AppTopBar extends Component {
  constructor() {
    super();
    this.state = {
      dropdownActive: false,
      isConfirmModalOpen: false
    }
    this.closeConfirmModal = this.closeConfirmModal.bind(this)
    this.toggleDropdownPanel = this.toggleDropdownPanel.bind(this)
  }

  toggleDropdownPanel() {
    this.setState({ dropdownActive: !this.state.dropdownActive })
  }

  openConfirmModal = () => {
    this.setState({ isConfirmModalOpen: true })
  }

  closeConfirmModal = () => {
    this.setState({ isConfirmModalOpen: false })
  }

  componentWillMount() {
    socket.on('check_status', this.socketCheckStatus.bind(this));
    socket.on('disconnect', this.socketDisconnect.bind(this));
  }

  socketCheckStatus(data) {
    this.props.updateLastActivityTimeByIdentifier(data.identifier);
  }
  socketDisconnect(data) {
    this.props.updateLastActivityTimeByIdentifier('00000000');
  }

  clickedDeleteMessage = async () => {
    await this.props.ignoreMessages(getUserIdFromIdentifier(this.props.params.id).toString());
    browserHistory.push('/chat-list');
  }
  render() {
    const { params, chatList } = this.props;
    if (!chatList.data.length) {
      return null;
    }
    const { id } = params;
    const chatRoom = findChatRoomByIdentifier(chatList, id);
    if (!chatRoom) {
      return null;
    }
    const { last_activity_date, end_match_at } = chatRoom;
    let isOnline = false;
    if (typeof last_activity_date !== 'undefined') {
      const last_activity_time = new Date(last_activity_date).getTime()
      if (new Date().getTime() - last_activity_time <= 15000) {
        isOnline = true;
      }
    }
    const isOnProfilePage = /profile/i.test(this.props.location.pathname)

    return (
      <div>
        <header className="ymc-topbar">
          <Link to={isOnProfilePage ? `/chat/${id}` : `/chat-list/${ymmStorage.getItem('type') ? ymmStorage.getItem('type') : 'default'}`} className="ymc-topbar__link mrm">
            <FaArrowLeft size={18} />
          </Link>
          <div className="ymc-topbar__content txt-center">
            <div>
              {!this.props.valid_user && <img className="img-lock mrs" src={`${CDN_URL}/chat_ui/Lock.png`} alt="Unlock Image" />}
              {getUserIdFromIdentifier(id) != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID && <span className="ymc-messages__meta mrs">
                  <i className="fa fa-circle" style={{'color': `${isOnline ? '#66ff66' : 'gray'}`}}/>
                </span>
              }
              {getUserIdFromIdentifier(id) != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID ? `${chatRoom.name} - ${chatRoom.age} tuổi` : 'Ymeet.me'}
            </div>
          </div>

          {/*<Link to={`/setting/${this.props.params.id}`} className="ymc-topbar__link">
            <FaInfoCircle size={18} />
          </Link>*/}
          {
            !isOnProfilePage && getUserIdFromIdentifier(id) != process.env.REACT_APP_CUSTOMER_SUPPORT_USER_ID &&
            <nav className="pt-nav">
              <ul className="pt-nav__lists">
                <li className="pt-nav__list has-dropdown">
                  <a className="txt-xs" onClick={this.toggleDropdownPanel}>
                    <i className="fa txt-white fa-circle mrx"></i>
                    <i className="fa txt-white fa-circle mrx"></i>
                    <i className="fa txt-white fa-circle mrx"></i>
                  </a>
                  <ul className={`pt-nav__dropdown-lists ${this.state.dropdownActive ? 'is-show' : '' }`}>
                    <li className="pt-nav__dropdown-list txt-right cursor">
                      <Link to={`/profile/${id}`}>
                        <i className="fa fa-user txt-blue padding-r5"></i>
                        Xem hồ sơ
                      </Link>
                    </li>
                    <li className="pt-nav__dropdown-list txt-right cursor">
                      <Link to={`/report/${id}`}>
                        <i className="fa fa-warning txt-warning padding-r5"></i>
                      Báo cáo</Link>
                    </li>
                    <li className="pt-nav__dropdown-list txt-right cursor">
                      <Link to={`/block-form/${id}`}>
                      <i className="fa fa-ban txt-red padding-r5"></i>
                      Chặn</Link>
                    </li>
                    <li className="pt-nav__dropdown-list txt-right cursor" onClick={() => {this.openConfirmModal();this.toggleDropdownPanel()}}>
                      <i className="fa fa-trash txt-red"></i>
                      <a className="pt-nav__dropdown-item padding-l5">Xóa trò chuyện</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          }
        </header>
        <div className="ymc-topbar__ghost"></div>
        <Modal
          isOpen={this.state.isConfirmModalOpen}
          onRequestClose={this.closeConfirmModal}
          className="modal__content modal__content--3"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="mbmd padding-t10 txt-center">
            <div className="mbl txt-lg txt-light">Xóa cuộc trò chuyện<br /> với {chatRoom.name}</div>
          </div>
          <div className="row padding-b10 mbm">
            <div className="col-xs-6 txt-right">
              <button className="btn mrm" onClick={this.closeConfirmModal} >
                <i className="fa fa-times"></i>
                <span className="padding-l5">Hủy</span>
              </button>
            </div>
            <div className="col-xs-6 txt-left">
              <button className="btn btn--blue mrm" onClick={this.clickedDeleteMessage} >
                <i className="fa fa-trash"></i>
                <span className="padding-l5">Xóa</span>
              </button>
            </div>
          </div>
          <button className="modal__btn-close" onClick={this.closeConfirmModal}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    );
  }
}

AppTopBar.propTypes = {
  params: PropTypes.object,
  messages: PropTypes.object,
};


function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    updateLastActivityTimeByIdentifier: (identifier) => dispatch(updateLastActivityTimeByIdentifier(identifier)),
    ignoreMessages: async (chat_room_ids) => await dispatch(ignoreMessages(chat_room_ids))
  };
}

const AppTopBarContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppTopBar);

export default AppTopBarContainer;
