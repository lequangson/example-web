import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { isEmpty } from 'lodash';
import $ from 'jquery';
import ListUsers from './chatList/ListUsers';
import SocketCom from './Socket';
import LoaderListUsers from './loaders/LoaderListUsers';
import Navigation from './navigation/Navigation';
import ReactTimeout from 'react-timeout'
import {
  CDN_URL, DEFAULT_LOAD_FEMALE, DEFAULT_LOAD_MALE,
  ANDROID_URL, IOS_URL
} from 'constants/Enviroment';
import { INTERVAL_TIME_GET_CHAT_LIST } from '../constants';
// import { subscribeUser } from '../helpers/userSubscription';
import HeaderContainer from 'containers/HeaderContainer';
import BannerDiscount from 'components/commons/BannerDiscount'
import BannerSwapSite from 'components/commons/BannerSwapSite'
import { BLOCK_TYPE_BLOCKED, GHOST_USER } from 'constants/userProfile'
import { getUserIdFromIdentifier } from 'utils/common'

const socket = require('socket.io-client')(process.env.REACT_APP_SOCKET_URL);

class ChatList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
      isMatchTypeQuick: false,
      selected_friend_ids: [],
      selected_checkbox: false
    }
    this.handleOnchange = this.handleOnchange.bind(this);
  }

  componentWillMount() {
    const { user, chatList } = this.props;
    if (isEmpty(user.data)) {
      // this.props.getUserProfile();
    }
    this.props.fetchChatList();
    socket.on('check_status', this.socketCheckStatus.bind(this));
    socket.on('disconnect', this.socketDisconnect.bind(this));
    if (chatList.data.length) {
      chatList.data.forEach(item => {
        socket.emit('join', { room: item.chat_room_id });
      })
      socket.on('new_msg', this.socketCheckNewMessage.bind(this));
    }
    // if (!this.props.user.isAskedPushNotiRequest) {
    //   subscribeUser(this.props)
    // }

    if (this.props.params.type == 'quick_match') {
      this.setState({ isMatchTypeQuick: true });
    }
  }

  componentDidMount() {
    window.scrollTo( 0, 0);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.chatList.data.length && nextProps.chatList.data.length) {
      const { chatList } = nextProps;
      chatList.data.forEach(item => {
        socket.emit('join', { room: item.chat_room_id });
      })
      socket.on('new_msg', this.socketCheckNewMessage.bind(this));
    }
    if (this.props.params.type != nextProps.params.type) {
      $(`.checkbox`).prop('checked', false);
      this.setState({ isEdit: false, selected_friend_ids: [], selected_checkbox: false })
    }
  }

  socketCheckNewMessage(data) {
    const { chatList } = this.props;
     if (window.location.href.indexOf('/chat/') > -1) { // does not recall api when user open chat room
      return;
    }
    // this.props.increaseChatListNumberMessage(data.msg.chat_room_id);
    this.props.fetchChatList();
  }

  socketCheckStatus(data) {
    this.props.updateLastActivityTimeByIdentifier(data.identifier);
  }
  socketDisconnect(data) {
    this.props.updateLastActivityTimeByIdentifier('00000000');
  }
  handleOnchange() {
    const inputKeyword = this.keyword.value.trim();
    sessionStorage.setItem('inputKeyword', inputKeyword);
    this.props.updateKeyWordFilter(inputKeyword);
  }

  clickEditMessages(isActive) {
    this.setState({
      isEdit: isActive
    });
  }

  renderBr() {
    if (window.innerWidth > 1200){
      return <br />
    }
    return;
  }

  renderRefundBanner() {
    const {current_user, random_match_users, matched_users} = this.props.user_profile;
    const { permission, gender, user_type } = current_user;
    if (typeof permission === 'undefined') {
      return <div/>;
    }
    const hasMatch = random_match_users.length > 0 || matched_users.length > 0  ? true : false
    const showBanner = hasMatch && gender == 'male' && permission.show_refund_banner && user_type !== 4 ? true : false
    if (!showBanner) {
      return <div/>;
    }

    return (
      <div className="row">
        <div className="col-xs-12">
          <Link
            style={{backgroundImage: `url(${CDN_URL}/general/Refund-banner-background.jpg)`, backgroundSize: '100% 100%', justifyContent: 'flex-end'}}
            className="banner mbm"
            to="/coin-instruction#refund"
          >
            <div className="banner__content banner__content--1 txt-white txt-center">
              <h3 className="banner__heading">{current_user.nick_name}</h3>
              <p className="banner__sub-heading txt-white txt-sm mbs">Ngại gì mà chưa mở chat với người mình thích? {this.renderBr()} Yên tâm! Bạn sẽ nhận lại 100 xu nếu không có hồi âm</p>
              <div className="btn btn--blue txt-xs txt-thin">Tìm hiểu thêm</div>
            </div>
          </Link>
        </div>
      </div>
    )
  }

  renderTabFilterMessages() {

    const { isMatchTypeQuick } = this.state;

    return (
      <div className="btn-ground cursor mbm">
        <Link className={`${isMatchTypeQuick ? '' : 'active'} btn-ground__btn txt-bold txt-light btn-ground__btn--left`}
          onClick={() => {
            this.setState({
              isMatchTypeQuick: false
            })
        }} to='/chat-list/default'>
          <i className="fa mrs fa-comments"></i>Kết đôi thường
        </Link>
        <Link className={`${isMatchTypeQuick ? 'active' : ''} btn-ground__btn txt-bold txt-light btn-ground__btn--right`}
          onClick={() => {
            this.setState({
              isMatchTypeQuick: true
            })
          }}
          to='/chat-list/quick_match'
        >
          <img className="img-supperlike" src={`${CDN_URL}/super_like/${isMatchTypeQuick ? 'Heart_white.png' : 'Heart_gray.png'}`} alt="Kết đôi nhanh" />Kết đôi nhanh
        </Link>
      </div>
    )
  }

  selectedAll = () => {
    const type = this.props.params.type || 'default';
    const { chatList } = this.props;
    const friend_ids = chatList.data.filter(user =>
      (
        (user.match_type == 3 && type == 'quick_match') ||
        (user.match_type != 3 && type == 'default')
      )
    ).map(item => getUserIdFromIdentifier(item.identifier));

    if (!this.state.selected_checkbox) {
      this.setState({ selected_checkbox: true , selected_friend_ids: friend_ids});
    } else {
      this.setState({ selected_checkbox: false, selected_friend_ids: []});
    }
  }

  selectedItem = (item) => {
    if (this.state.selected_friend_ids.length && this.state.selected_friend_ids.map(item => parseInt(item)).indexOf(item) > -1) {
      const newListIds = this.state.selected_friend_ids.filter(x => x != item);
      this.setState({ selected_friend_ids: newListIds });
      // this.setState({selected_friend_ids: this.state.selected_friend_ids.filter(rc => rc != item)});
      this.forceUpdate();
    } else {
      const newListIds = this.state.selected_friend_ids;
      this.setState({ selected_friend_ids: [...newListIds, item] });
      // this.setState({selected_friend_ids: this.state.selected_friend_ids.filter(rc => rc != item)});
      this.forceUpdate();
    }
  }

  clickedDeleteMessage = async () => {
    await this.props.ignoreMessages(this.state.selected_friend_ids.toString());
    this.props.fetchChatList();
    this.setState({ isEdit: false, selected_friend_ids: [], selected_checkbox: false });
  }

  render() {
    const campaign = this.props.campaign.data;
    const showSaleoffBanner = !_.isEmpty(campaign);
    const { chatList, params, user_profile, Enviroment } = this.props;
    const { has_message_from_admin, current_user } = user_profile;
    const { gender } = current_user;
    const isPaidUser = current_user && current_user.user_type === 4;
    const default_image = gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE
    let type = 'default';
    if (typeof params.type !== 'undefined') {
      type = params.type;
    }
    let { keyWordFilter } = chatList;
    if (!keyWordFilter.length) {
      keyWordFilter = sessionStorage.getItem('inputKeyword') || '';
    }
    const data = chatList.data.filter((user) => {
      return user.name.toLowerCase().includes(keyWordFilter.toLowerCase()) && (
        (user.match_type == 3 && type == 'quick_match') ||
        (user.match_type != 3 && type == 'default')
      ) && user.is_hidden != true
    });
    const isShowLoader = !this.state.isMatchTypeQuick && !data.length;
    // try{
    //   if (!localStorage.getItem('ymm_token')) {
    //     return <div></div>
    //   }
    // } catch(e) {}

    // sorting in descending by number_new_message
    // data.sort(function(b, a) {
    //   return parseFloat(a.number_new_message) - parseFloat(b.number_new_message);
    // });
    return (
      <div className="site-content">
        <HeaderContainer {...this.props} />
        <SocketCom />
        <div className="container">
          <BannerSwapSite />
          {showSaleoffBanner === false && this.renderRefundBanner()}
          {showSaleoffBanner &&
            <BannerDiscount
              {...this.props}
              timeStop={campaign.end_at}
              isPaidUser={isPaidUser}
              url={"/payment/upgrade"}
            />
          }
          {this.renderTabFilterMessages()}
          <header className="ymc-topbar--1">
              <div className="ymc-form__group-inline">
                <input
                  className="ymc-form__input ymc-radius"
                  type="search"
                  placeholder="Tìm người trong danh sách chat"
                  ref={(keyword) => { this.keyword = keyword; }}
                  onChange={this.handleOnchange}
                  defaultValue={keyWordFilter}
                />
                <button
                  className="ymc-form__cta-btn ymc-form__input-addon"
                  type="button"
                >
                  <i className="fa fa-search"></i>
                </button>
              </div>
          </header>
            {!!data.length && <div className="edit-messages padding-t10 padding-r10 padding-b10 txt-blue">
                {
                  this.state.isEdit ?
                  <div className="txt-right cursor">
                    <button className="btn mrm" onClick={() => {this.clickEditMessages(false)}}>
                      <span className="padding-l5">Hủy</span>
                    </button>
                    <button className="btn btn--blue mrm" onClick={this.clickedDeleteMessage} >
                      <i className="fa fa-trash"></i>
                      <span className="padding-l5">Xóa</span>
                    </button>
                    <label className="form__input--d l-inline-block padding-0 width-auto">
                      <input type="checkbox" name="message_ids" value={1} className="checkbox" onClick={this.selectedAll}/>
                      <div className="fake-checkbox"></div>
                    </label>
                  </div>
                  :
                  <div className="txt-right cursor" onClick={() => {this.clickEditMessages(true)}}>
                    <i className="fa fa-edit"></i>
                    <span className="padding-l5">Chỉnh sửa</span>
                  </div>
                }
              </div>
            }
          {
            this.state.isMatchTypeQuick && !data.length && <div className="row padding-t10">
              <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                <article className="mbl mc">
                  <div className="card__upper">
                    <div href="#" className="card__link mbm">
                      <img src={default_image} alt="" className="card__img"/>
                    </div>
                    <div className="txt-center txt-light txt-lg mbm">
                      <p className="txt-bold">Khi bạn sử dụng tính năng<br />
                      <img src={`${CDN_URL}/super_like/Heart_pink.png`} alt="Heart pink" className="img-supperlike"/>"Chat luôn không cần đợi thích"<br />
                      với thành viên nào, thành viên<br />
                      đó sẽ được hiển thị ở đây, nơi<br />
                      bạn có thể trò truyện.
                      </p>
                    </div>
                  </div>
                </article>
              </div>
            </div>
          }

          {
            !this.state.isMatchTypeQuick && !data.length && <div className="row padding-t10">
              <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
                <article className="mbl mc">
                  <div className="card__upper">
                    <div href="#" className="card__link mbm">
                      <img src={default_image} alt="" className="card__img"/>
                    </div>
                    <div className="txt-center txt-light txt-lg mbm">
                      <p className="txt-bold">Danh sách kết đôi sẽ được hiển thị ở đây

                      </p>
                    </div>
                  </div>
                </article>
              </div>
            </div>
          }

          {isShowLoader ? (
            <LoaderListUsers />
          ) : (
            <ListUsers
              isEdit={this.state.isEdit}
              selectedItem={this.selectedItem}
              selected_friend_ids={this.state.selected_friend_ids}
              chatList={chatList}
              data={data}
              params={this.props.params}
            />
          )}
          {showSaleoffBanner && this.renderRefundBanner()}
        </div>
      </div>
    );
  }
}

ChatList.propTypes = {
  fetchChatList: PropTypes.func,
  chatList: PropTypes.object,
};

export default ReactTimeout(ChatList)
