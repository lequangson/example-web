import React from 'react';
import SettingListItem from './SettingListItem';

const SettingList = () => (
  <ul className="ymc-list-unstyled">
    <li>
      <div className="ymc-media">
        <div className="ymc-media__item ymc-txt-bold ymc-txt-color-info ymc-txt-large">Cài đặt</div>
      </div>
    </li>
    <SettingListItem
      text="Bật thông báo"
      statusText="Bật"
      iconClass="fa-bell"
    />
    <SettingListItem
      text="Xem hồ sơ cá nhân"
      iconClass="fa-user"
    />
    <SettingListItem
      text="Chặn tài khoản"
      statusText="Chặn thông báo"
      iconClass="fa-ban"
    />
  </ul>
);

export default SettingList;
