import React, { PropTypes } from 'react';

const SettingListItem = props => (
  <li>
    <a className="ymc-media" href="#">
      <div className="ymc-media__item mrmd">
        <i className={`fa ${props.iconClass} ymc-txt-color-info`}></i>
      </div>
      <div className="ymc-media__item">
        <div>
          <span className="ymc-txt-bold ymc-txt-large">{props.text}</span>
          <br />
          <span className="ymc-txt-muted">{props.statusText}</span>
        </div>
      </div>
    </a>
  </li>
);

SettingListItem.propTypes = {
  iconClass: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  statusText: PropTypes.string,
};

export default SettingListItem;
