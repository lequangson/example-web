import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import { Link } from 'react-router'
import { PAYMENT_MODAL_IMAGE } from '../constants/index'

class PaymentModal extends Component {
  constructor() {
    super()
    this.state = {
      modalIsOpen: false,
    }

    this.modalOpen = this.modalOpen.bind(this)
    this.modalClose = this.modalClose.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    this.modalClose()
  }

  modalOpen() {
    this.setState({ modalIsOpen: true })
  }

  modalClose() {
    this.setState({ modalIsOpen: false })
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        className="modal__content modal__content--2"
        overlayClassName="modal__overlay"
        portalClassName="modal"
        contentLabel=''
      >
        <Link
          target="blank"
          to={`${process.env.REACT_APP_YMM_MOBILE_URL}payment/upgrade`}
          className="modal__content__frame"
          onClick={this.handleClick}
        >
          <img src={PAYMENT_MODAL_IMAGE} alt="PaymentModal" />
        </Link>
        <button
          className="modal__btn-close"
          onClick={this.props.onRequestClose}
        >
          <i className="fa fa-times"></i>
        </button>
      </Modal>
    )
  }
}

PaymentModal.propTypes = {
  modalShowNext: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
  }
}

const PaymentModalContainer = connect(mapStateToProps, mapDispachToProps)(PaymentModal);

export default PaymentModalContainer