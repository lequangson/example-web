import React from 'react';

const NotiWarning = props => (
  <div className="ymc-panel ymc-panel--warning mbm">
    <div className="ymc-panel__header">
      <h3 className="ymc-panel__title">{props.title}</h3>
    </div>
    <div className="ymc-panel__body">
      {props.children}
    </div>
  </div>
);

export default NotiWarning;
