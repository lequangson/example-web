import React from 'react';

const ButtonFacebookLoader = () => (
  <button className="btn btn--fb btn--block is-loading mbmd" disabled>
    <i className="fa fa-spinner mrs"></i>
    Đang xác thực
  </button>
);

export default ButtonFacebookLoader;
