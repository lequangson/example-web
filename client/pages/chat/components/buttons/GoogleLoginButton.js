import React, { PropTypes } from 'react';
import { CDN_URL } from '../../constants/index';

class GoogleLoginButton extends React.Component {

  click = () => {
    window.location.href = process.env.REACT_APP_YMM_URL;
  };

  renderWithFontAwesome() {
    return (
      <button
        className="btn btn--gg btn--block mbmd"
        onClick={this.click}
      >
        <div className="l-flex-center l-flex--ac">
          <div className="icon-bg">
            <img src={`${CDN_URL}/general/LandingPageNew/Google.png`} alt="Đăng nhập bằng google"/>
          </div>
          <div className="txt-left padding-l20">
            <span className="txt-h4">Đăng nhập bằng Google</span>
          </div>
        </div>
      </button>
    );
  }

  render() {
    return this.renderWithFontAwesome();
  }
}

GoogleLoginButton.propTypes = {
}

GoogleLoginButton.defaultProps = {
}

export default GoogleLoginButton;