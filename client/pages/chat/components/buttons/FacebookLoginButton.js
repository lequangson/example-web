import React, { PropTypes } from 'react';
import { CDN_URL } from '../../constants/index';

class FacebookLoginButton extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
      FBLoaded: false, // prevent user click before FB initialize successfully
      responsedAutoLoad: false,
    }
  }

  componentWillMount() {
    if (typeof FB !== 'undefined') {
      this.setState({ FBLoaded: true })
    }
  }
  componentDidMount() {
    const { appId, xfbml, cookie, version, autoLoad, language } = this.props;
    const fbRoot = document.createElement('div');
    fbRoot.id = 'fb-root';

    document.body.appendChild(fbRoot);

    window.fbAsyncInit = () => {
      window.FB.init({
        version: `v${version}`,
        appId,
        xfbml,
        cookie,
        status,
      });

      if (autoLoad || window.location.search.includes('facebookdirect')) {
        window.FB.getLoginStatus(this.checkLoginState);
      }
      this.setState({ FBLoaded: true })
    };
    // Load the SDK asynchronously
    ((d, s, id) => {
      const element = d.getElementsByTagName(s)[0];
      const fjs = element;
      let js = element;
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = `//connect.facebook.net/${language}/all.js`;
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');
  }

  responseApi = (authResponse) => {
    this.setState({ clicked: false, responsedAutoLoad: true })
    window.FB.api('/me', { fields: this.props.fields }, (me) => {
      Object.assign(me, authResponse);
      this.props.callback(me);
      this.props.facebookLogin(me);
    });
  };

  checkLoginState = (response) => {
    if (response.authResponse) {
      this.responseApi(response.authResponse);
    } else {
      if (this.props.callback) {
        this.setState({ clicked: false, responsedAutoLoad: true })
        this.props.callback({ status: response.status, 'props': this.props });
        this.props.facebookLogin({ status: response.status, 'props': this.props });
      }
    }
  };

  click = () => {
    if (this.state.clicked || !this.state.FBLoaded || (this.props.autoLoad && !this.state.responsedAutoLoad)) {
      return
    }
    this.setState({clicked: true})
    const { scope, appId } = this.props;
    if (navigator.userAgent.match('CriOS')) {
      window.location.href = `https://www.facebook.com/dialog/oauth?client_id=${appId}&redirect_uri=${window.location.href}&state=facebookdirect&${scope}&auth_type=rerequest`;
    } else {
      try{
        window.FB.login(this.checkLoginState, { scope, auth_type: 'rerequest' });
      } catch(e) {
        console.log('error')
      }
    }
  };

  renderWithFontAwesome() {
    return (
      <button
        className="btn btn--fb btn--block mbmd"
        onClick={this.click}
      >
        <div className="l-flex-center l-flex--ac">
          <div className="icon-bg">
            <img src={`${CDN_URL}/general/LandingPageNew/Facebook-white.png`} alt="Đăng nhập bằng facebook"/>
          </div>
          <div className="txt-left padding-l20">
            <span className="txt-h4">Đăng nhập bằng Facebook</span>
          </div>
        </div>
      </button>
    );
  }

  render() {
    return this.renderWithFontAwesome();
  }
}

FacebookLoginButton.propTypes = {
  callback: PropTypes.func.isRequired,
  appId: PropTypes.string.isRequired,
  xfbml: PropTypes.bool,
  cookie: PropTypes.bool,
  scope: PropTypes.string,
  textButton: PropTypes.string,
  autoLoad: PropTypes.bool,
  size: PropTypes.string,
  fields: PropTypes.string,
  cssClass: PropTypes.string,
  version: PropTypes.string,
  icon: PropTypes.string,
  language: PropTypes.string,
}

FacebookLoginButton.defaultProps = {
  textButton: 'Login with Facebook',
  scope: 'email, user_birthday, public_profile, user_friends, user_photos, user_relationships',
  // scope: 'email, user_birthday, user_friends, user_relationships, user_photos',
  xfbml: false,
  cookie: false,
  size: 'metro',
  fields: 'name',
  cssClass: 'kep-login-facebook',
  version: '2.3',
  language: 'en_US',
}

export default FacebookLoginButton;
