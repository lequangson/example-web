import React from 'react';

const ButtonFacebookLogin = props => (
  <button
    className="btn btn--fb btn--block mbm"
    onClick={props.onClick}
  >
    <i className="fa fa-facebook-square mrs"></i>
    Đăng nhập bằng Facebook
  </button>
);

export default ButtonFacebookLogin;
