import React, { Component } from 'react';
import SendButton from './messageBox/SendButton';
import CallToActionButton from './messageBox/CallToActionButton';
import EmojiButton from './messageBox/EmojiButton';
import TextBox from './messageBox/TextBox';

class MessageInput extends Component {
  constructor() {
    super();

    this.formSubmit = this.formSubmit.bind(this);
  }

  formSubmit(evt) {
    evt.preventDefault();
  }

  render() {
    const props = this.props;
    const msgBox = props.messageBox;
    return (
      <form
        className="ymc-form"
        action="/"
        onSubmit={this.formSubmit}
      >
        <TextBox {...props} ref={(input) => { this.textInput = input; }} />

        <EmojiButton {...props} />

        {msgBox.textBoxhasValue ? (
          <SendButton {...props} />
        ) : (
          <CallToActionButton {...props} />
        )}
      </form>
    );
  }
}

export default MessageInput;
