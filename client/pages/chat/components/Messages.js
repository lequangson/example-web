import React, { PropTypes } from 'react';
import Message from './messages/Message';

const Messages = props => (
  <div className="ymc-messages">
    <div className="ymc-messages__content">
      <Message text={props.message} read_at={props.read_at} />
    </div>
  </div>
);

Messages.propTypes = {
  message: PropTypes.string,
};

export default Messages;
