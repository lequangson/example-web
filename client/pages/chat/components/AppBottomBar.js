import React, { PropTypes } from 'react';
import MessageInput from './MessageInput';
import EmojiSelection from './messageBox/EmojiSelection';

const AppBottomBar = props => (
  <div>
    <section className="ymc-bottombar">
      <MessageInput {...props} />
      {props.messageBox.emojiSelectionIsOpen && (
        <EmojiSelection {...props} />
      )}
    </section>
    {props.messageBox.emojiSelectionIsOpen && (
      <div className="ymc-emoji__ghost" />
    )}
    <div className="ymc-bottombar__ghost"></div>
  </div>
);

AppBottomBar.propTypes = {
  messageBox: PropTypes.object,
};

export default AppBottomBar;
