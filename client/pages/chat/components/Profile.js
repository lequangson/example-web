import React, { PropTypes } from 'react'
import _ from 'lodash'
import { browserHistory } from 'react-router'
import ProfileNavigation from './Profile/ProfileNavigation'
import getUserIdFromIdentifier from '../helpers/getUserIdFromIdentifier';
import * as ymmStorage from '../helpers/ymmStorage';
import { defaultAvatarObject } from '../helpers/showDefaultAvatar'

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pictureInfo: {
        index: 0,
      },
    }
  }

  componentWillMount() {
    const identifier= this.props.params.id
    const index = this.props.user.selected_user.findIndex(user => getUserIdFromIdentifier(identifier) === getUserIdFromIdentifier(user.identifier))
    if (index == -1) {
      this.props.getDetailUserProfile(identifier).then(() => {
        const user = this.props.user.selected_user.find(user => getUserIdFromIdentifier(identifier) == getUserIdFromIdentifier(user.identifier)) || {}
        const userPictures = user.user_pictures
        this.setState({
          pictureInfo: {
            index: 0,
            pictureUrl: userPictures.length ? userPictures[0].large_image_url : defaultAvatarObject(user.gender, 'large').large_image_url
          },
        })
      })
    }
    else {
      const user = this.props.user.selected_user.find(user => getUserIdFromIdentifier(identifier) == getUserIdFromIdentifier(user.identifier)) || {}
      const userPictures = user.user_pictures
      this.setState({
        pictureInfo: {
          index: 0,
          pictureUrl: userPictures.length ? userPictures[0].large_image_url : defaultAvatarObject(user.gender, 'large').large_image_url
        },
      })
    }
  }

  componentDidMount() {
    window.scroll(0, 0)
  }

  setCarouselOffsetBottom(value) {
    this.setState({
      carouselOffsetBottom: value,
    })
  }

  nextPicture_OnClick(pictureIndex, stepIndex) {
    const user = this.props.user.selected_user.find(user => getUserIdFromIdentifier(this.props.params.id) == getUserIdFromIdentifier(user.identifier)) || {}
    const userPictures = user.user_pictures
    const picture = userPictures[pictureIndex + stepIndex]
    this.setState({
      pictureInfo: {
        index: pictureIndex + stepIndex,
        pictureUrl: picture.large_image_url,
        // is_main: picture.is_main,
      },
    })
  }

  render() {
    const identifier = this.props.params.id
    const user = this.props.user.selected_user.find(user => getUserIdFromIdentifier(identifier) == getUserIdFromIdentifier(user.identifier)) || {}
    const userPictures = !_.isEmpty(user) ? user.user_pictures : []
    const pictureIndex = this.state.pictureInfo.index
    const nextPicture = typeof pictureIndex !== 'undefined' ? userPictures[pictureIndex + 1] : ''
    const prevPicture = typeof pictureIndex !== 'undefined' ? userPictures[pictureIndex - 1] : ''

    return (
      <div>
        <div className="col padding-t10">
          <div className="card mbmd">
            <div className="card__link">
              <img src={this.state.pictureInfo.pictureUrl} alt="ymc-radius" className="ymc-radius"/>
              <div>
                <button
                  className={`pagi pagi__prev pagi__prev--1 pagi--2${prevPicture ? '' : ' is-hide'}`}
                  onClick={this.nextPicture_OnClick.bind(this, pictureIndex, -1)}
                >
                  <i className="fa fa-chevron-left"></i>
                </button>
                <button
                  className={`pagi pagi__next pagi__next--1 pagi--2${nextPicture ? '' : ' is-hide'}`}
                  onClick={this.nextPicture_OnClick.bind(this, pictureIndex, 1)}
                >
                  <i className="fa fa-chevron-right"></i>
                </button>
              </div>
            </div>
          </div>
          {
            user.self_introduction &&
            <div className="well mbmd">{user.self_introduction}</div>
          }
          <ProfileNavigation user = {user}/>
        </div>
        {/*<ProfileNavigation />*/}
      </div>
    )
  }
}


Profile.propTypes = {
}

export default Profile;
