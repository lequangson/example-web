import React, { PropTypes } from 'react';
import MessageHasEmoji from './MessageHasEmoji';
import MessageNotHasEmoji from './MessageNotHasEmoji';
import isMessageHasEmoji from '../../helpers/isMessageHasEmoji';

const Message = ({ text, read_at }) => (
  <div className="row justify-content-start">
    <div className="col-xs-11">
      <div className="ymc-messages__msg">
        {isMessageHasEmoji(text) ? (
          <MessageHasEmoji text={text} />
        ) : (
          <MessageNotHasEmoji text={text} />
        )}

        {read_at == null && <div className="ymc-messages__meta">
          <i className="fa fa-circle ymc-txt-color-info" />
        </div>}
      </div>
    </div>
  </div>
);

Message.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Message;
