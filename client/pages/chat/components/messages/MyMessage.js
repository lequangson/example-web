import React, { Component, PropTypes } from 'react';
import MessageHasEmoji from './MessageHasEmoji';
import MessageNotHasEmoji from './MessageNotHasEmoji';
import isMessageHasEmoji from '../../helpers/isMessageHasEmoji';
import { CDN_URL } from '../../constants/Enviroment'


class MyMessage extends Component {
  render() {
    const { text, total_msg, is_sent, index, chatRoom, is_valid_refund, openRefundModal} = this.props;
    return (
      <div className="ymc-messages__msg">
        {is_valid_refund && index == total_msg -1 && <button className="ymc-form__cta-btn mrmd cursor" onClick={openRefundModal}>
          <img src={`${CDN_URL}/chat_ui/refund-icon.png`} alt="refund-icon"/>
        </button>}
        {isMessageHasEmoji(text) ? (
          <MessageHasEmoji text={text} />
        ) : (
          <MessageNotHasEmoji text={text} />
        )}
        {/*<div className="ymc-messages__meta">
          <img
            className="ymc-messages__meta__img"
            src="https://s3.amazonaws.com/uifaces/faces/twitter/jina/128.jpg"
            role="presentation"
          />
        </div> */}
          {index == total_msg - 1 && is_sent && <div className="ymc-c-f__fix-pos ymc-c-f__fix-pos--right">
              <div className="ymc-mesages ymc-txt-muted">
                <i className="fa fa-check mrs"></i>
                Đã xem
              </div>
            </div>
          }
      </div>
    );
  }
}

MyMessage.propTypes = {
  text: PropTypes.string,
};

export default MyMessage;

// old message
/* <div className="ymc-messages__msg">
  <div className="ymc-messages__msg__content">mang theo đồ ăn cho khách</div>
  <div className="ymc-messages__meta"></div>
</div> */

// sent
/* <div className="ymc-messages__msg">
  <div
    className="ymc-messages__msg__content"
  >lấy hết đồ ăn ra khỏi bồn, đèn sẽ tắt và khách có thể thưởng thức món ăn.</div>
  <div className="ymc-messages__meta"><i className="fa fa-check-circle-o"></i></div>
</div> */

// received
/* <div className="ymc-messages__msg">
  <div className="ymc-messages__msg__content">Thực khách</div>
  <div className="ymc-messages__meta"><i className="fa fa-check-circle"></i></div>
</div> */
