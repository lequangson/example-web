import React, { PropTypes } from 'react';
import emojione from 'emojione';
import emojiCheatSheet from '../../constants/emojione';


const MessageHasEmoji = ({ text }) => {
	let formatMsg = emojione.shortnameToImage(text.replace(/(?:\r\n|\r|\n)/g, '<br />'));
	let hasSticker = false;
	emojiCheatSheet.ymm_short_names.forEach(ymm_short_name => {
		// formatMsg = formatMsg.replace(ymm_short_name, '<img class="emojione" alt="' + ymm_short_name + '" src="' + emojiCheatSheet.ymm_emoji[ymm_short_name] + '"/>');
		while(formatMsg.indexOf(ymm_short_name) > -1) {
			formatMsg = formatMsg.replace(ymm_short_name, '<img class="sticker" alt="ymm sticker" src="' + emojiCheatSheet.ymm_emoji[ymm_short_name] + '"/>');
			hasSticker = true;
		}
	})
	return <div
	    className={!hasSticker ? 'ymc-messages__msg__content' : ''}
	    dangerouslySetInnerHTML={{
	      __html: formatMsg,
	    }}
	  />
};


MessageHasEmoji.propTypes = {
  text: PropTypes.string,
};

export default MessageHasEmoji;
