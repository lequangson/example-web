import React from 'react';
{/*            <p dangerouslySetInnerHTML={{ __html: this.props.user.self_introduction ? this.props.user.self_introduction.replace(/(?:\r\n|\r|\n)/g, '<br />') : '' }}></p>  */}
  {/* <div className="ymc-messages__msg__content">{text.replace(/(?:\r\n|\r|\n)/g, '<br />')}</div> */ }
const MessageNotHasEmoji = ({ text }) => (

  <div
    className="ymc-messages__msg__content"
    dangerouslySetInnerHTML={{ __html: text ? text.replace(/(?:\r\n|\r|\n)/g, '<br />') : '' }}
  >
  </div>
);

export default MessageNotHasEmoji;
