import React from 'react';
import { browserHistory } from 'react-router';
import FaArrowLeft from 'react-icons/lib/fa/arrow-left';

const AppTopBarSetting = () => (
  <div>
    <header className="ymc-topbar">
      <button
        className="ymc-topbar__link mrlg"
        onClick={browserHistory.goBack}
      >
        <FaArrowLeft size={18} />
      </button>
      <div className="ymc-topbar__content">Chi tiết</div>
    </header>
  </div>
);

export default AppTopBarSetting;
