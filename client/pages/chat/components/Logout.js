import React from 'react'
import { browserHistory } from 'react-router'
import cookie from 'react-cookie';
import * as ymmStorage from '../helpers/ymmStorage';

class Logout extends React.Component {
  componentWillMount() {
    if (ymmStorage.getItem('ymm_token')) {
      ymmStorage.clear()
      browserHistory.push('/')
    } else {
      browserHistory.push('/')
    }
  }
  render() {
    return (<div></div>)
  }
}

export default Logout;
