import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as sendCampaignNotification from '../helpers/notifications/campaign';
import * as ymmStorage from '../helpers/ymmStorage';
import { CAMPAIGN_HAPPY_HOURS } from '../constants/Campaign';
import { fetchChatList } from '../actions/chatList';
const socket = require('socket.io-client')(process.env.REACT_APP_SOCKET_URL);

class Socket extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    socket.off('in_campaign');
    socket.on('in_campaign', this.inCampaign.bind(this))
    socket.off('end_campaign');
    socket.on('end_campaign', this.endCampaign.bind(this))
    socket.off('receive_event');
    socket.on('receive_event', this.receive_event.bind(this))
  }

  receive_event(event) {
    if (event.type == 'force-refresh') {
      location.reload();
    }
  }

  inCampaign(campaign) {
    // sendCampaignNotification.startHappyHours();
    if (campaign.type == CAMPAIGN_HAPPY_HOURS && !ymmStorage.getItem('startHappyHours')) {
      sendCampaignNotification.startHappyHours();
      let expired_time = new Date();
      expired_time.setHours(expired_time.getHours() + 2);// expired time in 2 hours
      setTimeout(function(){ ymmStorage.setItem('startHappyHours', 1, expired_time); }, 5000);
      // the following like for test
      ymmStorage.removeItem('stopHappyHours');
      // Do everything when starts campaign
      this.props.fetchChatList()
    }
  }

  endCampaign(campaign) {
    if (campaign.type == CAMPAIGN_HAPPY_HOURS) {
      sendCampaignNotification.stopHappyHours();
      let expired_time = new Date();
      expired_time.setHours(expired_time.getHours() + 2);// expired time in 2 hours
      // Do everything when end campaign
      this.props.fetchChatList()
    }
  }

  render() {
    return (
      <div></div>
    );
  }
}

Socket.propTypes = {
};

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    fetchChatList: () => {
      dispatch(fetchChatList());
    },
  };
}

const SocketContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Socket);

export default SocketContainer;