import React, { PropTypes } from 'react';
import './_assets/styles/main.css';
import '../public/styles/slick.css';
import 'babel-polyfill';

const App = props => (
  <div className="backdrop">
    <div className="container-detail">
      {props.children}
    </div>
  </div>
);

App.propTypes = {
  children: PropTypes.node.isRequired,
};

export default App;
