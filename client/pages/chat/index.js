/* global document */
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promise from 'redux-promise';
import createLoger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
// import Raven from 'raven-js';
import rootReducer from './reducers';
import App from './App';
import ChatListPage from './pages/ChatListPage';
import ChatPage from './pages/ChatPage';
import LoginPage from './pages/LoginPage';
import SettingPage from './pages/SettingPage';
import Profile from './pages/Profile'
import PageNotFound from './pages/404';
import Logout from './components/Logout';
import api from './middleware/api';
import { setLastTimeActivity } from './actions/user';
import { updateLastActivityTimeByIdentifier, fetchChatList} from './actions/chatList';
import { INTERVAL_TIME_SET_LAST_ACTIVITY, INTERVAL_TIME_SEND_STATUS } from './constants';
import * as ymmStorage from './helpers/ymmStorage';
import getIdentifierFromId from './helpers/getIdentifierFromId';
// Raven.config(process.env.REACT_APP_RAVEN).install()
const loggerMiddleware = createLoger();
const socket = require('socket.io-client')(process.env.REACT_APP_SOCKET_URL);
const middlewares = [
  promise,
  api,
  thunkMiddleware,
  // process.env.NODE_ENV !== 'production' && loggerMiddleware,
].filter(Boolean);

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(...middlewares)),
);
if (ymmStorage.getItem('ymm_token')) {
  store.dispatch(fetchChatList()).then(res => {
    store.dispatch(setLastTimeActivity());
  })
  setInterval(() => {
    store.dispatch(setLastTimeActivity());
  }, INTERVAL_TIME_SET_LAST_ACTIVITY);
}

setInterval(() => {
  if (ymmStorage.getItem('ymm_token')) {
    store.dispatch(updateLastActivityTimeByIdentifier('000000000000')); //fore refresh online status every 15 seconds
    const current_state = store.getState();
    const current_user_identifier = getIdentifierFromId(current_state.user.data._id);
    const chatList = current_state.chatList;
    if (chatList.data.length) {
      chatList.data.forEach(item => {
        // send status to list friend every 15 seconds
        socket.emit('send_status', { room: item.chat_room_id, identifier: current_user_identifier });
      })
    }
  }
}, INTERVAL_TIME_SEND_STATUS);

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={LoginPage} />
        <Route path="chat/:id" component={ChatPage} />
        <Route path="setting/:id" component={SettingPage} />
        <Route path="login" component={LoginPage} />
        <Route path="logout" component={Logout} />
        <Route path="chat-list(/:type)" component={ChatListPage} />
        <Route path="profile/:id" component={Profile} />
        <Route path="*" component={PageNotFound} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root'),
);
