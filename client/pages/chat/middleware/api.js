/**
 *
 * API middleware for call api
 * refer the following comments to understand how to use in redux action
 */
// export const testApi = () => {
//   return {
//     [CALL_CHAT_API]: {
//       url: 'users',
//       method: 'GET',
//       authenticated: true,
//       types: ['REQUEST', 'SUCCESS', 'FAILURE'],
//       data: {}, // for post, put
//       params: {} // for get
//     }
//   }
// }
import axios from 'axios';
import cookie from 'react-cookie';
import { HTTP_FORBIDDEN_CODE } from 'chat/constants/errorCode';
import { AUTH_YMM_TOKEN_NAME } from 'chat/constants/index';
import * as ymmStorage from 'chat/helpers/ymmStorage';

// const API = axios.create({ baseURL: process.env.REACT_APP_API_URL });
const API = axios.create({ baseURL: process.env.REACT_APP_CHAT_API_URL });

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_CHAT_API = Symbol('Call Chat API');

// A Redux middleware that interprets actions with CALL_CHAT_API info specified.
// Performs the call and promises when such actions are dispatched.
export default store => next => action => { // eslint-disable-line
    const callAPI = action[CALL_CHAT_API];

    if (typeof callAPI === 'undefined') {
        return next(action);
    }

    API.interceptors.response.use(response => (
        Promise.resolve(response)
    ), error => {
        if (error.response.status === HTTP_FORBIDDEN_CODE) {
          // should clear client data here because of security reason
          ymmStorage.clear();
          window.location.href = '/';
          return;
        }
        return Promise.reject(error);
    });

    let { method, url, types, authenticated, data, headers, params, custom_params } = callAPI;
    if (data) {
      data = Object.assign({}, data, {
        platform: 'web'
      });
    }

    if (params) {
      params = Object.assign({}, params, {
        platform: 'web'
      });
    }

    method = method || 'GET';
    authenticated = authenticated || false;

    const [requestType, successType, errorType] = types;
    const token = ymmStorage.getItem(AUTH_YMM_TOKEN_NAME) || null;
    const config = {
        method,
        url,
        data,
        params
    };

    if (authenticated) {
        if (token) config.headers = { Authorization: `Bearer ${token}` };
    }

    if (headers) {
        //config.headers = headers;
        config.headers = { Authorization: `Bearer ${token}`, 'Content-Type': 'application/x-www-form-urlencoded' };
    }

    next({ type: requestType });
    return API.request(config)
      .then(res => {
          return next({
              res: res.data,
              authenticated,
              type: successType,
              custom_params
          })
      })
      .catch(err => {
          return next({
              err,
              type: errorType
          })
      });
};