import axios from 'axios';
import {
  FB_LOGIN_REQUEST, FB_LOGIN_SUCCESS, FB_LOGIN_FAILURE, FB_LOGIN_CLEAR_ERROR,
} from '../constants';
import * as ymmStorage from '../helpers/ymmStorage';

// fb login validate
// sync action: request
export function fbLoginRequest() {
  return {
    type: FB_LOGIN_REQUEST,
  };
}

// sync action: success
export function fbLoginSuccess() {
  return {
    type: FB_LOGIN_SUCCESS,
  };
}

// sync action: failure
export function fbLoginFailure(errorMsg, code = undefined) {
  return {
    type: FB_LOGIN_FAILURE,
    errorMsg,
    code,
  };
}

// clear error message
export function fbClearErrors() {
  return {
    type: FB_LOGIN_CLEAR_ERROR
  }
}

// get API key before call to authentication server
export function getApiKey() {
  // get api key
  const request = axios({
    method: 'POST',
    url: process.env.REACT_APP_API_GENERATE_KEY_URL,
    headers: [],
    data: {
      ip_address: '127.0.0.1',
      issue_time: Date.now(),
      platform: 'web'
    },
  })

  return {
    type: 'FACEBOOK_LOGIN_REQUEST', // not need to update state
    payload: request,
  }
}

// get API key before call to authentication server
export function ymmAuth() {
  // get api key
  const request = axios({
    method: 'GET',
    url: process.env.REACT_APP_API_AUTH_URL,
    headers: { 'Authorization': `Bearer ${ymmStorage.getItem('api_key') || null}` },
    params: {
      fb_user_token: ymmStorage.getItem('fb_token') || null
    },
  })

  return {
    type: 'YMM_LOGIN', // not need to update state
    payload: request,
  }
}
