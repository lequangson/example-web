import { CALL_CHAT_API } from 'chat/middleware/api';
import axios from 'axios'
import * as ymmStorage from 'chat/helpers/ymmStorage';
import {
  GET_CHAT_USER_PROFILE_REQUEST, GET_CHAT_USER_PROFILE_SUCCESS, GET_CHAT_USER_PROFILE_FAILURE,
  GET_DETAIL_USER_PROFILE_REQUEST, GET_DETAIL_USER_PROFILE_SUCCESS, GET_DETAIL_USER_PROFILE_FAILURE,
  SEND_GOOGLE_ANALYTIC, UPDATE_PUSH_NOTI_MODAL_STATUS, IS_ASKED_PUSH_NOTI_REQUEST
} from 'chat/constants';

// async action: fetch message
export function getUserProfile() {
  return {
    [CALL_CHAT_API]: {
      url: 'user',
      method: 'GET',
      authenticated: true,
      types: [GET_CHAT_USER_PROFILE_REQUEST, GET_CHAT_USER_PROFILE_SUCCESS, GET_CHAT_USER_PROFILE_FAILURE],
    },
  };
}

export function getDetailUserProfile(identifier) {
  const chatApiUrl = process.env.REACT_APP_API_URL

  const request = axios({
    method: 'GET',
    url: `${chatApiUrl}users`,
    params: {
      user_identifier: identifier,
    },
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` }
  })

  return {
    type: GET_DETAIL_USER_PROFILE_SUCCESS,
    payload: request,
  }
}


export function setLastTimeActivity() {
  return {
    [CALL_CHAT_API]: {
      url: 'set-last-activity',
      method: 'POST',
      authenticated: true,
      types: ['LAST_ACTIVITY', 'LAST_ACTIVITY', 'LAST_ACTIVITY'],
    },
  };
}

export function sendGoogleAnalytic(eventAction, ga_track_data) {
  return {
    type: SEND_GOOGLE_ANALYTIC,
    eventAction,
    ga_track_data,
  }
}

export function userSubscription(endPoint) {
  const chatApiUrl = process.env.REACT_APP_API_URL

  const request = axios({
    method: 'POST',
    url: `${chatApiUrl}notify_subscription`,
    params: {
      end_point: endPoint,
      source_type: 2,
      platform: 'web'
    },
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` }
  })

  return {
    type: 'updateUserEndPoint',
    payload: request,
  }
}

export function updatePushNotiModalStatus(isOpen, to_user_id) {
  return {
    type: UPDATE_PUSH_NOTI_MODAL_STATUS,
    isOpen,
    to_user_id,
  }
}

export function isAskedPushNotiRequest(isAsked) {
  return {
    type: IS_ASKED_PUSH_NOTI_REQUEST,
    isAsked,
  }
}