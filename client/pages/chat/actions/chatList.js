import fetch from 'isomorphic-fetch';
import axios from 'axios';
import { CALL_CHAT_API } from 'chat/middleware/api';
import handleResponseErrors from 'chat/helpers/handleResponseErrors';
import {
  FETCH_CHATLIST_REQUEST, FETCH_CHATLIST_SUCCESS, FETCH_CHATLIST_FAILURE,
  UPDATE_CHAT_LIST_KEY_WORD_FILTER, UPDATE_CHAT_LIST_LAST_ACTIVITY_TIME,
  INCREASE_CHAT_LIST_NUMBER_MESSAGE,
  UPDATE_CHAT_TRIAL_EXPIRED_REQUEST, UPDATE_CHAT_TRIAL_EXPIRED_SUCCESS, UPDATE_CHAT_TRIAL_EXPIRED_FAILURE
} from 'chat/constants';
import * as ymmStorage from 'chat/helpers/ymmStorage';
// async action: fetch chat list
export function fetchChatList() {
    return {
      [CALL_CHAT_API]: {
        url: 'users',
        method: 'GET',
        authenticated: true,
        types: [FETCH_CHATLIST_REQUEST, FETCH_CHATLIST_SUCCESS, FETCH_CHATLIST_FAILURE]
      }
    }
}

export function updateKeyWordFilter(keyword = '') {
  return {
    type: UPDATE_CHAT_LIST_KEY_WORD_FILTER,
    keyword
  }
}

export function updateLastActivityTimeByIdentifier(identifier) {
  return {
    type: UPDATE_CHAT_LIST_LAST_ACTIVITY_TIME,
    identifier
  }
}

export function increaseChatListNumberMessage(chat_room_id) {
  return {
    type: INCREASE_CHAT_LIST_NUMBER_MESSAGE,
    chat_room_id
  }
}

export function chatTrialExpired(chat_room_id) {
  return {
      [CALL_CHAT_API]: {
        url: 'update-chat-trial-status',
        data: {
          chat_room_id
        },
        method: 'POST',
        authenticated: true,
        types: [UPDATE_CHAT_TRIAL_EXPIRED_REQUEST, UPDATE_CHAT_TRIAL_EXPIRED_SUCCESS, UPDATE_CHAT_TRIAL_EXPIRED_FAILURE]
      }
    }
}

export function ymmUpdateChatFree(partner_user_identifier) { //this api for ymeetme mobile ui

  const chatApiUrl = process.env.REACT_APP_API_URL
  // get api key
  const request = axios({
    method: 'POST',
    url: `${chatApiUrl}matches_free`,
    data: {
      partner_user_identifier,
      platform: 'web'
    },
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` }
  })

  return {
    type: 'UPDATE_FREE_CHAT',
    payload: request,
  }
}


export function updateChatFree(from_user_id, to_user_id, description) { // this for chat ui
  return {
      [CALL_CHAT_API]: {
        url: 'update-chat-room-free',
        data: {
          from_user_id,
          to_user_id,
          description,
          platform: 'web'
        },
        method: 'POST',
        authenticated: true,
        types: [UPDATE_CHAT_TRIAL_EXPIRED_REQUEST, UPDATE_CHAT_TRIAL_EXPIRED_SUCCESS, UPDATE_CHAT_TRIAL_EXPIRED_FAILURE]
      }
    }
}

/**
* delete single or multiple chat room messages
* friend_user_ids: string json encode of array
*/
export function ignoreMessages(friend_user_ids) { // this for chat ui
  return {
      [CALL_CHAT_API]: {
        url: 'ignore-messages',
        data: {
          friend_user_ids,
          platform: 'web'
        },
        method: 'POST',
        authenticated: true,
        types: ['NONE', 'NONE', 'NONE']
      }
    }
}