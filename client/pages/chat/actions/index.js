import axios from 'axios'
import * as ymmStorage from 'chat/helpers/ymmStorage';
// this action does not need to storre to reducer
export function updateYmmPermission(permission_name = 'show_refund_banner', permission_value = 1) {

  // get api key
  const request = axios({
    method: 'POST',
    url: `${process.env.REACT_APP_API_URL}update_permission`,
    data: {
      permission_name,
      permission_value,
      platform: 'web'
    },
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` }
  })

  return {
    type: 'updateYmmPermission',
    payload: request,
  }
}
