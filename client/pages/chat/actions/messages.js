import fetch from 'isomorphic-fetch';
import { CALL_CHAT_API } from 'chat/middleware/api';
import handleResponseErrors from 'chat/helpers/handleResponseErrors';
import {
  FETCH_MESSAGE_REQUEST, FETCH_MESSAGE_SUCCESS, FETCH_MESSAGE_FAILURE,
  UPDATE_MESSAGES_REQUEST, UPDATE_STATE_MESSAGE,
  UPDATE_MESSAGES_SUCCESS, UPDATE_TYPING_STATUS,
  UPDATE_MESSAGES_FAILURE, DEFAULT_PAGE_SIZE,
  UPDATE_MESSAGES, MARK_MESSAGE_AS_READ_REQUEST, MARK_MESSAGE_AS_READ_SUCCESS,
  MARK_MESSAGE_AS_READ_FAILURE, MARK_MESSAGE_AS_READ_IN_STATE,
  MARK_ALL_MESSAGE_AS_READ_REQUEST, MARK_ALL_MESSAGE_AS_READ_SUCCESS, MARK_ALL_MESSAGE_AS_READ_FAILURE,
} from 'chat/constants/messages';

// async action: fetch message
export function fetchMessages(chat_room_id = '', identifier = '', page_index = 1) {
  return {
    [CALL_CHAT_API]: {
      url: 'messages',
      method: 'GET',
      authenticated: true,
      types: [FETCH_MESSAGE_REQUEST, FETCH_MESSAGE_SUCCESS, FETCH_MESSAGE_FAILURE],
      params: {
        chat_room_id: chat_room_id,
        'paging_model[page_index]': page_index,
        'paging_model[page_size]': DEFAULT_PAGE_SIZE
      },
      custom_params: {
        identifier
      }
    }
  }
}

// update message state via socket
export function updateMessagesRequest() {
  return {
    type: UPDATE_MESSAGES_REQUEST,
    isFetching: true,
  };
}

export function updateStateMessages(identifier = '', message = {}) {
  return {
    type: UPDATE_STATE_MESSAGE,
    isFetching: false,
    identifier,
    message,
  };
}

// call api to add new message to friend_user_id
export function updateMessages(message = {}) {
  return {
    [CALL_CHAT_API]: {
      url: 'messages',
      method: 'POST',
      authenticated: true,
      types: [UPDATE_MESSAGES_REQUEST, UPDATE_MESSAGES_SUCCESS, UPDATE_MESSAGES_FAILURE],
      data: {
        chat_room_id: message.chat_room_id,
        friend_user_id: message.friend_user_id,
        message: message.message,
        type: message.type
      }
    }
  }
}

export function markMessageAsRead(messageIds = []) {

  return {
    [CALL_CHAT_API]: {
      url: 'read_messages',
      method: 'POST',
      authenticated: true,
      types: [MARK_MESSAGE_AS_READ_REQUEST, MARK_MESSAGE_AS_READ_SUCCESS, MARK_MESSAGE_AS_READ_FAILURE],
      data: messageIds.join('&'), //fake data
      headers: true
    }
  }
}

export function markAllMessageAsRead(friend_user_id, chat_room_id) {

  return {
    [CALL_CHAT_API]: {
      url: 'read_all_messages',
      method: 'POST',
      authenticated: true,
      types: [MARK_MESSAGE_AS_READ_REQUEST, MARK_MESSAGE_AS_READ_SUCCESS, MARK_MESSAGE_AS_READ_FAILURE],
      data: {
        friend_user_id,
        chat_room_id
      }
    }
  }
}

export function markMessageAsReadInState(identifier) {
  return {
    type: MARK_MESSAGE_AS_READ_IN_STATE,
    identifier
  }
}

export function updateMessagesFailure(errorMsg) {
  return {
    type: UPDATE_MESSAGES_FAILURE,
    isFetching: false,
    errorMsg,
  };
}

export function updateTypingStatus(isTyping = false, identifier = '') {
  return {
    type: UPDATE_TYPING_STATUS,
    isTyping,
    identifier
  }
}

export function sendChatNotification(user_id, sender_name, message, chat_room_identifier, avatar) {

  return {
    [CALL_CHAT_API]: {
      url: 'send-chat-notification',
      method: 'POST',
      authenticated: true,
      types: ['CHAT_NOTIFICATION_OFFLINE', 'CHAT_NOTIFICATION_OFFLINE', 'CHAT_NOTIFICATION_OFFLINE'],
      data: {
        user_id,
        message,
        chat_room_identifier,
        avatar,
        sender_name
      }
    }
  }
}