import { CALL_CHAT_API } from 'chat/middleware/api';
import axios from 'axios'
import * as ymmStorage from 'chat/helpers/ymmStorage';
import { UNLOCK_CHAT_ROOM_IN_STATE, IGNORE_REFUND_IN_STATE} from 'chat/constants/Payment';
// this action does not need to storre to reducer
export function getRemainingCoin() {

  const chatApiUrl = process.env.REACT_APP_API_URL
  // get api key
  const request = axios({
    method: 'GET',
    url: `${chatApiUrl}remaining_coins`,
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` }
  })

  return {
    type: 'getRemainingCoin',
    payload: request,
  }
}


export function unlockChat(male_id, female_id) {

  const chatApiUrl = process.env.REACT_APP_API_URL
  // get api key
  const request = axios({
    method: 'POST',
    url: `${chatApiUrl}unlock_chat`,
    data: {
      male_id: male_id,
      female_id: female_id,
      platform: 'web'
    },
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` }
  })

  return {
    type: 'unlockChat',
    payload: request,
  }
}

export function unlockChatRoomInState(chat_room_id) {
  return {
    type: UNLOCK_CHAT_ROOM_IN_STATE,
    chat_room_id
  }
}

export function ignoreRefundInState(chat_room_id) {
  return {
    type: IGNORE_REFUND_IN_STATE,
    chat_room_id
  }
}
export function ymm_refunds(to_user_id) {

  const chatApiUrl = process.env.REACT_APP_API_URL
  // get api key
  const request = axios({
    method: 'POST',
    url: `${chatApiUrl}refunds`,
    data: {
      coin_consume_type: 'coin_consume_refund_unlock_chat',
      to_user_id,
      platform: 'web'
    },
    headers: { Authorization: `Bearer ${ymmStorage.getItem('ymm_token')}` }
  })

  return {
    type: 'refund',
    payload: request,
  }
}

export function chat_refunds(user_id, friend_user_id) {
    return {
      [CALL_CHAT_API]: {
        url: 'refunds',
        method: 'POST',
        data: {
          user_id,
          friend_user_id,
          platform: 'web'
        },
        authenticated: true,
        types: ['no_call_request', 'no_call_success', 'no_call_error']
      }
    }
}

export function use_gift(gift_id, detail, to_user_id) {
  return {
    [CALL_CHAT_API]: {
      url: 'use_gift',
      method: 'POST',
      data: {
        gift_id,
        detail,
        to_user_id,
        platform: 'web'
      },
      authenticated: true,
      types: ['no_call_request', 'no_call_success', 'no_call_error']
    }
  }
}

export function updateChatRoomFree(from_user_id, to_user_id, description) {
  return {
    [CALL_CHAT_API]: {
      method: 'POST',
      url: 'update-chat-room-free',
      data: {
        from_user_id,
        to_user_id,
        description,
        platform: 'web'
      },
      authenticated: true,
      side: 'chat',
      types: ['no_call_request', 'no_call_success', 'no_call_error']
    },
  };
}
