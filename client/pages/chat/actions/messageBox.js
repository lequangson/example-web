import {
  TEXT_BOX_HAS_VALUE,
  ENABLE_KEYBOARD,
  TOGGLE_EMOJI_SELECTION,
  UPDATE_TEXT_BOX_VALUE,
  INSERT_EMOJI,
  UPDATE_CARET_POSITION,
  UPDATE_MESSAGE,
} from 'chat/constants';

function checkBoolValue(value) {
  if (typeof value !== 'boolean') {
    throw new Error(`Wrong data type (${typeof value}) - Expected boolean value`);
  }
}

export function textBoxhasValue(flag) {
  checkBoolValue(flag);

  return {
    type: TEXT_BOX_HAS_VALUE,
    flag,
  };
}

export function enableKeyboad(flag) {
  checkBoolValue(flag);

  return {
    type: ENABLE_KEYBOARD,
    flag,
  };
}

export function toggleEmojiSelection() {
  return {
    type: TOGGLE_EMOJI_SELECTION,
  };
}

export function updateTextBoxValue(value) {
  return {
    type: UPDATE_TEXT_BOX_VALUE,
    value,
  };
}

export function updateCaretPosition(position) {
  return {
    type: UPDATE_CARET_POSITION,
    position,
  };
}

export function insertEmoji(emoji) {
  return {
    type: INSERT_EMOJI,
    emoji,
  };
}

export function updateMessage(flag) {
  checkBoolValue(flag);

  return {
    type: UPDATE_MESSAGE,
    flag,
  };
}
