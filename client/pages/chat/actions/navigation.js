import * as nav from 'chat/constants/navigation';

export function showNavigation() {
  return {
    type: nav.SHOW_NAVIGATION,
  };
}

export function hideNavigation() {
  return {
    type: nav.HIDE_NAVIGATION,
  };
}
