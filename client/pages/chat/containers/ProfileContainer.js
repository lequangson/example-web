import { connect } from 'react-redux'
import Profile from '../components/Profile'
import q from 'q'
import {
  getDetailUserProfile
} from '../actions/user'

function mapStateToProps(state) {
  return Object.assign({}, state, {
    user: state.chat_user
  })
}

function mapDispachToProps(dispatch) {
  return {
    getDetailUserProfile: (identifier) => {
      const defer = q.defer();
      dispatch(getDetailUserProfile(identifier)).then(res => {
        defer.resolve(res);
      }).catch(() => {
        defer.reject(false);
      })
      return defer.promise;
    },
  }
}
const ProfileContainer = connect(mapStateToProps, mapDispachToProps)(Profile)

export default ProfileContainer
