import { connect } from 'react-redux';
import {
  textBoxhasValue,
  enableKeyboad,
  toggleEmojiSelection,
  updateTextBoxValue,
  updateCaretPosition,
  insertEmoji,
  updateMessage,
} from '../actions/messageBox';
import AppBottomBar from '../components/AppBottomBar';

function mapStateToProps(state) {
  return Object.assign({}, state, {
    user: state.chat_user
  })
}

function mapDispatchToProps(dispatch) {
  return {
    textBoxhasValue: flag => dispatch(textBoxhasValue(flag)),
    enableKeyboad: flag => dispatch(enableKeyboad(flag)),
    toggleEmojiSelection: () => dispatch(toggleEmojiSelection()),
    updateTextBoxValue: value => dispatch(updateTextBoxValue(value)),
    updateCaretPosition: position => dispatch(updateCaretPosition(position)),
    insertEmoji: emoji => dispatch(insertEmoji(emoji)),
    updateMessage: flag => dispatch(updateMessage(flag)),
  };
}

const AppBottomBarContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppBottomBar);

export default AppBottomBarContainer;
