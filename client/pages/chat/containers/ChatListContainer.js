import { connect } from 'react-redux';
import ChatList from 'chat/components/ChatList';
import {
  fetchChatList,
  updateKeyWordFilter,
  updateLastActivityTimeByIdentifier,
  increaseChatListNumberMessage,
  ignoreMessages
} from 'chat/actions/chatList';
import { showNavigation, hideNavigation } from 'chat/actions/navigation';
import { getUserProfile, userSubscription, isAskedPushNotiRequest } from 'chat/actions/user';

function mapStateToProps(state) {
  return Object.assign({}, state, {
    user: state.chat_user
  })
}

function mapDispatchToProps(dispatch) {
  return {
    fetchChatList: () => {
      if (window.location.href.indexOf('/chat/') !== -1) { // does not recall api when user open chat room
        return;
      }
      dispatch(fetchChatList());
    },
    getUserProfile: () => dispatch(getUserProfile()),
    updateKeyWordFilter: (keyword = '') => dispatch(updateKeyWordFilter(keyword)),
    showNavigation: () => dispatch(showNavigation()),
    hideNavigation: () => dispatch(hideNavigation()),
    updateLastActivityTimeByIdentifier: (identifier) => dispatch(updateLastActivityTimeByIdentifier(identifier)),
    increaseChatListNumberMessage: (chat_room_id) => dispatch(increaseChatListNumberMessage(chat_room_id)),
    userSubscription: (endpoint) => dispatch(userSubscription(endpoint)),
    isAskedPushNotiRequest: (isAsked) => dispatch(isAskedPushNotiRequest(isAsked)),
    ignoreMessages: async (chat_room_ids) => await dispatch(ignoreMessages(chat_room_ids))
  };
}

const ChatListContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatList);

export default ChatListContainer;
