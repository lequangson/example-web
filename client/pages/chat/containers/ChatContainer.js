import { connect } from 'react-redux';
import q from 'q';
import { fetchMessages, updateMessagesRequest, updateTypingStatus,
  updateStateMessages, updateMessages, markMessageAsRead, markAllMessageAsRead,
  markMessageAsReadInState, sendChatNotification,
} from 'chat/actions/messages';
import Chat from 'chat/components/Chat';
import { fetchChatList, chatTrialExpired } from 'chat/actions/chatList';
import getUserIdFromIdentifier from 'chat/helpers/getUserIdFromIdentifier';
import {
  getUserProfile,
  sendGoogleAnalytic,
  userSubscription,
  updatePushNotiModalStatus,
  isAskedPushNotiRequest
} from 'chat/actions/user';
import { getRemainingCoin, unlockChat, unlockChatRoomInState } from 'chat/actions/Payment';
import { updateYmmPermission } from 'chat/actions';

function mapStateToProps(state) {
  return Object.assign({}, state, {
    user: state.chat_user
  })
}

function mapDispatchToProps(dispatch) {
  return {
    updateYmmPermission: (permission_name = 'show_refund_banner', permission_value = 1) => dispatch(updateYmmPermission(permission_name, permission_value)),
    fetchMessages: (chat_room_id = '', identifier = '') => {
      dispatch(fetchMessages(chat_room_id, identifier)).then((response) => {
        dispatch(markAllMessageAsRead(getUserIdFromIdentifier(identifier), chat_room_id));
      })
    },
    chatTrialExpired: (chat_room_id) => {
      const defer = q.defer();
      dispatch(chatTrialExpired(chat_room_id)).then(res => {
        defer.resolve(res);
      }).catch(() => {
        defer.reject(false);
      })
      return defer.promise;
    },
    getRemainingCoin: () => {
      const defer = q.defer();
      dispatch(getRemainingCoin()).then(res => {
        defer.resolve(res);
      }).catch(() => {
        defer.reject(false);
      })
      return defer.promise;
    },
    unlockChat: (male_id, female_id) => {
      const defer = q.defer();
      dispatch(unlockChat(male_id, female_id)).then(res => {
        defer.resolve(res);
      }).catch(() => {
        defer.reject(false);
      })
      return defer.promise;
    },
    unlockChatRoomInState: (chat_room_id) => {
      dispatch(unlockChatRoomInState(chat_room_id));
    },
    fetchChatList: () => dispatch(fetchChatList()),
    updateMessages: (identifier, message, saveToDb = false) => { // @TODO shoulde be refactoring later
      dispatch(updateMessagesRequest());
      dispatch(updateStateMessages(identifier, message));
      if (saveToDb) {
        dispatch(updateMessages(message));
      }
    },
    sendChatNotification: (user_id, sender_name, message, chat_room_identifier, avatar) => {
      dispatch(sendChatNotification(user_id, sender_name, message, chat_room_identifier, avatar));
    },
    getUserProfile: () => dispatch(getUserProfile()),
    markMessageAsRead: (chat_message_ids) => dispatch(markMessageAsRead(chat_message_ids)),
    markAllMessageAsRead: (friend_user_id, chat_room_id) => dispatch(markAllMessageAsRead(friend_user_id, chat_room_id)),
    markMessageAsReadInState: (identifier) => dispatch(markMessageAsReadInState(identifier)),
    updateTypingStatus: (status = false, identifier) => dispatch(updateTypingStatus(status, identifier)),
    sendGoogleAnalytic: (eventAction, gaTrackData) => dispatch(sendGoogleAnalytic(eventAction, gaTrackData)),
    userSubscription: (endpoint) => {dispatch(userSubscription(endpoint))},
    updatePushNotiModalStatus: (isOpen, to_user_id) => {dispatch(updatePushNotiModalStatus(isOpen, to_user_id))},
    isAskedPushNotiRequest: (isAsked) => {dispatch(isAskedPushNotiRequest(isAsked))}
  };
}

const ChatContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);

export default ChatContainer;
