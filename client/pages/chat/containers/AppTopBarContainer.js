import { connect } from 'react-redux';
import AppTopBar from '../components/AppTopBar';

function mapStateToProps(state) {
  return Object.assign({}, state, {
    user: state.chat_user
  })
}

// function mapDispatchToProps(dispatch) {
//   return {
//
//   }
// }

const AppTopBarContainer = connect(
  mapStateToProps,
)(AppTopBar);

export default AppTopBarContainer;
