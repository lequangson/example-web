import { connect } from 'react-redux';
import AppSetting from '../components/AppSetting';
import { fetchChatList } from '../actions/chatList';

function mapStateToProps(state) {
  return Object.assign({}, state, {
    user: state.chat_user
  })
}

function mapDispatchToProps(dispatch) {
  return {
    fetchChatList: () => dispatch(fetchChatList()),
  };
}

const AppSettingContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppSetting);

export default AppSettingContainer;
