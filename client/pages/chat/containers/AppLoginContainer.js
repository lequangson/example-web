import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import AppLogin from '../components/AppLogin';
import { fbLoginRequest, fbLoginFailure, fbClearErrors, getApiKey, ymmAuth, testApi, fbLoginSuccess } from '../actions/fbLogin';
import { CANNOT_LOGIN_VIA_FACEBOOK_MSG } from '../constants/errorMessage';
import { WARNING_CODE, DANGER_CODE } from '../constants/errorCode';
import { AUTH_YMM_TOKEN_NAME } from '../constants/index';
import { setLastTimeActivity } from '../actions/user';
import { fetchChatList } from '../actions/chatList';
import * as ymmStorage from '../helpers/ymmStorage';

function mapStateToProps(state) {
  return Object.assign({}, state, {
    user: state.chat_user
  })
}

function mapDispatchToProps(dispatch) {
  return {
    setError: (message = '') => {
      dispatch(fbLoginFailure([message], DANGER_CODE));
    },

    facebookLogin: (facebookResponse) => {
      dispatch(fbClearErrors()); // clear error if exist before
      dispatch(fbLoginRequest());
      if (typeof facebookResponse.accessToken !== 'undefined') { // login successfully via facebook
        dispatch(getApiKey()).then((apiKey) => {
          // save token into client storage
          ymmStorage.setItem('api_key', apiKey.payload.data.token);
          ymmStorage.setItem('fb_token', facebookResponse.accessToken);
          dispatch(ymmAuth()).then((ymmResponse) => {
            if (typeof ymmResponse.payload.headers !== 'undefined' && typeof ymmResponse.payload.headers.token !== 'undefined') {
              dispatch(fbLoginSuccess());
              // authenticate via ymeetme is successfully
              ymmStorage.setItem(AUTH_YMM_TOKEN_NAME, ymmResponse.payload.headers.token);
              dispatch(fetchChatList()).then(res => {
                dispatch(setLastTimeActivity());
              })
              browserHistory.push('/chat-list');
            } else {
              dispatch(fbLoginFailure([ymmResponse.payload.response.data.message], DANGER_CODE));
            }
          }).catch((error) => {
            dispatch(fbLoginFailure([CANNOT_LOGIN_VIA_FACEBOOK_MSG], DANGER_CODE));
          })

        }).catch(() => {
          dispatch(fbLoginFailure([CANNOT_LOGIN_VIA_FACEBOOK_MSG], DANGER_CODE));
        });

      } else {
        dispatch(fbLoginFailure([CANNOT_LOGIN_VIA_FACEBOOK_MSG], DANGER_CODE));
      }
    },
  };
}

const AppLoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppLogin);

export default AppLoginContainer;
