
// CDN Url
export const CDN_URL = 'https://cdn-static.ymeet.me'

// App Install url
export const IOS_URL = 'https://itunes.apple.com/us/app/ymeet-me-app-h%E1%BA%B9n-h%C3%B2-k%E1%BA%BFt-b%E1%BA%A1n/id1280333225?mt=8'
export const ANDROID_URL = 'https://play.google.com/store/apps/details?id=com.ymmrnt'
