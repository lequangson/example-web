export const WARNING_CODE = 2;
export const DANGER_CODE = 3;
export const HTTP_UNAUTHORIZED_CODE = 401;
export const HTTP_FORBIDDEN_CODE = 403;