export const GET_REMAINING_COIN_REQUEST = 'GET_REMAINING_COIN_REQUEST';
export const GET_REMAINING_COIN_SUCCESS = 'GET_REMAINING_COIN_SUCCESS';
export const GET_REMAINING_COIN_FAILURE = 'GET_REMAINING_COIN_FAILURE';

export const UNLOCK_CHAT_BY_COIN_REQUEST = 'UNLOCK_CHAT_BY_COIN_REQUEST';
export const UNLOCK_CHAT_BY_COIN_SUCCESS = 'UNLOCK_CHAT_BY_COIN_SUCCESS';
export const UNLOCK_CHAT_BY_COIN_FAILURE = 'UNLOCK_CHAT_BY_COIN_FAILURE';

export const UNLOCK_CHAT_ROOM_IN_STATE = 'UNLOCK_CHAT_ROOM_IN_STATE';
export const IGNORE_REFUND_IN_STATE = 'IGNORE_REFUND_IN_STATE';
export const NUMBER_COIN_TO_UNLOCK = 125;