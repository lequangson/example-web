
// const ymm_stickers = [
// 	{
// 		shortNames: [
// 			'hi',
// 			'hihi',
// 			'hehe',
// 			'hí',
// 			'hí hí'
// 		],
// 		specialNames: [
// 			':)',
// 			':-)',
// 			'=)',
// 			':]'
// 		],
// 		sticker_src: 'https://cdn-static.ymeet.me/general/Chat/Emoji/smile.gif',
// 		gender: 'F'
// 	},
// 	{
// 		shortNames: [
// 			'hi',
// 			'hihi',
// 			'hehe',
// 			'hí',
// 			'hí hí'
// 		],
// 		specialNames: [
// 			':)',
// 			':-)',
// 			'=)',
// 			':]'
// 		],
// 		sticker_src: 'https://cdn-static.ymeet.me/general/Chat/Emoji/smile-M.png',
// 		gender: 'M'
// 	},
// 	{
// 		shortNames: [
// 			'hì',
// 			'ngại',
// 			'ngại quá',

// 		],
// 		specialNames: [
// 			':$',
// 			':">'
// 		],
// 		sticker_src: 'https://cdn-static.ymeet.me/general/Chat/Emoji/Shy-F.png',
// 		gender: 'F'
// 	},
// 	{
// 		shortNames: [
// 			'hiu'

// 		],
// 		specialNames: [
// 			':(',
// 			':-(',
// 			':<',
// 			':/',
// 			'=(',
// 			':['
// 		],
// 		sticker_src: 'https://cdn-static.ymeet.me/general/Chat/Emoji/Sad.gif',
// 		gender: 'F'
// 	},
// 	{
// 		shortNames: [
// 			'hiu'

// 		],
// 		specialNames: [
// 			':(',
// 			':-(',
// 			':<',
// 			':/',
// 			'=(',
// 			':['
// 		],
// 		sticker_src: 'https://cdn-static.ymeet.me/general/Chat/Emoji/Sad-M.gif',
// 		gender: 'M'
// 	},
// 	{
// 		shortNames: [
// 			'-!'

// 		],
// 		specialNames: [
// 			':-!'
// 		],
// 		sticker_src: 'https://cdn-static.ymeet.me/general/Chat/Emoji/relax.png',
// 		gender: 'All'
// 	},
// 	{
// 		shortNames: [
// 			'-!'
// 		],
// 		specialNames: [
// 			':-!'
// 		],
// 		sticker_src: 'https://cdn-static.ymeet.me/general/Chat/Emoji/OK.gif',
// 		gender: 'All'
// 	}
// ];

/* eslint-disable */
const emojiPeopleString = ':hi: :hì: :hiu: :-!: :okie: :*: :xì: :haha: :g9: :hic: :grinning: :grin: :joy: :rofl: :smiley: :smile: :sweat_smile: :laughing: :wink: :blush: :yum: :sunglasses: :heart_eyes: :kissing_heart: :kissing: :kissing_smiling_eyes: :kissing_closed_eyes: :relaxed: :slight_smile: :hugging: :thinking: :neutral_face: :expressionless: :no_mouth: :rolling_eyes: :smirk: :persevere: :disappointed_relieved: :open_mouth: :zipper_mouth: :hushed: :sleepy: :tired_face: :sleeping: :relieved: :nerd: :stuck_out_tongue: :stuck_out_tongue_winking_eye: :stuck_out_tongue_closed_eyes: :unamused: :sweat: :pensive: :confused: :upside_down: :money_mouth: :astonished: :frowning2: :slight_frown: :confounded: :disappointed: :worried: :triumph: :cry: :sob: :frowning: :anguished: :fearful: :weary: :grimacing: :cold_sweat: :scream: :flushed: :dizzy_face: :rage: :angry: :innocent: :mask: :thermometer_face: :head_bandage:';

const emojiPeopleArray = emojiPeopleString.split(' ');
const ymm_emoji = {
	':hi:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/smile.gif',
	':hì:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/Shy-F.png',
	':hiu:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/Sad.gif',
	':-!:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/relax.png',
	':okie:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/OK.gif',
	':*:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/love.gif',
	':xì:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/Hate.gif',
	':haha:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/Happy.gif',
	':g9:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/Good+night-F.png',
	':hic:': 'https://cdn-static.ymeet.me/general/Chat/Emoji/cry.gif',


}

const ymm_short_names = [
	':hi:',
	':hì:',
	':hiu:',
	':-!:',
	':okie:',
	':*:',
	':xì:',
	':haha:',
	':g9:',
	':hic:'
];
const emojiCheatSheet = {
  people: emojiPeopleArray,
  ymm_emoji,
  ymm_short_names
};

export default emojiCheatSheet;


