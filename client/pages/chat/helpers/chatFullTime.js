import moment from 'moment';

export default function chatFullTime(timestamp) {
  if (!timestamp) {
    return '';
  }
  timestamp = new Date(timestamp).getTime();
  const mm = moment(timestamp, 'x');
  const currentYear = new Date().getFullYear();
  const messageYear = mm.get('year');

  // only show year if message in past year
  if (messageYear === currentYear) {
    return mm.format('D MMMM - h:mm A');
  }

  return mm.format('D MMMM YYYY - h:mm A');
}
