import cookie from 'react-cookie';
import { AUTH_YMM_TOKEN_NAME } from '../constants';

// This function to concat name with env to prevent confused name for test, staging
function getNewName(name) {
  const new_name = process.env.REACT_APP_NODE_ENV !== 'production' ? `${process.env.REACT_APP_NODE_ENV}_${name}` : name;
  return new_name;
}

export function setItem(name, value) {
  // concat env & name to prevent confusion between root domain and sub domain when test
  const new_name = getNewName(name);
  const now = new Date();
  now.setDate(now.getDate() + 30);
  cookie.save(new_name, value, { expires: now, domain: process.env.REACT_APP_DOMAIN, path: '/' });
}

export function getItem(name) {
  const new_name = getNewName(name);
  return cookie.load(new_name);
}

export function removeItem(name) {
  const new_name = getNewName(name);
  cookie.remove(new_name, { domain: process.env.REACT_APP_DOMAIN, path: '/' });
}

export function clear() {
  // should refactoring this function
  const new_name = getNewName('');
  cookie.remove(`${new_name}${AUTH_YMM_TOKEN_NAME}`, { domain: process.env.REACT_APP_DOMAIN, path: '/' });
  cookie.remove(`${new_name}fb_token`, { domain: process.env.REACT_APP_DOMAIN, path: '/' });
  cookie.remove(`${new_name}api_key`, { domain: process.env.REACT_APP_DOMAIN, path: '/' });
}
