import { isArray } from 'lodash';

// find chat data in datas array using receipient id
export default function findChat(datas, recipientID) {
  return;
  if (!isArray(datas)) {
    throw new Error(`Wrong data type (${typeof datas}) - Expected an array`);
  }
  // make sure id is number
  return datas.find(el => el.id === parseInt(recipientID, 10));
}
