import {
  DEFAULT_AVATAR_MALE,
  DEFAULT_AVATAR_FEMALE,
} from '../constants'

export default function showDefaultAvatar(gender, size = 'small') {
  if (!gender) return ''

  if (['small', 'thumb', 'large'].indexOf(size) === -1) {
    throw new Error('Image size not valid')
  }

  if (gender === 'female') {
    return `${DEFAULT_AVATAR_FEMALE}${size}.jpg`
  } else {
    return `${DEFAULT_AVATAR_MALE}${size}.jpg`
  }
}

export function defaultAvatarObject(gender, size = 'small') {
  if (!gender) return ''

  if (['small', 'thumb', 'large'].indexOf(size) === -1) {
    throw new Error('Image size not valid')
  }

  if (gender === 'female') {
    return {
      large_image_url: `${DEFAULT_AVATAR_FEMALE}${size}.jpg`,
      extra_large_image_url: `${DEFAULT_AVATAR_FEMALE}${size}.jpg`,
      thumb_image_url: `${DEFAULT_AVATAR_FEMALE}${size}.jpg`,
    }
  } else {
    return {
      large_image_url: `${DEFAULT_AVATAR_MALE}${size}.jpg`,
      extra_large_image_url: `${DEFAULT_AVATAR_MALE}${size}.jpg`,
      thumb_image_url: `${DEFAULT_AVATAR_MALE}${size}.jpg`,
    }
  }
}
