export default function getUserIdFromIdentifier(identifier) {
  if (!identifier || identifier.length < 8) {

    return null
  }
  return parseInt(identifier.substr(6, identifier.length - 8), 10);
}