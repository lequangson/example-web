/**
 *
 * Get User device (PC, android or ios)
 *
 */

export function getName() {
  if( /android/i.test(navigator.userAgent.toLowerCase()) ) {
    return 'android';
  }
  if( /iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()) ) {
    return 'ios';
  }
  return 'pc';
}

export function getBrowserName() {
  if( /coc_coc|coc coc/i.test(navigator.userAgent.toLowerCase()) ) {
    return 'coc_coc';
  }
  if( /chrom/i.test(navigator.userAgent.toLowerCase()) ) {
    return 'chrom';
  }
  if( /firefox/i.test(navigator.userAgent.toLowerCase()) ) {
    return 'firefox';
  }
  if( /safari/i.test(navigator.userAgent.toLowerCase()) ) {
    return 'safari';
  }
  return '';
}

export function isViewFromZalo() {
  if( /zalo/i.test(navigator.userAgent.toLowerCase()) ) {
    return true
  }
  return false
}

export function isHeighResolution() {
  if (window.matchMedia('(min-width: 1200px) and (min-resolution: 1dppx)').matches) {
    return true;
  }
  if (window.matchMedia('(min-width: 320px) and (max-width: 768px) and (min-resolution: 2dppx)').matches) {
    return true;
  }
  return false;
}