/* global window, document */
// scroll window to bottom
export default function scrollToBottom() {
  window.scrollTo(0, document.body.scrollHeight);
}
