import moment from 'moment';
import _ from 'lodash';
export default function getIdentifierFromId(id) {
  return moment().format('DDMMYY') + id + _.random(10, 99);
}