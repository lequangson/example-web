import { MAXIMUM_CHAT_WORDS_PER_MESSAGE } from '../constants';

var chunks = function(array, size) {
  var results = [];
  while (array.length) {
    results.push(array.splice(0, size));
  }
  return results;
};

export function uniqChatMessages(chatMessages = []) {
  const n = {},r=[];
  const dataLength = chatMessages.length;
  for(let i = 0; i < dataLength; i++)
  {
    const _id = chatMessages[i]._id;
    if (!n[_id])
    {
      n[_id] = true;
      r.push(chatMessages[i]);
      // if (chatMessages[i].message.split(' ').length > MAXIMUM_CHAT_WORDS_PER_MESSAGE) {
      //   const currentMsg = chatMessages[i];
      //   const words = chatMessages[i].message.split(" ");
      //   const chunkWords = chunks(words, MAXIMUM_CHAT_WORDS_PER_MESSAGE);
      //   let newMsg = {};
      //   chunkWords.map((test) => {
      //     newMsg = {
      //       _id: currentMsg._id + i,
      //       chat_room_id: currentMsg.chat_room_id,
      //       created_at: currentMsg.created_at,
      //       friend_user_id: currentMsg.friend_user_id,
      //       is_deleted: currentMsg.is_deleted,
      //       message: test.join(' '),
      //       read_at: currentMsg.read_at,
      //       user_id: currentMsg.user_id,
      //     }
      //     r.push(newMsg);
      //   })
      // } else {
      //   r.push(chatMessages[i]);
      // }
    }
  }
  return r;
}

export function getCurrentUserIdFromChatRoomAndIdentifier(chat_room_id, identifier) {
  return parseInt(chat_room_id.replace(identifier, '').replace('_', ''));
}