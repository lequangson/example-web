export default function isMessageHasEmoji(text = '') {
  if (typeof text !== 'string') {
    throw new Error(`Wrong value type (${typeof text}) - Expected string`);
  }

  const regex = /:[\w-!*\u0080-\u00FF]+:/;
  return regex.test(text);
}
