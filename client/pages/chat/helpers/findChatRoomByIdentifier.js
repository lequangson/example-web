
/**
 *
 * Get chat room data from identifier
 * @param chatList in props, identifier from url
 * @return chatList object
 *
 */
import getUserIdFromIdentifier from './getUserIdFromIdentifier';

export default function findChatRoomByIdentifier(chatList, identifier) {
  if (chatList.data.length) {
    return chatList.data.find(room => getUserIdFromIdentifier(room.identifier) === getUserIdFromIdentifier(identifier));
  }
  return null;
}
