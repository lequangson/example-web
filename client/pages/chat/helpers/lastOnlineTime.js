import moment from 'moment';

export default function lastOnlineTime(time) {
  time = new Date(time).getTime();
  moment.locale('vi');
  return moment(time, 'x').startOf('day').fromNow();
}
