// handle 500/404 error
export default function handleResponseErorrs(response) {
  if (!response.ok) throw new Error(response.statusText);
  return response;
}
