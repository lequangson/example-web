import {isEmpty} from 'lodash';
const CHAT_TRIAL_MATCH_TYPE = 5;

 /**
 * type: 1 defaul message
 * type: 2 user to user
 * type: 3 ice breaking
 * type: 4 admin message
 **/

export function isValid(current_user, chatRoomData, messages) {
  if (isEmpty(chatRoomData) || isEmpty(current_user)) {
    return false;
  }
  const { match_type } = chatRoomData;
  if (match_type != CHAT_TRIAL_MATCH_TYPE && match_type != 6) { // 6: match question
    return false;
  }
  const total_sent_messages = messages.filter(
      item => item.user_id == current_user._id && item.type != 1 && item.type != 3
  ).length;
  if (total_sent_messages >= 10) { // n each chat box, I can send a maximum of 10 messages (ignore default message)
    return false;
  }

  //Either if female user liked me back with default message, or if they reply me with a message that contains more than 3 words
  const messages_received = messages.filter(
      item => item.user_id != current_user._id
  )
  let msg_contain_more_than_3_words = messages_received.some((receiveItem, i) => receiveItem.type != 1 && receiveItem.type != 3 && countWords(receiveItem.message) > 3);
  if (msg_contain_more_than_3_words) {
    return false;
  }
  return true;
}

export function countWords(str) {
  return str.trim().split(/\s+/).length;
}

export function containSuspendText(msg = '') {
  const clean_msg = msg.replace(/[,_\.\-\?!\s:]/g, '');
  // check if msg contain any suspend text
  if (/zalo|zlo|zl|fb|viber|skype|skyper|facebook|dalo|http|https|www|@/i.test(clean_msg)) {
    return true;
  }
  //filter phone number
  if (/\d{8,11}/.test(clean_msg)) {
    return true;
  }
  // filter email, social account
  let is_accountName = false;
  if (msg.length) {
    msg.split(" ").forEach((singleWord) => {
      const clean_word = singleWord.replace(/[,_\.\-\?!\s:]/g, '');
      if (/\w{2,}\d{2,}/.test(clean_word)) {
        is_accountName = true;
      }
    })
  }
  return is_accountName;
}