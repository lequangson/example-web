/* global ga: true */

export function sendGoogleAnalytic(eventAction, currentUser, gaTrackData) {
  const options = {
    hitType: 'event',
    eventCategory: eventAction.eventCategory,
    eventAction: eventAction.eventAction,
    eventLabel: eventAction.eventLabel,
    dimension1: currentUser ? currentUser.age : '',
    dimension2: currentUser ? currentUser.gender || '' : '',
    dimension3: currentUser ? currentUser.address : '',
    dimension11: currentUser ? currentUser.user_id : '',
    // Custom data
    dimension5: gaTrackData.partner_address || '',
    dimension8: gaTrackData.page_source || '',
    
    dimension10: gaTrackData.partner_age || null,
    dimension12: gaTrackData.partner_id || null,
    dimension13: gaTrackData.message || '',
    dimension14: gaTrackData.extra_information || '',
    dimension15: gaTrackData.extra_information2 || '',
  }


  console.log('gaTrackEvent', options)

  ga('send', options)
}