import {
  CDN_URL,
} from '../../constants';
import $ from 'jquery';

export function startHappyHours() {
  const clock_begin_url = `${CDN_URL}/general/clock-begin.png`
  const begin = $(`
    <div class="noti noti--bg-blue mbm">
      <div class="noti__body">
        <div class="noti__header mbm">
          <img src= ${clock_begin_url} alt="coin_reward_icon+coin.png" />
        </div>
        <div class="noti__content txt-bold">Giờ vui vẻ <br /> bắt đầu!</div>
      </div>
    </div>
  `)

  $('body').append(begin)

  setTimeout(function() {
    $(begin).remove()
  }, 10000)
}

export function stopHappyHours() {
  const clock_end_url = `${CDN_URL}/general/clock-end.png`
  const end = $(`
    <div class="noti noti--bg-red mbm">
      <div class="noti__body">
        <div class="noti__header mbm">
          <img src= ${clock_end_url} alt="coin_reward_icon+coin.png" />
        </div>
        <div class="noti__content txt-bold">Giờ vui vẻ <br /> kết thúc!</div>
      </div>
    </div>
  `)
  $('body').append(end)

  setTimeout(function() {
    $(end).remove()
  }, 10000)
}