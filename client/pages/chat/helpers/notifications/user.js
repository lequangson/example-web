import {
  CDN_URL,
} from '../../constants';
import $ from 'jquery';

export function lockedAccount() {
  // const clock_begin_url = `${CDN_URL}/general/clock-begin.png`
  const begin = $(`
    <div class="noti noti--bg-green mbm">
      <div class="noti__body">
        <div class="noti__content txt-bold">Người dùng đã khóa tài khoản!</div>
      </div>
    </div>
  `)

  $('body').append(begin)

  setTimeout(function() {
    $(begin).remove()
  }, 5000)
}