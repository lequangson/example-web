import {
  CDN_URL,
} from '../../constants';
import $ from 'jquery';

export function suspendMessage() {
  // const clock_begin_url = `${CDN_URL}/general/clock-begin.png`
  const begin = $(`
    <div class="noti noti--bg-yellow mbm">
      <div class="">
        <div class="txt-bold">Vui lòng không chia sẻ các thông tin cá nhân như Facebook, Zalo hay số điện thoại... Tin nhắn chứa nội dung này sẽ bị xoá.</div>
      </div>
    </div>
  `)

  $('body').append(begin)

  setTimeout(function() {
    $(begin).remove()
  }, 5000)
}