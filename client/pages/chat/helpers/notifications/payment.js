import {
  CDN_URL,
} from '../../constants';
import $ from 'jquery';

export function alreadyVerify() {
  // const clock_begin_url = `${CDN_URL}/general/clock-begin.png`
  const begin = $(`
    <div class="noti noti--bg-green mbm">
      <div class="noti__body">
        <div class="noti__header mbm">
          <i class="fa fa-check-circle-o txt-xxxlg"></i>
        </div>
        <div class="noti__content txt-bold">Bạn đã xác thực tài khoản rồi!</div>
      </div>
    </div>
  `)

  $('body').append(begin)

  setTimeout(function() {
    $(begin).remove()
  }, 5000)
}