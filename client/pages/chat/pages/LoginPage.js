import React from 'react';
import AppLoginContainer from '../containers/AppLoginContainer';
import BannerAppInstall from '../components/commons/BannerAppInstall'
import * as UserDevice from '../helpers/UserDevice';
import { ANDROID_URL, IOS_URL } from '../constants/Enviroment'

const LoginPage = props => (
  <div>
    {(UserDevice.getName() === 'ios' || UserDevice.getName() === 'android') &&
      <BannerAppInstall
        isOpen={true}
        {...props}
        url={UserDevice.getName() === 'ios' ? IOS_URL : ANDROID_URL}
      />
    }
    <AppLoginContainer {...props} />
  </div>
);

export default LoginPage;
