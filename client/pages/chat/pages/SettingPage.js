import React from 'react';
import AppTopBarSetting from '../components/AppTopBarSetting';
import AppSettingContainer from '../containers/AppSettingContainer';

const SettingPage = props => (
  <div className="yc-container">
    <AppTopBarSetting />
    <AppSettingContainer {...props} />
  </div>
);

export default SettingPage;
