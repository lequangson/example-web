import React, { PropTypes } from 'react'
import ProfileContainer from '../containers/ProfileContainer'
import AppTopBarContainer from '../containers/AppTopBarContainer'

const Profile = props => (
  <div>
    <AppTopBarContainer {...props} />
    <ProfileContainer {...props} />
  </div>
)

Profile.propTypes = {
  params: PropTypes.object.isRequired,
}

export default Profile
