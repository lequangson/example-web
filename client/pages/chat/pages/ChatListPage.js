import React from 'react';
import ChatListContainer from '../containers/ChatListContainer';

const ChatListPage = props => (
  <ChatListContainer {...props} />
);

export default ChatListPage;
