import React from 'react';
import ChatContainer from '../containers/ChatContainer';

const ChatPage = props => (
  <ChatContainer {...props} />
);

export default ChatPage;
