import React from 'react'
import SearchInfiniteContainer from '../containers/SearchInfiniteContainer'
import HeaderContainer from '../containers/HeaderContainer'

const SearchInfinite = props => (
  <div className="sticky-footer">
    <HeaderContainer {...props} />
    <SearchInfiniteContainer {...props} />
  </div>
)

export default SearchInfinite
