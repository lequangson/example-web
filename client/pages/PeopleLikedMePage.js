import React from 'react';
import PeopleLikedMe from 'pages/people-liked-me';
import HeaderContainer from 'containers/HeaderContainer';

const PeopleLikedMePage = props => (
  <div className="sticky-footer">
    <HeaderContainer {...props} />
    <PeopleLikedMe {...props} />
  </div>
)

export default PeopleLikedMePage;
