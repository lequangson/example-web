import React from 'react'
import { connect } from 'react-redux'
import HeaderContainer from '../containers/HeaderContainer'
import ReferralComponent from '../components/ReferralComponent'

const Referral = (props) => {
  return (
    <div>
      <HeaderContainer {...props} />
      <ReferralComponent {...props} />
    </div>
  )
}

function mapStateToProps(state) {
  return state
}

const ReferralPage = connect(mapStateToProps)(Referral)
export default ReferralPage
