import React from 'react'
import { Link } from 'react-router'
import { ICON_404 } from '../constants/Enviroment'
import AnonymousHeader from '../components/headers/AnonymousHeader'

const SuspendedAccount = props => (
  <div>
    <AnonymousHeader />
    <div className="container">
      <div className="row mbl">
        <div className="col-xs-12">
          <h3 className="txt-heading">Lỗi</h3>
        </div>
      </div>
      <div className="row txt-center">
        <img src={ICON_404} alt="" />
        <p>Rất tiếc, đã xảy ra lỗi!</p>
        <p>Tài khoản của bạn đã vi phạm <Link to="/terms-of-use">Điều khoản sử dụng</Link> của Ymeet.me. Vui lòng kiểm tra email để xem thông tin chi tiết về lỗi và cách khắc phục.</p><br />
        <p>Chân thành cảm ơn!</p>
      </div>
    </div>
  </div>
)

export default SuspendedAccount
