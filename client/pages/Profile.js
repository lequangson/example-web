import React, { PropTypes } from 'react'
import ProfileContainer from '../containers/ProfileContainer'

const Profile = props => (
  <div>
    <ProfileContainer identifier={props.params.identifier} {...props} />
  </div>
)

Profile.propTypes = {
  params: PropTypes.object.isRequired,
}

export default Profile
