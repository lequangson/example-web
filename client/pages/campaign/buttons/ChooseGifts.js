import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import $ from 'jquery';
import _ from 'lodash';
import {GIFT_IMAGES} from 'constants/Enviroment';
import GoogleOptimize from 'utils/google_analytic/google_optimize';
import { SendGift } from './';
import {
  CDN_URL
} from 'constants/Enviroment';

class ChooseGifts extends React.Component {

  static propTypes = {
  }

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
      selected_item: {},
      displayBuyCoin: false
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  @GoogleOptimize
  componentDidUpdate(){}

  openModal() {
    this.setState({ modalIsOpen: true })
  }

  closeModal() {
    this.setState({ modalIsOpen: false, selected_item: {} })
  }

  handleClick = (selected_item) => {
    const { current_user } = this.props;

    this.setState({ selected_item });
    $(`#gift-${selected_item.id}`).click();
    if(current_user.coins < selected_item.price){
      this.setState({
        displayBuyCoin: true
      });
    }
    else{
      this.setState({
        displayBuyCoin: false
      });
    }
  }

  render() {
    const { campaign, identifier, compatibility, current_user } = this.props;
    const { gift_dictionary } = campaign;
    const min_price = !_.isEmpty(gift_dictionary) && Math.min.apply(null, gift_dictionary.map(x => x.price));
    const showBuyCoinButton = !_.isEmpty(current_user) && (current_user.coins < min_price || this.state.displayBuyCoin);

    return (
      <div className="padding-t10 padding-b10">
        <button
          className='btn btn--b btn--p'
          onClick={this.openModal}
        >
          Chọn quà
        </button>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="GiftListModal modal__content"
          overlayClassName="modal__overlay modal__overlay--2"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="well well--e txt-blue radius-top txt-medium txt-lg txt-center mbm">
            Chọn quà
          </div>
          <div className="padding-rl10 mbm">
            <div className="row">
              <div className="txt-blue txt-uppercase txt-bold">
                Quà đặc biệt<img className="margin-l5 margin-0 banner__icon--1" src={`${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`} alt="Khuyến mãi 20/10" /> 150
              </div>
            </div>
            <div className="row l-flex l-flex-aglign-end">
              {gift_dictionary.filter(gift => gift.price == 150).map((item, i) => {
                return (
                  <div className="col-xs-4 modal-open l-flex-v-end padding-t10 padding-b10" key={i} onClick={() => {this.handleClick(item)}}  >
                    {item.is_hot && <div>
                      <div className="card__addon-2">
                        <div className="card__addon-2__title txt-lg">HOT</div>
                      </div>
                      <div className="card__pad-2 card__pad-2--red">
                    </div></div>}
                    <div className="col-xs-12 mbm txt-center">
                      <img src={`${item.photo_url}`} className="img-gift" alt="Gift Image" />
                    </div>
                    <div className="col-xs-12 txt-center">
                      <label>
                        <input type="radio" id={`gift-${item.id}`} name="radio" defaultChecked={item.id == this.state.selected_item.id ? true : false } />
                        <div className="fake-radio"></div>
                      </label>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>
          <div className="padding-rl10 mbm">
            <div className="row">
              <div className="txt-blue txt-uppercase txt-bold">
                Quà đồng giá<img className="margin-l5 margin-0 banner__icon--1" src={`${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`} alt="Khuyến mãi 20/10" /> 10
              </div>
            </div>
            <div className="row">
              <div className="l-card-info">
                {gift_dictionary.filter(gift => gift.price == 10).map((item, i) => {
                  return (
                    <div className="col-xs-4 modal-open l-flex-v-end padding-t10 padding-b10" key={i} onClick={() => {this.handleClick(item)}}  >
                      {item.is_hot && <div>
                        <div className="card__addon-2">
                          <div className="card__addon-2__title txt-lg">HOT</div>
                        </div>
                        <div className="card__pad-2 card__pad-2--red">
                      </div></div>}
                      <div className="col-xs-12 mbm txt-center">
                        <img src={`${item.photo_url}`} className="img-gift" alt="Gift Image" />
                      </div>
                      <div className="col-xs-12 txt-center">
                        <label>
                          <input type="radio" id={`gift-${item.id}`} name="radio" defaultChecked={item.id == this.state.selected_item.id ? true : false } />
                          <div className="fake-radio"></div>
                        </label>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
          <div className="padding-rl10 padding-b10">
            <SendGift
              item={this.state.selected_item}
              identifier={identifier}
              sent_success={this.closeModal}
              compatibility={compatibility}
              showBuyCoinButton={showBuyCoinButton}
            />
          </div>

          <button className="modal__btn-close" onClick={this.closeModal}>
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
    return {

    }
}
const ChooseGiftsContainer = connect(mapStateToProps, mapDispachToProps)(ChooseGifts);

export default ChooseGiftsContainer;
