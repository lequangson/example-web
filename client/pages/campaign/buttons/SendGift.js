import React, { Component } from 'react';
import PropTypes from 'prop-types';
import socket from "utils/socket";
import { browserHistory } from 'react-router';
import { ACTIVE_USER } from 'constants/userProfile';
import { NOTIFICATION_SUCCESS } from 'constants/Enviroment';
import notification from 'utils/errors/notification'
//for test async to wait official api

class SendGift extends Component {
    static propTypes = {
       item: PropTypes.object
    };

    constructor(props) {
       super(props);
       this.state = {
        isSending: false,
        isDisabled: true
       }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.item.id != nextProps.item.id) {
          this.setState({ isDisabled: !nextProps.item.id });
        }
    }

    sendGift = async () => {
      if (this.state.isSending) {
        return;
      }
      this.setState({ isSending: true });
      const { item, identifier, compatibility, user_profile } = this.props;
      const sender = Object.assign({}, user_profile.current_user, {
        compatibility
      })
      try {
        await this.props.sendGift(identifier, item.id, item.price);
        this.setState({ isSending: false });
        this.props.sent_success();
        notification(NOTIFICATION_SUCCESS, `Bạn đã gửi quà thành công.`);
        if (user_profile.current_user.user_status == ACTIVE_USER) {
          socket.emit('send_gift', {type: 'CAMPAIGN', room: identifier, sender, item});
        }
      } catch (e) {
        //console.log('error', e);
      }
    }

    buyCoin = () => {
      browserHistory.push('/payment/coin');
    }

    render() {
      if (this.props.showBuyCoinButton) {
        return (
          <button
            className="btn btn--b btn--p"
            onClick={this.buyCoin}
          >
            Nạp thêm xu
          </button>
        );
      }
      return (
        <button
          className="btn btn--b btn--p"
          onClick={this.sendGift}
          disabled={this.state.isDisabled}
        >
          Tặng quà
        </button>
      );
    }
}

export default SendGift;
