import { connect } from 'react-redux'
import ChooseGifts from './ChooseGifts';
import SendGift from './SendGift';
import {
  sendGift
} from '../Action';

function mapStateToProps(state) {
  return {
    campaign: state.campaign,
    user_profile: state.user_profile
  }
}

function mapDispatchToProps(dispatch) {
  return {
    sendGift: async (to_user_identifier, gift_id, price) => {
      await dispatch(sendGift(to_user_identifier, gift_id, price))
    }
  }
}

const SendGiftContainer =  connect(
  mapStateToProps,
  mapDispatchToProps
)(SendGift);

export {
  ChooseGifts,
  SendGiftContainer as SendGift
}
