import { connect } from 'react-redux'
import Campaign from './Campaign';
import {} from './Action';

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Campaign)
