
import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

import { ProgressiveImage } from 'components/commons/ProgressiveImage'
import * as UserDevice from 'utils/UserDevice';
import { defaultAvatarObject } from 'src/scripts/showDefaultAvatar'
import { GIFT_IMAGES,
    WOMEN_GIFT_BEAR_IMAGE,
    WOMEN_GIFT_FLOWER_IMAGE,
    WOMEN_GIFT_CARD_IMAGE } from 'constants/Enviroment'


class PresentItem extends Component {
    static propTypes = {
        user: PropTypes.object,
        giftInfo: PropTypes.object,
    }

    renderNewUserIcon() {
        const { user } = this.props;
        const isNewUser = user ? user.is_new_user : false;
        if (!isNewUser) {
            return ''
        }
        return (
          <div>
            <div className="card__addon-2">
                <div className="card__addon-2__title">Mới!</div>
            </div>
            <div className="card__pad-2"></div>
          </div>
        )
    }

    render() {
        const { user, giftInfo } = this.props;
        const giftName = giftInfo ? giftInfo.name ? giftInfo.name : "" : "";
        const giftImage = giftInfo ? giftInfo.photo_url ? giftInfo.photo_url : "" : "";
        let userPicture = user.user_pictures.find((picture) => picture.is_most_similar);
        if (_.isEmpty(userPicture)) {
          userPicture = user.user_pictures.length ?
            user.user_pictures[0] :
            defaultAvatarObject(user.gender, 'large')
        }

        return (
            <div className="row l-flex-vertical-center mbm element_height">
                <div className="col-xs-6 col-sm-4">
                    <article className="card">
                        <div className="card__upper">
                            <Link to={`/profile/${user.identifier}`} className="card__link">
                                <ProgressiveImage
                                preview={userPicture.small_image_url}
                                src={ UserDevice.isHeighResolution()? userPicture.extra_large_image_url : userPicture.large_image_url}
                                render={(src, style) => <img src={src} style={style} alt={user.nick_name} />}
                                />
                                <div className="card__addon">
                                    <i className="fa fa-heart"></i>
                                    <div className="txt-bold">{user.compatibility}%</div>
                                </div>
                                <div className="card__pad"></div>
                                {this.renderNewUserIcon()}
                                <div className="card__meta card__meta--1">
                                    <span className="txt-bold txt-lg">{user.nick_name} - {user.age}</span>
                                    <div className="txt-lg"> {
                                        (
                                            typeof user.residence !== 'undefined' &&
                                            typeof user.residence.value !== 'undefined' &&
                                            user.residence.value
                                          ) ? user.residence.value : 'Không rõ'
                                    }</div>
                                </div>
                            </Link>
                        </div>
                    </article>
                </div>
                <div className="col-xs-3 col-sm-4">
                    <span className="txt-medium">Tặng {giftName}</span>
                </div>
                <div className="col-xs-3 col-sm-4">
                    <img src={giftImage} className="img-100" alt={giftName} />
                </div>
            </div>
        );
    }
}

export default PresentItem;