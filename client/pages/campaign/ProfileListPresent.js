import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'

class ProfileListPresent extends Component {
	constructor(props) {
    super(props);
    this.openMyPresentList = this.openMyPresentList.bind(this);
  }

  openMyPresentList() {
  	browserHistory.push(`/my-present-list`);
  }

	render() {
		const my_gifts = this.props.campaign.my_gifts.slice(0, 4);
    const newGifts = this.props.campaign.my_gifts.filter(gift => gift.is_new === true);

		return (
			<div className="panel" id="gift">
			{
				<div>
					<div className="panel__heading">
						<h3 className="panel__title">Quà tặng của bạn</h3>
					</div>
					<div className="panel__body" onClick={this.openMyPresentList}>
						<div className="container">
							<div className="row">
							<div className="col-xs-12">

								<div className="padding-t10 padding-b10 l-flex-vertical-center">
								 {my_gifts.map((present, index) => {
									return (
									 <div className="progress__fill-into--1 txt-center" key={index}>
                    {present.gift.photo_url ?
                    <img src={present.gift.photo_url} className="img-gift" alt={present.gift.name}/>
                    : null}
									 </div>
									)
								 })}
								 {my_gifts.length > 3?
									<div className="progress__fill-into--1 txt-center">
									 <button className="btn btn--p btn--round-1">
									 Xem <br/>thêm
									 {newGifts.length > 0 && <div className="nav__addon rounded">{newGifts.length}</div>}
									 </button>
									</div>:
									null
								 }
								</div>

							</div>
							</div>
						</div>
					</div>
				</div>
			}
			</div>
		)
	}
}

ProfileListPresent.propTypes = {
	campaign: PropTypes.object,
};

function mapStateToProps(state) {
 return {
	campaign: state.campaign
 };
};

export default connect(
 mapStateToProps
)(ProfileListPresent);

