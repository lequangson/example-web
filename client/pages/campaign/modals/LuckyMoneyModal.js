import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux'
import Modal from 'components/modals/Modal';
import { CDN_URL } from 'constants/Enviroment';

class LuckyMoneyModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      hiddenCard: true,
      isOpenModal: true
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (this.props.Daily_Remind.campaign.lucky_money != nextProps.Daily_Remind.campaign.lucky_money &&
            nextProps.Daily_Remind.campaign.lucky_money > 0) ||
            !nextState.hiddenCard ||
            this.state.isOpenModal != nextState.isOpenModal;
  }

  closeModal = () => {
    this.setState({
      isOpenModal: false
    });
  }

  onShowReceivedCoins = () => {
    this.setState({
      hiddenCard: false
    })
  }

  render() {
    const { lucky_money } = this.props.Daily_Remind.campaign;
    return (
      <div>
      {
        lucky_money > 0 &&
        <Modal
          transitionName="modal"
          isOpen={this.state.isOpenModal}
          onRequestClose={this.closeModal}
          className="lucky-money-modal modal__content"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="container">
            {this.state.hiddenCard ?
              <div className="mbl padding-t10 txt-center">
                <div className="mbm"><span className="txt-bold txt-uppercase txt-blue txt-xlg">Lì xì đầu năm,<br />may mắn ngập tràn</span></div>
                <div className="mbl txt-lg txt-blue">Nhấp vào bao lì xì để nhận xu</div>
              </div>
            :
              <div className="mbl padding-t10 txt-center">
                <div className="mbs">
                  <span className="txt-bold txt-uppercase txt-blue txt-xlg">Bạn được lì xì<br />{lucky_money} xu !</span>
                </div>
                <div className="l-flex-vertical-center txt-center">
                  <img className="mt banner__icon--3" src={`${CDN_URL}/general/Payment/Coins/coin_reward_icon+coin.png`} alt="Xu" />
                  <span className="txt-bold txt-uppercase margin-l5 txt-warning txt-xxxlg">{lucky_money}</span>
                </div>
              </div>
            }
            <img className="banner__decor--2 img-full" src={`${CDN_URL}/general/Campaign/coin-bg.png`}/>
            <div className="row">
              <div className="col-xs-10 col-xs-offset-1 mbl txt-center">
                <div className="flipper">
                  <div className={`${this.state.hiddenCard? '' : 'active'} front`} onClick={this.onShowReceivedCoins}>
                    <img className="img-150" src={`${ CDN_URL }/general/Campaign/envelope1.png`} />
                  </div>
                  <div className={`${this.state.hiddenCard? '': 'active'} back`}>
                    <img className="img-150" src={`${ CDN_URL }/general/Campaign/envelope2.png?id=1`} />
                  </div>
                </div>
              </div>
            </div>

          </div>
          <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
        </Modal>
      }
      </div>
    );
  }
}

LuckyMoneyModal.propTypes = {
  Daily_Remind: PropTypes.object
}

function mapStateToProps(state) {
  return {
    Daily_Remind: state.Daily_Remind
  };
}

export default connect(mapStateToProps)(LuckyMoneyModal);
