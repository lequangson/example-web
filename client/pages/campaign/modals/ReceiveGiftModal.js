import React, { Component, PropTypes } from 'react'
import Modal from 'components/modals/Modal'
import { Link, browserHistory } from 'react-router'
import { connect } from 'react-redux'
import showDefaultAvatar from 'src/scripts/showDefaultAvatar'
import UserPicture from 'components/commons/UserPicture';
import { getAvatar } from 'src/scripts/showDefaultAvatar';
import {GIFT_IMAGES, GIFT_IMAGE} from 'constants/Enviroment';
import { shuffleArray } from 'utils/common';
import showOnlineStatus from 'scripts/showStatus';
import socket from "utils/socket";
import { CDN_URL } from 'constants/Enviroment';
import Snow from 'react-snow-effect';
import $ from 'jquery';
import { ACTIVE_USER } from 'constants/userProfile';
// import sizeMe from 'react-sizeme'
// import Confetti from 'react-confetti'
import './style.css';
import { getMyGifts } from '../Action'


class ReceiveGiftModal extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isOpenModal: false,
      isConfirmModalOpen: false,
      senders: [] // this for show offline notification
    }
    this.showModal = this.showModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.openConfirmModal = this.openConfirmModal.bind(this)
    this.closeConfirmModal = this.closeConfirmModal.bind(this)
  }

  showModal() {
    this.loadAnimation();
    this.setState({
      isOpenModal: true,
      isConfirmModalOpen: false,
    });

  }

  closeModal() {
    this.setState({
      isOpenModal: false,
      isConfirmModalOpen: false,
    });
  }

  openConfirmModal() {
    this.setState({ isConfirmModalOpen: true })
  }

  closeConfirmModal() {
    this.setState({ isConfirmModalOpen: false })
  }

  componentDidMount() {
      socket.on('receive_gift', this.receive_event.bind(this));
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.campaign.need_show_notification_gifts.length && nextProps.campaign.need_show_notification_gifts.length) {
      // this.setState({ senders: nextProps.campaign.need_show_notification_gifts })
      this.setState({
        isConfirmModalOpen: true,
        isOpenModal: false,
        // senders: [this.props.user_profile.current_user, this.props.user_profile.current_user]})
        senders: nextProps.campaign.need_show_notification_gifts
      });
      const _this = this;
      nextProps.campaign.need_show_notification_gifts.forEach(item => {
        _this.props.addActivityAudit(73, item.from_user.identifier, null, null);
      });

    }
  }

  loadAnimation() {
    const love = setInterval(function() {
      const r_num = Math.floor(Math.random() * 40) + 1;
      const r_size = Math.floor(Math.random() * 65) + 10;
      const r_left = Math.floor(Math.random() * 100) + 1;
      const r_bg = Math.floor(Math.random() * 25) + 100;
      const r_time = Math.floor(Math.random() * 5) + 5;

      $('.bg_heart').append("<div class='heart' style='width:" + r_size + "px;height:" + r_size + "px;left:" + r_left + "%;background:rgba(255," + (r_bg - 25) + "," + r_bg + ",1);-webkit-animation:love " + r_time + "s ease;-moz-animation:love " + r_time + "s ease;-ms-animation:love " + r_time + "s ease;animation:love " + r_time + "s ease'></div>");

      $('.bg_heart').append("<div class='heart' style='width:" + (r_size - 10) + "px;height:" + (r_size - 10) + "px;left:" + (r_left + r_num) + "%;background:rgba(255," + (r_bg - 25) + "," + (r_bg + 25) + ",1);-webkit-animation:love " + (r_time + 5) + "s ease;-moz-animation:love " + (r_time + 5) + "s ease;-ms-animation:love " + (r_time + 5) + "s ease;animation:love " + (r_time + 5) + "s ease'></div>");

      $('.heart').each(function() {
          const top = $(this).css("top").replace(/[^-\d\.]/g, '');
          const width = $(this).css("width").replace(/[^-\d\.]/g, '');
          if (top <= -100 || width >= 150) {
              $(this).detach();
          }
    });
    }, 500);
  }

  receive_event = async (data) => {
    if (data.type != 'CAMPAIGN') {
      return;
    }
    this.props.getMyGifts();
    const { user_profile } = this.props;
    const { current_user } = user_profile;
    if (current_user.user_status != ACTIVE_USER) {
      return;
    }

    // await this.setState({ isOpenModal: false, sender: {} });
    this.props.addActivityAudit(73, data.sender.identifier, null, null);
    const senders = [
      {
        from_user: data.sender,
        gift: data.item
      },
    ];
    this.setState({
      isConfirmModalOpen: true,
      senders
    });
  }

  gotoListGiftPage = async () => {
    await this.setState({ isOpenModal: false });
    await this.setState({ isConfirmModalOpen: false });
    browserHistory.push('/my-present-list');
  }

  render() {
    // console.log('isOpenModal', this.state.isOpenModal);
    const totalGifts = this.state.senders.length;
    if (!totalGifts) {
      return null;
    }
    const gender = this.state.senders[0].from_user.gender;
    const giftMessage = this.state.senders[0].gift.message;
    const genderTextForManyGift = gender === 'male' ? 'chàng trai' : 'cô gái';
    return (
      <div>
        {this.state.isOpenModal && <div className="bg_heart"></div>}
        <Modal
          transitionName="modal"
          isOpen={this.state.isConfirmModalOpen}
          onRequestClose={this.closeConfirmModal}
          className="receive-gift-modal l-flex-v-end modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >

          <div className="mbs padding-t10 txt-center">
            <div className="mbm txt-uppercase">
              {totalGifts === 1 ?
                <span className="txt-bold padding-t30 txt-pink txt-xlg">
                  Ai đó đã tặng quà<br />valentine đen cho bạn
                </span>
              :
                <span className="txt-bold txt-pink txt-xlg">
                  Rất nhiều {genderTextForManyGift}<br />đã tặng quà<br />valentine đen cho bạn
                </span>
              }
            </div>
            <div className="mbl txt-lg txt-pink">Nhấp vào hộp quà để mở</div>
          </div>
          <div className="row">
            <div className="col-xs-12 txt-center">
              <div onClick={this.showModal}>
                <img className="mbs img-150" src={`${ CDN_URL }/general/Campaign/gift-box.png`} />
              </div>
            </div>
          </div>
          <button className="modal__btn-close" onClick={this.closeConfirmModal}><i className="fa fa-times"></i></button>
        </Modal>
        <Modal
          transitionName="modal"
          isOpen={this.state.isOpenModal}
          onRequestClose={this.closeModal}
          className="receive-gift-modal open-gift padding-t10 padding-b10 modal__content modal__content"
          overlayClassName="modal__overlay receive-giftoverlay z-index-1400"
          portalClassName="modal"
          contentLabel=""
        >
          {totalGifts === 1 ?
          <div className="mbs container padding-t10 padding-b10">
            <div className="mbl txt-center">
              <img src={this.state.senders[0].gift.photo_url} className="img-100" alt="Gift Image" />
            </div>
            <div className="mbm txt-pink txt-uppercase txt-xlg txt-center">
              {giftMessage}
            </div>
            <div className="row">
              <div className="card col-xs-8 col-xs-offset-2 padding-0 mbm">
                <UserPicture goToProfileCallback={this.closeModal} go_to_profile={true} user={this.state.senders[0].from_user} haveChatButton={false} />
              </div>
            </div>
          </div>
          :<div className="mbs confettiful">
            <div className="mbl txt-center">
              <img src={GIFT_IMAGES} className="img-150" alt="Gift Image" />
            </div>
            <div className="mbm txt-pink txt-uppercase txt-xlg txt-center">
              Chúc {gender === 'male' ? 'em' : 'anh'} valentine đen vui vẻ!
            </div>
            <div className="mbm receive-gift__list txt-center">
            {
              this.state.senders.slice(0, 3).map((sender, i) => { // maximum display 3 user
                const avatar = getAvatar(sender.from_user);
                return (
                  <div className="category__item category__item--1" key={i}>
                    <div className="category__image">
                      <img src={avatar.large_image_url} className="img-full img-round" alt="SenterImage"/>
                    </div>
                  </div>
                )
              })
            }
            </div>
            <div className="txt-center">
              <button className="btn btn--6 btn--p" onClick={this.gotoListGiftPage} style={{'zIndex': 99999}} >
                Xem chi tiết người tặng
              </button>
            </div>
          </div>
          }
          <button
            className="modal__btn-close"
            onClick={this.closeModal}
          >
            <i className="fa fa-times"></i>
          </button>
        </Modal>
      </div>
    )
  }
}

ReceiveGiftModal.propTypes = {
  Mode: PropTypes.string,
}


function mapStateToProps(state) {
  return {
    user_profile: state.user_profile,
    campaign: state.campaign
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getMyGifts: () => dispatch(getMyGifts())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ReceiveGiftModal);
