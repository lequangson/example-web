import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router';
import * as UserDevice from 'utils/UserDevice';
import { ANDROID_URL, IOS_URL } from 'constants/Enviroment'
import IosDownloadButton from 'components/commons/Buttons/IosDownloadButton'
import AndroidDownloadButton from 'components/commons/Buttons/AndroidDownloadButton'
import { CDN_URL } from '../../../constants/Enviroment';

class WomenDayFooter extends Component {
  state = {  }
  render() {
    return (
      <footer className="lp-footer l2">
        <section className="lp-well lp-well--2">
          <div className="row">
            <div className="small-12 column l-flex">
              <img src={`${CDN_URL}/general/WomenDayPage/Coke.png`} />
              <h2 className="txt-xxlg mbxlg">Tải ứng dụng hẹn hò miễn phí Ymeet.me</h2>
            </div>
          </div>
          <div className="row">
            <div className="small-12 column">
              <div className="lp-installation-wrap">
                <div className="l-flex--jc l-flex--ac l-flex">
                  {(UserDevice.getName() === 'pc' || UserDevice.getName() === 'ios') &&
                    <IosDownloadButton redirect_url={IOS_URL} />
                  }
                  {UserDevice.getName() === 'pc' && // || UserDevice.getName() === 'android'
                    <AndroidDownloadButton redirect_url={ANDROID_URL}/>
                  }
                </div>
              </div>
            </div>
          </div>
        </section>
        <div className="lp-footer__content">
          <div className="row">
            <div className="small-12 column">
              <div className="lp-footer__facebook l-flex--jc l-flex--ac l-flex mblg">
                <div className="fb-page" data-href="https://www.facebook.com/ymeet.me/"
                data-width="280"
                data-hide-cover="false"
                data-show-facepile="true"></div>
              </div>
            </div>
          </div>
          <div className="row small-collapse">
            <div className="small-12 medium-6 column">
              <ul className="list-inline txt-left-mc">
                <li><Link to="/terms-of-use">Điều khoản sử dụng</Link></li>
                <li><Link to="/privacy">Bảo mật</Link></li>
                <li><Link to="/helps">Trợ giúp</Link></li>
                <li><Link to="/contact">Liên hệ</Link></li>
                <li><a href="https://ymeet.me/blog/" rel='noopener noreferrer' target="_blank">Blog</a></li>
              </ul>
            </div>
            <div className="small-12 medium-6 column">
              <div className="txt-right-mc">Copyright (c) 2017 <a href="http://mmj.vn" target="_blank" rel="noopener noreferrer" className="txt-bold">Media Max Japan</a> All Rights Reserved.</div>
            </div>
          </div>
        </div>
    </footer>
    );
  }
}

export default WomenDayFooter;