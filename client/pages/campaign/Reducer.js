import update from 'immutability-helper';
import {
  GET_CAMPAIGN_REQUEST, GET_CAMPAIGN_SUCCESS, GET_CAMPAIGN_FAILURE,
  GET_GIFT_DICTIONARY_REQUEST, GET_GIFT_DICTIONARY_SUCCESS, GET_GIFT_DICTIONARY_FAILURE,
  GET_MY_GIFTS_REQUEST, GET_MY_GIFTS_SUCCESS, GET_MY_GIFTS_FAILURE,
  // UPDATE_REMAIN_RANDOM_MATCH
} from './Constant';
import {
  CLEAR_NOTIFICATION_SUCCESS, NOTIFICATION_TYPE_VIEW_MY_PRESENT
} from 'constants/userProfile';
import { SAVE_LUCKY_SPINNER_RESULT_SUCCESS } from 'pages/lucky-spinner/Constant';
import _ from 'lodash';
import {
  getUserIdFromIdentifier
} from 'utils/common';

const defaultState = {
  isFetching: false,
  data: {},
  gift_dictionary: [],
  my_gifts: [],
  need_show_notification_gifts: [],
}

export default function campaign(state = defaultState, action) {
  switch (action.type) {
    case GET_CAMPAIGN_SUCCESS:
      return Object.assign({}, state, {
        data: action.response.data.length ? action.response.data[0] : state.data,
      });
    case GET_GIFT_DICTIONARY_SUCCESS:
      return Object.assign({}, state, {
        gift_dictionary: action.response.data,
      });
    case GET_MY_GIFTS_SUCCESS:
    const uniqueIds = [];
      return Object.assign({}, state, {
        my_gifts: action.response.data,
        need_show_notification_gifts: action.response.data.filter(gift => {
          if (gift.have_notification) return false;
          const userId = getUserIdFromIdentifier(gift.from_user.identifier);
          if (uniqueIds.indexOf(userId) === -1) {
            uniqueIds.push(userId);
            return true;
          }
          return false;
        }),
      });
    case CLEAR_NOTIFICATION_SUCCESS:
      if (action.extentions.type === NOTIFICATION_TYPE_VIEW_MY_PRESENT) {
        const newState = update(state, {my_gifts: {$apply: my_gifts => my_gifts.map((item, index) => {
            if (item.is_new) {
              return {...item, is_new: false}
            }
            else {
              return item;
            }
          })}});
        return newState;
      }
      return state;
    case SAVE_LUCKY_SPINNER_RESULT_SUCCESS:
      {
        let new_number_free_play = 0;
        if (action.extentions.gift_code === 9) {
          new_number_free_play = state.data.number_lucky_spinner_free == 0 ? 1 : state.data.number_lucky_spinner_free;
        } else {
          new_number_free_play = state.data.number_lucky_spinner_free > 0 ? state.data.number_lucky_spinner_free - 1 : 0;
        }
        return Object.assign({}, state, {
          data: {
            ...state.data,
            number_lucky_spinner_free: new_number_free_play,
          }
        });
      }
    default:
      return state;
  }
}