import {
  GET_CAMPAIGN_REQUEST, GET_CAMPAIGN_SUCCESS, GET_CAMPAIGN_FAILURE,
  GET_GIFT_DICTIONARY_REQUEST, GET_GIFT_DICTIONARY_SUCCESS, GET_GIFT_DICTIONARY_FAILURE,
  GET_MY_GIFTS_REQUEST, GET_MY_GIFTS_SUCCESS, GET_MY_GIFTS_FAILURE,
  SEND_GIFT_REQUEST, SEND_GIFT_SUCCESS, SEND_GIFT_FAILURE,
  UPDATE_REMAIN_RANDOM_MATCH
} from './Constant';
import { CALL_API } from 'middleware/api';
import {
  API_GET,
  API_POST
} from 'constants/Enviroment';

export function getCampaign() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'promotion_campaigns',
      params: {},
      authenticated: true,
      types: [
        GET_CAMPAIGN_REQUEST, GET_CAMPAIGN_SUCCESS, GET_CAMPAIGN_FAILURE,
      ],
    },
  }
}

export function sendGift(to_user_identifier, gift_id, price) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'send_gift',
      params: {
        to_user_identifier,
        gift_id
      },
      authenticated: true,
      types: [
        SEND_GIFT_REQUEST,
        SEND_GIFT_SUCCESS,
        SEND_GIFT_FAILURE
      ],
      extentions: {
        price
      }
    }
  }
}
export function getGiftDictionary() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'gifts',
      params: {},
      authenticated: true,
      types: [
        GET_GIFT_DICTIONARY_REQUEST, GET_GIFT_DICTIONARY_SUCCESS, GET_GIFT_DICTIONARY_FAILURE,
      ],
    },
  }
}

export function getMyGifts() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'my_gifts',
      params: {},
      authenticated: true,
      types: [
        GET_MY_GIFTS_REQUEST, GET_MY_GIFTS_SUCCESS, GET_MY_GIFTS_FAILURE,
      ],
    },
  }
}

export function updateRemainRandomMatch() {
  return {
    type: UPDATE_REMAIN_RANDOM_MATCH,
  }
}