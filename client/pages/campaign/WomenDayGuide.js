import React, { Component, PropTypes } from 'react';
import WomenDayHeader from './common/WomenDayHeader'
import WomenDayFooter from './common/WomenDayFooter'
import { CDN_URL } from '../../constants/Enviroment';
import * as ymmStorage from 'utils/ymmStorage';

class WomenDayGuide extends Component {
    static propTypes = {
        className: PropTypes.string,
    };

    componentDidMount() {
      this.landingPageScript();
      if (window.FB) {
        FB.XFBML.parse();
      }
    }

    componentWillUnmount() {
      const h = document.getElementsByTagName('html')[0];
      h.classList.remove('landingpage');
      if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        const html = document.getElementsByTagName('html')[0];
        html.classList.add('no-touch');
      }
      ymmStorage.removeItem('previous_url');
    }

    landingPageScript() {
      // remove no-touch class
      const html = document.getElementsByTagName('html')[0];
      html.classList.remove('no-touch');
      html.classList.add('landingpage');

      // hamburger button
      const ham = document.querySelector('.js-hamburger');
      const nav = document.querySelector('.js-nav');
      if (ham) {
        ham.addEventListener('click', () => {
          ham.classList.toggle('is-active');
          nav.classList.toggle('is-show');
        });
      }
    }

    constructor(props) {
        super(props);
    }

    render() {
        return (
          <div className="wd">
            <WomenDayHeader {...this.props}/>
              <section className="wd-hero wd">
                <div className="wd-hero__content full-width">
                  <div className="small-12 medium-12 large-7 column">
                  <img className="wd-hero__decor-1 padding-rl10" src={`${CDN_URL}/general/WomenDayPage/Popcorn-hero.png`} alt="Sale" />
                  </div>
                  <div className="small-12 medium-12 large-5 column">
                    <img className="wd-hero__decor-1 padding-rl10" src={`${CDN_URL}/general/WomenDayPage/sale.png`} alt="Sale" />
                  </div>
                </div>
              </section>
              <section className="wd-well wd-well--1">
                <img className="wd-well__decor-1" src={`${CDN_URL}/general/WomenDayPage/Popcorn-box.png`} alt="Tim-nguoi-yeu-de-dang-voi-Ymeetme" />
                <div className="row">
                  <div className="small-12 column">
                    <h3 className="wd-heading txt-h4 txt-bold">1. THỜI GIAN TỔ CHỨC: 05/03 - 05/04</h3>
                    <h3 className="wd-heading txt-h4 txt-bold">2. ĐỊA ĐIỂM: Toàn quốc</h3>
                    <h3 className="wd-heading txt-h4 txt-bold">3. MỤC ĐÍCH:</h3>
                    <p>Gặp để Yêu: giúp các cặp được ghép đôi thành công trên YmeetMe có trải nghiệm hẹn hò ngoài đời thực, phát triển tình cảm nhanh chóng và hiệu quả hơn.</p>
                    <h3 className="wd-heading txt-h4 txt-bold">4. THỂ LỆ:</h3>
                    <p>Trong khoảng thời gian chương trình diễn ra, 20 cặp ghép đôi thành công và đồng ý có buổi hẹn đầu tiên thông qua ứng dụng chat YmeetMe và gửi thông tin sớm nhất về BTC sẽ được tặng một cặp vé xem phim 2D bất kì tại các rạp CGV trên toàn quốc.</p>
                    <h3 className="wd-heading txt-h4 txt-bold">5. ĐIỀU KIỆN THAM GIA:</h3>
                    <div>Bạn là:
                      <ul>
                        <i className="fa fa-check"></i> Thành viên nam và nữ đã đăng ký thành công tài khoản trên YmeetMe<br/>
                        <i className="fa fa-check"></i> Bạn còn độc thân trong thời điểm tham gia chương trình (chưa kết hôn hoặc đã ly hôn)<br/>
                        <i className="fa fa-check"></i> Bạn muốn tìm bạn khác giới để tìm hiểu và hướng tới mối quan hệ nghiêm túc, lâu dài<br/>
                      </ul>
                    </div>
                  </div>
                  <div className="small-12 column">
                    <h3 className="wd-heading txt-h4 txt-bold">6. QUY TRÌNH THAM GIA:</h3>
                  </div>
                </div>
                <div className="row match-step-background">
                  <div className="small-12 medium-offset-1 medium-7 column">
                    <img src={`${CDN_URL}/general/WomenDayPage/Match-step.png`}/>
                    <p>
                      Bước 1: Ghép đôi thành công trong khoảng thời gian chương trình diễn ra<br />
                      (ghép đôi thành công là khi cả hai người cùng gửi thích cho nhau. Không áp<br />
                      dụng khi người dùng sử dụng tính năng Làm quen hoặc Ghép đôi ngẫu nhiên).
                    </p>
                  </div>
                  <div className="small-6 medium-3 column">
                    <div className="phone-arrow left">
                      <img src={`${CDN_URL}/general/WomenDayPage/Phone_1.png`}/>
                    </div>
                  </div>
                  <div className="small-6 medium-3 column">
                    <div className="phone-arrow right">
                      <img src={`${CDN_URL}/general/WomenDayPage/Phone_2.png`}/>
                    </div>
                  </div>
                  <div className="small-12 medium-5 middle-content column">
                    <p>
                      <b>Bước 2:</b> Trò chuyện cùng người ấy trên YmeetMe và mời họ (hoặc được mời) đồng ý tham gia buổi hẹn đầu tiên ngoài đời thực.
                    </p>
                  </div>
                  <div className="small-12 medium-4 column">
                  </div>
                  <div className="small-12 medium-9 column">
                    <img className="mbs" src={`${CDN_URL}/general/WomenDayPage/Mail.png`}/>
                    <div>
                      <b>Bước 3:</b> Gửi email cho BTC chương trình tại địa chỉ <b>contact@ymeet.me</b> với tựa đề [Tên bạn - Hẹn hò cùng YmeetMe] kèm theo các thông tin sau:
                      <ul>
                        <i className="fa fa-check"></i> ID người dùng của bạn và nửa kia (ấn vào ảnh đại diện trên YmeetMe để xem dòng ID bên dưới)<br/>
                        <i className="fa fa-check"></i> Ảnh chụp màn hình tin nhắn mời hẹn hò thành công trên ứng dụng chat YmeetMe<br/>
                        <i className="fa fa-check"></i> Ảnh chụp chứng minh thư hoặc bằng lái xe của bạn (nếu bạn đã xác thực tài khoản trên YmeetMe bằng chứng minh thư hoặc bằng lái xe thì có thể bỏ qua mục này)<br/>
                        <i className="fa fa-check"></i> Số điện thoại của bạn và người ấy<br/>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="row mbm">
                  <div className="small-12 medium-4 column">
                    <div className="txt-center mbs">
                      <img src={`${CDN_URL}/general/WomenDayPage/service.png`} alt="Chăm sóc khách hàng" />
                    </div>
                    <div>
                      <b>Bước 4:</b> Bộ phận CSKH của YmeetMe sẽ liên hệ lại với bạn để xác thực thông tin và thông báo nhận giải
                    </div>
                  </div>
                  <div className="small-12 medium-4 column">
                    <div className="txt-center mbs">
                      <img src={`${CDN_URL}/general/WomenDayPage/ticket.png`} alt="Vé phim" />
                    </div>
                    <div>
                    <b>Bước 5:</b> Nếu đạt yêu cầu, bạn sẽ nhận được thư xác nhận của YmeetMe, và một mã voucher qua email để quy đổi vé xem phim 2D hợp lệ với CGV.
                    </div>
                  </div>
                  <div className="small-12 medium-4 column">
                    <div className="txt-center mbs">
                      <img src={`${CDN_URL}/general/WomenDayPage/Popcorn-box-2.png`} alt="Vé phim" />
                    </div>
                    <div>
                    <b>Bước 6:</b> Hãy cùng xem phim, trò chuyện, hẹn hò thật vui vẻ và chia sẻ những khoảnh khắc hạnh phúc qua ảnh hoặc bài viết tới YmeetMe nhé!
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="small-12 column">
                    <h3 className="wd-heading txt-h4 txt-bold">7. QUY ĐỊNH CHUNG</h3>
                    <p className="txt-thin">
                      Mỗi người dùng có cơ hội được nhận <b>tối đa 01</b> vé xem phim. <br />
                      Khi một cặp đôi gửi email tham dự chương trình, cả hai bạn sẽ đều nhận được thông tin xác thực tương đương từ YmeetMe. YmeetMe từ chối mọi khiếu nại liên quan đến việc sử dụng mã e-voucher sau khi người dùng đã được trao thưởng. <br />
                      Voucher chỉ có thể áp dụng khi mua vé trực tuyến qua ứng dụng CGV hoặc qua trang web https://www.cgv.vn/. Voucher không có giá trị quy đổi tại quầy.<br />
                      YmeetMe không hỗ trợ mua hoặc quy đổi vé xem phim. Mọi vấn đề liên quan đến việc mua hoặc quy đổi vé xem phim, vui lòng liên hệ tổng đài hỗ trợ của CGV (1900 6017)<br />
                      YmeetMe hoàn toàn không chịu trách nhiệm về các vấn đề phát sinh sau buổi hẹn hò của các cặp đôi. Gặp được là duyên, nhưng bên nhau là phận!
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="small-12 column">
                    <h3 className="wd-heading txt-h4 txt-bold">8. CÂU HỎI THƯỜNG GẶP</h3>
                    <p>
                      <b>Q: Nếu tôi được nhiều người mời đi hẹn hò gặp mặt thì tôi có thể đồng ý và nhận cặp vé xem phim với tất cả bọn họ không?</b> <br />
                      A: Bạn có thể đồng ý đi gặp mặt tất cả những người đã mời bạn đi hẹn hò, điều này sẽ giúp bạn tăng cơ hội tìm hiểu và làm quen, tuy nhiên bạn chỉ có thể nhận 01 cặp vé xem phim từ YmeetMe với một người duy nhất.<br />
                      <b>Q: Nếu tôi được nhiều người mời đi hẹn hò gặp mặt hoặc tôi được nhiều người đồng ý hẹn hò thì làm sao chọn ra được một người để nhận giải cùng? </b><br />
                      A: Bạn sẽ là người quyết định xem mình muốn nhận giải thưởng này cùng ai, và bạn nên cho họ biết điều đó càng sớm càng tốt (vì họ cũng có thể nhận được nhiều lời mời tương tự và biết đâu đã đồng ý với ai khác!). YmeetMe sẽ dựa trên thời điểm email được gửi về để chọn ra các cặp đôi hợp lệ sớm nhất để trao giải.
                    </p>
                  </div>
                </div>
              </section>
            <WomenDayFooter {...this.props}/>
          </div>
        );
    }
}

export default WomenDayGuide;
