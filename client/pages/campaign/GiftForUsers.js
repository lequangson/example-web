import React, { Component, PropTypes } from 'react'
import { ChooseGifts } from './buttons';

class GiftForUsers extends Component {
    static propTypes = {
        nick_name: PropTypes.string,
    };
    constructor(props) {
        super(props)
    }
    render() {
        const { nick_name, identifier, compatibility } = this.props.user;
        return (
            <div className="panel">
                <div>
                    <div className="panel__heading">
                        <h3 className="panel__title">Tặng quà cho {nick_name}</h3>
                    </div>
                    <div className="panel__body">
                        <div className="container">
                            <div className="row">
                            <div className="col-xs-12">
                                <div className="row">
                                <div className="col-xs-8 col-xs-offset-2">
                                    <ChooseGifts
                                        identifier={identifier}
                                        compatibility={compatibility}
                                        current_user={this.props.current_user}
                                    />
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default GiftForUsers;
