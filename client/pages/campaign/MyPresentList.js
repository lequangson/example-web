import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import HeaderContainer from '../../containers/HeaderContainer'
import PresentItem from './list-card/PresentItem'
import {
  NOTIFICATION_TYPE_VIEW_MY_PRESENT
} from 'constants/userProfile'
import * as ymmStorage from 'utils/ymmStorage';
import { getMyGifts } from 'pages/campaign/Action';

class MyPresentList extends Component {

	componentWillMount() {
    this.props.getMyGifts();
	  this.props.clearNotification(NOTIFICATION_TYPE_VIEW_MY_PRESENT);
	}

	render() {
		const my_gifts = this.props.campaign.my_gifts
		return (
			<div>
				<HeaderContainer {...this.props} />
				<div className="site-content">
					<div className="container">
						<div className="row">
							<div className="col-xs-12">
								<h2 className="txt-heading">Danh sách quà tặng</h2>
							</div>
						</div>

						{ my_gifts.map((gift, i) => {
						  return (
							<PresentItem
								key={i}
								user={gift.from_user}
								giftInfo={gift.gift}
							/>
						  )
						}
						)}
					</div>
				</div>
			</div>
		);
	}
}

MyPresentList.propTypes = {
	campaign: PropTypes.object,
	clearNotification: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
	campaign: state.campaign
  };
};

function mapDispachToProps(dispatch) {
  return {
    getMyGifts: () => dispatch(getMyGifts())
  }
}

export default connect(
  mapStateToProps, mapDispachToProps
)(MyPresentList);
