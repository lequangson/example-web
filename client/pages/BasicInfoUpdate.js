import React from 'react'
import BasicInfoUpdateContainer from '../components/BasicInfoUpdate'
import HeaderContainer from '../containers/HeaderContainer'

const BasicInfoUpdate = props => (
  <div>
    <HeaderContainer {...props} />
    <BasicInfoUpdateContainer {...props} />
  </div>
)

export default BasicInfoUpdate
