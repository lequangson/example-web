import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import HeaderContainer from '../containers/HeaderContainer'
import ChangeAvatarContainer from '../containers/ChangeAvatarContainer'

const ChangeAvatar = props => (
  <div>
    { props.user_profile.current_user.welcome_page_completion > 3
      && <HeaderContainer {...props} />
    }
    <ChangeAvatarContainer id={props.params.id} {...props} />
  </div>
)

ChangeAvatar.propTypes = {
  params: PropTypes.object,
  user_profile: PropTypes.object,
}

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {}
}

const ChangeAvatarPage = connect(mapStateToProps,mapDispatchToProps)(ChangeAvatar)

export default ChangeAvatarPage
