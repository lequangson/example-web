import { connect } from 'react-redux';
import { getFacePartnersRequest, getFacePartners, shuffleFacePartners } from 'pages/similar-face/Action';
import { updatePreviousPage } from 'actions/Enviroment';
import {
  getSimilarFaceToMyFriendsRequest, getSimilarFaceToMyFriends,
} from './Action';
import ListSimilarFaceToMyYmmFriends from './ListSimilarFaceToMyYmmFriends';

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    getSimilarFaceToMyFriends: (current_user_id, user_id, filter_mode) => {
      dispatch(getSimilarFaceToMyFriendsRequest());
      dispatch(getSimilarFaceToMyFriends(current_user_id, user_id, filter_mode));
    },
    getFacePartners: (props) => {
      dispatch(getFacePartnersRequest());
      dispatch(getFacePartners(props));
    },
    shuffleFacePartners: () => {
      dispatch(shuffleFacePartners())
    },
    updatePreviousPage: (page) => {
      dispatch(updatePreviousPage(page))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListSimilarFaceToMyYmmFriends)
