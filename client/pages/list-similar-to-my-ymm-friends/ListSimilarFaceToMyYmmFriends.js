import React, { Component, PropTypes } from 'react';
import { isEmpty } from 'lodash';
import InfiniteListItem from 'components/infinite-list-items/InfiniteListItem';
import {
  GRID_VIEW_USER_CARD, DETAIL_VIEW_USER_CARD,
  NEW_PHOTO_CARD, SIMILAR_FACE_CARD,
} from 'components/infinite-list-items/Constant';
import addHeader from 'pages/similar-face/addHeader';
import SimilarToMyYmmFriendsEmpty from 'pages/similar-face/empty-page/SimilarToMeEmpty';
import { getUserIdFromIdentifier } from 'utils/common';

class ListSimilarFaceToMyYmmFriends extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {};
  // }

  componentWillMount() {
    // if (this.props.similar_face_to_my_ymm_friends.users.length === 0 &&
    //   !this.props.similar_face_to_my_ymm_friends.isFetching) {
      const { user_id } = this.props.params;
      const { current_user } = this.props.user_profile;
      if (!current_user.identifier) {
        return;
      }
      // filter by oposite gender
      const filter_mode = 1;
      this.props.getSimilarFaceToMyFriends(current_user.identifier, user_id, filter_mode);
    // }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.params.user_id != nextProps.params.user_id ||
      (isEmpty(this.props.user_profile.current_user) && !isEmpty(nextProps.user_profile.current_user))
    ) {
      const filter_mode = 1;
      const { current_user } = this.props.user_profile;
      this.props.getSimilarFaceToMyFriends(nextProps.user_profile.current_user.identifier, nextProps.params.user_id, filter_mode);
    }
  }

  render() {
    const { users } = this.props.similar_face_to_my_ymm_friends;
    if (users.length === 0) {
      return <SimilarToMyYmmFriendsEmpty {...this.props} />
    }
    return (
      <div className="sticky-footer">
        <InfiniteListItem
          listItems={this.props.similar_face_to_my_ymm_friends.users}
          loadingDataStatus={this.props.similar_face_to_my_ymm_friends.isFetching}
          reachEnd={this.props.similar_face_to_my_ymm_friends.reachEnd}
          viewMode={SIMILAR_FACE_CARD}
          current_user={this.props.user_profile.current_user}
          pageSource='SIMILAR_FACE_TO_MY_YMM_FRIENDS_PAGE'
          onClick={this.props.onClick}
        />
      </div>
    );
  }
}

export default addHeader(ListSimilarFaceToMyYmmFriends, 'similar_face_to_my_ymm_friends');
