import {
  GET_SIMILAR_TO_MY_YMM_FRIEND_REQUEST, GET_SIMILAR_TO_MY_YMM_FRIEND_SUCCESS, GET_SIMILAR_TO_MY_YMM_FRIEND_FAILURE
} from './Constant';
import { CALL_API } from 'middleware/api';
import {
  API_GET,
} from 'constants/Enviroment';


export function getSimilarFaceToMyFriendsRequest() {
  return {
    type: GET_SIMILAR_TO_MY_YMM_FRIEND_REQUEST,
  }
}

export function getSimilarFaceToMyFriends(current_user_id, user_id, filter_mode) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'similar_faces_by_user',
      params: {
        user_id,
        filter_mode,
        current_user_id
      },
      authenticated: true,
      types: [GET_SIMILAR_TO_MY_YMM_FRIEND_REQUEST, GET_SIMILAR_TO_MY_YMM_FRIEND_SUCCESS, GET_SIMILAR_TO_MY_YMM_FRIEND_FAILURE],
      extentions: {
        target_id: user_id,
      }
    },
  }
}

export function getSimilarFaceToMyFriendsFailure() {
  return {
    type: GET_SIMILAR_TO_MY_YMM_FRIEND_FAILURE,
  }
}