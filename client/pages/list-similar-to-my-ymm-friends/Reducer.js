import {
  GET_SIMILAR_TO_MY_YMM_FRIEND_REQUEST, GET_SIMILAR_TO_MY_YMM_FRIEND_SUCCESS, GET_SIMILAR_TO_MY_YMM_FRIEND_FAILURE
} from './Constant';
import { MODAL_SIMILAR_TO_MY_YMM_FRIENDS } from 'pages/similar-face/Constant';

const defaultState = {
  isFetching: false,
  users: [],
  reachEnd: false,
  totalItems: 0,
  modalType: MODAL_SIMILAR_TO_MY_YMM_FRIENDS,
  headerTitle: 'Giống bạn bè',
  shareTitle: 'Kết bạn trò chuyện với 0 người giống tôi trên Ymeet.me!',
  mode: 1,
  target_id: '',
  target_name: '',
  sharing_picture: '',
}

export default function similar_face_to_my_ymm_friends(state = defaultState, action) {
  switch (action.type) {
    case GET_SIMILAR_TO_MY_YMM_FRIEND_REQUEST:
      return Object.assign({}, state, {
        users: [],
        isFetching: true
      });
    case GET_SIMILAR_TO_MY_YMM_FRIEND_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_SIMILAR_TO_MY_YMM_FRIEND_SUCCESS:
    {
      const totalItem = action.response.data.similar_pictures ?
        action.response.data.similar_pictures.length : state.totalItems;
      return Object.assign({}, state, {
        users: action.response.data.similar_pictures,
        reachEnd: true,
        isFetching: false,
        totalItems: totalItem,
        target_id: action.extentions.target_id,
        shareTitle: `Kết bạn trò chuyện với ${totalItem} người giống ${action.response.data.user_picture.name} trên Ymeet.me!`,
        target_name: action.response.data.user_picture.name,
        sharing_picture: action.response.data.sharing_picture.url
      });
    }
    default:
      return state;
  }
}