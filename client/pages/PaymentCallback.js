import React from 'react'
import PaymentCallbackContainer from '../containers/PaymentCallbackContainer'

const PaymentCallback = props => (
  <div>
    <PaymentCallbackContainer {...props} />
  </div>
)

export default PaymentCallback
