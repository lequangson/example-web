import React, { PropTypes } from 'react'
import MyAnswerContainer from '../containers/MyAnswerContainer'
import HeaderContainer from '../containers/HeaderContainer'

const MyAnswerPage = props => (
  <div>
    <HeaderContainer {...props} />
    <MyAnswerContainer {...props} />
  </div>
)

export default MyAnswerPage
