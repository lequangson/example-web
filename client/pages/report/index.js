import { connect } from 'react-redux';
import q from 'q';
import Report from './Report';
import { reportBadUserRequest, reportBadUser } from './Action';

function mapStateToProps(state) {
  return state
}

function mapDispachToProps(dispatch) {
  return {
    reportBadUser: (params) => {
      const defer = q.defer()
      dispatch(reportBadUserRequest())
      dispatch(reportBadUser(params)).then(() => {
        defer.resolve(true)
      }).catch(() => {
        defer.reject(false)
      })
      return defer.promise
    },
  }
}

export default connect(mapStateToProps, mapDispachToProps)(Report)
