import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import _ from 'lodash';
import { getUserByIdentifierFromAppState } from 'utils/common';
import {
  LOADING_DATA_TEXT,
} from 'constants/Enviroment';
import {
  REPORT_BAD_USER_REASON_MIN_LENGTH,
  REPORT_BAD_USER_REASON_MAX_LENGTH
} from './Constant';
import {
  REPORT_BAD_USER_REASON_MIN_LENGTH_MSG,
  REPORT_BAD_USER_REASON_MAX_LENGTH_MSG,
} from 'constants/TextDefinition';
import MediaProfile from 'components/commons/MediaProfile';
import SelectEditable from 'components/commons/SelectEditable';
import * as ymmStorage from 'utils/ymmStorage';
import Header from 'components/headers/Header'

class Report extends Component {
  constructor() {
    super()
    this.state = {
      isActiveSubmit: false,
      identifier: '',
      selectValue: '',
      reportSuccess: false,
      clicked: false,
    }

    this.formValidate = this.formValidate.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.handleSelectChange = this.handleSelectChange.bind(this)
  }

  componentWillMount() {
    if(!ymmStorage.getItem('keep')) {
      ymmStorage.removeItem('reason')
      ymmStorage.removeItem('detailReason')
    }
    if (ymmStorage.getItem('reason')) {
      this.setState({ selectValue: ymmStorage.getItem('reason') })
    }
    this.setState({ identifier: this.props.params.identifier })
    const selected_user = getUserByIdentifierFromAppState(this.props.params.identifier, this.props, this.props.search.search_mode)
    if (_.isEmpty(selected_user)) {
      this.props.getUserProfileByIdentifier(this.props.params.identifier);
    } else {
      this.setState({ selected_user })
    }
  }

  componentDidMount() {
    this.setState({
      isActiveSubmit: Boolean(!this.showErrorMsg() && (this.state.selectValue || ymmStorage.getItem('reason'))),
    })
  }

  componentWillUnmount() {
    ymmStorage.removeItem('keep')
  }

  handleClick() {
    if (this.state.clicked) {
      return
    }
    this.setState({ clicked: true })
    // Prevent user inspect element and remove disabled attribute
    if (!this.state.isActiveSubmit) return
    const reasonText = this.textarea.value
    this.props.reportBadUser({
      'bad_user_report[reported_user_identifier]': this.state.identifier,
      'bad_user_report[bad_user_report_type]': this.state.selectValue,
      'bad_user_report[description]': reasonText,
    }).then(() => {
      ymmStorage.removeItem('detailReason');
      ymmStorage.removeItem('reason');
      this.setState({ reportSuccess: true, clicked: false })
    }).catch(() => {
      this.state({ clicked: false })
    })
  }

  formValidate() {
    ymmStorage.setItem('detailReason', this.textarea.value);
    // showErrorMsg() return '' -> textarea length valid
    this.setState({
      isActiveSubmit: Boolean(!this.showErrorMsg() && (this.state.selectValue || ymmStorage.getItem('reason'))),
    })
  }

  handleSelectChange(val) {
    // showErrorMsg() return '' -> textarea length valid
    this.setState({ selectValue: val })
    this.setState({ isActiveSubmit: Boolean(val && !this.showErrorMsg()) })
    ymmStorage.setItem('reason', val);
  }

  showErrorMsg() {
    const textareaLength = ymmStorage.getItem('detailReason') ? ymmStorage.getItem('detailReason').replace(/(\r\n|\n| |\r)/g, '').length : 0
    let errMsg = ''

    if (textareaLength > REPORT_BAD_USER_REASON_MAX_LENGTH) {
      errMsg = REPORT_BAD_USER_REASON_MAX_LENGTH_MSG
    }

    if (textareaLength < REPORT_BAD_USER_REASON_MIN_LENGTH) {
      errMsg = REPORT_BAD_USER_REASON_MIN_LENGTH_MSG
    }

    return errMsg
  }

  render() {
    const { dictionaries } = this.props
    const keep = ymmStorage.getItem('keep')
    const detailReason = keep ? ymmStorage.getItem('detailReason') : ''
    const reason = keep ? ymmStorage.getItem('reason') : '1000'
    if (
      _.isEmpty(dictionaries) ||
      (_.isEmpty(this.state.selected_user) && _.isEmpty(this.props.user_profile.selected_user))
    ) {
      return <div>{LOADING_DATA_TEXT}</div>
    }
    let user = this.state.selected_user
    if (_.isEmpty(user)) {
      user = this.props.user_profile.selected_user
    }
    return (
      <div>
        <Header {...this.props} />
        <div className="site-content">
          <div className="container">
            <div className="row mbl">
              <div className="col-xs-12">
                <h3 className="txt-heading">Báo cáo người dùng vi phạm</h3>
                <MediaProfile user={user} />
              </div>
            </div>
          </div>
          { !this.state.reportSuccess && <div>
            <div className="panel">
              <div className="panel__heading mbm">
                <h3 className="panel__title">
                  Lý do báo cáo <span className="txt-sm txt-thin txt-italic txt-red"> (bắt buộc)</span>
                </h3>
              </div>
              <div className="panel__body mbm">
                <div className="container">
                  <div className="row">
                    <div className="col-xs-12">
                      <SelectEditable
                        name="bad_user_report[bad_user_report_type]"
                        value={reason}
                        id="bad_user_report_type"
                        className="form__input--b"
                        data={dictionaries.bad_user_report_types}
                        disabled_update_data
                        select_option_text="Chọn một lý do"
                        onChange={this.handleSelectChange}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="panel">
              <div className="panel__heading mbm">
                <h3 className="panel__title">
                  Lý do chi tiết <span className="txt-sm txt-thin txt-italic txt-red"> (bắt buộc)</span>
                </h3>
              </div>
              <div className="panel__body mbs">
                <div className="container">
                  <div className="row">
                    <div className="col-xs-12">
                      <textarea
                        placeholder="VD: Tôi quen người này và người này đã có gia đình"
                        rows="5"
                        ref={(ref) => {
                          this.textarea = ref
                        }}
                        onChange={this.formValidate}
                        defaultValue={detailReason}
                      ></textarea>
                      <span className="txt-sm txt-red">{this.showErrorMsg()}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container mbl">
              <div className="row">
                <div className="col-xs-12 mbl">
                  <div className="mbs txt-bold">Chú ý:</div>
                  <ul className="list-unstyled txt-red">
                    <li>* Báo cáo phải tuân thủ <Link to="/terms-of-use">Điều khoản sử dụng</Link> của Ymeet.me</li>
                    <li>* Việc báo cáo nhằm xây dựng một cộng đồng văn minh,
                    không sử dụng cho mục đích cá nhân.</li>
                  </ul>
                </div>
              </div>
              <div className="row">
                <div className="col-xs-6">
                  <Link
                    to={`/profile/${this.props.params.identifier}`}
                    className="btn btn--b"
                  >Hủy</Link>
                </div>
                <div className="col-xs-6">
                  <button
                    className="btn btn--p btn--b mbs"
                    disabled={this.state.isActiveSubmit ? '' : 'disabled'}
                    onClick={this.handleClick}
                  >Gửi báo cáo</button>
                </div>
              </div>
            </div>
          </div>
          }

          { this.state.reportSuccess && <div className="container">
            <div className="row mbl">
              <div className="col-xs-12">
                <p>
                  Bạn đã gửi báo cáo vi phạm thành công. <br />
                  Chúng tôi sẽ xác nhận nội dung, trong trường hợp đúng là người dùng này vi phạm,
                  sẽ có biện pháp xử lý kịp thời.
                </p>
                <Link to={`/profile/${this.state.identifier}`} className="btn btn--b">Quay lại</Link>
              </div>
            </div>
          </div>
          }
        </div>
      </div>
    )
  }
}

Report.propTypes = {
  params: PropTypes.object.isRequired,
  user_profile: PropTypes.object,
  dictionaries: PropTypes.object,
  reportBadUser: PropTypes.func,
  getUserProfileByIdentifier: PropTypes.func,
}

export default Report
