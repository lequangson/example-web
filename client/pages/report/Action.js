import { CALL_API } from 'middleware/api';
import { API_POST } from 'constants/Enviroment';
import {
  REPORT_BAD_USER_REQUEST, REPORT_BAD_USER_SUCCESS, REPORT_BAD_USER_FAILURE,
} from './Constant';

export function reportBadUserRequest() {
  return {
    type: REPORT_BAD_USER_REQUEST,
  }
}

export function reportBadUser(params) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'bad_user_reports',
      params,
      authenticated: true,
      types: [REPORT_BAD_USER_REQUEST, REPORT_BAD_USER_SUCCESS, REPORT_BAD_USER_FAILURE],
    },
  }
}
export function reportBadUserFailure() {
  return {
    type: REPORT_BAD_USER_FAILURE,
  }
}