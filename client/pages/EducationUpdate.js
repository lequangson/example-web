import React, { PropTypes } from 'react'
import EducationUpdateContainer from '../containers/EducationUpdateContainer'
import HeaderContainer from '../containers/HeaderContainer'

const EducationUpdate = props => (
  <div>
    <HeaderContainer {...props} />
    <EducationUpdateContainer page={props.params.page} {...props} />
  </div>
)

EducationUpdate.propTypes = {
  params: PropTypes.object.isRequired,
}

export default EducationUpdate
