/* global io:true */

import React, { PropTypes } from 'react'
import AppContainer from '../containers/AppContainer'

import socket from "../utils/socket";

const App = props => (
  <AppContainer {...props} socket={socket}>
    { React.cloneElement(props.children, { ...props }) }
  </AppContainer>
)

App.propTypes = {
  children: PropTypes.element.isRequired,
}

export default App
