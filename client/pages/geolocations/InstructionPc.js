import React from 'react';
import DropdownPanel from '../../components/commons/DropdownPanel'
import { CDN_URL } from '../../constants/Enviroment';
import * as UserDevice from '../../utils/UserDevice';

const InstructionPc = () => (
  <div>
    {UserDevice.getBrowserName() === 'chrom' && <DropdownPanel
        panelTitle="Sử dụng trình duyệt Google Chrome"
        isOpen
      >
      <div className="col-xs-12">
        <div className="mbs"></div>
        <div className="mbm">
          Bước 1: Click chọn biểu tượng hình khóa trên thanh công cụ: <br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_pc_chrom_step1.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 2: Bạn sẽ thấy trạng thái của Location/Xác định vị trí hiện tại là “Block/Bị chặn bởi bạn”. Click chọn biểu tượng tam giác xổ xuống, sau đó chọn “Use Global default (Ask)/Sử dụng cài đặt mặc định chung (Hỏi)” <br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_pc_chrom_step2.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 3: Khi vào xem mục “Gần tôi”, khi được hỏi, nhớ cho phép chúng tôi xác định vị trí để có thể khám phá những ai gần bạn. <br />
        </div>
      </div>
      </DropdownPanel>
    }

      {UserDevice.getBrowserName() === 'coc_coc' && <DropdownPanel
        panelTitle="Sử dụng trình duyệt Cốc Cốc"
        isOpen
      >
      <div className="col-xs-12">
        <div className="mbs"></div>
        <div className="mbm">
          Bước 1: Click vào biểu tượng hình khóa trên thanh công cụ: <br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_pc_coccoc_step1.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 2: Bạn sẽ thấy trạng thái xác định vị trí hiện tại là “Bị chặn bởi bạn”. Click chọn biểu tượng tam giác xổ xuống, sau đó chọn “Sử dụng cài đặt mặc định chung (Hỏi)<br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_pc_coccoc_step2.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 3: F5 để refresh lại trang Ymeet.me. Sau đó, vào mục “Gần tôi”. Khi thấy yêu cầu sử dụng thông tin vị trí thiết bị của bạn, hãy ấn “Cho phép” để tiếp tục khám phá những ai gần bạn. <br />
        </div>
      </div>
      </DropdownPanel>
    }

      {UserDevice.getBrowserName() === 'firefox' && <DropdownPanel
        panelTitle="Sử dụng trình duyệt Firefox"
        isOpen
      >
      <div className="col-xs-12">
        <div className="mbs"></div>
        <div className="mbm">
          Bước 1: Click chọn biểu tượng hình khóa trên thanh công cụ: <br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_pc_firefox_step1.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 2: Click chọn biểu tượng mũi tên<br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_pc_firefox_step2.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 3: Chọn “Thông tin thêm” và vào phần “Quyền hạn”. Với phần “Truy cập vị trí của bạn”, click chọn “Luôn hỏi”.<br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_pc_firefox_step3.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 4: F5 để refresh lại trang Ymeet.me. Sau đó, vào mục “Gần tôi”. Khi thấy yêu cầu sử dụng thông tin vị trí thiết bị của bạn, hãy ấn “Cho phép” để tiếp tục khám phá những ai gần bạn.<br />
        </div>
      </div>
      </DropdownPanel>
    }
  </div>
);

export default InstructionPc;