import React from 'react';
import DropdownPanel from '../../components/commons/DropdownPanel';
import { CDN_URL } from '../../constants/Enviroment';
import * as UserDevice from '../../utils/UserDevice';

const InstructionIos = () => (
  <div>
    {UserDevice.getBrowserName() === 'safari' && <DropdownPanel
        panelTitle="Khi sử dụng Iphone (áp dụng cho Safari và tất cả các trình duyệt khác)"
        isOpen
      >
      <div className="col-xs-12">
        <div className="mbs"></div>
        <div className="mbm">
          Bước 1:  Vào Cài đặt/ Settings => Cài đặt chung/ General => Cài đặt lại/ Reset => Cài đặt lại vị trí và quyền riêng tư/ Reset Location & Privacy => Nhập mật khẩu điện thoại của bạn/ Enter your passcode => Cài đặt lại/ Reset setting: <br />
          <div className="mbl"><img src={`${CDN_URL}/general/geo_instruction_ios.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 2: Vào Ymeet.me, refresh lại trang Ymeet.me. Khi thấy yêu cầu sử dụng thông tin vị trí thiết bị của bạn, hãy ấn “Cho phép” để tiếp tục khám phá những ai gần bạn.<br />
        </div>
      </div>
      </DropdownPanel>
    }
  </div>
);

export default InstructionIos;