import React from 'react';
import DropdownPanel from '../../components/commons/DropdownPanel';
import { CDN_URL } from '../../constants/Enviroment';
import * as UserDevice from '../../utils/UserDevice';

const InstructionAndroid = () => (
  <div>
    {UserDevice.getBrowserName() === 'chrom' && <DropdownPanel
        panelTitle="Sử dụng trình duyệt Google Chrome"
        isOpen
      >
      <div className="col-xs-12">
        <div className="mbs"></div>
        <div className="mbm">
          Bước 1: Click chọn biểu tượng hình khoá trên thanh công cụ:  <br />
          <div className="mbl"><img src={`${CDN_URL}/general/geo_instruction_android_chrom_step1.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 2: Chọn Site setting/ Cài đặt trang: <br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_android_chrom_step2.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 3: Chọn Clear & Reset/ Xóa và cài đặt lại => Clear & Reset/ Xóa và cài đặt lại <br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_android_chrom_step3.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 4: Refresh lại trang Ymeet.me. Khi thấy yêu cầu sử dụng thông tin vị trí thiết bị của bạn, hãy ấn “Cho phép” để tiếp tục khám phá những ai gần bạn.<br />
        </div>
      </div>
      </DropdownPanel>
    }

      {UserDevice.getBrowserName() === 'coc_coc' && <DropdownPanel
        panelTitle="Sử dụng trình duyệt Cốc Cốc"
        isOpen
      >
      <div className="col-xs-12">
        <div className="mbs"></div>
        <div className="mbm">
          Bước 1: Click chọn biểu tượng hình khoá trên thanh công cụ:  <br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_android_coccoc_step1.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 2:Bạn sẽ nhìn thấy trạng thái xác định Vị trí hiện tại là “Bị chặn”. Click chọn “Cài đặt trang web”<br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_android_coccoc_step2.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 3: Chọn “Xóa và đặt lại” => “Xóa và đặt lại”<br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_android_coccoc_step3.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 4: Refresh lại trang Ymeet.me. Khi thấy yêu cầu sử dụng thông tin vị trí thiết bị của bạn, hãy ấn “Cho phép” để tiếp tục khám phá những ai gần bạn.
        </div>
      </div>
      </DropdownPanel>
    }

    {UserDevice.getBrowserName() === 'firefox' && <DropdownPanel
        panelTitle="Sử dụng trình duyệt Firefox"
        isOpen
      >
      <div className="col-xs-12">
        <div className="mbs"></div>
        <div className="mbm">
          Bước 1: Click chọn biểu tượng hình khoá trên thanh công cụ:  <br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_android_firefox_step1.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 2: Chọn Edit Site setting/ Chỉnh sửa cài đặt trang:<br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_android_firefox_step2.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 3: Click chọn ở phần Location/ Xác định vị trí => Clear<br />
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_android_firefox_step3.jpg`} /></div>
        </div>
        <div className="mbm">
          Bước 4: Refresh lại trang Ymeet.me. Khi thấy yêu cầu sử dụng thông tin vị trí thiết bị của bạn, hãy ấn “Cho phép” để tiếp tục khám phá những ai gần bạn.
          <div className="mbl"><img src={`${CDN_URL}/general/go_instruction_android_firefox_step4.jpg`} /></div>
        </div>
      </div>
      </DropdownPanel>
    }

  </div>
);

export default InstructionAndroid;