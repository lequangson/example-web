import React from 'react';
import { connect } from 'react-redux'
import HeaderContainer from '../../containers/HeaderContainer'
import ContactContainer from '../../containers/ContactContainer'
import AnonymousHeader from '../../components/headers/AnonymousHeader'
import AnonymousFooter from '../../components/footers/AnonymousFooter'
import * as ymmStorage from '../../utils/ymmStorage';
import InstructionPc from './InstructionPc';
import InstructionAndroid from './InstructionAndroid';
import InstructionIos from './InstructionIos';
import * as UserDevice from '../../utils/UserDevice';

const Instruction = (props) => {
  window.scroll(0, 0)
  let isAnonymousUser = true
  try {
    if (props.user_profile && props.user_profile.current_user && props.user_profile.current_user.welcome_page_completion === 4) {
      isAnonymousUser = false
    }
  } catch (e) {}
  return (
    <div>
      {isAnonymousUser && <AnonymousHeader />}
      {!isAnonymousUser && <HeaderContainer {...props} /> }
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h2 className="txt-heading">Cách cho phép Ymeet.me xác định vị trí của bạn sau khi đã từ chối trên {`${UserDevice.getName().toUpperCase()}`}</h2>
            </div>
          </div>
        </div>
      {UserDevice.getName() === 'pc' && <InstructionPc />}
      {UserDevice.getName() === 'android' && <InstructionAndroid />}
      {UserDevice.getName() === 'ios' && <InstructionIos />}
      </div>
      {isAnonymousUser && <AnonymousFooter />}
    </div>
  )
}

function mapStateToProps(state) {
  return state
}

const InstructionPage = connect(mapStateToProps)(Instruction)
export default InstructionPage;