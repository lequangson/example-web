import React from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import FacebookSyncContainer from '../containers/FacebookSyncContainer'

const FacebookSync = props => (
  <div>
    <HeaderContainer {...props} />
    <FacebookSyncContainer {...props} />
  </div>
)

export default FacebookSync
