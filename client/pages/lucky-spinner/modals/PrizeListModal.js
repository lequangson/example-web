import React, { Component, PropTypes } from 'react';
import Modal from 'components/modals/Modal';
import { connect } from 'react-redux';
import { CDN_URL } from 'constants/Enviroment';
import moment from 'moment';
import _ from 'lodash';

class PrizeListModal extends Component {
  static propTypes = {
    isOpen: PropTypes.bool,
    closeModal: PropTypes.func,
  }

  render() {
    const { user_profile } = this.props;
    const { lucky_spinner_gifts, current_user } = user_profile;
    const {
      bonus_5_coins_gift,
      bonus_20_coins_gift,
      bonus_50_coins_gift,
      boost_in_rank_free_gift,
      super_like_free_gift,
      three_month_subcription_free_gift,
      random_match_free_gift,
      unlock_chat_free_gift
    } = lucky_spinner_gifts;
    let totalGift = 0;
    const endDate = (typeof current_user.licence !== 'undefined' && current_user.licence !== null) ? moment(current_user.licence.free_license_end_at).format('DD/MM/YYYY') : '';

    _.map(lucky_spinner_gifts, (item) => { totalGift += item } )
    const totalCoinRecive = bonus_5_coins_gift * 5 + bonus_20_coins_gift * 20 + bonus_50_coins_gift * 50;

    return (
      <Modal
        transitionName="modal"
        isOpen={this.props.isOpen}
        onRequestClose={this.props.closeModal}
        className="PrizeListModal modal__content modal__content--6"
        overlayClassName="modal__overlay prize-listoverlay"
        portalClassName="modal"
        contentLabel=""
      >
        <div className="modal__txt-content">
          <div className="txt-center txt-lg txt-white padding-t10 padding-b10">
            <div className="txt-xlg txt-medium txt-uppercase">Danh sách phần thưởng</div>
            {totalGift == 0 && <div className="txt-lg txt-medium">Bạn chưa có phần thưởng nào</div>}
          </div>
          <div className="row padding-b10">
            <div className="col-xs-4 txt-center txt-white">
              <div className={random_match_free_gift > 0 ? "img-prize" : "img-prize prize-disable"}>
                <img className="img-70" src={`${CDN_URL}/general/LuckySpinner/Random_match.png`} alt="random-match"/>
              </div>
              <div className="prize-content padding-t10">
                {random_match_free_gift} Ghép đôi ngẫu nhiên
              </div>
            </div>
            <div className="col-xs-4 txt-center txt-white">
              <div className={super_like_free_gift > 0 ? "img-prize" : "img-prize prize-disable"}>
                <img className="img-70" src={`${CDN_URL}/general/LuckySpinner/Superlike.png`} alt="supper-like"/>
              </div>
              <div className="prize-content padding-t10">
                {super_like_free_gift} Supper Like
              </div>
            </div>
            <div className="col-xs-4 txt-center txt-white">
              <div className={unlock_chat_free_gift > 0 ? "img-prize" : "img-prize prize-disable"}>
                <img className="img-70" src={`${CDN_URL}/general/LuckySpinner/Unlock_chat.png`} alt="unlock-chat"/>
              </div>
              <div className="prize-content padding-t10">
                {unlock_chat_free_gift} Mở khóa chat
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-4 txt-center txt-white">
              <div className={boost_in_rank_free_gift > 0 ? "img-prize" : "img-prize prize-disable"}>
                <img className="img-70" src={`${CDN_URL}/general/LuckySpinner/Boost_in_rank.png`} alt="boost-inrank"/>
              </div>
              <div className="prize-content padding-t10">
                {boost_in_rank_free_gift} Nổi bật
              </div>
            </div>
{/*            <div className="col-xs-4 txt-center txt-white">
              <div className={totalCoinRecive > 0 ? "img-prize" : "img-prize prize-disable"}>
                <img className="mbt" src={`${CDN_URL}/general/LuckySpinner/Coin.png`} alt="coin"/>
              </div>
              <div className="prize-content padding-t10">
                { totalCoinRecive } Xu
              </div>
            </div>*/}
            <div className="col-xs-4 txt-center txt-white">
              <div className={three_month_subcription_free_gift > 0 ? "img-prize" : "img-prize prize-disable"}>
                <img className="img-70" src={`${CDN_URL}/general/LuckySpinner/Lucky_upgrade_account.png`} alt="unlock-chat"/>
              </div>
              <div className="prize-content padding-t10">
                {three_month_subcription_free_gift} Nâng cấp tài khoản 3 tháng <br/>
                {
                  three_month_subcription_free_gift > 0 &&
                  `Ngày hết hạn: ${endDate}`
                }
              </div>
            </div>
          </div>
        </div>
        <button
          className="modal__btn-close"
          onClick={this.props.closeModal}
        >
          <i className="fa fa-times"></i>
        </button>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    user_profile: state.user_profile,
  };
}

function mapDispatchToProps(dispatch) {
  return {

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(PrizeListModal);