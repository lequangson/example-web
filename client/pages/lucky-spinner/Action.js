import {
  GET_LUCKY_SPINNER_RESULT_REQUEST, GET_LUCKY_SPINNER_RESULT_SUCCESS, GET_LUCKY_SPINNER_RESULT_FAILURE,
  SAVE_LUCKY_SPINNER_RESULT_REQUEST, SAVE_LUCKY_SPINNER_RESULT_SUCCESS, SAVE_LUCKY_SPINNER_RESULT_FAILURE,
} from './Constant';
import { CALL_API } from 'middleware/api';
import {
  API_GET,
  API_POST
} from 'constants/Enviroment';

export function getLuckySpinnerResult() {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'lucky_spinner_gift_code',
      params: {},
      authenticated: true,
      types: [
        GET_LUCKY_SPINNER_RESULT_REQUEST, GET_LUCKY_SPINNER_RESULT_SUCCESS, GET_LUCKY_SPINNER_RESULT_FAILURE
      ]
    },
  }
}

export function saveLuckySpinnerResult(gift_code) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'play_lucky_spinner',
      params: {},
      authenticated: true,
      types: [
        SAVE_LUCKY_SPINNER_RESULT_REQUEST, SAVE_LUCKY_SPINNER_RESULT_SUCCESS, SAVE_LUCKY_SPINNER_RESULT_FAILURE,
      ],
      extentions: {
        gift_code
      }
    }
  }
}
