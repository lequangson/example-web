import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import HeaderContainer from 'containers/HeaderContainer';
import {
  increaseCurrentCoin, getLuckySpinnerGift, getUserLicence, getCurrentUser
} from 'actions/userProfile';
import {
  getLuckySpinnerResult, saveLuckySpinnerResult
} from './Action';
import LuckySpinnerPage from './LuckySpinnerPage';

const LuckySpinner = props => (
  <div>
    <HeaderContainer {...props} />
    <LuckySpinnerPage {...props} />
  </div>
)

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    getLuckySpinnerResult: async () => {
      return await dispatch(getLuckySpinnerResult());
    },
    saveLuckySpinnerResult: async (gift_code) => {
      await dispatch(saveLuckySpinnerResult(gift_code));
    },
    increaseCurrentCoin: (coins) => {
      dispatch(increaseCurrentCoin(coins));
    },
    getMyPrizes: () => {
      dispatch(getLuckySpinnerGift());
    },
    getUserLicence: () => {
      dispatch(getUserLicence());
    },
    getCurrentUser: () => dispatch(getCurrentUser()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LuckySpinner);