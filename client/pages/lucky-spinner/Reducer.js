import {
  GET_LUCKY_SPINNER_RESULT_REQUEST,
  GET_LUCKY_SPINNER_RESULT_SUCCESS,
  GET_LUCKY_SPINNER_RESULT_FAILURE,
} from './Constant';

const defaultState = getDefaultState();

function getDefaultState() {
  return {
    isFetching: false,
    result_gift_code: '',
    gifts: [
      {
        gift_code: 5,
        bonus_coin: 50,
      },
      {
        gift_code: 6,
        bonus_coin: 20
      },
      {
        gift_code: 7,
        bonus_coin: 5
      }
    ],
    result_gift: {},
  };
}

export default function lucky_spinner(state = defaultState, action) {
  switch (action.type) {
    case GET_LUCKY_SPINNER_RESULT_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case GET_LUCKY_SPINNER_RESULT_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_LUCKY_SPINNER_RESULT_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        result_gift_code: action.response.data.gift_code,
        result_gift: state.gifts.find(gift => gift.gift_code === action.response.data.gift_code) || {},
      });
    default:
      return state;
  }
}
