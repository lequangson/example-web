import React, { Component, PropTypes } from 'react';
import Winwheel from 'winwheel';
import { CDN_URL } from 'constants/Enviroment';
import { Link, browserHistory } from 'react-router';
import { isEmpty } from 'lodash';
import * as UserDevice from 'utils/UserDevice';
import * as ymmStorage from 'utils/ymmStorage';
import Modal from 'components/modals/Modal';
import {
  THREE_MONTH_SUBCRIPTION_FREE_GIFT, RANDOM_MATCH_FREE_GIFT,
  SUPER_LIKE_FREE_GIFT, UNLOCK_CHAT_FREE_GIFT, BOOST_IN_RANK_FREE_GIFT
} from 'constants/userProfile';
import * as deductionNofi from '../../utils/notifications/campaign';
import { LUCKY_SPINNER_COST } from './Constant';
import PrizeListModal from './modals/PrizeListModal';

var theWheel = null

var wheelSpinning = false;

class LuckySpinnerPage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
      modalPrizeIsOpen: false,
    }
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
    this.openPrizeListModal = this.openPrizeListModal.bind(this);
    this.closePrizeListModal = this.closePrizeListModal.bind(this);
  }

  state = {
    isShowResult: false,
    isSpinning: false,
  }

  componentDidUpdate() {
    if (this.props.user_profile.current_user.gender === 'female') {
      browserHistory.push('/whathot');
    }
  }

  componentDidMount() {
    this.loadSpinnerScript();
  }

  loadSpinnerScript() {
    const widthScreen = UserDevice.getName() === 'pc' ? 434 : window.innerWidth - 30

    theWheel = new Winwheel({
      'numSegments'       : 10,
      'outerRadius'       : UserDevice.getName() !== 'pc' ? (widthScreen / 2 - 30) * 0.85 : (widthScreen / 2 - 13),
      'drawMode'          : 'image',
      'drawText'          : false,      // Need to set this true if want code-drawn text on image wheels.
      'textFontSize'      : 0,
      'textOrientation'   : 'curved',
      'textDirection'     : 'reversed',
      'textAlignment'     : 'outer',
      'textMargin'        : 5,
      'textFontFamily'    : 'monospace',
      'textStrokeStyle'   : 'black',
      'textLineWidth'     : 2,
      'textFillStyle'     : 'white',
      'segments'          :                    // Define segments including image and text.
      [
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
         {'text' : '1'},
      ],
      'animation' :
      {
        'type'     : 'spinToStop',
        'duration' : 5,
        'spins'    : 8,
        'callbackFinished' : this.callBack
      }
    });

    // Create new image object in memory.
    var loadedImg = new Image();

    // Create callback to execute once the image has finished loading.
    loadedImg.onload = function()
    {
        theWheel.wheelImage = loadedImg;    // Make wheelImage equal the loaded image object.
        theWheel.drawWheelImage();                    // Also call draw function to render the wheel.
    }

    // Set the image source, once complete this will trigger the onLoad callback (above).
    loadedImg.src = `${CDN_URL}/general/LuckySpinner/Wheel.png`;
  }

  callBack = async () => {
    const { result_gift_code, result_gift } = this.props.lucky_spinner;
    this.setState({ isShowResult: true, isSpinning: false });
    window.scroll(0, 0);
    await this.props.saveLuckySpinnerResult(result_gift_code);
    if (result_gift.bonus_coin) {
      this.props.increaseCurrentCoin(result_gift.bonus_coin);
    }
    switch (result_gift_code) {
      case THREE_MONTH_SUBCRIPTION_FREE_GIFT:
        this.props.getCurrentUser();
        this.props.getMyPrizes();
        break;
      case RANDOM_MATCH_FREE_GIFT:
      case SUPER_LIKE_FREE_GIFT:
      case UNLOCK_CHAT_FREE_GIFT:
      case BOOST_IN_RANK_FREE_GIFT:
        this.props.getMyPrizes();
        break;
      default:
        break;
    }
  }
  // -------------------------------------------------------
  // Click handler for spin button.
  // -------------------------------------------------------
  startSpin = async () => {
    // Ensure that spinning can't be clicked again while already running.
    this.setState({ isSpinning : true });
    const result_gift_code = await this.props.getLuckySpinnerResult();
    const gift = result_gift_code.response.data.gift_code;
    if (wheelSpinning == false)
    {
        // Based on the power level selected adjust the number of spins for the wheel, the more times is has
        // to rotate with the duration of the animation the quicker the wheel spins.
        theWheel.animation.spins = 3;

        // var segmentNumber = 9
        var stopAt = theWheel.getRandomForSegment(gift);

        theWheel.animation.stopAngle = stopAt;

        // Begin the spin animation by calling startAnimation on the wheel object.
        theWheel.startAnimation();
        // Set to true so that power can't be changed and spin button re-enabled during
        // the current animation. The user will have to reset before spinning again.
        wheelSpinning = true;
    }
  }

  openModal() {
    this.loadSpinnerScript();
    wheelSpinning = false;
    this.setState({ modalIsOpen: true });
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  openPrizeListModal() {
    // this.props.getMyPrizes();
    this.props.getUserLicence();
    this.setState({ modalPrizeIsOpen: true });
  }

  closePrizeListModal() {
    this.setState({ modalPrizeIsOpen: false });
  }

  luckySpinnerStyle() {
    let heightScreen = 93;
    if(window.outerHeight < 980 && window.outerHeight > 640) {
      heightScreen = 118;
    }
    return {
      minHeight: heightScreen + 'vh',
      marginTop: '-1rem',
      backgroundImage: `url(${CDN_URL}/general/LuckySpinner/Fortune_wheel_Background.png)`,
      backgroundSize: 'cover'
    }
  }

  luckySpinnerWrapStyle() {
    let heightScreen = 95;
    if(window.outerHeight < 980 && window.outerHeight > 640) {
      heightScreen = 138;
    }
    return {
      minHeight: heightScreen + 'vh'
    }
  }

  playSpinner = () => {
    if (this.state.isSpinning) { return; }
    const { number_lucky_spinner_free } = this.props.campaign.data;
    const { lucky_spinner_gifts } = this.props.user_profile;
    const number_spinner_free = number_lucky_spinner_free + lucky_spinner_gifts.more_spin_gift;
    this.setState({ isShowResult: false });
    if (number_spinner_free > 0) {
      this.loadSpinnerScript();
      wheelSpinning = false;
      this.startSpin();
    } else {
      this.openModal();
    }
  }

  playAgain = () => {
    this.props.increaseCurrentCoin(-30);
    deductionNofi.deductionNofi();
    this.closeModal();
    this.startSpin();
  }

  renderMessageGift(giftType) {
    switch (giftType)
    {
      case 1:
        return (
          <div className="txt-bold">
            <div className="txt-uppercase txt-xxlg">Bạn được <br/>
            1 ghép đôi ngẫu nhiên</div>
            <img className="img-100 padding-t10" src={`${CDN_URL}/general/LuckySpinner/Random_match.png`} alt="random-match"/>
          </div>
        )
        break;
      case 2:
        return (
          <div className="txt-bold">
            <div className="txt-uppercase txt-xxlg">Bạn được <br/>
            1 supper like</div>
            <img className="img-70 padding-t10" src={`${CDN_URL}/general/LuckySpinner/Superlike.png`} alt="supper-like"/>
          </div>
        )
        break;
      case 3:
        return (
          <div className="txt-bold">
            <div className="txt-uppercase txt-xxlg">Bạn được <br/>
            1 lần mở khóa chat</div>
            <img className="img-70 padding-t10" src={`${CDN_URL}/general/LuckySpinner/Unlock_chat.png`} alt="unlock-chat"/>
          </div>
        )
        break;
      case 4:
        return (
          <div className="txt-bold">
            <div className="txt-uppercase txt-xxlg">Bạn được <br/>
            1 lần nổi bật</div>
            <img className="img-70 padding-t10" src={`${CDN_URL}/general/LuckySpinner/Boost_in_rank.png`} alt="Boost_in_rank"/>
          </div>
        )
        break;
      case 5:
        return (
          <div className="txt-bold">
            <div className="txt-uppercase txt-xxlg">Bạn được <br/>
            + 50 xu</div>
            <img className="img-70 padding-t10" src={`${CDN_URL}/general/LuckySpinner/Coin.png`} alt="Coin"/>
          </div>
        )
        break;
      case 6:
        return (
          <div className="txt-bold">
            <div className="txt-uppercase txt-xxlg">Bạn được <br/>
            + 20 xu</div>
            <img className="img-70 padding-t10" src={`${CDN_URL}/general/LuckySpinner/Coin.png`} alt="Coin"/>
          </div>
        )
        break;
      case 7:
        return (
          <div className="txt-bold">
            <div className="txt-uppercase txt-xxlg">Bạn được <br/>
            + 5 xu</div>
            <img className="img-70 padding-t10" src={`${CDN_URL}/general/LuckySpinner/Coin.png`} alt="Coin"/>
          </div>
        )
        break;
      case 8:
        return (
          <div className="txt-bold">
            <div className="txt-uppercase txt-xxlg">Chúc bạn may mắn <br/>
            lần sau!</div>
            <img className="img-70 padding-t10" src={`${CDN_URL}/general/LuckySpinner/Good+luck.png`} alt="good-luck"/>
          </div>
        )
        break;
      case 9:
        return (
          <div className="txt-bold">
            <div className="txt-uppercase txt-xxlg">Bạn được<br />
            quay thêm 1 lần nữa
            </div>
            <div className="txt-xxxxlg">+ 1</div>
          </div>
        )
        break;
      case 10:
        return (
          <div className="txt-bold pos-relative">
            <div className="txt-uppercase txt-xxlg">Bạn đã giành giải<br /> đặc biệt!!!</div>
            <div className="txt-lg padding-t10 padding-b10">Tài khoản của bạn<br /> được nâng cấp trong 3 tháng</div>
            <img className="img-unlock-account" src={`${CDN_URL}/general/LuckySpinner/winner.png`} alt="3-month"/>
          </div>
        )
        break;
      default:
        return ;
    }
  }

  render() {

    const { user_profile, campaign } = this.props;
    if (isEmpty(campaign.data)) {
      return null;
    }
    const coins = user_profile.current_user ? user_profile.current_user.coins : 0;
    const { result_gift_code } = this.props.lucky_spinner;

    return (
      <div className="site-content lucky-spinner-content" style={this.luckySpinnerStyle()}>
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div className="spinner-wrap" style={this.luckySpinnerWrapStyle()}>
                <div className="spinner-result padding-t20 txt-center txt-white">
                  {!this.state.isShowResult &&
                    <div className="txt-bold">
                      <div className="txt-uppercase txt-xxlg padding-b10">Truy cập mỗi ngày<br />
                        Nhận ngay<br />
                        1 Lượt quay may mắn!
                      </div>
                      <div className="txt-lg">Giải đặc biệt:<br />
                      Gói nâng cấp tài khoản 3 tháng</div>
                    </div>
                  }
                  {this.state.isShowResult && this.renderMessageGift(result_gift_code)}
                </div>
                <canvas id="canvas" width='750' height='750'>
                  <p>Xin lỗi trình duyệt của bạn không hỗ trợ canvas, Vui lòng dùng trình duyệt khác.</p>
                </canvas>
                <img id="spin_button" className="img-50" src={`${CDN_URL}/general/LuckySpinner/Choi.png`} alt="Spin" onClick={this.playSpinner} />
              </div>
            </div>
          </div>
        </div>
        <a className="open-prize" onClick={() => {this.openPrizeListModal()}}>
          <img className="img-70" src={`${CDN_URL}/general/LuckySpinner/Button.png`} alt="Open Prize" />
        </a>
        <PrizeListModal
          isOpen={this.state.modalPrizeIsOpen}
          closeModal={this.closePrizeListModal}
        />
        <Modal
          transitionName="modal"
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className="modal__content modal__content--2"
          overlayClassName="modal__overlay"
          portalClassName="modal"
          contentLabel=""
        >
          <div className="txt-center txt-blue padding-t10 padding-b10 txt-bold">
            Bạn đã hết lượt chơi<br /> miễn phí trong ngày
          </div>
          <div className="modal__txt-content">
            <div className="txt-center padding-b10"> Chơi tiếp: <span className="txt-blue">{LUCKY_SPINNER_COST} xu</span></div>
            <div className="txt-center padding-b20"> Số xu hiện có: <span className="txt-blue">{coins} xu</span></div>
            {(coins >= LUCKY_SPINNER_COST) ?
              <button
                onClick ={this.playAgain}
                className='btn btn--p btn--b'
              >
                Chơi tiếp!
              </button>
              :
              <Link
                to="/payment/coin"
                className='btn btn--p btn--b'
                onClick={() => {
                  ymmStorage.setItem('Payment_Intention', 'LUCKY_SPINNER');
                }}
              >
                Nạp thêm xu
              </Link>
            }
          </div>
          <button className="modal__btn-close" onClick={this.closeModal}><i className="fa fa-times"></i></button>
        </Modal>
      </div>
    );
  }
}

export default LuckySpinnerPage;