import {
  GET_SIMILAR_TO_ME_REQUEST, GET_SIMILAR_TO_ME_SUCCESS, GET_SIMILAR_TO_ME_FAILURE
} from './Constant';
import { MODAL_SIMILAR_TO_ME } from 'pages/similar-face/Constant';

const defaultState = {
  isFetching: false,
  users: [],
  reachEnd: false,
  totalItems: 0,
  modalType: MODAL_SIMILAR_TO_ME,
  headerTitle: 'Giống tôi',
  shareTitle: 'Kết bạn trò chuyện với 0 người giống tôi trên Ymeet.me!',
  mode: 0,
  target_id: '',
  target_name: '',
  sharing_picture: '',
}

export default function similar_face_to_me(state = defaultState, action) {
  switch (action.type) {
    case GET_SIMILAR_TO_ME_REQUEST:
      return Object.assign({}, state, {
        users: [],
        isFetching: true
      });
    case GET_SIMILAR_TO_ME_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_SIMILAR_TO_ME_SUCCESS:
    {
      const totalItem = action.response.data.similar_pictures ?
        action.response.data.similar_pictures.length : state.totalItems;
      const shareTitle = `Kết bạn trò chuyện với ${action.response.data.similar_pictures.length} người giống tôi trên Ymeet.me!`;
      return Object.assign({}, state, {
        users: action.response.data.similar_pictures,
        reachEnd: true,
        isFetching: false,
        totalItems: totalItem,
        target_id: action.extentions.target_id,
        shareTitle: shareTitle,
        sharing_picture: action.response.data.sharing_picture.url,
      });
    }
    default:
      return state;
  }
}