import { connect } from 'react-redux'
import {
  getSimilarFaceToMeRequest, getSimilarFaceToMe,
} from './Action';
import { getFacePartnersRequest, getFacePartners, shuffleFacePartners } from 'pages/similar-face/Action';
import { updatePreviousPage } from 'actions/Enviroment';
import ListSimilarFaceToMe from './ListSimilarFaceToMe';

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    getSimilarFaceToMe: (current_user_id, user_id, filter_mode) => {
      dispatch(getSimilarFaceToMeRequest());
      dispatch(getSimilarFaceToMe(current_user_id, user_id, filter_mode));
    },
    getFacePartners: (props) => {
      dispatch(getFacePartnersRequest());
      dispatch(getFacePartners(props));
    },
    shuffleFacePartners: () => {
      dispatch(shuffleFacePartners())
    },
    updatePreviousPage: (page) => {
      dispatch(updatePreviousPage(page))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListSimilarFaceToMe)
