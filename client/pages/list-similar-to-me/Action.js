import {
  GET_SIMILAR_TO_ME_REQUEST, GET_SIMILAR_TO_ME_SUCCESS, GET_SIMILAR_TO_ME_FAILURE
} from './Constant';
import { CALL_API } from 'middleware/api';
import {
  API_GET,
} from 'constants/Enviroment';


export function getSimilarFaceToMeRequest() {
  return {
    type: GET_SIMILAR_TO_ME_REQUEST,
  }
}

export function getSimilarFaceToMe(current_user_id, user_id, filter_mode) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'similar_faces_by_user',
      params: {
        user_id,
        filter_mode, // 0 or 1
        current_user_id
      },
      authenticated: true,
      types: [GET_SIMILAR_TO_ME_REQUEST, GET_SIMILAR_TO_ME_SUCCESS, GET_SIMILAR_TO_ME_FAILURE],
      extentions: {
        target_id: user_id,
      }
    },
  }
}

export function getSimilarFaceToMeFailure() {
  return {
    type: GET_SIMILAR_TO_ME_FAILURE,
  }
}