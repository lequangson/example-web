import React, { Component, PropTypes } from 'react'
import { isEmpty } from 'lodash';
import InfiniteListItem from '../../components/infinite-list-items/InfiniteListItem';
import {
  GRID_VIEW_USER_CARD, DETAIL_VIEW_USER_CARD,
  NEW_PHOTO_CARD, SIMILAR_FACE_CARD,
} from 'components/infinite-list-items/Constant';
import addHeader from 'pages/similar-face/addHeader';
import SimilarToMeEmpty from 'pages/similar-face/empty-page/SimilarToMeEmpty';
import { getUserIdFromIdentifier } from 'utils/common';

class ListSimilarFaceToMe extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    if (this.props.similar_face_to_me.users.length === 0 &&
      !this.props.similar_face_to_me.isFetching) {
      const { current_user } = this.props.user_profile;
      if (!current_user.identifier) {
        return;
      }
      // filter by oposite gender
      const filter_mode = 0;
      this.props.getSimilarFaceToMe(current_user.identifier, current_user.identifier, filter_mode);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (isEmpty(this.props.user_profile.current_user) && !isEmpty(nextProps.user_profile.current_user)) {
      const filter_mode = 0;
      const { current_user } = nextProps.user_profile;
      this.props.getSimilarFaceToMe(current_user.identifier, current_user.identifier, filter_mode);
    }
  }

  render() {
    const { users } = this.props.similar_face_to_me;
    if (users.length === 0) {
      return <SimilarToMeEmpty {...this.props} />
    }
    return (
      <div className="sticky-footer">
        <InfiniteListItem
          listItems={this.props.similar_face_to_me.users}
          loadingDataStatus={this.props.similar_face_to_me.isFetching}
          reachEnd={this.props.similar_face_to_me.reachEnd}
          viewMode={SIMILAR_FACE_CARD}
          current_user={this.props.user_profile.current_user}
          pageSource='SIMILAR_FACE_TO_ME_PAGE'
          onClick={this.props.onClick}
        />
      </div>
    );
  }
}

export default addHeader(ListSimilarFaceToMe, 'similar_face_to_me');

