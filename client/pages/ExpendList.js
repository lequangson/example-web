import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import HeaderContainer from '../containers/HeaderContainer'

const ExpendList = props => (
  <div>
    <HeaderContainer {...props} />
    <div className="site-content">
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <h2 className="txt-heading">Danh sách mở rộng</h2>
          </div>
        </div>

        <div className="row">
          <ul className="list-border text-indent">
            <li className="list-border__item">
              <Link to="/people-i-liked" className="list-border__link" >
                <i className="fa fa-thumbs-o-up"></i>Danh sách đã gửi thích
              </Link>
            </li>
            <li className="list-border__item">
              <Link to="/favorite-lists" className="list-border__link" >
                <i className="fa fa-star"></i>Danh sách đã quan tâm
              </Link>
            </li>
            <li className="list-border__item">
              <Link
                to="/muted-users"
                className="list-border__link"
              >
                <i className="fa fa-bell-slash"></i>Danh sách chặn thông báo
              </Link>
            </li>
            <li className="list-border__item">
              <Link
                to="/blocked-users"
                className="list-border__link"
              >
                <i className="fa fa-ban"></i>Danh sách chặn hai chiều
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
)

export default ExpendList
