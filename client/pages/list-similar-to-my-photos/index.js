import { connect } from 'react-redux';
import { getFacePartnersRequest, getFacePartners, shuffleFacePartners } from 'pages/similar-face/Action';
import { updatePreviousPage } from 'actions/Enviroment';
import ListSimilarFaceToMyPhotos from './ListSimilarFaceToMyPhotos';
import { getSimilarFacesByPhoto, updateSelectedUser } from './Action';

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    getSimilarFacesByPhoto: async (current_user_id, photo_id) => await dispatch(getSimilarFacesByPhoto(current_user_id, photo_id)),
    updateSelectedUser: (data) => dispatch(updateSelectedUser(data)),
    getFacePartners: (props) => {
      dispatch(getFacePartnersRequest());
      dispatch(getFacePartners(props));
    },
    shuffleFacePartners: () => {
      dispatch(shuffleFacePartners())
    },
    updatePreviousPage: (page) => {
      dispatch(updatePreviousPage(page))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListSimilarFaceToMyPhotos)