import { MODAL_SIMILAR_TO_MY_PHOTOS } from 'pages/similar-face/Constant';
import {
  GET_SIMILAR_FACES_BY_PHOTO_REQUEST,
  GET_SIMILAR_FACES_BY_PHOTO_SUCCESS,
  GET_SIMILAR_FACES_BY_PHOTO_FAILURE,
  UPDATE_SELECTED_USER_FROM_DEVICE
} from './Constant';

const defaultState = {
  isFetching: false,
  users: [],
  reachEnd: false,
  totalItems: 0,
  modalType: MODAL_SIMILAR_TO_MY_PHOTOS,
  headerTitle: 'Tìm theo ảnh',
  shareTitle: 'Kết bạn trò chuyện với 0 người giống ảnh này trên Ymeet.me!',
  mode: 2,
  target_id: '',
  target_name: '',
  selected_user: {},
  sharing_picture: ''
}

export default function similar_face_to_my_photos(state = defaultState, action) {
  switch (action.type) {
    case GET_SIMILAR_FACES_BY_PHOTO_REQUEST:
      return Object.assign({}, state, {
        users: [],
        isFetching: true
      });
    case GET_SIMILAR_FACES_BY_PHOTO_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_SIMILAR_FACES_BY_PHOTO_SUCCESS:
    {
      const totalItem = action.response.data.similar_pictures ?
        action.response.data.similar_pictures.length : state.totalItems;
      return Object.assign({}, state, {
        users: action.response.data.similar_pictures,
        reachEnd: true,
        isFetching: false,
        totalItems: totalItem,
        target_id: action.extentions.target_id,
        shareTitle: `Kết bạn trò chuyện với ${totalItem} người giống ảnh này trên Ymeet.me!`,
        sharing_picture: action.response.data.sharing_picture.url,
        selected_user: {
          ...state.selected_user,
          extra_large_image_url: action.response.data.user_picture.url,
          id: action.extentions.target_id
        }
      });
    }
    case UPDATE_SELECTED_USER_FROM_DEVICE:
      return Object.assign({}, state, {
        selected_user: action.selected_user
      })
    default:
      return state
  }
}