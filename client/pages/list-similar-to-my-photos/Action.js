import { CALL_API } from 'middleware/api';
import {
  GET_SIMILAR_FACES_BY_PHOTO_REQUEST,
  GET_SIMILAR_FACES_BY_PHOTO_SUCCESS,
  GET_SIMILAR_FACES_BY_PHOTO_FAILURE,
  UPDATE_SELECTED_USER_FROM_DEVICE
} from './Constant';

export function getSimilarFacesByPhoto(current_user_id, photo_id) {
  return {
    [CALL_API]: {
      endpoint: 'similar_faces_by_photo',
      method: 'GET',
      authenticated: true,
      params: {
        photo_id,
        current_user_id
      },
      types: [
        GET_SIMILAR_FACES_BY_PHOTO_REQUEST,
        GET_SIMILAR_FACES_BY_PHOTO_SUCCESS,
        GET_SIMILAR_FACES_BY_PHOTO_FAILURE
      ],
      extentions: {
        target_id: photo_id,
      }
    },
  };
}

export function updateSelectedUser(selected_user) {
  return {
    type: UPDATE_SELECTED_USER_FROM_DEVICE,
    selected_user
  }
}