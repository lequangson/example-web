import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { isEmpty } from 'lodash';
import InfiniteListItem from 'components/infinite-list-items/InfiniteListItem';
import {
  GRID_VIEW_USER_CARD, DETAIL_VIEW_USER_CARD,
  NEW_PHOTO_CARD, SIMILAR_FACE_CARD,
} from 'components/infinite-list-items/Constant';
import { PUSH_TO_SOCKET_COMMAND } from './Constant';
import addHeader from '../similar-face/addHeader';
import SimilarToMyPhotosEmpty from '../similar-face/empty-page/SimilarToMyPhotosEmpty';
import {
  UploadImageFromDevice
} from 'components/buttons/upload-image';

class ListSimilarFaceToMyPhotos extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    const { current_user } = this.props.user_profile;
    const { picture_id  } = this.props.params;
    const { isUploadingImageFromDevice } = this.props.upload_image;
    if (!current_user.identifier || !picture_id || isUploadingImageFromDevice) {
      return;
    }
    this.props.getSimilarFacesByPhoto(current_user.identifier, picture_id);
  }

  componentWillReceiveProps(nextProps) {
    const { current_user } = this.props.user_profile;
    const { picture_id  } = this.props.params;
    if (!nextProps.params.picture_id) {
      return;
    }
    if ((isEmpty(current_user) && !isEmpty(nextProps.user_profile.current_user))
    ) {
      this.props.getSimilarFacesByPhoto(nextProps.user_profile.current_user.identifier, nextProps.params.picture_id);
    }
  }

  uploadFromDeviceSuccess = (data = null) => {
    if (data) {
      this.props.updateSelectedUser(data);
      browserHistory.push(`/similar-face-to-my-photos/${data.id}`);
    }
  }

  render() {
    const { users } = this.props.similar_face_to_my_photos;
    if (users.length === 0) {
      return <SimilarToMyPhotosEmpty {...this.props} />
    }
    return (
      <div className="sticky-footer">
        <InfiniteListItem
          listItems={this.props.similar_face_to_my_photos.users}
          loadingDataStatus={this.props.similar_face_to_my_photos.isFetching}
          reachEnd={this.props.similar_face_to_my_photos.reachEnd}
          viewMode={SIMILAR_FACE_CARD}
          current_user={this.props.user_profile.current_user}
          pageSource='SIMILAR_FACE_TO_MY_PHOTOS_PAGE'
          onClick={this.props.onClick}
        />
        <div className="fixed-bottom">
          <div className="container padding-t5">
            <UploadImageFromDevice
              picture_type={6}
              upload_success={this.uploadFromDeviceSuccess}
              upload_command={PUSH_TO_SOCKET_COMMAND}
            >
              <button className="btn txt-center btn--p btn--b">Tải ảnh khác lên</button>
            </UploadImageFromDevice>
          </div>
        </div>

        <div className="well"></div>
        <div className="well"></div>
      </div>
    );
  }
}

export default addHeader(ListSimilarFaceToMyPhotos, 'similar_face_to_my_photos');
