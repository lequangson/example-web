import React from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import PeopleILikedContainer from '../containers/PeopleILikedContainer'

const PeopleILiked = props => (
  <div>
    <HeaderContainer {...props} />
    <PeopleILikedContainer {...props} />
  </div>
)

export default PeopleILiked
