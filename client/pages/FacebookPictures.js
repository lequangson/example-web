import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import HeaderContainer from '../containers/HeaderContainer'
import FacebookPicturesContainer from '../containers/FacebookPicturesContainer'

const FacebookPictures = props => (
  <div>
    { props.user_profile.current_user.welcome_page_completion > 3
      && <HeaderContainer {...props} />
    }
    <FacebookPicturesContainer
      albumId={props.params.albumId}
      pictureType={props.params.pictureType}
      {...props}
    />
  </div>
)

FacebookPictures.propTypes = {
  params: PropTypes.object.isRequired,
}

function mapStateToProps(state) {
  return state
}

const FacebookPicturesPage = connect(mapStateToProps)(FacebookPictures)

export default FacebookPicturesPage