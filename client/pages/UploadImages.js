import React, { PropTypes } from 'react'
import UploadImagesContainer from '../containers/UploadImagesContainer'
import HeaderContainer from '../containers/HeaderContainer'

const UploadImagesPage = props => (
  <div>
    <HeaderContainer {...props} />
    <UploadImagesContainer
      pictureType={props.params.pictureType}
      uploadType={props.params.uploadType}
      {...props}
    />
  </div>
)

UploadImagesPage.propTypes = {
  params: PropTypes.object.isRequired,
}

export default UploadImagesPage
