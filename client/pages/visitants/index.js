import { connect } from 'react-redux'
import {
  getVisitant,
} from './Action';
import { 
  updatePreviousPage, updateEnvironmentState 
} from 'actions/Enviroment';
import ListVisitants from './ListVisitants';

function mapStateToProps(state) {
  return {
    visitants: state.visitants
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getVisitant: (pageIndex) => {
      dispatch(getVisitant(pageIndex));
    },
    updatePreviousPage: (page_source) => {
      dispatch(updatePreviousPage(page_source));
    },
    updateEnvironmentState: (key, value) => {
      dispatch(updateEnvironmentState(key, value))
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListVisitants)
