import { CALL_API } from 'middleware/api';
import { API_GET, API_POST, API_DELETE } from 'constants/Enviroment';
import {
  SEARCH_PAGE_SIZE,
} from 'constants/search';
import {
  GET_VISITANT_REQUEST, GET_VISITANT_SUCCESS, GET_VISITANT_FAILURE,
} from './Constant';

export function getVisitantRequest() {
  return {
    type: GET_VISITANT_REQUEST,
  }
}

export function getVisitant(pageIndex) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'visitants',
      params: {
        'paging_model[page_size]': SEARCH_PAGE_SIZE,
        'paging_model[page_index]': pageIndex,
      },
      authenticated: true,
      types: [
        GET_VISITANT_REQUEST,
        GET_VISITANT_SUCCESS,
        GET_VISITANT_FAILURE,
      ],
      extentions: {
        pageIndex: pageIndex,
      },
    },
  }
}

export function getVisitantFailure() {
  return {
    type: GET_VISITANT_FAILURE,
  }
}
