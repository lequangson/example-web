import React, { Component, PropTypes } from 'react';
import InfiniteListItem from 'components/infinite-list-items/InfiniteListItem';
import { FOOTPRINT_PAGE, DEFAULT_LOAD_MALE, DEFAULT_LOAD_FEMALE } from 'constants/Enviroment'
import { LOADING_DATA_TEXT, LOADING_DATA_TEXT_AFTER_4SECS } from 'constants/TextDefinition'
import GoogleOptimize from 'utils/google_analytic/google_optimize';
import {
  DETAIL_VIEW_USER_CARD
} from 'components/infinite-list-items/Constant';
import HeaderContainer from 'containers/HeaderContainer'

class ListVisitants extends Component {
  static propTypes = {
    getVisitant: PropTypes.func.isRequired,
    updatePreviousPage: PropTypes.func.isRequired,
    updateEnvironmentState: PropTypes.func.isRequired,
  }

  componentWillMount() {
    this.props.getVisitant(1);
    this.props.updatePreviousPage(FOOTPRINT_PAGE);
    setTimeout(() => {this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT_AFTER_4SECS)}, 4000);
  }

  @GoogleOptimize
  componentDidMount() {
  }

  loadMoreDataCallback = () => {
    this.props.getVisitant(this.props.visitants.pageIndex);
  }

  render() {
    const { 
      isFetching, reachEnd, users,
    } = this.props.visitants;

    return (
      <div className="sticky-footer">
        <HeaderContainer {...this.props}/>
        <div className="site-content">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <h2 className="txt-heading">Danh sách khách thăm</h2>
              </div>
            </div>
            <div className="row">
              <InfiniteListItem
                listItems={users}
                loadingDataStatus={isFetching}
                reachEnd={reachEnd}
                loadMoreDataCallback={this.loadMoreDataCallback}
                viewMode={DETAIL_VIEW_USER_CARD}
                pageSource={FOOTPRINT_PAGE}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ListVisitants;