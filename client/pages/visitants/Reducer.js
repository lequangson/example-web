import {
  filterUniqueUsers
} from 'utils/common';
import {
  DETAIL_VIEW_USER_CARD
} from 'components/infinite-list-items/Constant';
import {
  GET_VISITANT_REQUEST, GET_VISITANT_SUCCESS, GET_VISITANT_FAILURE,
} from './Constant';
import {
  SEARCH_PAGE_SIZE,
} from 'constants/search';

const defaultState = {
  isFetching: false,
  pageIndex: 1,
  viewMode: DETAIL_VIEW_USER_CARD,
  users: [],
  reachEnd: false,
}

export default function visitants(state = defaultState, action) {
  switch (action.type) {
    case GET_VISITANT_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case GET_VISITANT_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });
    case GET_VISITANT_SUCCESS:
    {
      return Object.assign({}, state, {
        users: action.extentions.pageIndex <= 1 ? 
          action.response.data : filterUniqueUsers(state.users, action.response.data),
        reachEnd: action.response.data.length < SEARCH_PAGE_SIZE,
        pageIndex: state.pageIndex + 1,
        isFetching: false,
      });
    }
    default:
      return state;
  }
}
