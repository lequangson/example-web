import React, { PropTypes } from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import BlockUsersContainer from '../containers/BlockUsersContainer'

const BlockUsers = props => (
  <div>
    <HeaderContainer {...props} />
    <BlockUsersContainer page={props.params.page} {...props} />
  </div>
)

BlockUsers.propTypes = {
  params: PropTypes.object.isRequired,
}

export default BlockUsers
