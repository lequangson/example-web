import React from 'react'
import PaymentContainer from '../containers/PaymentContainer'
import HeaderContainer from '../containers/HeaderContainer'

const Upgrade = props => (
  <div>
    <HeaderContainer {...props} />
    <PaymentContainer {...props} />
  </div>
)

export default Upgrade
