import React, { PropTypes } from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import VerifyContainer from '../containers/VerifyContainer'

const Verify = props => (
  <div>
    <HeaderContainer {...props} />
    <VerifyContainer {...props} />
  </div>
)

export default Verify
