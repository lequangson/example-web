import React from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import WhoAddMeToFavouriteContainer from '../containers/WhoAddMeToFavouriteContainer'

const WhoAddMeToFavourite = props => (
  <div>
    <HeaderContainer {...props} />
    <WhoAddMeToFavouriteContainer {...props} />
  </div>
)

export default WhoAddMeToFavourite
