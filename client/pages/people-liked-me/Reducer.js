import notification from 'utils/errors/notification';
import { NOTIFICATION_ERROR } from 'constants/Enviroment';
import { DEFAULT_ERROR_LOAD_PAGE } from 'constants/TextDefinition';
import {
  GET_PEOPLE_LIKED_ME_REQUEST, GET_PEOPLE_LIKED_ME_SUCCESS,
  GET_PEOPLE_LIKED_ME_FAILURE, NEXT_PEOPLE_LIKED_ME_USER,
  HIDDEN_LIKE_SUCCESS, HIDDEN_LIKE_FAILURE
} from './Constants';

const defaultState = {
  isFetching: false,
  is_page_loading: false,
  users: [],
  currentPageIndex: 0,
  peopleLikedMeSelectedUserIndex: 0,
  errorMessage: ''
}

export default function people_liked_me(state = defaultState, action) {
  switch (action.type) {
    case GET_PEOPLE_LIKED_ME_REQUEST: // get people liked me request
      return Object.assign({}, state, {
        isFetching: true,
        is_page_loading: false,
        errorMessage: null
      })

    case GET_PEOPLE_LIKED_ME_SUCCESS: // get people liked me success
      return Object.assign({}, state, {
        users: [...state.users, ...action.response.data],
        is_page_loading: true,
        isFetching: false,
        peopleLikedMeSelectedUserIndex: 0,
        currentPageIndex: action.extentions.pageIndex,
    })

    case GET_PEOPLE_LIKED_ME_FAILURE: // get people liked me failure
      notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
        return Object.assign({}, state, {
          isFetching: false,
          errorMessage: action.error_message
        })

    case HIDDEN_LIKE_SUCCESS:
      const nextUserIndex = state.peopleLikedMeSelectedUserIndex + 1
        return Object.assign({}, state, {
          peopleLikedMeSelectedUserIndex: nextUserIndex
        })

    case HIDDEN_LIKE_FAILURE:
      notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
          errorMessage: action.error_message
        })

    case NEXT_PEOPLE_LIKED_ME_USER:
      const selectedUserIndex = state.peopleLikedMeSelectedUserIndex + 1
        return Object.assign({}, state, {
          peopleLikedMeSelectedUserIndex: selectedUserIndex
        })

    default:
      return state;
  }
}