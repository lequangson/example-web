import React, { Component, PropTypes } from 'react';
import {
  PEOPLE_LIKED_ME_PAGE, CDN_URL, DEFAULT_LOAD_MALE, DEFAULT_LOAD_FEMALE
} from 'constants/Enviroment';
import {
  THERE_IS_NO_ONE_LIKED_ME_MESSAGE, LOADING_DATA_TEXT, LOADING_DATA_TEXT_AFTER_4SECS
} from 'constants/TextDefinition';
import { OTHER_PICTURE } from 'constants/userProfile';
import IconLinkSuperLikeSpinner from 'pages/super-like-spinner/card-item/IconLinkSuperLikeSpinner';
import DetailItem from 'components/searches/DetailItem';
import NotPaymentList from 'components/searches/NotPaymentList';
import * as ymmStorage from 'utils/ymmStorage';
import UploadImageButton from 'components/commons/Buttons/UploadImageButton';
import TinderListUsers from 'components/commons/TinderListUsers';
import FacebookLikeCampaign from './FacebookLikeCampaign';

class PeopleLikedMe extends Component {

  componentWillMount() {
    const { notification_number_likes } = this.props.user_profile.current_user;
    ymmStorage.removeItem('keep');
    if (notification_number_likes > 0 ||
        (this.props.people_liked_me.currentPageIndex <= 0 && !this.props.people_liked_me.isFetching)) {
      this.props.getPeopleLikedMe(1);
    }
    this.props.updatePreviousPage(PEOPLE_LIKED_ME_PAGE);
    setTimeout(() => {this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT_AFTER_4SECS)}, 4000);
  }

  componentWillUnmount() {
    this.props.updateEnvironmentState('isLoadingText', LOADING_DATA_TEXT)
  }

  showNextUser = () => {

  }

  loadMoreData = () => {
    this.props.getPeopleLikedMe(this.props.people_liked_me.currentPageIndex + 1)
  }

  skipShowLikeFanpage = () => {
    ymmStorage.setItem('skipShowLikeFanpage', true);
    this.forceUpdate();
  }

  renderPeopleLikedMeList() {
    if (this.props.people_liked_me.is_page_loading && this.props.people_liked_me.users.length > 0) {
      return (
        <TinderListUsers
          listUsers = {this.props.people_liked_me.users}
          selectedUserIndex = {this.props.people_liked_me.peopleLikedMeSelectedUserIndex}
          showNextUser = {this.showNextUser}
          loadMoreData = {this.loadMoreData}
          reachEnd = {true}
          pageSource = {PEOPLE_LIKED_ME_PAGE}
          forceLike={false}
        />
      )
    }
  }

  renderLoadingStatus() {
    const loadingText = this.props.Enviroment.isLoadingText
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE

    if (!this.props.people_liked_me.is_page_loading) {
      return (
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
            <article className="mbl mc">
              <div className="card__upper">
                <div href="#" className="card__link">
                  <img src={default_image} alt="" className="card__img"/>
                </div>
                <div className="card__center loader is-load">
                  <div className="mbs">{loadingText}</div>
                  <i className="fa fa-spinner fa-3x"></i>
                </div>
              </div>
            </article>
          </div>
        </div>
      )
    }
  }

  renderDescription() {
    const default_image = this.props.user_profile.current_user.gender == 'male' ? DEFAULT_LOAD_FEMALE : DEFAULT_LOAD_MALE

    if (
      this.props.people_liked_me.is_page_loading && (this.props.people_liked_me.users.length === 0 ||
      this.props.people_liked_me.peopleLikedMeSelectedUserIndex > (this.props.people_liked_me.users.length - 1))
    ) {
      return (
        <div className="row">
           <div className="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
             <article className="mbl mc">
               <div className="card__upper">
                 <div href="#" className="card__link mbm">
                   <img src={default_image} alt="" className="card__img"/>
                 </div>
                 <div className="txt-center txt-light txt-lg">
                   <p className="txt-bold">Những người đã thích bạn sẽ được hiển thị ở đây. Hãy thích lại họ và 2 bạn có thể trò chuyện!</p>
                   <br />Hãy tải nhiều ảnh để nhận được nhiều thích nhé!
                 </div>
                 <UploadImageButton className='btn btn--b btn--p' buttonName='Tải ảnh ngay' page_source={PEOPLE_LIKED_ME_PAGE} pictureType={OTHER_PICTURE} {...this.props} />
               </div>
             </article>
           </div>
         </div>
      )
    }
  }

  render() {
    const { has_like_fb_fanpage } = this.props.user_profile.current_user;
    const showLikeFanpageCard = !has_like_fb_fanpage && !ymmStorage.getItem('skipShowLikeFanpage') && this.props.people_liked_me.peopleLikedMeSelectedUserIndex > 0;

    return (
      <div className="site-content">
        <div className="container">
          <div className="row mbm">
            <div className="col-xs-12">
              <h3 className="txt-heading">Những người đã thích bạn</h3>
            </div>
          </div>
            {
              showLikeFanpageCard
              ?
              <div className="row">
              <FacebookLikeCampaign
                skipShowLikeFanpage={this.skipShowLikeFanpage}
              />
            </div>
              :
              <div className="row">
                {this.renderPeopleLikedMeList()}
                {this.renderLoadingStatus()}
                {this.renderDescription()}
              </div>
            }
            {
              this.props.user_profile.current_user.gender === 'male' &&
              <IconLinkSuperLikeSpinner />
            }
        </div>
      </div>
    )
  }
}

PeopleLikedMe.propTypes = {
  getPeopleLikedMe: PropTypes.func.isRequired,
  updatePreviousPage: PropTypes.func.isRequired,
  Enviroment: PropTypes.object.isRequired,
  updateScrollPossition: PropTypes.func.isRequired,
  user_profile: PropTypes.object.isRequired,
  setProfileUserNextIndex: PropTypes.func,
}

export default PeopleLikedMe;
