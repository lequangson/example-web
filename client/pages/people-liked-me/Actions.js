import { CALL_API } from 'middleware/api';
import { API_GET, API_POST } from 'constants/Enviroment';
import {
  GET_PEOPLE_LIKED_ME_REQUEST, GET_PEOPLE_LIKED_ME_SUCCESS,
  GET_PEOPLE_LIKED_ME_FAILURE, PEOPLE_LIKED_ME_PAGE_SIZE,
  HIDDEN_LIKE_REQUEST, HIDDEN_LIKE_SUCCESS, HIDDEN_LIKE_FAILURE,
  NEXT_PEOPLE_LIKED_ME_USER
} from './Constants';

export function getPeopleLikedMeRequest() {
  return {
    type: GET_PEOPLE_LIKED_ME_REQUEST,
  }
}

export function getPeopleLikedMe(pageIndex) {
  return {
    [CALL_API]: {
      method: API_GET,
      endpoint: 'likemes',
      params: {
        'paging_model[page_index]': pageIndex,
        'paging_model[page_size]': PEOPLE_LIKED_ME_PAGE_SIZE,
      },
      authenticated: true,
      types: [
        GET_PEOPLE_LIKED_ME_REQUEST,
        GET_PEOPLE_LIKED_ME_SUCCESS,
        GET_PEOPLE_LIKED_ME_FAILURE,
      ],
      extentions: {
        pageIndex: pageIndex,
      }
    },
  }
}

export function getPeopleLikedMeFailure() {
  return {
    type: GET_PEOPLE_LIKED_ME_FAILURE,
  }
}

export function hiddenLike(identifier) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'like_hidden',
      params: {
        user_identifier: identifier,
      },
      authenticated: true,
      types: [
        HIDDEN_LIKE_REQUEST,
        HIDDEN_LIKE_SUCCESS,
        HIDDEN_LIKE_FAILURE
      ],
      extentions: { identifier },
    },
  }
}

export function hiddenLikeRequest() {
  return {
    type: HIDDEN_LIKE_REQUEST,
  }
}

export function hiddenLikeFailure(msg = null) {
  return {
    type: HIDDEN_LIKE_FAILURE,
    msg,
  }
}

export function showNextPeopleLikedMe() {
  return {
    type: NEXT_PEOPLE_LIKED_ME_USER,
  }
}