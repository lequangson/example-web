import React, { Component,PropTypes } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router'
import FbLikeButton from 'components/commons/Buttons/FbLikeButton';
import { browserHistory } from 'react-router';
import { CDN_URL } from 'constants/Enviroment';
import * as ymmStorage from 'utils/ymmStorage';

class FacebookLikeCampaign extends Component {
	render(){
    const { onSkip } = this.props;
    return(
			<div className="col-sm-12 col-md-8 col-md-offset-2 padding-t30">
        <article className="card mbm">
          <div className="card__upper card__upper--bg-white">
            <div className="padding-b20">
              <div className="card__link" style={{paddingBottom: '50%'}} >
                <img
                  src={`${CDN_URL}/general/Campaign/Facebook-like-campaign.png`}
                  alt="Facebook-like-campaign"
                  className="img-full"
                />
              </div>
            </div>
            <div className="txt-center">
              <div className="txt-bold txt-xlg txt-blue" >
                Like Fanpage để được <br />
                <img src={`${CDN_URL}/general/Icon/coin.png`} alt="coin" className="img-small img-baseline mrs"/>
                10 xu miễn phí
              </div>
            </div>
          </div>
          <div className="card__under card__under--bg-white">
            <div className="txt-center padding-b10">
              Và cập nhật những bí kíp hẹn hò <br /> hiệu quả nhất!
            </div>
            <div className="txt-center padding-b10">
              <FbLikeButton
                appId={process.env.FACEBOOK_APP_ID}
                version="v2.10"
                href="https://www.facebook.com/ymeet.me/"
                showFaces={false}
                layout="button_count"
                share={false}
              />
            </div>
            <div>
              <div className="row padding-b10">
                <div className="col-xs-6 col-xs-offset-3">
                  <button className="btn btn--b txt-dark" onClick={this.props.skipShowLikeFanpage}>
                    Bỏ qua
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="card__top"></div>
        </article>
      </div>
		);
	}
}

FacebookLikeCampaign.propsType = {
  skipCallBack: PropTypes.func,
};

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(FacebookLikeCampaign);
