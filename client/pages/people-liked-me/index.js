import { connect } from 'react-redux';
import { getPeopleLikedMe } from './Actions';
import PeopleLikedMe from './PeopleLikedMe';

function mapStateToProps(state) {
  return state;
}

function mapDispachToProps(dispatch) {
  return {
    getPeopleLikedMe: (pageIndex) => {
      dispatch(getPeopleLikedMe(pageIndex));
    },
  }
}

export default connect(mapStateToProps, mapDispachToProps)(PeopleLikedMe);
