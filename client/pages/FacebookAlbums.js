import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import HeaderContainer from '../containers/HeaderContainer'
import FacebookAlbumsContainer from '../containers/FacebookAlbumsContainer'

const FacebookAlbums = props => (
  <div>
    { props.user_profile.current_user.welcome_page_completion > 3
      && <HeaderContainer {...props} />
    }
    <FacebookAlbumsContainer pictureType={props.params.pictureType} {...props} />
  </div>
)

FacebookAlbums.propTypes = {
  params: PropTypes.object.isRequired,
}

function mapStateToProps(state) {
  return state
}

const FacebookAlbumsPage = connect(mapStateToProps)(FacebookAlbums)

export default FacebookAlbumsPage