import React from 'react'
import UpdateSelfIntroContainer from '../containers/UpdateSelfIntroContainer'
import HeaderContainer from '../containers/HeaderContainer'

const UpdateSelfIntro = props => (
  <div>
    <HeaderContainer {...props} />
    <UpdateSelfIntroContainer {...props} />
  </div>
)

export default UpdateSelfIntro
