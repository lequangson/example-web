import React from 'react'
import HeaderContainer from '../containers/HeaderContainer'
import ChatListContainer from '../containers/ChatListContainer'
import RemindRandomMatchModal from '../components/modals/RemindRandomMatchModal'

const ChatList = props => (
  <div>
    <HeaderContainer {...props} />
    <ChatListContainer {...props} />
    <RemindRandomMatchModal {...props} />
  </div>
)

export default ChatList
