import React from 'react'
import DropdownPanel from '../components/commons/DropdownPanel';
import { Link } from 'react-router'
import HeaderContainer from '../containers/HeaderContainer'
import { CDN_URL } from 'constants/Enviroment';

const PayCoinInstruction = props => {
  const hashParts = window.location.hash.split('#')
  if (hashParts.length > 1) {
     setTimeout(() => {
      const hash = hashParts.slice(-1)[0];
      const element = document.querySelector(`#${hash}`);
      if (element) {
          element.scrollIntoView();
      }
    }, 1)
  }
  return (
    <div className="container">
      <HeaderContainer {...props} />
      <div className="row">
        <div className="col-xs-12">
          <h3 className="txt-heading">Xu</h3>
          <p>- Bạn muốn trò chuyện với người mình thích?</p>
          <p>- Bạn muốn làm nổi bật bản thân để gây ấn tượng?</p>
          <p>- Bạn gặp khó khăn trong việc kết đôi?</p>
          <p>- Bạn muốn việc hẹn hò thú vị và bất ngờ hơn?</p>

          <p className="txt-blue padding-l5">Đừng lo! Xu sẽ giúp bạn.</p>

          <div>
            <span className="txt-medium">Với Xu, bạn hoàn toàn có thể:</span>
            <ol className="padding-l30">
              <li> Mở chat không giới hạn với người mình đã ghép đôi</li>
              <li> Trò chuyện trực tiếp mà KHÔNG CẦN ghép đôi</li>
              <li> Đứng đầu trên mọi danh sách tìm kiếm</li>
              <li>Ghép đôi ngẫu nhiên</li>
            </ol>
          </div>

          <div>
            <span className="txt-medium"> Vậy làm thế nào để có được Xu?</span>
            <ol className="padding-l30">
              <li> Để bắt đầu, Ymeet.me tặng bạn <span className="txt-blue padding-l5">40 xu</span></li>
              <li> Gửi trên 20 lượt thích. Bạn có ngay <span className="txt-blue padding-l5">20 xu</span></li>
              <li>Nhận tối đa <span className="txt-blue padding-l5">130 xu</span> nếu đăng nhập liên tiếp 31 ngày</li>
              <li> Ảnh được vote nhiều nhất tuần. Có ngay <span className="txt-blue padding-l5">20 xu</span></li>
              <li> Có tổng số vote nhiều nhất tuần. Có ngay <span className="txt-blue padding-l5">20 xu</span></li>
            </ol>
          </div>

          <div className="mbm">
            <p className="txt-medium">Bạn cũng có thể nạp thêm Xu rất dễ dàng đấy nhé!</p>
            <ul className="text-indent-3 list-unstyled">
              <li> Có ngay <span className="txt-blue padding-l5">65 xu</span> chỉ với 20.000 VND</li>
              <li> Có ngay <span className="txt-blue padding-l5">180 xu</span> chỉ với 50.000 VND</li>
              <li> Có ngay <span className="txt-blue padding-l5">450 xu</span> chỉ với 100.000 VND</li>
            </ul>
          </div>

          <div className="mbm">
            <div>Còn nữa, nhận hoàn trả Xu ngay nếu bạn không hài lòng!</div>
            <div>Bạn đã gửi đi thật nhiều tin nhắn cho người mình thích, nhưng chờ mãi chẳng có hồi âm? </div>
            <p className="txt-medium" id="refund">Đừng buồn, bạn có thể nhận Hoàn trả 100 Xu ngay lập tức với những điều kiện sau:</p>
            <ul className="text-indent-3 list-unstyled">
              <li>- Bạn đã Mở chat với người trong danh sách ghép đôi</li>
              <li>- Bạn nhắn tin cho người ấy nhưng họ không trả lời lại</li>
              <li>- Không áp dụng cho các gói Nâng cấp tài khoản</li>
            </ul>
            <div>Hãy chọn nút “Hoàn trả xu” ở phòng chat để nhận lại xu ngay nhé! </div>
            <div className="txt-red">* Lưu ý, phòng chat sẽ bị khoá sau khi bạn nhận hoàn trả xu.</div>
          </div>

          <Link to="/payment/coin" className="btn btn--b btn--p mbm">Nạp thêm xu</Link>
        </div>
      </div>

      <DropdownPanel
      panelTitle="Hướng dẫn nạp thêm xu"
      >
        <div className="mbs"></div>
        <div className="txt-blue txt-bold">Cách 1: Sử dụng thẻ cào điện thoại</div>
        <ol className="padding-l30">
          <li>  Click vào nút “Nạp thêm xu”.</li>
          <li> Chọn phương thức thanh toán “Thẻ cào”</li>
          <li>Chọn gói nạp xu</li>
          <li> Chọn nhà mạng tương ứng với thẻ nạp của bạn. (Viettel, Mobifone hoặc Vinaphone)</li>
          <li> Nhập mã số thẻ và số seri
            <div className="padding-t10"><img src={`${CDN_URL}/general/Payment/Guide/mobi_guide.png`} alt="mobi_guide.png" /> </div>
          </li>
          <li> Click chọn “Thanh toán” để hoàn tất.</li>
        </ol>
        <div className="txt-underline">Chú ý: </div>
        <p>Hệ thống sẽ thông báo nạp xu thành công nếu bạn nhập đúng thông tin trên thẻ cào, báo lỗi nếu bạn nhập sai bất kỳ 1 ô nào hoặc thẻ không hợp lệ.</p>

        <div className="txt-blue txt-bold">Cách 2: Sử dụng thẻ ngân hàng</div>
        <ol className="padding-l30">
          <li>  Click vào nút “Nạp thêm xu”.</li>
          <li> Chọn phương thức thanh toán “Thẻ ngân hàng”</li>
          <li>Chọn gói nạp xu</li>
          <li>  Chọn ngân hàng bạn muốn sử dụng dịch vụ</li>
          <li> Nhập tên và số điện thoại của bạn</li>
          <li> Click chọn “Thanh toán” để để chuyển sang hệ thống thanh toán VTC Pay</li>
          <li> Nhập đầy đủ thông tin thẻ
            <div className="text-indent">
              <p>- Tên chủ thẻ</p>
              <p>- Số thẻ</p>
              <p>- Ngày phát hành</p>
            </div>
          </li>
          <li> Chờ VTC Pay thông báo giao dịch thành công. Click chọn [Đồng ý] để hoàn tất giao dịch</li>
        </ol>
        <div className="txt-underline">Chú ý: </div>
        <p>- Tùy ngân hàng sẽ có giao diện giao dịch khác nhau.</p>
        <p>- Sau khi xác thực giao dịch trên website riêng của ngân hàng đã thành công, vui lòng không đóng trình duyệt mà để trình duyệt tự động chuyển về Ymeet.me</p>
        <p>- Để sử dụng cách này, thẻ ngân hàng của bạn cần đăng ký Internet-Banking hoặc dịch vụ thanh toán trực tuyến tại ngân hàng.
        </p>

        <div className="txt-blue txt-bold">Cách 3: Sử dụng thẻ Credit Card</div>
        <ol className="padding-l30">
          <li>  Click vào nút “Nạp thêm xu”.</li>
          <li> Chọn phương thức thanh toán “Credit Card”</li>
          <li>Chọn gói nạp xu</li>
          <li>  Chọn loại thẻ (Visa hoặc MasterCard)</li>
          <li> Nhập tên và số điện thoại của bạn</li>
          <li> Click chọn “Thanh toán” để để chuyển sang hệ thống thanh toán VTC Pay</li>
          <li> Nhập đầy đủ thông tin thẻ
            <div className="padding-t10"><img src={`${CDN_URL}/general/Payment/Guide/credit_guide.png`} alt="credit_guide.png" /> </div>
          </li>
          <li> Chờ VTC Pay thông báo giao dịch thành công. Lưu ý không được đóng trình duyệt mà chọn [Đồng ý] để hoàn tất giao dịch.</li>
        </ol>
        <div className="txt-underline">Chú ý: </div>
        <p>Khi nạp xu theo hình thức này, số xu của bạn thông thường sẽ được cập nhật sau khoảng 20-30 phút.</p>
        <Link to="/payment/coin" className="btn btn--b btn--p mbm">Nạp thêm xu</Link>
        <p className="txt-red txt-sm txt-center">Hotline(7h-22h): 0936 332 079</p>
      </DropdownPanel>
    </div>
  )
}

export default PayCoinInstruction
