import React, { PropTypes }from 'react'
import HomeContainer from '../containers/HomeContainer'

const HomeIndex = props => (
  <div className="sticky-footer">
    <HomeContainer {...props} />
  </div>
)

HomeIndex.propTypes = {
}

export default HomeIndex