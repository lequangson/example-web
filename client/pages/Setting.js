import React from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import HeaderContainer from '../containers/HeaderContainer'

const Setting = props => (
  <div>
    <HeaderContainer {...props} />
    <div className="container">
      <div className="row">
        <div className="col-xs-12">
          <h3 className="txt-heading">Cài đặt hệ thống</h3>
        </div>
      </div>
    </div>

    <ul className="list-border">
      {props.user_profile.current_user.provider === 'facebook' &&
        <li className="list-border__item">
          <Link to="/facebook-sync" className="list-border__link">
            Đồng bộ hóa dữ liệu từ Facebook <i className="fa fa-chevron-right pull-right"></i>
          </Link>
        </li>
      }

{/*      <li className="list-border__item">
        <a href="/" className="list-border__link">
          Tạm ngưng tài khoản <i className="fa fa-chevron-right pull-right"></i>
        </a>
      </li>*/}
      <li className="list-border__item">
        <Link to="/delete-account" className="list-border__link">
          Xóa tài khoản <i className="fa fa-chevron-right pull-right"></i>
        </Link>
      </li>
    </ul>
  </div>
)

function mapStateToProps(state) {
  return state
}

const SettingPage = connect(mapStateToProps)(Setting)

export default SettingPage
