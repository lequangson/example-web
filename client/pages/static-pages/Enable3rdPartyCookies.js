import React, { PropTypes } from 'react'
import { CDN_URL } from 'constants/Enviroment'
import * as UserDevice from 'utils/UserDevice';

class Enable3rdPartyCookies extends React.Component {

  renderCoccocInstruction() {
    if (UserDevice.getName() === 'pc' && UserDevice.getBrowserName() === 'coc_coc') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Vào Cài đặt/ Setting của Cốc cốc:  <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_CocCoc1.png`} /></div>
          </div>
          <div className="mbm">
            <div className="txt-bold">
              Bước 2: Chọn Riêng tư/ Privacy và chọn Cài đặt nội dung/ Content: <br /><br />
            </div>
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_CocCoc2.png`} /></div>
            Nếu Cốc cốc của bạn không hiển thị như hình, bạn có thể vào phần Cài đặt tìm kiếm/ Search ở góc trên cùng bên phải, gõ “nội dung/ content”.<br /><br />
          </div>
          <div className="mbm txt-bold">
            Bước 3: Bỏ chọn ở ô “Chặn dữ liệu trang web và cookie của bên thứ ba/ Block web data and cookie of third party”. Và chọn Hoàn tất. <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_CocCoc3.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Refresh lại trang Ymeet.me của bạn.<br />
          </div>
        </div>
      )
    }
    else {
      return ''
    }
  }

  renderChromeInstruction() {
    if (UserDevice.getName() === 'pc' && UserDevice.getBrowserName() === 'chrom') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Vào Cài đặt của Google Chrome : <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Chrome0.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Vào Cài đặt tìm kiếm/ Search, gõ “nội dung/ content”: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Chrome1.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Chọn Cài đặt nội dung: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Chrome2.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Bỏ chọn ở ô “Chặn dữ liệu trang web và cookie của bên thứ ba/ Block web data and cookie of third party”. Và chọn Hoàn tất<br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Chrome3.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 5: Refresh lại trang Ymeet.me của bạn<br />
          </div>
        </div>
      )
    }
    else {
      return ''
    }
  }

  renderAndroidCoccocInstruction() {
    if (UserDevice.getName() === 'android' && UserDevice.getBrowserName() === 'coc_coc') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Vào phần Cài đặt của trình duyệt bằng cách click vào logo Cốc Cốc ở góc phải trên cùng: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Coccoc1.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Vào phần “Cài đặt”: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Coccoc2.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Chọn Cài đặt trang web: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Coccoc3.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Chọn Cookie:<br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Coccoc4.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 5: Trạng thái cài đặt Cookie của bạn đang là Bị chặn, hãy kéo sang phải để chuyển trạng thái sang Cho phép:<br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Coccoc5.png`} /></div>
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Coccoc6.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 6: Refresh lại trang Ymeet.me của bạn<br />
          </div>
        </div>
      )
    }
    else {
      return ''
    }
  }

  renderAndroidChromeInstruction() {
    if (UserDevice.getName() === 'android' && UserDevice.getBrowserName() === 'chrom') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Vào Cài đặt/ Setting Chrome của bạn: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Chrome1.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Chọn Cài đặt/ Setting: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Chrome2.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Chọn Cài đặt trang web: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Chrome3.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Chọn Cookie:<br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Chrome4.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 5: Kéo sang phải để đưa trạng thái của Cookie về Cho phép:<br /><br />
            <div className="mbl txt-bold"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Chrome5.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 6: Refresh lại trang Ymeet.me của bạn<br />
          </div>
        </div>
      )
    }
    else {
      return ''
    }
  }

  renderAndroidFirefoxInstruction() {
    if (UserDevice.getName() === 'android' && UserDevice.getBrowserName() === 'firefox') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Vào Cài đặt/ Setting Firefox của bạn: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_FireFox1.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Chọn Cài đặt/ Setting: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_FireFox2.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Chọn Quyền riêng tư/ Privacy: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_FireFox3.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Chọn Cookie (bạn sẽ thấy trạng thái hiện tại là Chặn/ Disabled):<br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_FireFox4.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 5: Chọn Cho phép/ Enabled:<br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_FireFox5.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 6: Refresh lại trang Ymeet.me của bạn<br />
          </div>
        </div>
      )
    }
    else {
      return ''
    }
  }

  renderFirefoxInstruction() {
    if (UserDevice.getName() === 'pc' && UserDevice.getBrowserName() === 'firefox') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Vào Cài đặt/ Setting Firefox của bạn: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Pc_FireFox1.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Chọn “Tùy chọn”, sau đó chọn “Riêng tư”. Ở phần nhật ký bạn sẽ thấy trạng thái là “Không bao giờ ghi nhớ lược sử” <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Pc_FireFox2.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Ở phần Nhật ký, “Firefox sẽ” bạn chọn “Ghi nhớ lược sử”: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Pc_FireFox3.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Refresh lại trang Ymeet.me của bạn<br />
          </div>
        </div>
      )
    }
    else {
      return ''
    }
  }

  renderIosInstruction() {
    if (UserDevice.getName() === 'ios' && UserDevice.getBrowserName() === 'safari') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Vào Settings > Safari: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Ios_Safari1.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Chọn Block Cookies: <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Ios_Safari2.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Chọn Always Allow (Luôn cho phép) hoặc Allow from websites I visit (Cho phép những trang tôi ghé thăm), Allow from Current Websites Only (Chỉ cho phép trang hiện tại): <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Ios_Safari3.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Refresh lại trang Ymeet.me trong Safari của bạn<br />
          </div>
        </div>
      )
    }
    else {
      return ''
    }
  }

  render() {
    return (
      <div>
        {this.renderCoccocInstruction()}
        {this.renderChromeInstruction()}
        {this.renderFirefoxInstruction()}
        {this.renderAndroidCoccocInstruction()}
        {this.renderAndroidChromeInstruction()}
        {this.renderAndroidFirefoxInstruction()}
        {this.renderIosInstruction()}
      </div>
    )
  }
}

Enable3rdPartyCookies.propTypes = {

}

export default Enable3rdPartyCookies;
