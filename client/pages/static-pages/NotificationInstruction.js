import React, { Component, PropTypes } from 'react';
import Header from 'components/headers/Header';
import { CDN_URL } from 'constants/Enviroment'
import * as UserDevice from 'utils/UserDevice';

class NotificationInstruction extends Component {
  renderCoccocInstruction() {
    if (UserDevice.getName() === 'pc' && UserDevice.getBrowserName() === 'coc_coc') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Click vào biểu tượng hình khóa trên thanh công cụ:  <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction14.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Bạn sẽ thấy phần “Thông báo” đang ở trạng thái “Chặn”. Click chọn biểu tượng tam giác xổ xuống, sau đó chọn “Sử dụng cài đặt mặc định chung (Hỏi) <br /><br />
            <div className="mbl"><img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction9.png`} /></div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Đăng xuất và đăng nhập lại vào Ymeet.me. Khi đăng nhập, và được yêu cầu cho phép nhận thông báo, chọn “Cho phép” để hoàn thành.<br />
          </div>
        </div>
      )
    }
    return ''
  }

  renderChromeInstruction() {
    if (UserDevice.getName() === 'pc' && UserDevice.getBrowserName() === 'chrom') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Click vào biểu tượng hình khóa trên thanh công cụ như hình sau: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction1.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Bạn sẽ thấy phần “Thông báo” đang ở trạng thái “Chặn”. Click chọn biểu tượng tam giác xổ xuống, sau đó chọn “Sử dụng cài đặt mặc định chung” <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction10.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Đăng xuất và đăng nhập lại vào Ymeet.me. Khi đăng nhập, và được yêu cầu cho phép nhận thông báo, chọn “Cho phép” để hoàn thành.<br />
          </div>
        </div>
      )
    }
    return ''
  }

  renderAndroidCoccocInstruction() {
    if (UserDevice.getName() === 'android' && UserDevice.getBrowserName() === 'coc_coc') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Chọn biểu tượng khóa trên thanh công cụ: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction3.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Bạn sẽ nhìn thấy trạng thái nhận thông báo hiện tại là Bị chặn. Click chọn “Cài đặt trang web” để tiếp tục cài đặt: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction2.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Click chọn “Xóa và cài đặt lại” <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction7.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Đăng xuất và đăng nhập lại vào Ymeet.me. Khi đăng nhập, và được yêu cầu cho phép nhận thông báo, chọn “Cho phép” để hoàn thành.<br />
          </div>
        </div>
      )
    }
    return ''
  }

  renderAndroidChromeInstruction() {
    if (UserDevice.getName() === 'android' && UserDevice.getBrowserName() === 'chrom') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Click chọn biểu tượng khóa ở thanh công cụ như sau: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction5.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Click chọn Site etting/ Cài đặt trang web: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction6.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Click chọnClear & Reset/ Xóa và cài đặt lại <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction11.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Đăng xuất và đăng nhập lại vào Ymeet.me. Khi đăng nhập, và được yêu cầu cho phép nhận thông báo, chọn “Cho phép” để hoàn thành.<br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Mobile_Android_Chrome4.png`} alt="" />
            </div>
          </div>
        </div>
      )
    }
    return ''
  }

  renderAndroidFirefoxInstruction() {
    if (UserDevice.getName() === 'android' && UserDevice.getBrowserName() === 'firefox') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Click chọn biểu tượng hình khóa ở thanh công cụ như sau: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction16.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Chọn Edit Site setting/ Chỉnh sửa cài đặt trang: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction13.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Click chọn ở phần Notifications/ Thông báo. Chọn Clear <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/NotificationInstruction/noti_instruction13.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Đăng xuất và đăng nhập lại vào Ymeet.me. Khi đăng nhập, và được yêu cầu cho phép nhận thông báo, chọn “Cho phép” để hoàn thành.<br />
          </div>
        </div>
      )
    }
    return ''
  }

  renderFirefoxInstruction() {
    if (UserDevice.getName() === 'pc' && UserDevice.getBrowserName() === 'firefox') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Click vào biểu tượng hình khóa trên thanh công cụ: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general//NotificationInstruction/noti_instruction4.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Bạn sẽ thấy phần cài đặt “Quyền hạn” với phần Nhận thông báo/ Receive Notification đang ở trạng thái là Chặn/ Block. Click chọn “Always Ask/ Luôn hỏi”  <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general//NotificationInstruction/noti_instruction15.png`} alt="" />
            </div>
            <br />
            hoặc nếu firefox của bạn không hiển thị được như trên. Click vào biểu tượng mũi tên:
            <div className="mbl">
              <img src={`${CDN_URL}/general//NotificationInstruction/noti_instruction8.png`} alt="" />
            </div>
            <br />
            Sau đó, chọn “Thông tin thêm”, vào phần “Quyền hạn”. Với phần “Nhận thông báo/ Receive notification”, click chọn “Luôn hỏi”:
            <div className="mbl">
              <img src={`${CDN_URL}/general//NotificationInstruction/noti_instruction12.png`} alt="" />
            </div>
            <br />
          </div>
          <div className="mbm txt-bold">
            Bước 3:  Đăng xuất và đăng nhập lại vào Ymeet.me. Khi đăng nhập, và được yêu cầu cho phép nhận thông báo, chọn “Cho phép” để hoàn thành.<br />
            <div className="mbl">
              <img src={`${CDN_URL}/general//NotificationInstruction/noti_instruction1.png`} alt="" />
            </div>
          </div>
        </div>
      )
    }
    return ''
  }

  renderIosInstruction() {
    if (UserDevice.getName() === 'ios' && UserDevice.getBrowserName() === 'safari') {
      return (
        <div className="col-xs-12">
          <div className="mbs"></div>
          <div className="mbm txt-bold">
            Bước 1: Vào Settings > Safari: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Ios_Safari1.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 2: Chọn Block Cookies: <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Ios_Safari2.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 3: Chọn Always Allow (Luôn cho phép) hoặc Allow from websites I visit (Cho phép những trang tôi ghé thăm), Allow from Current Websites Only (Chỉ cho phép trang hiện tại): <br /><br />
            <div className="mbl">
              <img src={`${CDN_URL}/general/InstructionPage/Enable3rdCooky/Enable_3rd_Cooky_Ios_Safari3.png`} alt="" />
            </div>
          </div>
          <div className="mbm txt-bold">
            Bước 4: Refresh lại trang Ymeet.me trong Safari của bạn<br />
          </div>
        </div>
      )
    }
    return ''
  }

  render() {
    return (
      <div className="container">
        <Header {...this.props} />
        <div className="row mbm">
          <div className="col-xs-12">
            <h3 className="txt-heading">
              Cách cho phép nhận thông báo từ Ymeet.me sau khi đã từ chối trên {`${UserDevice.getName().toUpperCase()}`}
            </h3>
          </div>
        </div>
        {this.renderCoccocInstruction()}
        {this.renderChromeInstruction()}
        {this.renderFirefoxInstruction()}
        {this.renderAndroidCoccocInstruction()}
        {this.renderAndroidChromeInstruction()}
        {this.renderAndroidFirefoxInstruction()}
        {this.renderIosInstruction()}
      </div>
    )
  }
}

export default NotificationInstruction;
