import React, { Component } from 'react';
import Header from 'components/headers/Header';
import AnonymousHeader from 'components/headers/AnonymousHeader'
import AnonymousFooter from 'components/footers/AnonymousFooter'
import * as ymmStorage from 'utils/ymmStorage';

export default function addHeader(Wrappedcomponent) {
  return class extends Component {
    render() {
      window.scroll(0, 0);
      let isAnonymousUser = true;
      try {
        if (this.props.user_profile && this.props.user_profile.current_user && this.props.user_profile.current_user.welcome_page_completion === 4) {
          isAnonymousUser = false;
        }
      } catch (e) {}
      return (
        <div>
          {!isAnonymousUser && <Header {...this.props} />}
          {isAnonymousUser && <AnonymousHeader />}
          <Wrappedcomponent {...this.props} />
          {isAnonymousUser && <AnonymousFooter />}
        </div>
      );
    }
  }
}
