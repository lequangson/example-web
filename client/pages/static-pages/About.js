import React from 'react';
import { connect } from 'react-redux';
import DropdownPanel from 'components/commons/DropdownPanel';
import addHeader from './addHeader';

class About extends React.Component {
  render() {
    return(
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h2 className="txt-heading">Giới thiệu</h2>
              <p>Chào mừng bạn đến với <b>Ymeet.me - Hẹn hò an toàn cho phụ nữ</b>. Một sản phẩm được xây dựng và phát triển bởi Công ty TNHH Media Max Japan (Việt Nam)</p>
            </div>
          </div>
        </div>
        <DropdownPanel
          panelTitle="Câu chuyện của Ymeet.me"
          isOpen
        >
          <div className="col-xs-12">
            <div className="mbs"></div>
            <p>Hà Nội, ngày 11 tháng 11</p>
            <p>Nếu bạn đang đọc những dòng tâm sự này thì chắc hẳn câu chuyện mà tôi kể sau đây sẽ khiến bạn lưu tâm. Vì đơn giản đây là một câu chuyện có thật và là khởi nguồn cho thứ mà bạn đang sử dụng.</p>
            <p>Tôi là một người Nhật, đã sinh sống và làm việc ở Việt Nam được hai năm. Cuộc sống ở Việt Nam khá tốt. Tôi có nhiều bạn bè ở đây, và đặc biệt rất thân thiết với một cô bạn.</p>
            <p>Một ngày đông Hà Nội, cô ấy bất ngờ gọi cho tôi và bật khóc: “Em không tin vào đàn ông nữa. Em mệt mỏi quá rồi!”. Cô ấy vừa nói vừa khóc, nhưng qua cách cô ấy nói, tôi hiểu cô ấy vừa chia tay người yêu vì phát hiện ra anh ta không hề chung thuỷ. Những khó khăn trong chuyện tình cảm, áp lực từ gia đình khiến cô ấy vô cùng mệt mỏi và chán chường.</p>
            <p>Một thời gian sau, tôi có gợi ý cô ấy hãy thử sử dụng một dịch vụ hẹn hò trực tuyến nào đó. Vì ở Nhật, những người như cô rất nhiều và họ thường tìm đến các dịch vụ hẹn hò trực tuyến để tìm kiếm tình yêu thực sự cho bản thân. Tuy nhiên, câu trả lời của cô khiến tôi suy nghĩ mãi: “Em sợ lắm, ở Việt Nam không giống ở Nhật đâu. Em có cảm giác không an toàn vì ở đây dịch vụ như thế không phổ biến, và em cũng chẳng biết nhiều thông tin”.</p>
            <p>Làm việc ở Việt Nam, tôi nhận ra rằng những người như cô bạn của tôi không hề ít và việc nhiều người không hề tin tưởng vào dịch vụ hẹn hò trực tuyến là khá phổ biến. Ý tưởng về một nơi mà qua đó phụ nữ có thể tin tưởng và cảm thấy thực sự an toàn để tìm được đúng một nửa của đời mình nhen nhóm dần.</p>
            <p>Và Ymeet.me - hẹn hò an toàn được ra đời như thế!</p>
            <div className="txt-bold">Ymeet.me đem đến cho bạn: </div>

            <div className="txt-bold">Sự tin cậy & an toàn:</div>
            <p>Ymeet.me đòi hỏi người dùng phải đăng ký bằng tài khoản Facebook của họ, và chỉ người dùng với tình trạng "độc thân" mới có thể tham gia. Việc xác thực thành viên như xác minh độ tuổi, tình trạng hôn nhân, v.v sẽ được thực hiện một cách nghiêm ngặt. Mọi hoạt động sau đó trên hệ thống đều được giám sát 24 giờ một ngày, 365 ngày một năm.</p>

            <div className="txt-bold">Nghiêm túc:</div>
            <p>Với bộ lọc đa dạng thông tin và thuật toán ghép đôi thông minh, bạn sẽ tìm được chính xác những người đủ điều kiện và phù hợp nhất với mình. Ymeet.me cho phép người dùng khác giới gửi tín hiệu THÍCH cho nhau, và chỉ có thể trò chuyện khi cả hai cùng THÍCH nhau. Nhờ đó, bạn sẽ không bao giờ phải gặp tình trạng quấy rối bởi những người xấu.</p>

            <div className="txt-bold">Bí mật tuyệt đối:</div>
            <p>Bạn không muốn người quen, bạn bè biết bạn tham gia Ymeet.me? Không sao cả, hệ thống sẽ đồng bộ với tài khoản Facebook của bạn, qua đó không một ai trong danh sách bạn bè Facebook có thể thấy bạn trên Ymeet.me.</p>
            <p>Bên cạnh đó, tất cả thông tin và hoạt động trên hệ thống của Ymeet.me được giữ bí mật hoàn toàn. Ymeet.me cho phép người dùng không hiển thị tên thật, Không hiển thị bất kỳ thông tin nào lên Facebook cá nhân của người dùng.</p>
            <p>Hơn cả một ứng dụng hẹn hò, Ymeet.me ra đời từ sự đồng cảm và thấu hiểu với những tâm tư của người phụ nữ trong tình yêu.</p>
            <p>Đồng sáng lập Ymeet.me</p>
            <div className="txt-bold">Tomokazu Imamura</div>
          </div>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="Về Media Max Japan (Việt Nam)"
        >
          <div className="col-xs-12">
            <div className="mbs"></div>
            <p>Media Max Japan được thành lập cách đây gần 20 năm (1996), chỉ sau một thời gian ngắn kể từ khi internet có mặt tại Nhật Bản.</p>
            <p>Với mục tiêu trở thành một công ty dịch vụ trực tuyến được nhiều người biết đến tại châu Á, trụ sở của Media Max Japan tại Việt Nam chính thức được thành lập kể từ tháng 7 năm 2014.</p>
            <p>Hiện nay, Media Max Japan (Việt Nam) đang cung cấp các dịch vụ chính sau:</p>
            <ul>
              <li>Inhouse Solution (Giải pháp thị trường nội địa): Với chiến lược xây dựng các sản phẩm “Tạo ra thị trường, chứ không chỉ là thị trường chọn sản phẩm”, hiện nay chúng tôi đang triển khai một số dự án lớn tại thị trường Việt Nam. Mà Ymeet.me chính là một trong số những dự án đó.</li>
              <li>
                Web-Outsourcing Solution (Giải pháp web thuê ngoài): Với kinh nghiệm gần 20 năm trong việc phát triển các ứng dụng web, chúng tôi có:
                <ul>
                  <li>Các dịch vụ chất lượng.</li>
                  <li>Khả năng phát triển toàn diện với quy trình nghiệp vụ hoàn chỉnh.</li>
                  <li>Sự thấu hiểu và cam kết cao nhất cho dự án.</li>
                  <li>Tính linh hoạt cao sau khi chuyển giao.</li>
                </ul>
              </li>
              <li>Web Marketing: Với đội ngũ các chuyên gia online marketing, SEO đến từ Nhật Bản, chúng tôi cam kết mang lại các dịch vụ hỗ trợ thúc đẩy việc bán hàng trực tuyến thuận lợi và hiệu quả.</li>
              <li>Web Design (Thiết kế web): Trang web đóng vai trò quan trọng như chiếc danh thiếp trực tuyến của tổ chức bạn. Giao diện hấp dẫn, thân thiện với người sử dụng trang web chính là chìa khóa cho sự thành công của mỗi công ty. 2/3 người dùng Internet Việt Nam truy cập các trang web trên điện thoại di động mỗi ngày & Mobile web sẽ vượt qua máy tính để bàn trong vòng 5 năm tới...Bạn có thực sự muốn một trang Mobile web?</li>
            </ul>
          </div>
        </DropdownPanel>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(addHeader(About));
