import React from 'react';
import { connect } from 'react-redux';
import DropdownPanel from 'components/commons/DropdownPanel'
import addHeader from './addHeader';

class Privacy extends React.Component {
  render() {
    return(
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h2 className="txt-heading">
                Chính sách bảo mật
                <br />
                <span className="txt-red txt-thin txt-sm">(Hiệu lực từ tháng 3/2016)</span>
              </h2>
              <p>
                Quy định về chính sách bảo mật của Ymeet.me giúp bạn hiểu rằng chúng tôi thu thập và sử dụng thông tin mà bạn chia sẻ, giúp bạn có những quyết định sáng suốt khi sử dụng từng chức năng tại Ymeet.me
                <br />
                Khi sử dụng Ymeet.me, bạn mặc nhiên chấp nhận tất cả những điều khoản trong Quy định về quyền riêng tư này.
                Nếu bạn có bất kỳ câu hỏi hoặc đề xuất nào về các quy định này, xin bạn vui lòng liên lạc với chúng tôi qua địa chỉ email: <a href="mailto:contact@ymeet.me"><b>contact@ymeet.me</b></a>
              </p>
            </div>
          </div>
        </div>
        <DropdownPanel
          panelTitle="Thu thập thông tin"
          isOpen
        >
          <p className="col-xs-12">
            Khi bạn đăng ký làm thành viên, Ymeet.me chỉ thu thập thông tin về tên, tuổi, giới tính, ngày sinh, e-mail và số bạn trên Facebook của bạn. Mọi thông tin khác là do bạn cung cấp và chịu trách nhiệm.
            <br />
            Khi bạn quyết định sử dụng Ymeet.me, tức là bạn đã đồng ý chia sẻ thông tin cá nhân với người dùng khác.
            <br />
            Nếu bạn thấy bất kỳ sự lạm dụng hoặc thu thập trái phép về thông tin người dùng, vui lòng thông báo cho chúng tôi qua địa chỉ email: <a href="mailto:contact@ymeet.me"><b>contact@ymeet.me</b></a>.
          </p>
        </DropdownPanel>

        <DropdownPanel
          panelTitle="Sử dụng thông tin"
        >
          <div className="col-xs-12">
            <div className="mbs"></div>
            <ul className="list-unstyled">
              <li>Chúng tôi sử dụng thông tin của bạn để giúp bạn hẹn hò trực tuyến hiệu quả thông qua Ymeet.me. Ngoài ra, để gửi tới bạn những cập nhật mới nhất của hệ thống.</li>
              <li>Chúng tôi không cung cấp thông tin cá nhân của bạn cho bên thứ ba mà không được sự đồng ý trước của bạn.</li>
              <li>Chúng tôi cung cấp thông tin của bạn cho bên thứ ba, khi việc này là yêu cầu pháp lý hoặc được bạn đồng ý.</li>
              <li>Để các hoạt động của Website đem lại kết quả, bao gồm việc xử lý các giao dịch và hỗ trợ các hoạt động của bạn trên Ymeet.me,
              chúng tôi có thể chia sẻ thông tin cá nhân của bạn cho đại lý, đại diện, hoặc các đơn vị thầu khoán để họ có thể hỗ trợ chúng tôi về các dịch vụ như:
              các cách thức chi trả tiền, dịch vụ email và thực hiện đơn đặt hàng của bạn.</li>
              <li>Bên thứ ba được yêu cầu không sử dụng thông tin của bạn cho bất kỳ mục đích nào khác.</li>
              <li>Chúng tôi sẽ không tiết lộ thông tin khi chưa xác định được những đề nghị của cơ quan pháp luật hoặc của người yêu cầu là hợp pháp theo qui định hiện hành.
              Ngoài ra, chúng tôi có thể sử dụng tài khoản hoặc các thông tin khác của bạn khi chúng tôi cần phải tuân theo yêu cầu của pháp luật, để bảo vệ quyền lợi và tài sản của chúng tôi,
              nhằm ngăn chặn sự gian lận hoặc các hoạt động phi pháp khác thông qua các dịch vụ của Ymeet.me. Điều này có thể gồm cả việc chia sẻ với các công ty, luật sư, đại lý, các phòng ban của chính phủ.</li>
            </ul>
          </div>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="Sửa đổi hoặc xóa thông tin"
        >
          <p className="col-xs-12">
            Việc truy nhập và quản lý toàn bộ thông tin cá nhân của các thành viên trên Ymeet.me là không khó khăn bởi bạn có thể cập nhật hồ sơ của mình trên trang quản lý hồ sơ cá nhân của bạn trên Ymeet.me.
            Thông tin sẽ được cập nhật ngay lập tức. Bất kỳ thành viên nào muốn xóa tài khoản đều có thể dễ dàng thực hiện trên trang Ymeet.me.
            Khi đã xóa, tài khoản và tất cả các thông tin của bạn sẽ không hiển thị trên trang Ymeet.me.
          </p>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="Bảo mật"
        >
          <p className="col-xs-12">
            Thông tin về tài khoản của bạn được đặt trên một máy chủ an toàn và được tường lửa bảo vệ.
            Trên thực tế, an ninh mạng không an toàn tuyệt đối, vẫn có thể xảy ra sự cố. Vì thế nên Ymeet.me không thể đảm bảo 100% về tính bảo mật này, nếu bạn sơ suất để lộ thông tin cá nhân gây hậu quả nghiêm trọng,
            Ymeet.me không chịu trách nhiệm về những thiệt hại xảy ra. Hãy luôn luôn cẩn trọng và tự bảo vệ thông tin cá nhân của bạn.
            <br />
            Vì email và tin nhắn nhanh không được coi là những phương thức liên lạc an toàn, chúng tôi yêu cầu bạn không gửi thông tin cá nhân cho người dùng khác của Ymeet.me qua dịch vụ email hay tin nhắn nhanh khi chưa thực sự chắc chắn để đảm bảo sự tương tác an toàn cho bạn.
            Nếu bạn có bất kỳ câu hỏi nào về bảo mật của website, vui lòng liên lạc với chúng tôi qua địa chỉ email: <a href="mailto:contact@ymeet.me"><b>contact@ymeet.me</b></a>
          </p>
        </DropdownPanel>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(addHeader(Privacy));


