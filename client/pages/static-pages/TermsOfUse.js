import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import DropdownPanel from 'components/commons/DropdownPanel'
import * as ymmStorage from 'utils/ymmStorage';
import GoogleOptimize from 'utils/google_analytic/google_optimize';
import addHeader from './addHeader';

class TermsOfUse extends Component {

  componentWillUnmount() {
    ymmStorage.setItem('keep', true)
  }

  @GoogleOptimize
  componentDidMount() {}

  render() {
    return(
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h2 className="txt-heading">
                Điều khoản sử dụng
                <br />
                <span className="txt-red txt-thin txt-sm">(Hiệu lực từ tháng 3/2016)</span>
              </h2>
              <p>
                Điều khoản sử dụng (gọi tắt là ĐKSD) là các thoả thuận mang tính pháp lý giữa Ymeet.me với thành viên đăng ký trên trang Ymeet.me (gọi tắt là thành viên), về việc truy cập trang Ymeet.me tại địa chỉ: https://ymeet.me/.
                Bao gồm các dịch vụ, tính năng, nội dung hoặc các ứng dụng được giới thiệu trên trang Ymeet.me và các trang web khác được liên kết trực tiếp hoặc gián tiếp từ trang web này (gọi tắt là website).
              </p>
              <p>
                Bằng việc sử dụng các dịch vụ của chúng tôi, có nghĩa là bạn đã đồng ý với các điều khoản được đăng tải ở đây.
                Tại bất cứ thời điểm nào, chúng tôi có thể cập nhật các quy định này mà không cần báo trước.
                Khi bạn tiếp tục sử dụng dịch vụ sau khi các điều khoản được cập nhật, có nghĩa là bạn đã chấp nhận và đồng ý tuân theo bản thỏa thuận đã được cập nhật.
                Chúng tôi khuyến khích bạn thường xuyên tham khảo điều khoản sử dụng và tuân thủ các điều này để sử dụng dịch vụ của chúng tôi một cách hiệu quả nhất.
              </p>
            </div>
          </div>
        </div>
        <DropdownPanel
          panelTitle="1. Đăng ký, cảnh báo và khóa tài khoản"
          isOpen
        >
          <div className="col-xs-12">
            <p>Để đăng ký thành viên bạn phải đáp ứng đầy đủ 3 điều kiện sau:</p>
            <ul>
            <li>Từ 18 tuổi trở lên.</li>
            <li>Đã có tài khoản Facebook với ít nhất 10 người bạn.</li>
            <li>Tình trạng quan hệ trên Facebook phải là: đang độc thân, có mối quan hệ phức tạp, đã ly hôn, hoặc góa.</li>
            </ul>
            <p>Ymeet.me có thể chấm dứt tư cách thành viên của bạn ngay lập tức mà không cần phải thông báo trước. Các trường hợp mà bạn có thể bị chúng tôi khóa tài khoản bao gồm:</p>
            <ul>
              <li><span className="txt-red">Có ảnh chứa nội dung độc hại:</span> khiêu dâm, bạo lực, kinh dị,...</li>
              <li><span className="txt-red">Có ảnh chứa thông tin liên hệ:</span> địa chỉ, email, số điện thoaị, facebook, zalo, các đường dẫn,...</li>
              <li><span className="txt-red">Có thông tin cá nhân chứa nội dung độc hại:</span> tìm kiếm tình dục, khiêu dâm, bạo lực, kinh dị, ma túy,...</li>
              <li><span className="txt-red">Có thông tin cá nhân chứa thông tin liên hệ:</span> địa chỉ, email, số điện thoaị, facebook, zalo, các đường dẫn,...</li>
              <li><span className="txt-red">Có tài khoản giả mạo:</span> đã kết hôn, hồ sơ giả, thông tin giả,...</li>
              <li><span className="txt-red">Có hành vi, hành động xấu:</span> đe dọa, rình rập, quấy rối người dùng khác,...</li>
              <li><span className="txt-red">Có lời lẽ xấu:</span> thô tục, lăng mạ hay tấn công người dùng khác,...</li>
              <li><span className="txt-red">Có mục đích xấu:</span> kinh doanh, lợi nhuận,...</li>
            </ul>
            <p>Ymeet.me có quyền xóa ảnh của bạn nếu ảnh đó không hợp lệ. Các trường hợp ảnh không hợp lệ bao gồm:</p>
            <ul>
              <li><span className="txt-red">Không phải ảnh người:</span> Ảnh động vật, đồ vật, phong cảnh, thiên nhiên, ...</li>
              <li><span className="txt-red">Ảnh nhân vật:</span> Truyện tranh, tranh vẽ, game, hoạt hình, ...</li>
              <li><span className="txt-red">Ảnh người nổi tiếng:</span> Diễn viên, người mẫu, ca sĩ, vận động viên, cầu thủ, ...</li>
              <li><span className="txt-red">Ảnh người khác:</span> Trẻ em, bố mẹ, người khác giới, ...</li>

            </ul>
            <p>Người dùng có thể báo cáo vi phạm của người dùng khác cho chúng tôi. Thông tin sẽ được kiểm chứng và có biện pháp xử lý kịp thời.</p>
          </div>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="2. Quyền sở hữu trí tuệ"
        >
          <p className="col-xs-12">Tất cả quyền sở hữu trí tuệ tồn tại trong website này đều thuộc về Ymeet.me hoặc được cấp phép hợp pháp cho Ymeet.me sử dụng tại website này.
          Trừ khi được sự đồng ý bằng văn bản của Ymeet.me, bạn không được phép đăng tải, gửi, xuất bản, tái sản xuất, sửa đổi, truyền hoặc phân phát dưới bất cứ hình thức nào.
          </p>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="3. Hành vi ứng xử của thành viên"
        >
          <div className="col-xs-12">
            <h4>A. Các nội dung chính trị, tôn giáo, sắc tộc và đồi trụy </h4>
            <div>Bạn đồng ý rằng:</div>
            <ul>
              <li>Không đăng tải hay bình luận về các vấn đề chính trị, tôn giáo, sắc tộc.</li>
              <li>Không đăng tải hay trao đổi các thông tin liên quan tới chính sách của Đảng và Nhà nước CHXHCN Việt Nam.</li>
              <li>Không lợi dụng website để tiếp tay cho các hành động vi phạm pháp luật Nhà nước CHXHCN Việt Nam.</li>
              <li>Không truyền bá các nội dung văn hoá phẩm bạo lực, đồi trụy, trái với thuần phong mỹ tục Việt Nam.</li>
            </ul>

            <h4>B. Cách thức ứng xử</h4>
            <div>Bạn đồng ý rằng không được phép đăng tải, gửi, xuất bản, tái sản xuất, sửa đổi, truyền hoặc phân phát những thông tin saudưới bất cứ hình thức nào:</div>
            <ul>
              <li>Những thông tin về bản quyền, các nhãn hiệu, tên gọi độc quyền và các nội dung khác được bảo vệ bởi luật sở hữu trí tuệ vào mục đích thương mại, hoặc tải lên, gửi, xuất bản, truyền, tái sản xuất, hoặc tạo ra các biến thể của các nội dung đó mà không có sự đồng ý bằng văn bản của chủ nhân hoặc người giữ bản quyền.</li>
              <li>Bất kỳ thông tin và hình ảnh bất hợp pháp, nguy hại, đe dọa, lạm dụng, sách nhiễu, có hại, phỉ báng, khiếm nhã, lừa gạt, bôi nhọ, sỉ nhục, tục tĩu, khiêu dâm, ghê rợn, xúc phạm, lăng mạ, giả mạo cá nhân hoặc tổ chức khác, thù hận, kích động, phân biệt chủng tộc, dân tộc … gây khó chịu hoặc trái với chuẩn mực đạo đức chung của xã hội thuộc bất kỳ loại nào, bao gồm cả việc truyền bá hay khuyến khích những hành vi có thể cấu thành tội phạm hay vi phạm bất cứ điều khoản nào của luật pháp địa phương, quốc gia hay nước ngoài.</li>
              <li>Các thông tin dẫn đến việc truyền bá hoặc khuyến khích các hành vi trái luật pháp.</li>
              <li>Tải lên, đăng, gửi thư, cung cấp hoặc truyền tải các tài liệu quảng cáo, khuyến mại, "thư linh tinh", "thư rác", "thư gửi hàng loạt", …và các thông tin khác mà người dùng không mong muốn hoặc không cho phép vì mục đích thương mại.</li>
              <li>Gửi hoặc truyền bất kỳ thông tin hoặc phần mềm nào có chứa bất kỳ loại virus, Trojan, các đoạn mã máy tính, các tập tin hoặc các chương trình được thiết kế để gây cản trở, phá hỏng hoặc hạn chế các chức năng của phần cứng, phần mềm máy tính hoặc thiết bị viễn thông.</li>
              <li>Bất kì thông tin cá nhân của người khác mà chưa được người đó đồng ý.</li>
            </ul>

            <h4>C. Gây rối, hoạt động phạm pháp</h4>
            <div>Bạn đồng ý rằng:</div>
            <ul>
              <li>Chỉ sử dụng Dịch vụ của chúng tôi cho các mục đích sử dụng cá nhân, phi thương mại.</li>
              <li>Bạn sẽ không sử dụng các dịnh vụ của chúng tôi vào các hoạt động phi pháp.</li>
              <li>Không cố gắng để truy nhập bất hợp pháp đến Dịch vụ, thu thập các thông tin hoặc tài liệu, các tài khoản của người dùng khác.</li>
              <li>Không gây cản trở hoặc phá rối dịch vụ.</li>
              <li>Không tạo nhiều hơn một tài khoản trên cùng một địa chỉ IP.</li>
              <li>Không quấy rối trong các bài viết. Trong trường hợp bạn lợi dụng các dịch vụ của chúng tôi để thực hiện các hành vi vi phạm pháp luật (ví dụ như lừa đảo, chiếm đoạt, mại dâm …) thì bạn phải hoàn toàn chịu trách nhiệm trước pháp luật và bồi thường mọi thiệt hại cho chúng tôi.</li>
            </ul>
          </div>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="4. Nội dung thành viên đăng tải trên Website"
        >
          <div className="col-xs-12">
            <ul>
              <li>Bạn sẽ phải hoàn toàn chịu trách nhiệm về tất cả nội dung thông tin mà bạn đăng tải, công bố, truyền và phân phối trên website (gọi tắt là thông tin thành viên).
              </li>
              <li>Bạn đồng ý rằng Ymeet.me không giữ quyền điều khiển các nội dung này, tuy nhiên chúng tôi có quyền xóa các thông tin do bạn gửi lên nếu nội dung của chúng không tuân thủ các điều khoản trong ĐKSD,
              hoặc đi ngược lại với thuần phong mỹ tục, pháp luật và văn hóa Việt Nam. Hoặc nội dung của chúng dùng để lăng mạ, sỉ nhục, phỉ báng, gây hấn, kích động, chia rẽ các thành viên khác.</li>
            </ul>
          </div>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="5. Các liên kết tới các trang Web khác"
        >
          <div className="col-xs-12">
            <ul>
              <li>Để thuận tiện cho việc sử dụng của người dùng, dịch vụ của chúng tôi có cung cấp cho người dùng một số liên kết đến các trang web khác (có tên miền không chứa ymeet.me).
              Chúng tôi không chịu trách nhiệm về các thông tin, dịch vụ và nội dung của những website này.</li>
              <li>Bất cứ sự tham chiếu (truy cập) nào tới trang web nào được liên kết ở đây hoặc tới một sản phẩm hay dịch vụ đặc biệt nào của hãng thứ ba trên trang web này không phải là một khuyến cáo của Ymeet.me.
              Người dùng chịu hoàn toàn trách nhiệm trong việc sử dụng và khai thác các website này.</li>
            </ul>
          </div>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="6. Thông báo vi phạm bản quyền"
        >
          <div className="col-xs-12">
            <p>Nếu bạn phát hiện ra ý tưởng, tác phẩm của bạn đã bị sao chép dẫn đến vi phạm bản quyền hoặc nếu bạn phát hiện việc vi phạm thông tin trên trang web của chúng tôi,
            vui lòng liên hệ với chúng tôi và cung cấp cho chúng tôi một số thông tin dưới đây:</p>
            <ul>
              <li>Mô tả tác phẩm của bạn mà bạn cho rằng đã bị vi phạm bản quyền.</li>
              <li>Mô tả nơi thông tin bị vi phạm trên Website.</li>
              <li>Địa chỉ, số điện thoại và địa chỉ email của bạn.</li>
              <li>Bạn phải chứng minh được rằng việc sử dụng này là không được sự cho phép của người giữ bản quyền, người đại diện hoặc luật pháp.</li>
              <li>Bạn phải khẳng định rằng, các thông tin trên là chính xác và bạn chính là người giữ bản quyền hoặc là người được ủy thác giữ bản quyền đó.</li>
              <li>Chữ ký vật lý hoặc điện tử của người được ủy thác độc quyền.</li>
            </ul>
            <p>
              Vui lòng gửi thông tin của bạn tới Ymeet.me theo địa chỉ sau: <span className="txt-bold">contact@ymeet.me</span>.
              Căn cứ vào các thông tin nhận được, Ymeet.me sẽ thông báo cho thành viên về việc được cho là vi phạm.
              (Không bao gồm thông tin cá nhân của bạn, các thông tin chỉ được cung cấp nếu pháp luật yêu cầu).
              Nếu Ymeet.me tiếp tục nhận được thông báo về việc vi phạm bản quyền liên quan đến nội dung đã được đăng tải của thành viên, thành viên đó sẽ được xem là tái vi phạm bản quyền.
              Ymeet.me sẽ xem xét việc chấm dứt hoạt động đối với tài khoản của những người tái vi phạm bản quyền này.
            </p>
          </div>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="7. Giới hạn trách nhiệm pháp lý"
        >
          <div className="col-xs-12">
            <p>
              Ymeet.me sẽ không chịu trách nhiệm pháp lý cho những hậu quả, những thiệt hại (bao gồm kinh tế, lợi nhuận, thu nhập, uy tín, việc sử dụng, gián đoạn kinh doanh, mất mát dữ liệu, hoặc những thiệt hại về chi phí mua hàng hóa,
              hàng hóa/dịch vụ thay thế và các thiệt hại vô hình khác) nảy sinh trực tiếp, gián tiếp, ngẫu nhiên, hay là kết quả logic do truy cập hay ngoài việc truy cập được tới trang Web này hoặc các trang liên kết mặc dù đã được khuyến cáo về khả năng xảy ra những thiệt hại như vậy.
            </p>
          </div>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="8. Miễn trừ"
        >
          <div className="col-xs-12">
            <p>
              Bạn đồng ý bồi thường và đảm bảo cho Ymeet.me, công ty chủ quản (Media Max Japan (Việt Nam)), các viên chức và các nhân viên của chúng tôi, các tổ chức liên kết và các tổ chức liên quan không phải chịu trách nhiệm đối với các yêu cầu bồi thường, bao gồm các khoản lệ phí pháp lý hợp lý, của bất kỳ bên thứ ba nào do hoặc phát sinh từ việc bạn sử dụng dịch vụ, từ nội dung mà bạn gửi, đăng, truyền tải hoặc cung cấp trên Web Site, việc bạn vi phạm Điều khoản sử dụng, việc bạn vi phạm bất kỳ quyền của bất kỳ người nào khác, hoặc việc bạn vi phạm luật pháp hiện hành.
            </p>
          </div>
        </DropdownPanel>
        <DropdownPanel
          panelTitle="9. Thông tin chung"
        >
          <div className="col-xs-12">
            <p>
              Các điều khoản này cùng với Quy định về <a href="/privacy">bảo mật</a> được coi là toàn bộ sự thỏa thuận giữa bạn và Ymeet.me về việc sử dụng dịch vụ của bạn.
              Nếu bất kỳ điều khoản nào của ĐKSD bị vô hiệu thì các điều khoản khác của ĐKSD vẫn sẽ có đầy đủ giá trị và hiệu lực.
              <br />
            </p>
          </div>
        </DropdownPanel>
      </div>
    )
  }

}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(addHeader(TermsOfUse));


