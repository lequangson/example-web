import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import $ from 'jquery';
import { connect } from 'react-redux';
import DropdownPanel from 'components/commons/DropdownPanel';
import { gaSend } from 'actions/Enviroment';
import { CDN_URL, GA_ACTION_OPEN_NEXTLINK_TIPS } from 'constants/Enviroment';
import Enable3rdPartyCookies from './Enable3rdPartyCookies';
import addHeader from './addHeader';

class Helps extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      activeLink: false,
      activeDropdowm: 1
    }
    this.handleClick = this.handleClick.bind(this)
    this.sendGaEvent = this.sendGaEvent.bind(this)
  }

  sendGaEvent(onclickIndex) {
    const { bannerIndex } = this.props.location.query
    if (bannerIndex && parseInt(bannerIndex, 10) === onclickIndex) {
      const ga_message = this.props.Tips.banners[onclickIndex].plainText
      const ga_track_data = {
        message: ga_message,
      }
      this.props.gaSend(GA_ACTION_OPEN_NEXTLINK_TIPS, ga_track_data)
    }
  }

  handleClick() {
    this.setState({
      activeLink: true,
      activeDropdowm: 6
    })
  }

  componentDidUpdate() {
    const { tab, question } = this.props.params
    const activeLink = this.state.activeLink;
    const that = this
    if (activeLink) {
      setTimeout( () => {
      $('html, body').animate({
          scrollTop: $(`#${tab}${question}`).offset().top -50
      }, 500);
      console.log('activeLink', $(`#${tab}${question}`).offset().top) -50
      }
      , 1000)
      this.setState({activeLink: false,})
    }
  }

  componentDidMount() {
    const { tab, question } = this.props.params
    if (tab && question) {
      $('html, body').animate({
          scrollTop: $(`#${tab}${question}`).offset().top - 50
      }, 500);
    }
    else {
      window.scroll(0, 0);
    }
  }

  render() {
    let tab_number = this.props.params.tab ? parseInt(+this.props.params.tab, 10) : 1;
    return (
      <div className="site-content">
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <h2 className="txt-heading">Trợ giúp</h2>
          </div>
        </div>
      </div>
      <DropdownPanel
      panelTitle="Đăng ký và quản lý tài khoản trên Ymeet.me"
      isOpen={tab_number === 1 ? true : false}
      >
      <div className="col-xs-12">
        <div className="mbs"></div>

        <div className="txt-blue txt-bold">1. Q: Làm thế nào để tôi có thể hẹn hò trên Ymeet.me?</div>
        <div className="mbm">
          <b>A:</b> Để hẹn hò trên Ymeet.me, bạn cần tạo một tài khoản, vô cùng đơn giản theo các bước sau đây:
          <ol>
            <li>Đi đến <a href="https://ymeet.me">Ymeet.me</a></li>
            <li>Ấn vào nút "Đăng nhập qua Facebook" hoặc “Đăng nhập bằng G+”</li>
            <li>Nhập các thông tin về tài khoản Facebook hoặc G+ của bạn bao gồm: địa chỉ email và mật khẩu</li>
            <li>Chờ hệ thống xác nhận tài khoản và hoàn thành đăng ký.</li>
          </ol>
        </div>

        <div className="txt-blue txt-bold">2. Q: Tại sao tôi không thể đăng ký tài khoản?</div>
        <div className="mbm">
          <div className="mbm">  <b>A:</b> Có thể tài khoản Facebook của bạn chưa đủ điều kiện để trở thành thành viên của Ymeet.me. Bao gồm:</div>
          <b>- Nếu đăng ký bằng Facebook:</b>
          <ol>
            <li>Bạn phải ít nhất 18 tuổi</li>
            <li> Bạn sở hữu tài khoản có >=10 bạn bè trên Facebook</li>
            <li>Tình trạng quan hệ trên Facebook là độc thân hoặc có mối quan hệ phức tạp hoặc đã ly hôn hoặc góa.</li>
          </ol>
          <b>- Nếu đăng ký bằng G+:</b>
          <ol>
            <li>Bạn phải ít nhất 18 tuổi</li>
            <li>Đã có tài khoản Gmail</li>
          </ol>
          Vui lòng xác nhận lại các yêu cầu này trước khi đăng ký. Nếu bạn có bất kỳ thắc mắc nào, vui lòng <a href="/contact">Liên hệ</a> với chúng tôi.
        </div>

        <div className="txt-blue txt-bold" id="q3">3. Q: Nếu tôi không có tài khoản Facebook, tôi có thể sử dụng Ymeet.me bằng cách khác không?</div>
        <div className="mbm">
          <b>A:</b> Có, bạn có thể đăng nhập bằng tài khoản G+ của mình. Dù đăng nhập bằng cách nào thì người dùng vẫn được coi tương đương như nhau.
        </div>

        <div className="txt-blue txt-bold">4. Q: Tôi đã có tài khoản trên Ymeet.me, tại sao tôi không thể đăng nhập?</div>
        <div className="mbm">
          <div><b>A:</b></div>
          <div><b>- Nếu bạn không thể đăng nhập bằng Facebook:</b></div>
          <div> Có thể bạn đã thay đổi các thông tin trên Facebook dẫn đến trường hợp bạn không còn đủ điều kiện là thành viên của Ymeet.me<div><b>- Nếu bạn không thể đăng nhập bằng G+:</b></div>
          <div> Vui lòng click vào phần thông báo lỗi của hệ thống được hiển thị trên góc phải trên cùng của trang và làm theo hướng dẫn.</div>
          Vui lòng xem <Link to="/terms-of-use">Điều khoản sử dụng</Link> của chúng tôi để biết thêm chi tiết.
          Ngoài ra, vui lòng kiểm tra lại kết nối Internet.
          </div>
        </div>

        <div className="txt-blue txt-bold">5. Q: Làm thế nào để tôi xóa tài khoản trên Ymeet.me?</div>
        <div className="mbm">
          <b>A:</b> Nếu bạn muốn xóa tài khoản trên Ymeet.me, vui lòng vào Cài đặt hệ thống, chọn <Link to="/delete-account">Xóa tài khoản</Link>.
          <div className="txt-red"> Lưu ý: Sau khi xóa tài khoản, bạn vẫn có thể quay trở lại bất cứ lúc nào, mọi thông tin về tài khoản của bạn sẽ được hệ thống khôi phục, ngoại trừ hình ảnh. </div>
        </div>

        <div className="txt-blue txt-bold" id="abc">6. Q: Làm thế nào để tôi đồng bộ dữ liệu từ Facebook lên Ymeet.me?</div>
        <div className="mbm">
          <b>A:</b> Hệ thống sẽ tự động đồng bộ hóa dữ liệu từ Facebook của bạn ngay tại thời điểm đăng ký.
          Đồng thời, việc đồng bộ hóa cũng được tiến hành tự động liên tục hàng tuần.
          Bên cạnh đó, bạn cũng có thể chủ động đồng bộ hóa bằng cách vào Cài đặt hệ thống, chọn <Link to="/facebook-sync" >Đồng bộ hóa dữ liệu từ Facebook</Link> và bấm vào nút Đồng bộ hóa.
        </div>

        <div className="txt-blue txt-bold">7. Q: Làm thế nào để tôi có thể tạo lại tài khoản trên Ymeet.me sau khi đã xóa?</div>
        <div className="mbm">
          <b>A:</b> Bạn cần làm theo các bước sau:
          <ol>
            <li>Đi đến <a href="https://ymeet.me">Ymeet.me</a></li>
            <li>Chọn "Đăng nhập Facebook" hoặc “Đăng nhập bằng G+” (chú ý chọn đúng phương thức đăng nhập mà khi đăng ký bạn đã chọn)</li>
            <li>Chờ hệ thống xác thực tài khoản của bạn</li>
          </ol>
          Sau khi đã xác thực thành công, mọi thông tin về tài khoản của bạn sẽ được hệ thống khôi phục, ngoại trừ hình ảnh. Vì thế hãy nhanh chóng cập nhật các hình ảnh để tiếp tục hẹn hò cùng Ymeet.me nhé.
        </div>

        <div className="txt-blue txt-bold">8. Q: Tại sao tôi lại bị khóa tài khoản?</div>
        <div className="mbm">
          <b>A:</b>Bạn bị khóa tài khoản vì vi phạm các <Link to="/terms-of-use">Điều khoản sử dụng</Link> của chúng tôi. <br />
          Đặc biệt, trong trường hợp có bất cứ một người dùng nào đó đã báo cáo bạn vì những hành vi vi phạm và những hành vi vi phạm đó đã được chúng tôi tìm hiểu, xác định là vi phạm thì bạn cũng sẽ bị khóa tài khoản. Trong trường hợp bạn nghĩ là tài khoản của bạn bị khóa vì một sự nhầm lẫn nào đó, vui lòng <Link to="/contact">Liên hệ</Link> với chúng tôi để được xét duyệt khôi phục lại.
        </div>
      </div>
    </DropdownPanel>
    <DropdownPanel
    panelTitle="Thiết lập hồ sơ trên Ymeet.me"
    isOpen={tab_number === 2 ? true : false}
    >
    <div className="col-xs-12">
      <div className="mbs"></div>

      <div className="txt-blue txt-bold" id="21">1. Q: Làm thế nào để tôi cập nhật và chỉnh sửa các thông tin cá nhân?</div>
      <div className="mbm">
        <b>A:</b> Để cập nhật và chỉnh sửa các thông tin cá nhân bạn vui lòng làm theo hướng dẫn sau đây:
        <ol>
          <li>Đăng nhập Ymeet.me</li>
          <li>Chọn “<i className="fa fa-user"></i> Hồ sơ”, một menu sẽ trượt từ bên phải sang. Chọn phần chứa ảnh và tên hiển thị của bạn ở trên cùng.</li>
          <li>Lựa chọn các thông tin để cập nhật và chỉnh sửa.</li>
        </ol>
        Bằng việc cho phép người dùng đăng ký bằng tài khoản Facebook, Ymeet.me đã đồng bộ hóa tuổi của bạn được thể hiện theo như thông tin trên Facebook. Trong trường hợp bạn muốn thay đổi tuổi của mình, bạn phải sửa đổi ngày tháng năm sinh của mình trên Facebook, sau đó vào Ymeet.me và chọn “Đồng bộ hóa dữ liệu từ Facebook”
Riêng với tên, bạn chỉ được phép thay đổi một lần duy nhất. Đối với các thông tin khác, bạn được thay đổi nhiều lần.
Nếu muốn thay đổi thông tin, bạn vào mục “Hồ sơ” và bắt đầu sửa. Với các thông tin dạng lựa chọn đáp án, sau khi lựa chọn, các kết quả được tự động lưu lại. Đối với các thông tin dạng điền văn bản chi tiết, sau khi điền bạn phải nhớ ấn chọn "Cập nhật" để lưu lại các thay đổi.
      </div>

      <div className="txt-blue txt-bold">2. Q: Tôi có cần phải cập nhật tất cả thông tin cá nhân không?</div>
      <div className="mbm">
        <b>A:</b> Cập nhật tất cả thông tin là không bắt buộc.
        Tuy nhiên, thông tin cá nhân đầy đủ sẽ giúp bạn có những gợi ý phù hợp nhất cũng như giúp cho việc tìm kiếm đối tượng được chính xác nhất.
        Vì thế, hãy cố gắng hoàn thiện tối đa các thông tin cá nhân của bạn để hẹn hò thật chính xác và hiệu quả cùng với Ymeet.me nhé.
      </div>

      <div className="txt-blue txt-bold">3. Q: Tại sao tuổi của tôi không được hiển thị đúng?</div>
      <div className="mbm">
        <div><b>A:</b></div>
        <div><b>- Trong trường hợp bạn đăng ký bằng Facebook:</b></div>
        Bạn vui lòng kiểm tra và sửa lại ngày sinh của mình trên Facebook cho đúng. Sau khi chỉnh sửa, đăng nhập Ymeet.me và tiến hành <Link to="/facebook-sync" >Đồng bộ hóa dữ liệu từ Facebook</Link>.
        <div><b>- Trong trường hợp bạn đăng ký bằng G+:</b></div>
          Bạn không thể tự sửa được do đây chính là thông tin bạn khai báo khi đăng kí tài khoản. Vì vậy, nếu muốn sửa, hãy <Link to="/contact">Liên hệ</Link> với chúng tôi để được giúp đỡ.
      </div>

      <div className="txt-blue txt-bold">4. Q: Tôi có thể tải lên tối đa bao nhiêu ảnh?</div>
      <div className="mbm">
        <b>A:</b> : Bạn có thể tải không giới hạn ảnh cá nhân của mình.
      </div>

      <div className="txt-blue txt-bold">5. Q: Tại sao ảnh của tôi lại bị xóa?</div>
      <div className="mbm">
        <b>A:</b> Ảnh của bạn sẽ bị hệ thống xóa bỏ nếu vi phạm 1 trong các điều sau:
        <ol>
          <li><span className="txt-red">Không phải ảnh người:</span> ảnh động vật, phong cảnh thiên nhiên, đồ vật, ...</li>
          <li><span className="txt-red">Ảnh nhân vật:</span> truyện, tranh vẽ, game, hoạt hình, ...</li>
          <li><span className="txt-red">Ảnh người nổi tiếng:</span> diễn viên, người mẫu, ca sĩ, ...</li>
          <li><span className="txt-red">Ảnh người khác:</span> Trẻ em, bố mẹ, người khác giới, ...</li>
        </ol>
      </div>

      <div className="txt-blue txt-bold">6. Q: Tại sao tôi không thể tải ảnh lên?</div>
      <div className="mbm">
        <b>A:</b> Trong trường hợp bạn muốn tải ảnh lên từ thiết bị di động hoặc máy tính, bạn cần lưu ý chỉ những tệp ảnh có định dạng: .png, .gif, .jpg với dung lượng tối đa là 10Mb mới có thể đăng tải lên được.
        Ngoài ra, bạn vui lòng kiểm tra kết nối Internet.
      </div>

      <div className="txt-blue txt-bold">7. Q: Tôi có thể đổi tên không?</div>
      <div className="mbm">
        <b>A:</b> Bạn được quyền thay đổi tên duy nhất 1 lần. Tên của bạn phải tuân thủ những quy định sau:
        <ol>
        <li>Độ dài tối thiểu là 2, tối đa là 10 ký tự.</li>
        <li>Không chứa ký tự đặc biệt.</li>
        <li>Bắt buộc phải chứa kí tự chữ cái ở đầu, được phép chứa kí tự chữ số.</li>
        </ol>
        <div className="txt-red">Lưu ý:</div>
        <ol>
        <li>Nên để tên thật (họ và tên hoặc tên đệm và tên).</li>
        <li>Tên không chứa thông tin cá nhân (địa chỉ, liên lạc, ngày sinh, ...).</li>
        <li>Tên không chứa từ có ý nghĩa xấu (tục tĩu, xúc phạm, khiêu dâm, phân biệt chủng tộc...).</li>
        <li>Cố ý vi phạm sẽ bị xử lý.</li>
        </ol>
      </div>
    </div>
    </DropdownPanel>

    <DropdownPanel
    panelTitle="Tìm kiếm và Thích"
    isOpen={tab_number === 3 ? true : false}
    >
    <div className="col-xs-12">
      <div className="mbs"></div>

      <div className="txt-blue txt-bold">1. Q: Tôi có thể gửi Thích tới nhiều người cùng một lúc không?</div>
      <div className="mbm">
       <b>A:</b> Có. Bạn hoàn toàn có thể thoải mái gửi Thích tới những người bạn quan tâm mà không hệ bị giới hạn số lượt Thích. Vì thế, đừng ngại ngần gửi Thích để tăng cơ hội gặp người ấy nhanh hơn nhé!
      </div>

    <div className="txt-blue txt-bold">2. Q: Làm thế nào để tôi có thể tìm kiếm những đối tượng theo đúng tiêu chuẩn riêng của cá nhân tôi?</div>
    <div className="mbm">
      <b>A:</b> Chúng tôi hiểu rằng tình yêu không đơn thuần chỉ là những cảm xúc. Hơn thế, giữa hai người cũng cần những đồng điệu và tiêu chuẩn nhất định.
      Chỉ khi đạt được những tiêu chuẩn đó thì việc tìm hiểu và gắn kết về sau mới dễ dàng phát triển được. <br />
      Vì thế, Ymeet.me có những bộ lọc thông minh cho phép bạn có thể tìm kiếm được đối tượng với nhiều điều kiện, tiêu chí khác nhau một cách nhanh chóng.<br />
      Để tiến hành tìm kiếm, bạn làm theo các bước sau:
      <ol>
       <li>Vào "<i className="fa fa-heart "></i> Một nửa" ở góc trái trên cùng màn hình.</li>
       <li>Vào mục “Tìm kiếm”.</li>
       <li>Chọn các thông tin cụ thể đối với từng điều kiện tìm kiếm.</li>
       <li>Bấm chọn nút "Tìm kiếm" để tiến hành tìm kiếm.</li>
     </ol>
    </div>
    </div>
    </DropdownPanel>
    <DropdownPanel
    panelTitle="Tương tác với đối tượng"
    isOpen={tab_number === 4 ? true : false}
    >
    <div className="col-xs-12">
      <div className="mbs"></div>
      <div className="txt-blue txt-bold">1. Q: Làm thế nào để tôi có thể đánh giá nhanh được ai đó có hợp với tôi hay không?</div>
      <div className="mbm">
        <b>A:</b> Ngoài một cơ duyên, chúng tôi tin rằng những điểm tương đồng trong tính cách, đời sống, hay những điều kiện cuộc sống của hai bạn cũng chính là nền tảng vững chắc của bất cứ một mối quan hệ lâu dài và thành công.<br />
        Vì thế, để giúp người dùng có thể dễ dàng nhận thấy được một đối phương bất kỳ có tương đồng hay phù hợp với mình hay không,
        chúng tôi có một thuật toán để tính ra "Tỷ lệ tương đồng" giữa bạn và người dùng khác, kết quả thể hiện ngay trên góc dưới bên phải ảnh đại diện của tất cả người dùng.<br />
        "Tỷ lệ tương đồng" này cũng sẽ nhanh chóng được cập nhật khi có bất cứ sự thay đổi nào trong thông tin cá nhân của bạn hay của đối phương.
        Nếu bạn vẫn đang lo ngại những đối phương quanh mình có đủ phù hợp với chính bản thân mình không, thì "Tỷ lệ tương đồng" chắc chắn là một nguồn tham khảo đáng tin cậy cho sự quyết định lựa chọn của bạn.
      </div>

      <div className="txt-blue txt-bold">2. Q: Làm sao để tôi thêm một ai đó vào danh sách quan tâm của tôi</div>
      <div className="mbm">
        <b>A:</b> Khi bạn cảm thấy thực sự quan tâm và rất yêu thích một người dùng nào đó mà chưa dám trò chuyện, bạn có thể thêm người đó vào "Danh sách quan tâm". Để thêm họ vào "Danh sách quan tâm", bạn làm theo các bước sau đây:
        <ol>
         <li>Đi đến hồ sơ của người dùng đó trên Ymeet.me</li>
         <li> Ấn chọn nút Quan tâm</li>
       </ol>
     </div>

     <div className="txt-blue txt-bold">3. Q: Làm sao để tôi xóa một ai đó khỏi danh sách quan tâm của tôi</div>
     <div className="mbm">
      <b>A:</b> Khi bạn cảm thấy không còn quan tâm một người dùng nào đó nữa, bạn có thể xóa người đó khỏi "Danh sách quan tâm". Để xóa, bạn làm theo các bước sau đây:
      <ol>
       <li>Đi đến hồ sơ của người dùng đó trên Ymeet.me</li>
       <li> Ấn chọn nút Bỏ quan tâm. </li>
     </ol>
    </div>

    <div className="txt-blue txt-bold">4. Q: Làm sao để tôi chặn một người dùng</div>
    <div className="mbm">
      <b>A:</b> Bạn có thể chặn một người trên Ymeet.me bằng cách sau:
      <ol>
       <li>Đi đến hồ sơ của người dùng đó trên Ymeet.me</li>
       <li>Ấn chọn nút Chặn</li>
       <li>Chọn hình thức “Chặn thông báo” hoặc “Chặn hai chiều”.</li>
       <li>Ấn vào "Cập nhật" để hoàn thành.</li>
     </ol>
    </div>

    <div className="txt-blue txt-bold">5. Q: Làm sao để tôi báo cáo một người dùng có hại?</div>
    <div className="mbm">
      <b>A:</b> Khi bạn cảm thấy bất cứ ai đó không an toàn hay có hành vi sai trái trên Ymeet.me, bạn có thể ngay lập tức báo cáo người dùng đó bằng cách:
      <ol>
       <li>Đi đến hồ sơ của người dùng đó trên Ymeet.me.</li>
       <li> Ấn chọn nút "Báo cáo vi phạm"</li>
       <li> Chọn “Lý do báo cáo”.</li>
       <li>Điền “Lý do chi tiết”</li>
       <li>Ấn “Gửi báo cáo" để hoàn thành.</li>
     </ol>
    </div>

    <div className="txt-blue txt-bold">6. Q: Tôi có thể bỏ chặn ai đó sau khi chặn họ không?</div>
    <div className="mbm">
      <b>A:</b> Có. Để bỏ chặn, bạn làm theo các bước sau:
      <ol>
       <li>Đi đến hồ sơ của người dùng đó trên Ymeet.me</li>
       <li>2. Ấn chọn nút “Bỏ chặn”</li>
     </ol>
    </div>
    <div className="txt-blue txt-bold">7. Q: “Ai đặc biệt hôm nay” là gì?</div>
    <div className="mbm">
      <b>A:</b> Hàng ngày, trong lần đăng nhập đầu tiên vào hệ thống Ymeet.me của bạn, bạn sẽ nhận được một bảng đề xuất gồm 4 người khác nhau. Đây là một tính năng đặc biệt ưu tiên mà Ymeet.me dành cho các người dùng, vì thế, hãy tận dụng cơ hội này để tăng thêm khả năng tìm kiếm 1 nửa của mình nhé.
    </div>
    <div className="txt-blue txt-bold">8. Q: Tôi muốn xem nhiều ảnh của đối phương?</div>
    <div className="mbm">
      <b>A:</b> Để có thể xem hết tất cả ảnh của tất cả các người dùng khác, bạn cần đăng lên hồ sơ cá nhân của mình trên Ymeet.me ít nhất 4 ảnh. Vì ảnh có thể tiết lộ nhiều điều quan trọng về tính cách, cuộc sống của đối phương, chúng tôi mong sẽ có thể giúp tất cả các người dùng có những lựa chọn chính xác và thú vị hơn khi hẹn hò cùng Ymeet.me. Đặc biệt hơn cả, nghiên cứu của chúng tôi cũng chỉ ra rằng, khi có hơn 4 ảnh trong hồ sơ của mình, bạn cũng tự tăng tỷ lệ nhận được lượt thích của mình lên đến 90%. Hãy nhanh tay <Link to="/myprofile" >cập nhật ảnh</Link> và tham khảo các mẹo để chọn ảnh thật đẹp ở <Link to="/helps/6/3" onClick={() => {this.handleClick()}}>đây</Link> nhé!
    </div>
    </div>
    </DropdownPanel>
    <DropdownPanel
    panelTitle="Trò chuyện"
    isOpen={tab_number === 5 ? true : false}
    >
    <div className="col-xs-12">
      <div className="mbs"></div>
      <div className="txt-blue txt-bold">1. Q: Làm sao để tôi có thể nhắn tin cho một người?</div>
      <div className="mbm">
        <b>A:</b> Nhắn tin là cách duy nhất mà người dùng có thể liên hệ, trò chuyện với nhau trên Ymeet.me.
        Để tạo ra một môi trường hẹn hò trực tuyến an toàn và nghiêm túc cho tất cả mọi người, chúng tôi chỉ cho phép hai người dùng có thể trò chuyện sau khi cả hai người đã ghép đôi thành công.
      </div>
    </div>
    </DropdownPanel>
    <DropdownPanel
    panelTitle="Hẹn hò trực tuyến an toàn, hiệu quả"
    isOpen={tab_number === 6 || +this.state.activeDropdowm === 6 ? true : false}
    activeDropdowm = {this.state.activeDropdowm}
    >
    <div className="col-xs-12">
      <div className="mbs"></div>

      <div className="txt-blue txt-bold">1. Q: Tôi nên làm thế nào để nổi bật hơn so với người khác?</div>
      <div className="mbm">
      <b>A:</b> Cập nhật đầy đủ thông tin trên hồ sơ của bạn. Đây không chỉ là cách để 90% người dùng tìm được đối tượng mong muốn mà còn chính là cách để bạn trở nên đáng tin và thu hút hơn.
      <br />
    <Link to ="/myprofile" onClick={() => { this.sendGaEvent(1) }}><u> Cập nhật hồ sơ ngay! </u></Link>
    </div>

    <div className="txt-blue txt-bold" id="62">2. Q: Tôi nên đăng tải ảnh đại diện như thế nào để thu hút hơn?</div>
    <div className="mbm">
      <b>A:</b> Trước hết, hãy đảm bảo ảnh của bạn không vi phạm <Link to="/terms-of-use">Điều khoản sử dụng</Link> của Ymeet.me đã nhé. <br />
      Sau đó, tham khảo một vài gợi ý dưới đây để chọn được bức ảnh đại diện đẹp nhất và đăng tải lên nào.
      <ol>
       <li>Ánh sáng tốt.</li>
       <li>Cười lên nào.</li>
       <li>Ảnh rõ ràng, không bị bóng đen trên mặt.</li>
       <li>Thoải mái và tạo cảm giác tích cực nhé.</li>
       <li>Nên là một khoảnh khắc vui vẻ, hạnh phúc. Sinh nhật của bạn chẳng hạn.</li>
       <li>Nên chụp toàn thân.</li>
       <li>Tông màu đỏ giúp bạn trông thu hút hơn, tông màu xanh giúp bạn trông tin cậy hơn.</li>
       <li>Không nên đeo kính đen.</li>
       <li>Không nên nghiêm nghị, tức giận hoặc buồn bã.</li>
       <li>Không nên là một bức ảnh với ánh sáng yếu hoặc chụp ngược sáng.</li>
       <li>Không nên là một bức ảnh chụp với người yêu cũ.</li>
       <li>Không nên là một bức ảnh cũ.</li>
       <li>Không nên là một bức ảnh chụp với nhiều người.</li>
      </ol>
      Hãy vận dụng những mẹo nhỏ này, chắc chắn bạn sẽ thu hút hơn rất nhiều đấy! <br />
      <Link to="/myprofile" onClick={() => { this.sendGaEvent(0) }}><u> Cập nhật ảnh đại diện ngay!</u></Link>
    </div>

    <div className="txt-blue txt-bold" id="63">3. Q: Tôi nên đăng tải những bức ảnh phụ như thế nào để hấp dẫn hơn?</div>
    <div className="mbm">
      <b>A:</b> Khả năng nhận được sự quan tâm từ người khác tăng lên tới 90% khi bạn đăng tải ít nhất 4 ảnh trong hồ sơ.
      Ngoài ra, hãy tham khảo một vài gợi ý dưới đây để chọn được những bức ảnh đẹp nhất và đăng tải lên nhé:
      <ol>
       <li>Chọn ảnh với người trong ảnh là chính bạn.</li>
       <li>Chọn những bức ảnh rõ nét, nhưng không chỉnh sửa thái quá. Tất nhiên, đặt vào phương diện của chính bạn, bạn cũng sẽ không muốn gặp một người trông khác hoàn toàn với ảnh phải không nào?</li>
       <li>Chọn những bức ảnh với nội dung về cuộc sống hàng ngày của bạn. Thông qua ảnh, đối phương cũng hiểu hơn về bạn rồi đấy.</li>
      </ol>
      Hãy vận dụng những mẹo nhỏ này, chắc chắn bạn sẽ thu hút hơn rất nhiều đấy! <br />
      <Link to="/myprofile" onClick={() => { this.sendGaEvent(7) }}><u> Cập nhật ảnh phụ ngay!</u></Link>
    </div>

      <div className="txt-blue txt-bold" id="64">4. Q: Làm thế nào để viết giới thiệu bản thân cuốn hút hơn?</div>
      <div className="mbm">
        <b>A:</b> Giới thiệu bản thân trong vòng 5 câu là khả năng thu hút những đối tượng khác của bạn đã tăng lên 70%. Thế nhưng làm sao để có một lời giới thiệu ngắn gọn nhưng lại đủ cuốn hút để nhiều người ấn tượng với bạn hơn?
        Dưới đây sẽ là một số mẹo nho nhỏ giúp bạn nh<b>A:</b>
        <ol>
         <li>Chân thành và thật lòng.<br />
          Điều quan trọng nhất trong một bản miêu tả bản thân là bạn cần phải trung thực với mọi người và chính bản thân bạn.
          Bởi bản thân bạn đã là một điều hay ho, khác biệt với bất cứ ai trên thế giới này rồi, thì điều bạn cần làm chỉ là trung thực với chính bản thân mình và biết cách thể hiện điều đó ra mà thôi.
        </li>
        <li>Nói về những thứ bạn thích<br />
        Hãy miêu tả đôi chút về sở thích, những thứ bạn đam mê hay những việc bạn thích được làm cùng người yêu hàng ngày chẳng hạn.
        </li>
        <li>Nói một chút về tham vọng của bạn<br />
        Những ước mơ, tham vọng về cuộc sống cá nhân hay gia đình trong tương lai luôn là chủ đề được các cặp đôi rất quan tâm về nhau. Khá dễ để bạn chia sẻ đúng không nào?
        </li>
        <li>Hài hước hay những điều làm bạn khác biệt<br />
         Có lẽ chẳng có ai là không thích một người hài hước cả. Nếu bạn không hài hước cho lắm, hãy cho đối phương biết điểm khác biệt hay điều gì đó đặc trưng nhất của riêng bạn.
         Đó sẽ luôn là điều khiến đối phương ấn tượng và nhớ đến bạn mà không hòa lẫn với bất cứ ai.
        </li>
      </ol>
      <Link to="/myprofile/update-self-intro" onClick={() => { this.sendGaEvent(2) }}><u> Cập nhật giới thiệu bản thân ngay!</u></Link>
    </div>

    <div className="txt-blue txt-bold">5. Q: Cách để khám phá những ai “Gần tôi”</div>
    <div>- Trường hợp bạn vẫn chưa bao giờ khám phá chuyên mục “Gần tôi”: Vào “Một nửa”, chọn “Gần tôi”, khi đó, hãy chọn “Cho phép” khi hệ thống sẽ hỏi quyền biết vị trí thiết bị của bạn:</div>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_1.png`} alt="location" ></img></div>
    <div>- Trường hợp bạn đã “Chặn” không cho hệ thống biết vị trí thiết bị của bạn:  Vào “Một nửa”, chọn “Gần tôi”, làm theo hướng dẫn của hệ thống hoặc Xem hướng dẫn chi tiết.
    </div>

    <div className="txt-blue txt-bold">6. Q: Xác thực tài khoản trên Ymeet.me:</div>
    <div className="txt-blue txt-bold">6.1 Xác thực tài khoản để làm gì?</div>
    <div>Xác thực tài khoản là cách duy nhất để Ymet.me giúp bảo vệ bạn và tất cả những người dùng khác, từ đó, xây dựng một môi trường hẹn hò trực tuyến an toàn và hiệu quả. Đặc biệt, nếu bạn là người có khá nhiều đối tượng để cùng trò chuyện thì đây cũng là một cách vô cùng TIẾT KIỆM và TỐI ƯU bởi không ít những quyền lợi đặc biệt được bao gồm như:
    </div>
    {/*<div>- Trò chuyện miễn phí với tất cả những người ghép đôi trong vòng 1 tháng liền</div>*/}
    <div>- Xuất hiện ở vị trí ưu tiên trong “Một nửa”</div>
    <div>- Được đính huy hiệu xác thực lên tài khoản để người dùng khác nhận ra</div>
    <div>- Đảm bảo hẹn hò an toàn, nghiêm túc</div>

    <div className="txt-blue txt-bold">6.2 Xác thực tài khoản bằng cách nào?</div>
    <p>Có 2 cách để bạn xác thực tài khoản của mình:</p>
    <div className="txt-bold">Cách 1: Xác thực miễn phí bằng các giấy tờ tùy thân như: chứng minh thư hoặc bằng lái xe</div>
    <p>Bạn chắc chắn yên tâm rằng: tất cả các thông tin , hình ảnh bạn cung cấp đều được giữ bí mật tuyệt đối!</p>
    <div>- Xác thực bằng chứng minh thư</div>
    <div>- Xác thực bằng bằng lái xe</div>
    <p>Bạn chắc chắn yên tâm rằng: tất cả các thông tin , hình ảnh bạn cung cấp đều được giữ bí mật tuyệt đối!</p>
    <p>Các bước xác thực tài khoản</p>
    <div>Bước 1: Click chọn Hồ sơ</div>
    <div>Bước 2: Chọn “Xác thực tài khoản”</div>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_0.png`} alt="location"></img></div>
    <p>Bước 3: Đọc đầy đủ các hướng dẫn, chọn “Xác thực bằng cách 1”, sau đó chọn “Tải ảnh lên”:</p>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_7.png`} alt="location"></img></div>
    <div>Bước 4: Chọn “Tải ảnh chứng minh thư” hoặc “tải ảnh bằng lái xe”</div>
    <p>Sau khi tải thành công, bạn cần chờ đợi để được hệ thống xác thực tài khoản của mình. Khi hoàn tất quá trình xác thực, bạn nhớ kiểm tra email hoặc kiểm tra tin nhắn hệ thống trong phần “Trò chuyện” trên Ymeet.me của mình để nhận được kết quả xác thực tài khoản.</p>

    <p className="txt-bold">Cách 2: Xác thực bằng ảnh:</p>
    <div>Đây cũng là một giải pháp tuyệt vời nếu như bạn vẫn muốn được hưởng những quyền lợi đặc biệt của người dùng đã được xác thực tài khoản mà không muốn tiết lộ những thông tin liên quan đến bản thân bạn. Tất nhiên, bạn phải bỏ ra 1 khoản tương ứng 450 xu để thực hiện.</div>
    <p>Cách xác thực bằng ảnh:</p>
    <div>Bước 1: Click chọn Hồ sơ</div>
    <div>Bước 2: Chọn “Xác thực tài khoản”</div>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_0.png`} alt="location"></img></div>
    <p>Bước 3: Đọc đầy đủ các hướng dẫn, chọn “Xác thực bằng cách 2”, sau đó chọn “Tải ảnh lên”:</p>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_9.jpg`} alt="location"></img></div>
    <div>Bước 4: Làm theo các bước hướng dẫn.</div>
    <div className="txt-blue txt-bold">6.3 Có phải ai đăng ký cũng được xác thực thành công không?</div>
    <div>A: Không.</div>
    Để được xác thực thành công, bạn cần:
    <p>a. Đối với cách 1: Xác thực bằng giấy tờ:</p>
    <div>- Bạn chỉ cần chọn tải ảnh của 1 trong 2 loại giấy tờ trên.</div>
    <div>- Bạn phải tải đầy đủ 2 ảnh gồm 2 mặt của giấy tờ.</div>
    <div>- Các thông tin trong giấy tờ xác thực phải đúng với các thông tin trên Hồ sơ cá nhân của bạn để được xác thực tài khoản thành công.</div>
    <p>b. Đối với cách 2: Xác thực bằng ảnh:</p>
    <div>- Bạn cần trả 450 xu</div>
    <div>- Tải ảnh lên có tạo dáng giống như ảnh mẫu được cung cấp</div>
    <div>- Ảnh được gửi phải là ảnh của bạn và giống với ảnh trên hồ sơ của bạn.</div>


    <div className="txt-blue txt-bold" id="67">7. Thông báo từ Ymeet.me</div>
    <div className="txt-blue txt-bold">7.1 Vì sao phải nhận thông báo?</div>
    <p>Nhận thông báo của hệ thống là chìa khóa giúp bạn hẹn hò trên Ymeet.me hiệu quả. Các thông báo chỉ bao gồm những thông tin quan trọng liên quan đến các hoạt động của bạn và liên quan đến bạn trên Ymeet.me như:</p>
    <div>- Có đối phương “Thích” bạn</div>
    <div>- Có người đã đồng ý “Ghép đôi” với bạn</div>
    <div>- Có người gửi tin nhắn cho bạn</div>
    <div>- Có người đang ghé thăm, để mắt tới bạn</div>
    <div>- Đối tượng tiềm năng mới ra nhập Ymeet.me của ngày</div>
    <div className="txt-blue txt-bold">7.2 Làm sao để nhận được thông báo?</div>
    <p>Khi bạn chưa từng được hệ thống hỏi quyền gửi thông báo: Ấn chọn “Cho phép”</p>
    <div  className="txt-center mbm"><img src={`${CDN_URL}/general/Help/help_2.png`} alt="7.2" ></img></div>
    {/*<p>Khi bạn đã từng “Chặn” thông báo từ Ymeet.me:</p>*/}

    <div className="txt-blue txt-bold">8. Tính năng “Nổi bật ngay”</div>
    <div className="txt-blue txt-bold">8.1. Tính năng “Nổi bật ngay” là gì?</div>
    <p>“Nổi bật ngay” là cách tuyệt vời để bạn vượt lên trên tất cả các “tình địch” tiềm năng khác trong Ymeet.me. Bạn sẽ xuất hiện ở vị trí trên cùng với hồ sơ nổi bật trong tất cả các trang “Một nửa” của những người dùng khác giới. Khi đó, cơ hội để bạn và người ấy tương lai sẽ tăng lên gấp bội phần</p>
    <div className="txt-blue txt-bold">8.2 Làm sao để tôi có thể dùng “Nổi bật ngay”</div>
    <p>Tài khoản của bạn cần phải có ít nhất 30 xu để có thể được nổi bật ngay. Khi đã đủ điều kiện, bạn làm theo các bước sau:
    </p>
    <div>Bước 1: Click chọn “Hồ sơ” và chọn nút “Nổi bật ngay”:</div>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_4.png`} alt="8.2" ></img></div>
    <div>Bước 2: Làm theo hướng dẫn của hệ  thống.</div>
    <div>Bước 3: Hoàn tất: Sau khi hoàn tất, bạn sẽ nhận được thông báo cũng như thời gian còn lại cho 24h nổi bật của mình:</div>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_8.png`} alt="8.3" ></img></div>

    <div className="txt-blue txt-bold">9. “Ghép đôi ngẫu nhiên”</div>
    <div className="txt-blue txt-bold">9.1 “Ghép đôi ngẫu nhiên” là gì?</div>
    <p>Đôi khi chuyện tình yêu chẳng đơn giản như những gì bạn mong đợi và suy tính, vì thế, một cơ duyên bất ngờ mà ông tơ bà nguyệt Ymeet.me se nên, biết đâu, lại là mối tình bấy lâu bạn tìm kiếm? Tính năng “Ghép đôi ngẫu nhiên” cho phép bạn có cơ hội nói chuyện tâm sự miễn phí trong vòng 24 với 1 người bất kỳ bạn chọn được trong hộp may mắn. Hai bạn sẽ tự động được ghép đôi với nhau trong vòng 24h đó, sau 24h các bạn có thể chủ động “Thích’ lại nhau và nói chuyện như bình thường.
    </p>
    <div className="txt-blue txt-bold">9.2 Cách để sử dụng “Ghép đôi ngẫu nhiên”</div>
    <p>Tài khoản của bạn cần phải có ít nhất 25 xu để có thể có Ghép đôi ngẫu nhiên Khi đã đủ điều kiện, bạn làm theo các bước sau:</p>
    <div>Bước 1: Click vào banner “Chọn ghép đôi ngẫu nhiên” trong trang Trò chuyện:</div>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_5.png`} alt="9.2" ></img></div>
    <div>Bước 2: Làm theo hướng dẫn của hệ thống</div>
    <div>Bước 3: Trong thời gian 24h kết đôi, bạn có thể vào mục “Trò chuyện” để biết ai đang Ghép đôi ngẫu nhiên với mình.</div>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_3.png`} alt="9.2" ></img></div>

    <div className="txt-blue txt-bold">10. Nâng cấp tài khoản</div>
    <div className="txt-blue txt-bold">10.1.Tại sao tôi phải nâng cấp tài khoản?</div>
    <p>Nâng cấp tài khoản là cách duy nhất để bạn sử dụng những tính năng mạnh và quan trọng như Trò chuyện, Nổi bật ngay hay Được gợi nhắc và một số tính năng đặc biệt chỉ có trong gói Nâng cấp tài khoản của mình. Không chỉ vậy, đây còn là cách hữu hiệu để bạn có thể tiết kiệm hầu bao và thời gian khi phải đi mua xu nhiều lần nữa đấy! Quan trọng hơn hết, khi nâng cấp tài khoản, bạn chắc chắn sẽ được đảm bảo hẹn hò an toàn, bí mật và hiệu quả hơn với Ymeet.me, từ đó, bạn chẳng cần phải ưu tư về chuyện hẹn hò của mình nữa rồi.</p>
    <div className="txt-blue txt-bold">10.2 Ymeet.me cung cấp những hình thức thanh toán nào</div>
    <p>Ymeet.me chấp nhận các hình thức thanh toán: Thẻ điện thoại, thẻ tín dụng và thẻ ngân hàng. Tất cả đều được thực hiện qua cổng thanh toán VTCpay để đảm bảo bí mật thông tin của khách hàng. Mức phí trả cho cổng thanh toán do Ymeet.me chịu.</p>
    <div><b>10.2.1 Hướng dẫn thanh toán bằng thẻ điện thoại: </b></div>
    <div>- Bạn có thể sử dụng thẻ của 1 trong 3 nhà mạng: Viettel, Mobifone, Vinaphone.</div>
    <div>-  Bạn có thể nạp nhiều hơn 1 thẻ để đảm bảo đủ số tiền tương ứng với gói cước sắp mua</div>
    <div>- Bạn cần lựa chọn số thẻ và tổng mệnh giá các thẻ tương ứng với gói cước sắp mua. </div>
    <div>- Nhập đúng và đủ mã số thẻ và số seri của từng thẻ.</div>
    <div><b>10.2.2 Hướng dẫn thanh toán bằng thẻ ngân hàng</b></div>
    <p>Để có thể thanh toán bằng thẻ ngân hàng:</p>
    <div>- Chọn ngân hàng bạn muốn sử dụng dịch vụ </div>
    <div>- Đăng ký dịch vụ internet-banking hoặc thanh toán trực tuyến với tài khoản ngân hàng của mình trước. </div>
    <div>- Điền thông tin tên, số điện thoại của bạn</div>
    <div>- Click chọn Thanh toán để chuyển sang hệ thống thanh toán của VTCpay.</div>
    <div>- Nhập đầy đủ các thông tin thẻ, gồm: Tên chủ thẻ, số thẻ, ngày phát hành.</div>
    <div>-  Chờ VTC Pay thông báo giao dịch thành công. Click chọn [Đồng ý] để hoàn tất giao dịch</div>
    <div className="txt-red">Lưu ý:</div>
    <div>- Không đóng trình duyệt sau khi thực hiện giao dịch thành công trên website của ngân hàng mà đợi trình duyệt tự động chuyển về Ymeet.me
    </div>
    <div>- Mỗi ngân hàng sẽ có giao diện giao dịch khác nhau.</div>
    <div><b>10.2.3 Hướng dẫn thanh toán bằng thẻ tín dụng (Visa/ Mastercard)</b></div>
    <div>- Chọn loại thẻ thanh toán; Visa hoặc Mastercard</div>
    <div>- Nhập tên và số điện thoại</div>
    <div>- Click chọn Thanh toán để tự động chuyển sang hệ thống thanh toán của VTCPay</div>
    <div>- Nhập đầy đủ các thông tin thẻ được yêu cầu</div>
    <div>- Chờ VTC Pay thông báo giao dịch thành công. Lưu ý không được đóng trình duyệt mà chọn [Đồng ý] để hoàn tất giao dịch.</div>
    <div><span className="txt-underline">Lưu ý:</span>Khi nạp xu theo hình thức này, số xu của bạn thông thường sẽ được cập nhật sau khoảng 20-30 phút</div>
    <div className="txt-blue txt-bold">10.3 Làm thế nào nếu tổng số tiền tôi thanh toán ít hơn giá của gói cước tôi đăng ký.</div>
    <p>Hệ thống sẽ:</p>
    <div>- Không tự động liên hệ để thông báo với bạn</div>
    <div>- Không tự động chuyển chuyển sang một gói cước bất kỳ có mệnh giá nhỏ hơn với gói cước bạn đã đăng ký</div>
    <div>- Tự động cộng dồn vào các lần thanh toán tiếp theo</div>
    <div>- Lưu lại tổng số tiền bạn đã gửi thiếu ban đầu sang tài khoản ghi nợ của Ymeet.me</div>
    <p>Bạn cần:</p>
    <div>- Chuyển tiếp đủ số tiền tương ứng với gói cước muốn mua để mua thành công</div>
    <div>- Liên hệ với Ymeet.me theo số hotline để nhận lại tiền nếu tổng số tiền trong tài khoản ghi nợ trên lớn hơn hoặc bằng 500.000 VNĐ. Tuy nhiên, bạn phải chịu tất cả các chi phí liên quan đến việc hoàn lại.</div>
    <div className="txt-blue txt-bold">10.4 Làm thế nào nếu tôi chuyển thừa tiền so với giá của gói cước tôi đăng ký.</div>
    <p>Hệ thống sẽ:</p>
    <div>-  Không tự động đăng ký cho bạn một gói cước nào tương đương với số tiền thừa</div>
    <div>- Tự động tính vào các lần thanh toán tiếp theo</div>
    <div>- Lưu lại số tổng số tiền thừa sau mỗi lần thanh toán sang tài khoản ghi nợ của Ymeet.me</div>
    <p>Bạn cần:</p>
    <div>- Liên hệ với Ymeet.me theo số hotline để nhận lại tiền nếu tổng số tiền thừa trong tài khoản ghi nợ của Ymeet.me lớn hơn hoặc bằng 500.000 VNĐ . Tuy nhiên, bạn phải chịu tất cả các chi phí liên quan đến việc hoàn lại.</div>
    <div className="txt-blue txt-bold">10.5 Làm sao để tôi nâng cấp tài khoản?</div>
    <p>Bước 1: Click vào “Hồ sơ”, chọn “Nâng cấp tài khoản”</p>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_6.png`} alt="10.4" ></img></div>
    <div>Bước 2: Chọn gói thanh toán phù hợp nhất với bạn. Bạn có thể chọn gói 1 tháng, 3 tháng hoặc 6 tháng liền
    </div>
    <div>Bước 3: Xem tổng giá tiền và chọn phương thức thanh toán. Bạn cũng có thể thanh toán bằng: Thẻ điện thoại, Thẻ tín dụng hoặc Chuyển khoản ngân hàng. </div>
    <div>Bước 4: Tiến hành thanh toán</div>
    <div className="txt-blue txt-bold">11. Xu</div>
    <div className="txt-blue txt-bold">11.1 Xu là gì?</div>
    <p>Xu là phương tiện duy nhất để bạn sử dụng những tính năng vượt trội của Ymeet.me từ đó giúp bạn:</p>
    <div>- Đứng đầu mọi danh sách tìm kiếm với tính năng “Nổi bật ngay”</div>
    <div>- Ghép đôi ngẫu nhiên, bất ngờ và nói chuyện thoải mái với người đó trong ngày ghép đôi với tính năng “ghép đôi ngẫu nhiên”</div>
    <div className="txt-blue txt-bold">11.2 Tại sao tôi có 40 xu?</div>
    <p>Khi trở thành người dùng của Ymeet.me, bạn được tặng miễn phí 40 xu. Với 40 xu miễn phí này, bạn có thể sử dụng để trả cho bất cứ tính năng nào được trả bằng xu của Ymeet.me.</p>
    <div className="txt-blue txt-bold">11.3 Tôi có bắt buộc phải mua xu không?</div>
    <p>Bạn không bắt buộc phải mua xu. Tuy nhiên, để có thể có những giây phút hẹn hò hiệu quả và thú vị, bạn nên sử dụng những tính năng được trả bằng xu. Hơn nữa, xu của Ymeet.me cũng rất rẻ nên đừng nên tiếc chi mấy đồng tiền lẻ mà mua xu và trải nghiệm những tính năng hay ho nhất của Ymeet.me nhé</p>
    <div className="txt-blue txt-bold">11.4 Tôi có thể mua bao nhiêu xu?</div>
    <p>Bạn có thể mua 96, 180 hoặc 450 xu tương ứng với chỉ 20.000 VNĐ, 50.000 VNĐ, hoặc 100.000 VNĐ cho một lần mua</p>
    <div className="txt-blue txt-bold">11.5 Làm thế nào để tôi mua xu</div>
    <p>Để mua xu, bạn vào “Hồ sơ”, chọn “Mua xu” sau đó làm theo hướng dẫn.</p>
    <div className="txt-blue txt-bold">11.6 Làm sao để tôi quản lý số xu của mình</div>
    <p>Để biết bạn còn bao nhiêu xu, bạn có thể vào “<Link to ="/myprofile">Hồ sơ</Link>”, khi đó, bạn sẽ biết mình còn lại bao nhiêu xu để sử dụng</p>
    <div className="txt-blue txt-bold" id="611">11.7 Có cách nào để tôi có xu miễn phí không?</div>
    <p>Có một vài cách rất đơn giản để bạn có xu mà không cần trả tiền: </p>
    <div>- Đã gửi đi trên 20 lượt Thích, bạn được tặng ngay 20 xu</div>
    <div>- Đăng nhập trên hệ thống Ymeet.me trong suốt 3 ngày liên tiếp, bạn được tặng ngay 5 xu</div>
    <div>- Có ảnh được nhiều lượt bình chọn nhất tuần, bạn có ngay 20 xu</div>
    <div>- Vào ngày sinh nhật, bạn được tặng ngay 10 xu.</div>
    <p>Ngoài ra,  khi đã trở thành người dùng của Ymeet.me, bạn cũng đã được tặng ngay 40 xu miễn phí rồi.</p>

    <div className="txt-blue txt-bold">12.  Làm quen</div>
    <div className="txt-blue txt-bold">12.1 Q: “Làm quen” là như thế nào?</div>
    <div><b>A:</b>Nếu bạn đã từng có cảm giác say nắng một ai đó trên Ymeet.me nhưng lại phải đợi khá lâu để chờ người ấy “Thích” lại rồi mới trò chuyện được thì chắc chắn bạn nên thử tính năng mới này. Bằng việc ấn chọn “Làm quen” cho ai đó, bạn sẽ có thể:</div>
    <div>- Ghép đôi với người ấy ngay mà không cần chờ người ấy phản hồi</div>
    <div>- Trò chuyện miễn phí với người ấy ngay lập tức mà cũng không cần mua thêm xu để trò chuyện</div>
    <div className="txt-blue txt-bold">12.2 Q: Làm thế nào để dùng “Làm quen”?</div>
    <div><b>A:</b>Để dùng tính năng siêu thú vị này bạn cần có không ít hơn 200 xu. Sau đó, làm theo các bước sau đây:</div>
    <div>Bước 1: Vào xem thông tin hồ sơ cá nhân của người mà bạn ấn tượng để “Làm quen”</div>
    <div>Bước 2: Bấm chọn “Làm quen” ở phần cuối thông tin cá nhân của người ấy:</div>
    <div className="txt-center"><img src={`${CDN_URL}/general/Help/help_10.jpg`} alt="10.4" ></img></div>
    <div>Bước 3: Trình tự làm theo các bước yêu cầu. Và nhớ nói chuyện ngay và luôn với người ấy nhé!</div>

    <div className="txt-blue txt-bold">13. Q: Cách viết tin nhắn trò chuyện đầu tiên thật ấn tượng là gì?</div>
    <div className="mbm">
      <b>A:</b> Mở lời trước luôn thật khó phải không? Bạn phân vân không biết làm sao để có được một lời chào hỏi đầu tiên thật khác biệt và ấn tượng, để đối phương có thể trả lời bạn trong tích tắc. Hãy thử vận dụng những gợi ý sau đây:
      <ol>
       <li>Không viết hoa tất cả.</li>
       <li>Không viết tắt quá nhiều.</li>
       <li>Viết những câu đầy đủ, ngắn gọn, nhưng không cộc lốc.</li>
       <li>Hãy chân thật, vừa phải, không phô trương, không nói quá.</li>
       <li>Hãy khen ngợi về vẻ đẹp, tính cách, sở thích,...của đối phương thông qua những thông tin chia sẻ. </li>
       <li>Hãy thể hiện rằng bạn bị ấn tượng bởi niềm đam mê hay sở thích chung của bạn và đối phương. Ai mà chẳng thích nói chuyện về những điều mình đam mê phải không nào.</li>
     </ol>
    </div>

    <div className="txt-blue txt-bold">14. Q: Làm thế nào để an toàn thông tin?</div>
    <div className="mbm">
      <b>A:</b> Hãy cẩn thận khi chia sẻ với bất cứ ai đó về các thông tin cá nhân chi tiết của bạn.
      Những thông tin như: Họ tên đầy đủ, số điện thoại, địa chỉ email, địa chỉ nhà riêng, tài khoản Facebook hay các tài khoản mạng xã hội khác của bạn nên được giữ kín.
      Chỉ chia sẻ khi bạn đã trò chuyện và cảm thấy thực sự có thể tin tưởng được người kia.
      Nếu bạn cần bất cứ sự giúp đỡ gì từ hệ thống chúng tôi trong vấn đề này, hãy liên hệ với chúng tôi để được thêm nhiều tư vấn và hướng dẫn cụ thể hơn nhé.
    </div>

    <div className="txt-blue txt-bold">15. Q: Làm thế nào để tránh bị lừa tiền?</div>
    <div className="mbm">
      <b>A:</b> Để tự bảo vệ tài chính của bạn, hãy từ chối bất cứ lời mời hay yêu cầu nào liên quan đến việc gửi tiền hay các loại thông tin liên quan đến tài khoản tài chính của bạn.
      Ngoài ra, hãy nhanh chóng báo cáo người dùng này cho Ymeet.me để chúng tôi có thể hỗ trợ, bảo vệ bạn một cách nhanh chóng và hiệu quả nhất.
    </div>

    <div className="txt-blue txt-bold">16. Q: Làm thế nào để nhận dạng những nguời dùng không tốt?</div>
    <div className="mbm">
      <b>A:</b> Hãy từ chối nói chuyện, tương tác với những kẻ đáng nghi với một vài dấu hiệu, hành động sau đây nhé:
      <ol>
       <li>Yêu cầu bạn gửi tiền hay tài sản để từ thiện</li>
       <li>Thành viên gửi tin nhắn quấy rối hoặc xúc phạm</li>
       <li>Những thành viên có hành vi cư xử không đúng mực sau khi gặp</li>
       <li>Những hồ sơ gian lận</li>
       <li>Chào mời hoặc quấy rối để chào bán sản phẩm hay dịch vụ</li>
       <li>Ngay lập tức hỏi các thông tin cá nhân liên quan đến: Họ tên đầy đủ, số điện thoại, địa chỉ email, địa chỉ nhà riêng, tài khoản facebook hay các tài khoản mạng xã hội khác của bạn</li>
       <li>Gửi những đoạn tin nhắn với nhiều ký tự đặc biệt hay nhiều lỗi chính tả, nhiều đường dẫn thông tin lạ, ...</li>
       <li>Yêu cầu địa chỉ của bạn dưới lời mời tặng quà bạn</li>
       <li>Ngay lập tức yêu cầu gặp mặt bạn</li>
     </ol>
     ...
     Trong trường hợp gặp phải những người này, hãy báo cáo người dùng cho Ymeet.me để chúng tôi kịp thời ngăn chặn nhé.
    </div>

    <div className="txt-blue txt-bold">17. Q: Làm thế nào để thêm an tâm trong lần hẹn gặp mặt đầu tiên?</div>
    <div className="mbm">
      <b>A:</b> Để tránh những trường hợp xấu nhất có thể xảy ra trong lần gặp mặt đầu tiên, bạn nên thử vận dụng những bước sau đây:
      <ol>
       <li>Thử gõ tên của người đó vào các công cụ tìm kiếm để có thêm những thông tin hữu ích.</li>
       <li>Nên hẹn gặp mặt ở nơi công cộng, đông người.</li>
       <li>Kể cho bạn bè, người thân nghe. Điều tốt nhất là bạn nên kể cho ít nhất một người thân của bạn biết về thời gian, địa điểm mà hai người gặp nhau.</li>
       <li>Tự làm chủ bản thân, tránh những hành vi lạ, đáng nghi.</li>
       <li>Tránh những cuộc gặp mặt tại nơi khiến bạn phải đi xa.</li>
     </ol>
    </div>

    <div className="txt-blue txt-bold">18. Q: Tôi phải làm thế nào khi không nhận được bất cứ tin nhắn trả lời nào?</div>
    <div className="mbm">
      <b>A:</b> Hãy bình tĩnh, điều đó hoàn toàn bình thường. Sẽ đến lúc có một người thích hợp và sẵn sàng trả lời những tin nhắn của bạn.
      Đừng dùng những lời lẽ khiếm nhã, bạn có thể bị họ báo cáo và bị khóa tài khoản.
    </div>

    <div className="txt-blue txt-bold">19. Q: Liệu Ymeet.me có thật sự hiệu quả? Có thể giúp tôi tìm được người yêu không?</div>
    <div className="mbm">
      <b>A:</b><span className="txt-blue padding-rl5">Ymeet.me</span> ĐẢM BẢO 100% bạn sẽ tìm được Người ấy <span className="txt-blue padding-rl5">CHỈ TRONG 90 NGÀY</span> nếu bạn làm theo những bí kíp sau:
    </div>
    <p className="text-indent"></p>
    <ol className="padding-l30">
      <li>Sử dụng gói VIP 3 tháng của <span className="txt-blue padding-rl5">Ymeet.me</span></li>
      <li>Đăng nhập <span className="txt-blue padding-rl5">Ymeet.me</span> liên tục mỗi ngày</li>
      <li>Gửi đi ít nhất 30 lượt thích liên tục mỗi ngày</li>
      <li>Thích lại tất cả những người đã thích bạn</li>
      <li> Chủ động nhắn tin với những người mình đã ghép đôi</li>
      <li> Nhanh chóng trả lời tất cả các cuộc trò chuyện</li>
      <li> Hẹn người ấy đi chơi sau 3 ngày chuyện trò</li>
    </ol>
    </div>
    </DropdownPanel>
    <DropdownPanel
    panelTitle="Câu hỏi thường gặp"
    isOpen={tab_number === 7 ? true : false}
    >
      <div className="col-xs-12">
        <div className="mbs"></div>
        <div className="txt-blue txt-bold" id="71">1. Q: Tại sao nút "Đăng nhập bằng G+/Facebook" của tôi không hoạt động?</div>
        <div className="mbm">
          <b>A:</b> Bạn đã thiết lập chế độ "Chặn dữ liệu trang web và cookie của bên thứ ba" trên trình duyệt của bạn đang dùng.
          Để có thể đăng nhập bằng G+ hoặc Facebook, hãy thực hiện theo các bước sau!
        </div>
        <Enable3rdPartyCookies />
      </div>
    </DropdownPanel>
    </div>
    )
  }
}

Helps.propTypes = {
  location: PropTypes.object,
  Tips: PropTypes.object,
  gaSend: PropTypes.func,
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    gaSend: (eventAction, ga_track_data) => {
      dispatch(gaSend(eventAction, ga_track_data))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(addHeader(Helps));
