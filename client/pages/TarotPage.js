import React, { PropTypes } from 'react'
import TarotContainer from '../containers/TarotContainer'
import HeaderContainer from '../containers/HeaderContainer'

const TarotPage = props => (
  <div>
    <HeaderContainer {...props} />
    <TarotContainer {...props} />
  </div>
)

export default TarotPage
