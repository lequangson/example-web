import {
 SEND_VERIFICATION_CODE_REQUEST,
 SEND_VERIFICATION_CODE_SUCCESS,
 SEND_VERIFICATION_CODE_FAILURE,
 VERIFY_CODE_REQUEST,
 VERIFY_CODE_SUCCESS,
 VERIFY_CODE_FAILURE
} from './Constant';
import { CALL_API } from 'middleware/api';
import {
  API_GET,
  API_POST
} from 'constants/Enviroment';

export function sendVerificationCode(mobile_number) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'send_verification_code',
      params: {
        mobile_number,
        function_code: 1
      },
      authenticated: true,
      types: [
        SEND_VERIFICATION_CODE_REQUEST,
        SEND_VERIFICATION_CODE_SUCCESS,
        SEND_VERIFICATION_CODE_FAILURE,
      ],
    },
  }
}

export function verifyCode(sms_message_id, code) {
  return {
    [CALL_API]: {
      method: API_POST,
      endpoint: 'verify_code',
      params: {
        code,
        sms_message_id,
        function_code: 1
      },
      authenticated: true,
      types: [
        VERIFY_CODE_REQUEST,
        VERIFY_CODE_SUCCESS,
        VERIFY_CODE_FAILURE
      ]
    }
  }
}