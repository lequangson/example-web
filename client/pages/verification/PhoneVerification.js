import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import HeaderContainer from 'containers/HeaderContainer';
import * as ymmStorage from 'utils/ymmStorage';
import {
  sendVerificationCode,
  verifyCode
} from './Action';
import {
  REGEX_PHONE,
} from 'constants/Enviroment';
import notification from 'utils/errors/notification'
import { NOTIFICATION_SUCCESS } from 'constants/Enviroment';
import {
  NOT_VERIFIED,
  PENDING_FOR_APPROVE,
  VERIFIED
} from './Constant';

class PhoneVerification extends Component {
  constructor() {
    super()

    this.state = {
      isPhoneValid: false,
      sendPhoneSuccess: false,
      phone_number: null,
      sms_message_id: null,
      verification_code: null,
      sending_sms: false,
      verifying_code: false
    }

  }

  componentDidMount() {
    this.validatePhone()
  }

  validatePhone = () => {
    if (!this.userPhone) {
      return;
    }
    if (this.userPhone.value.match(REGEX_PHONE)) {
      this.setState({ isPhoneValid: true, phone_number: this.userPhone.value })
    } else {
      this.setState({ isPhoneValid: false, phone_number: this.userPhone.value })
    }
  }

  onChangeVerificationCode = () => {
      this.setState({
        verification_code: this.verifyCode.value
      })
  }

  isFormValid() {
    return this.state.isPhoneValid
  }

  handleSendSms =  async () => {
    if (this.state.sending_sms) {
      return;
    }
    this.setState({ sending_sms: true });
    try{
      const response = await this.props.sendVerificationCode(this.state.phone_number);
      const { sms_message_id } = response.response.data;
      // const { verification_status } = this.props.user_profile.current_user;
      // verification_status.phone_verify_status = 1;
      this.setState({ sendPhoneSuccess: true, sending_sms: false, sms_message_id })
    }catch(e) {
      this.setState({ isPhoneValid: false, sending_sms: false });
    }

    // console.log('never go here', response);

  }

  handleSendCode = async () => {
    if (this.state.verifying_code) {
      return;
    }
    this.setState({ verifying_code: true });
    try {
      await this.props.verifyCode(this.state.sms_message_id, this.state.verification_code);
      notification(NOTIFICATION_SUCCESS, `Bạn đã xác minh số điện thoại thành công.`);
      this.setState({ verifying_code: false });
      const { verification_status } = this.props.user_profile.current_user;
      verification_status.phone_verify_status = 2;
      browserHistory.push('/verification');
      return;
    } catch(e) {
      this.setState({ verifying_code: false });
      notification(NOTIFICATION_SUCCESS, `Mã xác nhận không chính xác.`);
    }

  }

  renderInputPhone() {

    const { user_profile } = this.props
    const { current_user } = user_profile
    const isFormValid = this.isFormValid();

    return (
      <div>
        <div className="row">
          <div className="col-xs-12">
            <h3 className="txt-heading">Xác thực bằng số điện thoại</h3>
          </div>
          <div className="col-xs-12 mbl">
            Bạn cần cung cấp số điện thoại cá nhân của bạn. Chúng tôi sẽ gửi mã xác nhận đến
            số điện thoại mà bạn cung cấp.
          </div>
          <div className="col-xs-12 mbl">
            <label className="txt-blue" htmlFor="phone">Số điện thoại</label>
            <input
              key={1}
              type="number"
              ref={(ref) => {
                this.userPhone = ref
              }}
              onChange={this.validatePhone}
              id="phone"
              placeholder=""
              // defaultValue={userPhone}
            />
            {this.state.isPhoneValid || !this.state.phone_number || <span className="txt-sm txt-light txt-red">Số điện thoại không hợp lệ</span>}
          </div>
        </div>
        <div className="row">
          {!this.state.sendPhoneSuccess &&
            <div className="col-xs-6 col-xs-offset-3">
              <button
                className="btn btn--p btn--b"
                disabled={!isFormValid || this.state.sending_sms}
                onClick={this.handleSendSms}
              >
                {this.state.sending_sms? 'Đang gửi mã xác nhận cho bạn' : 'Gửi mã xác nhận cho tôi'}
              </button>
            </div>
          }
        </div>
      </div>
    )
  }

  renderInputCode() {
    return (
      <div>
      <div className="row">
        <div className="col-xs-12">
          <h3 className="txt-heading">Xác thực bằng số điện thoại</h3>
        </div>
        <div className="col-xs-12 mbl">
          Mã xác nhận đã được gửi đến số điện thoại mà bạn cung cấp. Vui lòng nhập mã xác nhận để hoàn tất quá trình xác thực
        </div>
        <div className="col-xs-12 mbl">
          <label className="txt-blue" htmlFor="phone">Mã xác nhận</label>
          <input
            type="text"
            key={2}
            ref={(ref) => {
              this.verifyCode = ref
            }}
            onChange={this.onChangeVerificationCode}
            id="verifyCode"
            placeholder=""
          />
        </div>
      </div>
      <div className="row">
        <div className="col-xs-6 col-xs-offset-3">
          <button
            className="btn btn--p btn--b mbl"
            onClick={this.handleSendCode}
          >
            Hoàn tất
          </button>
        </div>
        <div className="col-xs-6 col-xs-offset-3 txt-center">
          {!this.state.sending_sms && <a className="cursor txt-underline" onClick={this.handleSendSms}>
              Không nhận được mã?<br />
              Gửi lại
            </a>
          }
        </div>
      </div>
    </div>
    )
  }

  renderInfo(msg) {
    return <div>
        <HeaderContainer {...this.props} />
        <div>{msg}</div>
    </div>
  }

  render() {
    const { verification_status } = this.props.user_profile.current_user;
    if (!verification_status) {
      return null;
    }
    const { phone_verify_status } = verification_status;

    if (phone_verify_status == VERIFIED) {
      return this.renderInfo('Số điện thoại của bạn đã được xác thực');
    }

    return (
      <div>
        <HeaderContainer {...this.props} />
        <div className="site-content" >
          <div className="container">
            {this.state.sendPhoneSuccess === true
              ? this.renderInputCode()
              : this.renderInputPhone()
            }

          </div>

        </div>
      </div>
    );
  }
}

PhoneVerification.propTypes = {

}

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    verifyCode: async (sms_message_id, code) => await dispatch(verifyCode(sms_message_id, code)),
    sendVerificationCode: async (phone_number) => await dispatch(sendVerificationCode(phone_number))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhoneVerification)

