import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import HeaderContainer from 'containers/HeaderContainer'
import { connect } from 'react-redux'
import {
  CDN_URL,
  GA_ACTION_VERIFY,
  UPLOAD_DEVIDE_MODE,
} from 'constants/Enviroment';
import Modal from 'react-modal';
import _ from 'lodash'
import Dropzone from 'react-dropzone'
import {
  JOB_VERIFICATION_CODE,
  NOT_VERIFIED,
  PENDING_FOR_APPROVE,
  VERIFIED
} from './Constant';

class JobVerification extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.uploadIdCardImage = this.uploadIdCardImage.bind(this);
    this.onOpenClick = this.onOpenClick.bind(this);
  }

  openModal() {
    this.props.gaSend(GA_ACTION_VERIFY, { page_source: 'Verification_Type_1' });
    this.setState(
      {
        modalIsOpen: true,
      }
    )
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  uploadIdCardImage(files) {
    this.props.saveFilesUpload(files, JOB_VERIFICATION_CODE, UPLOAD_DEVIDE_MODE)
  }

  onOpenClick(type) {
    this.props.gaSend(GA_ACTION_VERIFY, { page_source: 'Upload_Job_Verification_Button' });
    this.dropzone.open();
  }


  renderInfo(msg) {
    return <div>
        <HeaderContainer {...this.props} />
        <div>{msg}</div>
    </div>
  }

  render() {
    const { verification_status } = this.props.user_profile.current_user;
    if (!verification_status) {
      return null;
    }
    const { job_verify_status } = verification_status;
    if (job_verify_status == PENDING_FOR_APPROVE) {
      return this.renderInfo('Công việc của bạn chờ xác thực');
    }
    if (job_verify_status == VERIFIED) {
      return this.renderInfo('Công việc của bạn đã được xác thực');
    }

    return (
      <div>
        <HeaderContainer {...this.props} />
        <div className="site-content" >
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <h3 className="txt-heading">Xác thực công việc của bạn</h3>
              </div>
              <div className="col-xs-12 mbm">
                Bạn cần cung cấp 1 ảnh rõ nét
                hợp đồng lao động hoặc bảng lương của bạn (bao gồm tên bạn, tên Công ty và vị trí công việc)
                để kiểm tra độ trùng khớp với thông tin của bạn trên hệ thống
              </div>
            </div>

            <div className="image-helper">
              <div className="txt-center mbm">
                <i className="fa fa-check fa-lg txt-green mrs"></i>
                Gợi ý ảnh hợp lệ
              </div>
              <div className="txt-center l-flex-vertical-center mbl">
                <article className="card shadow">
                  <div className="card__upper">
                    <img className="img-150" src={`${CDN_URL}/general/Verify/contract.png`} />
                  </div>
                </article>
              </div>
            </div>

            <div className="row">
              <div className="col-xs-6">
                <Link to="/verification"><button
                    className="btn btn--b btn--5"
                  >
                    Thoát
                  </button>
                  </Link>
              </div>

              <div className="col-xs-6 mbm">
                <Dropzone
                  onDrop={this.uploadIdCardImage}
                  ref={(drz) => { this.dropzone = drz; }}
                  multiple={false} style={{ display: 'none' }}
                />
                <button
                  className="btn btn--p btn--b mbm"
                  onClick={() => {this.onOpenClick()}}
                >
                  Tải ảnh lên
                </button>
              </div>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

JobVerification.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
}

function mapStateToProps(state) {
  return state
}


export default connect(
  mapStateToProps
)(JobVerification)
