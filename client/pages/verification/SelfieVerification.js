import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import HeaderContainer from 'containers/HeaderContainer';
import { connect } from 'react-redux';
import {
  CDN_URL,
  GA_ACTION_VERIFY,
  UPLOAD_CAPTURE_MODE,
  GA_ACTION_GET_MORE_COIN
} from 'constants/Enviroment';
import Modal from 'react-modal';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import * as ymmStorage from 'utils/ymmStorage';
import TakePhoto from 'components/buttons/take-photo/TakePhoto';
import {
  VERIFY_PICTURE_TYPE,
  VERIFY_PICTURE_SECOND_WAY_TYPE,
  NOT_VERIFIED,
  PENDING_FOR_APPROVE,
  VERIFIED
} from './Constant';

class SelfieVerification extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
    }
  }

  onSendGaTakePhoto = () => {
    this.props.gaSend(GA_ACTION_VERIFY, { page_source: 'Verification_Type_2' }); //verify user in second way
  }

 uploadVerifyPictureSecondWay = (files) => {
    this.props.saveFilesUpload(files, VERIFY_PICTURE_SECOND_WAY_TYPE, UPLOAD_CAPTURE_MODE)
  }

  renderInfo(msg) {
    return <div>
        <HeaderContainer {...this.props} />
        <div>{msg}</div>
    </div>
  }

  render() {
    const { verification_status, gender } = this.props.user_profile.current_user;
    if (!verification_status) {
      return null;
    }
    const { selfie_verify_status } = verification_status;
    if (selfie_verify_status == PENDING_FOR_APPROVE) {
      return this.renderInfo('Ảnh của bạn chờ xác thực');
    }
    if (selfie_verify_status == VERIFIED) {
      return this.renderInfo('Ảnh của bạn đã được xác thực');
    }

    return (
      <div>
        <HeaderContainer {...this.props} />
        <div className="site-content" >
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <h3 className="txt-heading">Tạo dáng theo mẫu</h3>
              </div>
              <div className="col-xs-12 mbl">
                Bạn cần cung cấp 1 ảnh của bạn.<br /> Chú ý tạo dáng tay giống với ảnh mẫu được cung cấp của chúng tôi sau đây:
              </div>
            </div>

            <div className="col-xs-12">
                <div className="txt-center mbl">
                  {gender == 'male' ?
                    <img src={`${CDN_URL}/general/Verify/sample-photo-male1.jpg`} alt="sample-photo-male1.jpg" />
                    :
                    <img src={`${CDN_URL}/general/Verify/sample-photo-female1.jpg`} alt="sample-photo-female1.jpg" />
                  }
                </div>
              </div>

            <div className="row">
              <div className="col-xs-6 col-xs-offset-3">
                  <TakePhoto
                    title='Chụp ảnh'
                    customStyle='btn btn--p btn--b mbm'
                    onTakePhotoSuccess={this.uploadVerifyPictureSecondWay}
                    onSendGaTakePhoto={this.onSendGaTakePhoto}
                    gender={gender}
                  />
                </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SelfieVerification.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
}

function mapStateToProps(state) {
  return state
}


export default connect(
  mapStateToProps
)(SelfieVerification)
