import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router'
import {
  NOT_VERIFIED,
  PENDING_FOR_APPROVE,
  VERIFIED
} from './Constant';

class Verification extends Component {

  constructor(props) {
    super(props)

  }
  render() {
    const { verification_status, gender } = this.props.user_profile.current_user;
    if (!verification_status) {
      return null;
    }
    const {
      id_card_verify_status,
      selfie_verify_status,
      phone_verify_status,
      job_verify_status,
      education_verify_status
    } = verification_status;
    const is_card_verified = id_card_verify_status == VERIFIED || false;
    const is_card_pending_verify = id_card_verify_status == PENDING_FOR_APPROVE || false;
    return (
      <div className="site-content" >
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3 className="txt-heading">Xác thực tài khoản</h3>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <div className="text-indent">
                Xác thực tài khoản ngay hôm nay để trải nghiệm hẹn hò một cách hiệu quả nhất:
              </div>
              {/*gender == 'male' ?<div dangerouslySetInnerHTML={{ __html: '* Được trò chuyện thoải mái trong <span class="txt-blue"> 30 ngày</span><br />' }}></div> : ''*/}
              * Được xuất hiện ở vị trí ưu tiên trong <span className="txt-blue txt-bold"><i className="fa fa-heart"></i> Một nửa.</span><br />
              * Được đính huy hiệu xác thực lên tài khoản.<br />
              * Được đảm bảo hẹn hò an toàn và nghiêm túc.<br /><br />
              <div className="padding-l5">
                <div className="txt-blue txt-bold">Chọn các cách sau để xác thực:</div>
                <p className="txt-red">Bạn sẽ được đính huy hiệu tương ứng với mức độ bạn xác thực. Xác thực càng nhiều bạn càng có uy tín đối với các thành viên khác.</p>
              </div>
            </div>
          </div>
          <div className="row mbm">
            <div className="col-xs-12">
              <ul className="list-border">
                <li className={`${is_card_verified ? 'active ' : ' '}list-border__item bg-li--1 panel__heading`}>
                  <Link to={is_card_verified || id_card_verify_status == PENDING_FOR_APPROVE ? '/verification' : '/verification-id-card'} className="list-border__link">
                    <span className={is_card_verified || id_card_verify_status == PENDING_FOR_APPROVE ? 'txt-dark' : 'txt-blue'}>
                      1. Tải ảnh CMND/ Bằng lái xe
                      {id_card_verify_status == PENDING_FOR_APPROVE && <span className='txt-sm txt-dark'><br />(Đang chờ xác thực)</span>}
                      {id_card_verify_status == VERIFIED && <span className='txt-dark'><br />Đã xác thực</span>}
                      <i className={`${id_card_verify_status == VERIFIED ? 'fa fa-check pull-right txt-green' : 'fa fa-chevron-right pull-right'}`} aria-hidden="true"></i>
                    </span>
                  </Link>
                </li>
                {gender == 'male' && <li className={`${phone_verify_status == VERIFIED ? 'active ' : ' '}list-border__item bg-li--1 panel__heading`}>
                    <Link
                      to={
                        phone_verify_status == NOT_VERIFIED
                          ? '/verification-phone'
                          : '/verification'
                      }
                      className="list-border__link"
                    >
                      <span className={phone_verify_status == NOT_VERIFIED ? 'txt-blue' : 'txt-dark'}>
                        2. Xác thực bằng số điện thoại
                        {/*phone_verify_status == PENDING_FOR_APPROVE && <span className='txt-sm txt-dark'><br />(Đang chờ xác thực)</span>*/}
                        {phone_verify_status == VERIFIED && <span className='txt-dark'><br />Đã xác thực</span>}
                        <i className={`${phone_verify_status == VERIFIED ? 'fa fa-check pull-right txt-green' : 'fa fa-chevron-right pull-right'}`} aria-hidden="true"></i>
                      </span>
                    </Link>
                  </li>
                }
                <li className={`${selfie_verify_status == VERIFIED ? 'active ' : ' '}list-border__item bg-li--1 panel__heading`}>
                  <Link
                    to={
                      selfie_verify_status == NOT_VERIFIED
                        ? '/verification-selfie'
                        : '/verification'
                    }
                    className="list-border__link"
                  >
                    <span className={selfie_verify_status == NOT_VERIFIED? 'txt-blue' : 'txt-dark'}>
                      {gender == 'female' ? "2": "3"}. Tạo dáng theo mẫu
                      {selfie_verify_status == PENDING_FOR_APPROVE && <span className='txt-sm txt-dark'><br />(Đang chờ xác thực)</span>}
                      {selfie_verify_status == VERIFIED && <span className='txt-dark'><br />Đã xác thực</span>}
                      <i className={`${selfie_verify_status == VERIFIED ? 'fa fa-check pull-right txt-green' : 'fa fa-chevron-right pull-right'}`} aria-hidden="true"></i>
                    </span>
                  </Link>
                </li>
                {gender == 'male' && <li className={`${id_card_verify_status == VERIFIED && job_verify_status == VERIFIED ? 'active ' : ' '}
                    ${job_verify_status == NOT_VERIFIED && is_card_verified ? 'bg-li--1 ' : ' '}
                    list-border__item panel__heading`}>
                    <Link
                      to={
                        job_verify_status == NOT_VERIFIED && is_card_verified
                          ? '/verification-job'
                          : '/verification'
                      }
                      className="list-border__link"
                    >
                      <span className={job_verify_status == NOT_VERIFIED && is_card_verified ? 'txt-blue' : 'txt-dark'}>
                        4. Xác thực công việc của bạn
                        {id_card_verify_status != VERIFIED && <span className='txt-sm txt-dark'><br />(Chỉ có thể làm sau khi đã xác thực bằng cách 1)</span>}
                        {job_verify_status == PENDING_FOR_APPROVE && <span className='txt-sm txt-dark'><br />(Đang chờ xác thực)</span>}
                        {job_verify_status == VERIFIED && <span className='txt-dark'><br />Đã xác thực</span>}
                        <i className={`${job_verify_status == VERIFIED ? 'fa fa-check pull-right txt-green' : 'fa fa-chevron-right pull-right'}`} aria-hidden="true"></i>
                      </span>
                    </Link>
                  </li>
                }
                {gender == 'male' && <li className={`${id_card_verify_status == VERIFIED && education_verify_status == VERIFIED ? 'active ' : ' '}
                    ${education_verify_status == NOT_VERIFIED && is_card_verified ? 'bg-li--1 ' : ' '}
                    list-border__item panel__heading`}>
                    <Link
                      to={
                        education_verify_status == NOT_VERIFIED && is_card_verified
                          ? '/verification-education'
                          : '/verification'
                      }
                      className="list-border__link"
                    >
                      <span className={education_verify_status == NOT_VERIFIED && is_card_verified ? 'txt-blue' : 'txt-dark'}>
                        5. Xác thực học vấn của bạn
                        {id_card_verify_status != VERIFIED && <span className='txt-sm txt-dark'><br />(Chỉ có thể làm sau khi đã xác thực bằng cách 1)</span>}
                        {education_verify_status == PENDING_FOR_APPROVE && <span className='txt-sm txt-dark'><br />(Đang chờ xác thực)</span>}
                        {education_verify_status == VERIFIED && <span className='txt-dark'><br />Đã xác thực</span>}
                        <i className={`${education_verify_status == VERIFIED ? 'fa fa-check pull-right txt-green' : 'fa fa-chevron-right pull-right'}`} aria-hidden="true"></i>
                      </span>
                    </Link>
                  </li>
                }
              </ul>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <div className="txt-underline txt-bold">Chú ý:</div>
              <ol className="padding-l20">
                <li>Tất cả các thông tin, hình ảnh bạn cung cấp đều được giữ <span className="txt-blue txt-bold">bí mật tuyệt đối.</span></li>
                <li>Việc xác thực là <span className="txt-blue txt-bold">không bắt buộc.</span></li>
                <li>Ymeet.me không chịu trách nhiệm với bất cứ rủi ro nào của tài khoản chưa xác thực.</li>
              </ol>
            </div>
          </div>
        </div>

      </div>
    )
  }
}

Verification.propTypes = {

}

function mapStateToProps(state) {
  return state
}


export default connect(
  mapStateToProps,
)(Verification)
