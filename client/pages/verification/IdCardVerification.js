import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import HeaderContainer from 'containers/HeaderContainer'
import { connect } from 'react-redux'
import {
  CDN_URL,
  GA_ACTION_VERIFY,
  UPLOAD_DEVIDE_MODE,
} from 'constants/Enviroment';
import Modal from 'components/modals/Modal'
import _ from 'lodash'
import Dropzone from 'react-dropzone'
import {
  VERIFY_PICTURE_TYPE,
  NOT_VERIFIED,
  PENDING_FOR_APPROVE,
  VERIFIED
} from './Constant';

class IdCardVerification extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.uploadIdCardImage = this.uploadIdCardImage.bind(this);
    this.onOpenClick = this.onOpenClick.bind(this);
  }

  openModal() {
    this.props.gaSend(GA_ACTION_VERIFY, { page_source: 'Verification_Type_1' });
    this.setState(
      {
        modalIsOpen: true,
      }
    )
  }

  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  uploadIdCardImage(files) {
    this.props.saveFilesUpload(files, VERIFY_PICTURE_TYPE, UPLOAD_DEVIDE_MODE)
  }

  onOpenClick(type) {
    if (type === 1) {
      this.props.gaSend(GA_ACTION_VERIFY, { page_source: 'Upload_IDCard_Button' });
    }
    else if (type === 2) {
      this.props.gaSend(GA_ACTION_VERIFY, { page_source: 'Upload_DriverLicense_Button' });
    }
    this.dropzone.open();
  }

  renderInfo(msg) {
    return <div>
        <HeaderContainer {...this.props} />
        <div>{msg}</div>
    </div>
  }

  render() {
    const { verification_status } = this.props.user_profile.current_user;
    if (!verification_status) {
      return null;
    }
    const { id_card_verify_status } = verification_status;
    if (id_card_verify_status == PENDING_FOR_APPROVE) {
      return this.renderInfo('CMND/Bằng lái xe đang chờ xác thực');
    }
    if (id_card_verify_status == VERIFIED) {
      return this.renderInfo('CMND/Bằng lái xe đã được xác thực');
    }

    return (
      <div>
        <HeaderContainer {...this.props} />
        <div className="site-content" >
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <h3 className="txt-heading">Xác thực CMND/ Bằng lái xe</h3>
              </div>
              <div className="col-xs-12 mbm">
                Bạn cần cung cấp 1 ảnh rõ nét
                mặt trước của chứng minh thư hoặc bằng lái xe của bạn
                để kiểm tra độ trùng khớp với thông tin của bạn trên hệ thống
              </div>
            </div>

            <div className="image-helper">
              <div className="txt-center mbm">
                <i className="fa fa-check fa-lg txt-green mrs"></i>
                Gợi ý ảnh hợp lệ
              </div>
              <div className="txt-center mbm">
                <img src={`${CDN_URL}/general/id_card_2.png`} />
              </div>
            </div>

            <div className="row">
              <div className="col-xs-6">
                <Link to="/verification"><button
                    className="btn btn--b btn--5"
                  >
                    Thoát
                  </button>
                  </Link>
              </div>

              <div className="col-xs-6 mbm">
                <button
                  className="btn btn--p btn--b"
                  onClick={this.openModal}
                >
                  Tải ảnh lên
                </button>
              </div>
            </div>

          </div>

          <Modal
            transitionName="modal"
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
            className="modal__content modal__content--1"
            overlayClassName="modal__overlay"
            portalClassName="modal"
            contentLabel=""
          >
            <div className="well well--a well--sm mbm">
              <div className="txt-center txt-bold txt-blue">Xác thực tài khoản</div>
            </div>
            <div>
              <Dropzone
                onDrop={this.uploadIdCardImage}
                ref={(drz) => { this.dropzone = drz; }}
                multiple={false}
                style={{ display: 'none' }}
              />
              <button className="btn btn--b btn--p mbm" onClick={() => {this.onOpenClick(1)}} key="1" >
                Tải ảnh chứng minh thư
              </button>
              <button className="btn btn--b btn--p mbm" onClick={() => {this.onOpenClick(2)}} key="2" >
                Tải ảnh bằng lái xe
              </button>
            </div>

            <div className="modal__extra txt-center">
              <div className="txt-bold txt-lg txt-uppercase mbm txt-blue">Ảnh Hợp Lệ</div>
              <div className="l-flex-cspa mbm">
              <div>
                  <div className="mbs"><i className="fa fa-check-circle fa-2x txt-green"></i></div>
                  <div className="frame frame--md mbs">
                    <img src={`${CDN_URL}/general/id_card_2.png`} />
                  </div>
                </div>

              </div>
            </div>
            <button className="modal__btn-close" onClick={this.closeModal}>
              <i className="fa fa-times"></i>
            </button>
          </Modal>

        </div>
      </div>
    );
  }
}

IdCardVerification.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
}

function mapStateToProps(state) {
  return state
}


export default connect(
  mapStateToProps
)(IdCardVerification)
