import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Verification from './Verification';
import HeaderContainer from 'containers/HeaderContainer'
import {} from './Action';

export { default as EducationVerification } from './EducationVerification';
export { default as  JobVerification } from './JobVerification';
export { default as PhoneVerification } from './PhoneVerification';
export { default as SelfieVerification } from './SelfieVerification';
export { default as IdCardVerification } from './IdCardVerification';

const Verify = props => (
  <div>
    <HeaderContainer {...props} />
    <Verification {...props} />
  </div>
)

export default Verify;
