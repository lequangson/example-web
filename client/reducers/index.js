import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import similar_face_to_me from 'pages/list-similar-to-me/Reducer';
import similar_face_to_my_fb_friends from 'pages/list-similar-to-my-fb-friends/Reducer';
import similar_face_to_my_photos from 'pages/list-similar-to-my-photos/Reducer';
import similar_face_to_my_ymm_friends from 'pages/list-similar-to-my-ymm-friends/Reducer';
import similar_sharing from 'pages/similar-face/Reducer';
import upload_image from 'components/buttons/upload-image/Reducer';
import campaign from 'pages/campaign/Reducer';
import quiz_vui from 'pages/quiz-vui/Reducer';
import list_boost_in_rank from 'pages/list-boost-in-rank/Reducer';
import lucky_spinner from 'pages/lucky-spinner/Reducer';
import super_like_spinner from 'pages/super-like-spinner/Reducer';
import people_liked_me from 'pages/people-liked-me/Reducer';
import list_same_gender_boost_users from 'containers/list-same-gender-boost-users/Reducer';
import auth from './auth';
import search from './search';
import user_profile from './userProfile';
import dictionaries from 'pages/dictionaries/Reducer';
import Enviroment from './Enviroment';
import PeopleILiked from './PeopleILiked';
import Like from './Like';
import Tips from './Tips';
import photoStream from './photoStream';
import Payment from './Payment';
import Tarot from './Tarot';
import Daily_Remind from './Daily_Remind';
import BestVotePhoto from './BestVotePhoto';
import random_match from './random_match';
import WhatHot from './WhatHot';
import Match_Questions from './Match_Questions';

// migrate chat
import chatList from './chatList';
import chat_user from './chat_user';
import navigation from './navigation';
import messages from './messages';
import messageBox from './messageBox';
import iceBreakingQuestions from './iceBreakingQuestions';
import visitants from 'pages/visitants/Reducer';

const appReducer = combineReducers({
  auth,
  search,
  user_profile,
  dictionaries,
  Enviroment,
  PeopleILiked,
  Like,
  Tips,
  photoStream,
  Payment,
  Tarot,
  BestVotePhoto,
  random_match,
  Daily_Remind,
  WhatHot,
  Match_Questions,
  similar_face_to_me,
  similar_face_to_my_fb_friends,
  similar_face_to_my_photos,
  similar_face_to_my_ymm_friends,
  similar_sharing,
  upload_image,
  campaign,
  quiz_vui,
  list_boost_in_rank,
  lucky_spinner,
  super_like_spinner,
  list_same_gender_boost_users,
  //migrate chat
  chatList,
  chat_user,
  navigation,
  messages,
  messageBox,
  iceBreakingQuestions,
  visitants,
  people_liked_me,
})

const rootReducer = (state, action) => {
  if (action.type === 'AUTH_LOGOUT') {
    state = undefined;
  }
  return appReducer(state, action);
}
export default rootReducer;
