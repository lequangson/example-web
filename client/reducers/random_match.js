import { GET_RANDOM_MATCH_REQUEST, GET_RANDOM_MATCH_SUCCESS, GET_RANDOM_MATCH_FAILURE,
  UPDATE_RANDOM_MATCH_REQUEST, UPDATE_RANDOM_MATCH_SUCCESS, UPDATE_RANDOM_MATCH_FAILURE,
  UPDATE_MODAL_STATE, CLEAR_MODAL_DATA, GET_DETAIL_RANDOM_MATCH_REQUEST, GET_DETAIL_RANDOM_MATCH_SUCCESS, GET_DETAIL_RANDOM_MATCH_FAILURE,
  UPDATE_DETAIL_RANDOM_MATCH, UPDATE_RANDOM_MATCH_STATE,
  GET_REMIND_RANDOM_MATCH_REQUEST, GET_REMIND_RANDOM_MATCH_SUCCESS, GET_REMIND_RANDOM_MATCH_FAILURE,
  SHOW_BUTTON_START_CHAT,
 } from '../constants/random_match'
import { PEOPLE_I_LIKED_PAGE, NOTIFICATION_ERROR } from '../constants/Enviroment'
import { UPDATE_USER_PROFILE_STATE, VOTE, UNVOTE } from '../constants/userProfile'
import { DEFAULT_ERROR_LOAD_PAGE } from '../constants/TextDefinition'
import { VOTE_PHOTO_REQUEST, VOTE_PHOTO_FAILURE } from '../constants/photoStream'
import { compareIdentifiers } from '../utils/common'
import notification from '../utils/errors/notification'

function random_match(state = {
  isFetching: false,
  error: false,
  data: [],
  remind: {},
  hiddenCard: true,
  isOpenNotificationRandomMatch: false,
  randomMatchInfo: {},
  isUpdateRandomMatch: false,
  random_match_user: '',
  isModalCloseable: true,
}, action) {
  switch (action.type) {
    case GET_RANDOM_MATCH_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false,
      })
    case GET_RANDOM_MATCH_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        data: action.response.data.slice(0,4), //prevent api response more than 4 people because re-index problem
        isUpdateRandomMatch: false,
      })
    case GET_RANDOM_MATCH_FAILURE:
      notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
      return Object.assign({}, state, {
        isFetching: false,
        error: true,
      })
    case UPDATE_MODAL_STATE:
      return Object.assign({}, state, {
        hiddenCard: action.state,
      })
    case CLEAR_MODAL_DATA:
      return Object.assign({}, state, {
        data: [],
      })

    case GET_REMIND_RANDOM_MATCH_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false,
      })
    case GET_DETAIL_RANDOM_MATCH_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        randomMatchInfo: action.response.data
      })
    case GET_REMIND_RANDOM_MATCH_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        remind: action.response.data,
      })
    case GET_REMIND_RANDOM_MATCH_FAILURE:
      notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
      return Object.assign({}, state, {
        isFetching: false,
        error: true,
      })

    case UPDATE_DETAIL_RANDOM_MATCH:
      return Object.assign({}, state, {
        randomMatchInfo: action.random_match,
        isOpenNotificationRandomMatch: true
      })

    case UPDATE_RANDOM_MATCH_STATE:
      const current_random_match = state;
      current_random_match[action.key] = action.value;
      return Object.assign({}, current_random_match);

    case UPDATE_RANDOM_MATCH_REQUEST:
      return Object.assign({}, state, {
        isUpdateRandomMatch: true,
        random_match_user: action.extentions.nick_name,
        isModalCloseable: false,
      })

     case SHOW_BUTTON_START_CHAT:
       const updateObj = state.data.find(user => user.identifier == action.identifier) || [];
       updateObj.is_random_matched = true;
       return Object.assign({}, state, {
         isFetching: false,
         isModalCloseable: true,
       })
    default:
      return state
  }
}

export default random_match;