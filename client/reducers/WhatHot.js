import _ from 'lodash'
import {
  GET_WHAT_HOT_REQUEST, GET_WHAT_HOT_SUCCESS, GET_WHAT_HOT_FAILURE,
  NEXT_WHAT_HOT_USER, HIDE_ADVERTISMENT_PROGRAM,
} from '../constants/WhatHot'
import {
  SEARCH_PAGE_SIZE, UPDATE_SEARCH_STATE
} from '../constants/search'
import { compareIdentifiers, uniqUsers } from '../utils/common'

const defaultState = {
  isFetching: false,
  error: false,
  errorMessage: '',
  users: [],
  totalItem: 0,
  currentPageIndex: 0,
  selectedUserIndex: -1,
  reachEnd: false,
  showAdProgram: false,
  adProgramIndex: 0,
  allowAdProgramIndex: [0, 1, 2, 3, 4, 5],
  totalLikeSent: 0,
}

function WhatHot(state = defaultState, action) {
  switch (action.type) {
    case GET_WHAT_HOT_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false,
        errorMessage: '',
      })
    case GET_WHAT_HOT_SUCCESS:
      const listUser = [...state.users,...action.response.data];
      const unique_users = uniqUsers(listUser);
      const total_duplicate_users = listUser.length - unique_users.length;
      let total_result = action.response.paging_model.total_item?
        action.response.paging_model.total_item - total_duplicate_users :
        state.totalItem;
      if (!action.response.data.length || action.response.data.length % SEARCH_PAGE_SIZE !== 0) {
        total_result = unique_users.length;
      }
      const selectedUserIndex = state.selectedUserIndex === -1? 0 : state.selectedUserIndex;

      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        errorMessage: '',
        users: unique_users,
        totalItem: total_result,
        currentPageIndex: action.extentions.pageIndex,
        selectedUserIndex: selectedUserIndex,
        // reachEnd: action.response.data.length < SEARCH_PAGE_SIZE
      })

    case GET_WHAT_HOT_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true,
        errorMessage: action.error_message,
      })

    case NEXT_WHAT_HOT_USER:
      const selectedIndex = state.selectedUserIndex <= state.users.length -1?
        state.selectedUserIndex + 1 : state.selectedUserIndex;
      const newLikeSent = action.extentions.isLikeAction ? state.totalLikeSent + 1 : state.totalLikeSent;
      let showAdProgram = state.showAdProgram;
      if (action.extentions.isLikeAction &&
        state.allowAdProgramIndex.length > 0 &&
        (newLikeSent === 3 || newLikeSent === 8)) {
        showAdProgram = true;
      }

      let newAdProgramIndex = state.adProgramIndex;
      if (showAdProgram) {
        if (action.extentions.userGender === 'male' &&
          action.extentions.userType !== 4) {
          const index = _.random(0, state.allowAdProgramIndex.length - 1);
          newAdProgramIndex = state.allowAdProgramIndex[index];
        }
        else {
          if (state.allowAdProgramIndex.indexOf(0) === -1) {
            showAdProgram = false;
          }
          else {
            newAdProgramIndex = 0;
          }
        }
      }

      let newAllowAdProgramIndex
      if (showAdProgram) {
        const index = state.allowAdProgramIndex.indexOf(newAdProgramIndex);
        newAllowAdProgramIndex = [...state.allowAdProgramIndex.slice(0, index), ...state.allowAdProgramIndex.slice(index + 1)];
      }
      else {
        newAllowAdProgramIndex = state.allowAdProgramIndex;
      }

      return Object.assign({}, state, {
        selectedUserIndex: selectedIndex,
        totalLikeSent: newLikeSent,
        showAdProgram: showAdProgram,
        adProgramIndex: newAdProgramIndex,
        allowAdProgramIndex: newAllowAdProgramIndex,
      })
    case UPDATE_SEARCH_STATE:
      let user_index = state.users.findIndex(userProfile =>
      compareIdentifiers(userProfile.identifier, action.identifier))
      if (user_index == -1) {
        return state
      }
      let updateData = {}
      updateData[action.key] = action.value
      const new_users =  [
        ...state.users.slice(0, user_index), // before the one we are updating
        {...state.users[user_index], ...updateData},
        ...state.users.slice(user_index + 1), // after the one we are updating
      ]
      return Object.assign({}, state, {
        users: user_index > -1 ? new_users : state.users
      })
    case HIDE_ADVERTISMENT_PROGRAM:
      return Object.assign({}, state, {
        showAdProgram: false
      })
    default:
      return state
  }
}

export default WhatHot;