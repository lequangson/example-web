import { browserHistory } from 'react-router';
import update from 'immutability-helper';
import {
  GET_USER_PROFILE_REQUEST, GET_USER_PROFILE_SUCCESS, GET_USER_PROFILE_FAILURE,
  GET_CURRENT_USER_REQUEST, GET_CURRENT_USER_SUCCESS, GET_CURRENT_USER_FAILURE,
  SEND_LIKE_REQUEST, SEND_LIKE_SUCCESS, SEND_LIKE_FAILURE, INCREASE_NOTIFICATION_NUMBER,
  UPDATE_USER_PROFILE_REQUEST,  UPDATE_USER_PROFILE_SUCCESS, UPDATE_USER_PROFILE_FAILURE,
  GET_SELF_INTRODUCTION_REQUEST, GET_SELF_INTRODUCTION_SUCCESS, GET_SELF_INTRODUCTION_FAILURE,
  UPLOAD_IMAGE_VALIDATE_REQUEST, UPLOAD_IMAGE_VALIDATE_SUCCESS, UPLOAD_IMAGE_VALIDATE_FAILURE,
  UPLOAD_IMAGE_REQUEST, UPLOAD_IMAGE_SUCCESS, UPLOAD_IMAGE_FAILURE,
  DELETE_USER_PICTURE_REQUEST, DELETE_USER_PICTURE_SUCCESS, DELETE_USER_PICTURE_FAILURE,
  AVATAR_PICTURE, OTHER_PICTURE, ADD_FAVORITE_REQUEST, ADD_FAVORITE_SUCCESS, ADD_FAVORITE_FAILURE,
  DELETE_FAVORITE_REQUEST, DELETE_FAVORITE_SUCCESS, DELETE_FAVORITE_FAILURE,
  GET_FAVORITE_REQUEST, GET_FAVORITE_SUCCESS, GET_FAVORITE_FAILURE, NOTIFICATION_TYPE_CHAT_APP,
  GET_FB_ALBUM_REQUEST, GET_FB_ALBUM_SUCCESS, GET_FB_ALBUM_FAILURE,
  GET_FB_PICTURE_REQUEST, GET_FB_PICTURE_SUCCESS, GET_FB_PICTURE_FAILURE,
  SYNCH_FB_PICTURE_REQUEST, SYNCH_FB_PICTURE_SUCCESS, SYNCH_FB_PICTURE_FAILURE,
  UNBLOCK_USER_REQUEST, UNBLOCK_USER_SUCCESS, UNBLOCK_USER_FAILURE,
  BLOCK_USER_REQUEST, BLOCK_USER_SUCCESS, BLOCK_USER_FAILURE, PEOPLE_LIKE_ME_REAL_TIME,
  GET_BLOCKED_USERS_REQUEST, GET_BLOCKED_USERS_SUCCESS, GET_BLOCKED_USERS_FAILURE,
  BLOCK_TYPE_BLOCKED, BLOCK_TYPE_MUTED, UPDATE_USER_PROFILE_STATE,
  BLOCK_NOTIFICATION, UNBLOCK_NOTIFICATION, INCREASE_VISITANT_NUMBER,
  GET_MATCHED_USERS_REQUEST, GET_MATCHED_USERS_SUCCESS, GET_MATCHED_USERS_FAILURE, NOTIFICATION_TYPE_WHO_ADD_ME_FAVOURITE,
  CLEAR_NOTIFICATION_SUCCESS, NOTIFICATION_TYPE_LIKE, NOTIFICATION_TYPE_CHAT, NOTIFICATION_TYPE_VIEW_MY_PROFILE,
  BLOCKED_USER_CODE, VISITANT_REAL_TIME, SET_AVATAR_REQUEST, SET_AVATAR_SUCCESS,
  SET_AVATAR_FAILURE, UPDATE_MATCHED_NOTIFICATION, DECREASE_NOTIFICATION_NUMBER,
  UPDATE_PAYMENT_PACKAGE_REQUEST, UPDATE_PAYMENT_PACKAGE_SUCCESS, UPDATE_PAYMENT_PACKAGE_FAILURE, UPDATE_ASKED_LOCATION_STATUS,
  UPLOADED_VERIFY_IMAGE, UPDATE_LIKE_STYLE, UPDATE_LIKED_MY_PHOTO_MODAL, UPDATE_FIRST_LIKE_STATUS,
  VOTE, UNVOTE, ACTIVE_BOOST_RANK_REQUEST, ACTIVE_BOOST_RANK_SUCCESS, ACTIVE_BOOST_RANK_FAILURE,
  GET_WHO_ADD_ME_TO_FAVOURITE_REQUEST, GET_WHO_ADD_ME_TO_FAVOURITE_SUCCESS, GET_WHO_ADD_ME_TO_FAVOURITE_FAILURE,
  NOTIFICATION_TYPE_VIEW_WHO_ADD_FAVOURITE,
  GET_CHAT_SUGGESTED_USER_REQUEST, GET_CHAT_SUGGESTED_USER_SUCCESS, GET_CHAT_SUGGESTED_USER_FAILURE,
  UPDATE_USER_PERMISSION_SUCCESS, SHOW_CHAT_SUGGESTED_USER, START_CHAT, UPDATE_VERIFICATION_STATUS,
  GET_LUCKY_SPINNER_GIFT_SUCCESS, GIFTSDICT,
  INCREASE_CURRENT_COIN, GET_USER_LICENCE_SUCCESS
} from 'constants/userProfile'
import {
    UPDATE_WELCOMEPAGE_COMPLETION_REQUEST, UPDATE_WELCOMEPAGE_COMPLETION_SUCCESS,
    UPDATE_WELCOMEPAGE_COMPLETION_FAILURE, UPDATE_BIRTHDAY_WELCOME_PAGE
} from 'containers/welcome/Constants';
import {
  DELETE_MY_ACCOUNT_REQUEST, DELETE_MY_ACCOUNT_SUCCESS, DELETE_MY_ACCOUNT_FAILURE,
} from 'pages/delete-account/Constant';
import { UPDATE_RANDOM_MATCH_REQUEST, UPDATE_RANDOM_MATCH_SUCCESS, UPDATE_RANDOM_MATCH_FAILURE } from '../constants/random_match'
import { NotificationManager, NotificationContainer } from 'components/commons/Notifications';
import notification from '../utils/errors/notification'
import {
    NOTIFICATION_INFOR, NOTIFICATION_SUCCESS, NOTIFICATION_WARNING, NOTIFICATION_ERROR, GA_SEND, SEND_GOOGLE_ANALYTIC,
    PEOPLE_LIKED_ME_PAGE, FAVORITE_PAGE, FOOTPRINT_PAGE, BLOCK_USER_BLOCKED_PAGE, BLOCK_USER_MUTED_PAGE, PEOPLE_I_LIKED_PAGE, MATCHED_PAGE, OTHER_PROFILE_PAGE,
    BOOST_RANK_COST, RANDOM_MATCH_COST, UNLOCK_WHO_LIKE_ME_COST, UNLOCK_WHO_LIKE_ME_PARAMS
} from '../constants/Enviroment'
import { GET_LAST_MESSAGES_SUCCESS } from '../constants/ChatMessage'
import { COIN_CONSUME_REQUEST, COIN_CONSUME_SUCCESS, COIN_CONSUME_FAILURE } from '../constants/Payment'
import { NUMBER_COINS_NEED_FOR_SUPER_LIKE } from '../constants/Like';
import { DEFAULT_ERROR_LOAD_PAGE } from '../constants/TextDefinition'
import {
  gaTrackEvent, sendGoogleAnalytic,
} from '../utils/google_analytic/ga_event'
import { compareIdentifiers, getUserId } from '../utils/common'
import { SEND_GIFT_SUCCESS } from 'pages/campaign/Constant';
import { SAVE_LUCKY_SPINNER_RESULT_SUCCESS } from 'pages/lucky-spinner/Constant';
import * as ymmStorage from '../utils/ymmStorage';


const countGift = (type_id, gifts) => {
    return gifts.filter((gift) => { return gift.gift_code == type_id && gift.is_active === true }).length;
};

const inventoryGifts = (gifts) => {
    const giftList = {};
    for( let key in GIFTSDICT ){
        giftList[key] = countGift(GIFTSDICT[key], gifts);
    }
    return giftList;
};

function processUploadPicture(pictureType, current_user, uploadedPictures){
    let pictures = [];
    switch (Number(pictureType)){
        case AVATAR_PICTURE:
            if (uploadedPictures){
                if (Array.isArray(uploadedPictures)){
                    for(let i = 0; i < uploadedPictures.length; i++){
                        pictures.push(uploadedPictures[i]);
                    }
                }
                else{
                    pictures.push(uploadedPictures);
                }
            }
            if (current_user.user_pictures){
                for (let i = 1; i < current_user.user_pictures.length; i ++){
                    pictures.push(current_user.user_pictures[i]);
                }
            }
            break;
        case OTHER_PICTURE:
            if (current_user.user_pictures){
                for (let i = 0; i < current_user.user_pictures.length; i ++){
                    pictures.push(current_user.user_pictures[i]);
                }
            }
            if (Array.isArray(uploadedPictures)){
                for(let i = 0; i < uploadedPictures.length; i++){
                    pictures.push(uploadedPictures[i]);
                }
            }
            else{
                pictures.push(uploadedPictures);
            }
            break;
        default:
            if (current_user.user_pictures){
                for (let i = 0; i < current_user.user_pictures.length; i ++){
                    pictures.push(current_user.user_pictures[i]);
                }
            }
            break;
    }
    return pictures;
}

function profile(state = {
    selected_user: [],
    isFetching_Selected_User: false,
    current_user: {},
    chat_suggested: {
        chat_suggested_users: [],
        numberTimesShown: 0
    },
    people_liked_me_real_time: [],
    visitant_real_time: [],
    favorite_lists: [],
    who_add_favourite: [],
    self_introduction: [],
    isFetching: false,
    isLoaded: false,
    error: false,
    error_message: null,
    upload_avater: false,
    fb_albums: [],
    fb_pictures: [],
    visitants: [],
    blocked_users: [],
    muted_users: [],
    blocked_notification: [],
    unblocked_notification: [],
    matched_users: [],
    haveLoadedMatch: false,
    random_match_users: [],
    isLoadingMatchedUser: false,
    isUploadingAvatar: false,
    total_unread_message:0,
    number_chat_room_has_message: 0,
    isUploadingSubPhoto: false,
    has_message_from_admin: false,
    has_start_chat: false,
    pack: '',
    like_style: '',
    uploaded_verify_images: false,
    people_liked_my_photo_modal: {},
    chat_status: {
      last_messages: [],
      online_status: [],
    },
    is_page_loading: {
      visitants: false,
      favorite_lists: false,
      who_add_favourite: false,
      matched_users:false,
      blocked_users: false,
    },
    lucky_spinner_gifts: {}
  }, action) {
  switch (action.type) {
    case UPLOADED_VERIFY_IMAGE:
        return Object.assign({}, state, {
            uploaded_verify_images: action.status,
        })
    case GET_USER_PROFILE_REQUEST: //get user profile by identifier
        return Object.assign({}, state, {
            isFetching_Selected_User: true,
            error: false,
        })

    case GET_USER_PROFILE_SUCCESS: // get user profile by identifier in case successfully
        return Object.assign({}, state, {
            selected_user: action.response.data,
            isFetching_Selected_User: false,
            error: false,
        })

    case GET_USER_PROFILE_FAILURE: // get user profile by identifier in case failure
        notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
         return Object.assign({}, state, {
            isFetching_Selected_User: false,
            errorMessage: action.error_message,
            error: true
        })

    //get current user
    case GET_CURRENT_USER_REQUEST: // get current user logged in in case request
        return Object.assign({}, state, {
            isFetching: true,
            errorMessage: null
        })

    case GET_CURRENT_USER_SUCCESS: // get current user logged in success case
        var current_user = action.response.data
        dataLayer.push({
            'SourceAge': current_user ? current_user.age : '',
            'SourceGender': current_user ? current_user.gender || '' : '',
            'SourceResidence': current_user ? (current_user.residence && current_user.residence.value) ? current_user.residence.value : '' : '',
            'SourceBirthPlace': current_user ? (current_user.birth_place && current_user.birth_place.value) ? current_user.birth_place.value : '' : '',
            'SourceUserId': current_user ? getUserId(current_user.identifier) : ''
        })
        return Object.assign({}, state, {
            current_user: {...state.current_user, ...current_user},
            isFetching: false,
        })
    case UPDATE_VERIFICATION_STATUS:
        const newCurrentUser = update(state.current_user, {verification_status: {[action.field_name]: {$set: 1}}});
        return Object.assign({}, state, {
                current_user: {...state.current_user, ...newCurrentUser}
        })
    case GET_CURRENT_USER_FAILURE:
        if (action.error_code === BLOCKED_USER_CODE) {
            ymmStorage.clear()
            browserHistory.push('/suspend-account')
        } else {
            notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
        }
        // browserHistory.push('/suspend-account')
        return Object.assign({}, state, {
            isFetching: false,
            errorMessage: action.error_message
        })

    //-------------------Begin: Send like to partner--------------------------//
    case SEND_LIKE_REQUEST:
        return Object.assign({}, state, {
            isFetching: true,
            errorMessage: null
        })

    case SEND_LIKE_SUCCESS:
        if (action.extentions.likeSuccessNotification){
          notification(NOTIFICATION_SUCCESS, action.extentions.likeSuccessNotification);
        }
        return Object.assign({}, state, {
                isFetching: false,
                errorMessage: null
        })
    case SEND_LIKE_FAILURE:
        if(action.error_message){
            notification(NOTIFICATION_ERROR, action.error_message)
        }
        return Object.assign({}, state, {
            isFetching: false,
            errorMessage: action.error_message
        })

    //-------------------End  : Send like to partner--------------------------//

    //-------------------End  : Get People Like Me--------------------------//
    case GET_CHAT_SUGGESTED_USER_REQUEST:
        return Object.assign({}, state, {
            isFetching: true,
            isLoaded: false,
            errorMessage: null
        })
    case GET_CHAT_SUGGESTED_USER_SUCCESS:
        return Object.assign({}, state, {
            chat_suggested: {...state.chat_suggested, chat_suggested_users: action.response.data},
            isLoaded: true,
            isFetching: false,
        })
    case GET_CHAT_SUGGESTED_USER_FAILURE:
        return Object.assign({}, state, {
            isFetching: false,
            errorMessage: action.error_message
        })

    case SHOW_CHAT_SUGGESTED_USER:
        const nextNumberTimesShown = state.chat_suggested.numberTimesShown + 1;
        return Object.assign({}, state, {
            chat_suggested: {...state.chat_suggested, numberTimesShown: nextNumberTimesShown}
        })

    //-------------------Begin: Get Self Introduction--------------------------//
    case GET_SELF_INTRODUCTION_REQUEST:
        return Object.assign({}, state, {
            isFetching: true,
            errorMessage: null
        })

    case GET_SELF_INTRODUCTION_SUCCESS:
        return Object.assign({}, state, {
            self_introduction: action.response.data,
            isFetching: false,
        })

    case GET_SELF_INTRODUCTION_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
            isFetching: false,
            errorMessage: action.error_message
        })
    //-------------------End  : Get Self Introduction--------------------------//

    //-------------------Begin: Validate image---------------------------------//
    case UPLOAD_IMAGE_VALIDATE_REQUEST:
        return Object.assign({}, state, {
            isUploadingAvatar: action.pictureType === AVATAR_PICTURE ? true : state.isUploadingAvatar,
            isUploadingSubPhoto: action.pictureType === OTHER_PICTURE ? true : state.isUploadingSubPhoto,
            error: false,
            errorMessage: null
        })

    case UPLOAD_IMAGE_VALIDATE_SUCCESS:
        return Object.assign({}, state, {
            error: false,
            isUploadingAvatar: action.extentions.pictureType === AVATAR_PICTURE ? false : state.isUploadingAvatar,
            isUploadingSubPhoto: action.extentions.pictureType === OTHER_PICTURE ? false : state.isUploadingSubPhoto,
        })

    case UPLOAD_IMAGE_VALIDATE_FAILURE:
        if(!action.extentions.ignoreMsg){
            notification(NOTIFICATION_ERROR, action.error_message)
        }
        return Object.assign({}, state, {
            isUploadingAvatar: action.extentions.pictureType === AVATAR_PICTURE ? false : state.isUploadingAvatar,
            isUploadingSubPhoto: action.extentions.pictureType === OTHER_PICTURE ? false : state.isUploadingSubPhoto,
            error: true
        })
    //-------------------End  : Validate image---------------------------------//

    //-------------------Begin: Upload image---------------------------------//
    case UPLOAD_IMAGE_REQUEST:
        return Object.assign({}, state, {
            isUploadingAvatar: action.pictureType === AVATAR_PICTURE ? true : state.isUploadingAvatar,
            isUploadingSubPhoto: action.pictureType === OTHER_PICTURE ? true : state.isUploadingSubPhoto,
            error: false
        })

    case UPLOAD_IMAGE_SUCCESS:
        let pictures = [];
        if (action.extentions && state.current_user){
            pictures = processUploadPicture(action.extentions, state.current_user, action.response.data);
        }
        return Object.assign({}, state, {
            isUploadingAvatar: action.extentions === AVATAR_PICTURE ? false : state.isUploadingAvatar,
            isUploadingSubPhoto: action.extentions === OTHER_PICTURE ? false : state.isUploadingSubPhoto,
            error: false,
            current_user: {...state.current_user, user_pictures: pictures}
        })

    case UPLOAD_IMAGE_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
            isUploadingAvatar: action.pictureType === AVATAR_PICTURE ? false : state.isUploadingAvatar,
            isUploadingSubPhoto: action.pictureType === OTHER_PICTURE ? false : state.isUploadingSubPhoto,
            error: true
        })
    //-------------------End  : Upload image---------------------------------//

    //-------------------Begin: Set Avatar---------------------------------//
    case SET_AVATAR_REQUEST:
        return Object.assign({}, state, {
            isUploadingAvatar: true,
            error: false
        })

    case SET_AVATAR_SUCCESS:
    {
        const pictures = action.response.data ? action.response.data : state.current_user.user_pictures;
        return Object.assign({}, state, {
            isUploadingAvatar: false,
            error: false,
            current_user: {...state.current_user, user_pictures: pictures}
        })
    }

    case SET_AVATAR_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
            isUploadingAvatar: false,
            error: true,
        })
    //-------------------End  : Set Avatar---------------------------------//

    case UPDATE_USER_PROFILE_REQUEST: // get people liked me request
        return Object.assign({}, state, {
            isFetching: true,
            errorMessage: null
        })

    case UPDATE_USER_PROFILE_SUCCESS: // get people liked me success
        return Object.assign({}, state, {
            isFetching: false,
            current_user: {...state.current_user, ...action.response.data}
        })

    case UPDATE_USER_PROFILE_FAILURE: // get people liked me failure
        if(action.msg) {
            notification(NOTIFICATION_ERROR, action.msg)
            return state
        }
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
            isFetching: false,
            errorMessage: action.error_message
        })

    //-------------------Begin: Delete picture---------------------------------//
    case DELETE_USER_PICTURE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        errorMessage: null
      })

    case DELETE_USER_PICTURE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        current_user: {...state.current_user, user_pictures: state.current_user.user_pictures.filter(picture => picture.id != action.extentions)}
      })

    case DELETE_USER_PICTURE_FAILURE:
      notification(NOTIFICATION_ERROR, action.error_message)
      return Object.assign({}, state, {
        isFetching: false,
        errorMessage: action.error_message
      })
    //-------------------End  : Delete picture---------------------------------//

    //-------------------Begin: Add favorite ---------------------------------//
    case ADD_FAVORITE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        errorMessage: null
      })

    case ADD_FAVORITE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      })

    case ADD_FAVORITE_FAILURE:
      notification(NOTIFICATION_ERROR, action.error_message)
      return Object.assign({}, state, {
        isFetching: false,
        errorMessage: action.error_message
      })
    //-------------------End: Add favorite ---------------------------------//

    //-------------------Begin: Add favorite ---------------------------------//
    case DELETE_FAVORITE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        errorMessage: null
      })

    case DELETE_FAVORITE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      })

    case DELETE_FAVORITE_FAILURE:
      notification(NOTIFICATION_ERROR, action.error_message)
      return Object.assign({}, state, {
        isFetching: false,
        errorMessage: action.error_message
      })
    //-------------------End: Add favorite ---------------------------------//

    //-------------------Begin: Add favorite ---------------------------------//
    case GET_FAVORITE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        is_page_loading: {...state.is_page_loading, favorite_lists: false},
        errorMessage: null
      })

    case GET_FAVORITE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        is_page_loading: {...state.is_page_loading, favorite_lists: true},
        favorite_lists: action.response.data
      })

    case GET_FAVORITE_FAILURE:
      notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
      return Object.assign({}, state, {
        isFetching: false,
        errorMessage: action.error_message
      })
    //-------------------End: Add favorite ---------------------------------//

    //-------------------Begin: Send google analytic---------------------------------//
    case GA_SEND:
        gaTrackEvent(action.eventAction, state.current_user, action.ga_track_data);
        return state;

    case SEND_GOOGLE_ANALYTIC:
        sendGoogleAnalytic(action.eventAction, state.current_user, action.ga_track_data);
        return state;
    //-------------------End  : Send google analytic---------------------------------//

    //-------------------Begin: Get Facebook Albums---------------------------------//
    case GET_FB_ALBUM_REQUEST:
        return Object.assign({}, state, {
            isFetching: true
        })

    case GET_FB_ALBUM_SUCCESS:
        return Object.assign({}, state, {
            isFetching: false,
            fb_albums: action.response.data
        })

    case GET_FB_ALBUM_FAILURE:
        notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
        return Object.assign({}, state, {
            isFetching: false
        })
    //-------------------End  : Get Facebook Albums---------------------------------//

    //-------------------Begin: Get Facebook Pictures---------------------------------//
    case GET_FB_PICTURE_REQUEST:
        return Object.assign({}, state, {
            isFetching: true
        })

    case GET_FB_PICTURE_SUCCESS:
        return Object.assign({}, state, {
            isFetching: false,
            fb_pictures: action.response.data
        })

    case GET_FB_PICTURE_FAILURE:
        notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
        return Object.assign({}, state, {
            isFetching: false
        })
    //-------------------End  : Get Facebook Pictures---------------------------------//

    //-------------------Begin: Synchronize Facebook Pictures-------------------------//
    case SYNCH_FB_PICTURE_REQUEST:
        return Object.assign({}, state, {
            isUploadingAvatar: action.pictureType === AVATAR_PICTURE ? true : state.isUploadingAvatar,
            isUploadingSubPhoto: action.pictureType === OTHER_PICTURE ? true : state.isUploadingSubPhoto,
            error: false
        })

    case SYNCH_FB_PICTURE_SUCCESS:
        let user_pictures = [];
        try{
            if (action.extentions && state.current_user){
                user_pictures = processUploadPicture(action.extentions, state.current_user, action.response.data);
            }
            else{
                user_pictures = state.current_user.user_pictures;
            }
        }
        catch(ex){
            console.log('SYNCH_FB_PICTURE_SUCCESS', ex)
        }
        return Object.assign({}, state, {
            isUploadingAvatar: action.extentions === AVATAR_PICTURE ? false : state.isUploadingAvatar,
            isUploadingSubPhoto: action.extentions === OTHER_PICTURE ? false : state.isUploadingSubPhoto,
            error: false,
            current_user: {...state.current_user, user_pictures: user_pictures}
        })

    case SYNCH_FB_PICTURE_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
            isUploadingAvatar: action.pictureType === AVATAR_PICTURE ? false : state.isUploadingAvatar,
            isUploadingSubPhoto: action.pictureType === OTHER_PICTURE ? false : state.isUploadingSubPhoto,
            error: true
        })
    //-------------------End  : Synchronize Facebook Pictures----------------------------//
    
   //-------------------Begin: Block  or Mute User-------------------------//
    case BLOCK_USER_REQUEST:
        return Object.assign({}, state, {
            isFetching: true,
            error: false
        })
    case BLOCK_USER_SUCCESS:
        //Do anything here
        return Object.assign({}, state, {
            isFetching: false,
            error: false
        })

    case BLOCK_USER_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
            isFetching: false,
            error: true
        })
    //-------------------End  : Synchronize Facebook Pictures----------------------------//

    case UNBLOCK_USER_REQUEST:
        return Object.assign({}, state, {
            isFetching: true,
            error: false
        })

    case UNBLOCK_USER_SUCCESS:
        //Do anything here
        return Object.assign({}, state, {
            isFetching: false,
            error: false
        })

    case UNBLOCK_USER_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
            isFetching: false,
            error: true
        })
    case GET_BLOCKED_USERS_REQUEST:
        return Object.assign({}, state, {
            isFetching: true,
            is_page_loading: {...state.is_page_loading, blocked_users: false },
            error: false
        })

    case GET_BLOCKED_USERS_SUCCESS:
        //Do anything here
        return Object.assign({}, state, {
            isFetching: false,
            blocked_users: action.extentions == BLOCK_TYPE_BLOCKED ?  action.response.data : state.blocked_users,
            muted_users: action.extentions == BLOCK_TYPE_MUTED ?  action.response.data : state.muted_users,
            is_page_loading: {...state.is_page_loading, blocked_users: true },
            error: false
        })

    case GET_BLOCKED_USERS_FAILURE:
        notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
        return Object.assign({}, state, {
            isFetching: false,
            error: true
        })
    //-------------------End  : Synchronize Facebook Pictures----------------------------//

    //-------------------Begin: Delete my account----------------------------------------//
    case DELETE_MY_ACCOUNT_REQUEST:
        return Object.assign({}, state, {
            isFetching: true,
            error: false
        })
    case DELETE_MY_ACCOUNT_SUCCESS:
        return Object.assign({}, state, {
            isFetching: false,
            error: false,
            current_user: Object.assign({}, state.current_user, {
              user_status: 3
            })
        })
    case DELETE_MY_ACCOUNT_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
            isFetching: false,
            error: true
        })
    //-------------------End  : Delete my account----------------------------------------//
    case UPDATE_USER_PROFILE_STATE:
      if(action.page != PEOPLE_I_LIKED_PAGE) {
        switch(action.page) {
            case PEOPLE_LIKED_ME_PAGE:
                var state_data = "people_liked_me"
            break
            case FAVORITE_PAGE:
                var state_data = 'favorite_lists'
            break
            case FOOTPRINT_PAGE:
                var state_data = 'visitants'
            break
            case BLOCK_USER_BLOCKED_PAGE:
                var state_data = 'blocked_users'
            break
            case BLOCK_USER_MUTED_PAGE:
                var state_data = "muted_users"
            break
            case MATCHED_PAGE:
                var state_data = "matched_users"
            break
            default:
                var state_data = ''
            break
        }
        if(state_data == '') {
            return state
        }
        var user_index = state[state_data].findIndex((userProfile) => compareIdentifiers(userProfile.identifier, action.identifier))
        let updateData = {}
        updateData[action.key] = action.value
        const data =  [
            ...state[state_data].slice(0,user_index), // before the one we are updating
            {...state[state_data][user_index], ...updateData},
            ...state[state_data].slice(user_index + 1), // after the one we are updating
        ];
        state[state_data] = data
        return state
      }
    case BLOCK_NOTIFICATION:
        var new_blocked_notification = state.blocked_notification
        new_blocked_notification.push(action.identifier)
        var new_unBlock_notification = state.unblocked_notification.filter((identifier) => !compareIdentifiers(identifier, action.identifier))
        return Object.assign({}, state, {
            blocked_notification: new_blocked_notification,
            unblocked_notification: new_unBlock_notification
        })
    case UNBLOCK_NOTIFICATION:
        var new_blocked_notification = state.blocked_notification.filter((identifier) => !compareIdentifiers(identifier, action.identifier))
        var new_unBlock_notification = state.unblocked_notification
        new_unBlock_notification.push(action.identifier)
        return Object.assign({}, state, {
            blocked_notification: new_blocked_notification,
            unblocked_notification: new_unBlock_notification
        })
    //-------------------Begin: Get People Matched Me--------------------------//
    case GET_MATCHED_USERS_REQUEST:
        return Object.assign({}, state, {
            isLoadingMatchedUser: true,
            is_page_loading: {...state.is_page_loading, matched_users: false},
            errorMessage: null
        })

    case GET_MATCHED_USERS_SUCCESS:
        const random_match_list = action.response.data.filter(user => user.is_random_matched === true)
        return Object.assign({}, state, {
            matched_users: action.response.data,
            haveLoadedMatch: true,
            random_match_users: random_match_list,
            is_page_loading: {...state.is_page_loading, matched_users: true},
            isLoadingMatchedUser: false,
        })

    case GET_MATCHED_USERS_FAILURE:
        notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
        return Object.assign({}, state, {
            isLoadingMatchedUser: false,
            errorMessage: action.error_message
        })
    case UPDATE_MATCHED_NOTIFICATION:
        let total_unread_message = 0;
        let number_chat_room_has_message = 0;
        const customer_support_user_id = action.notifications.filter((item) => getUserId(item.user_identifier) == process.env.CUSTOMER_SUPPORT_USER_ID);
        if (customer_support_user_id.length) {
            number_chat_room_has_message = 1;// 1 message from admin support
        }
        const matched_users = state.matched_users.map((user) => {
            const new_message_number = action.notifications.filter(
                item => compareIdentifiers(item.user_identifier, user.identifier)
            )
            .map(item2 => item2.number_new_message).reduce((a, b) => a + b, 0);
            user.new_message_number = new_message_number;
            total_unread_message += new_message_number;
            if (new_message_number) {
                number_chat_room_has_message ++;
            }
            return user;
        });
        const random_match_users = state.random_match_users.map((user) => {
            const new_message_number = action.notifications.filter(
                item => compareIdentifiers(item.user_identifier, user.identifier)
            )
            .map(item2 => item2.number_new_message).reduce((a, b) => a + b, 0);
            user.new_message_number = new_message_number;
            total_unread_message += new_message_number;
            if (new_message_number) {
                number_chat_room_has_message ++;
            }
            return user;
        });
        return Object.assign({}, state, {
            matched_users: matched_users,
            total_unread_message,
            number_chat_room_has_message,
            has_message_from_admin: customer_support_user_id.length ? true : false
        })

    case DECREASE_NOTIFICATION_NUMBER:
        const remaining_unread_message = state.total_unread_message - action.number_message;
        const matched_users2 = state.matched_users.map((user) => {
            if (compareIdentifiers(action.identifier, user.identifier)) {
                user.new_message_number = 0;
            }
            return user;
        })

        return Object.assign({}, state, {
            matched_users: matched_users2,
            total_unread_message: remaining_unread_message > 0 ? remaining_unread_message : 0
        })

    case START_CHAT:
        return Object.assign({}, state, {
            has_start_chat: true,
        })
    //-------------------End  : Get People Like Me--------------------------//

    case INCREASE_VISITANT_NUMBER:
        return Object.assign({}, state, {
            current_user: {...state.current_user, notification_number_visitants: state.current_user.notification_number_visitants + 1},
        })
    case VISITANT_REAL_TIME:
        var visitant_real_time = state.visitant_real_time
        visitant_real_time.push(action.identifier)
        return Object.assign({}, state, {
            visitant_real_time: visitant_real_time,
        })
    case PEOPLE_LIKE_ME_REAL_TIME:
        var new_people_liked_me_real_time = state.people_liked_me_real_time
        new_people_liked_me_real_time.push(action.identifier)
        return Object.assign({}, state, {
            people_liked_me_real_time: new_people_liked_me_real_time,
        })
    case CLEAR_NOTIFICATION_SUCCESS:
        return Object.assign({}, state, {
            current_user: {
                ...state.current_user,
                notification_number_visitants: action.extentions.type == NOTIFICATION_TYPE_VIEW_MY_PROFILE ? 0 : state.current_user.notification_number_visitants,
                notification_number_added_me_favorite: action.extentions.type == NOTIFICATION_TYPE_VIEW_WHO_ADD_FAVOURITE ? 0 : state.current_user.notification_number_added_me_favorite
            },
        })
    case INCREASE_NOTIFICATION_NUMBER:
        return Object.assign({}, state, {
            current_user: {
                ...state.current_user,
                notification_number_messages: action.notification_type == NOTIFICATION_TYPE_CHAT ? (action.reset ? 0 : state.current_user.notification_number_messages + 1) : state.current_user.notification_number_messages,
                notification_number_chat_app_messages: action.notification_type == NOTIFICATION_TYPE_CHAT_APP ? action.notification_num : (state.current_user.notification_number_chat_app_messages ? state.current_user.notification_number_chat_app_messages : 0),
                notification_number_likes: action.notification_type == NOTIFICATION_TYPE_LIKE ? (action.reset ? 0 : state.current_user.notification_number_likes + 1) : state.current_user.notification_number_likes,
                notification_number_added_me_favorite: action.notification_type == NOTIFICATION_TYPE_WHO_ADD_ME_FAVOURITE ? (action.reset ? 0 : state.current_user.notification_number_added_me_favorite + 1) : state.current_user.notification_number_added_me_favorite,
            },
        })

    case UPDATE_WELCOMEPAGE_COMPLETION_REQUEST:
        return Object.assign({}, state, {
            isFetching: true,
            error: false
        })
    case UPDATE_WELCOMEPAGE_COMPLETION_SUCCESS:
      return Object.assign({}, state, {
        current_user: {
            ...state.current_user,
            welcome_page_completion: action.extentions,
        },
        isFetching: false,
        error: false,
      })
    case UPDATE_WELCOMEPAGE_COMPLETION_FAILURE:
      notification(NOTIFICATION_ERROR, action.error_message)
      return Object.assign({}, state, {
        isFetching: false,
        error: true,
        errorMessage: action.error_message
      })
      case UPDATE_BIRTHDAY_WELCOME_PAGE:
        return Object.assign({}, state, {
          current_user: {...state.current_user, birthday: action.birthday}
        })
      case UPDATE_PAYMENT_PACKAGE_REQUEST:
          return Object.assign({}, state, {
              isFetching: true,
              error: false
          })
      case UPDATE_PAYMENT_PACKAGE_SUCCESS:
        return Object.assign({}, state, {
          pack: action.extentions,
          current_user: Object.assign({}, state.current_user, {
            user_type: 4
          }),
          isFetching: false,
          error: false,
        })
      case UPDATE_PAYMENT_PACKAGE_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
          isFetching: false,
          error: true,
          errorMessage: action.error_message
        })
      case UPDATE_ASKED_LOCATION_STATUS:
        return Object.assign({}, state, {
            current_user: {
                ...state.current_user,
                asked: action.status
            },
        })
      case UPDATE_LIKE_STYLE:
        return Object.assign({}, state, {
          like_style: action.style,
        })
      case UPDATE_LIKED_MY_PHOTO_MODAL:
        return Object.assign({}, state, {
          people_liked_my_photo_modal: action.modalInfo,
        })
      case UPDATE_FIRST_LIKE_STATUS:
        return Object.assign({}, state, {
                isFetching: false,
                current_user: {
                  ...state.current_user,
                  has_first_like: action.status,
                },
                errorMessage: null
        })
    case 'UPDATE_VOTED_USERS':
        const photo_index = state.current_user.user_pictures.findIndex(pic => pic.id == action.picture_id)
        if (photo_index !== -1) {
            const update_user_picture = state.current_user.user_pictures[photo_index];
            const voted_user_ids = update_user_picture.voted_user_ids.split(',');
            if (action.vote_type == 1) {
                // add vote_user_id into state
                voted_user_ids.push(action.vote_user_id.toString());
            }
            if (voted_user_ids.length && action.vote_type == 2) {
                // remove vote_user_id out of state
                const removeIndex = voted_user_ids.indexOf(action.vote_user_id);
                if( removeIndex > -1) {
                    voted_user_ids.splice(removeIndex, 1);
                }
            }
            update_user_picture.voted_user_ids = voted_user_ids.filter((item) => item != '').join(',');
        }
        return Object.assign({}, state, {
          current_user: state.current_user,
        })

    case ACTIVE_BOOST_RANK_SUCCESS:
        const coins_consume = action.response.data.ontop_24h.coins_consume;
        const coins_left = state.current_user.coins - coins_consume
        const begin_at = action.response.data.ontop_24h.begin_at
        const end_at = action.response.data.ontop_24h.end_at
        return Object.assign({}, state, {
            current_user: { ...state.current_user, coins: coins_left, ontop_24h: {begin_at: begin_at, end_at: end_at} }
        })

    case UPDATE_RANDOM_MATCH_SUCCESS:
        return Object.assign({}, state, {
            current_user: { ...state.current_user,
                            notification_number_messages: state.current_user.notification_number_messages + 1,
                            coins: state.current_user.coins - action.response.data.coins_consume,
                            permission: {
                                ...state.current_user.permission,
                                number_random_match_free: action.response.data.number_of_free_random_match,
                            }
                          }
        })

    case INCREASE_CURRENT_COIN:
        return Object.assign({}, state, {
            current_user: { ...state.current_user,
                coins: state.current_user.coins + action.coins,
            }
        })

    case COIN_CONSUME_SUCCESS:
        if (action.extentions === UNLOCK_WHO_LIKE_ME_PARAMS) {
          return Object.assign({}, state, {
              current_user: { ...state.current_user, coins: state.current_user.coins - UNLOCK_WHO_LIKE_ME_COST, },
          })
        }
        if (action.extentions === 'coin_consume_super_match' && state.current_user.gender === 'male') {
          return Object.assign({}, state, {
              current_user: { ...state.current_user, coins: state.current_user.coins - NUMBER_COINS_NEED_FOR_SUPER_LIKE, },
          })
        }
        return state

    case GET_WHO_ADD_ME_TO_FAVOURITE_REQUEST:
        return Object.assign({}, state, {
            isFetching: true,
            is_page_loading: {...state.is_page_loading, who_add_favourite: false},
            errorMessage: null
        })

    case GET_WHO_ADD_ME_TO_FAVOURITE_SUCCESS:
        return Object.assign({}, state, {
            who_add_favourite: action.response.data,
            is_page_loading: {...state.is_page_loading, who_add_favourite: true},
            isFetching: false,
        })

    case GET_WHO_ADD_ME_TO_FAVOURITE_FAILURE:
        notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
        return Object.assign({}, state, {
            isFetching: false,
            errorMessage: action.error_message
        })

    case GET_LAST_MESSAGES_SUCCESS:
        return Object.assign({}, state, {
            isFetching: false,
            chat_status: action.payload.data.data,
        })
    case UPDATE_USER_PERMISSION_SUCCESS:
        return Object.assign({}, state, {
            current_user: { ...state.current_user, permission: {
                ...state.current_user.permission, [action.extentions]: true
            }},
        })
    case SEND_GIFT_SUCCESS:
      return Object.assign({}, state, {
        current_user: {
          ...state.current_user,
          coins: state.current_user.coins - action.extentions.price,
        }
      })
    case GET_LUCKY_SPINNER_GIFT_SUCCESS:
      return Object.assign({}, state, {
          lucky_spinner_gifts: inventoryGifts(action.response.data)
      })
    case GET_USER_LICENCE_SUCCESS:
      return Object.assign({}, state, {
        current_user: {
          ...state.current_user,
          licence: action.response.data,
        }
      })
    case SAVE_LUCKY_SPINNER_RESULT_SUCCESS:
      return Object.assign({}, state, {
        lucky_spinner_gifts: {
          ...state.lucky_spinner_gifts,
          more_spin_gift: 0,
        }
      })
    default:
      return state
    }
}

export default profile;
