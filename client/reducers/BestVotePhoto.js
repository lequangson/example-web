import {
  UPDATE_BEST_VOTE_PHOTO_STATE, BEST_VOTE_PHOTO_FEMALE, BEST_VOTE_PHOTO_MALE, BEST_VOTE_PHOTO_FEMALE_PHOTO, BEST_VOTE_PHOTO_MALE_PHOTO
} from '../constants/CoinReward';
import { COIN_REWARD_RIBBON_BLUE, COIN_REWARD_RIBBON_PINK } from '../constants/Enviroment';
import { GET_BEST_PHOTOS_REQUEST, GET_BEST_PHOTOS_SUCCESS, GET_BEST_PHOTOS_FAILURE } from '../constants/photoStream'
import { isEmpty } from 'lodash';

function BestVotePhoto(state = {
  isFetching: false,
  error: false,
  banners: [],
  banners_image: [
    COIN_REWARD_RIBBON_BLUE,
    COIN_REWARD_RIBBON_PINK,
  ],
  modalInfo: [],
}, action) {
  switch (action.type) {
    case UPDATE_BEST_VOTE_PHOTO_STATE:
      return Object.assign({}, state, {
          modalInfo: action.modalInfo,
      })
    case GET_BEST_PHOTOS_SUCCESS:
      return Object.assign({}, state, {
        banners: action.response.data,
      })
    default:
      return state
  }
}

export default BestVotePhoto;