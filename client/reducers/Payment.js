import {
  PAYMENT_GET_PACKAGE_REQUEST, PAYMENT_GET_PACKAGE_SUCCESS,
  PAYMENT_GET_PACKAGE_FAILURE, PAYMENT_UPDATE_STATE,
  PAYMENT_PROCESS_REQUEST, PAYMENT_PROCESS_SUCCESS, PAYMENT_PROCESS_FAILURE,
  PAYMENT_CALLBACK_SUCCESS, PAYMENT_CALLBACK_FAILURE,
} from '../constants/Payment';
import { CDN_URL } from '../constants/Enviroment'

import notification from '../utils/errors/notification';
import { isEmpty } from 'lodash';

const defaultState = {
  isFetching: false,
  error: false,
  error_message: '',
  error_required: [],
  duplicated_code: [],
  duplicated_serial: [],
  package_type: null, // nap xu hoac monthly
  package_id: null,
  default_package_id: null,
  payment_method: 3, // mobile card, bank card or visa, default mobile car is 3
  provider_code: '', // viettel, vinaphone, mobile phone, TPBank, VIPBank..., mastercard, ..
  customer_name: '',
  customer_phone_number: '',
  mobile_card_info: [
    {
      input_id: 1,
      card_code: '',
      card_id: '',
      payment_info_id: '',
      payment_status: null
    }
  ], // su dung cho hinh thuc the cao
  packages: [], // cac goi 1 thang, 2 thang, 100xu, 200 xu
  shopping_cart: {},
  order: {},
  bank_codes: [
    {
      code: 'Vietcombank',
      name: 'Ngân hàng Ngoại Thương Việt Nam',
      image: `${CDN_URL}/general/Payment/vietcombank.png`
    },
    {
      code: 'Techcombank',
      name: 'Ngân hàng Kỹ thương Việt Nam',
      image: `${CDN_URL}/general/Payment/techcombank.png`
    },
    {
      code: 'MB',
      name: 'Ngân hàng Quân Đội MB',
      image: `${CDN_URL}/general/Payment/mbbank.png`
    },
    {
      code: 'Vietinbank',
      name: 'Ngân hàng Công thương Việt Nam Vietinbank',
      image: `${CDN_URL}/general/Payment/vietinbank.png`
    },
    {
      code: 'Agribank',
      name: 'Agribank - Ngân hàng Nông nghiệp & Phát triển Nông thôn',
      image: `${CDN_URL}/general/Payment/agribank.png`
    },
    {
      code: 'DongABank',
      name: 'Ngân hàng Đông Á',
      image: `${CDN_URL}/general/Payment/dongabank.png`
    },
    {
      code: 'Oceanbank',
      name: 'Ngân hàng Đại Dương Ocean Bank',
      image: `${CDN_URL}/general/Payment/ocean+bank.png`
    },
    {
      code: 'BIDV',
      name: 'Ngân hàng Đầu tư và Phát triển Việt Nam BIDV',
      image: `${CDN_URL}/general/Payment/bidv.png`
    },
    {
      code: 'SHB',
      name: 'Ngân hàng Sài Gòn – Hà Nội SHB',
      image: `${CDN_URL}/general/Payment/shb+bank.png`
    },
    {
      code: 'VIB',
      name: 'Ngân hàng Quốc tế VIB',
      image: `${CDN_URL}/general/Payment/vib+bank.png`
    },
    {
      code: 'MaritimeBank',
      name: 'Ngân hàng Hàng Hải Việt Nam Maritime Bank',
      image: `${CDN_URL}/general/Payment/maritime+bank.png`
    },
    {
      code: 'Eximbank',
      name: 'Ngân hàng Xuất Nhập Khẩu Việt Nam Eximbank',
      image: `${CDN_URL}/general/Payment/eximbank.png`
    },
    {
      code: 'ACB',
      name: 'Ngân hàng Á Châu ACB',
      image: `${CDN_URL}/general/Payment/acb+bank.png`
    },
    {
      code: 'HDBank',
      name: 'Ngân hàng Phát Triển Nhà TP. Hồ Chí Minh HDBank',
      image: `${CDN_URL}/general/Payment/hd+bank.png`
    },
    {
      code: 'NamABank',
      name: 'Ngân hàng Nam Á Nam A Bank',
      image: `${CDN_URL}/general/Payment/nam a bank.png`
    },
    {
      code: 'SaigonBank',
      name: 'Ngân hàng Sài Gòn Công Thương SAIGONBANK',
      image: `${CDN_URL}/general/Payment/saigon+bank.png`
    },
    {
      code: 'Sacombank',
      name: 'Ngân hàng Sài Gòn Thương Tín Sacombank',
      image: `${CDN_URL}/general/Payment/sacombank.png`
    },
    {
      code: 'VietABank',
      name: 'Ngân hàng Việt Á VietABank',
      image: `${CDN_URL}/general/Payment/viet a bank.png`
    },
    {
      code: 'VPBank',
      name: 'Ngân hàng Việt Nam Thịnh Vượng VPBank',
      image: `${CDN_URL}/general/Payment/vp+bank.png`
    },
    {
      code: 'TienPhongBank',
      name: 'Ngân hàng TMCP Tiên Phong TienPhongBank',
      image: `${CDN_URL}/general/Payment/tp bank.png`
    },
    {
      code: 'SeaABank',
      name: 'Ngân hàng Đông Nam Á SeABank',
      image: `${CDN_URL}/general/Payment/sea bank.png`
    },
    {
      code: 'PGBank',
      name: 'Ngân hàng Thương mại Cổ phần Xăng dầu Petrolimex PGBank',
      image: `${CDN_URL}/general/Payment/pg bank.png`
    },
    {
      code: 'Navibank',
      name: 'Ngân hàng TMCP Quốc dân',
      image: `${CDN_URL}/general/Payment/navi+bank.png`
    },
    {
      code: 'GPBank',
      name: 'Ngân hàng Thương mại TNHH MTV Dầu khí Toàn Cầu',
      image: `${CDN_URL}/general/Payment/gp+bank.png`
    },
    {
      code: 'BACABANK',
      name: 'Bac A Bank',
      image: `${CDN_URL}/general/Payment/bac+a+bank.png`
    },
    {
      code: 'PHUONGDONG',
      name: 'Ngân hàng Phương Đông',
      image: `${CDN_URL}/general/Payment/ocb bank.png`
    },
    {
      code: 'ABBANK',
      name: 'Ngân hàng TMCP An Bình',
      image: `${CDN_URL}/general/Payment/ab bank.png`
    },
    {
      code: 'SCBBank',
      name: 'Ngân hàng TMCP Sai Gon Bank',
      image: `${CDN_URL}/general/Payment/scb bank.png`
    },
    {
      code: 'DAIABANK',
      name: 'Ngân hàng TMCP Đại Á',
      image: `${CDN_URL}/general/Payment/daia+bank.png`
    },
    {
      code: 'LienVietPostBank',
      name: 'Ngân hàng TMCP Bưu Điện Liên Việt',
      image: `${CDN_URL}/general/Payment/lien viet bank.png`
    },
    {
      code: 'BVB',
      name: 'Ngân hàng TMCP Bảo Việt',
      image: `${CDN_URL}/general/Payment/bao+viet+bank.png`
    },
    {
      code: 'KienLongBank',
      name: 'Ngân hàng TMCP Kiên Long',
      image: `${CDN_URL}/general/Payment/kienlong+bank.png`
    }
  ]
};

function setPaymentDataLayer(order) {
  const dataParameters = {
    'Amount': order.amount? order.amount : '',
    'Coins': order.coins? order.coins : ''
  };
  dataLayer.push(dataParameters);
}

function clearPaymentDataLayer() {
  const dataParameters = {
    'Amount': '',
    'Coins': ''
  };
  dataLayer.push(dataParameters);
}

function Payment(state = defaultState, action) {
  switch (action.type) {
    case PAYMENT_GET_PACKAGE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false
      })
    case PAYMENT_GET_PACKAGE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        packages: action.response.data
      })
    case PAYMENT_GET_PACKAGE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      })
    case PAYMENT_UPDATE_STATE:
      const payment = {};
      payment[action.key] = action.value;
      return Object.assign({}, state, payment);
    case PAYMENT_PROCESS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false
      })
    case PAYMENT_PROCESS_SUCCESS:
      const order = (typeof action.response !== 'undefined' &&
        typeof action.response.data !== 'undefined' &&
        typeof action.response.data.order !== 'undefined') ?
        action.response.data.order : {};
      setPaymentDataLayer(order);

      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        order: order
      })
    case PAYMENT_PROCESS_FAILURE:
      const process_response = action.response.data;
      const mobile_card_info = state.mobile_card_info;
      if (typeof process_response.shopping_cart !== 'undefined') {
        const { shopping_cart_payment_infos } = process_response.shopping_cart;
        if (shopping_cart_payment_infos.length) {
          shopping_cart_payment_infos.map(detail_payment => {
            const update_mobile_card = mobile_card_info.find(card => card.card_id == detail_payment.mobile_card_serial)
            if (!isEmpty(update_mobile_card)) {
              update_mobile_card.payment_info_id = detail_payment.id;
              update_mobile_card.payment_status = detail_payment.payment_status;
            }
          })
        }
      }
      clearPaymentDataLayer();
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        shopping_cart: action.response.data
      })
    case PAYMENT_CALLBACK_SUCCESS:
      const paymentOrder = (typeof action.response !== 'undefined' &&
        typeof action.response.data !== 'undefined' &&
        typeof action.response.data.order !== 'undefined') ?
        action.response.data.order : {};
      setPaymentDataLayer(paymentOrder);

      return Object.assign({}, state, {
        error: false,
        order: paymentOrder
      })
    case PAYMENT_CALLBACK_FAILURE:
      clearPaymentDataLayer();
      return Object.assign({}, state, {
        error: false,
        error_message: action.response.data.error_message,
        shopping_cart: action.response.data,
      })
    default:
      return state
  }
}

export default Payment;