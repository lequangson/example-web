import {
  MATCH_QUESTION_UPDATE_STATE,
  ANSWER_MY_EXPLORER_QUESTION, ANSWER_OTHER_EXPLORER_QUESTION,
  GET_USER_QUESTIONS_REQUEST, GET_USER_QUESTIONS_SUCCESS, GET_USER_QUESTIONS_FAILURE,
  ANSWER_QUESTION_REQUEST, ANSWER_QUESTION_SUCCESS, ANSWER_QUESTION_FAILURE,
  CHOOSE_EXPLORER_QUESTION_REQUEST, CHOOSE_EXPLORER_QUESTION_SUCCESS, CHOOSE_EXPLORER_QUESTION_FAILURE,
  GET_MATCHED_ANSWERED_QUESTION_REQUEST, GET_MATCHED_ANSWERED_QUESTION_SUCCESS, GET_MATCHED_ANSWERED_QUESTION_FAILURE
} from '../constants/Match_Questions'

const defaultState = {
  isFetching: false,
  error: false,
  errorMessage: '',
  my_questions: [],
  other_questions: [],
  matched_answers: 0,
  isOpenMatchModal: false,
  match_info: {},
  isOpenHurdleNotification: false,
  hurdleInfo: {},
  hurdle_matched_answers: 0,
  update_answer: false,
  answers:[
    {
      title: 'Hoàn toàn chính xác',
      code: 1
    },
    {
      title: 'Cũng hơi đúng',
      code: 2
    },
    {
      title: 'Không đúng cũng chẳng sai',
      code: 3
    },
    {
      title: 'Không chính xác',
      code: 4
    },
    {
      title: 'Hoàn toàn không phải tôi',
      code: 5
    },
  ]
}

function Match_Questions(state = defaultState, action) {
  switch (action.type) {
    case MATCH_QUESTION_UPDATE_STATE:
      const current_state = state;
      current_state[action.key] = action.value;
      return Object.assign({}, current_state);
    case GET_USER_QUESTIONS_SUCCESS:
      let data = []
      if (action.extentions.identifier !== null) {
        data = action.response.data.map(q => {
          q.answer_code = null
          return q
        })
      }
      return Object.assign({}, state, {
        my_questions: action.extentions.identifier !== null ? state.my_questions : action.response.data,
        other_questions: action.extentions.identifier !== null ? data : state.other_questions,
      })

    case ANSWER_QUESTION_REQUEST:
      return Object.assign({}, state, {
        update_answer: false,
      })

    case ANSWER_QUESTION_SUCCESS:
      const current_question = action.extentions.type == 1 ? state.other_questions.find(q => q.id == action.extentions.question_id) : state.my_questions.find(q => q.id == action.extentions.question_id)
      current_question.answer_code = action.extentions.answer_code
      return Object.assign({}, state, {
        update_answer: true,
      })

    case CHOOSE_EXPLORER_QUESTION_REQUEST:
      return Object.assign({}, state, {
        update_answer: false,
      })

    case CHOOSE_EXPLORER_QUESTION_SUCCESS:
      const current_explorer_question = state.my_questions.find(q => q.id == action.extentions.question_id)
      current_explorer_question.is_explore_question = action.extentions.is_explore_question
      return Object.assign({}, state, {
        update_answer: true,
      })

    case GET_MATCHED_ANSWERED_QUESTION_SUCCESS:
      return Object.assign({}, state, {
        matched_answers: action.response.data.matched_answers,
      })
    default:
      return state
  }
}

export default Match_Questions;