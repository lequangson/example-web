import {
  SET_LIKE_INFO_REQUEST, SET_LIKE_INFO, UPDATE_FIRST_LIKE_MODAL, UPDATE_SIMILAR_USER_MODAL,
  GET_TEMPLATE_LIKE_MSG_REQUEST, GET_TEMPLATE_LIKE_MSG_SUCCESS, GET_TEMPLATE_LIKE_MSG_FAILURE, UPDATE_MATCH_MODAL,
} from '../constants/Like'
import notification from '../utils/errors/notification'

function Like(state = {
  isFetching: false,
  error: false,
  isLikeModalOpen: false,
  isLikeMessageModalOpen: false,
  isMatchModalOpen: false,
  isFirstLikeModalOpen: false,
  isSimilarUserModalOpen: false,
  isSuperLikeModalOpen: false,
  firstLikeInfo: {},
  likeFrom: {},
  likeTo: {},
  page_source: '',
  messageTemplate: [],
  matchInfo: {},
  firstLikeReceived: false,
  firstLikeSent: false,
}, action) {
  switch (action.type) {
    case SET_LIKE_INFO_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false,
      })
    case SET_LIKE_INFO:
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        isLikeModalOpen: action.isLikeModalOpen,
        isLikeMessageModalOpen: action.isLikeMessageModalOpen,
        likeFrom: action.likeFrom,
        likeTo: action.likeTo,
        page_source: action.page_source,
      })
    case GET_TEMPLATE_LIKE_MSG_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false,
      })
    case GET_TEMPLATE_LIKE_MSG_SUCCESS:
       return Object.assign({}, state, {
            messageTemplate: action.response.data,
            isLoaded: true,
            isFetching: false,
        })
    case GET_TEMPLATE_LIKE_MSG_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
      })
    case UPDATE_MATCH_MODAL:
      return Object.assign({}, state, {
        isMatchModalOpen: action.isMatchModalOpen,
        isFirstLikeModalOpen: false,
        error: false,
        matchInfo: action.matchInfo,
      })
    case UPDATE_FIRST_LIKE_MODAL:
      return Object.assign({}, state, {
        isFirstLikeModalOpen: action.isOpen,
        error: false,
        firstLikeSent: action.firstLikeSent,
        firstLikeReceived: action.firstLikeReceived,
        firstLikeInfo: action.firstLikeInfo,
      })
    case UPDATE_SIMILAR_USER_MODAL:
      return Object.assign({}, state, {
        isSimilarUserModalOpen: action.isOpen,
        error: false,
        similarUserInfo: action.similarUserInfo,
        likeFrom: {},
        likeTo: action.likeTo
      })
    default:
      return state
  }
}

export default Like;