import {
  GET_DAILY_REMIND_REQUEST, GET_DAILY_REMIND_SUCCESS, GET_DAILY_REMIND_FAILURE,
  SHOW_REMIND_MODAL, CLOSE_DAILY_REMIND_MODAL,
  SEND_3_LIKES, SEND_15_LIKES, SEND_20_LIKES,
  DAILY_ACCESS_CONSECUTIVE_DAYS, BIRTHDAY,
  UNLOCK_PEOPLE_LIKE_ME_REMIND, HAPPY_HOUR_REMIND,
} from 'constants/Daily_Remind';
import {
  SEND_LIKE_SUCCESS
} from 'constants/userProfile';
import { COIN_REWARD_URL, CDN_URL, RECOMMENDATION_PAGE } from 'constants/Enviroment';

const defaultState = {
  isFetching: false,
  error: false,
  isOpen: false,
  reward_coins: '0',
  campaign: {
    lucky_money: 0
  },
  reminds: [
    {
      name: SEND_3_LIKES,
      isActivated: false,
      reward_coins: '0',
      text: 'Gửi 20 lượt thích, có ngay <span class="txt-blue">20 xu</span>',
      button_text: 'Tiếp tục gửi thích',
      image: `${COIN_REWARD_URL}coin_reward_3likes.jpg`,
    },
    {
      name: SEND_15_LIKES,
      isActivated: false,
      reward_coins: '0',
      text: 'Vài like nữa thôi được <span class="txt-blue">20 xu</span> rồi!',
      button_text: 'Tiếp tục gửi thích',
      image: `${COIN_REWARD_URL}coin_reward_15likes.jpg`,
    },
    {
      name: SEND_20_LIKES,
      isActivated: false,
      reward_coins: '20',
      text: 'Ymeet.me tặng bạn <span class="txt-blue">20 xu</span> nè!',
      button_text: 'Nhận xu',
      image: `${COIN_REWARD_URL}coin_reward_20likes.jpg`,
    },
    {
      name: DAILY_ACCESS_CONSECUTIVE_DAYS,
      isActivated: false,
      reward_coins: '0',
      text: '',
      button_text: 'Nhận xu',
      image: '',
      notiText: ''
    },
    {
      name: BIRTHDAY,
      isActivated: false,
      reward_coins: '10',
      text: 'Ymeet.me tặng bạn <span class="txt-blue">10 xu</span> nè!',
      button_text: 'Phewwww!',
      image: `${COIN_REWARD_URL}coin_reward_birthday.jpg`,
    },
    {
      name: UNLOCK_PEOPLE_LIKE_ME_REMIND,
      isActivated: false,
      reward_coins: '0',
      text: 'Bạn chỉ còn <span class="txt-blue">3 ngày</span> cho tính năng này',
      button_text: 'Nhất trí!',
      image: `${CDN_URL}/general/3day-remind.png`,
    },
    {
      name: HAPPY_HOUR_REMIND,
      isActivated: false,
      reward_coins: '0',
      text: 'Đăng nhập Ymeet.me từ 8 - 10h mỗi tối thứ sáu. Trò chuyện thoải mái với người đã kết đôi',
      button_text: 'Nhất trí!',
      image: `${CDN_URL}/general/3day-remind.png`,
    }
  ]
}

function Daily_Remind(state = defaultState, action) {
  switch (action.type) {
    case GET_DAILY_REMIND_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false,
      })
    case GET_DAILY_REMIND_SUCCESS:
      let reminds = state.reminds;
      if (action.response.data &&
        action.response.data.birthday &&
        action.response.data.birthday.is_birthday) {
        const index = reminds.findIndex(item => item.name === BIRTHDAY)
        if (index >= 0) {
          reminds[index].isActivated = true;
          reminds[index].reward_coins = action.response.data.birthday.reward_coins;
        }
      }

      const specialMilestones = [3, 10, 31];
      if (action.response.data && action.response.data.daily_access) {
        const agg_days = action.response.data.daily_access.number_consecutively_day_access
        const isInMilestones = specialMilestones.indexOf(agg_days) > -1
        const isDivBy3 = agg_days % 3 == 0
        if (isInMilestones || isDivBy3) {
          const index = reminds.findIndex(item => item.name === DAILY_ACCESS_CONSECUTIVE_DAYS)
          if (index >= 0) {
            reminds[index].isActivated = true;
            const { reward_coins, next_target_days, next_target_coins } = action.response.data.daily_access;
            reminds[index].reward_coins = reward_coins
            if (isInMilestones) {
              reminds[index].text = `Ymeet.me tặng bạn <span class="txt-blue">${reward_coins} xu</span> nè!`
              if (next_target_days) {
                reminds[index].next_target_text = `${next_target_days} ngày liên tiếp bạn sẽ nhận thêm ${next_target_coins} xu`
              }
              reminds[index].image = `${COIN_REWARD_URL}coin_reward_${agg_days}days.jpg`
            } else {
              reminds[index].notiOnly = true
              reminds[index].notiText = `Bạn được ${reward_coins} xu vì đăng nhập đều đặn!`
            }
          }
        }
      }

      if (action.response.data &&
        action.response.data.unlock_people_liked_me_remind) {
        const index = reminds.findIndex(item => item.name === UNLOCK_PEOPLE_LIKE_ME_REMIND)
        if (index >= 0) {
          reminds[index].isActivated = true;
        }
      }

      /* Do not need to show now
      if (action.response.data &&
        action.response.data.happy_hours_in_day_remind) {
        const index = reminds.findIndex(item => item.name === HAPPY_HOUR_REMIND)
        if (index >= 0) {
          reminds[index].isActivated = true;
          reminds[index].text = action.extentions.gender === 'male' ? 'Đăng nhập Ymeet.me từ 8 - 10h mỗi tối thứ sáu. Trò chuyện thoải mái với người đã kết đôi' :
            'Đăng nhập Ymeet.me từ 8 - 10h mỗi tối thứ sáu. Tự do khám phá những người đã thích bạn';
        }
      }*/

      const activedIndex = reminds.findIndex(item => item.isActivated === true)

      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        isOpen: activedIndex >= 0,
        reward_coins: activedIndex >= 0 ? reminds[activedIndex].reward_coins : '',
        reminds: reminds,
        campaign: {
          ...state.campaign,
          lucky_money: action.response.data ? action.response.data.campaign.lucky_money ?
                                    action.response.data.campaign.lucky_money : 0 : 0
        }
      })

    case GET_DAILY_REMIND_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true,
      })

    case CLOSE_DAILY_REMIND_MODAL:
      let reminds1 = state.reminds;
      const selectedIndex = reminds1.findIndex(item => item.isActivated === true)
      if (selectedIndex >= 0) {
        reminds1[selectedIndex].isActivated = false;
      }
      const activedIndex1 = reminds1.findIndex(item => item.isActivated === true)
      return Object.assign({}, state, {
        isOpen: activedIndex1 >= 0,
        reward_coins: activedIndex1 >= 0 ? reminds1[activedIndex1].reward_coins : '',
        reminds: reminds1,
      })
    case SHOW_REMIND_MODAL:
      let reminds2 = state.reminds;
      const selectedIndex2 = reminds2.findIndex(item => item.name === action.modalName)
      if (selectedIndex2 >= 0) {
        reminds2[selectedIndex2].isActivated = true;
      }
      const activedIndex2 = reminds2.findIndex(item => item.isActivated === true)
      return Object.assign({}, state, {
        isOpen: activedIndex2 >= 0,
        reward_coins: activedIndex2 >= 0 ? reminds2[activedIndex2].reward_coins : '',
        reminds: reminds2,
        notiOnly: true
      })
    case SEND_LIKE_SUCCESS:
      if (action.extentions.page_source && action.extentions.page_source !== RECOMMENDATION_PAGE &&
        // (action.response.data.like_sent == '3' ||
        (action.response.data.like_sent == '15' ||
        action.response.data.like_sent == '20')) {
        let reminds3 = state.reminds;
        let modalName = '';
        // if (action.response.data.like_sent == '3') {
        //   modalName = SEND_3_LIKES;
        // }
        if (action.response.data.like_sent == '15') {
          modalName = SEND_15_LIKES;
        }
        else if (action.response.data.like_sent == '20') {
          modalName = SEND_20_LIKES;
        }
        const selectedIndex3 = reminds3.findIndex(item => item.name === modalName)
        if (selectedIndex3 >= 0) {
          reminds3[selectedIndex3].isActivated = true;
          if (modalName == SEND_20_LIKES) {
            reminds3[selectedIndex3].reward_coins = 20;
          }
        }
        const activedIndex3 = reminds3.findIndex(item => item.isActivated === true)
        return Object.assign({}, state, {
          isOpen: activedIndex3 >= 0,
          reward_coins: activedIndex3 >= 0 ? reminds3[activedIndex3].reward_coins : '',
          reminds: reminds3,
        })
      }
      else {
        return state
      }
    default:
      return state
  }
}

export default Daily_Remind;