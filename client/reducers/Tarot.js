import {
  TAROT_GET_PACKAGE_REQUEST, TAROT_GET_PACKAGE_SUCCESS,
  TAROT_GET_PACKAGE_FAILURE,  TAROT_PAGE_1, TAROT_PAGE_2, TAROT_PAGE_3, UPDATE_TOROT_PAGE, UPDATE_CONTENT_PAGE_3
} from '../constants/Tarot';
import { CDN_URL } from '../constants/Enviroment'
import { isEmpty } from 'lodash';


function Tarot(state = {
  isFetching: false,
  error: false,
  error_message: '',
  tarot_curent_page: 1,
  tarot_page : [
    {
      pageNunber: TAROT_PAGE_1,
      url : `${CDN_URL}/general/Tarot/Triangle.png`,
      text1: 'Chọn 1 câu hỏi',
      text2: 'Bạn muốn được giải đáp'
    },
    {
      pageNunber: TAROT_PAGE_2,
      url : `${CDN_URL}/general/Tarot/Triangle.png`,
      text1: 'Rút 1 lá bài',
      text2: 'Trước khi rút bài, hãy tĩnh tâm và <br /> suy nghĩ kĩ về câu hỏi bạn muốn được giải đáp'
    },
    {
      pageNunber: TAROT_PAGE_3,
      url : '',
      text1: 'Lá bài của bạn',
      text2: ''
    }
  ],
  tarot_card_codes: [
    {
      code: 'Cups01',
      name: 'Ace of Cups',
      image: `${CDN_URL}/general/Tarot/Cups01.jpg`,
      text: [
        'Lá này thường có nghĩa rằng tình yêu chính là cốt lõi của tình huống hiện tại. Nó có thể hoặc cũng không hẳn chỉ là tình yêu nam nữ. Hãy tìm kiếm những cách thức để bạn có thể bắt đầu một mối liên kết với những người khác. Xem xét xem  liệu đang có một ai đó mà bạn muốn tha thứ, hay một ai mà bạn muốn nhận được sự tha thứ từ họ hay không?',
        'Các bạn mới đang bắt đầu bước vào tình yêu với thật nhiều bỡ ngỡ và niềm vui. Hãy tận hưởng nó một cách chân thành nhất nhé!',
        'Hãy thử xem xét xem liệu bạn đang có một ai đó mà bạn muốn tha thứ, hay một ai mà bạn muốn nhận được sự tha thứ từ họ hay không? Bạn có thể bỏ qua cơn giận dữ và tìm thấy sự bình yên? Bạn có muốn bỏ đi tâm thế phòng thủ và biểu lộ cảm xúc thật của mình?'
      ]
    },
    {
      code: 'Cups02',
      name: '2 of Cups',
      image: `${CDN_URL}/general/Tarot/Cups02.jpg`,
      text: [
        'Lá này đại diện cho tình yêu chân chính, mối quan hệ cân bằng, trách nhiệm và hầu hết những gì mà chúng ta thường mong muốn khi đề cập đến tình yêu và mối quan hệ. Và nếu bạn đang tìm tình yêu cho mình thì hãy thư giãn đi, vì có vẻ như tình yêu sắp gõ cửa nhà bạn rồi đấy.',
        '2 of Cups chỉ cho thấy các nền tảng, các yếu tố thực tế, sự ổn định và tiềm năng cho tình yêu thật sự cũng như sự quyến rũ đang hiện hữu. Hai bạn sẽ có một mối quan hệ khá tuyệt vời.',
        'Hai bạn gặp trục trặc về tình yêu và không ai chịu ai. Hãy thử thoả hiệp và “đình chiến” trong một thời gian, dành thời gian nói chuyện để có thể kết nối với nhau nhiều hơn nhé.'
      ]
    },
    {
      code: 'Cups03',
      name: '3 of Cups',
      image: `${CDN_URL}/general/Tarot/Cups03.jpg`,
      text: [
        'Mặc dù 3 of Cups có thể mang nghĩa thông báo về một cuộc hội ngộ với một tình yêu đã mất; nhưng không phải lúc nào cũng thế. Nếu bạn còn độc thân, việc giải bài rất có thể là bạn có thể sẽ sớm gặp một tình yêu – một người thực sự phù hợp với “hình mẫu” mà bạn đang tìm kiếm.',
        'Mặc dù 3 of Cups có thể mang nghĩa thông báo về một cuộc hội ngộ với một tình yêu đã mất; nhưng không phải lúc nào cũng thế. Hoặc có thể, người ấy thực sự rất yêu và hợp với bạn về mọi mặt.',
        'Hai bạn cần ra ngoài đi chơi nhiều hơn, thậm chí là rủ cả hội bạn thân để có thể vui vẻ và thư giãn nhiều hơn. Đó là những gì tình yêu của hai người đang cần.'
      ]
    },
    {
      code: 'Cups04',
      name: '4 of Cups',
      image: `${CDN_URL}/general/Tarot/Cups04.jpg`,
      text: [
        'Liệu trái tim của bạn đang loại trừ tất cả mọi người khác đến với bạn hay không? Ai là người không thật sự trao cho bạn nhiều tình cảm hoặc gần như lúc nào cũng vắng mặt mỗi khi bạn cần? Lựa chọn thật kỹ trước khi quyết định tiến đến với một ai đó nhé!',
        'Bạn dần nhận ra có vẻ như người ấy không thực sự quan tâm đến bạn như đã nghĩ. Những giây phút khó khăn, chỉ có mình ban ở đó và tự đối đầu với mọi chuyện. Đây là lúc cần suy nghĩ lại về mối quan hệ này.',
        'Hai bạn không có sự quan tâm hay chăm sóc cho đối phương. Sự hờ hững chính là yếu tố cản trở hai người. Điều các bạn cần lúc này là khoảng thời gian tĩnh tâm, để thật sự cảm nhận xem tình cảm dành cho đối phương có đủ mạnh để tiếp tục không.'
      ]
    },
    {
      code: 'Cups05',
      name: '5 of Cups',
      image: `${CDN_URL}/general/Tarot/Cups05.jpg`,
      text: [
        'Một mối quan hệ liên quan đến tình yêu của bạn có thể vừa kết thúc hoặc có thể là sắp bước vào giai đoạn kết thúc. Bạn cần phải nhận ra rằng “bạn không mất tất cả.” Bạn có lẽ chưa sẵn sàng để bắt đầu một mối quan hệ mới đâu, nên hãy trao cho mình nhiều thời gian và không gian hơn hiện tại… rồi mới trở lại với cuộc sống yêu đương. ',
        'Mối tình này có vẻ không dẫn tới một kết thúc đẹp như bạn vẫn mong. Tuy nhiên, bạn phải hiểu rằng bạn không mất tất cả, chỉ là đó là người không phù hợp với bạn mà thôi.',
        'Mối quan hệ hiện tại dường như chỉ là cơ hội để bạn tìm thấy một mối quan hệ khác phù hợp hơn. Vì vậy, dù nó có sắp kết thúc, bạn cũng không cần quá buồn.'
      ]
    },
    {
      code: 'Cups06',
      name: '6 of Cups',
      image: `${CDN_URL}/general/Tarot/Cups06.jpg`,
      text: [
        'Đôi lúc lá này sẽ hàm ý về việc tái xuất hiện của một người yêu cũ trong cuộc sống của bạn. Hãy suy nghĩ thật kỹ trước khi tái bắt đầu một mối tình cũ nào đó. Cần nhớ lại lý do anh ấy/cô ấy trở thành “người cũ” của bạn.',
        'Nếu bạn đang yêu, thì lá bài nói rằng mối quan hệ này cần có sự tập trung của cả hai. Hãy nói về mục tiêu và ước mơ của hai bạn. Hãy khám phá và thử nghiệm những điều mới mẻ. Đừng để mình bị mắc két lại trong quá khứ.',
        'Nếu bạn đang yêu thì rất có thể là mối quan hệ của bạn đang bị bỏ ngỏ. Lá bài chỉ ra rằng cả hai cần tập trung để cùng nhau hướng tới tương lai. Hãy nói về mục tiêu và ước mơ của mình với đối phương, đừng để bản thân mắc kẹt trong quá khứ.'
      ]
    },
    {
      code: 'Cups07',
      name: '7 of Cups',
      image: `${CDN_URL}/general/Tarot/Cups07.jpg`,
      text: [
        'Có thể bạn đang nắm được nhiều lựa chọn tình cảm trong tay. Nhưng hãy rõ ràng và cụ thể về những gì bạn muốn trong một mối quan hệ, hãy đánh giá các lựa chọn đó một cách thấu đáo và kỹ lưỡng, hãy thành thật với tất cả những người liên quan (bao gồm cả bản thân bạn), rồi sau đó mới tiến tới.',
        'Hãy rõ ràng và cụ thể về những gì bạn muốn trong một mối quan hệ, có như vậy thì mối quan hệ của hai người mới có thể bền vững được.',
        'Hãy nhớ rằng việc yêu đương chớp nhoáng chỉ xảy ra ở giai đoạn đầu của một mối quan hệ mà thôi. Không có mối quan hệ nào có thể tồn tại cảm giác “chớp nhoáng” đó mãi được. Những mối quan hệ tốt kể cả mối quan hệ về mặt tinh thần đều sẽ tiến đến giai đoạn trầm lắng và ít hấp tấp hơn vào một lúc nào đó.'
      ]
    },
    {
      code: 'Cups08',
      name: '8 of Cups',
      image: `${CDN_URL}/general/Tarot/Cups08.jpg`,
      text: [
        'Đừng nhảy ngay vào một mối quan hệ mới; trước hết hãy cho mình một khoảng thời gian và không gian để đánh giá mọi việc sau khi vừa chấm dứt một tình yêu lâu dài.',
        'Bạn có thể nhận thấy rằng một mối quan hệ lâu dài sẽ đến hồi kết thúc. Nếu bạn chắc chắn về điều này, đừng chờ đợi thời điểm nào là đúng đắn để nói ra điều đó, bởi vì sẽ không bao giờ có “thời điểm tốt” để nói với ai đó rằng mối quan hệ giữa bạn và người ấy cần phải kết thúc.',
        'Đây không còn là một mối quan hệ có thể níu giữ được nữa rồi. Đừng chờ đợi thời điểm nào là đúng đắn để nói ra điều đó, bởi vì sẽ không bao giờ có “thời điểm tốt” để nói với ai đó rằng mối quan hệ giữa bạn và người ấy cần phải kết thúc.'
      ]
    },
    {
      code: 'Cups09',
      name: '9 of Cups',
      image: `${CDN_URL}/general/Tarot/Cups09.jpg`,
      text: [
        'Nếu bạn còn độc thân thì đây là thời điểm tuyệt vời để gặp gỡ mọi người. Hãy bước ra ngoài, hòa nhập, trò chuyện với những người mà bạn gặp được mà không cần phải kỳ vọng vào bất kỳ kết quả tiềm năng nào cả . Mục đích ở đây chỉ là để giải stress mà thôi.',
        'Lá này là một dấu chỉ rất tốt cho cuộc sống tình yêu của bạn. Nếu bạn đang trong một mối quan hệ tình cảm, thì mối quan hệ đó nhiều khả năng sẽ trở nên sâu sắc hơn, tốt hơn và ngọt ngào hơn.',
        'Thực ra hai bạn không có vấn đề gì cả, chỉ đơn giản là khi mọi thứ quá đầy đủ, người ta sinh ra sự chán chường mà thôi. Hãy tìm cách đổi mới cho tình yêu để có thể yêu nhau nhiều hơn nhé!'
      ]
    },
    {
      code: 'Cups10',
      name: '10 of Cups',
      image: `${CDN_URL}/general/Tarot/Cups10.jpg`,
      text: [
        ' Nếu bạn đang trong tâm thế chuẩn bị bắt đầu một mối quan hệ, thì chúc mừng bạn, con đường tình yêu trong những tháng tới sẽ vô cùng tốt đẹp và vui vẻ. Có khi, bạn chuẩn bị nhận được lời mời đi hẹn hò ngay ngày hôm nay đấy!',
        'Nếu bạn đang lo lắng về một mối quan hệ tình cảm và rút được lá bài này trong lượt trải bài, vậy thì đây là dấu hiệu tốt rằng bạn thật sự không cần lo lắng gì đâu. Bạn và người yêu bạn đều đang cùng nhìn về một phía.',
        'Giải pháp cho vấn đề của hai người ư? Có vẻ như các bạn cần một quyết định cuối cùng để có một đám cưới hạnh phúc'
      ]
    },
    {
      code: 'Cups11',
      name: 'Page of Cups',
      image: `${CDN_URL}/general/Tarot/Cups11.jpg`,
      text: [
        'Một người trẻ tuổi hơn (có thể là chỉ trẻ hơn bạn vài ngày) có thể là một phần trong viễn cảnh tình yêu của bạn. Hãy nhớ rằng tình yêu không phân biệt tuổi tác, cho nên đừng xem nhẹ bất cứ ai dù người đó có trẻ hơn bạn!',
        'Đừng bao giờ xem nhẹ những điều người ấy làm cho bạn vì đó là những điều thật tâm, xuất phát từ trong trái tim của họ. Hãy cho họ một cơ hội để thể hiện bản thân mình nhé!',
        'Như đã nói ở trên, lá Page cho thấy sự trẻ con trong tính cách của cả hai bạn. Điều đó cho thấy mối quan hệ của hai bạn không phải lúc nào cũng êm đềm và khó có thể tạo nên một tương lai tốt đẹp hơn. Nhưng cả hai bạn đều không quan tâm về điều này. Cả hai chỉ muốn sống hết mình cho từng khoảnh khắc và điều đó giúp hai bạn bỏ qua những khác biệt mà làm lành với nhau.'
      ]
    },
    {
      code: 'Cups12',
      name: 'Knight of Cups',
      image: `${CDN_URL}/general/Tarot/Cups12.jpg`,
      text: [
        'Đây là một lá bài cực kỳ tích cực khi xuất hiện trong lượt hỏi về tình yêu. Nếu bạn đang còn độc thân hay chưa có mối quan hệ tình cảm với ai, thì lá bài này thường hàm ý rằng một ai đó sắp gây được ấn tượng với bạn đấy.',
        'Nếu bạn đang trong một mối quan hệ, lá Knight này có thể hàm ý đến một lời cầu hôn / cuộc hôn nhân. Ít nhất là trong bối cảnh câu hỏi về tình yêu, lá bài này sẽ luôn mang những thông điệp tích cực, những điều mà bạn sẽ thích nghe. Hãy mở lòng khám phá những cảm giác của mình.',
        'Cần lưu ý rằng lá bài Tarot này không liên quan gì đến những cam kết lâu dài – thứ cần thiết để duy trì một mối quan hệ chính thức. Vậy nên, nếu hai bạn thực sự muốn tiến xa, cần phải cố gắng vun đắp thật nhiều cho mối quan hệ này.'
      ]
    },
    {
      code: 'Cups13',
      name: 'Queen of Cups',
      image: `${CDN_URL}/general/Tarot/Cups13.jpg`,
      text: [
        'Khi Queen of Cups xuất hiện trong một câu hỏi thuộc bối cảnh tình yêu, lá bài thường hàm ý về một viễn cảnh rất tích cực về một mối quan hệ. Nếu bạn đang đơn thân (chưa có người yêu) và lá bài xuất hiện, vậy thì đây là một dấu hiệu tốt khuyến khích bạn đi ra ngoài và kết bạn, bởi vì một mối quan hệ lãng mạn mới rất tích cực đang trong tầm tay của bạn.',
        'Khi Queen of Cups xuất hiện, bạn sẽ rất dễ được lôi cuốn vào chuyện yêu đương. Hãy giữ cân bằng và giữ vững quan điểm của bạn. Ngay cả khi bạn hào hứng với tình yêu mới, điều quan trọng là cần phải đảm bảo sao cho những việc khác vẫn tiếp diễn bình thường trong cuộc sống của bạn.',
        'Trong một trải bài mối quan hệ, lá bài Tarot này thể hiện về cảm xúc của bạn trong mối quan hệ. Hãy làm theo những điều mà linh cảm mách bảo nhé!'
      ]
    },
    {
      code: 'Cups14',
      name: 'King of Cups',
      image: `${CDN_URL}/general/Tarot/Cups14.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm tình yêu, lá bài này hàm ý một ai đó rất cân bằng và tình yêu sẽ sớm được dẫn dắt theo hướng của bạn. Lá King là tin mừng về tình yêu.',
        ' Với những câu hỏi về tình yêu, khi lá King này xuất hiện, những tin tức nhận được sẽ rất tốt. Bạn sẽ nhận được sự yêu thương ngập tràn từ người đó.',
        'King of Cups đại diện cho một quan điểm trung tính hoặc tích cực. Cũng với ý nghĩa như vậy trong trải bài quan hệ, nó cho thấy một mối quan hệ êm đềm không sóng gió. Nói cách khác, cả hai người biết cách xử lý các vấn đề một cách êm thấm và bởi vì vậy, không có tranh cãi nào xảy ra giữa họ. Nhìn chung, lá King of Cups xuôi là một lá bài tốt khi xuất hiện trong trải bài mối quan hệ.'
      ]
    },
    {
      code: 'Pents01',
      name: 'Ace of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents01.jpg`,
      text: [
        'Nếu bạn vẫn đang tìm kiếm tình yêu cuộc đời, vậy thì đã đến lúc bước ra ngoài và gặp gỡ mọi người, tìm kiếm những khởi đầu mới đầy hứa hẹn về khả năng trở thành tình yêu của bạn. Hãy sẵn sàng. Hãy nghĩ tích cực. Hãy tận hưởng niềm vui.',
        'Nếu bạn đang có một mối quan hệ tình cảm mà hai bên đã hứa hẹn với nhau về tương lai, vậy thì đây là thời điểm mối quan hệ đó có thể đạt đến tầm cao mới trong không bao lâu nữa.',
        'Để có thể giải quyết những vấn đề tồn đọng giữa hai người, bạn cần có sự tin tưởng tuyệt đối vào đối phương. Đây chính là yếu tố quan trọng nhất giúp mối quan hệ này bền vững và phát triển hơn.'
      ]
    },
    {
      code: 'Pents02',
      name: '2 of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents02.jpg`,
      text: [
        'Nếu như bạn đang tìm kiếm tình yêu thì đừng tự huyễn hoặc bản thân rằng bạn đã sẵn sàng nếu thật sự bạn vẫn chưa hề sẵn sáng cho một mối quan hệ nghiêm túc. Bạn không cần phải là một người hoàn hảo ngay từ đầu nhưng bạn sẽ cần phải sẵn sàng dành thời gian, không gian và sức khỏe cho sự lãng mạn của tình yêu.',
        'Nếu bạn đang trong một mối quan hệ tình cảm mà hai bên đã hứa hẹn cùng nhau, thì có khả năng bạn và người yêu đang gặp chút rắc rối trong việc dành thời gian và sức khỏe cho nhau. Hãy đảm bảo rằng mối quan hệ của các bạn vẫn luôn được duy trì ở mức độ ưu tiên cao trong cuộc sống hàng ngày; nếu không thì người ấy sẽ không còn ở đó khi bạn quay lại tìm đâu.',
        'Có vẻ như cả hai cùng đang bị mất cân bằng trong cuộc sống, đây cũng chính là lý do dẫn đến xích mích giữa hai người. Hãy ngừng việc ôm đồm mọi điều trong cuộc sống, biết buông bỏ những thứ không cần thiết để gìn giữ những điều quan trọng nhé!'
      ]
    },
    {
      code: 'Pents03',
      name: '3 of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents03.jpg`,
      text: [
        'Nếu bạn chưa có người yêu nhưng đang tìm kiếm, vậy thì lá bài này cho biết rằng bạn nhiều khả năng sẽ gặp người đó trong quá trình làm việc (của bạn hoặc của họ). Hãy luôn để ý và nhạy bén trong cuộc tìm kiếm này!',
        'Trong một trải bài về tình yêu, lá 3 of Pentacles cũng là một dấu hiệu tốt. Đặc biệt nếu bạn đã có ai đó để mà yêu thương và quan tâm, thì lá bài này đem lại thông điệp rằng người đó nhìn thấy ở bạn một ánh sáng vô cùng tích cực và rằng bạn là người hội đủ những tiêu chuẩn mà họ đang tìm kiếm, mặc dù tình yêu vẫn chưa phát triển. Vì vậy, hãy kiên nhẫn.',
        'Three of Pentacles cho thấy một sự việc đang bị trì hoãn. Trong tình yêu, điều này có nghĩa rằng mối quan hệ giữa hai bạn chưa thể thành. Cả hai người đều quá thụ động. Cả hai cần một khoảng lặng để nhìn nhận nghiêm túc mối quan hệ này trước khi tiếp tục đến với nhau.'
      ]
    },
    {
      code: 'Pents04',
      name: '4 of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents04.jpg`,
      text: [
        'Nếu bạn vẫn còn đang độc thân và nghĩ rằng bản thân mình đã sẵn sàng cho một mối quan hệ nghiêm túc, vậy thì hãy xác định tư tưởng rằng bạn cần bước chân ra khỏi “vùng an toàn” của mình để có thể khiến mối quan hệ đó đến với bạn. Hãy nhớ rằng tình yêu đích thực không rơi từ trên trời xuống bao giờ.',
        'Lá 4 of Pentacles là lá bài của sự sợ hãi, ngần ngại và về cơ bản thì những cảm giác này có liên quan đến tình yêu. Có thể là bạn hoặc người yêu bạn đang cố “kiểm soát” người kia quá nhiều, hoặc cũng có thể đang có một người nào đó khác đang muốn kiểm soát bạn (trong vần đề tình cảm). Việc kiểm soát hay “lụy tình” này rất không tốt, vì trong tình yêu đích thực mọi cặp đôi phải luôn dành cho nhau đủ không gian riêng, và người kia không bao giờ được phép “kiểm soát” người còn lại dù với bất cứ lý do gì.',
        'Dường như, bạn đang trong một mối quan hệ chưa rõ ràng và chưa đủ ràng buộc. Cả hai đều đặt bản thân mình lên cao hơn, đều chưa có sự để tâm và chăm lo đúng mức cho người còn lại.'
      ]
    },
    {
      code: 'Pents05',
      name: '5 of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents05.jpg`,
      text: [
        'Bạn cần nhớ rằng thế giới này không chỉ có một người duy nhất có thể sánh đôi cùng bạn, chia sẻ với bạn mọi đắng cay, ngọt bùi, và cùng bạn duy trì một mối quan hệ tình cảm tốt đẹp. Khi tình yêu đến đúng lúc với đúng người, tự thân nó sẽ diễn ra một cách tốt đẹp. Hãy mở rộng phạm vi suy nghĩ của mình theo hướng tích cực. Bạn không phải là kẻ không đáng được yêu!',
        'Đây không phải là một dấu hiệu của sự hạnh phúc khi xuất hiện trong câu hỏi tình cảm. Có thể bạn đang trao trái tim mình cho một người mà bạn cảm thấy không đối xử với bạn một cách phù hợp. Khả năng là bạn cần thôi ảo tưởng về người đó. Lá bài này gợi lên cảm giác bị bỏ rơi trong lạnh giá rất rõ ràng.',
        'Lá bài cho thấy bạn đang có một mối quan hệ luẩn quẩn, vô định. Dù thế nào đi nữa thì cũng rất khó để đào sâu tình cảm thực sự của mỗi người.'
      ]
    },
    {
      code: 'Pents06',
      name: '6 of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents06.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm người sẽ gắn kết với bạn cả cuộc đời, thì sự xuất hiện của lá bài này là thông điệp cho thấy trong không bao lâu nữa bạn sẽ có thể gặp gỡ một người mới, và đó là người có suy nghĩ rất tích cực, tử tế và quảng đại. Mối quan hệ mới sắp tới đó có thể do một người nào đó mà bạn biết giới thiệu. Hãy chắc chắn là mọi người xung quanh bạn biết rõ bạn đang “tìm” người yêu.',
        'Nếu bạn đã có một mối quan hệ tình cảm mà hai bên đã cam kết chuyện lâu dài, thì lá 6 of Pentacles là lời tiên đoán mối tình này của bạn sẽ vô cùng hạnh phúc, thoải mái, công bằng và tốt đẹp, ít nhất là trong một khoảng thời gian đáng kể. Hãy đón nhận những gì người yêu của bạn trao, dù là về khía cạnh vật chất hay tinh thần, đồng thời hãy dành lại cho người ấy những điều tốt đẹp nhất một cách công bằng, để mối quan hệ này luôn ở thế vững chắc.',
        'Nếu bạn không chắc chắn mối quan hệ sẽ có tương lai thế nào thì hãy nhớ rằng số 6 mang ý nghĩa của sự công tâm, vẻ đẹp và sự dịu dàng. Hơn thế nữa, nó còn được bảo trợ bởi thần Venus, nên nó không chỉ nói về sự công tâm mà còn là về tình yêu và sự lãng mạn. Một ý nghĩa khác của lá bài là cán cân, thể hiện rằng cả hai người đều đang ở cùng một hướng trong tình yêu.'
      ]
    },
    {
      code: 'Pents07',
      name: '7 of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents07.jpg`,
      text: [
        'Nếu bạn chưa có người yêu, thì có thể bạn cần phải nghĩ thoáng hơn và mở lòng cho những mối quan hệ tiềm năng khác. Không ai trong chúng ta chỉ có mỗi DUY NHẤT một người mà ta có thể gắn bó suốt cuộc đời. Đừng để bản thân rơi vào những cảm giác nghi ngại hay sợ hãi. Mọi thứ sẽ tốt lên thôi.',
        'Xét trong bối cảnh tình yêu, lá 7 of Pentacles là lời dự báo mọi thứ liên quan đến người bạn yêu sẽ diễn ra một cách tốt đẹp',
        'Nếu bạn nhìn vào người đàn ông trong Seven of Pentacles bạn sẽ thấy rằng anh ta đang ngắm nhìn các loại trái cây hoặc những đồng tiền mà không có vẻ gì là đang chăm sóc chúng cả. Nó thể hiện dấu hiệu bạn không chắc chắn liệu bạn muốn ở lại với nửa kia của bạn hay không, nhưng vẫn đang duy trì mối tình này. Và dường như bạn chỉ ngồi đó chứ không nỗ lực để cải thiện mối quan hệ này.'
      ]
    },
    {
      code: 'Pents08',
      name: '8 of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents08.jpg`,
      text: [
        'Nếu bạn đang trong giai đoạn tìm kiếm tình yêu, thì sự xuất hiện của lá bài này thường cho biết rằng bạn đang thực sự quá tập trung vào chuyện tiền bạc và lăn lộn trong cuộc sống đến mức khó có thể thực sự tạo ra đủ không gian cho một mối quan hệ mới. Hãy xem xét các ưu tiên của bạn và nhìn nhận bạn đang đầu tư thời gian và năng lượng của bản thân vào đâu.',
        'Nếu bạn hiện đang trong một mối quan hệ mà hai bên đã cam kết lâu dài, thì khối lượng công việc mà bạn hay người yêu của bạn đang phải gánh vác nhiều khả năng đang ảnh hưởng đến mối quan hệ của hai bạn. Hãy luôn chú ý dành đủ thời gian và không gian cho mối quan hệ của mình, không để dính đến công việc cá nhân, bằng không mọi thứ sẽ trở thành gánh nặng cho bạn.',
        'Một trong những từ khóa chính xác nhất với Eight of Pentacles là “xây dựng một cái gì đó có giá trị”. Trong một mối quan hệ, điều này có nghĩa là tạo ra sự tin tưởng và thậm chí đàm phán để giữ cho mọi thứ trôi chảy, và yên bình. Lá bài này thường xuất hiện trong các trải bài liên quan đến những bất đồng trong mối quan hệ. Việc của nó là báo cho họ biết rằng họ sẽ có thể vượt qua được tình trạng tồi tệ này.'
      ]
    },
    {
      code: 'Pents09',
      name: '9 of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents09.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm tình yêu, thì lá bài này cho bạn biết rằng có thể một người mới có khả năng đem đến cho bạn một tình yêu sâu đậm và ý nghĩa đang tìm đến với bạn. Hãy cười lên. Ai cũng thích nhìn thấy những cặp đôi hạnh phúc mà.',
        'Lá bài Nine of Pentacles là một lá bài tuyệt vời về tình yêu. Nếu bạn đang trong một mối quan hệ gắn kết với ai đó, vậy thì bạn có thể kỳ vọng rằng mối quan hệ này sẽ thăng tiến lên một nấc thang mới xét về độ thân mật và niềm vui.',
        'Nine of Pentacles có hình ảnh một người phụ nữ với một con chim đứng trong một khu vườn không chỉ có trái cây, mà còn có rất nhiều đồng tiền.Trông cách ăn mặc của bà ta cũng thật trang nhã. Tất cả điều này nói lên rằng bà đang có tất cả những gì mà bà mong muốn. Bà có tiền, đồ ăn thức uống, sự xa hoa và một không gian an toàn. Tuy nhiên, điều duy nhất mà người phụ nữ ấy thiếu đó chính là tình yêu.'
      ]
    },
    {
      code: 'Pents10',
      name: '10 of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents10.jpg`,
      text: [
        'Nếu bạn chưa có một mối quan hệ yêu đương nào, thì lá bài này có thể hàm ý rằng một tình yêu mới sẽ có thể bước vào cuộc sống của bạn rất nhanh chóng, đến mức khiến đầu óc bạn bị quay cuồng. Hãy luôn cởi mở.',
        'Lá bài Ten of Pentacle là một dấu hiệu rất hạnh phúc về tình yêu. Nếu hiện bạn đã có một mối tình đã hứa hẹn và cam kết, thì lá bài này có thể hàm ý rằng mối quan hệ tình cảm của bạn sẽ sớm tiến lên một mức độ mới; chung sống cùng nhau, đính hôn, kết hôn, sinh con... ',
        'Trong một mối quan hệ, những chi tiết này đại diện cho một sự đầu tư khôn ngoan bởi vì chúng liên quan đến tình yêu, hạnh phúc, an toàn, tầm nhìn về tương lai và chu đáo. Nói cách khác, nó cho thấy hai người xứng đáng ở bên nhau vì họ biết rằng họ phù hợp với nhau. Một ý nghĩa khác liên quan đến lá bài là sự gắn kết đang ngày càng lớn dần.'
      ]
    },
    {
      code: 'Pents11',
      name: 'Page of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents11.jpg`,
      text: [
        'Nếu hiện tại bạn chưa ràng buộc chuyện tình cảm, và cảm thấy như thể bạn không có cơ hội gặp gỡ được đúng người, vậy thì hãy xem xét cẩn thận những gì bạn đang giữ trong lòng mình. Bạn có thể vẫn còn vương vấn tình cũ, và có thể cần buông bỏ những ký ức xưa đi trước khi bạn có thể kỳ vọng mọi chuyện sẽ diễn biến tốt đẹp hơn.',
        'Nếu bạn đang trong một mối quan hệ tình cảm gắn kết khi lá bài này xuất hiện, thì sự xuất hiện của lá bài này có thể là dấu hiệu cho thấy cuộc tình này có đôi chút thiếu lãng mạn.',
        'Tuy không hàm ý một hồi chuông cảnh tỉnh, nhưng lá bài vẫn là lời kêu gọi bạn và người yêu bạn nhanh chóng hành động và thảo luận với nhau. Để tạo ra sự thay đổi và cải thiện mối quan hệ này, bạn cần phải hết sức chân thật về cảm giác của mình.'
      ]
    },
    {
      code: 'Pents12',
      name: 'Knight of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents12.jpg`,
      text: [
        'Nếu bạn đang tìm người yêu khi rút được lá bài này, thì lá bài chính là một dấu hiệu rõ ràng cho biết rằng bạn cần thay đổi những suy nghĩ và hành động theo lối mòn của mình và thử nghiệm những điều mới mẻ nếu bạn muốn gặp một người mới. Bạn cần phải đánh đổi điều gì đó để có được cái mình mong muốn.',
        ' Trong bối cảnh về tình yêu, là bài này hàm chỉ những trách nhiệm và nghĩa vụ của mỗi bên trong một mối quan hệ yêu đương. Tình yêu không phải lúc nào cũng chỉ có sự mê đắm và lãng mạn. Một tình yêu thực sự sẽ bao gồm sự sự sẵn lòng hợp tác cùng nhau để vượt qua các thử thách và xây dựng mối quan hệ được bền vững.',
        'Tình yêu không phải lúc nào cũng chỉ có sự mê đắm và lãng mạn. Một tình yêu thực sự sẽ bao gồm sự sự sẵn lòng hợp tác cùng nhau để vượt qua các thử thách và xây dựng mối quan hệ được bền vững. Làm cách nào bạn có thể đáp ứng tốt được các trách nhiệm của mình? Hãy cố gắng quan tâm đến đối phương nhiều hơn.'
      ]
    },
    {
      code: 'Pents13',
      name: 'Queen of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents13.jpg`,
      text: [
        ' Trong bối cảnh câu hỏi về tình yêu, Queen of Pentacles là một lá bài thiên về tính nữ và hàm chỉ định hướng, đặc biệt dành cho những người hiện chưa có sự gắn kết tình cảm với bất kỳ ai. Định hướng chiến thuật tìm kiếm tình yêu là một định hướng thiên về tính nữ. Đừng chỉ theo đuổi tình yêu, thay vào đó hãy làm cho bản thân mình sẵn sàng đón nhận tình cảm.',
        ' Khi thời điểm thích hợp đến, tình yêu sẽ tìm đến mà bạn không cần làm gì khác. Hãy luôn lạc quan, và tâm niệm rằng bạn đáng được yêu thương. Và với đối tượng này, chắc chắn bạn sẽ nhận được những điều mình xứng đáng được nhận.',
        'Queen of Pentacles là một người cực kỳ lý trí trong tình yêu và không hề muốn yêu đương một cách mù quáng nên họ sẽ muốn chắc chắn rằng mối quan hệ này có tương lai hay không. Sẽ có thời gian họ tìm cách thay đổi để mối quan hệ tốt lên, nhưng nếu thất bại, Queen of Pentacles sẽ không ngần ngại gì mà bỏ đi và không thèm nhìn lại đến một lần.'
      ]
    },
    {
      code: 'Pents14',
      name: 'King of Pentacles',
      image: `${CDN_URL}/general/Tarot/Pents14.jpg`,
      text: [
        'Lá King of Pentacles là một dấu hiệu tốt khi xuất hiện trong bối cảnh câu hỏi về tình yêu. Một ai đó có thể xuất hiện và thu hút sự chú ý của bạn ngay từ cái nhìn đầu tiên. Hãy cứ tiến lên theo tiếng gọi con tim, nhưng đừng cố tỏ ra là một người khác mà không phải là chính mình. Người này sẽ chỉ yêu chính con người bạn mà thôi.',
        'Nếu bạn đang trong một mối quan hệ gắn kết với một ai đó, vậy thì mối quan hệ này rất nhiều khả năng sẽ tiến triển lên một vị thế thoải mái hơn nữa (theo nghĩa ẩn dụ).',
        'Nếu bạn không cảm thấy hài lòng về bất cứ điều gì trong cuộc tình này, thì bạn cần phải nói ra điều đó để kỳ vọng vào một sự thay đổi. Đừng mong đợi nửa kia của mình sẽ hiển nhiên biết được suy nghĩ của bạn.'
      ]
    },
    {
      code: 'RWS_Tarot_00_Fool',
      name: 'The Fool',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_00_Fool.jpg`,
      text: [
        'Giống như trong hình, bạn đang là “The Fool”, một kẻ ngốc chuẩn bị bước chân vào hành trình tình yêu. Bạn sẽ đón nhận mối cơ duyên này với sự ngây thơ và không toan tính, cũng không lường trước được những khó khăn sẽ phải trải qua.',
        'Lá bài The Fool (thằng khờ) thể hiện giai đoạn đầu trong một chặng đường tình yêu dài. Mối quan hệ của hai người chỉ mới bắt đầu hoặc yêu nhau chưa được bao lâu, cũng có thể là đang tìm hiểu nhau hay vừa bước vào một thời kì mới nữa của tình yêu (ra mắt gia đình đối phương, hai bạn tạm xa nhau,...). Điều này khiến cho bạn cảm thấy ngỡ ngàng, lạ lẫm, nhiều đổi thay, nhiều thách thức mới mẻ phía trước và không thực sự chắc chắn về tình cảm của người mình yêu.',
        'Bạn cần phải xuôi theo diễn biến sự việc sắp xảy ra và chấp nhận mọi rủi ro một cách lạc quan và tích cực. Đây sẽ là chìa khoá giúp bạn tìm ra giải pháp hiệu quả cho vấn đề tình cảm của mình. như một đứa trẻ ngây thơ và lạc quan. Hãy cởi mở với những ý tưởng mới, có thể là sai lầm khi cứ bám lấy những phương pháp truyền thống, bởi tiềm thức đang chỉ dẫn bạn tới một con đường hoàn toàn khác.'
      ]
    },
    {
      code: 'RWS_Tarot_01_Magician',
      name: 'Magician',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_01_Magician.jpg`,
      text: [
        '3 tháng tới sẽ là khoảng thời gian để bạn chuẩn bị cho những kế hoạch chinh phục tình yêu của bản thân. Với khả năng khéo léo của mình, bạn sẽ vô cùng thận trọng để đảm bảo cho kết quả của mối tình này.',
        'The Magician cho thấy anh người yêu của bạn là một người lý trí và tận dụng cả lý trí lẫn tình cảm để phát triển tình yêu của hai bạn. Đồng thời, người yêu của bạn sẵn sàng chủ động và luôn cố gắng vun đắp cho mối quan hệ. Anh ấy hiểu rất rõ về bạn và sẽ cố gắng chiều chuộng bạn ở mức có thể.',
        'Hãy sử dụng những thế mạnh bản thân đang có để gây ân tượng. Nếu bạn muốn duy trì một mối quan hệ tình cảm hãy chịu khó giao tiếp và tham gia nhiều hoạt động với người đó. Bạn có thể tìm cách thức nào đó để nói ra những suy nghĩ, những gì mình mong muốn để cả hai được hiểu nhau hơn.'
      ]
    },
    {
      code: 'RWS_Tarot_02_High_Priestess',
      name: 'High Priestess',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_02_High_Priestess.jpg`,
      text: [
        'Đây là khoảng thời gian để bạn tự nhìn lại và có những cảm nhận sâu hơn vào tiềm thức của mình, cố gắng hiểu xem thực sự mình mong muốn điều gì ở người yêu tương lai. Đừng vội vàng tìm kiếm một nửa, hãy tự hỏi lại bản thân về những tiêu chuẩn và kỳ vọng với người ấy trước đã nhé!',
        ' Lá bài The High Priestess ám chỉ một người nội tâm và nhạy cảm. Dù vậy nhưng cảm xúc của bạn được duy trì rất tốt, không làm bạn vì sống tình cảm mà trở nên yếu đuối. Bạn biết cách điều chỉnh cảm xúc, tin tưởng vào bản thân mình. Yêu cầu của bạn về đối phương không cao vì bạn nghĩ rằng mình có đủ khả năng để hiểu được và kiểm soát đối phương. Bạn là người khôn ngoan, biết rõ bản thân cần gì muốn gì trong tình yêu nhưng cần cẩn thận vì sự tự tin quá mức có thể dẫn bạn đi sai đường.',
        'Bạn nên hạ bớt bức tường phòng thủ bản thân đang dựng lên trước đã. Có thể quá khứ đã để lại cho bạn nhiều vết thương chưa lành nên bạn mới thu bản thân mình nhiều đến như vậy. Hãy mở lòng ra, thế giới không tiêu cực như bạn suy nghĩ đâu. Khi bạn thoải mái bạn sẽ thấy mọi thứ đáng yêu và dễ chịu hơn nhiều.'
      ]
    },
    {
      code: 'RWS_Tarot_03_Empress',
      name: 'The Empress',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_03_Empress.jpg`,
      text: [
        'The Empress là hình ảnh một người mẹ, người phụ nữ lý tưởng trong gia đình, không phải trong tình yêu. Bạn luôn ân cần, quan tâm săn sóc hỗ trợ và chăm chút mọi thứ chu đáo đến từng kẽ răng, điều này giúp bạn ghi được điểm cao trong mắt mọi người xung quanh nhưng lại rất khó để bước vào tình yêu. Muốn mọi việc hoàn hảo dĩ nhiên không xấu, tuy nhiên có thể dẫn đến hậu quả bạn quên mất việc quan tâm chính bản thân mình. Hãy học cách yêu bản thân mình trước khi yêu một ai đó nhé!',
        'Khi xuất hiện trong một mối quan hệ, lá The Empress mang rất nhiều ý nghĩa. Đầu tiên, hình ảnh của lá bài có thể cho ta thấy rằng người phụ nữ đã mang thai hoặc đang có ý định đó. Nếu không thì có thể thấy rằng mọi thứ trong mối quan hệ này vẫn diễn ra rất suôn sẻ.',
        'Lá bài The Empress cho thấy bạn đang sống trong hoài niệm và quá khứ, luôn nhớ đến chuyện cũ, người cũ, những kỉ niệm đã qua. Điều này khiến cho bạn dễ đi vào vết xe đổ của quá khứ. Những hiểu lầm, gây gổ, xích mích với người yêu đều xuất phát từ những quyết định bằng cảm tính mà ít suy nghĩ lý trí của bạn. Hơn nữa, bạn dễ bị tác động bởi những người xung quanh làm cho những quyết định của bạn dẫn đến mâu thuẫn, thiếu sáng suốt. Tuy nhiên, vì người yêu của bạn là người giàu tình cảm, nên sẽ luôn chăm sóc, yêu thương và trân trọng bạn.'
      ]
    },
    {
      code: 'RỨ_Tarot_04_Emperor',
      name: 'The Emperor',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_04_Emperor.jpg`,
      text: [
        'The Emperor là người có quyền lực tối cao, dĩ nhiên họ sẽ không chấp nhận người yêu của mình vượt trội. Tính cách gia trưởng sẽ khiến người ta chạy mất dép. Họ có đủ kiêu ngạo để nghĩ rằng mọi người xung quanh phải phủ phục dưới chân và cầu cạnh tình yêu của mình. Thật là sai lầm! Tình yêu là ngang hàng, là hỗ trợ nhau, là cùng nhau tiến lên, không phải sự thuần phục. Hoàng Đế cao cao tại thượng, cũng sẽ vô cùng cô độc. Hãy học cách nhún nhường trước khi học yêu.',
        'Trong một mối quan hệ, lá bài thường báo một tín hiệu tốt. Lí do chính là lá the Emperor nhấn mạnh vào từ “ở đây”.  Anh ta vẫn ở lại, vẫn yêu tha thiết và dành rất nhiều sự quan tâm cho người phụ nữ của mình. Dù phải lưu ý một chút rằng lá Emperor thể hiện sự kiểm soát, điều khiển của người đàn ông. Vậy nên với câu hỏi rằng khi nào chuyện hẹn hò này sẽ đi đến một mối quan hệ nghiêm túc và chính thức? Câu trả lời không nằm ở bạn, mà khả năng cao lại do đối phương quyết định.',
        'Điều bạn cần bây giờ sự quyết liệt và kỷ luật. Tình yêu là cảm xúc nhưng không có nghĩa bạn có thể khiến nó rối tung lên như vậy. Cần rất nhiều sự chín chắn và nghiêm túc để mối quan hệ này có khả năng tiếp tục.'
      ]
    },
    {
      code: 'RWS_Tarot_05_Hierophant',
      name: 'Hierophant',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_05_Hierophant.jpg`,
      text: [
        'Bạn là người chuẩn mực, truyền thống, kể cả trong tình yêu cũng vậy. Truyền thống với bạn là những tinh hoa được tích luỹ lâu đời với giá trị vô giá nhưng không vì vậy mà bạn chỉ chăm chăm vào nó. Truyền thống khác với quy củ. Truyền thống mà lá Hierophant hướng tới là mối quan hệ phù hợp với các chuẩn mực đạo đức xã hội. Hãy lựa chọn những đối tượng có xu hướng như vậy.',
        'Cả hai bạn đều đặt ra những luật lệ, quy tắc của riêng mình. Điều này đúng cả khi đôi bên đồng ý với nhau việc trở thành người yêu hoặc vợ chồng. Lá bài Hierophant gắn với truyền thống, mà theo chuẩn mực xã hội là khi việc quan hệ tình dục diễn ra giữa vợ và chồng. Thế nên, khi lá bài xuất hiện trong trường hợp cặp đôi không có sợi dây ràng buộc nào giữa họ, nó cho thấy họ sẽ làm “chuyện vợ chồng” với nhau, tuân theo một quyết định riêng giữa họ mà không có bất cứ phán xét nào cả.',
        'Chuyện tình cảm của hai bạn thực chất vẫn đang tốt đẹp nhưng có vẻ hơi thiếu sự mới mẻ. Bạn nên cư xử theo cách mà mình nghĩ, hãy làm điều gì đó lạ lẫm và khác thường để có thể cải thiện tình yêu của bạn.'
      ]
    },
    {
      code: 'RWS_Tarot_06_Lovers',
      name: 'The Lovers',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_06_Lovers.jpg`,
      text: [
        'Con người ta thường ít kiên nhẫn khi theo đuổi một mối tình nên vội vàng sống, vội vàng yêu. Yêu thì dễ nhưng để có được một tình yêu thực sự thì khó. Tuy ta thường nghe rằng tình yêu vượt lên mọi rào cản, vượt xa mọi ranh giới hiện thực nhưng trên đời đếm được mấy mối tình như vậy. Có chăng những cuộc tình đó cũng thật ngắn ngủi ngủi, vụt sáng như ánh sao băng trên bầu trời rồi nhanh chóng lụi tàn. Tình yêu của Lovers không như vậy. Bạn mong chờ một tình yêu thực sự, lâu dài và bền vững. Vậy nên bạn không hề vội vàng mà chậm rãi chờ đợi một người thích hợp.',
        'Dấu hiệu liên kết với lá bài The Lovers là chòm sao Song Tử (Gemini). The Lovers tượng trưng cho sự hoàn hảo, sự hài hòa và hấp dẫn lẫn nhau. Nghĩa là cả hai đều tin tưởng vào nửa kia của mình, điều đó là động lực giúp cho họ có thể vượt qua mọi khó khăn, trở ngại để cùng nhau nắm tay bước tiếp trong cuộc sống. The Lovers cũng có hàm ý về kết giao tình dục vô cùng mạnh mẽ, sự kết hợp về thể xác, khao khát tình dục lẫn nhau vượt xa sự thỏa mãn và ham muốn tức thời để gợi đến niềm đam mê và khát khao mãnh liệt tồn tại giữa hai người đang yêu nhau.',
        'Các bạn có một tình yêu rất đẹp, đừng đánh mất nó chỉ vì những yếu tố bên ngoài tác động. Hãy yêu nhau vì chính bản thân đối phương chứ không vì điều gì khác. Hãy tận dụng mọi khoảnh khắc để ở bên nhau và trò chuyện, tâm sự. Càng gần đối phương, bạn sẽ càng cảm thấy mọi việc đều thật dễ dàng.'
      ]
    },
    {
      code: 'RWS_Tarot_07_Chariot',
      name: 'The Chariot',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_07_Chariot.jpg`,
      text: [
        'The Chariot mang bản tính thích chinh phục. Mục tiêu càng khó thì họ càng hứng thú. Tính hiếu thắng mang bản tính phá hoại, đặc biệt trong tình yêu. Họ chỉ lo tính toán hơn thua mà quên đi cái mình tìm là một tình yêu thực sự, không  phải để tính toán chi li. Hãy bỏ qua những suy nghĩ thực dụng và để tâm vào điều trái tim mình thật sự muốn nhé!',
        'The Chariot là lá bài liên kết với chòm sao Cự Giải (Cancer). Ý nghĩa của lá bài trong tình yêu tức là khi hai bạn mới đến với nhau, cả hai luôn quấn quýt bên nhau như hai cực nam châm trái dấu hút nhau mạnh mẽ. Hai bạn là hai nửa bổ sung cho nhau để trở thành một thể hoàn hảo.',
        'Sự khác biệt giữa hai người dễ dẫn đến xung đột. Nếu không muốn mất đối phương, bạn phải tìm cách giải quyết các mâu thuẫn ấy thật nhẹ nhàng và nhanh chóng. Các bạn gắn bó với nhau một thời gian dài và xuất hiện dấu hiệu "lười yêu". Bạn thường có suy nghĩ yêu nhau là do duyên số, vậy nên đôi lúc khiến người mình yêu cảm thấy bị bỏ rơi. Bạn phải xác định rõ ràng và nghiêm túc mối quan hệ của bạn và người mình yêu nếu muốn tình yêu đó được bền lâu và mãi mãi.'
      ]
    },
    {
      code: 'RWS_Tarot_08_Strength',
      name: 'Strength',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_08_Strength.jpg`,
      text: [
        'Bạn mong muốn một tình yêu mà trong đó, đối phương là người đủ vững chãi để bạn có thể dựa dẫm hẳn vào mọi mặt. Tuy nhiên trên đời làm gì có ai hoàn hảo, sự rụt rè quá mức sẽ làm mất đi cơ hội nắm lấy tình yêu của cuộc đời. Hãy thử thay đổi bản thân bằng cách trở nên độc lập hơn, và tự đứng lên đi tìm lấy cơ hội tình yêu cho bản thân nhé!',
        'Lá Strength trong tình yêu, ngược hẳn với tên gọi, là một người khá yếu đuối. Bạn mong muốn một tình yêu mà trong đó, đối phương là người đủ vững chãi để bạn có thể dựa dẫm hẳn vào mọi mặt. Hãy thử thay đổi bản thân bằng cách trở nên độc lập hơn, đôi khi cuộc đời lại thích trêu ngươi chúng ta như vậy, các chàng trai với bản tính thích chinh phục lại nổi lên ham muốn bao bọc một cô gái như vậy thì sao. Và ngược lại, các bạn trai hoàn toàn có thể thể hiện sự yếu đuối và nhạy cảm của mình với người con gái mình yêu, để cô ấy có thể hiểu bạn hơn.',
        'Tình yêu của bạn rồi sẽ tốt đẹp cả thôi. Quan trọng là cả hai đều cần nỗ lực và chủ động để hàn gắn những bất đồng. Đây sẽ là yếu tố vô cùng quan trọng quyết định cho việc tiếp tục hay kết thúc trong mối quan hệ của hai người.'
      ]
    },
    {
      code: 'RWS_Tarot_09_Hermit',
      name: 'Hermit',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_09_Hermit.jpg`,
      text: [
        'Lá Hermit tượng trưng cho một con người cô độc. Họ chỉ muốn sống một cuộc đời độc thân, thoát khỏi những vấn đề phức tạp của tình yêu, hôn nhân và gia đình. Tình yêu đối với họ không có nhiều ý nghĩa và họ cũng không cho rằng chúng quá quan trọng. Luôn tự tách mình ra khỏi mọi người, họ dành nhiều thời gian để thích thú với sự tự do hiện tại của mình. Có lẽ Hermit như một cơn gió, thà cô đơn chứ không thể chịu nổi sự trói buộc của tình yêu.',
        'Lá Hermit tượng trưng cho sự cô độc. Nếu hai bạn đang trong một mối quan hệ thì có vẻ như đây là mối quan hệ không có mấy ý nghĩa với đối phương hoặc với bạn. Cả hai thường có xu hướng tự tách nhau để có thể tự do làm điều mình thích. Dường như, tình yêu này chỉ như một cơn gió, không thể buộc cả hai vào với nhau.',
        'Nếu đang có một mối quan hệ cam kết rõ ràng, thì có thể là hai người đang đi theo những hướng khác nhau. Hãy dùng cả lí trí để suy nghĩ xem mối quan hệ ngày trước có đủ để bạn đánh đổi tình yêu hiện tại của mình không nhé.'
      ]
    },
    {
      code: 'RWS_Tarot_10_Wheel_of_Fortune',
      name: 'Wheel of Fortune',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_10_Wheel_of_Fortune.jpg`,
      text: [
        'Wheel of Fortune tượng trưng cho vòng quay của số phận, nghĩa là bạn chẳng thể làm gì ngoài việc tiếp nhận những điều mà cuộc sống đem tới. Nếu bạn từng đổ vỡ trong quá khứ, hãy ngưng nuối tiếc và tiếp tục sống với hiện tại. Còn nếu bạn vẫn đang lẻ bóng, đây có thể là một dấu hiệu tốt cho thấy trong 3 tháng tới, thần tinh yêu sẽ gõ cửa nhà bạn một cách bất ngờ đấy!',
        'Còn đầy những điều chưa chắc chắn trong mối quan hệ của hai bạn, khả năng cao là lịch sử tình yêu sẽ bị lặp lại. Mặt khác, đây là báo hiệu cơ hội lớn để đạt bước tiến mạnh mẽ trong mối quan hệ giữa hai người.',
        'Có thể mối quan hệ hiện tại cần được “nói chuyện nghiêm túc” vì nó không còn đáp ứng mong muốn của bạn nữa. Điều này không có nghĩa là mối quan hệ hiện tại của bạn đã đi tới hồi kết. Hãy mở lòng để người ấy có thể hiểu về cảm xúc và suy nghĩ của bạn hơn.'
      ]
    },
    {
      code: 'RWS_Tarot_11_Justice',
      name: 'Justice',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_11_Justice.jpg`,
      text: [
        ' Lá Justice chỉ ra bạn rất nghiêm túc khi tìm kiếm đối tượng cho bản thân, vì vậy việc này mất kha khá thời gian. Bạn dự định tình yêu kế tiếp sẽ đi thẳng đến vấn đề hôn nhân. Trong đầu bạn luôn mường tượng ra hình ảnh "ngôi nhà và những đứa trẻ", tuy vậy không có nghĩa là bạn vội vã yêu. Bạn vẫn kiên trì chờ đợi người phù hợp sẽ cho bạn một mái ấm gia đình mà bạn hằng mong đợi. Việc yêu đương nhăng nhít đối với bạn bây giờ không còn phù hợp nữa.',
        'Đây là một tình yêu lý trí, cả hai đều đạt được sự cân bằng. Nhưng tình yêu là một cảm xúc, không phải một suy nghĩ có thể điều chỉnh. Và rất có thể một trong hai đang trở nên quá tải vì cố gắng làm hài lòng người kia. Hãy thoải mái và để tình yêu được nói lên suy nghĩ của nó.',
        'Nếu bạn đang tự hỏi không biết mình và người yêu có đi tiếp với nhau được không thì có thể câu trả lời sẽ là: đường ai nấy đi. Trong lòng bạn đã rõ suy nghĩ này rồi, chỉ là bản thân chưa đủ dũng cảm để nói ra mà thôi.'
      ]
    },
    {
      code: 'RWS_Tarot_12_Hanged_Man',
      name: 'Hanged Man',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_12_Hanged_Man.jpg`,
      text: [
        'Bạn đang khao khát bắt đầu mối quan hệ với người trong lòng, tuy nhiên đối phương có vẻ không mấy quan tâm cho lắm. Tình cảm này chỉ đến từ một phía, một người yêu đơn phương. Nghị lực của người yêu đơn phương bao giờ cũng thật phi thường và đáng khâm phục. Đơn phương là những đau đớn âm ỉ tê tái thấm sâu vào tiềm thức mà không thể nói thành lời, là sự chờ đợi thầm lặng trong vô vọng. Dẫu vậy, chỉ cần một lời nói, một ánh nhìn, một câu hỏi han bâng quơ cũng đủ khiến ta như bay lên chín tầng mây.',
        'Bạn đang hy sinh quá nhiều cho mối quan hệ, thường xuyên là người cho đi nhiều hơn là nhận lại. Mối quan hệ này có thể không hề đẹp như những gì bạn đang mộng tưởng.',
        'Một lần nữa, bạn cần học cách buông bỏ. Ví dụ, buông bỏ người mà bạn không chạm tới được có thể sẽ tạo cơ hội cho người phù hợp xuất hiện với bạn. Nhưng đó chỉ là một ví dụ. Rất có thể bạn cần buông bỏ cách nhìn nhận đối với một mối quan hệ mà bạn cho rằng “mình chẳng thể hạnh phúc nếu thiếu anh ấy/cô ấy. Nếu bạn đang có mối quan hệ lâu năm, có thể hai bạn đang ở ngã ba đường. Đừng quá cố gắng níu kéo một thứ không còn là của bạn.'
      ]
    },
    {
      code: 'RWS_Tarot_13_Death',
      name: 'Death',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_13_Death.jpg`,
      text: [
        'Hãy thả lỏng mình, mở toang cánh cửa cũ kỹ khép kín trái tim bao lâu nay để đón nhận luồng khí mới tràn đầy sức sống. Thử chấp nhận một vài chàng trai đáng yêu vẫn luôn âm thầm quan tâm bạn bao lâu nay và bạn sẽ có được cảm giác ngây ngất như một bà hoàng. Đã đến lúc thay đổi và sẵn sàng đón nhận hạnh phúc mà bạn luôn luôn xứng đáng.',
        'Khi lá bài The Death xuất hiện, có vẻ chuyện tình cảm của bạn đã đi đến đoạn đường cuối cùng của cuộc hành trình. Bạn cần để lại quá khứ ở phía sau và đưa tay nắm lấy những cơ hội trước mắt. Trong một số trường hợp, lá bài đánh một dấu chấm hết cho mối quan hệ. Tuy nhiên, bạn cũng cần nhìn các lá bài xung quanh vì nó cũng có thể chỉ báo hiệu sẽ có sự thay đổi lớn và câu chuyện sẽ rẽ sang một chiều hướng mới mẻ và tích cực hơn.',
        ' Hãy nói chuyện thẳng thắn, nếu cả hai không thể thay đổi thì có lẽ là đến lúc bỏ nó đi rồi. Không phải mọi mối quan hệ đều có thể “sửa” được. Việc cố chấp sẽ chỉ khiến bạn khổ đau. Nếu đủ can đảm, bạn sẽ tìm được một người phù hợp hơn.'
      ]
    },
    {
      code: 'RWS_Tarot_14_Temperance',
      name: 'Temperance',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_14_Temperance.jpg`,
      text: [
        'Trong mọi trường hợp, bạn luôn tìm cách để giữ được sự bình tĩnh. Sự cố gắng trong việc dung hoà tất cả mọi thứ rất tốt, nhưng không phải tất cả đều dễ dàng như vậy. Bạn trì hoãn việc yêu đương vì luôn tìm cách để tình yêu không gây ra ảnh hưởng quá lớn đến cuộc sống hiện tại. Dễ dàng bị ảnh hưởng khi có bất kỳ một thay đổi nhỏ là lý do khiến bạn trốn tránh tình yêu. Bạn cần một chút can đảm để dám tiến lên khám phá những cung bậc cảm xúc thú vị của tình yêu.',
        'Bạn và người yêu có sự thu hút với đối phương vô cùng mãnh liệt nhưng cần thêm sự hỗ trợ và thoả thuận với nhau để không phải xung đột về sự khác biệt giữa hai người. Cả hai đều biết cân bằng vừa phải, tạo được cảm hứng cho đối phương.',
        'Bạn cần có sự bình tâm trong chính bản thân mình trước khi đạt được bình yên trong mối quan hệ của mình. Hãy cố gắng tha thứ cho những lỗi lầm trong quá khứ của bản thân. Nếu người kia có thể chờ bạn trải qua giai đoạn này, sẽ là một tín hiệu rất tốt cho mối quan hệ của hai người. Còn nếu không, đấy là lúc bạn cần rời xa người ấy.'
      ]
    },
    {
      code: 'RWS_Tarot_15_Devil',
      name: 'Devil',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_15_Devil.jpg`,
      text: [
        'Tình dục là nhu cầu thiết yếu của con người, cũng như ăn, ngủ, thở, …Tình dục là sự thăng hoa của tình yêu lên một cung bậc mới đầy cảm xúc, nhưng tình dục mà không có tình yêu thì chỉ là một thứ phàm tục trần trụi. The Devil như một người chỉ chăm chăm vào sự thỏa mãn thể xác của bản thân, trầm luân trong ngọn lửa tình dục. Đối tượng mà bạn đang kiếm tìm có thể là bất kỳ ai và tình một đêm sẽ là một lựa chọn tốt.',
        'Ngay ấn tượng đầu tiên, chúng ta thấy một người đàn ông và một người phụ nữ khỏa thân, bị xiềng xích ở cổ bởi một con quỷ to lớn phía bên trên. Vì thế, dù lá bài này có thể chỉ một mối quan hệ ngược đãi vốn bắt nguồn từ một nhu cầu quyền lực và kiểm soát, thì nạn nhân vẫn có thể thoát khỏi tình huống này nếu như họ có thể nhận ra được điều đó.',
        'Rất có thể bạn và cả người ấy đều đang cảm thấy tù túng trong mối quan hệ hiện tại. Cách giải quyết tốt nhất là hãy trò chuyện và cởi mở để cả hai có thể hiểu nhau hơn, cũng như biết được những khó khăn mà đối phương gặp phải.'
      ]
    },
    {
      code: 'RWS_Tarot_16_Tower',
      name: 'Tower',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_16_Tower.jpg`,
      text: [
        'Bạn rõ ràng vẫn chưa sẵn sàng với tình yêu. Những sự thay đổi đột ngột trong các khía cạnh khác của cuộc sống gây ra sự bùng nổ mạnh mẽ trong tâm trí làm bạn nghĩ rằng sẽ tốt hơn nếu mọi thứ vẫn như bây giờ. Bạn sẽ khó mà chấp nhận thêm một con người xa lạ nào khác bước vào cuộc đời để mà xáo tung nó lên. Bạn sợ tất cả những gì diễn biến quá nhanh và sẽ tốt hơn nếu bạn dành ra một quãng thời gian kha khá bình ổn cảm xúc trước khi bước vào mối quan hệ mới.',
        'Trong tình yêu, lá bài này thường nói về các yếu tố ngoại cảnh, các tác động bên ngoài, những người xung quanh sẽ ảnh hưởng đến mối quan hệ của hai bạn. Dường như chuyện tình cảm của bạn vừa xảy ra một biến động lớn, khiến bạn cảm thấy lo lắng, đứng ngồi không yên. Tác nhân bên ngoài luôn khiến bạn cảm thấy nghi ngờ tình cảm của đối phương dành cho mình. Nếu bạn không tin vào người ấy, có khả năng hai bạn sẽ khó có thể cùng nhau vượt qua các biến cố đang chờ đợi phía trước',
        'Nếu bạn muốn tiếp tục mối quan hệ này, đây là thời điểm quan trọng cho việc kiểm soát những mối nguy hại có thể gây ảnh hưởng đến tình yêu của cả hai Hãy trò chuyện để biết người ấy đang cảm thấy như thế nào.'
      ]
    },
    {
      code: 'RWS_Tarot_17_Star',
      name: 'Star',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_17_Star.jpg`,
      text: [
        'Lá bài này như cái thảm cảnh về tình yêu mà thời đại này ta hay nghe phàn nàn cả ngày lẫn đêm. Tình yêu mất đi sự mơ mộng vốn có, nữ chăm chăm vào vẻ đẹp trai cộng thêm "nhà mặt phố bố làm to", nam chỉ quan tâm "chân dài mặt xinh da trắng". Họ quên mất rằng có những việc thậm chí còn tệ hại hơn cả khi thiếu thốn vật chất. Bạn hoàn toàn mất niềm tin vào tình yêu trong thời điểm hiện tại.',
        'Bạn có thể hoàn toàn yên tâm về tình yêu của mình vì đây là điềm báo tin vui về tình yêu sẽ đến với bạn. Hai bạn là cặp đôi cực kì ăn ý và hòa hợp với nhau. Cuộc sống của bạn chỉ hạnh phúc và có ý nghĩa khi nửa kia luôn ở bên cạnh chăm sóc, yêu thương bạn. Nửa kia của bạn là ngôi sao may mắn mang lại cho bạn nhiều điều hạnh phúc, tốt đẹp. Quan trọng hơn, người ấy sẽ là động lực mạnh mẽ giúp bạn vượt qua mọi khó khăn, thử thách trong cuộc sống.',
        'Hãy luôn mở lòng để đón nhận những điều mới mẻ mà tình yêu mang lại. Thực ra đây là một mối quan hệ rất tốt, chỉ là trong lòng bạn có quá nhiều suy nghĩ gây ảnh hưởng tới tình yêu của hai người mà thôi.'
      ]
    },
    {
      code: 'RWS_Tarot_18_Moon',
      name: 'Moon',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_18_Moon.jpg`,
      text: [
        'Với lá bài The Moon, một lá bài thiên về trực giác, bạn chỉ cần tin tưởng vào giác quan thứ 6 của mình. Có thể bạn đang yêu đồng thời cả hai người mà chưa đưa ra được quyết định, hay đơn giản là bạn bỗng dưng có một niềm tin mãnh liệt rằng bạn sẽ gặp được tình nhân trong mộng của mình. Hãy cứ tin thôi. Còn bây giờ có phải bạn đang muốn hỏi làm sao có thể biết chắc được cảm giác đó là đúng? Đúng thế, bạn chẳng bao giờ biết chắc được.',
        'Mối quan hệ của bạn đang khác xa với những gì bạn cảm nhận được bên trong. Đừng quá hy sinh bản thân trong mối quan hệ này, dù rằng bạn bị thu hút một cách mãnh liệt bởi đối phương. Hãy tỉnh ngộ và đòi hỏi sự đáp trả tình yêu từ phía người kia.',
        'Lá bài này chỉ ra rằng, bạn đang đến thời điểm sôi sục, nhiều cảm xúc trong chuyện tình cảm. Hãy nghỉ ngơi nếu bạn thấy cần, hay thậm chí tranh cãi nhẹ nhàng với người ấy. Đừng vội chấm dứt để bắt đầu mối quan hệ mới.'
      ]
    },
    {
      code: 'RWS_Tarot_19_Sun',
      name: 'Sun',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_19_Sun.jpg`,
      text: [
        'The Sun, cũng như tên, toả sáng như ánh mặt trời vậy. Họ có một sức hấp dẫn mạnh mẽ để thu hút tất cả mọi người. The Sun - chính là bạn, như đứa trẻ, mang lại niềm vui, sự ấm áp nhưng cũng rất vô tâm vô phế. Nói đến tình yêu với The Sun như "nước đổ lá khoai" vậy. Một lý do nữa mà những con người nổi bật này khó có thể tìm được tình yêu là chính bản thân họ. Ai cũng yêu mặt trời, nhưng mấy ai nhìn lâu vào nó.',
        'Hai bạn đang có một  tình yêu bùng nổ, tràn ngập năng lượng. Mỗi khi gặp hay nói chuyện với đối phương, người còn lại sẽ cảm thấy như được tiếp thêm sinh lực. Có vẻ như hai bạn đều là những người ưa nổi bật, vậy nên thường có xu hướng ích kỷ trong tình cảm.',
        'Nếu bạn đang trong một mối quan hệ nghiêm túc thì đây chính là lúc bạn và người yêu đang cần có không gian riêng hơn bao giờ hết. Đây không phải là lúc để nhõng nhẽo, dựa dẫm, hoặc quá cần nhau.'
      ]
    },
    {
      code: 'RWS_Tarot_20_Judgement',
      name: 'Judgment',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_20_Judgement.jpg`,
      text: [
        'Bạn rất thiếu tự tin về bản thân mình. Bạn luôn suy ngẫm, tự đánh giá con người mình. Bên cạnh đó, bạn cũng không nhận biết được đâu là người phù hợp. Quá cứng rắn và khắt khe với bản thân thực sự không thích hợp cho tình yêu. Tất cả những gì bạn cần là thoải mái hơn và nghe theo cảm xúc của mình.',
        'Người yêu của bạn giúp bạn nhận ra những điều mà bạn xứng đáng được hưởng trên cuộc đời. Judgment còn cho thấy các bạn có sự đồng điều nhất định về mặt tâm hồn. Tuy nhiên, bản thân bạn cũng có những đánh giá nhất định về đối phương, điều này có thể là một thách thức trong tình yêu của hai bạn.',
        'Nếu bạn đang có một mối quan hệ, đây là khoảng thời gian thành-hoặc-bại. Đây là lúc để bạn làm thật rõ những gì bạn muốn từ cuộc sống hoặc từ mối quan hệ của bạn, và sau đó là tiến tới thực hiện nó.'
      ]
    },
    {
      code: 'RWS_Tarot_21_World',
      name: 'World',
      image: `${CDN_URL}/general/Tarot/RWS_Tarot_21_World.jpg`,
      text: [
        'The World cho thấy bạn không sẵn sàng cho một cuộc tình mới ngay cả khi cuộc tình cũ kết thúc từ lâu. Bạn bị trì trệ do những cảm giác buồn chán và sự suy sụp kéo dài cũng như sự ám ảnh bởi người tình cũ. Cảm giác có thể rất mệt mỏi khiến bạn nghĩ rằng mọi việc đang đi trong vòng tròn và con đường đi đến một tương lai hạnh phúc dường như dài đằng đẵng. Bạn cảm thấy mình không đủ tốt để sẵn sàng yêu bất cứ ai và do đó tốt hơn hết là bạn không nên gặp gỡ những người mới.',
        'Hai bạn có một tình yêu đong đầy và trọn vẹn. Cả hai yêu nhau và giúp nhau hoàn thiện hơn mỗi ngày. Đây cũng là một tín hiệu cho thấy bạn và người ấy có thể có một cái kết tuyệt vời - một đám cưới vào tương lai không xa chẳng hạn.',
        'Hãy cho cả hai cơ hội xa nhau, thậm chí là đi du lịch riêng để cả hai nhận ra rằng không thể sống thiếu nhau được. Bạn chính là cả thế giới với người kia và ngược lại. Chỉ là hai bạn đang trải qua cảm giác tù túng vì ở bên nhau quá lâu mà thôi.'
      ]
    },
    {
      code: 'Swords01',
      name: 'Ace of Swords',
      image: `${CDN_URL}/general/Tarot/Swords01.jpg`,
      text: [
        'Tình duyên của bạn sẽ có sự khởi sắc trong vài tháng tới, tuy nhiên, cần phải đề phòng những mối quan hệ thoáng qua hoặc có thể gây tổn thương cho bạn.',
        'Nếu bạn đang trong một mối quan hệ tình cảm, thì lá bài này có thể hàm ý đến một vấn đề nào đó trong mối quan hệ sẽ khiến bạn muốn chấm dứt duy trì cuộc tình này. Đừng ngần ngại nói lên suy nghĩ của bạn.',
        'Nếu một điều gì đó trong mối quan hệ gây tổn thương hay không tốt cho bạn, và bạn mất đi người yêu vì những gì bạn đã hay sẽ nói ra – vậy thì thực ra bạn không hề mất mát gì cả đâu, mà chỉ đơn giản là bạn đang hướng tới một mối quan hệ tốt hơn cho bạn mà thôi. Hãy nói lên sự thật!'
      ]
    },
    {
      code: 'Swords02',
      name: '2 of Swords',
      image: `${CDN_URL}/general/Tarot/Swords02.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm tình yêu, hãy nhìn nhận nghiêm túc về bản thân mình, về những cảm xúc nào ngoài tầm kiểm soát mà bạn cần phải giải quyết. Đừng nhìn ra bên ngoài chính bản thân mình khi muốn tìm kiếm cho mình sự hạnh phúc. Một khi hiểu và làm được điều đó, mối quan hệ mà bạn thực sự muốn và cần sẽ đến thôi.',
        '2 of Swords là một điềm tốt, đặc biệt là cho một mối quan hệ tình cảm đang tồn tại. Lá bài này biểu thị rằng mối quan hệ của bạn sẽ trở nên dễ dàng phát triển, ổn định và cân bằng hơn, tuy nhiên, hai bạn cần đối xử với nhau một cách công bằng và trân trọng nhau hơn.',
        'Bạn đang đứng giữa hai lựa chọn và phân vân không biết nên làm thế nào. Lời khuyên dành cho bạn là hãy ngưng sợ hãi, thật bình tâm và nhìn vào sâu thẳm trong trái tim để biết rằng mình thực sự muốn điều gì.'
      ]
    },
    {
      code: 'Swords03',
      name: '3 of Swords',
      image: `${CDN_URL}/general/Tarot/Swords03.jpg`,
      text: [
        'Bạn vẫn còn đang đau khổ với mối tình cũ và nó đã trở thành một vết thương khó lành. Hãy dành thời gian cho bản thân để tìm lại sự bình ổn trước khi tiến tới với một mối quan hệ khác.',
        'Lá bài này không phải là hồi chuông báo tử dành cho một mối quan hệ, nhưng nó là lời cảnh báo rằng mối quan hệ đang gặp nhiều khó khăn và hai bên đang chịu nhiều đau khổ. Để mối quan hệ tình cảm tồn tại và đơm hoa, cả hai cần sẵn sàng hành động, trò chuyện và suy nghĩ về mối quan hệ đấy.',
        'Để một mối quan hệ tình cảm tồn tại và đơm hoa, cả hai cần sẵn sàng hành động, trò chuyện và suy nghĩ về mối quan hệ đấy. Bạn có thể thấy rằng một người mà bạn nghĩ rằng quan tâm đến bạn thật ra lại không yêu bạn như bạn đã nghĩ. Nếu đó chính là vấn đề mà bạn đang mắc phải, vậy thì thì đừng chỉ ôm nỗi đau trong lòng cho riêng mình và cũng đừng đau buồn quá lâu.'
      ]
    },
    {
      code: 'Swords04',
      name: '4 of Swords',
      image: `${CDN_URL}/general/Tarot/Swords04.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm tình yêu, thì bây giờ không phải là lúc vội vàng. Tốt hơn hết là bạn hãy tìm hiểu một cách rất, rất cụ thể và chính xác về những gì bạn đang tìm kiếm cho một mối quan hệ. Rồi cũng sẽ đến khi bạn sẽ gặp một ai đó thôi,nhưng có lẽ không phải trong thời gian này.',
        'Nếu bạn đang trong một mối quan hệ yêu đương, bạn có thể cảm thấy bị mất kết nối đôi chút. Điều quan trọng là để cho nửa kia của bạn biết bạn đang nghĩ gì, nhưng hãy nhớ rằng cảm xúc của bạn là của riêng bạn, không phải lúc nào họ cũng phải làm cho bạn cảm thấy vui.',
        'Điều quan trọng là để cho nửa kia của bạn biết bạn đang nghĩ gì, nhưng hãy nhớ rằng cảm xúc của bạn là của riêng bạn, không phải lúc nào họ cũng phải làm cho bạn cảm thấy vui. Có thể hiện tại hai bạn chỉ cần đơn giản là tạm xa nhau một thời gian, mỗi người tự tìm cho mình không gian riêng.'
      ]
    },
    {
      code: 'Swords05',
      name: '5 of Swords',
      image: `${CDN_URL}/general/Tarot/Swords05.jpg`,
      text: [
        'Những kỳ vọng của bạn về một người có thể không giống với thực tế. Đừng quá mộng tưởng khi bước chân vào mối quan hệ này nhé. Hãy nhìn nhận sự việc một cách khách quan hơn.',
        'Thật không may khi lá 5 of Swords xuất hiện trong một trải bài liên quan về tình yêu, nó có nghĩa là xung đột trong tình yêu là viễn cảnh gần như chắc chắn. Mọi thứ có thể không diễn ra như bạn kỳ vọng trong mối quan hệ yêu đương hiện tại.',
        'Hãy suy nghĩ bình tĩnh và cẩn thận về điều gì gây ra những rạn vỡ trong mối quan hệ này. Hãy chân thật với bản thân mình trước, rồi hãy chân thật với người mình yêu. Hãy biết chấp nhận thực tế.'
      ]
    },
    {
      code: 'Swords06',
      name: '6 of Swords',
      image: `${CDN_URL}/general/Tarot/Swords06.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm tình yêu, vậy thì lá bài này cho thấy bạn cần từ bỏ những thói quen cố hữu của bản thân nếu như bạn muốn gặp một ai đó mới. Hãy khám phá những phương thức và thói quen mới.',
        'Về tình yêu, lá bài 6 of Swords có thể hàm ý về một nhu cầu thay đổi tình cảnh. Các bạn đang bị gò bó vào những thói quen thường ngày, và những điều này dần trở nên nhàm chán',
        'Nếu bạn đang trong một mối quan hệ, hãy cùng người yêu mình đi đâu đó, có thể chỉ đơn thuần là xuống phố đi lòng vòng hay đại loại vậy cũng được. Điều này sẽ giúp tâm trạng tốt hơn, dễ dàng cải thiện tình trạng xích mích giữa hai người.'
      ]
    },
    {
      code: 'Swords07',
      name: '7 of Swords',
      image: `${CDN_URL}/general/Tarot/Swords07.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm tình yêu, bạn sẽ có cơ hội gặp một người rất thú vị vào thời điểm này, nhưng đừng cố tránh mặt cho dù bạn không hứng thú với một mối quan hệ; người tình tiềm năng sẽ dần tiến tới thành người yêu gắn kết hơn với bạn. Hãy cố gắng giữ cân bằng.',
        'Nếu bạn đang có một mối quan hệ gắn kết khi lá bài này xuất hiện, thì khi đó lá bài này truyền tải một thông điệp rất rõ ràng rằng sự tin tưởng giữa hai bạn  là yếu tố cực kỳ quan trọng để làm nên mối quan hệ gắn bó dài lâu.',
        'Nếu bạn cảm thấy nghi ngờ vì một lý do nào đó về đối phương của mình, hãy nói chuyện về điều này cho dù sẽ gặp khó khăn. Việc công kích thụ động sẽ chỉ khiến mọi chuyện tệ hơn thôi.'
      ]
    },
    {
      code: 'Swords08',
      name: '8 of Swords',
      image: `${CDN_URL}/general/Tarot/Swords08.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm tình yêu, điều đầu tiên là xem bạn đã yêu bản thân mình như thế nào. Người khác không thể đi cùng bạn và chỉnh sửa bạn. Thế nên hãy bắt đầu từ chính bản thân bạn.',
        'Về tình yêu, lá 8 of Swords cho thấy bạn cần đánh giá lại một mối quan hệ một cách nghiêm túc. Những dấu hiệu mà bạn đã cố tình làm ngơ sẽ xuất hiện ngày càng nhiều.',
        'Lá này cho thấy bạn cần đánh giá lại một mối quan hệ một cách nghiêm túc. Bạn có thể đang trong mối quan hệ với người không tốt với bạn, vì bạn cho rằng điều đó “còn hơn là không có gì”. Nếu vậy, hãy suy nghĩ lại. Việc có một mối quan hệ không lành mạnh thực sự là điều tồi tệ hơn nhiều so với việc FA.'
      ]
    },
    {
      code: 'Swords09',
      name: '9 of Swords',
      image: `${CDN_URL}/general/Tarot/Swords09.jpg`,
      text: [
        'Ngoài ra, nếu gần đây bạn đã gặp một người mới mà bạn cảm thấy thích, hãy chú ý đừng quá bị lôi cuốn và ngập chìm trong niềm tin rằng mọi thứ đều tốt đẹp giống như vẻ ngoài. Sự tin tưởng luôn cần thời gian để bồi đắp. Đừng quá vội vã.',
        'Có một điều gì đó đang khiến bạn cảm thấy bất an về mối quan hệ tình cảm hiện tại. Đừng phớt lờ cảm giác đó. Nếu bạn đang cảm thấy có điều gì đó không lành mạnh về mối quan hệ của của mình, vậy thì rất có thể bạn đã cảm thấy đúng.',
        'Nếu bạn đang cảm thấy có điều gì đó không lành mạnh về mối quan hệ của của mình, vậy thì rất có thể bạn đã cảm thấy đúng. Nếu bạn nghi ngại rằng có điều gì đó đang bị che giấu, cách tốt nhất là hỏi người yêu của bạn về mối nghi ngờ này.'
      ]
    },
    {
      code: 'Swords10',
      name: '10 of Swords',
      image: `${CDN_URL}/general/Tarot/Swords10.jpg`,
      text: [
        'Nếu bạn mới gặp ai đó mới gần đây (và bạn đang tìm kiếm tình yêu) thì hãy tiếp tục xem xét và cân nhắc. Có khả năng những người bạn đã gặp cho đến lúc này không giống như bạn nghĩ đâu.',
        'Liên quan đến câu hỏi tình yêu, đáng tiếc lá bài 10 of Swords không phải là một dấu hiệu tốt. Có điều gì đó trong tình yêu của bạn đang không thật sự ổn. Hãy tự hỏi bản thân mình các câu hỏi, chấp nhận thực tế, không nhắm mắt trước sự thật. Bạn không thể thúc ép một ai đó yêu bạn hay đối xử với bạn một cách trân trọng được đâu.',
        'Liên quan đến câu hỏi tình yêu, đáng tiếc lá bài 10 of Swords không phải là một dấu hiệu tốt. Hãy tự hỏi bản thân mình các câu hỏi, chấp nhận thực tế, không nhắm mắt trước sự thật. Bạn không thể thúc ép một ai đó yêu bạn hay trân trọng bạn được đâu.'
      ]
    },
    {
      code: 'Swords11',
      name: 'Page of Swords',
      image: `${CDN_URL}/general/Tarot/Swords11.jpg`,
      text: [
        'Nếu bạn chưa gắn kết tình cảm với ai đó, vậy thì hãy tìm kiếm và hẹn hò với một người phù hợp. Tình yêu sẽ không tự dưng từ trời rớt xuống đâu.',
        'Trong những câu hỏi về tình cảm, lá bài Page of Swords hàm ý đến khả năng xuất hiện các xích mích nhẹ. Hãy cho phép đối phương của mình có không gian riêng của người ấy. Đây là điều cốt yếu để có hạnh phúc.',
        ' Hãy biết lúc nào cần cương và lúc nào cần nhu, nhiều cuộc tranh cãi thật sự không đáng xảy ra (nếu như đối phương của bạn đang hành xử với bạn một cách tôn trọng). Hãy cho phép đối phương của mình có không gian riêng của người ấy. Đây là điều cốt yếu để có hạnh phúc.'
      ]
    },
    {
      code: 'Swords12',
      name: 'Knight of Swords',
      image: `${CDN_URL}/general/Tarot/Swords12.jpg`,
      text: [
        'Bạn có thể đang sắp yêu một ai đó, dù bạn là nam hay nữ. Nếu bạn đang tìm kiếm tình yêu, thì nó sẽ có thể đến nhanh hơn mong đợi của bạn nhưng bạn phải giữ liên lạc với người vốn sẵn sàng mở cửa trái tim để đón bạn vào vườn hồng tình yêu. Hãy sửa soạn vẻ ngoài cho phù hợp và hẹn hò với người ấy. Bạn sẽ tìm được rất nhiều niềm vui.',
        'Nếu hiện tại bạn đang trong một mối quan hệ tình cảm gắn kết, vậy thì mối quan hệ này dường như sẽ trở nên tươi sáng hơn, sâu đậm hơn và thỏa mãn hơn.',
        'Người yêu của bạn có phần mãnh liệt và hiếu thắng. Đôi khi, bạn cảm thấy quá mệt mỏi vì phải tranh cãi với người ấy. Hãy nói chuyện thẳng thắn về cảm xúc của bạn để hai người có thể hiểu nhau và cùng tiết chế.'
      ]
    },
    {
      code: 'Swords13',
      name: 'Queen of Swords',
      image: `${CDN_URL}/general/Tarot/Swords13.jpg`,
      text: [
        'Về tình yêu, lưu ý duy nhất về lá Queen of Swords là hãy cẩn trọng với những cơn bốc đồng quá mạnh mẽ. Tất cả chúng ta đều được dạy rằng chúng ta sẽ gặt hái được nhiều điều tốt đẹp hơn nếu chúng ta cư xử tử tế, và quan điểm đó hoàn toàn đúng trong trường hợp này.',
        'Mẹ của bạn hoặc một người giống mẹ của bạn có thể ngăn cản mối quan hệ của bạn, cho dù bà ấy có muốn hay không. Hãy tìm hiểu xem giới hạn của bạn nằm ở đâu với vấn đề này, và hãy cho người phụ nữ ấy biết.',
        'Có vẻ như bạn chính là nguyên nhân khiến mối quan hệ rơi vào rắc rối. Bạn luôn thể hiện sự không hài lòng về những điểm xấu của anh ấy, thường xuyên đưa ra những lời nhận xét lạnh lùng. Hãy tiết chế lại những điều đó và thể hiện sự dịu dàng của bản thân nhé, đàn ông rất dễ xuôi lòng trước vẻ nhẹ nhàng của phụ nữ đấy!'
      ]
    },
    {
      code: 'Swords14',
      name: 'King of Swords',
      image: `${CDN_URL}/general/Tarot/Swords14.jpg`,
      text: [
        'Bạn có thể sẽ chuẩn bị bước vào một mối quan hệ với một người đàn ông mạnh mẽ và có chính kiến. Ban đầu, anh ấy sẽ là một chỗ dựa đáng tin cậy nhưng về lâu dài, bạn sẽ có lúc rơi vào cảm giác bị bao bọc và giữ gìn quá đà.',
        'Trong mối quan hệ yêu đương, King of Swords có thể hàm ý đến một người đàn ông có chính kiến và mạnh mẽ có thể đóng một vai trò nào đó trong mối quan hệ này. Nếu người đàn ông này là người yêu bạn, nó có nghĩa rằng bạn sẽ phải chấp nhận anh ấy như những gì chính anh ấy có, và cho dù bạn là nam hay nữ, bạn sẽ đối xử tốt nhất với anh ấy bằng cách phát huy phần nữ tính về phía bạn.',
        'Lá bài này nói rằng bạn sẽ phải chấp nhận anh ấy như những gì chính anh ấy có. Tuy nhiên, cũng đừng chịu đựng nếu bạn bị ngược đãi. Hãy biết tự bảo vệ mình.'
      ]
    },
    {
      code: 'Wands01',
      name: 'Ace of Wands',
      image: `${CDN_URL}/general/Tarot/Wands01.jpg`,
      text: [
        'Nếu bạn đang “lẻ bóng”, lá bài này có thể hàm ý một khởi đầu của một mối quan hệ yêu đương mới đang tìm đến bạn. Hãy chuẩn bị tinh thần cho chính bạn bằng mọi cách mà bạn cảm thấy ổn để sẵn sàng đón nhận tình yêu.',
        'Nếu bạn đã có một mối quan hệ tình cảm nhiều hứa hẹn, thì lá bài Ace of Wands cho bạn biết rằng mối quan hệ này sắp sửa trải qua một “sự khởi đầu mới” và hai bạn sẽ hiểu biết nhiều hơn về nhau. Hãy bộc lộ bản thân mình.',
        'Hai bạn mới trong giai đoạn đầu của mối quan hệ, việc gặp các khó khăn là điều không thể tránh khỏi. Điều hai bạn cần làm là hãy tâm sự và chia sẻ với nhau nhiều hơn nhé!'
      ]
    },
    {
      code: 'Wands02',
      name: '2 of Wands',
      image: `${CDN_URL}/general/Tarot/Wands02.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm tình yêu, vậy thì một người nào đó có thể rất tốt với bạn sẽ có khả năng đã là một phần trong cuộc đời bạn rồi. Đừng đánh giá người khác qua vẻ bề ngoài. Nếu ai đó quan tâm đến bạn, hãy cho họ một cơ hội. Mối quan hệ này có thể là điều bạn đang tìm kiếm.',
        'Nếu bạn đã có một mối quan hệ gắn kết, thì mối quan hệ này sẽ dần bình đẳng hơn. Hãy tập trung vào những ý tưởng về tính bình đẳng và cân bằng và chia sẻ chúng với người yêu của bạn.',
        'Lá bài này thể hiện sự nỗ lực và hợp tác đến từ hai phía trong một mối quan hệ.  Bạn không cần lo lắng nữa rồi bởi Two of Wands cho thấy chuyện tình cảm của bạn vẫn ổn. Hãy nhớ rằng hai cây gậy ở đây thể hiện sự cân bằng.'
      ]
    },
    {
      code: 'Wands03',
      name: '3 of Wands',
      image: `${CDN_URL}/general/Tarot/Wands03.jpg`,
      text: [
        'Nếu gần đây bạn gặp một người mới, thì lá bài này cho bạn biết rằng người ấy đánh giá bạn khá cao. Và nếu bạn đang tìm kiếm tình yêu nhưng cảm thấy chưa có bất kỳ triển vọng nào, thì thực tế bạn có lẽ đang tiêu tốn quá nhiều thời gian và nỗ lực cho sự nghiệp, công việc của mình. Hãy suy nghĩ về việc cho phép bản thân thêm thời gian để tận hưởng cuộc sống cá nhân.',
        'Các mối quan hệ tình cảm của bạn đều ổn, miễn là người yêu của bạn sẵn lòng đối xử với bạn một cách bình đẳng. Nếu không, đây có thể là thời điểm để “bước qua cuộc tình” / từ bỏ tình yêu sau khi bạn đã giải thích mọi chuyện rõ ràng.',
        'Trong một trải bài về mối quan hệ, lá bài này đại diện cho những suy nghĩ bình yên của bạn về người ấy, nhưng với rất nhiều thắc mắc cần được trả lời. Sự bình tĩnh ở đây đôi lúc có thể là để suy xét lại vấn đề. Tuy vậy, cũng không có điều gì đáng lo lắng cho lắm bởi những lá Wands đều mang năng lượng tốt đẹp nhất định.'
      ]
    },
    {
      code: 'Wands04',
      name: '4 of Wands',
      image: `${CDN_URL}/general/Tarot/Wands04.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm tình yêu, lá bài cho thấy rằng những sự kiện đặc biệt (như đám cưới, tiệc tùng, lễ kỷ niệm) là những dịp và địa điểm rất tốt để bạn gặp gỡ một ai đó đặc biệt. Nếu bạn được mời đến một sự kiện giống như vậy, và bản thân đang chưa có người yêu, vậy thì hãy cố gắng tham gia và ghi điểm nhé.',
        ' Xét về tình yêu, lá bài này có thể hàm ý rằng mối quan hệ của bạn sắp đến được giai đoạn cam kết trọn vẹn, và có thể là một lá bài tiềm năng cho một sự kiện cưới hỏi.',
        'Nếu bạn cảm thấy người ấy có vẻ không quan tâm đến mình thì đừng vội lo lắng, rất có thể là anh ấy đang bận với các mối quan hệ gia đình hay bạn bè nên chưa có thời gian quan tâm tới bạn. Hãy nói rõ điều đó với anh ấy để cả hai có thể hiểu nhau hơn nhé!'
      ]
    },
    {
      code: 'Wands05',
      name: '5 of Wands',
      image: `${CDN_URL}/general/Tarot/Wands05.jpg`,
      text: [
        'Người bạn đang để mắt đến có thể đang bị theo đuổi bởi những kẻ khác. Ngay cả như vậy thì bạn cũng hãy cố gắng hết sức mình. Lá bài này cũng có ý nghĩa khác rằng bạn sẽ được theo đuổi bởi một hay vài người lãng mạn khác đấy.',
        'Người bạn yêu có thể đang bị theo đuổi bởi những kẻ khác. Ngay cả như vậy thì bạn cũng hãy cố gắng hết sức mình. Bạn cần biết rằng bạn đã làm tất cả những gì có thể cho mối quan hệ này.',
        'Five of Wands còn nói về sự khác biệt trong mối quan hệ, cả hai bạn đều khá bướng bỉnh và chính vì thế, hai bạn khó lòng có thể thấu hiểu được nhau. Đôi khi lá bài xuất hiện khi trải bài khuyên người ấy của bạn nên dừng tranh luận lại vì điều này ảnh hưởng đến đời sống riêng của cả hai bạn.'
      ]
    },
    {
      code: 'Wands06',
      name: '6 of Wands',
      image: `${CDN_URL}/general/Tarot/Wands06.jpg`,
      text: [
        'Nếu bạn không được hấp dẫn hay đủ tự tin, thì sự xuất hiện của lá bài này có thể hàm ý rằng bạn sắp bước vào một mối quan hệ với một ai đó mà bạn đang mong đợi. Đây là một dấu hiệu rất tích cực.',
        'Trong một mối quan hệ đã gắn kết, lá bài này hàm ý rằng mọi thứ đang tiến triển tốt. Bất chợt bạn có thể sẽ nhận ra ở chính mình cảm giác gần gũi hơn và hạnh phúc hơn với đối phương – người yêu của mình so với trước đây.',
        'Điều này báo hiệu mối quan hệ của bạn đang có các định hướng tích cực và tương lai tươi sáng. Nó sẽ kết thúc một bất đồng hay mẫu thuẫn của các bạn với một chiều hướng tốt đẹp nhất.'
      ]
    },
    {
      code: 'Wands07',
      name: '7 of Wands',
      image: `${CDN_URL}/general/Tarot/Wands07.jpg`,
      text: [
        'Nếu bạn đang tìm kiếm nửa còn lại, thì lá bài này có thể hàm ý rằng bạn sắp gặp một ai đó mới ở cách xa nơi bạn hay thường lui tới. Đôi khi hãy thử một điều gì đó mới mẻ.',
        'Nếu bạn đang trong một mối quan hệ gắn bó và hai bên đã cam kết tương lai, đừng ngần ngại nói ra suy nghĩ của mình với người yêu. Điều này sẽ cải thiện được mối quan hệ của bạn miễn là bạn thận trọng trong cách thể hiện suy nghĩ của mình.',
        'Thực ra điều hai bạn còn thiếu chính là sự gắn kết sâu sắc về mặt tâm hồn. Hãy mở lòng cùng nhau để có thể giúp mối quan hệ này trở nên tốt hơn.'
      ]
    },
    {
      code: 'Wands08',
      name: '8 of Wands',
      image: `${CDN_URL}/general/Tarot/Wands08.jpg`,
      text: [
        'Bạn tưởng rằng mình đã sẵn sàng cho  một mối quan hệ mới nhưng thực chất không phải. Bạn không thể có được những nhiều mình mong muốn bằng cách ép buộc. Người bạn yêu có thể sẽ tìm đến bạn khi bạn chưa kịp biết.',
        'Trong vấn đề tình cảm, khi lá bài 8 of Wands xuất hiện, nhiều khả năng là bạn đang cảm thấy bản thân mình đã sẵn sàng cho một bước tiến triển mới trong quan hệ tình cảm hiện tại (như hôn nhân), trong khi thực tế bạn dường như vẫn chưa sẵn sàng đâu. Đừng thúc ép người khác trước khi họ sẵn sàng.',
        'Bạn không thể có được những gì mình cần bằng cách ép buộc. Hãy biết phân tán bản thân nếu cần thiết, và người bạn yêu có thể sẽ là một nguồn an ủi bất ngờ mà bạn chưa hề nghĩ tới.'
      ]
    },
    {
      code: 'Wands09',
      name: '9 of Wands',
      image: `${CDN_URL}/general/Tarot/Wands09.jpg`,
      text: [
        'Nếu bạn chưa có người yêu, và đang khao khát yêu đương và được yêu, thì hãy hiểu rằng thời điểm này chưa phù hợp cho bạn. Liệu có điều gì đó về bản thân bạn mà bạn muốn thay đổi không? Cần biết đây là thời điểm tốt để kiểm soát lại toàn bộ xúc cảm của bản thân bạn.',
        'Trong vấn đề tình cảm, một điều rất quan trọng cần nhớ là không có bất kỳ mối quan hệ nào trên thế giới có thể “yên ổn” một cách bất biến. Hãy thử cho người yêu của bạn nhiều không gian hơn',
        'Trong vấn đề tình cảm, một điều rất quan trọng cần nhớ là không có bất kỳ mối quan hệ nào trên thế giới có thể “yên ổn” một cách bất biến. Hãy thử cho người yêu của bạn nhiều không gian hơn.'
      ]
    },
    {
      code: 'Wands10',
      name: '10 of Wands',
      image: `${CDN_URL}/general/Tarot/Wands10.jpg`,
      text: [
        'Nếu bạn vẫn còn đang loay hoay tìm kiếm tình yêu, đừng nản chí. Thời điểm này cuối cùng rồi cũng sẽ đến, nhưng có lẽ đây là thời điểm tốt để bạn dừng cuộc tìm kiếm của mình lại và đơn giản là cho phép bản thân mình thư giãn, và đợi tín hiệu phản hồi lại.',
        'Sự xuất hiện của lá 10 of Wands thường biểu hiện một thời điểm khó khăn trong một mối quan hệ. Khó khăn này có thể xuất phát từ việc người yêu của bạn đang trải qua một giai đoạn xấu vốn không ảnh hưởng gì đến mối quan hệ của bạn nhưng lại rất cần sự hỗ trợ của bạn.',
        'Tuy nhiên, thường thì khó khăn này cũng hàm ý rằng có một điều gì đó trong mối quan hệ của hai bạn hiện đang cần được xem xét, nhìn nhận, và cân nhắc lại. Sẽ không ai trong hai bạn có lợi nếu giả vờ như không có rắc rối gì đang xảy ra.'
      ]
    },
    {
      code: 'Wands11',
      name: 'Page of Wands',
      image: `${CDN_URL}/general/Tarot/Wands11.jpg`,
      text: [
        'Những thông điệp yêu đương và cảm giác được yêu có thể đang đến với bạn. Nếu bạn đang một mình, một người bạn theo đuổi vốn rất ưa mạo hiểm có thể xuất hiện trong thế giới của bạn. Hãy nắm lấy cơ hội đó, nhưng chỉ khi bạn đã sẵn sàng.',
        'Nếu bạn đã có một mối quan hệ tình cảm gắn bó nhiều hứa hẹn mà không cảm thấy hạnh phúc, thì đây là thời điểm để bắt đầu quyết định xem liệu mối quan hệ đó có thể được cứu vãn hay không, và bạn sẽ cố gắng hàn gắn trong bao lâu.',
        'Cả hai bên cần phải sẵn sàng hợp tác cùng nhau trong một mối quan hệ. Nếu người yêu bạn không làm vậy, thì đây có thể là thời điểm để từ bỏ cuộc tình này.'
      ]
    },
    {
      code: 'Wands12',
      name: 'Knight of Wands',
      image: `${CDN_URL}/general/Tarot/Wands12.jpg`,
      text: [
        'Bạn có thể gặp một đối tượng mới thông qua công việc của mình. Người mới này có thể có mái tóc màu hơi sáng.',
        'Nếu hiện tại bạn đã đang trong một mối quan hệ gắn kết với một ai đó, hãy đảm bảo rằng các cuộc nói chuyện giữa bạn và người đó luôn rõ ràng nhất có thể.',
        'Hãy hẹn hò nhiều hơn để dành thời gian nói chuyện với nhau. Điều này về căn bản sẽ giúp cải thiện cảm giác gần gũi thân thuộc của hai bạn.'
      ]
    },
    {
      code: 'Wands13',
      name: 'Queen of Wands',
      image: `${CDN_URL}/general/Tarot/Wands13.jpg`,
      text: [
        'Lá bài Queen of Wands là một dấu hiệu rất tích cực trong bối cảnh một câu hỏi về mối quan hệ yêu đương. Nếu bạn đang tìm kiếm người yêu, lá bài cho biết bạn sẽ gặp được ai đó thích hợp thông qua một người quen biết.',
        'Nếu hiện bạn không cảm thấy tự tin là chính mình, hãy tìm cách thoát khỏi mớ cảm xúc hỗn độn và không có lợi này, tin tưởng nhiều hơn vào bản thân và rồi vũ trụ sẽ mang đến bạn một tình yêu phù hợp và những hỗ trợ cần thiết mà bạn muốn và cần.',
        'Nếu hiện bạn không cảm thấy tự tin là chính mình, hãy tìm cách thoát khỏi mớ cảm xúc hỗn độn và không có lợi này, tin tưởng nhiều hơn vào bản thân và rồi vũ trụ sẽ mang đến bạn một tình yêu phù hợp và những hỗ trợ cần thiết mà bạn muốn và cần.'
      ]
    },
    {
      code: 'Wands14',
      name: 'King of Wands',
      image: `${CDN_URL}/general/Tarot/Wands14.jpg`,
      text: [
        'Hiện tại bạn có thể đang tập trung nhiều hơn cho công việc / sự nghiệp của bạn, nhưng hãy dành thời gian cho chuyện tình cảm (ngay cả khi bạn đang cô đơn) nếu không bạn sẽ chỉ biết đến công việc như thứ duy nhất trong cuộc sống mình.',
        'Xét trong bối cảnh về tình yêu, King of Wands là một dấu hiệu tuyệt vời. Lá bài  hàm ý rằng người ấy  cũng có cảm giác che chở và quan tâm về bạn, và rằng họ cũng muốn làm hết sức có thể để tạo điều kiện cho mối quan hệ giữa người ấy và bạn phát triển tốt',
        'Hiện tại bạn có thể đang tập trung nhiều hơn cho công việc / sự nghiệp của bạn, nhưng hãy dành thời gian cho chuyện tình cảm, nếu không bạn sẽ chỉ biết đến công việc như thứ duy nhất trong cuộc sống mình.'
      ]
    }
  ]
}, action) {
  switch(action.type) {
    case UPDATE_TOROT_PAGE:
      return Object.assign({}, state, {
        tarot_curent_page: action.curentPage,
      })
    case UPDATE_CONTENT_PAGE_3:
      const new_tarot_page = [
        ...state.tarot_page.slice(0, 2), // before the one we are updating
        { ...state.tarot_page[2], url: action.url, text2: action.text2 },
      ]
      return Object.assign({}, state, {
        tarot_page: new_tarot_page,
      })
    default:
      return state
  }
}

export default Tarot;