import {
  BASE_API_URL, LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_SUCCESS,
  FACEBOOK_LOGIN_REQUEST, FACEBOOK_LOGIN_SUCCESS, FACEBOOK_LOGIN_FAILURE,
  GOOGLE_LOGIN_REQUEST, GOOGLE_LOGIN_SUCCESS, GOOGLE_LOGIN_FAILURE,
  YMEETME_LOGIN_REQUEST, YMEETME_LOGIN_SUCCESS, YMEETME_LOGIN_FAILURE, AUTO_LOGOUT_NAME, AUTO_LOGOUT_TIME,
  AUTH_NEW_USER_REGISTRATION, AUTH_USER_LOGIN, AUTH_LOGOUT
} from '../constants/auth'
import notification from '../utils/errors/notification'
import {
    NOTIFICATION_INFOR, NOTIFICATION_SUCCESS, NOTIFICATION_WARNING, NOTIFICATION_ERROR, GA_SEND, GA_ACTION_LOGIN_FAILURE,
} from '../constants/Enviroment'
import { gaTrackEvent } from '../utils/google_analytic/ga_event'
import * as ymmStorage from '../utils/ymmStorage';
import { browserHistory } from 'react-router'

//need to create a common util function to handle format
function getCurrentTime() {
  return Date.now();
}
function setLastActivityTime() {
  ymmStorage.setItem(AUTO_LOGOUT_NAME, getCurrentTime())
}

function getLastActivityTime() {
  return ymmStorage.getItem(AUTO_LOGOUT_NAME)
}

function removeYmmToken() {
  // ymmStorage.removeItem('ymm_token')
  ymmStorage.clear()
}

function removeLastActivityTime() {
  ymmStorage.removeItem(AUTO_LOGOUT_NAME)
}

function setErrorExpiredTime() {
  ymmStorage.setItem('expired_time_error', 'Hết hạn đăng nhập, vui lòng đăng nhập lại để  trải nghiệm tiếp Ymeetme')
}

function auth(state = {
    isAuthenticated: ymmStorage.getItem('ymm_token') ? true : false,
    isFetching: false,
    error: false,
    error_message: null,
    errorMessage: null,
    isRegistrationUser: false
  }, action) {
  var current_time = getCurrentTime()
  var last_activity_time = getLastActivityTime()
  if(state.isAuthenticated && last_activity_time && (current_time - last_activity_time > AUTO_LOGOUT_TIME )) {
    removeYmmToken()
    removeLastActivityTime()
    // setErrorExpiredTime()
    return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: 'Hết hạn đăng nhập, vui lòng đăng nhập lại để  trải nghiệm tiếp Ymeet.me'
      })
  }
  try{ //try catch to prevent private mode which does not support ymmStorage writing
    if (state.isAuthenticated) {
      setLastActivityTime()
    }
  }catch(e) {
    return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: 'Trình duyệt của bạn đang bật chế độ riêng tư. Vui lòng tắt chế độ riêng tư và thử lại.'
      })
  }

  switch (action.type) {
    //handle facebook login
    case FACEBOOK_LOGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false,
        errorMessage: null
      })
    case FACEBOOK_LOGIN_FAILURE:
      const ga_fb_failure = {
        extra_information: 'Login Facebook fail: not authenticated'
      }
      // gaTrackEvent(GA_ACTION_LOGIN_FAILURE, state.current_user, ga_fb_failure);
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: (action.extentions === GOOGLE_LOGIN_FAILURE?
          'Không thể đăng nhập qua Google, vui lòng đăng nhập lại.' :
         'Không thể đăng nhập qua Facebook, vui lòng đăng nhập lại.')
      })
     case 'FACEBOOK_LOGIN_FAILURE_PRIVATE_MODE':
      const ga_primode_failure = {
        extra_information: 'Browser in private mode'
      }
      dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_primode_failure))
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: 'Bạn đang bật chế độ riêng tư. Vui lòng tắt chế độ riêng tư và thử lại.'
      })

    case GOOGLE_LOGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false,
        errorMessage: null
      })
    case GOOGLE_LOGIN_FAILURE:
      const ga_google_failure = {
        extra_information: 'Login Google fail'
      }
      gaTrackEvent(GA_ACTION_LOGIN_FAILURE, state.current_user, ga_google_failure);
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: 'Không thể đăng nhập qua Google, vui lòng đăng nhập lại.'
      })
     //case 'GOOGLE_LOGIN_FAILURE_PRIVATE_MODE':
     // const ga_primode_failure = {
     //   extra_information: 'Browser in private mode'
     // }
     // dispatch(gaSend(GA_ACTION_LOGIN_FAILURE, ga_primode_failure))
     // return Object.assign({}, state, {
     //   isFetching: false,
     //   isAuthenticated: false,
     //   errorMessage: 'Bạn đang bật chế độ riêng tư. Vui lòng tắt chế độ riêng tư và thử lại.'
     // })

    //handle ymm login after facebook login successfully
    case YMEETME_LOGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false,
        errorMessage: null
      })
    case YMEETME_LOGIN_SUCCESS:
      const isRegistration = action.error_code == AUTH_NEW_USER_REGISTRATION;
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: true,
        errorMessage: null,
        isRegistrationUser: isRegistration
      })
    case YMEETME_LOGIN_FAILURE:
    const ga_ymm_failure = {
      extra_information: 'Ymeet.me authen fail: ' + action.error_message
    }
    browserHistory.push('/');
    gaTrackEvent(GA_ACTION_LOGIN_FAILURE, state.current_user, ga_ymm_failure);
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: action.error_message
      })
    case AUTH_LOGOUT:
      return Object.assign({}, state, {
        isAuthenticated: false,
      })
    default:
      return state
    }
}
export default auth;
