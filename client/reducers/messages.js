import {
  FETCH_MESSAGE_REQUEST, FETCH_MESSAGE_SUCCESS, FETCH_MESSAGE_FAILURE,
  UPDATE_MESSAGES_REQUEST, UPDATE_MESSAGES, UPDATE_STATE_MESSAGE,
  UPDATE_TYPING_STATUS, DEFAULT_PAGE_SIZE, MARK_MESSAGE_AS_READ_IN_STATE,
} from 'chat/constants/messages';

const initialState = {
  isFetching: false,
  data: {},
  paging_model: {
    page_index: 1,
    page_size: DEFAULT_PAGE_SIZE,
    total: 0,
  },
  errorMsg: '',
  isTyping: false,
};

export default function messages(state = initialState, action) {
  switch (action.type) {
    case FETCH_MESSAGE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_MESSAGE_SUCCESS:
      const data = state.data;
      // data[action.custom_params.identifier] = action.res.data.reverse();
      const list_messages = action.res.data.map(item => {
        if (item.read_at) {
          item.is_sent = true;
        } else {
          item.is_sent = false;
        }
        return item;
      })
      data[action.custom_params.identifier] = list_messages;
      return Object.assign({}, state, {
        isFetching: false,
        data,
        paging_model: action.res.paging_model
        // data: [...state.data, ...action.res.data], // It is not currently correct, if I select  1 item many time, many new array is pushed in to state
      });

    case FETCH_MESSAGE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        errorMsg: action.errorMsg,
      });

    // Update state via socket from other client and after user enter to send message
    case UPDATE_MESSAGES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case UPDATE_STATE_MESSAGE: {
      const data = state.data;
      data[action.identifier] = [action.message, ...data[action.identifier]]
      return Object.assign({}, state, {
        isFetching: false,
        data
      });
      // return Object.assign({}, state, {
      //   isFetching: false,
      //   datas: [
      //     ...state.datas.slice(0, index),
      //     {
      //       ...state.datas[index],
      //       messages: [...state.datas[index].messages, action.message]
      //     },
      //     ...state.datas.slice(index + 1)
      //   ]
      // });
    }
    case MARK_MESSAGE_AS_READ_IN_STATE:
      const current_data = state.data;
      current_data[action.identifier] = current_data[action.identifier].map((msg) => {
        msg.read_at = true;
        return msg;
      })
      return Object.assign({}, state, {
        isFetching: false,
        data: current_data
      });
    case UPDATE_TYPING_STATUS:
      const datax = state.data;
      if (typeof action.identifier !== 'undefined' && action.identifier.length && datax[action.identifier].length) {
        datax[action.identifier][0].is_sent = true;
      }
      return Object.assign({}, state, {
        isTyping: action.isTyping,
        data: datax
      });
    default:
      return state;
  }
}
