import {
  USER_SUBSCRIPTION_REQUEST,
  USER_SUBSCRIPTION_SUCCESS,
  USER_SUBSCRIPTION_FAILURE,

  USER_UNSUBSCRIPTION_REQUEST,
  USER_UNSUBSCRIPTION_SUCCESS,
  USER_UNSUBSCRIPTION_FAILURE,
} from '../constants/GCMNotification'

const defaultState = {

}

function GCMNotification(state = defaultState, action) {
  switch (action.type) {
    case USER_SUBSCRIPTION_SUCCESS:
      return state;
    case USER_UNSUBSCRIPTION_SUCCESS:
      return state;
    default:
      return state
  }
}

export default GCMNotification
