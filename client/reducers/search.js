import {
	SEARCH_USER_REQUEST, SEARCH_USER_SUCCESS, SEARCH_USER_FAILURE,
	SEARCH_CONDITION_UPDATE_FAILURE, SEARCH_CONDITION_UPDATE_SUCCESS,
	UPDATE_PAGING_INFORMATION, SEARCH_PAGE_SIZE, NEED_SCROLL_TO_POSITION,
	SEARCH_CONDITION_GET_REQUEST, SEARCH_CONDITION_GET_SUCCESS, SEARCH_CONDITION_GET_FAILURE,
	UPDATE_SEARCH_USER_PROFILE, UPDATE_SEARCH_STATE, SEARCH_UPDATE_VIEW_MODE, SEARCH_RESET_DATA,
	SEARCH_NEW_USER_REQUEST, SEARCH_NEW_USER_SUCCESS, SEARCH_NEW_USER_FAILURE,
	SEARCH_ONLINE_USER_REQUEST, SEARCH_ONLINE_USER_SUCCESS, SEARCH_ONLINE_USER_FAILURE,
	SEARCH_MATCHING_RATIO_USER_REQUEST, SEARCH_MATCHING_RATIO_USER_SUCCESS, SEARCH_MATCHING_RATIO_USER_FAILURE,
	SEARCH_NEARBY_USER_REQUEST, SEARCH_NEARBY_USER_SUCCESS, SEARCH_NEARBY_USER_FAILURE,
	SEARCH_HARMONY_OF_AGE_REQUEST, SEARCH_HARMONY_OF_AGE_SUCCESS, SEARCH_HARMONY_OF_AGE_FAILURE,
	SEARCH_HARMONY_OF_HOROSCOPE_REQUEST, SEARCH_HARMONY_OF_HOROSCOPE_SUCCESS, SEARCH_HARMONY_OF_HOROSCOPE_FAILURE,
	SEARCH_HARMONY_OF_FATE_REQUEST, SEARCH_HARMONY_OF_FATE_SUCCESS, SEARCH_HARMONY_OF_FATE_FAILURE,
	SEARCH_RESET_SELECTED
} from 'constants/search';
import {
  GET_LIST_BOOST_IN_RANK_SUCCESS
} from 'pages/list-boost-in-rank/Constant';
import notification from 'utils/errors/notification';
import { VOTE_PHOTO_REQUEST, VOTE_PHOTO_SUCCESS, VOTE_PHOTO_FAILURE } from 'constants/photoStream';
import { VOTE, UNVOTE } from 'constants/userProfile';
import {
    NOTIFICATION_INFOR, NOTIFICATION_SUCCESS, NOTIFICATION_WARNING, NOTIFICATION_ERROR,
    GA_SEND, REMIND_BANNER, SEARCH_PAGE
} from 'constants/Enviroment';
import { DEFAULT_ERROR_LOAD_PAGE } from 'constants/TextDefinition';
import { compareIdentifiers, uniqUsers, uniqAndFilterUsers } from 'utils/common';
import * as ymmStorage from 'utils/ymmStorage';

function getViewMode() {
	return ymmStorage.getItem('viewMode')? ymmStorage.getItem('viewMode') : 'LIST_VIEW';
}

const defaultState = {
	condition: [{
		isLoading: false,
		age_min: 0,
		age_max: 0,
		birth_place_ids: '',
		constellation_types: '',
		body_type_ids: '',
		drink_types: '',
		education_ids: '',
		has_self_introduction: false,
		has_sub_images: false,
		height_max: 0,
		height_min: 0,
		holiday_types: '',
		hope_meets: '',
		house_mate_types: '',
		intention_marriages: '',
		job_description: '',
		last_visits: '',
		location_ids: '',
		marital_status_ids: '',
		new_user_only: false,
		occupation_ids: '',
		income_types: '',
		presence_child_statuses: '',
		registered_in_3_days: false,
		school_name: '',
		search_sort: 0,
		smoking_types: '',
		blood_types: '',
		search_type: 0
	},
	{
		isLoading: false,
		age_min: 0,
		age_max: 0,
		birth_place_ids: '',
		constellation_types: '',
		body_type_ids: '',
		drink_types: '',
		education_ids: '',
		has_self_introduction: false,
		has_sub_images: false,
		height_max: 0,
		height_min: 0,
		holiday_types: '',
		hope_meets: '',
		house_mate_types: '',
		intention_marriages: '',
		job_description: '',
		last_visits: '',
		location_ids: '',
		marital_status_ids: '',
		new_user_only: false,
		occupation_ids: '',
		income_types: '',
		presence_child_statuses: '',
		registered_in_3_days: false,
		school_name: '',
		search_sort: 0,
		smoking_types: '',
		blood_types: '',
		search_type: 1
	}],
	updated_condition: false,
  loaded_condition: false,
	page_index: 1,
	page_size: SEARCH_PAGE_SIZE,
  boostInRankUser: [],
	isFetching: false,
  	error: false,
  	error_message: null,
  	need_scroll_to_position: false,
  	search_mode: 'default', //get source for view other prifile
  	default: {
  		data: [],
  		total_result: -1,
  		total_elements: 20,
  		current_page: 1,
    		view_mode: getViewMode(),
    		scroll_position: 0,
  	},
  	new: {
  		data: [],
  		total_result: -1,
  		total_elements: 20,
  		current_page: 1,
  		view_mode: getViewMode(),
  		scroll_position: 0,
      need_reload: false,
  	},
  	online: {
  		data: [],
  		total_result: -1,
  		total_elements: 20,
  		current_page: 1,
  		view_mode: getViewMode(),
  		scroll_position: 0,
      need_reload: false,
  	},
  	matching_ratio: {
  		data: [],
  		total_result: -1,
  		total_elements: 20,
  		current_page: 1,
    		view_mode: getViewMode(),
    		scroll_position: 0,
  	},
  	nearby: {
	  	data: [],
	  	total_result: -1,
	  	total_elements: 20,
	  	current_page: 1,
	   	view_mode: getViewMode(),
	    	scroll_position: 0,
	},
  	harmony_age: {
  		data: [],
  		total_result: -1,
  		total_elements: 20,
  		current_page: 1,
    		view_mode: getViewMode(),
    		scroll_position: 0,
  	},
  	harmony_fate: {
  		data: [],
  		total_result: -1,
  		total_elements: 20,
  		current_page: 1,
    		view_mode: getViewMode(),
    		scroll_position: 0,
  	},
  	harmony_horoscope: {
  		data: [],
  		total_result: -1,
  		total_elements: 20,
  		current_page: 1,
    		view_mode: getViewMode(),
    		scroll_position: 0,
  	},
};

function search(state = defaultState, action) {
	switch (action.type) {
		case SEARCH_USER_REQUEST:
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: null
			})
		case SEARCH_USER_SUCCESS:
			const search_data = state.updated_condition ? action.response.data : [...state.default.data,...action.response.data];
			const unique_data = uniqUsers(search_data);
			const total_duplicate_users = search_data.length - unique_data.length;
			let total_result = action.response.data.length
				? action.response.paging_model.total_item - total_duplicate_users
				: state.default.total_result;
			if (!action.response.data.length || action.response.data.length % SEARCH_PAGE_SIZE !== 0) {
				total_result = unique_data.length;
			}
			return Object.assign({}, state, {
				isFetching: false,
				default: {...state.default, total_result: total_result, data: unique_data,},
				updated_condition: false,
				errorMessage: null
			})
		case SEARCH_USER_FAILURE:
			notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		// new users
		case SEARCH_NEW_USER_REQUEST:
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: null
			})
		case SEARCH_NEW_USER_SUCCESS:
			const search_new_data = [...state.new.data,...action.response.data];
			const unique_new_data = uniqAndFilterUsers(search_new_data, state.boostInRankUser);
			const total_duplicate_new_users = search_new_data.length - unique_new_data.length;
			let total_new_result = action.response.data.length
				? action.response.paging_model.total_item - total_duplicate_new_users
				: state.new.total_new_result;
			if (!action.response.data.length || action.response.data.length % SEARCH_PAGE_SIZE !== 0) {
				total_new_result = unique_new_data.length;
			}
			return Object.assign({}, state, {
				isFetching: false,
				new: {...state.new, total_result: total_new_result, data: unique_new_data,},
				errorMessage: null
			})
		case SEARCH_NEW_USER_FAILURE:
			notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		// online users
		case SEARCH_ONLINE_USER_REQUEST:
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: null
			})
		case SEARCH_ONLINE_USER_SUCCESS:
			const search_online_data = [...state.online.data,...action.response.data];
			const unique_online_data = uniqAndFilterUsers(search_online_data, state.boostInRankUser);
			const total_duplicate_online_users = search_online_data.length - unique_online_data.length;
			let total_online_result = action.response.data.length
				? action.response.paging_model.total_item - total_duplicate_online_users
				: state.online.total_online_result;
			if (!action.response.data.length || action.response.data.length % SEARCH_PAGE_SIZE !== 0) {
				total_online_result = unique_online_data.length;
			}
			return Object.assign({}, state, {
				isFetching: false,
				online: {...state.online, total_result: total_online_result, data: unique_online_data,},
				errorMessage: null
			})
		case SEARCH_ONLINE_USER_FAILURE:
			notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		case SEARCH_MATCHING_RATIO_USER_REQUEST:
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: null
			})
		case SEARCH_MATCHING_RATIO_USER_SUCCESS:
			const search_match_data = [...state.matching_ratio.data,...action.response.data];
			const unique_match_data = uniqUsers(search_match_data);
			const total_duplicate_match_users = search_match_data.length - unique_match_data.length;
			let total_match_result = action.response.data.length
				? action.response.paging_model.total_item - total_duplicate_match_users
				: state.matching_ratio.total_match_result;
			if (!action.response.data.length || action.response.data.length % SEARCH_PAGE_SIZE !== 0) {
				total_match_result = unique_match_data.length;
			}
			return Object.assign({}, state, {
				isFetching: false,
				matching_ratio: {...state.matching_ratio, total_result: total_match_result, data: unique_match_data,},
				errorMessage: null
			})
		case SEARCH_MATCHING_RATIO_USER_FAILURE:
			notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		case SEARCH_NEARBY_USER_REQUEST:
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: null
			})
		case SEARCH_NEARBY_USER_SUCCESS:
			const search_nearby_data = [...state.nearby.data,...action.response.data];
			const unique_nearby_data = uniqUsers(search_nearby_data);
			const total_duplicate_nearby_users = search_nearby_data.length - unique_nearby_data.length;
			let total_nearby_result = action.response.data.length
				? action.response.paging_model.total_item - total_duplicate_nearby_users
				: state.nearby.total_nearby_result;
			if (!action.response.data.length || action.response.data.length % SEARCH_PAGE_SIZE !== 0) {
				total_nearby_result = unique_nearby_data.length;
			}
			return Object.assign({}, state, {
				isFetching: false,
				nearby: {...state.nearby, total_result: total_nearby_result, data: unique_nearby_data,},
				errorMessage: null
			})
		case SEARCH_NEARBY_USER_FAILURE:
			notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		case SEARCH_HARMONY_OF_AGE_REQUEST:
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: null
			})
		case SEARCH_HARMONY_OF_AGE_SUCCESS:
			const search_harmony_age_data = [...state.harmony_age.data,...action.response.data];
			const unique_harmony_age_data = uniqUsers(search_harmony_age_data);
			const total_duplicate_harmony_age_users = search_harmony_age_data.length - unique_harmony_age_data.length;
			let total_harmony_age_result = action.response.data.length
				? action.response.paging_model.total_item - total_duplicate_harmony_age_users
				: state.harmony_age.total_harmony_age_result;
			if (!action.response.data.length || action.response.data.length % SEARCH_PAGE_SIZE !== 0) {
				total_harmony_age_result = unique_harmony_age_data.length;
			}
			return Object.assign({}, state, {
				isFetching: false,
				harmony_age: {...state.harmony_age, total_result: total_harmony_age_result, data: unique_harmony_age_data,},
				errorMessage: null
			})
		case SEARCH_HARMONY_OF_AGE_FAILURE:
			notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		case SEARCH_HARMONY_OF_FATE_REQUEST:
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: null
			})
		case SEARCH_HARMONY_OF_FATE_SUCCESS:
			const search_harmony_fate_data = [...state.harmony_fate.data,...action.response.data];
			const unique_harmony_fate_data = uniqUsers(search_harmony_fate_data);
			const total_duplicate_harmony_fate_users = search_harmony_fate_data.length - unique_harmony_fate_data.length;
			let total_harmony_fate_result = action.response.data.length
				? action.response.paging_model.total_item - total_duplicate_harmony_fate_users
				: state.harmony_fate.total_harmony_fate_result;
			if (!action.response.data.length || action.response.data.length % SEARCH_PAGE_SIZE !== 0) {
				total_harmony_fate_result = unique_harmony_fate_data.length;
			}
			return Object.assign({}, state, {
				isFetching: false,
				harmony_fate: {...state.harmony_fate, total_result: total_harmony_fate_result, data: unique_harmony_fate_data,},
				errorMessage: null
			})
		case SEARCH_HARMONY_OF_FATE_FAILURE:
			notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		case SEARCH_HARMONY_OF_HOROSCOPE_REQUEST:
			return Object.assign({}, state, {
				isFetching: true,
					errorMessage: null
			})
		case SEARCH_HARMONY_OF_HOROSCOPE_SUCCESS:
			const search_harmony_horoscope_data = [...state.harmony_horoscope.data,...action.response.data];
			const unique_harmony_horoscope_data = uniqUsers(search_harmony_horoscope_data);
			const total_duplicate_harmony_horoscope_users = search_harmony_horoscope_data.length - unique_harmony_horoscope_data.length;
			let total_harmony_horoscope_result = action.response.data.length
				? action.response.paging_model.total_item - total_duplicate_harmony_horoscope_users
				: state.harmony_horoscope.total_harmony_horoscope_result;
			if (!action.response.data.length || action.response.data.length % SEARCH_PAGE_SIZE !== 0) {
				total_harmony_horoscope_result = unique_harmony_horoscope_data.length;
			}
			return Object.assign({}, state, {
				isFetching: false,
				harmony_horoscope: {...state.harmony_horoscope, total_result: total_harmony_horoscope_result, data: unique_harmony_horoscope_data,},
				errorMessage: null
			})
		case SEARCH_HARMONY_OF_HOROSCOPE_FAILURE:
			notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		case SEARCH_CONDITION_UPDATE_SUCCESS:
			return Object.assign({}, state, {
  			isFetching: false,
  			default: { ...state.default, current_page: 1,},
	      condition: action.response.data,
		    updated_condition: true,
	      errorMessage: null
		  })

		case SEARCH_CONDITION_UPDATE_FAILURE:
			notification(NOTIFICATION_ERROR, action.error_message)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		//handle get search condition
		case SEARCH_CONDITION_GET_REQUEST:
			return Object.assign({}, state, {
				isFetching: true
			})
		case SEARCH_CONDITION_GET_SUCCESS:
			return Object.assign({}, state, {
				isFetching: false,
				condition: action.response.data,
        loaded_condition: true,
				errorMessage: null
			})
		case SEARCH_CONDITION_GET_FAILURE:
			notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.error_message
			})

		case SEARCH_UPDATE_VIEW_MODE:
			return Object.assign({}, state, {
				[action.enviroment]: {...state[action.enviroment], view_mode: action.view_mode,}
			})

		case UPDATE_PAGING_INFORMATION:
			return Object.assign({}, state, {
	        		[action.enviroment]: {...state[action.enviroment], total_elements: action.total_elements, current_page: action.current_page, scroll_position: action.scroll_position },
	      			[action.next_enviroment]: (action.next_enviroment === 'new' || action.next_enviroment === 'online') ? {...state[action.next_enviroment], data: [], scroll_position:0 } : {...state[action.next_enviroment] } ,
	        		search_mode: action.enviroment,
	      		})

	    	case NEED_SCROLL_TO_POSITION:
	    		return Object.assign({}, state, {
					need_scroll_to_position: action.need_scroll_to_position
			})

  	case SEARCH_RESET_DATA:
      switch (action.pageSource) {
        case 'online':
          return Object.assign({}, state, {
            online: { ...state.online, data: [], scroll_position: 0,},
          });
        case 'new':
          return Object.assign({}, state, {
            new: { ...state.new, data: [], scroll_position: 0,},
          });
        case 'default':
          return Object.assign({}, state, {
            default: { ...state.default, data: [], scroll_position: 0,},
          });
        default:
          return state;
      }

		case UPDATE_SEARCH_USER_PROFILE:
			const env = ['default', 'new', 'nearby', 'online', 'matching_ratio', 'harmony_age', 'harmony_fate', 'harmony_horoscope']
			let newState = state
			for (let i = 0; i < env.length; i += 1) {
				newState = Object.assign({}, newState, {
					[env[i]]: {...newState[env[i]], data: newState[env[i]].data.map(user => {
						if (compareIdentifiers(user.identifier, action.identifier)) {
							return Object.assign({}, user, {
								[action.key]: action.value
							})
						}
						return user;
					})
					}
				})
			}
			return newState
		case UPDATE_SEARCH_STATE:
			const enviroment = ['default', 'new', 'nearby', 'online', 'matching_ratio', 'harmony_age', 'harmony_fate', 'harmony_horoscope']
			let new_State = state
			for (let i = 0; i < enviroment.length; i += 1) {
				let user_index = new_State[enviroment[i]].data.findIndex(userProfile =>
					compareIdentifiers(userProfile.identifier, action.identifier))
				if (user_index == -1) {
					continue
				}
				let updateData = {}
				updateData[action.key] = action.value
				const data =  [
				  ...new_State[enviroment[i]].data.slice(0,user_index), // before the one we are updating
				  {...new_State[enviroment[i]].data[user_index], ...updateData},
				  ...new_State[enviroment[i]].data.slice(user_index + 1), // after the one we are updating
				];
				let new_total_result = new_State[enviroment[i]].total_result
				if (action.key === 'block_type') {
				  new_total_result = parseInt(action.value) == 1 ? new_State[enviroment[i]].total_result - 1 : (new_State[enviroment[i]].data[user_index]['block_type'] ? new_State[enviroment[i]].total_result + 1 : new_State[enviroment[i]].total_result)
				}
				new_State = Object.assign({}, new_State, {
					[enviroment[i]]: {...new_State[enviroment[i]], data: user_index > -1 ? data : new_State[enviroment[i]].data, total_result: new_total_result}
				})
			}
			return new_State;
    case GET_LIST_BOOST_IN_RANK_SUCCESS:
      return Object.assign({}, state, {
        boostInRankUser: [...action.response.data]
      })
      case SEARCH_RESET_SELECTED:
      	const newDefaultState = {...defaultState};
      	return Object.assign({}, state, {
      		condition: [newDefaultState.condition[0], state.condition[1]]
      	})
		default:
			return state;
		}
}

export default search;
