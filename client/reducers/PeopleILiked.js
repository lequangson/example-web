import { GET_PEOPLEILIKED_REQUEST, GET_PEOPLEILIKED_SUCCESS,
         GET_PEOPLEILIKED_FAILURE,
 } from '../constants/PeopleILiked'
import { PEOPLE_I_LIKED_PAGE, NOTIFICATION_ERROR } from '../constants/Enviroment'
import { UPDATE_USER_PROFILE_STATE, VOTE, UNVOTE } from '../constants/userProfile'
import { VOTE_PHOTO_REQUEST, VOTE_PHOTO_FAILURE } from '../constants/photoStream'
import { compareIdentifiers } from '../utils/common'
import notification from '../utils/errors/notification'
import { DEFAULT_ERROR_LOAD_PAGE } from '../constants/TextDefinition'

function PeopleILiked(state = {
  isFetching: false,
  loaded: false,
  error: false,
  data: [],
}, action) {
  switch (action.type) {
    case GET_PEOPLEILIKED_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false,
      })
    case GET_PEOPLEILIKED_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        loaded: true,
        data: action.response.data,
      })
    case GET_PEOPLEILIKED_FAILURE:
      notification(NOTIFICATION_ERROR, DEFAULT_ERROR_LOAD_PAGE)
      return Object.assign({}, state, {
        isFetching: false,
        error: true,
      })
    case UPDATE_USER_PROFILE_STATE:
      if (action.page == PEOPLE_I_LIKED_PAGE) {
        const user_index = state.data.findIndex(userProfile =>
          compareIdentifiers(userProfile.identifier, action.identifier))
        const new_data = [
          ...state.data.slice(0, user_index), // before the one we are updating
          { ...state.data[user_index], block_type: action.value },
          ...state.data.slice(user_index + 1), // after the one we are updating
        ];
        return Object.assign({}, state, {
          data: new_data,
        })
      }
      return state
    default:
      return state
  }
}

export default PeopleILiked;