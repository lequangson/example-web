import {
  GET_PHOTO_STREAM_SUCCESS, GET_PHOTO_STREAM_REQUEST,
  OPEN_PHOTO_STREAM_MODAL,
  CLOSE_PHOTO_STREAM_MODAL,
  UPDATE_INFO_PHOTO_STREAM_MODAL,
  UPDATE_PHOTO_STREAM_STATE,
  VOTE_PHOTO_REQUEST, VOTE_PHOTO_FAILURE,
  GET_VOTED_USERS_REQUEST, GET_VOTED_USERS_SUCCESS, GET_VOTED_USERS_FAILURE,
} from '../constants/photoStream'
import { VOTE, UNVOTE } from '../constants/userProfile'
import { PHOTO_STREAM, VOTED_MODAL_PAGE } from '../constants/Enviroment'
import { DEFAULT_ERROR_LOAD_PAGE } from '../constants/TextDefinition'
import { 
  compareIdentifiers, getUserIdFromIdentifier, getUserId, 
  uniqUsers, 
} from '../utils/common'
import {
  SEARCH_PAGE_SIZE,
} from 'constants/search';

const defaultState = {
  photos: [],
  reachEnd: false,
  pageIndex: 1,
  isModalOpen: false,
  isLoaded: false,
  modalInfo: {},
  currentPhotoIndex: 0,
  voted_users: [],
  isShowPictureById: true,
}

function filterUniquePhotos(listPhotos, newPhotos) {
  if (!newPhotos) {
    return listPhotos;
  }
  
  if (!listPhotos) {
    return newPhotos;
  }

  const newUniquePhotos = newPhotos.filter(
    newPhoto => !listPhotos.find(photo => photo.picture.id === newPhoto.picture.id)
  );
  return [...listPhotos, ...newUniquePhotos];
}

function photoStream(state = defaultState, action) {
  switch (action.type) {
    case GET_PHOTO_STREAM_REQUEST:
      return Object.assign({}, state, {
        isLoaded: false,
      })

    case GET_PHOTO_STREAM_SUCCESS:
      return Object.assign({}, state, {
        photos: action.extentions.pageIndex <= 1 ?
                action.response.data : filterUniquePhotos(state.photos, action.response.data),
        isLoaded: true,
        reachEnd: action.response.data.length === 0,
        pageIndex: action.extentions.pageIndex + 1,
      })

    case OPEN_PHOTO_STREAM_MODAL:
      return Object.assign({}, state, {
        isModalOpen: true,
        isShowPictureById: false,
      })

    case CLOSE_PHOTO_STREAM_MODAL:
      return Object.assign({}, state, {
        isModalOpen: false,
      })

    case UPDATE_INFO_PHOTO_STREAM_MODAL:
      return Object.assign({}, state, {
        modalInfo: action.photo,
        currentPhotoIndex: action.photoIndex,
      })

    case UPDATE_PHOTO_STREAM_STATE:
      let newState = state
      let user_index = state.photos.filter(photo =>
        compareIdentifiers(photo.user_profile.identifier, action.identifier))
      if (user_index.length == 0) {
        return state
      }
      let data = []
      for (let i = 0; i < user_index.length; i += 1) {
        const index = newState.photos.indexOf(user_index[i])
        data = [
          ...newState.photos.slice(0,index),
          {...newState.photos[index], user_profile: {...newState.photos[index].user_profile, [action.key]: action.value}},
          ...newState.photos.slice(index + 1),
        ]
        newState = Object.assign({}, newState, {
          photos: user_index.length > -1 ? data : state.photos
        })
      }
      return newState
    case GET_VOTED_USERS_SUCCESS:
      const unique_voted_users = uniqUsers(action.response.data);
      return Object.assign({}, state, {
        voted_users: unique_voted_users,
      })
    default:
      return state
  }
}

export default photoStream
