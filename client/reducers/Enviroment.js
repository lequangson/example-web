import {
  SEND_NOTIFICATION, UPDATE_PREVIOUS_PAGE, UPDATE_SCROLL_POSSITION,
  SEARCH_PAGE, PEOPLE_LIKED_ME_PAGE, NOTIFICATION_SUCCESS,
  BLOCK_USER_BLOCKED_PAGE, BLOCK_USER_MUTED_PAGE, NOTIFICATION_ERROR,
  MY_PROFILE_PAGE, SAVE_FILES_UPLOAD, CLEAR_FILES_UPLOAD,
  FAVORITE_PAGE, MATCHED_PAGE, ADD_ACTIVITY_AUDIT_REQUEST, ADD_ACTIVITY_AUDIT_SUCCESS, ADD_ACTIVITY_AUDIT_FAILURE,
  FOOTPRINT_PAGE, SET_VIEW_PROFILE_SOURCE, SET_PROFILE_NEXT_USER_INDEX,
  MODAL_SHOW_NEXT, UPDATE_SEARCH_ELEMENT_HEIGHT, PEOPLE_I_LIKED_PAGE,
  FACEBOOK_SYNC_REQUEST, FACEBOOK_SYNC_POST_SUCCESS, SAVE_IMAGE,
  FACEBOOK_SYNC_GET_SUCCESS, FACEBOOK_SYNC_FAILURE, SEND_CONTACT_REQUEST, SEND_CONTACT_SUCCESS, SEND_CONTACT_FAILURE,
  UPDATE_UPLOAD_PICTURE_PAGESOURCE, UPDATE_LAST_UPDATE, UPDATE_PUSH_NOTI_MODAL, UPDATE_ENVIRONMENT_STATE, TOTAL_BANNER,
  GET_REMIND_SUCCESS, GET_REMIND_REQUEST, GET_REMIND_FAILURE,
  UPDATE_REMIND_SUCCESS, UPDATE_REMIND_REQUEST, UPDATE_REMIND_FAILURE,
  SHOW_NEXT_REMIND_USER, REMIND_BANNER, DISABLE_THIRD_PARTY_COOKIES
} from '../constants/Enviroment'
import notification from '../utils/errors/notification'
import { LOADING_DATA_TEXT } from '../constants/TextDefinition'
import {
  UPLOAD_DEVIDE_MODE, UPLOAD_FACEBOOK_MODE,
  UPLOAD_CAPTURE_MODE, UPDATE_USER_PROFILE_STATE } from '../constants/userProfile'
import * as ymmStorage from '../utils/ymmStorage';

const date = new Date();
function enviroment(state = {
    file: {},
    current_page: '',
    previous_page: ymmStorage.getItem('previous_page'),
    profile_next_user_index: 0,
    search_scroll_top: 0,
    my_profile_scroll_top: 0,
    favorite_scroll_top: 0,
    people_liked_me_scroll_top: 0,
    people_i_liked_scroll_top: 0,
    visitants_scroll_top: 0,
    blocked_user_scroll_top: 0,
    muted_user_scroll_top: 0,
    matched_list_scroll_top: 0,
    files_upload: [],
    facebook_pictures_synch: [],
    view_profile_source: '',
    modalShowNext: '',
    search_element_height_list_view: 0,
    search_element_height_detail_view:0,
    search_view_mode: '',
    isFetching: false,
    facebookSynchronize: '',
    upload_picture_page_source: MY_PROFILE_PAGE,
    lastUpdate: date.getTime(),
    isPushNotiModalOpen: false,
    pushNotiModalType: 0,
    pushNotiModalNickname: '',
    isLoadingText: LOADING_DATA_TEXT,
    remind: {
      remindIndex: 0,
      data: 'unload',
    },
    isLoaded: false,
    disable3rdPartyCookies: false,
    pathname: '',
  },
  action) {
    switch (action.type) {
      //-------------------Begin: Update previous page--------------------------//
      case UPDATE_PREVIOUS_PAGE:
        ymmStorage.setItem('previous_page', action.previous_page)
        return Object.assign({}, state, {
          previous_page: action.previous_page
        })
      //-------------------End  : Update previous page--------------------------//
      case UPDATE_UPLOAD_PICTURE_PAGESOURCE:
        return Object.assign({}, state, {
          upload_picture_page_source: action.page
        })
      //-------------------Begin: Update scroll position------------------------//
      case UPDATE_SCROLL_POSSITION:
        return Object.assign({}, state, {
          search_scroll_top: action.page === SEARCH_PAGE? action.scroll_top : state.search_scroll_top,
          my_profile_scroll_top: action.page === MY_PROFILE_PAGE? action.scroll_top : state.my_profile_scroll_top,
          people_liked_me_scroll_top: action.page === PEOPLE_LIKED_ME_PAGE? action.scroll_top : state.people_liked_me_scroll_top,
          favorite_scroll_top: action.page === FAVORITE_PAGE ? action.scroll_top : state.favorite_scroll_top,
          visitants_scroll_top: action.page === FOOTPRINT_PAGE ? action.scroll_top : state.visitants_scroll_top,
          blocked_user_scroll_top: action.page === BLOCK_USER_BLOCKED_PAGE ? action.scroll_top : state.blocked_user_scroll_top,
          muted_user_scroll_top: action.page === BLOCK_USER_MUTED_PAGE ? action.scroll_top : state.muted_user_scroll_top,
          people_i_liked_scroll_top: action.page === PEOPLE_I_LIKED_PAGE? action.scroll_top : state.people_i_liked_scroll_top,
          matched_list_scroll_top: action.page === MATCHED_PAGE? action.scroll_top : state.matched_list_scroll_top,
        })
      //-------------------End  : Update scroll position------------------------//
      case SEND_NOTIFICATION:
        const timeoutValue = action.timeout? action.timeout : 0;
        notification((action.type_error ? action.type_error : NOTIFICATION_SUCCESS), action.msg, timeoutValue);
        return state

      case SAVE_FILES_UPLOAD:
        return Object.assign({}, state, {
          files_upload: action.uploadType == UPLOAD_DEVIDE_MODE ||
            action.uploadType == UPLOAD_CAPTURE_MODE ?
            action.files : state.files_upload,
          facebook_pictures_synch: action.uploadType == UPLOAD_FACEBOOK_MODE? action.files : state.facebook_pictures_synch
        })
      case SAVE_IMAGE:
        return Object.assign({}, state, {
          file: action.file,
        })
      case CLEAR_FILES_UPLOAD:
        return Object.assign({}, state, {
          files_upload: action.uploadType == UPLOAD_DEVIDE_MODE? [] : state.files_upload,
          facebook_pictures_synch: action.uploadType == UPLOAD_FACEBOOK_MODE? [] : state.facebook_pictures_synch
        })
      case SET_VIEW_PROFILE_SOURCE:
        return Object.assign({}, state, {
          view_profile_source: action.viewSource
        })
      case SET_PROFILE_NEXT_USER_INDEX:
        return Object.assign({}, state, {
          profile_next_user_index: action.next_index
        })
      case MODAL_SHOW_NEXT:
        return Object.assign({}, state, {
          modalShowNext: action.modalName
        })
      case UPDATE_SEARCH_ELEMENT_HEIGHT:
        return Object.assign({}, state, {
          [action.key]: action.value,
          search_view_mode: action.view_mode
        })
      case FACEBOOK_SYNC_REQUEST:
        return Object.assign({}, state, {
          isFetching: true
        })
      case FACEBOOK_SYNC_POST_SUCCESS:
        return Object.assign({}, state, {
          isFetching: false
        })
      case FACEBOOK_SYNC_GET_SUCCESS:
        return Object.assign({}, state, {
          facebookSynchronize: action.response.data
        })
      case FACEBOOK_SYNC_FAILURE:
        return Object.assign({}, state, {
          isFetching: false
        })
      case SEND_CONTACT_REQUEST:
        return Object.assign({}, state, {
          isFetching: true
        })
      case SEND_CONTACT_SUCCESS:
        return Object.assign({}, state, {
          isFetching: false
        })
      case SEND_CONTACT_FAILURE:
        notification(NOTIFICATION_ERROR, action.error_message)
        return Object.assign({}, state, {
          isFetching: false
        })
      case UPDATE_LAST_UPDATE:
        return Object.assign({}, state, {
          lastUpdate: action.time
        })
      case UPDATE_PUSH_NOTI_MODAL:
      return Object.assign({}, state, {
        isPushNotiModalOpen: action.isOpen,
        pushNotiModalType: action.modalType,
        pushNotiModalNickname: action.nick_name,
      })
      case UPDATE_ENVIRONMENT_STATE:
        const current_environment = state;
        current_environment[action.key] = action.value
        return Object.assign({}, current_environment)

      case GET_REMIND_SUCCESS:
        state.remind.data = action.response.data
        return Object.assign({}, state, {
          isFetching: false,
          error: false,
          isLoaded: true,
        })
      case UPDATE_REMIND_SUCCESS:
        return Object.assign({}, state, {
          isFetching: false,
          error: false,
        })
      case UPDATE_USER_PROFILE_STATE:
        if (action.page === REMIND_BANNER) {
          return Object.assign({}, state, {
            remind: {...state.remind, block_type: action.value},
          })
        }
        return state
      case SHOW_NEXT_REMIND_USER:
        return Object.assign({}, state, {
          remind: {...state.remind, remindIndex: state.remind.remindIndex === state.remind.data.length - 1 ? 0 : state.remind.remindIndex + 1},
        })
      case DISABLE_THIRD_PARTY_COOKIES:
        return Object.assign({}, state, {
            disable3rdPartyCookies: action.disabled,
          })
      default:
        return state
    }
}

export default enviroment;
