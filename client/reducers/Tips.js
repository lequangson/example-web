// import {
// } from '../constants/Tips'

function Tips(state = {
  isFetching: false,
  error: false,
  banners: [
    {
      icon_number: '1',
      heading: 'Ảnh đại diện thu hút',
      html: 'Hạ gục đối phương trong 1 nốt nhạc!',
      plainText: 'Hạ gục đối phương trong 1 nốt nhạc!',
      url: '/helps/6/2?bannerIndex=0',
    },
    // {
    //   icon_number: '2',
    //   heading: 'Cập nhật đủ thông tin',
    //   html: 'Cơ hội tìm được người ấy <strong>tăng 90%</strong>',
    //   plainText: 'Cơ hội tìm được người ấy tăng 90%',
    //   url: '/helps/2/1?bannerIndex=1',
    // },
    {
      icon_number: '3',
      heading: 'Xác thực tài khoản',
      html: 'Hẹn hò an toàn dễ dàng trong tầm tay',
      plainText: 'Hẹn hò an toàn dễ dàng trong tầm tay',
      url: '/verification',
    },
    {
      icon_number: '4',
      heading: 'Nâng cấp tài khoản',
      html: 'Trải nghiệm hẹn hò không giới hạn',
      plainText: 'Trải nghiệm hẹn hò không giới hạn',
      url: '/payment/upgrade',
    },
    {
      icon_number: '5',
      heading: 'Sạc đầy hồ sơ của bạn',
      html: 'Thông tin đầy đủ - Tình đầy tim ngay!',
      plainText: 'Thông tin đầy đủ - Tình đầy tim ngay!',
      url: '/myprofile',
    },
    {
      icon_number: '6',
      heading: 'Nhận thông báo offline',
      html: 'Cập nhật mọi động thái của người ấy',
      plainText: 'Cập nhật mọi động thái của người ấy',
      url: '/helps/6/7?bannerIndex=5',
    },
    {
      icon_number: '7',
      heading: 'Không còn lo yêu xa!',
      html: '<i class="fa fa-music"></i><strike> Khoảng cách giết đôi ta trong phút giây </strike><i class="fa fa-music"></i>',
      plainText: 'Khoảng cách giết đôi ta trong phút giây',
      url: '/search/nearby',
    },
    {
      icon_number: '8',
      heading: 'Chào các bạn, tui là Xu!',
      html: 'Nạp năng lượng cho tình yêu của bạn đi nè',
      plainText: 'Nạp năng lượng cho tình yêu của bạn đi nè',
      url: '/coin-instruction',
    },
  ],
}, action) {
  switch (action.type) {
    default:
      return state
  }
}

export default Tips;
