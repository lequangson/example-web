var path = require('path');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var src_dir = path.dirname(path.dirname(__filename));
module.exports = {
  entry: [
    'whatwg-fetch',
    'babel-polyfill',
    './client/index'
  ],
  output: {
    path: './build/test',
    filename: 'js/bundle.js',
  },
  resolve: {
    root: [
      src_dir
    ]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('test'),
        'FACEBOOK_APP_ID': "'1715923778659669'",
        'BASE_API_AUTHEN_URL': "'//auth.test.ymeet.me:8511/'",
        'BASE_API_URL': "'//api.test.ymeet.me:8530/'",
        'CHAT_SERVER_URL' : "'http://test.ymeet.me/'",
        'CHAT_API_URL': "'http://chat-api.test.ymeet.me:8598/'",
        'SOCKET_URL': "'//socket.test.ymeet.me:8587/'",
        'API_GENERATE_KEY_URL': "'//api-key.test.ymeet.me:8587/api/get-api-key'",
        'ANONYMOUS_TOKEN': "'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjowLCJpcF9hZGRyZXNzIjpudWxsLCJpYXQiOjE0ODIzOTEyNDIsImV4cCI6ODk0NzM1MTI0Mn0.A4QYSOq1BBiCGoWqB89A6Hl_dgcDTubmXBsUcJQgXaU'",
        'DOMAIN':"'test.ymeet.me'",
        'ROOT_URL':"'http://test.ymeet.me'",
        'CUSTOMER_SUPPORT_USER_ID': "3759",
        'FCM_SERVER_PUBLIC_KEY': "'BNVDDGV2omVj1ZQipqcoyAeouJA36pCkYYXXNjD5PsLGzxuZveBpBPlI5M0CJUfhx4F8aoVtqfxeOCqG1ClPKoE'",
        'GOOGLE_API_CLIENT_ID': "'888754797622-697vcqrk2ie8ela4280e5emd1b1sip0s.apps.googleusercontent.com'",

        'REACT_APP_NODE_ENV': "'test'",
        'REACT_APP_DOMAIN': "'test.ymeet.me'",
        'REACT_APP_SOCKET_URL': "'//chat-socket.test.ymeet.me/'",
        'REACT_APP_API_AUTH_URL': "'//auth.test.ymeet.me/'",
        'REACT_APP_API_URL': "'//api.test.ymeet.me/'",
        'REACT_APP_FACEBOOK_APP_ID': "'1715923778659669'",
        'REACT_APP_API_GENERATE_KEY_URL': "'//socket.test.ymeet.me/api/get-api-key'",

        'REACT_APP_CHAT_API_URL': "'//chat-api.test.ymeet.me/'",
        'REACT_APP_CHAT_URL': "'//chat.test.ymeet.me/'",
        'REACT_APP_YMM_API_URL': "'//api.test.ymeet.me/'",
        'REACT_APP_YMM_AUTH_URL': "'//auth.test.ymeet.me/'",
        'REACT_APP_YMM_URL': "'//test.ymeet.me/'",

        'REACT_APP_YMM_AVATAR_DEFAULT': "'https://s3.amazonaws.com/ymmtest/avatar/male_default_thumb.jpg'",
        'REACT_APP_YMM_MOBILE_URL': "'http://test.ymeet.me/'",
        'REACT_APP_PUBLIC_CDN': "'https://cdn-static.ymeet.me'",
        'REACT_APP_DEFAULT_MALE_AVATAR': "'/general/male_default_small.jpg'",
        'REACT_APP_DEFAULT_FEMALE_AVATAR': "'/general/female_default_small.jpg'",
        'REACT_APP_CUSTOMER_SUPPORT_USER_ID': "'3759'",
        'REACT_APP_RAVEN': "'https://ec21aee1aa4840818b2cca9542dbe2e6@sentry.io/169057'",
        'REACT_APP_FCM_SERVER_PUBLIC_KEY': "'BNVDDGV2omVj1ZQipqcoyAeouJA36pCkYYXXNjD5PsLGzxuZveBpBPlI5M0CJUfhx4F8aoVtqfxeOCqG1ClPKoE'",
        'REACT_APP_MOBILE_SOCKET': "'//socket.test.ymeet.me/'"
      }
    }),
    // new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new CopyWebpackPlugin([
        // {output}/file.txt
        { from: './public/favicon.ico' },
        { from: './public/index.html' },
        { from: './public/sharing.html' },
        { from: './public/index.js' },
        { from: './public/package.json' },
        { from: './public/.htaccess' },
        { from: './public/robots.txt' },
        { from: './public/sitemap.xml' },
        { from: './public/service_worker.js' },
        { from: './public/landingpage-assets', to: 'landingpage-assets' },
        { from: './public/scripts/main.js', to: 'scripts/main.js' },
        { from: './public/scripts/analytics.js', to: 'scripts/analytics.js' },
        { from: './public/manifest', to: 'manifest' }
    ]),
    new ExtractTextPlugin("/css/style.css"),
    new webpack.optimize.UglifyJsPlugin({
      // cacheFolder: path.resolve(__dirname, 'public/cached_uglify/'),
      debug: true,
      minimize: true,
      sourceMap: false,
      output: {
        comments: false
      },
      compressor: {
        warnings: false
      }
    })
  ],
  module: {
    loaders: [
    // js
    {
      test: /\.js$/,
      // exclude: /node_modules/,
      loaders: ['babel'],

      include: path.join(__dirname, 'client')
    },
    // CSS
    {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract("style-loader", "css-loader")
      // loader: 'style-loader!css-loader!postcss-loader'
    },
    { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/font-woff" },
    { test: /\.(ttf|eot|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=/fonts/[name].[ext]" },
    { test: /\.(jpg|jpg\?\w+|png\?\w+|png)$/, loader: "url-loader?name=/images/[name].[ext]&limit10000&mimetype=image/jpg" },
    ]
  }
};
