var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'source-map',
  entry: [
    'whatwg-fetch',
    'babel-polyfill',
    'webpack-hot-middleware/client',
    './client/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    path: __dirname + '/public/',
    filename: 'js/bundle.js',
    publicPath: '/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': "'development'",
        'FACEBOOK_APP_ID': "'1715923778659669'",
        'BASE_API_AUTHEN_URL': "'//auth.test.ymeet.me:8511/'",
        'BASE_API_URL': "'//api.test.ymeet.me:8530/'",
        'CHAT_SERVER_URL' : "'http://localhost:8088/'",
        'CHAT_API_URL': "'http://chat-api.test.ymeet.me:8598/'",
        'SOCKET_URL': "'//socket.test.ymeet.me:8587/'",
        'API_GENERATE_KEY_URL': "'//api-key.test.ymeet.me:8587/api/get-api-key'",
        // 'API_GENERATE_KEY_URL': "'http://localhost:8587/api/get-api-key'",
        'ANONYMOUS_TOKEN': "'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjowLCJpcF9hZGRyZXNzIjpudWxsLCJpYXQiOjE0ODIzOTEyNDIsImV4cCI6ODk0NzM1MTI0Mn0.A4QYSOq1BBiCGoWqB89A6Hl_dgcDTubmXBsUcJQgXaU'",
        'DOMAIN':"'localhost'", // this for cookie
        'ROOT_URL':"'http://localhost:8088'",
        'FCM_SERVER_PUBLIC_KEY': "'BNVDDGV2omVj1ZQipqcoyAeouJA36pCkYYXXNjD5PsLGzxuZveBpBPlI5M0CJUfhx4F8aoVtqfxeOCqG1ClPKoE'",
        'CUSTOMER_SUPPORT_USER_ID': "3759",
        'GOOGLE_API_CLIENT_ID': "'888754797622-697vcqrk2ie8ela4280e5emd1b1sip0s.apps.googleusercontent.com'",


        'REACT_APP_NODE_ENV': "'development'",
        'REACT_APP_DOMAIN': "'localhost'",
        'REACT_APP_SOCKET_URL': "'//chat-socket.test.ymeet.me/'",
        'REACT_APP_API_AUTH_URL': "'//auth.test.ymeet.me/'",
        'REACT_APP_API_URL': "'//api.test.ymeet.me/'",
        'REACT_APP_FACEBOOK_APP_ID': "'1715923778659669'",
        'REACT_APP_API_GENERATE_KEY_URL ': " '//socket.test.ymeet.me/api/get-api-key'",

        'REACT_APP_CHAT_API_URL': "'http://chat-api.test.ymeet.me/'",
        'REACT_APP_CHAT_URL': "'//localhost:3000/'",
        'REACT_APP_YMM_API_URL': "'//api.test.ymeet.me/'",
        'REACT_APP_YMM_AUTH_URL': "'//auth.test.ymeet.me/'",
        'REACT_APP_YMM_URL': "'//localhost:8088/'",

        'REACT_APP_YMM_MOBILE_URL': "'http://test.ymeet.me/'",
        'REACT_APP_PUBLIC_CDN': "'https://cdn-static.ymeet.me'",
        'REACT_APP_DEFAULT_MALE_AVATAR': "'/general/male_default_small.jpg'",
        'REACT_APP_DEFAULT_FEMALE_AVATAR': "'/general/female_default_small.jpg'",
        'REACT_APP_CUSTOMER_SUPPORT_USER_ID': "'3759'",
        'REACT_APP_RAVEN': "'https://ec21aee1aa4840818b2cca9542dbe2e6@sentry.io/169057'",
        'REACT_APP_FCM_SERVER_PUBLIC_KEY': "'BNVDDGV2omVj1ZQipqcoyAeouJA36pCkYYXXNjD5PsLGzxuZveBpBPlI5M0CJUfhx4F8aoVtqfxeOCqG1ClPKoE'",
        'REACT_APP_MOBILE_SOCKET': "'//socket.test.ymeet.me/'"
      }
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [
    // js
    {
      test: /\.js$/,
      loaders: ['babel'],
      include: path.join(__dirname, 'client')
    },
    // CSS
    {
      test: /\.styl$/,
      include: path.join(__dirname, 'client'),
      loader: 'style-loader!css-loader!stylus-loader'
    },
    { test: /\.scss$/, loader: 'css?modules&localIdentName=[local]!postcss!sass'},
    {
      test: /\.css$/,
      loader: 'style-loader!css-loader!postcss-loader'
    },
    { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
    { test: /\.(ttf|eot|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
    { test: /\.(jpg|jpg\?\w+|png\?\w+|png)$/, loader: "url-loader?limit10000&mimetype=image/jpg" },
    ]
  }
};
