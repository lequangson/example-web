FROM ubuntu:14.04

#use mirrors for faster apt downloads no matter where the machine that builds the image is
RUN echo "deb mirror://mirrors.ubuntu.com/mirrors.txt trusty main restricted universe multiverse" > /etc/apt/sources.list; \
  echo "deb mirror://mirrors.ubuntu.com/mirrors.txt trusty-updates main restricted universe multiverse" >> /etc/apt/sources.list; \
  echo "deb mirror://mirrors.ubuntu.com/mirrors.txt trusty-backports main restricted universe multiverse" >> /etc/apt/sources.list; \
  echo "deb mirror://mirrors.ubuntu.com/mirrors.txt trusty-security main restricted universe multiverse" >> /etc/apt/sources.list

#install required software before using nvm/node/npm
RUN apt-get update \
    && apt-get install -y curl git python build-essential \
    && apt-get -y autoclean

RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash

#change it to your required node version
ENV NODE_VERSION 6.8.0

#needed by nvm install
ENV NVM_DIR /root/.nvm

#install the specified node version and set it as the default one, install the global npm packages
RUN . ~/.nvm/nvm.sh && nvm install $NODE_VERSION && nvm alias default $NODE_VERSION

# add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# confirm installation
RUN node -v
RUN npm -v

ENV APP_HOME /ymm_mobile_ui

RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD . $APP_HOME