var path = require('path');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var src_dir = path.dirname(path.dirname(__filename));
module.exports = {
  entry: [
    'whatwg-fetch',
    'babel-polyfill',
    './client/index'
  ],
  output: {
    path: './build/production',
    filename: 'js/bundle.js',
  },
  resolve: {
    root: [
      src_dir
    ]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production'),
        'FACEBOOK_APP_ID': "'1591140367804678'",
        'BASE_API_AUTHEN_URL': "'//auth.ymeet.me/'",
        'BASE_API_URL': "'//api.ymeet.me/'",
        'CHAT_SERVER_URL' : "'https://m.ymeet.me/'",
        'CHAT_API_URL': "'//chat-api.ymeet.me/'",
        'SOCKET_URL': "'//socket.ymeet.me/'",
        'API_GENERATE_KEY_URL': "'//socket.ymeet.me/api/get-api-key'",
        'ANONYMOUS_TOKEN': "'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjowLCJpcF9hZGRyZXNzIjpudWxsLCJpYXQiOjE0ODIzOTEyOTcsImV4cCI6ODk0NzM1MTI5N30.mu8vXYKUa4d1h4FzDxlS6v1D8gj5_ICqPKFMkzxEZkE'",
        'DOMAIN':"'ymeet.me'",
        'ROOT_URL':"'https://m.ymeet.me'",
        // 'DOMAIN':"'localhost'",
        'CUSTOMER_SUPPORT_USER_ID': "5118",
        'FCM_SERVER_PUBLIC_KEY': "'BNVDDGV2omVj1ZQipqcoyAeouJA36pCkYYXXNjD5PsLGzxuZveBpBPlI5M0CJUfhx4F8aoVtqfxeOCqG1ClPKoE'",
        'GOOGLE_API_CLIENT_ID': "'310086174369-u7kigknnmsmjkueuau9kt177aj8n99m1.apps.googleusercontent.com'"
      }
    }),
    // new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new CopyWebpackPlugin([
        // {output}/file.txt
        { from: './public/favicon.ico' },
        { from: './public/index_production.html', to: 'index.html' },
        { from: './public/service_worker_production.js', to: 'service_worker.js' },
        { from: './public/sharing.html' },
        { from: './public/index.js' },
        { from: './public/package.json' },
        { from: './public/robots.txt' },
        { from: './public/sitemap.xml' },
        { from: './public/service_worker.js' },
        { from: './public/landingpage-assets', to: 'landingpage-assets' },
        { from: './public/scripts/main.js', to: 'scripts/main.js' },
        { from: './public/scripts/analytics.js', to: 'scripts/analytics.js' },
        { from: './public/manifest', to: 'manifest' }
    ]),
    new ExtractTextPlugin("/css/style.css"),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compressor: {
          warnings: false
      }
    })
  ],
  module: {
    loaders: [
    // js
    {
      test: /\.js$/,
      // exclude: /node_modules/,
      loaders: ['babel'],
      include: path.join(__dirname, 'client')
    },
    // CSS
    {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract("style-loader", "css-loader")
      // loader: 'style-loader!css-loader!postcss-loader'
    },
    { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/font-woff" },
    { test: /\.(ttf|eot|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=/fonts/[name].[ext]" },
    { test: /\.(jpg|jpg\?\w+|png\?\w+|png)$/, loader: "url-loader?name=/images/[name].[ext]&limit10000&mimetype=image/jpg" },
    ]
  }
};
